<%@page language="java" import="java.util.*,java.lang.*"%>
<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 26/10/2007
   // �ltima Actualizaci�n :
   // Bit�cora
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>

<%
  response.setHeader("Pragma","no-cache");
  response.setDateHeader("Expires",0);
  response.addHeader("Cache-Control","no-cache, no-store, must-revalidate");
   int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");

  int anno             = 0;
  int semestre         = 0;
 // fecha actual
    java.util.Calendar f = new java.util.GregorianCalendar();
    int a�o = f.get(java.util.Calendar.YEAR);
    int dia_actual = f.get(java.util.Calendar.DAY_OF_MONTH);
    int mes_actual = f.get(java.util.Calendar.MONTH)+1;
    String fecha_actual = (dia_actual<10)?"0"+dia_actual:dia_actual+"";
    if (mes_actual < 10) fecha_actual += "/0"+ mes_actual+"/"+ a�o;
    else fecha_actual += "/"+ mes_actual +"/"+a�o;
    general.getAbrirConexion();
    general.setGenerarConexionHorario();
    general.getPeriodoSemestreActual(1,1);
   if (anno == 0){
      semestre = general.getSemestreActual();
      anno = general.getA�oActual();
   }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Gimnasio - Estad�stica</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-Calendario -->
<link type="text/css" rel="stylesheet" href="js/dhtml_calendar/dhtml_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="js/dhtml_calendar/dhtml_calendar.js?random=20060118"></script>
<!-Estilos -->
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script src="js/centrar.js"></script>
<script language="JavaScript" type="text/JavaScript">
  function AbrirVentana(f,a) {
    var alto  = 550;
    var ancho = 800;
    f.target = "ventana";
    f.action = a;
    var par = 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=yes';
    var w = Centrar03(f.target,alto,ancho,par);
    w.focus();
    f.submit();
  }

  function Generar(){
    var tipo = document.form_listado.tipo_listado.value;
    var fecha_desde = document.form_listado.fecha_desde.value;
    var fecha_hasta = document.form_listado.fecha_hasta.value;
    var semestre = document.form_listado.semestre.value;
    if ((fecha_desde.substring(6,10)+fecha_desde.substring(3,5)+fecha_desde.substring(0,2))/1
        > (fecha_hasta.substring(6,10)+fecha_hasta.substring(3,5)+fecha_hasta.substring(0,2))/1)
    {
      alert ('La fecha Desde debe ser menor a la otra fecha');
      return false;
    }
   /* 
   if (fecha_desde.substring(6) != document.form_listado.anno.value ||
        fecha_hasta.substring(6) != document.form_listado.anno.value) {
      alert ('La fecha debe estar dentro del a�o seleccionado');
      return false;
    }*/
    AbrirVentana(document.form_listado,"gim_generar.jsp");
  }
// End -->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style></head>

<body>
<form name="form_listado" method="post">
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="30" class="Estilo_Verde">ESTAD&Iacute;STICA</td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td width="10%" class="Estilo_Gris">Tipo</td>
    <td width="90%" bgcolor="#FFFFFF">
     <select name="tipo_listado" class="Option">
        <option value="1" selected>Porcentaje de Uso</option>
        <option value="2">Promedio por Bloque</option>
        <option value="3">Promedio por Semana</option>
    </select>
     <input type="checkbox" name="version" value="1">
     <span class="Input01">Versi&oacute;n detallada </span></td>
  </tr>
  <tr>
    <td width="10%" class="Estilo_Gris">Sala</td>
    <td width="90%"><select name="sala" class="Option" id="sala">
        <option value="0">-- Seleccione --</option>
        <option value="10335" selected>M&aacute;quinas</option>
        <option value="10336">Multiuso</option>
        <option value="3">Todos</option>
    </select></td>
    </tr>
  <tr>
    <td height="24" class="Estilo_Gris">Semestre</td>
    <td><select name="anno" class="Option" id="anno" >
      <%for(int i = 2006;i<=a�o;i++){%>
      <option value="<%=i%>" <%=(i==anno)?"selected":""%>><%=i%></option>
      <%}%>
    </select>
-
<select name="semestre" class="Option" id="semestre" >
  <option value="1" <%=(semestre==1)?"selected":""%>>1</option>
  <option value="2" <%=(semestre==2)?"selected":""%>>2</option>
  <option value="3" <%=(semestre==3)?"selected":""%>>3</option>
</select></td>
    </tr>
  <tr>
    <td height="24" valign="bottom" class="Estilo_Verde">Per&iacute;odo</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="Estilo_Gris">Desde</td>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="14%"><input id="fecha_desde" name="fecha_desde" type="text" class="Input01" value="<%=fecha_actual%>" size="10" maxlength="14"></td>
        <td width="86%"><a href="javascript:void(0)" onclick="displayCalendar(document.form_listado.fecha_desde,'dd/mm/yyyy',this)"> <img src="imagen/gim_calendario_verde.gif" alt="CALENDARIO" width="16" height="16" border="0"></a></td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td class="Estilo_Gris">Hasta</td>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="14%"><input id="fecha_hasta" name="fecha_hasta" type="text" class="Input01" value="<%=fecha_actual%>" size="10" maxlength="14"></td>
        <td width="86%"><a href="javascript:void(0)" onclick="displayCalendar(document.form_listado.fecha_hasta,'dd/mm/yyyy',this)"> <img src="imagen/gim_calendario_verde.gif" border="0" alt="CALENDARIO" width="16" height="16"></a></td>
      </tr>
    </table></td>
    </tr>
</table>
<br>
<table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseLEER.gif">
  <tr onClick="Generar()" onMouseOver="BotonOver(this,'Generar')" onMouseOut="BotonOut(this)">
    <td width="27">&nbsp;</td>
    <td width="64" class="Estilo_Azulino">Generar</td>
  </tr>
</table>
<input type="hidden" name="tipo" value="2">
</form>
</body>
</html>
