<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 10/08/2007
   // �ltima Actualizaci�n :
   // Registro listado.    Se listan los alumnos inscritos en el paralelo y per�odo
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>


<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int   a�o =  request.getParameter("anno")!=null &&
                            !request.getParameter("anno").equals("null") &&
                            !request.getParameter("anno").equals("NULL") &&
                            !request.getParameter("anno").equals("")  &&
                            !request.getParameter("anno").equals(" ")
                            ?Integer.parseInt(request.getParameter("anno")+""):0;
  int   semestre =  request.getParameter("semestre")!=null &&
                          !request.getParameter("semestre").equals("null") &&
                          !request.getParameter("semestre").equals("NULL") &&
                          !request.getParameter("semestre").equals("")  &&
                          !request.getParameter("semestre").equals(" ")
                          ?Integer.parseInt(request.getParameter("semestre")+""):0;
  int   cod_asign =  request.getParameter("cod_asign")!=null &&
                        !request.getParameter("cod_asign").equals("null") &&
                        !request.getParameter("cod_asign").equals("NULL") &&
                        !request.getParameter("cod_asign").equals("")  &&
                        !request.getParameter("cod_asign").equals(" ")
                        ?Integer.parseInt(request.getParameter("cod_asign")+""):0;
  int   paralelo =  request.getParameter("paralelo")!=null &&
                       !request.getParameter("paralelo").equals("null") &&
                       !request.getParameter("paralelo").equals("NULL") &&
                       !request.getParameter("paralelo").equals("")  &&
                       !request.getParameter("paralelo").equals(" ")
                        ?Integer.parseInt(request.getParameter("paralelo")+""):0;
  java.util.Vector vec = new java.util.Vector();
  String titulo = "Lista de alumnos de la asignatura ";
  if (a�o > 0 && semestre > 0 ) {
    general.getAbrirConexion();
    general.setGenerarConexionHorario();
    vec = general.getListaCurso(a�o, semestre, cod_asign, paralelo);
    general.getCerrarConexion();
    if(vec.size() > 0)
      titulo +="<br>" + ((Vector) vec.get(0)).get(5)+ ", Paralelo "+ paralelo;
  }

java.util.Calendar f = new java.util.GregorianCalendar();
int dia = f.get(java.util.Calendar.DAY_OF_MONTH);
int mes = f.get(java.util.Calendar.MONTH) + 1;
int anno = f.get(java.util.Calendar.YEAR);
int hora = f.get(java.util.Calendar.HOUR);
int minuto = f.get(java.util.Calendar.MINUTE);
%>
<html>
<head>
<title>Gimnasio - Lista de Curso</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #000000}
-->
</style>
</head>
<body>

<table width="100%"  border="0" cellspacing="0" cellpadding="1">
<%if (vec != null && vec.size()>0){%>
  <tr>
    <td height="20" ><table width="100%"  border="0">
      <tr>
        <td width="20%" height="50" class="letra8" >Gimnasio III<br>
          USM </td>
        <td width="65%" class="letra10" align="center"><%=titulo%><span class="letra8"><br>
          </span></td>
        <td width="15%" >&nbsp;
          <div align="right"><span class="letra8"><%=dia%>/<%=mes%>/<%=anno%><br>
            <%=hora%>:<%=minuto%></span></div></td>
      </tr>
    </table>      </td>
  </tr>
  <tr>
    <td width="100%" height="20" align="center" valign="top" class="letra8">&nbsp;</td>
  </tr>
  <tr>
    <td height="20" align="center" valign="top" class="letra10">&nbsp;</td>
  </tr>
</table>
<table width="100%" height="49%"  border="1" cellpadding="2" cellspacing="1">
  <tr bgcolor="#FFFFFF">
    <td class="Estilo_GrisBold">N&deg;</td>
    <td class="Estilo_GrisBold">Paterno</td>
    <td class="Estilo_GrisBold">Materno</td>
    <td class="Estilo_GrisBold">Nombres</td>
    <td  class="Estilo_GrisBold">RUT</td>
  </tr>
  <%for (int i=0;vec != null && i < vec.size(); i++){%>
  <tr bgcolor="#FFFFFF">
    <td class="Estilo_Azul"><%=i+1%></td>
    <td class="Estilo_Azul"><%=((Vector) vec.get(i)).get(2)+ ""%></td>
    <td class="Estilo_Azul"><%=((Vector) vec.get(i)).get(3)+ ""%></td>
    <td class="Estilo_Azul"><%=((Vector) vec.get(i)).get(4)+ ""%></td>
    <td width="14%" class="Estilo_Azul"><%=((Vector) vec.get(i)).get(0)+ ""%>-<%=((Vector) vec.get(i)).get(1)+ ""%></td>
  </tr>
   <%}%>
   <%}%>

</table>
<%if (vec != null && vec.size()>0){%>
<div align="center"><font class="barra" color="#000000"><br>
  <br>
  &copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a</font>
</div><%}%>
</body>
</html>
