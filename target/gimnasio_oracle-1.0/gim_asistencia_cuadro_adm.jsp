<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 18/07/2007
   // �ltima Actualizaci�n :
   // Asistencia. Mantenedor de asistencia.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
   int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");

  int   sala =  request.getParameter("sala")!=null &&
                  !request.getParameter("sala").equals("null") &&
                  !request.getParameter("sala").equals("NULL") &&
                  !request.getParameter("sala").equals("")  &&
                  !request.getParameter("sala").equals(" ")
               ?Integer.parseInt(request.getParameter("sala")+""):0;
  int bloque1              = request.getParameter("bloque1")!=null &&
                            !request.getParameter("bloque1").equals("null") &&
                            !request.getParameter("bloque1").equals("NULL") &&
                            !request.getParameter("bloque1").equals("")  &&
                            !request.getParameter("bloque1").equals(" ")?
                            Integer.parseInt(request.getParameter("bloque1")+""):0;
  int bloque2              = request.getParameter("bloque2")!=null &&
                          !request.getParameter("bloque2").equals("null") &&
                          !request.getParameter("bloque2").equals("NULL") &&
                          !request.getParameter("bloque2").equals("")  &&
                          !request.getParameter("bloque2").equals(" ")?
                            Integer.parseInt(request.getParameter("bloque2")+""):0;
   String fecha            = request.getParameter("fecha_inicio")!=null &&
                            !request.getParameter("fecha_inicio").equals("null") &&
                            !request.getParameter("fecha_inicio").equals("NULL") &&
                            !request.getParameter("fecha_inicio").equals("")  &&
                            !request.getParameter("fecha_inicio").equals(" ")?
                            request.getParameter("fecha_inicio")+"":"";
   int dia              = request.getParameter("dia")!=null &&
                              !request.getParameter("dia").equals("null") &&
                              !request.getParameter("dia").equals("NULL") &&
                              !request.getParameter("dia").equals("")  &&
                              !request.getParameter("dia").equals(" ")?
                              Integer.parseInt(request.getParameter("dia")+""):0;
   int codAsign  = request.getParameter("cod_asign") != null?Integer.parseInt(request.getParameter("cod_asign")+""):0;                              
   Vector vec_periodo = new Vector();
   Vector vec_bloque = new Vector();

   String fecha_inicio = "";
   String fecha_termino = "";


   String hora_inicio = "";
   String hora_termino = "";
   String texto_dia = "";
   String periodo = "";
   String a = "";
   String m = "";
   String dd = "";
   String f = "";


   int a�o = 0;
   int semestre = 0;
   int cupo = 0;
   int inscritos = 0;
   int alumno = 0;
   int tutoria = 0;
   int rama = 0;
   int funcionario = 0;
   int espera = 0;
   int total = 0;



   if(dia > 0 && sala > 0 && bloque1 > 0 && bloque2 > 0 && !fecha.trim().equals("")){

   switch (dia){
     case 1: texto_dia = "Lunes";
       break;
     case 2: texto_dia = "Martes";
       break;
     case 3: texto_dia = "Mi�rcoles";
       break;
     case 4: texto_dia = "Jueves";
       break;
     case 5: texto_dia = "Viernes";
       break;
     case 6: texto_dia = "S�bado";
       break;
   }
   m = (Integer.parseInt(fecha.substring(3,5)) < 10)?
      ("0"+Integer.parseInt(fecha.substring(3,5))):
      (""+Integer.parseInt(fecha.substring(3,5)));
  dd = (Integer.parseInt(fecha.substring(0,2)) < 10)?
       ("0"+Integer.parseInt(fecha.substring(0,2))):
       (""+Integer.parseInt(fecha.substring(0,2)));
  a  = (Integer.parseInt(fecha.substring(6,10)) < 10)?
       ("0"+Integer.parseInt(fecha.substring(6,10))):
       (""+Integer.parseInt(fecha.substring(6,10)));
  f = a + m + dd;


   general.getAbrirConexion();
   general.setGenerarConexionReserva();
   general.setGenerarConexionHorario();
   vec_periodo = general.getPeriodoGimnasioSala(sala, f);

   periodo = (vec_periodo.size()> 0)?vec_periodo.get(2)+"":"";
   a�o = (vec_periodo.size()> 0)?Integer.parseInt(vec_periodo.get(3)+""):0;
   semestre = (vec_periodo.size()> 0)?Integer.parseInt(vec_periodo.get(4)+""):0;
   fecha_inicio = (vec_periodo.size()> 0)?vec_periodo.get(0)+"":"";
   fecha_termino = (vec_periodo.size()> 0)?vec_periodo.get(1)+"":"";

   //System.out.println("CuadroAdm codAsign: "+codAsign);
   cupo = general.getCupoGimnasio(a�o, semestre, dia, bloque1, bloque2, sala,codAsign);
  
   inscritos = general.getContInscritosGimnasio(a�o, semestre, dia, f, periodo,
                                                bloque1, bloque2, sala);
   alumno = general.getContAsistenciaGimnasio(a�o, semestre, dia, periodo, bloque1,
                                              bloque2, 1, f, sala);
   tutoria = general.getContAsistenciaGimnasio(a�o, semestre, dia, periodo, bloque1,
                                              bloque2, 2, f, sala);
   rama = general.getContAsistenciaGimnasio(a�o, semestre, dia, periodo, bloque1,
                                              bloque2, 3, f, sala);
   funcionario = general.getContAsistenciaGimnasio(a�o, semestre, dia, periodo, bloque1,
                                              bloque2, 4, f, sala);
   espera = general.getContAsistenciaGimnasio(a�o, semestre, dia, periodo, bloque1,
                                              bloque2, 5, f, sala);
   general.getCerrarConexion();



   hora_inicio = (vec_bloque.size()> 0)?vec_bloque.get(0)+"":"";
   hora_termino = (vec_bloque.size()> 0)?vec_bloque.get(1)+"":"";
   total = alumno + tutoria + rama + funcionario;

   }
 %>

<html>
<head>
<title>Gimnasio - Cuadro de Asistencia</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-Estilos -->
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
        margin-left: 0px;
        margin-top: 0px;
}
-->
</style></head>

<body >
<% if(dia > 0 && sala > 0 && bloque1 > 0 && bloque2 > 0 && !fecha.trim().equals("")){%>
      <table width="100%"  border="0" cellspacing="0" cellpadding="1">
        <tr>
          <td bgcolor="#CCCCCC"><table width="100%" height="100%"  border="0" cellpadding="1" cellspacing="1">
              <tr align="center" valign="top" bgcolor="#DBDBDB" >
                <td colspan="8" class="Estilo_Azul">Periodo <br>
                  <%=fecha_inicio%> al <%=fecha_termino%><br>
                <span class="Estilo_VerdeOsc"> <%=texto_dia%></span></td>
              </tr>
              <tr valign="bottom" bgcolor="#FFFFFF">
                <td colspan="2" align="center" background="imagen/boton_baseFONDO03.gif" bgcolor="#EFFCF3" class="Estilo_Gris letra7">Bloque</td>
                <td colspan="6" align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0" class="Estilo_Gris letra7">Asistencia</td>
              </tr>
              <tr bgcolor="#FFFFFF">
                <td width="14%" align="center" background="imagen/boton_baseFONDO03.gif" bgcolor="#EFFCF3" class="Estilo_Gris letra7"><acronym title="Cupo">Cupo</acronym></td>
                <td width="14%" align="center" background="imagen/boton_baseFONDO03.gif" bgcolor="#EFFCF3" class="Estilo_Gris letra7"><acronym title="Inscritos">Insc.</acronym></td>
                <td width="12%" align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0" class="Estilo_Gris letra7"><acronym title="Alumnos asistentes">Alu.</acronym></td>
                <td width="12%" align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0" class="Estilo_Gris letra7"><acronym title="Alumnos de Tutor�a asistentes">Tut.</acronym></td>
                <td width="12%" align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0" class="Estilo_Gris letra7"><acronym title="Alumnos de Rama asistentes">Rama</acronym></td>
                <td width="12%" align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0" class="Estilo_Gris letra7"><acronym title="Funcionarios asistentes">Func.</acronym></td>
                <td width="12%" align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0" class="Estilo_Gris letra7"><acronym title="Alumnos en Espera">Esp.</acronym></td>
                <td width="12%" align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0" class="Estilo_Gris letra7"><acronym title="Asistencia Total">Total</acronym></td>
              </tr>
              <tr bgcolor="#FFFFFF" class="Estilo_Azulino">
                <td align="center" background="imagen/boton_baseFONDO03.gif" bgcolor="#EFFCF3"><%=cupo%></td>
                <td align="center" background="imagen/boton_baseFONDO03.gif" bgcolor="#EFFCF3"><%=inscritos%></td>
                <td align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0"><%=alumno%></td>
                <td align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0"><%=tutoria%></td>
                <td align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0"><%=rama%></td>
                <td align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0"><%=funcionario%></td>
                <td align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0"><%=espera%></td>
                <td align="center" background="imagen/boton_baseFONDO02.gif" bgcolor="#E0EBE0"><%=total%></td>
              </tr>
          </table></td>
        </tr>
      </table>

      <%}%>
</body>
</html>
