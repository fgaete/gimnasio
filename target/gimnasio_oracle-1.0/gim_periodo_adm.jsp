<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 20/06/2007
   // �ltima Actualizaci�n :
   // Per�odo.Para la sala de M�quinas y pesas (10335) se registra per�odo
   // de vigencia, de inscripci�n, se cierra per�odo y se cambia de semestre
   // para sal de aer�bica (10336) se registra per�odo de vigencia
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");

  int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  int cod_perfil = session.getAttribute("cod_perfil") != null?
                   Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
  int   accion =  request.getParameter("accion")!=null &&
                  !request.getParameter("accion").equals("null") &&
                  !request.getParameter("accion").equals("NULL") &&
                  !request.getParameter("accion").equals("")  &&
                  !request.getParameter("accion").equals(" ")
               ?Integer.parseInt(request.getParameter("accion")+""):0;
  int   anno =  request.getParameter("anno")!=null &&
                !request.getParameter("anno").equals("null") &&
                !request.getParameter("anno").equals("NULL") &&
                !request.getParameter("anno").equals("")  &&
                !request.getParameter("anno").equals(" ")
             ?Integer.parseInt(request.getParameter("anno")+""):0;
  int   semestre =  request.getParameter("semestre")!=null &&
                    !request.getParameter("semestre").equals("null") &&
                    !request.getParameter("semestre").equals("NULL") &&
                    !request.getParameter("semestre").equals("")  &&
                    !request.getParameter("semestre").equals(" ")
                 ?Integer.parseInt(request.getParameter("semestre")+""):0;
  int   sala =  request.getParameter("sala")!=null &&
                  !request.getParameter("sala").equals("null") &&
                  !request.getParameter("sala").equals("NULL") &&
                  !request.getParameter("sala").equals("")  &&
                  !request.getParameter("sala").equals(" ")
                 ?Integer.parseInt(request.getParameter("sala")+""):0;
  int   anulacion =  request.getParameter("anulacion")!=null &&
                !request.getParameter("anulacion").equals("null") &&
                !request.getParameter("anulacion").equals("NULL") &&
                !request.getParameter("anulacion").equals("")  &&
                !request.getParameter("anulacion").equals(" ")
                 ?Integer.parseInt(request.getParameter("anulacion")+""):0;
  String fecha_inicio = request.getParameter("fecha_inicio")!= null?
                     request.getParameter("fecha_inicio") :"";
  String fecha_apertura = request.getParameter("fecha_apertura")!= null?
                    request.getParameter("fecha_apertura") :"";
  String fecha_termino = request.getParameter("fecha_termino")!= null?
                    request.getParameter("fecha_termino") :"";
  String fecha_cierre = request.getParameter("fecha_cierre")!= null?
                    request.getParameter("fecha_cierre") :"";
  String fecha_ini = request.getParameter("fecha_ini")!= null?
                    request.getParameter("fecha_ini") :"";
  String fecha_fin = request.getParameter("fecha_fin")!= null?
                    request.getParameter("fecha_fin") :"";
  String id = request.getParameter("id")!= null?
                    request.getParameter("id") :"0";
  String fecha_mod = request.getParameter("fecha")!= null?
                    request.getParameter("fecha") :"";
   Vector vec_periodo = new Vector();

if (rut_usuario == 0)  response.sendRedirect("index.jsp");
boolean soloconsulta = false;
boolean listar       = false;
Vector vec_per_anulado = new Vector();
 if (cod_perfil==28) soloconsulta = true;
 Vector vec = new Vector();
 Vector vec_premiadoSancionado = new Vector();
 Vector vec_listado = new Vector();
 String mensaje = "";
 String onload = "";
 int error = 0;
 general.getAbrirConexion();
 general.setGenerarConexionReserva();

//System.out.println(accion+" fecha_inicio "+fecha_inicio);
 if (accion > 0){
   vec = new Vector();
   vec.addElement(anno+"");
   vec.addElement(semestre+"");
   vec.addElement(sala+"");
   vec.addElement(fecha_inicio);
   vec.addElement(fecha_termino);
   if (sala==10335)
     vec.addElement(fecha_apertura);
   else
     vec.addElement(fecha_inicio); // para sala aer�bica se repite fecha
   if (sala==10335)
     vec.addElement(fecha_cierre);
   else
     vec.addElement(fecha_termino); // para sala aer�bica se repite fecha
   vec.addElement(rut_usuario+"");
   vec.addElement(id); // pergim


 switch (accion){
       case 1: { // agregar
         vec.addElement("1"); // agregar
         error = general.setPeriodoGimnasio(vec);
         if(error == 0) mensaje = "Se registr� satisfactoriamente";
         else if (error == -1) mensaje = "No puede registrar, pues existe per�odo que no ha sido cerrado";
         else if (error == -3) mensaje = "No puede registrar, pues la fecha de t�rmino de la vigencia debe ser mayor a la de inicio";
         else if (error == -4) mensaje = "No puede registrar, pues la fecha de inicio de la vigencia debe ser mayor a la registrada en el �ltimo per�odo";
         else mensaje = "Problemas al registrar ";


         error = -1;
         break;
       }
     case 2: { // Cerrar per�odo
       vec.addElement("2"); // cerrar
       error = general.setPeriodoGimnasio(vec);
       if(error == 0) mensaje = "Se registr� satisfactoriamente";
       else mensaje = "Problemas al registrar ";
       error = -1;
       break;
        }
       case 3: { // eliminar periodo
         vec.addElement("3"); // eliminar
         error = general.setPeriodoGimnasio(vec);
         if(error == 0) mensaje = "Se Elimin� satisfactoriamente";
         else if (error == -8) mensaje = "No puede eliminar, pues existen reservas registradas en el per�odo";
         else  if (error == -9) mensaje = "No puede eliminar, pues hay registro de clase en suspensi�n, anulaci�n o cerrada en el per�odo";
         else mensaje = "Problemas al eliminar el per�odo";
         error = -1;
         break;
       }
     case 4: { // agregar suspension nueva
       vec = new Vector();
       vec.addElement(id);
       vec.addElement(fecha_ini);
       vec.addElement(fecha_fin);
       vec.addElement(anulacion+"");
       vec.addElement(rut_usuario+"");
       error = general.setEstadoClasePeriodo(vec);
       String tt = (anulacion == 5)?"Suspensi�n":"Anulaci�n";
       if(error == 0) mensaje = "Se agreg� satisfactoriamente la "+ tt;
       else  mensaje = "Problemas al agregar la "+tt;
       error = -1;
       break;
     }
   case 5: { // modificar suspension
     vec = new Vector();
     vec.addElement(id);
     vec.addElement(fecha_mod);
     vec.addElement(anulacion+"");
     vec.addElement(rut_usuario+"");
     error = general.setUpdEstadoClasePeriodo(vec);
     String tt = (anulacion == 5)?"Suspensi�n":"Anulaci�n";
     if(error == 0) mensaje = "Se modific� satisfactoriamente la "+ tt;
     else  mensaje = "Problemas al modificar la "+tt;
     error = -1;
     break;
   }
 case 6: { // eliminar suspension
   vec = new Vector();
   vec.addElement(id);
   vec.addElement(fecha_mod);
   error = general.setDltEstadoClasePeriodo(vec);
   String tt = (anulacion == 5)?"Suspensi�n":"Anulaci�n";
   if(error == 0) mensaje = "Se elimin� satisfactoriamente la "+ tt;
   else  mensaje = "Problemas al eliminar la "+tt;
   error = -1;
   break;
 }
 case 7: { // generar sancionados y premiados
   vec = new Vector();
   vec.addElement(id);
   vec.addElement(rut_usuario+"");
   vec_listado = general.setGeneraPremiadoSancionado(vec);

   if (vec_listado.size()> 0) {
     vec_premiadoSancionado = general.getPremiadoSancionado(id);
     if (vec_premiadoSancionado.size()>0)mensaje = "Se gener� listado de premiados y sancionados satisfactoriamente.";
     else mensaje = "Al generar premiados y sancionados no hay alumnos asociados al listado.";
     if ((((Vector) vec_listado.get(0)).get(0)+ "").equals("0") ||
         (((Vector) vec_listado.get(0)).get(0)+ "").equals("-1") ||
         (((Vector) vec_listado.get(0)).get(0)+ "").equals("-2") ||
         (((Vector) vec_listado.get(0)).get(0)+ "").equals("-3"))
       if ((((Vector) vec_listado.get(0)).get(0)+ "").equals("0"))
         mensaje += "Quedaron todos los alumnos premiados con reserva para el siguiente per�odo, sino existe no se generaron las reservas.";
     else
       mensaje = "Error en generar premiados y sancionados";
     else{
       mensaje += " El siguiente listado son los Premiados que no se generaron sus reservas autom�ticas para el siguiente per�odo, por ya tener registradas reservas en ese per�odo.  ";
       session.setAttribute("vec_listado",vec_listado);
       listar = true;
     }
   } else mensaje = "Problemas al generar listado de premiados y sancionados del per�odo.";
    error = -1;
  break;
 }
 case 8: { // ELIMINAR sancionados y premiados
   error = general.setEliminaPremiadoSancionado(id);
   if (error == 0) mensaje = "Se elimin� listado de premiados y sancionados satisfactoriamente.";
  else mensaje = "Problemas al eliminar listado de premiados y sancionados del per�odo.";

   error = -1;
  break;
 }
 }
 }
 if (anno == 0 || semestre == 0){
   general.setGenerarConexionHorario();
   general.getPeriodoSemestreActual(1,1);
   anno = general.getA�oActual();
   semestre = general.getSemestreActual();
 }

 if (anno>0 && semestre > 0)
   vec_periodo = general.getPeriodoGimnasio(anno, semestre, sala);

 if (accion == 4 || accion == 5 || accion == 6 || accion == 7 || accion == 8){
   int indice = -1;
   for (int i=0; i < vec_periodo.size();i++){
     if ((((Vector) vec_periodo.get(i)).get(5)+"").trim().equals(id.trim()))
       indice = i;
   }
   if (indice >= 0) {
     if (accion == 7 || accion == 8)
       onload = "escondeBloque('BLOQUE_premiados"+indice+"', 2);";
     else  onload = "escondeBloque('BLOQUE_datos"+indice+"', 2);";
   }
 }

 java.util.Calendar f = new java.util.GregorianCalendar();
 int a�o = f.get(java.util.Calendar.YEAR);
 int dia = f.get(java.util.Calendar.DAY_OF_MONTH);
 int mes = f.get(java.util.Calendar.MONTH) + 1;
 String s_dia = (dia<10)?"0"+dia:dia+"";
 String s_mes = (mes<10)?"0"+mes:mes+"";
 String fecha = s_dia +"/"+s_mes + "/"+a�o;
 String inicio = "";
 String termino = "";
 String apertura = "";
 String cierre = "";
 String cerrado = "";
 int fecha_actual = Integer.parseInt(a�o+s_mes+s_dia);
 int fecha_eval = 0;
 String disable = "";
   if(soloconsulta) disable = "disabled";
 %>

<html>
<head>
<title>Gimnasio - Periodo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-Calendario -->
<link type="text/css" rel="stylesheet" href="js/dhtml_calendar/dhtml_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="js/dhtml_calendar/dhtml_calendar.js?random=20060118"></script>

<!-Estilos -->
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script src="js/calcula_fecha.js"></script>
<script src="js/link.js"></script>
<script src="js/layer.js"></script>
<script src="js/centrar.js"></script>
<script language = "javascript" >
 function Mensaje(){
<%if(error<0){%>
<% if (listar) {%>
  var respuesta = confirm('<%=mensaje%>' + '\n�Desea ver listado generado?');
  if (respuesta == true)
  AbrirVentana(document.form_listado,"gim_genera_list.jsp",0);
   <%} else {%>alert ('<%=mensaje%>');<%}}%>
}
function AbrirVentana(f,a, tipo) {

var alto  = 600;
var ancho = 700;
f.target = "ventana";
f.action = a;
f.tipo.value = tipo;
var par = 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=yes';
var w = Centrar03(f.target,alto,ancho,par);
w.focus();
f.submit();
}
<%if(!soloconsulta){%>

function Grabar(f,accion){
  if (f.sala.value == 0){
    alert ('Debe seleccionar una sala');
    f.sala.focus();
    return false;
  }

    if (f.fecha_inicio.value== '' ){
      alert ('Debe Ingresar fecha de inicio de vigencia');
      f.fecha_inicio.focus();
      return false;
    }
    if (f.fecha_termino.value== '' ){
      alert ('Debe Ingresar fecha de t�rmino de vigencia');
      f.fecha_termino.focus();
      return false;
    }
    fecha_termino = recalcF1(f.fecha_inicio.value,19);
//    if (f.fecha_termino.value != fecha_termino ){
//      alert ('El per�odo de vigencia es de 3 semanas, por lo tanto la fecha de t�rmino de vigencia debe ser: '+fecha_termino);
//      f.fecha_termino.focus();
//      return false;
//    }

    if (f.sala.value == 10335){
     if (f.fecha_apertura.value == '' ){
      alert ('Debe Ingresar fecha de inicio de inscripci�n');
      f.fecha_apertura.focus();
      return false;
    }
    if (f.fecha_cierre.value == ''){
      alert ('Debe Ingresar fecha de t�rmino de inscripci�n');
      f.fecha_cierre.focus();
      return false;
    }
    fecha_inicio = f.fecha_inicio.value.substring(6,10) +'' +f.fecha_inicio.value.substring(3,5) +''+f.fecha_inicio.value.substring(0,2);
    fecha_termino = f.fecha_termino.value.substring(6,10) +'' +f.fecha_termino.value.substring(3,5) +''+f.fecha_termino.value.substring(0,2);
    fecha_apertura = f.fecha_apertura.value.substring(6,10) +'' +f.fecha_apertura.value.substring(3,5) +''+f.fecha_apertura.value.substring(0,2);
    fecha_cierre = f.fecha_cierre.value.substring(6,10) +'' +f.fecha_cierre.value.substring(3,5) +''+f.fecha_cierre.value.substring(0,2);


    if (fecha_apertura < fecha_inicio || fecha_apertura > fecha_termino ||
        fecha_cierre < fecha_inicio || fecha_cierre > fecha_termino ){
      alert ('El per�odo de inscripci�n debe estar dentro del per�odo de vigencia');
      f.fecha_apertura.focus();
      return false;
      }
 fecha_cierre = recalcF1(f.fecha_apertura.value,5);
  if (f.fecha_cierre.value != fecha_cierre ){
    alert ('El per�odo de inscripci�n es de 1 semana, por lo tanto la fecha de t�rmino de inscripci�n debe ser '+fecha_cierre);
    f.fecha_cierre.focus();
    return false;
    }
  }


  f.accion.value = accion;
  f.submit();
}

function Eliminar(f, accion){
  var pregunta = "Ud. va a ELIMINAR el per�odo definitivamente.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  f.accion.value = accion;
  if (respuesta == true) f.submit();
}
function Cerrar(f, accion){
  var pregunta = "Ud. va a Cerrar el per�odo definitivamente. " ;

  pregunta += "\n � Desea continuar ?";
  var respuesta = confirm(pregunta);
  f.accion.value = accion;
  if (respuesta == true) f.submit();
}

function Validar(f, apertura, cierre){
   if (f.fecha_ini.value == "" ){
   alert ('Debe ingresar la fecha de inicio');
   f.fecha_ini.focus();
   return false;
    }
    if (f.fecha_fin.value == "" ){
      alert ('Debe ingresar la fecha de t�rmino');
      f.fecha_fin.focus();
      return false;
    }
    if (f.fecha_ini.value == "" ){
      alert ('Debe ingresar la fecha de inicio');
      f.fecha_ini.focus();
      return false;
    }
    fecha_inicio = apertura.substring(6,10) +'' +apertura.substring(3,5) +''+apertura.substring(0,2);
    fecha_termino = cierre.substring(6,10) +'' +cierre.substring(3,5) +''+cierre.substring(0,2);
    fecha_ini = f.fecha_ini.value.substring(6,10) +'' +f.fecha_ini.value.substring(3,5) +''+f.fecha_ini.value.substring(0,2);
    fecha_fin = f.fecha_fin.value.substring(6,10) +'' +f.fecha_fin.value.substring(3,5) +''+f.fecha_fin.value.substring(0,2);
    if (fecha_ini <  fecha_inicio || fecha_ini > fecha_termino ||
        fecha_fin < fecha_inicio || fecha_fin > fecha_termino){
       alert ('Debe ingresar una fecha dentro del per�odo');
       f.fecha_ini.focus();
       return false;
    }
    f.submit();
}
function Registrar(f, acc){
  f.accion.value = acc;
  f.submit();
}
<%}%>
  function Deshacer(bloque){

//DisableEnableLinks(false);
  escondeBloque(bloque,1);
}
<%if (sala == 10335) {%>
function Generar(f, accion){
  texto="";
  if (accion == 7) texto = "Generar";
  if (accion == 8) texto = "Eliminar";
  var pregunta = "Ud. va a " + texto + " los Sancionados y Premiados del Per�odo. " ;

  pregunta += "\n � Desea continuar ?";
  var respuesta = confirm(pregunta);
  f.accion.value = accion;
  if (respuesta == true) f.submit();
}
<%}%>
 </script>
<style type="text/css">
<!--
body {
        margin-left: 0px;
        margin-top: 0px;
}
-->
</style></head>

<body onload = "<%=onload%>Mensaje();">

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="37" colspan="5" class="Estilo_Verde">PERIODO</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <form name="fingresar" method="post" action="gim_periodo_adm.jsp">
   <input type="hidden" name="accion" value="">
   <td width="14%" height="20" bgcolor="#FFFFFF" class="Estilo_Gris">Sala</td>
    <td bgcolor="#FFFFFF">
      <select name="sala" class="Option" id="sala" onchange="document.fingresar.submit();">
        <option value="0" selected>-- Seleccione --</option>
        <option value="10335" <%=(sala == 10335)?"selected":""%>>M&aacute;quinas</option>
        <option value="10336" <%=(sala == 10336)?"selected":""%>>Multiuso</option>
      </select>
   </td>
    <td bgcolor="#FFFFFF"><span class="Estilo_Gris">Per&iacute;odo</span></td>
    <td bgcolor="#FFFFFF"><select name="anno" class="Option" id="anno" onchange="document.fingresar.submit();">
      <%for(int i = 2006;i<=a�o;i++){%>
       <option value="<%=i%>" <%=(i==anno)?"selected":""%>><%=i%></option>
      <%}%>
      </select>
      -
      <select name="semestre" class="Option" id="semestre" onchange="document.fingresar.submit();">
        <option value="1" <%=(semestre==1)?"selected":""%>>1</option>
        <option value="2" <%=(semestre==2)?"selected":""%>>2</option>
        <option value="3" <%=(semestre==3)?"selected":""%>>3</option>
        </select></td>
    <td bgcolor="#FFFFFF">&nbsp;</td>

  <%if(!soloconsulta){%>
  <tr bgcolor="#FFFFFF">
    <td width="14%" bgcolor="#FFFFFF" class="Estilo_Gris">Vigencia</td>
    <td width="25%"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="50%"><input id="fecha_inicio" name="fecha_inicio" readonly type="text" class="Input01" onChange="" value="<%=fecha%>" size="10" maxlength="14"></td>
        <td valign="bottom"><a href="javascript:void(0)" onclick="displayCalendar(document.fingresar.fecha_inicio,'dd/mm/yyyy',this)">
<img src="imagen/gim_calendario_verde.gif" border="0" alt="CALENDARIO" width="16" height="16"></a></td>
      </tr>
    </table></td>
    <%if (sala == 10335) {%>
    <td width="15%" class="Estilo_Gris">Inscripci&oacute;n</td>
    <td bgcolor="#FFFFFF"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="70%"><input id="fecha_apertura" name="fecha_apertura" type="text" class="Input01" readonly value="<%=fecha%>" onChange="" size="10" maxlength="14"></td>
        <td valign="bottom"><a href="javascript:void(0)" onclick="displayCalendar(document.fingresar.fecha_apertura,'dd/mm/yyyy',this)">
<img src="imagen/gim_calendario_verde.gif" border="0" alt="CALENDARIO" width="16" height="16"></a></td>
      </tr>
    </table></td>

    <td bgcolor="#FFFFFF">&nbsp;</td> <%}%>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td bgcolor="#FFFFFF" class="Estilo_Gris">hasta</td>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="50%"><input id="fecha_termino" name="fecha_termino" type="text" class="Input01" readonly onChange="" value="<%=fecha%>" size="10" maxlength="14"></td>
        <td valign="bottom"><a href="javascript:void(0)" onclick="displayCalendar(document.fingresar.fecha_termino,'dd/mm/yyyy',this)">
<img src="imagen/gim_calendario_verde.gif" border="0" alt="CALENDARIO" width="16" height="16"></a></td>
      </tr>
    </table></td>
    <%if (sala == 10335) {%>
    <td class="Estilo_Gris">hasta</td>
    <td width="18%" bgcolor="#FFFFFF"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="70%"><input id="fecha_cierre" name="fecha_cierre" type="text" readonly class="Input01" onChange="" value="<%=fecha%>" size="10" maxlength="14"></td>
        <td valign="bottom"><a href="javascript:void(0)" onclick="displayCalendar(document.fingresar.fecha_cierre,'dd/mm/yyyy',this)">
<img src="imagen/gim_calendario_verde.gif" border="0" alt="CALENDARIO" width="16" height="16"></a></td>
      </tr>
    </table></td><%}%>
    <td width="28%" bgcolor="#FFFFFF"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseOK.gif">
      <tr onClick="Grabar(document.fingresar,1);" onMouseOver="BotonOver(this,'Agregar')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino">Agregar</td>
      </tr>
    </table></td>
  </tr>
  <%}%>
</form>
</table>
<%if (vec_periodo.size()> 0) {%>
<br>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#999999">
<td  class="Estilo_Gris">
<table width="100%"  border="0" cellspacing="0" cellpadding="3">
  <tr bgcolor="#008040" class="Estilo_GrisOsc">
    <td  background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">N&deg;</span></td>
    <td background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Vigencia</span></td>
    <%if (sala == 10335) {%>
    <td background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Inscripci&oacute;n</span></td>
    <%}%>
    <td width="23%" align="center" background="imagen/boton_baseFONDO04.gif" class="Estilo_Blanco">Acci&oacute;n</td>


    </tr>
   <%for (int i=0; i < vec_periodo.size(); i++){
        inicio = ((Vector) vec_periodo.get(i)).get(0)+ "";
        termino = ((Vector) vec_periodo.get(i)).get(1)+ "";
        apertura = ((Vector) vec_periodo.get(i)).get(2)+ "";
        cierre = ((Vector) vec_periodo.get(i)).get(3)+"";
        cerrado = ((Vector) vec_periodo.get(i)).get(4)+ "";
        fecha_eval = Integer.parseInt(termino.substring(6,10)+termino.substring(3,5)+termino.substring(0,2));
        vec_premiadoSancionado = general.getPremiadoSancionado(((Vector) vec_periodo.get(i)).get(5)+"");
%>
  <tr bgcolor="#E8E8E8" onmouseover='this.style.background="#F4F4F4"' onmouseout='this.style.background="#E8E8E8"'>

    <td class="Estilo_Azul"><%=vec_periodo.size()-i%></td>
    <td class="Estilo_Azul"><%=inicio%> al <%=termino%> </td>
    <%if (sala == 10335) {%>
    <td class="Estilo_Azul"><%=(!apertura.trim().equals("null"))?apertura + " al "+cierre:""%></td>
    <%}%>
    <td >
      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <form name="form_grabar<%=i%>" method="post" action="gim_periodo_adm.jsp" target="_self">
     <input type="hidden" name="id" value="<%=((Vector) vec_periodo.get(i)).get(5)%>">
     <input type="hidden" name="accion" value="">
     <input type="hidden" name="sala" value="<%=sala%>">
     <input type="hidden" name="anno" value="<%=anno%>">
     <input type="hidden" name="semestre" value="<%=semestre%>">
     <input type="hidden" name="listado" value="5">
     <input type="hidden" name="tipo" value="">
        <tr >

          <td align="right" >
             <%if (sala == 10335) {%>
            <% if ((cerrado == null || cerrado.trim().equals("null") || cerrado.trim().equals("")) && fecha_actual>=fecha_eval ){%>
            <%if(!soloconsulta){%>
            <a HREF="#" id="accion<%=i%>" onClick="Cerrar(document.form_grabar<%=i%>,2);"><img src="imagen/gim_candado.gif" alt="CERRAR" width="16" height="16" border="0" align="absmiddle">&nbsp;</a>

            <%}}%>
            <a HREF="#" onClick="escondeBloque('BLOQUE_premiados<%=i%>', 2);"><img src="imagen/ico_premiados.gif" alt="PREMIADOS Y SANCIONADOS" width="20" height="22" border="0" align="absmiddle"></a>
              <%}%>
            <%if(!soloconsulta){%>
            <a HREF="#"  onClick="escondeBloque('BLOQUE_datos<%=i%>', 2);">&nbsp;<img src="imagen/ico_suspension.gif" alt="ANULACI�N Y/O SUSPENSI�N" width="22" height="22" border="0" align="absmiddle"></a>
            <a HREF="#"  onClick="Eliminar(document.form_grabar<%=i%>,3); ">&nbsp;<img src="imagen/gim_basurero.gif" alt="ELIMINAR" width="15" height="18" align="absmiddle" border="0"></a>
            <%}%></td>

          </tr></form>
      </table></td>

     </tr>

  <tr bgcolor="#E8E8E8" >
    <td colspan="4" >
        <DIV id="BLOQUE_datos<%=i%>" STYLE="display:none">
    <%
    vec_per_anulado = general.getPeriodoAnulado(((Vector) vec_periodo.get(i)).get(5)+"");
        if (vec_per_anulado.size() > 0) {%>
        <br>
<table width="77%"  border="0" align="center">
   <tr>
     <td>Fechas de periodo suspendidas o anuladas. </td>
     </tr>
</table>
<table width="77%"  border="0" align="center" class="Celda02">
<%
   for (int j=0;j < vec_per_anulado.size() ;j++){%>
   <form name="form_periodo<%=i%><%=j%>" method="post" action="gim_periodo_adm.jsp" target="_self">
   <input type="hidden" name="id" value="<%=((Vector) vec_periodo.get(i)).get(5)%>">
   <input type="hidden" name="accion" value="">
   <input type="hidden" name="sala" value="<%=sala%>">
   <input type="hidden" name="anno" value="<%=anno%>">
   <input type="hidden" name="semestre" value="<%=semestre%>">
   <tr>
      <td width="37%" ><%=((Vector) vec_per_anulado.get(j)).get(0)%><input id="fecha" name="fecha" type="hidden" value="<%=((Vector) vec_per_anulado.get(j)).get(0)%>">
      </td>
      <td width="46%" ><input name="anulacion" type="radio" value="4" <%=(Integer.parseInt(((Vector) vec_per_anulado.get(j)).get(1)+"")== 4)?"checked":""%> <%=disable%>>
Anulaci&oacute;n
<input name="anulacion" type="radio" value="5" <%=(Integer.parseInt(((Vector) vec_per_anulado.get(j)).get(1)+"")== 5)?"checked":""%> <%=disable%>>
Suspensi&oacute;n </td>
    <td width="17%" align="center" >
    <%if(!soloconsulta){%><a href="javascript:void(0)" onClick="Registrar(document.form_periodo<%=i%><%=j%>,5);">
    <img src="imagen/gim_grabar.gif" alt="GRABAR" width="16" height="16" border = "0" align="absmiddle"></a>
    <a HREF="javascript:void(0)" onClick="Registrar(document.form_periodo<%=i%><%=j%>,6); ">&nbsp;
   <img src="imagen/gim_basurero.gif" alt="ELIMINAR" width="15" height="18" border = "0" align="absmiddle"></a>
    <%}%></td>
  </tr></form>
  <%}%>
</table>
  <%}%><br>
      <form name="form_periodo_nuevo<%=i%>" method="post" action="gim_periodo_adm.jsp" target="_self">
      <input type="hidden" name="accion" value="4">
      <input type="hidden" name="id" value="<%=((Vector) vec_periodo.get(i)).get(5)%>">
      <input type="hidden" name="sala" value="<%=sala%>">
      <input type="hidden" name="anno" value="<%=anno%>">
     <input type="hidden" name="semestre" value="<%=semestre%>">
   <%if(!soloconsulta){%>
    <table width="77%"  border="0" align="center">
      <tr>
        <td colspan="3">Ingrese rango de fechas de anulaci&oacute;n o suspensi&oacute;n</td>
      </tr>
           </table><%}%>
    <%if(!soloconsulta){%>
    <table width="77%"  border="0" align="center" class="Celda02">
      <tr>
        <td width="13%">Inicio</td>
        <td width="70%"><input id="fecha_ini<%=i%>" name="fecha_ini" type="text" class="Input01" readonly onChange="" value="" size="10" maxlength="14">
           <a href="javascript:void(0)" onclick="displayCalendar(document.form_periodo_nuevo<%=i%>.fecha_ini,'dd/mm/yyyy',this)">
<img src="imagen/gim_calendario_verde.gif" border="0" alt="CALENDARIO" width="16" height="16"></a></td>
        <td width="17%">&nbsp;</td>
      </tr>
      <tr>
        <td>T&eacute;rmino</td>
        <td><input id="fecha_fin<%=i%>" name="fecha_fin" type="text" class="Input01" readonly value=""  onChange="" size="10" maxlength="14">
          <a href="javascript:void(0)" onclick="displayCalendar(document.form_periodo_nuevo<%=i%>.fecha_fin,'dd/mm/yyyy',this)">
<img src="imagen/gim_calendario_verde.gif" border="0" alt="CALENDARIO" width="16" height="16"></a>
          <input name="anulacion" type="radio" value="4" checked>
Anulaci&oacute;n
<input name="anulacion" type="radio" value="5">
Suspensi&oacute;n</td>
        <td align="center">
        <a HREF="javascript:void(0)"  onClick="Validar(document.form_periodo_nuevo<%=i%>, '<%=inicio%>', '<%=termino%>');" >
       <img src="imagen/gim_grabar.gif" alt="GRABAR" width="16" height="16" border = "0" align="absmiddle"></a><a href="javascript:void(0)"  onClick="Deshacer('BLOQUE_datos<%=i%>');" border = "0"><img src="imagen/gim_deshacer.gif" alt="DESHACER" width="16" height="16" border = "0" align="absmiddle"></a>
       <a HREF="javascript:void(0)"  onClick="Deshacer('BLOQUE_datos<%=i%>');" border = "0"></a></td>
      </tr>
    </table> <%}%>
       <%if(soloconsulta){%>
       <table width="77%"  border="0" align="center" >
        <div align="right"> <a HREF="javascript:void(0)"  onClick="Deshacer('BLOQUE_datos<%=i%>');" border = "0">
       Deshacer <img src="imagen/gim_deshacer.gif" width="16" height="16" border = "0" align="absmiddle"></a></div>
       </table><%}%>
        </form>
        </div>
        <%if (sala == 10335) {%>        <DIV id="BLOQUE_premiados<%=i%>"  STYLE="display:none">
        <table width="77%"  border="0" align="center">
      <tr>
        <td width="30%">Premiados y Sancionados del Per&iacute;odo<br>
        <font color="#990000">Antes de generar los premiados y sancionados aseg&uacute;rese que el <br /><b>nuevo periodo</b> se encuentre creado.<br>
        Ante cualquier problema, puede eliminar la lista y volver a generar.</font></td>
        </tr>
           </table>

        <table width="77%"  border="0" align="center" class="Celda02">
          <tr>
            <td width="83%" ><%if (vec_premiadoSancionado.size()> 0) {%>
              <%="<a HREF=\"#\" onClick=\"AbrirVentana(form_grabar"+i+",'gim_genera_list.jsp',6)\" TITLE=\"Ver Listado de Premiados\">Premiados</a>"%> y <%="<a HREF=\"#\" onClick=\"AbrirVentana(form_grabar"+i+",'gim_genera_list.jsp',5)\"  TITLE=\"Ver Listado de Sancionados\">Sancionados</a>"%>
              <%}  else {%>Generar<%}%>  </td>
            <td width="17%" align="center" ><%if (vec_premiadoSancionado.size()==0){%>  <%if(!soloconsulta){%>
              <a href="javascript:void(0)"  onClick="Generar(document.form_periodo_nuevo<%=i%>, 7);" > <img src="imagen/ico_generar.gif" alt="GENERAR PREMIADOS Y SANCIONADOS" width="19" height="19" border = "0" align="absmiddle"></a>
              <%}} else { %>   <%if(!soloconsulta){%>
              <a href="javascript:void(0)"  onClick="Generar(document.form_periodo_nuevo<%=i%>, 8);" ><img src="imagen/gim_basurero.gif" alt="ELIMINAR PREMIADOS Y SANCIONADOS" width="15" height="18" border = "0" align="absmiddle"></a>
              <%}}%>
              <a href="javascript:void(0)"  onClick="Deshacer('BLOQUE_premiados<%=i%>');" border = "0"><img src="imagen/gim_deshacer.gif" alt="DESHACER" width="16" height="16" border = "0" align="absmiddle"></a></td>
          </tr>
        </table>
        </div><%}%>

        </td>
  </tr>

<%}%>
</table>
</td>
</tr>
</table>
<%if(!soloconsulta){%>
<table width="100%"  border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td height="56" align="center"  valign="bottom"><%if (sala==10335){%><img src="imagen/gim_candado.gif" width="16" height="16"> <span class="Estilo_GrisOsc">Cerrar</span>&nbsp;&nbsp;&nbsp;<img src="imagen/ico_premiados.gif" alt="PREMIADOS Y SANCIONADOS" width="20" height="22" border="0" align="absmiddle"> <span class="Estilo_GrisOsc">Premiados y Sancionados&nbsp;</span><%}%><img src="imagen/ico_suspension.gif" alt="ANULACI&Oacute;N Y/O SUSPENSI&Oacute;N" width="22" height="22" border="0" align="absmiddle"> <span class="Estilo_GrisOsc">Anulaci&oacute;n y/o Suspensi&oacute;n </span>&nbsp;<img src="imagen/gim_basurero.gif" alt="ELIMINAR" width="15" height="18" align="absmiddle"> <span class="Estilo_GrisOsc">Eliminar<br>
      &nbsp;</span><%if (sala==10335){%><img src="imagen/ico_generar.gif" alt="GENERAR PREMIADOS Y SANCIONADOS" width="19" height="19" border = "0" align="absmiddle"> <span class="Estilo_GrisOsc">Generar Premiados y Sancionados&nbsp; </span><%}%><img src="imagen/gim_grabar.gif" alt="GRABAR" width="16" height="16" border = "0" align="absmiddle">
        <span class="Estilo_GrisOsc">Grabar</span>&nbsp;<img src="imagen/gim_deshacer.gif" alt="DESHACER" width="16" height="16" border = "0" align="absmiddle"> <span class="Estilo_GrisOsc">Deshacer </span></td>
  </tr>
</table>
<%}} general.getCerrarConexion();%>
 <form name="form_listado" method="post" action="" target="_self">
  <input type="hidden" name="listado" value="10">
    <input type="hidden" name="tipo" value="">
  </form>
</body>
</html>
