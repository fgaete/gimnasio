<%@page language="java"%>
<%
  response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");


  int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int tipo        = request.getParameter("tipo")!=null &&
                    !request.getParameter("tipo").equals("")?
                    Integer.parseInt(request.getParameter("tipo")):0;

  int listado     = request.getParameter("tipo_listado")!=null &&
                    !request.getParameter("tipo_listado").equals("")?
                    Integer.parseInt(request.getParameter("tipo_listado")):0;

  String nom_pagina = "";
  String param      = "";


  if (tipo > 0) {

    // listados
    if (tipo == 1) {
      if (listado == 1 || listado == 2) {
        int sala            = request.getParameter("sala")!=null &&
                              !request.getParameter("sala").equals("null") &&
                              !request.getParameter("sala").equals("NULL") &&
                              !request.getParameter("sala").equals("")  &&
                              !request.getParameter("sala").equals(" ")?
                              Integer.parseInt(request.getParameter("sala")+""):0;
        int bloque          = request.getParameter("bloque")!=null &&
                              !request.getParameter("bloque").equals("null") &&
                              !request.getParameter("bloque").equals("NULL") &&
                              !request.getParameter("bloque").equals("")  &&
                              !request.getParameter("bloque").equals(" ")?
                              Integer.parseInt(request.getParameter("bloque")+""):-1;
        int bloque1         = request.getParameter("bloque1")!=null &&
                              !request.getParameter("bloque1").equals("null") &&
                              !request.getParameter("bloque1").equals("NULL") &&
                              !request.getParameter("bloque1").equals("")  &&
                              !request.getParameter("bloque1").equals(" ")?
                              Integer.parseInt(request.getParameter("bloque1")+""):0;
        int bloque2         = request.getParameter("bloque2")!=null &&
                              !request.getParameter("bloque2").equals("null") &&
                              !request.getParameter("bloque2").equals("NULL") &&
                              !request.getParameter("bloque2").equals("")  &&
                              !request.getParameter("bloque2").equals(" ")?
                              Integer.parseInt(request.getParameter("bloque2")+""):0;
        int anno            = request.getParameter("anno")!=null &&
                              !request.getParameter("anno").equals("null") &&
                              !request.getParameter("anno").equals("NULL") &&
                              !request.getParameter("anno").equals("")  &&
                              !request.getParameter("anno").equals(" ")?
                              Integer.parseInt(request.getParameter("anno")+""):0;
        int semestre        = request.getParameter("semestre")!=null &&
                              !request.getParameter("semestre").equals("null") &&
                              !request.getParameter("semestre").equals("NULL") &&
                              !request.getParameter("semestre").equals("")  &&
                              !request.getParameter("semestre").equals(" ")?
                              Integer.parseInt(request.getParameter("semestre")+""):0;
        String fecha_inicio = request.getParameter("fecha_inicio")!=null &&
                              !request.getParameter("fecha_inicio").equals("null") &&
                              !request.getParameter("fecha_inicio").equals("NULL") &&
                              !request.getParameter("fecha_inicio").equals("")  &&
                              !request.getParameter("fecha_inicio").equals(" ")?
                              request.getParameter("fecha_inicio")+"":"";
        int dia             = request.getParameter("dia")!=null &&
                              !request.getParameter("dia").equals("null") &&
                              !request.getParameter("dia").equals("NULL") &&
                              !request.getParameter("dia").equals("")  &&
                              !request.getParameter("dia").equals(" ")?
                              Integer.parseInt(request.getParameter("dia")+""):0;
        String texto        = request.getParameter("texto")!=null &&
                              !request.getParameter("texto").equals("null") &&
                              !request.getParameter("texto").equals("NULL") &&
                              !request.getParameter("texto").equals("")  &&
                              !request.getParameter("texto").equals(" ")?
                              request.getParameter("texto")+"":"";
        String nom_dia      = request.getParameter("nom_dia")!=null &&
                              !request.getParameter("nom_dia").equals("null") &&
                              !request.getParameter("nom_dia").equals("NULL") &&
                              !request.getParameter("nom_dia").equals("")  &&
                              !request.getParameter("nom_dia").equals(" ")?
                              request.getParameter("nom_dia")+"":"";

        // Inscripciones por bloque
        if (listado == 1) nom_pagina = "gim_asistencia_list.jsp";
        // Asistencia por bloque
        if (listado == 2) nom_pagina = "gim_asistencia_list.jsp";

        param = "?sala="+sala+"&bloque="+bloque+
                "&bloque1="+bloque1+"&bloque2="+bloque2+
                "&anno="+anno+"&semestre="+semestre+
                "&fecha_inicio="+fecha_inicio+"&dia="+dia+
                "&texto="+texto+"&nom_dia="+nom_dia+
                "&tipo_listado="+listado;

      }

      if (listado == 3 ||
          listado == 4 ||
          listado == 5 ||
          listado == 6) {

        int anno         = request.getParameter("anno")!=null &&
                           !request.getParameter("anno").equals("null") &&
                           !request.getParameter("anno").equals("NULL") &&
                           !request.getParameter("anno").equals("")  &&
                           !request.getParameter("anno").equals(" ")?
                           Integer.parseInt(request.getParameter("anno")+""):0;
        int semestre     = request.getParameter("semestre")!=null &&
                           !request.getParameter("semestre").equals("null") &&
                           !request.getParameter("semestre").equals("NULL") &&
                           !request.getParameter("semestre").equals("")  &&
                           !request.getParameter("semestre").equals(" ")?
                           Integer.parseInt(request.getParameter("semestre")+""):0;
        int tipo_usuario = request.getParameter("tipo_usuario")!=null &&
                           !request.getParameter("tipo_usuario").equals("null") &&
                           !request.getParameter("tipo_usuario").equals("NULL") &&
                           !request.getParameter("tipo_usuario").equals("")  &&
                           !request.getParameter("tipo_usuario").equals(" ")?
                           Integer.parseInt(request.getParameter("tipo_usuario")+""):0;
        String periodo   = request.getParameter("periodo")!=null &&
                           !request.getParameter("periodo").equals("null") &&
                           !request.getParameter("periodo").equals("NULL") &&
                           !request.getParameter("periodo").equals("")  &&
                           !request.getParameter("periodo").equals(" ")?
                           request.getParameter("periodo")+"":"";

        // Alumnos de Tutoría
        if (listado == 3) nom_pagina = "gim_registro_list.jsp";
        // Alumnos de Rama Deportiva
        if (listado == 4) nom_pagina = "gim_registro_list.jsp";
        // Alumnos (normal)
        if (listado == 5) nom_pagina = "gim_registro_list.jsp";
        // Funcionarios
        if (listado == 6) nom_pagina = "gim_registro_list.jsp";

        param = "?anno="+anno+"&semestre="+semestre+
                "&tipo_usuario="+tipo_usuario+
                "&periodo="+periodo;
      }

      // premiados
      if (listado == 7) nom_pagina = "gim_rama_list.jsp";

      // tutoría
      if (listado == 8) {

        int anno         = request.getParameter("anno")!=null &&
                           !request.getParameter("anno").equals("null") &&
                           !request.getParameter("anno").equals("NULL") &&
                           !request.getParameter("anno").equals("")  &&
                           !request.getParameter("anno").equals(" ")?
                           Integer.parseInt(request.getParameter("anno")+""):0;
        int semestre     = request.getParameter("semestre")!=null &&
                           !request.getParameter("semestre").equals("null") &&
                           !request.getParameter("semestre").equals("NULL") &&
                           !request.getParameter("semestre").equals("")  &&
                           !request.getParameter("semestre").equals(" ")?
                           Integer.parseInt(request.getParameter("semestre")+""):0;

        nom_pagina = "gim_tutoriaasist_list.jsp";

        param = "?anno="+anno+"&semestre="+semestre;
      }

    }

    // estadísticas
    if (tipo == 2) {

      String fecha_desde = request.getParameter("fecha_desde")!=null &&
                           !request.getParameter("fecha_desde").equals("null") &&
                           !request.getParameter("fecha_desde").equals("NULL") &&
                           !request.getParameter("fecha_desde").equals("")  &&
                           !request.getParameter("fecha_desde").equals(" ")?
                           request.getParameter("fecha_desde")+"":"";
      String fecha_hasta = request.getParameter("fecha_hasta")!=null &&
                           !request.getParameter("fecha_hasta").equals("null") &&
                           !request.getParameter("fecha_hasta").equals("NULL") &&
                           !request.getParameter("fecha_hasta").equals("")  &&
                           !request.getParameter("fecha_hasta").equals(" ")?
                           request.getParameter("fecha_hasta")+"":"";
      int version =        request.getParameter("version")!=null &&
                           !request.getParameter("version").equals("null") &&
                           !request.getParameter("version").equals("NULL") &&
                           !request.getParameter("version").equals("")  &&
                           !request.getParameter("version").equals(" ")?
                           Integer.parseInt(request.getParameter("version")+""):0;
      int sala =           request.getParameter("sala")!=null &&
                           !request.getParameter("sala").equals("null") &&
                           !request.getParameter("sala").equals("NULL") &&
                           !request.getParameter("sala").equals("")  &&
                           !request.getParameter("sala").equals(" ")?
                           Integer.parseInt(request.getParameter("sala")+""):0;
      int anno =           request.getParameter("anno")!=null &&
                           !request.getParameter("anno").equals("null") &&
                           !request.getParameter("anno").equals("NULL") &&
                           !request.getParameter("anno").equals("")  &&
                           !request.getParameter("anno").equals(" ")?
                           Integer.parseInt(request.getParameter("anno")+""):0;
      int semestre =       request.getParameter("semestre")!=null &&
                           !request.getParameter("semestre").equals("null") &&
                           !request.getParameter("semestre").equals("NULL") &&
                           !request.getParameter("semestre").equals("")  &&
                           !request.getParameter("semestre").equals(" ")?
                           Integer.parseInt(request.getParameter("semestre")+""):0;



      if (listado == 1) nom_pagina = "gim_estadistica01_list.jsp";
      if (listado == 2) nom_pagina = "gim_estadistica02_list.jsp";
      if (listado == 3) nom_pagina = "gim_estadistica03_list.jsp";

      param = "?fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+
              "&version="+version+"&sala="+sala+
              "&anno="+anno+"&semestre="+semestre;

    }


    nom_pagina = nom_pagina + param;
// System.out.println(nom_pagina);
%>

<html>
<head>
<title>Gimnasio - Generar impresi&oacute;n</title>
<!-Estilos -->
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<SCRIPT LANGUAGE="JavaScript">
  <!-- Begin
  var da = (document.all) ? 1 : 0;
  var pr = (window.print) ? 1 : 0;
  var mac = (navigator.userAgent.indexOf("Mac") != -1);

  function printPage(frame, arg) {
    if (frame == window) {
      printThis();
    } else {
      link = arg; // a global variable
      printFrame(frame);
    }
    return false;
  }

  function printThis() {
    if (pr) { // NS4, IE5
      window.print();
    } else if (da && !mac) { // IE4 (Windows)
      vbPrintPage();
    } else { // otros browsers
      alert("Su Browser no soporta esta característica");
    }
  }

  function printFrame(frame) {
    if (pr && da) { // IE5
      frame.focus();
      window.print();
      link.focus();
    } else if (pr) { // NS4
      frame.print();
    } else if (da && !mac) { // IE4 (Windows)
      frame.focus();
      setTimeout("vbPrintPage(); link.focus();", 100);
    } else { // otros browsers
      alert("Su Browser no soporta esta característica");
    }
  }

  if (da && !pr && !mac) with (document) {
    writeln('<OBJECT ID="WB" WIDTH="0" HEIGHT="0" CLASSID="clsid:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>');
    writeln('<' + 'SCRIPT LANGUAGE="VBScript">');
    writeln('Sub window_onunload');
    writeln('  On Error Resume Next');
    writeln('  Set WB = nothing');
    writeln('End Sub');
    writeln('Sub vbPrintPage');
    writeln('  OLECMDID_PRINT = 6');
    writeln('  OLECMDEXECOPT_DONTPROMPTUSER = 2');
    writeln('  OLECMDEXECOPT_PROMPTUSER = 1');
    writeln('  On Error Resume Next');
    writeln('  WB.ExecWB OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER');
    writeln('End Sub');
    writeln('<' + '/SCRIPT>');
  }

  function Cerrar() {
  parent.self.close();
  }
// End -->
</script>
</head>
<body>
<iframe name="cuerpo" src="<%=nom_pagina%>" scrolling="auto" width="100%" height="92%" border="0" frameborder="1"></iframe>
<table width="100%" height="35" border="0" cellpadding="1" cellspacing="0">
  <tr valign="bottom">
    <td width="50%" align="right">
      <table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseIMPRIMIR.gif">
      <tr onClick="return printPage(parent.cuerpo, this)" onMouseOver="BotonOver(this,'Imprimir')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino letra8">Imprimir</td>
      </tr>
      </table></td>
    <td width="1%"></td>
    <td width="50%">
    <table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseCERRAR.gif">
      <tr onClick="Cerrar()" onMouseOver="BotonOver(this,'Imprimir')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino letra8">Cerrar</td>
      </tr>
      </table>
   </td>
  </tr>
</table>
</body>
</html>
<% } %>
