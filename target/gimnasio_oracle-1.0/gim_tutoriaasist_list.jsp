<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 18/06/2007
   // �ltima Actualizaci�n :
   // Registro listado.    Se listan los registros de tutor�a del a�o-semestre
%>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>


<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
    if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int   a�o =  request.getParameter("anno")!=null &&
                            !request.getParameter("anno").equals("null") &&
                            !request.getParameter("anno").equals("NULL") &&
                            !request.getParameter("anno").equals("")  &&
                            !request.getParameter("anno").equals(" ")
                            ?Integer.parseInt(request.getParameter("anno")+""):0;
  int   semestre =  request.getParameter("semestre")!=null &&
                          !request.getParameter("semestre").equals("null") &&
                          !request.getParameter("semestre").equals("NULL") &&
                          !request.getParameter("semestre").equals("")  &&
                          !request.getParameter("semestre").equals(" ")
                          ?Integer.parseInt(request.getParameter("semestre")+""):0;
  String horario1 = "";
  String horario2 = "";
  String horario3 = "";
  String sala = "";
  int indice = 0;
  int indice2 = 0;
  Vector vec_tutoria = new Vector();
  Vector v_asistencia = new Vector();
  general.getAbrirConexion();
  general.setGenerarConexionRegistro();
  java.util.Vector vec = new java.util.Vector();
  if (a�o > 0 && semestre > 0 ) {
   vec = general.getListadoRegistro(a�o, semestre, 2);
 }
  java.util.Calendar f = new java.util.GregorianCalendar();
  int dia = f.get(java.util.Calendar.DAY_OF_MONTH);
  int mes = f.get(java.util.Calendar.MONTH) + 1;
  int anno = f.get(java.util.Calendar.YEAR);
  int hora = f.get(java.util.Calendar.HOUR_OF_DAY);
  int minuto = f.get(java.util.Calendar.MINUTE);
  String  dn       = "AM";
  if (hora>12){
    dn    = "PM";
    hora = hora - 12;
  }
  if (hora == 0) hora = 12;

%>
<html>
<head>
<title>Gimnasio - Lista de Tutor&iacute;as</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
</head>
<body>

<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td height="20" ><table width="100%"  border="0">
      <tr>
        <td width="20%" height="50" class="letra8" >Gimnasio III<br>
          USM </td>
        <td width="65%" class="letra10" align="center">Lista de Asistencias <br>
          de Tutor&iacute;as <%=a�o%>-<%=semestre%> </td>
        <td width="15%" align="right" ><span class="letra8"><%=dia%>/<%=mes%>/<%=anno%><br>
          <%=hora%>:<%=minuto%> <%=dn%></span></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="100%" height="20" align="center" valign="top" class="letra8">&nbsp;</td>
  </tr>
  <tr>
    <td height="20" align="center" valign="top" class="letra10">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="1" cellpadding="2" cellspacing="1">
  <tr bgcolor="#FFFFFF">
    <td width="4%" class="Estilo_GrisBold letra8">N&deg;</td>
    <td width="13%" class="Estilo_GrisBold letra8">Paterno</td>
    <td width="13%" class="Estilo_GrisBold letra8">Materno</td>
    <td width="15%" class="Estilo_GrisBold letra8">Nombres</td>
    <td  class="Estilo_GrisBold letra8">RUT</td>
    <td align="center"  class="Estilo_GrisBold letra8">Horario 1 </td>
    <td align="center"  class="Estilo_GrisBold letra8">Horario 2 </td>
    <td align="center"  class="Estilo_GrisBold letra8">Horario 3 </td>

  </tr>
   <%for (int i=0;vec != null && i < vec.size(); i++){
   /* String  horario = ((Vector) vec.get(i)).get(4)+ "";
     indice = horario.indexOf("<br>");
     sala = horario.substring(0,indice);
     indice2 =  horario.indexOf("<br>",indice+4);
     horario1 = horario.substring(indice+4,indice2);

     indice = indice2;
     indice2 =  horario.indexOf("<br>",indice+1);
     horario2 = horario.substring(indice+4,indice2);
     horario3 = horario.substring(indice2+4, horario.length());
     String asistencia = ((Vector) vec.get(i)).get(5)+ "";*/
     String asistencia = "";
     String asis = "";
     general.setGenerarConexionReserva();

general.setGenerarConexionRegistro();
%>

  <tr valign="top" bgcolor="#FFFFFF">
    <td class="letra8"><%=i+1%></td>
    <td class="letra8"><%=((Vector) vec.get(i)).get(0)+ ""%></td>
    <td class="letra8"><%=((Vector) vec.get(i)).get(1)+ ""%></td>
    <td class="letra8"><%=((Vector) vec.get(i)).get(2)+ ""%></td>
    <td width="10%" nowrap class="letra8"><%=((Vector) vec.get(i)).get(3)+ ""%></td>
    <%  vec_tutoria = general.getReservaTutoriaPeriodoList(((Vector) vec.get(i)).get(5)+ "",
        a�o,semestre);
  //   if (vec_tutoria.size() > 0){
       for (int k=0; k < vec_tutoria.size();k++){
%>
    <td width="15%">    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr align="center" class="letra8">

        <td colspan="2" nowrap class="Estilo_Azul"><%=((Vector)vec_tutoria.get(k)).get(0)+""%><br><%=((Vector)vec_tutoria.get(k)).get(1)+""%> </td>
        </tr>
      <% v_asistencia = general.getAsistenciaBloque(Integer.parseInt(((Vector)vec_tutoria.get(k)).get(2)+""),
      ((Vector)vec_tutoria.get(k)).get(3)+"",((Vector)vec_tutoria.get(k)).get(4)+"",
      ((Vector)vec_tutoria.get(k)).get(5)+"", Integer.parseInt(((Vector) vec_tutoria.get(k)).get(6)+ ""),
      Integer.parseInt(((Vector) vec_tutoria.get(k)).get(7)+ ""));
  asis = "&nbsp;";
  if(v_asistencia.size()>0){
     for (int j=0; j< v_asistencia.size();j++){
      switch (Integer.parseInt(((Vector)v_asistencia.get(j)).get(2)+"")){
      case 1: asis = "<span class='Estilo_Azul'>A</span>";
        break;
      case 2: asis = "<span class='Estilo_Gris'>--</span>";
        break;
      default: asis = "<span class='Estilo_Rojo'>X</span>";
    }
    asistencia =(!(((Vector)v_asistencia.get(j)).get(1)+"").trim().equals("") &&
                 !(((Vector)v_asistencia.get(j)).get(1)+"").trim().equals(""))?
                ((Vector)v_asistencia.get(j)).get(1)+"":"&nbsp;";
   %>
      <tr class="letra8">
        <td width="64%" align="center"><%=asistencia%></td>
        <td width="36%" align="center" class="Estilo_Azulino"><%=asis%></td>
      </tr>
      <%}
}%>

    </table></td>
    <%} int cont = vec_tutoria.size();
      while (cont < 3){%>
       <td >&nbsp;</td>
      <%cont++;
      }
    //  }
%>

  </tr>
  <%}%>
</table>
<div align="center"><font class="barra" color="#000000"><br>
</font>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr class="letra8">
      <td colspan="2" nowrap>C&oacute;digos de asistencia </td>
    </tr>
    <tr class="letra8">
      <td nowrap class="Estilo_Azul">A</td>
      <td nowrap>Asiste </td>
    </tr>
    <tr class="letra8">
      <td width="4%" class="Estilo_Rojo">X</td>
      <td width="96%">Falta </td>
    </tr>
    <tr class="letra8">
      <td class="Estilo_Gris">--</td>
      <td>Justifica</td>
    </tr>
  </table>
  <font class="barra" color="#000000">  <br>
  <br>
&copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a</font></div>
</body>
</html>
<%   general.getCerrarConexion();%>
