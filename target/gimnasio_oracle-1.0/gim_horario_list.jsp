<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 04/07/2007
   // �ltima Actualizaci�n :
   // Listado de horario.
%>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
   int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");

  int   anno =  request.getParameter("anno")!=null &&
                !request.getParameter("anno").equals("null") &&
                !request.getParameter("anno").equals("NULL") &&
                !request.getParameter("anno").equals("")  &&
                !request.getParameter("anno").equals(" ")
             ?Integer.parseInt(request.getParameter("anno")+""):0;
  int   semestre =  request.getParameter("semestre")!=null &&
                    !request.getParameter("semestre").equals("null") &&
                    !request.getParameter("semestre").equals("NULL") &&
                    !request.getParameter("semestre").equals("")  &&
                    !request.getParameter("semestre").equals(" ")
                 ?Integer.parseInt(request.getParameter("semestre")+""):0;
  int   sala =  request.getParameter("sala")!=null &&
                !request.getParameter("sala").equals("null") &&
                !request.getParameter("sala").equals("NULL") &&
                !request.getParameter("sala").equals("")  &&
                !request.getParameter("sala").equals(" ")
               ?Integer.parseInt(request.getParameter("sala")+""):0;
  Vector vec_horario1 = (session.getAttribute("vec_horario1")!= null)?
                        (Vector) session.getAttribute("vec_horario1"):new Vector();
  Vector vec_horabloque1 = (session.getAttribute("vec_horabloque1")!= null)?
                           (Vector) session.getAttribute("vec_horabloque1"):new Vector();
  Vector ind_vec_horabloque = (session.getAttribute("ind_vec_horabloque")!= null)?
                              (Vector) session.getAttribute("ind_vec_horabloque"):new Vector();
  Vector vec_hora = new Vector();
  java.util.Calendar f = new java.util.GregorianCalendar();
  int dia = f.get(java.util.Calendar.DAY_OF_MONTH);
  int mes = f.get(java.util.Calendar.MONTH) + 1;
  int a�o = f.get(java.util.Calendar.YEAR);
  int hora = f.get(java.util.Calendar.HOUR);
  int minuto = f.get(java.util.Calendar.MINUTE);
  if (anno == 0)
    anno = a�o;


%>
<html>
<head>
<title>Gimnasio - Lista de Asistentes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
</head>

<body>

<table width="100%"  border="0">
  <tr>
    <td width="20%" height="50" class="letra8" >Gimnasio III<br>
      USM </td>
    <td width="65%" class="letra10" align="center"><span class="letra8">Horario <%=anno%>-<%=semestre%> <br>
        <%=(sala==10335)?"Sala de M&aacute;quinas":"Sala de Multiuso"%><br>
    </span></td>
    <td width="15%" >&nbsp;
        <div align="right"><span class="letra8"><%=dia%>/<%=mes%>/<%=a�o%><br>
              <%=hora%>:<%=minuto%></span></div></td>
  </tr>
</table>
<br>
<br>
<table width="90%"  border="1" align="center" cellpadding="3" cellspacing="0">
  <tr align="center" bgcolor="#CCCCCC">
    <td width="10%" height="16"><strong>Bloques</strong></td>
    <td width="15%"><strong>Lunes</strong></td>
    <td width="15%"><strong>Martes</strong></td>
    <td width="15%"><strong>Mi&eacute;rcoles</strong></td>
    <td width="15%"><strong>Jueves</strong></td>
    <td width="15%"><strong>Viernes</strong></td>
    <td width="15%"><strong>S&aacute;bado</strong></td>
  </tr>
 <%Vector vec_hordia  = new Vector();
         Vector vec_rowspan = new Vector();
         for (int i = 0; i < vec_horabloque1.size(); i++) {
            Vector v = (Vector) vec_horabloque1.get(i);
            int cod_bloque = Integer.parseInt(v.get(0).toString());
            int ind_cod_bloque = ind_vec_horabloque.indexOf(v.get(0).toString()) + 1;

       %>
        <tr align="center" bgcolor="#FFFFFF">
          <td class="letra7 Estilo_Azul"><%=v.get(1)%><br><%=v.get(2)%></td>
          <%

 /*
          0 d�a
          1 bloque inicio
          2 bloque fin
          3 nom.tipo reserva
          4 profesor
          5 tipo reserva
          6 color texto

          */
          for (int d = 1; d < 7; d++) {
            boolean pasa = false;
            String td   = "";

            for (int j = 0; j < vec_horario1.size(); j++) {
              Vector h = (Vector) vec_horario1.get(j);
               dia = Integer.parseInt(h.get(0).toString());
              int bi  = Integer.parseInt(h.get(1).toString());
              int bt  = Integer.parseInt(h.get(2).toString());
              int cod_reserva = Integer.parseInt(h.get(5).toString());
              int ind_bi = ind_vec_horabloque.indexOf(h.get(1).toString())+1;
              int ind_bt = ind_vec_horabloque.indexOf(h.get(2).toString())+1;

              if (d == dia &&
                ind_cod_bloque >= ind_bi  &&
                ind_cod_bloque <= ind_bt  &&
                !vec_hordia.contains(dia+"-"+bi+"-"+bt)) {

                td   = "";
                pasa = true;
                vec_hordia.addElement(dia+"-"+bi+"-"+bt);

                // guarda los bloques reales con rowspan
                for (int b = ind_bi; b <= ind_bt; b++) {
                if (!vec_rowspan.contains(d+"-"+b))
                    vec_rowspan.addElement(d+"-"+b);
                }

                String prof   = h.get(4).toString();
                String estilo = h.get(6).toString();
                String tipo   = h.get(3).toString();

                if (sala == 10336 && cod_reserva != 1)
                     prof += "<br>"+ h.get(8).toString();

                String rowspan = "";

                int dif = (ind_bt - ind_bi) + 1;
               if (dif > 1) rowspan = "rowspan=\""+dif+"\"";



                    td = "<td "+rowspan+" id=\"dia_"+dia+"_"+i+"\""+
                         " class=\""+estilo+"\">"+tipo+"<br>"+prof+"</td>";


              }
            }
            if (!pasa && !vec_rowspan.contains(d+"-"+ind_cod_bloque)) {
              td = "<td >&nbsp;</td>";
            }
        %>
          <%=td%>
        <%}%>
        </tr>
        <%}%>
</table>
<div align="center"><font class="barra" color="#000000"><br>
  <br>
&copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a</font>
</div>
</body>
</html>
