<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<%
	// Creado por           : M�nica Barrera Frez
	// Fecha                : 3/7/2007
	// �ltima Actualizaci�n :
	// Horario. Consulta de horarios.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
	response.addHeader("Cache-Control",
			"no-cache, no-store, must-revalidate");

	if (session.getAttribute("vec_horario1") != null)
		session.removeAttribute("vec_horario1");
	if (session.getAttribute("vec_horabloque1") != null)
		session.removeAttribute("vec_horabloque1");
	if (session.getAttribute("ind_vec_horabloque") != null)
		session.removeAttribute("ind_vec_horabloque");

	int rut_usuario = session.getAttribute("rut_usuario") != null ? Integer
			.parseInt(session.getAttribute("rut_usuario") + "")
			: 0;
	if (rut_usuario == 0)
		response.sendRedirect("index.jsp");
	int cod_perfil = session.getAttribute("cod_perfil") != null ? Integer
			.parseInt(session.getAttribute("cod_perfil") + "")
			: 0;
	int anno = request.getParameter("anno") != null
			&& !request.getParameter("anno").equals("null")
			&& !request.getParameter("anno").equals("NULL")
			&& !request.getParameter("anno").equals("")
			&& !request.getParameter("anno").equals(" ") ? Integer
			.parseInt(request.getParameter("anno") + "") : 0;
	int semestre = request.getParameter("semestre") != null
			&& !request.getParameter("semestre").equals("null")
			&& !request.getParameter("semestre").equals("NULL")
			&& !request.getParameter("semestre").equals("")
			&& !request.getParameter("semestre").equals(" ") ? Integer
			.parseInt(request.getParameter("semestre") + "") : 0;
	int sala = request.getParameter("sala") != null
			&& !request.getParameter("sala").equals("null")
			&& !request.getParameter("sala").equals("NULL")
			&& !request.getParameter("sala").equals("")
			&& !request.getParameter("sala").equals(" ") ? Integer
			.parseInt(request.getParameter("sala") + "") : 0;
	int dia1 = request.getParameter("dia1") != null
			&& !request.getParameter("dia1").equals("null")
			&& !request.getParameter("dia1").equals("NULL")
			&& !request.getParameter("dia1").equals("")
			&& !request.getParameter("dia1").equals(" ") ? Integer
			.parseInt(request.getParameter("dia1") + "") : 0;
	int dia2 = request.getParameter("dia2") != null
			&& !request.getParameter("dia2").equals("null")
			&& !request.getParameter("dia2").equals("NULL")
			&& !request.getParameter("dia2").equals("")
			&& !request.getParameter("dia2").equals(" ") ? Integer
			.parseInt(request.getParameter("dia2") + "") : 0;
	int bloque1 = request.getParameter("bloque1") != null
			&& !request.getParameter("bloque1").equals("null")
			&& !request.getParameter("bloque1").equals("NULL")
			&& !request.getParameter("bloque1").equals("")
			&& !request.getParameter("bloque1").equals(" ") ? Integer
			.parseInt(request.getParameter("bloque1") + "") : 0;
	int bloque2 = request.getParameter("bloque2") != null
			&& !request.getParameter("bloque2").equals("null")
			&& !request.getParameter("bloque2").equals("NULL")
			&& !request.getParameter("bloque2").equals("")
			&& !request.getParameter("bloque2").equals(" ") ? Integer
			.parseInt(request.getParameter("bloque2") + "") : 0;
	String onload = request.getParameter("onload") != null
			&& !request.getParameter("onload").equals("null")
			&& !request.getParameter("onload").equals("NULL")
			&& !request.getParameter("onload").equals("")
			&& !request.getParameter("onload").equals(" ") ? request
			.getParameter("onload")
			+ "" : "escondeBloqueTodo();";
	int preferencia = request.getParameter("preferencia") != null
			&& !request.getParameter("preferencia").equals("null")
			&& !request.getParameter("preferencia").equals("NULL")
			&& !request.getParameter("preferencia").equals("")
			&& !request.getParameter("preferencia").equals(" ") ? Integer
			.parseInt(request.getParameter("preferencia") + "")
			: 0;
	int paralelo = request.getParameter("paralelo") != null
			&& !request.getParameter("paralelo").equals("null")
			&& !request.getParameter("paralelo").equals("NULL")
			&& !request.getParameter("paralelo").equals("")
			&& !request.getParameter("paralelo").equals(" ") ? Integer
			.parseInt(request.getParameter("paralelo") + "") : 0;
	int accion = request.getParameter("accion") != null
			&& !request.getParameter("accion").equals("null")
			&& !request.getParameter("accion").equals("NULL")
			&& !request.getParameter("accion").equals("")
			&& !request.getParameter("accion").equals(" ") ? Integer
			.parseInt(request.getParameter("accion") + "") : 0;
	int actividad = request.getParameter("actividad") != null
			&& !request.getParameter("actividad").equals("null")
			&& !request.getParameter("actividad").equals("NULL")
			&& !request.getParameter("actividad").equals("")
			&& !request.getParameter("actividad").equals(" ") ? Integer
			.parseInt(request.getParameter("actividad") + "") : 0;
	String fecha_inicio = request.getParameter("fecha_inicio") != null
			&& !request.getParameter("fecha_inicio").equals("null")
			&& !request.getParameter("fecha_inicio").equals("NULL")
			&& !request.getParameter("fecha_inicio").equals("")
			&& !request.getParameter("fecha_inicio").equals(" ") ? request
			.getParameter("fecha_inicio")
			+ ""
			: "null";
	String fecha_termino = request.getParameter("fecha_termino") != null
			&& !request.getParameter("fecha_termino").equals("null")
			&& !request.getParameter("fecha_termino").equals("NULL")
			&& !request.getParameter("fecha_termino").equals("")
			&& !request.getParameter("fecha_termino").equals(" ") ? request
			.getParameter("fecha_termino")
			+ ""
			: "null";
	int num_dia = request.getParameter("num_dia") != null
			&& !request.getParameter("num_dia").equals("null")
			&& !request.getParameter("num_dia").equals("NULL")
			&& !request.getParameter("num_dia").equals("")
			&& !request.getParameter("num_dia").equals(" ") ? Integer
			.parseInt(request.getParameter("num_dia") + "") : 0;
	int bloque_inicio = request.getParameter("bloque_inicio") != null
			&& !request.getParameter("bloque_inicio").equals("null")
			&& !request.getParameter("bloque_inicio").equals("NULL")
			&& !request.getParameter("bloque_inicio").equals("")
			&& !request.getParameter("bloque_inicio").equals(" ") ? Integer
			.parseInt(request.getParameter("bloque_inicio") + "")
			: 0;
	int bloque_termino = request.getParameter("bloque_termino") != null
			&& !request.getParameter("bloque_termino").equals("null")
			&& !request.getParameter("bloque_termino").equals("NULL")
			&& !request.getParameter("bloque_termino").equals("")
			&& !request.getParameter("bloque_termino").equals(" ") ? Integer
			.parseInt(request.getParameter("bloque_termino") + "")
			: 0;
	int horario = request.getParameter("horario") != null
			&& !request.getParameter("horario").equals("null")
			&& !request.getParameter("horario").equals("NULL")
			&& !request.getParameter("horario").equals("")
			&& !request.getParameter("horario").equals(" ") ? Integer
			.parseInt(request.getParameter("horario") + "") : 0;
	String id_aseo = request.getParameter("id_aseo") != null
			&& !request.getParameter("id_aseo").equals("null")
			&& !request.getParameter("id_aseo").equals("NULL")
			&& !request.getParameter("id_aseo").equals("")
			&& !request.getParameter("id_aseo").equals(" ") ? request
			.getParameter("id_aseo")
			+ "" : "0";
	int rutnum = request.getParameter("rutnum") != null
			&& !request.getParameter("rutnum").equals("null")
			&& !request.getParameter("rutnum").equals("NULL")
			&& !request.getParameter("rutnum").equals("")
			&& !request.getParameter("rutnum").equals(" ") ? Integer
			.parseInt(request.getParameter("rutnum") + "") : 0;
	int cupo = request.getParameter("cupo") != null
			&& !request.getParameter("cupo").equals("null")
			&& !request.getParameter("cupo").equals("NULL")
			&& !request.getParameter("cupo").equals("")
			&& !request.getParameter("cupo").equals(" ") ? Integer
			.parseInt(request.getParameter("cupo") + "") : 0;
	int rama = request.getParameter("rama") != null
			&& !request.getParameter("rama").equals("null")
			&& !request.getParameter("rama").equals("NULL")
			&& !request.getParameter("rama").equals("")
			&& !request.getParameter("rama").equals(" ") ? Integer
			.parseInt(request.getParameter("rama") + "") : 0;
	boolean soloconsulta = false;
	if (cod_perfil == 2)
		soloconsulta = true;

	Vector vec_horabloque1 = new Vector();
	java.util.Calendar f = new java.util.GregorianCalendar();
	Vector vec_horario1 = new Vector();
	Vector vec_horario_s = new Vector();
	Vector vec_aseo = new Vector();
	Vector vec_bloque = new Vector();
	Vector vec_actividad = new Vector();
	Vector vec_mantactividad = new Vector();
	Vector v = new Vector();
	Vector vec_paralelo = new Vector();
	Vector vec_horario = new Vector();
	Vector vec_programacion = new Vector();
	Vector vec_prof = new Vector();
	Vector vec_tipo_reserva = new Vector();
	Vector ind_vec_horabloque = new Vector();
	StringBuffer combo_paralValue = new StringBuffer();
	StringBuffer combo_paralText = new StringBuffer();
	StringBuffer combo_paral = new StringBuffer();
	StringBuffer combo_horValue = new StringBuffer();
	StringBuffer combo_horText = new StringBuffer();
	StringBuffer combo_hor_blo1 = new StringBuffer();
	StringBuffer combo_hor_blo2 = new StringBuffer();
	StringBuffer combo_hor_cod = new StringBuffer();
	StringBuffer combo_hor_par = new StringBuffer();
	StringBuffer combo_hor_dia = new StringBuffer();
	StringBuffer combo_hor_pref = new StringBuffer();
	StringBuffer combo_progValue = new StringBuffer();
	StringBuffer combo_progText = new StringBuffer();
	StringBuffer combo_prog = new StringBuffer();
	StringBuffer combo_progCupo = new StringBuffer();
	StringBuffer combo_progRama = new StringBuffer();
	StringBuffer combo_progProf = new StringBuffer();
	StringBuffer combo_progRut = new StringBuffer();
	StringBuffer combo_progRutDig = new StringBuffer();
	StringBuffer combo_progInscritos = new StringBuffer();
	StringBuffer combo_profValue = new StringBuffer();
	StringBuffer combo_profText = new StringBuffer();
	StringBuffer combo_prof = new StringBuffer();

	String estilo = "";
	String mensaje = "";
	int dia = 0;
	int diafin = 0;
	int blo1 = 0;
	int blo2 = 0;
	String nombre = "";
	String nom_prof = "";
	int cod_reserva = 0;
	int error = 0;

	int a�o = f.get(java.util.Calendar.YEAR);
	int dia_actual = f.get(java.util.Calendar.DAY_OF_MONTH);
	int mes_actual = f.get(java.util.Calendar.MONTH) + 1;
	String fecha = (dia_actual < 10) ? "0" + dia_actual : dia_actual
			+ "";
	if (mes_actual < 10)
		fecha += "/0" + mes_actual + "/" + a�o;
	else
		fecha += "/" + mes_actual + "/" + a�o;
	general.getAbrirConexion();
	general.setGenerarConexionHorario();
	general.getPeriodoSemestreActual(1, 1);
	general.setGenerarConexionReserva();
	vec_tipo_reserva = general.getTipoReserva();
	if (anno == 0) {
		semestre = general.getSemestreActual();
		anno = general.getA�oActual();
	}
	if (sala > 0 && anno > 0) {

		if (accion > 0) {
			if (accion == 5) {
				v = new Vector();
				v.addElement(anno + "");
				v.addElement(semestre + "");
				v.addElement(sala + "");
				v.addElement(rut_usuario + "");
				error = general.setCopiarHorario(v);
				if (error == 0)
					mensaje = "Se copi� satisfactoriamente";
				else
					mensaje = "Problemas al copiar " + error;
			} else {
				if (actividad == 0) { // aseo
					if (accion == 1) { // agrega o modifica	
						error = general.setAgregarAseo(1, anno,
								semestre, 1, sala, bloque1, bloque2,
								fecha_inicio, fecha_termino,
								rut_usuario, dia1, dia2, id_aseo);
						switch (error) {
						case 0:
							mensaje = "Se registr� satisfactoriamente.";
							break;
						case -1:
							mensaje = "No puede registrar, pues hay aseo asignado.";
							break;
						case -2:
							mensaje = "No puede registrar, pues hay horario asignado. ";
							break;
						case -3:
							mensaje = "Error al actualizar.";
							break;
						case -4:
							mensaje = "No puede registrar, pues no existe registro a modificar.";
							break;
						case -5:
							mensaje = "Error al registrar.";
							break;
						}

					} else { // la accion es 3
						error = general.setEliminaAseo(id_aseo);
						if (error == 0)
							mensaje = "Se elimin� satisfactoriamente.";
						if (error == -1)
							mensaje = "Problemas al eliminar.";
						if (error == -2)
							mensaje = "Problemas al eliminar, no existe registro.";
					}

				} else { // horario asignatura
					if (accion == 1) {// agrega o modifica
						if (horario == -1) // agrega
							error = general.setAgregarHorario(1, anno,
									semestre, sala, bloque1, bloque2,
									dia1, dia2, preferencia, paralelo,
									actividad, 1, rut_usuario);
						else
							// modifica
							error = general.setModificaHorario(1, anno,
									semestre, sala, bloque1, bloque2,
									dia1, dia2, preferencia, paralelo,
									actividad, 1, bloque_inicio,
									bloque_termino, num_dia, num_dia,
									rut_usuario);
						switch (error) {
						case 0:
							mensaje = "Se registr� satisfactoriamente.";
							break;
						case -1:
							mensaje = "No puede registrar, problemas al Insertar.";
							break;
						case -2:
							mensaje = "El bloque seleccionado ya tiene registrado horario para Aseo.\\nVerifique la asignaci�n de Aseo.";
							break;
						case -3:
							mensaje = "El bloque seleccionado ya tiene registrado horario.\\nVerifique asignaci�n de Horario.";
							break;
						case -4:
							mensaje = "Verifique horario a modificar, pues no est� registrado.";
							break;
						case -5:
							mensaje = "Verifique horario a modificar, pues tiene reserva asociada.";
							break;
						case -6:
						case -7:
							mensaje = "Problemas al registrar.";
							break;
						}

					}
					if (accion == 3) {// elimina horario
						error = general.setEliminaHorario(1, anno,
								semestre, sala, bloque1, bloque2,
								num_dia, rut_usuario);
						if (error == 0)
							mensaje = "Se elimin� satisfactoriamente.";
						if (error == -1)
							mensaje = "Problemas al eliminar.";
						if (error == -2)
							mensaje = "El horario no puede ser eliminado, pues existen reservas. ";
					}
					if (accion == 2) {// agrega/modifica programacion, programacion_profesor
						error = general.setAgregaProgramacion(anno,
								semestre, actividad, paralelo, rutnum,
								rut_usuario, cupo, rama);
						switch (error) {
						case 0:
							mensaje = "Se Registr� satisfactoriamente.";
							break;
						case -1:
						case -2:
							mensaje = "Problemas al Insertar.";
							break;
						case -3:
						case -4:
						case -5:
							mensaje = "Problemas al actualizar. ";
							break;
						case -6:
							mensaje = "Problemas al Insertar. No est� registrada la actividad. ";
							break;
						}
					}
					if (accion == 4) {// eliminar programacion, programacion_profesor 
						error = general.setEliminaProgramacion(anno,
								semestre, actividad, paralelo);
						switch (error) {
						case 0:
							mensaje = "Se Elimin� satisfactoriamente.";
							break;
						case -1:
							mensaje = "Problemas al Eliminar, existe encuesta asociada.";
							break;
						case -2:
							mensaje = "Problemas al Eliminar, existe horario asociado.";
							break;
						case -3:
							mensaje = "Problemas al Eliminar, existe inscripci�n asociada.";
							break;
						case -4:
							mensaje = "Problemas al Eliminar, existe nota parcial asociada.";
							break;
						case -5:
							mensaje = "Problemas al Eliminar, existe porcentaje de nota asociado.";
							break;
						case -6:
							mensaje = "Problemas al Eliminar, existe carrera asociada.";
							break;
						case -7:
							mensaje = "Problemas al Eliminar, existe curso asociado.";
							break;
						case -9:
						case 10:
							mensaje = "Problemas al Eliminar.";
							break;
						case -11:
							mensaje = "Problemas al Eliminar, No existe registro a eliminar.";
							break;

						}
					}
				}
			}
			error = -1;
		}

		vec_horabloque1 = general.getHoraBloque(sala, anno, semestre);

		v = new Vector();

		for (int i = 0; i < vec_horabloque1.size(); i++) {
			v = (Vector) vec_horabloque1.get(i);
			ind_vec_horabloque.addElement(v.get(0).toString());
		}
		vec_horario_s = general.getHorarioSala(anno, semestre, sala);
		vec_aseo = general.getHorarioAseo(sala, anno, semestre);
		vec_paralelo = general.getParalelo(anno, semestre);
		vec_horario = general.getHorario(anno, semestre, sala);
		vec_programacion = general.getProgramacion(anno, semestre);
		vec_prof = general.getProfesorGimnasio();

		/* M.A.A. Se agreo profesor 600 para casos en los que el profesor no este en la base */
		v = new Vector();
		v.addElement("600");
		v.addElement("9");
		v.addElement("-- PROFESOR COMODIN (NN) --");
		vec_prof.addElement(v);

		if (sala == 10335) { // sala m�quinas
			v = new Vector();
			v.addElement("1410030");
			v.addElement("M�quinas");
			vec_actividad.addElement(v);
			vec_mantactividad.addElement(v);
			//         v = new Vector();
			//         v.addElement("1410020");
			//         v.addElement("GIMNASIA AEROBICA");
			//         vec_actividad.addElement(v);
			//          vec_mantactividad.addElement(v);
		} else {

			vec_actividad = general.getActividad();
			v = new Vector();
			v.addElement("1410020");
			v.addElement("GIMNASIA AEROBICA (Docencia)");
			vec_actividad.addElement(v);

			v = new Vector();
			v.addElement("1410017");
			v.addElement("KARATE (DAMAS Y VARONES) (Docencia)");
			vec_actividad.addElement(v);

			vec_mantactividad = general
					.getMantActividad(anno, semestre);

		}

		// se llena con los cod_bloques
		for (int i = 0; i < vec_horabloque1.size(); i++) {
			vec_bloque.addElement(((Vector) vec_horabloque1.get(i))
					.get(0)
					+ "");
		}
		// se llena con estilos

		// System.out.println("vec_bloque " + vec_bloque);
		Vector vec_color = new Vector();
		vec_color.addElement("Estilo_Azulino");
		vec_color.addElement("Estilo_Cafe");
		vec_color.addElement("Estilo_Morado");
		vec_color.addElement("Estilo_Naranjo");
		vec_color.addElement("Estilo_AzulinoClaro");
		vec_color.addElement("Estilo_Verde");
		vec_color.addElement("Estilo_MoradoClaro");
		vec_color.addElement("Estilo_Azul");

		// se llena con ebgcolor
		Vector vec_bgcolor = new Vector();
		vec_bgcolor.addElement("#B5D9EE"); // azulino
		vec_bgcolor.addElement("#663300"); // caf�
		vec_bgcolor.addElement("#6600CC"); // morado
		vec_bgcolor.addElement("#CC6600"); // naranjo
		vec_bgcolor.addElement("#006699"); // azulino claro
		vec_bgcolor.addElement("#FFE6FF"); // verde
		vec_bgcolor.addElement("#663366"); // morado claro
		vec_bgcolor.addElement("#003366"); // azul

		// agrega a plantilla el horario de sala
		int ind_color = 0;
		Vector vec_nom_asign = new Vector();
		Vector vn = new Vector();
		Vector vg = new Vector();

		// System.out.println("vec_horario " + vec_horario); 
		// System.out.println("vec_horario_s " + vec_horario_s); 
		for (int y = 0; y < vec_horario_s.size(); y++) {
			nombre = ((Vector) vec_horario_s.get(y)).get(3) + "";
			v = new Vector();
			v.addElement(((Vector) vec_horario_s.get(y)).get(0) + ""); // 0 dia
			v.addElement(((Vector) vec_horario_s.get(y)).get(1) + ""); // 1 bl inicio
			v.addElement(((Vector) vec_horario_s.get(y)).get(2) + ""); // 2 bl termino
			v.addElement(nombre); // 3 nombre
			v.addElement(((Vector) vec_horario_s.get(y)).get(4) + ""); // 4 prof

			int tipo_ocup = 0;
			String bg = "";
			tipo_ocup = Integer
					.parseInt(((Vector) vec_horario_s.get(y)).get(5)
							+ "");

			v.addElement(tipo_ocup + ""); // 5 tipo reserva

			if (sala == 10335) {
				// tipo reserva
				switch (tipo_ocup) {
				case 1: {
					estilo = "Estilo_Azulino"; // alumnos
					bg = "#B5D9EE";
					break;
				}
				case 2: {
					estilo = "Estilo_Naranjo"; // funcionarios
					bg = "#FDEACE";
					break;
				}
				case 3: {
					estilo = "Estilo_Morado"; // rector�a
					bg = "#E2D5F0";
					break;
				}

				case 4: {
					estilo = "Estilo_MoradoClaro"; // directores
					bg = "#FFE6FF";
					break;
				}
				}
			} else {
				if (vec_nom_asign.indexOf(nombre) < 0) {
					vn.addElement(vec_color.get(ind_color));
					vg.addElement(vec_bgcolor.get(ind_color));
					vec_nom_asign.addElement(nombre);
					estilo = vec_color.get(ind_color) + "";
					bg = vec_bgcolor.get(ind_color) + "";
				} else {
					estilo = vn.get(vec_nom_asign.indexOf(nombre)) + "";
					bg = vg.get(vec_nom_asign.indexOf(nombre)) + "";
				}
				if (ind_color < vec_color.size() - 1)
					ind_color++;
				else
					ind_color = 0;
			}
			v.addElement(estilo); // 6 estilo
			v.addElement(bg); // 7 bg color
			v.addElement(((Vector) vec_horario_s.get(y)).get(6) + ""); // 8 tipo reserva
			v.addElement(((Vector) vec_horario_s.get(y)).get(7) + ""); // 9 cod_asign
			vec_horario1.addElement(v);

		}

		// agrega a vec horario el aseo
		Vector v_hor = new Vector();
		Vector blo_inicio = new Vector();
		Vector blo_fin = new Vector();
		String b1 = "";
		String b2 = "";
		String ndia = "";
		int bloini = 0;
		int blofin = 0;
		for (int y = 0; y < vec_aseo.size(); y++) {
			dia = Integer.parseInt(((Vector) vec_aseo.get(y)).get(0)
					+ "");
			diafin = Integer.parseInt(((Vector) vec_aseo.get(y)).get(1)
					+ "");
			//     blo1 = vec_bloque.indexOf(((Vector)vec_aseo.get(y)).get(2)+"");
			//     blo2 = vec_bloque.indexOf(((Vector)vec_aseo.get(y)).get(3)+"");
			//     bloini = Integer.parseInt(vec_bloque.get(blo1)+"");
			//     blofin = Integer.parseInt(vec_bloque.get(blo2)+"");
			bloini = Integer.parseInt(((Vector) vec_aseo.get(y)).get(2)
					+ "");
			blofin = Integer.parseInt(((Vector) vec_aseo.get(y)).get(3)
					+ "");

			String texto_td = "";
			int tipo_ocup = 0;
			nombre = "-- Aseo --"; // 1 - nombre
			estilo = "Estilo_Gris"; // alumnos
			for (int j = dia; j <= diafin; j++) {
				v = new Vector();
				v.addElement(j + ""); // 0 dia
				v.addElement(bloini + ""); // 1 bl inicio
				v.addElement(blofin + ""); // 2 bl fin
				v.addElement(nombre); // 3 reserva
				v.addElement(""); // 4 profesor
				v.addElement("0"); // 5 tipo reserva
				v.addElement(estilo); // 6 estilo
				v.addElement("#999999"); // 7 bg color
				v.addElement(""); // 8 tipo reserva
				v.addElement(((Vector) vec_aseo.get(y)).get(4) + ""); // 9 id
				vec_horario1.addElement(v);

			}
			// para llenar el horario del mantenedor
			v_hor = new Vector();
			v_hor.addElement("0"); // 0 cod asign
			v_hor.addElement("1"); // 1 paralelo
			v_hor.addElement(dia + "@" + diafin); // 2 id
			v_hor.addElement(bloini + ""); // 3 cod bl inicio
			v_hor.addElement(blofin + ""); // 4 cod b2 fin
			blo_inicio = general.getBloqueCodigoSede(1, 1, bloini);
			blo_fin = general.getBloqueCodigoSede(1, 1, blofin);
			b1 = (blo_inicio.size() > 0) ? blo_inicio.get(0) + "" : "";
			b2 = (blo_inicio.size() > 0) ? blo_inicio.get(1) + "" : "";
			v_hor.addElement(b1 + "-" + b2); // 5 -bloque_inicio
			b1 = (blo_fin.size() > 0) ? blo_fin.get(0) + "" : "";
			b2 = (blo_fin.size() > 0) ? blo_fin.get(1) + "" : "";
			v_hor.addElement(b1 + "-" + b2); // 6 -bloque_termino

			switch (dia) {
			case 1:
				ndia = "Lu";
				break;
			case 2:
				ndia = "Ma";
				break;
			case 3:
				ndia = "Mi";
				break;
			case 4:
				ndia = "Ju";
				break;
			case 5:
				ndia = "Vi";
				break;
			case 6:
				ndia = "S�";
				break;
			}
			switch (diafin) {
			case 1:
				if (ndia != "Lu")
					ndia += " - Lu";
				break;
			case 2:
				if (ndia != "Ma")
					ndia += " - Ma";
				break;
			case 3:
				if (ndia != "Mi")
					ndia += " - Mi";
				break;
			case 4:
				if (ndia != "Ju")
					ndia += " - Ju";
				break;
			case 5:
				if (ndia != "Vi")
					ndia += " - Vi";
				break;
			case 6:
				if (ndia != "S�")
					ndia += " - S�";
				break;
			}
			v_hor.addElement(ndia); // 7 dia con palabra
			v_hor.addElement(((Vector) vec_aseo.get(y)).get(5) + "@"+ ((Vector) vec_aseo.get(y)).get(4) + ""); // fecha - id
			v_hor.addElement(((Vector) vec_aseo.get(y)).get(6));//9-secuencia inicio
			v_hor.addElement(((Vector) vec_aseo.get(y)).get(7));//10-secuencia termino
			vec_horario.addElement(v_hor);
		}

		session.setAttribute("vec_horario1", vec_horario1);
		session.setAttribute("vec_horabloque1", vec_horabloque1);
		session.setAttribute("ind_vec_horabloque", ind_vec_horabloque);
	}
	general.getCerrarConexion();
%>
<html>
<head>
<title>Gimnasio - Horario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-Calendario -->
<link type="text/css" rel="stylesheet"
	href="js/dhtml_calendar/dhtml_calendar.css?random=20051112"
	media="screen"></LINK>
<SCRIPT type="text/javascript"
	src="js/dhtml_calendar/dhtml_calendar.js?random=20060118"></script>
<!-Estilos -->
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/centrar.js"></script>
<script src="js/extendida.js"></script>
<script src="js/boton.js"></script>
<script src="js/horario.js"></script>
<script src="js/valida_login.js"></script>
<script language="JavaScript" src="js/trim.js"></script>
<script language="JavaScript" type="text/javascript" src="js/layer.js"></script>
<script LANGUAGE="JavaScript" TYPE="text/javascript">
<%if(soloconsulta) {%>
combo_paralValue = new Array();
combo_paralText  = new Array();
combo_paral      = new Array();
combo_horValue = new Array();
combo_horText  = new Array();
combo_hor_blo1  = new Array();
combo_hor_blo2  = new Array();
combo_hor_cod  = new Array();
combo_hor_par  = new Array();
combo_hor_dia  = new Array();
combo_hor_pref = new Array();
combo_hor_fecha = new Array();
combo_hor_id = new Array();
combo_progValue = new Array();
combo_progText  = new Array();
combo_prog      = new Array();
combo_progCupo  = new Array();
combo_progRama  = new Array();
combo_progProf  = new Array();
combo_progRut  = new Array();
combo_progRutDig  = new Array();
combo_progInscritos  = new Array();
combo_profValue = new Array();
combo_profText  = new Array();
combo_prof  = new Array();
<%
 if(vec_paralelo.size() > 0){
    combo_paralValue.append("\"0\"");
    combo_paralText.append("\"-- Seleccione --\"");
    combo_paral.append("\"0\"");

    for(int i = 0; i < vec_paralelo.size();i++){
        combo_paralValue.append(",\"");
        combo_paralValue.append(((Vector)vec_paralelo.get(i)).get(0));
        combo_paralValue.append("\"");
        combo_paralText.append(",\"");
        combo_paralText.append(((Vector)vec_paralelo.get(i)).get(1));
        if (!(((Vector)vec_paralelo.get(i)).get(2)+"").trim().equals("")) {
        combo_paralText.append(" - ");
        combo_paralText.append(((Vector)vec_paralelo.get(i)).get(2));
        }
        combo_paralText.append("\"");
        combo_paral.append(",\"");
        combo_paral.append(((Vector)vec_paralelo.get(i)).get(1));
        combo_paral.append("\"");
      }
    out.println("");
    out.println("combo_paralValue = new Array("+combo_paralValue+");"); // cod_asign
    out.println("");
    out.println("combo_paralText  = new Array("+combo_paralText+");"); // texto
    out.println("");
   out.println("combo_paral  = new Array("+combo_paral+");"); // paralelo

  }
   // 0 - cod_asign
   // 1 - paralelo
   // 2 - num_dia
   // 3 - cod_bloque_inicio
   // 4 - cod_bloque_termino
   // 5 -bloque_inicio
   // 6 -bloque_termino
   // 7 - dia con palabra
   // 8 - cod_reserva


  if(vec_horario.size() > 0){
    combo_horValue.append("\"0\"");
    combo_horText.append("\"-- Seleccione --\"");
    combo_hor_blo1.append("\"0\"");
    combo_hor_blo2.append("\"0\"");
    combo_hor_cod.append("\"0\"");
    combo_hor_par.append("\"0\"");
    combo_hor_dia.append("\"0\"");
    combo_hor_pref.append("\"0\"");
     for(int i = 0; i < vec_horario.size();i++){
         combo_horValue.append(",\"");
         combo_horValue.append(i+"");
         combo_horValue.append("\"");
         combo_horText.append(",\"");
         combo_horText.append(((Vector)vec_horario.get(i)).get(7)); // dia
         combo_horText.append(" - (");
         combo_horText.append(((Vector)vec_horario.get(i)).get(5));
         combo_horText.append(" al ");
         combo_horText.append(((Vector)vec_horario.get(i)).get(6));
         combo_horText.append(")");
         combo_horText.append("\"");
         combo_hor_blo1.append(",\"");
         combo_hor_blo1.append(((Vector)vec_horario.get(i)).get(3)+"@"+((Vector)vec_horario.get(i)).get(9));
         combo_hor_blo1.append("\"");
         combo_hor_blo2.append(",\"");
         combo_hor_blo2.append(((Vector)vec_horario.get(i)).get(4)+"@"+((Vector)vec_horario.get(i)).get(10));
         combo_hor_blo2.append("\"");
         combo_hor_cod.append(",\"");
         combo_hor_cod.append(((Vector)vec_horario.get(i)).get(0));
         combo_hor_cod.append("\"");
         combo_hor_par.append(",\"");
         combo_hor_par.append(((Vector)vec_horario.get(i)).get(1));
         combo_hor_par.append("\"");
         combo_hor_dia.append(",\"");
         combo_hor_dia.append(((Vector)vec_horario.get(i)).get(2));
         combo_hor_dia.append("\"");
         combo_hor_pref.append(",\"");
         combo_hor_pref.append(((Vector)vec_horario.get(i)).get(8));
         combo_hor_pref.append("\"");
     }
     out.println("");
     out.println("combo_horValue = new Array("+combo_horValue+");"); // indice
     out.println("");
     out.println("combo_horText  = new Array("+combo_horText+");"); // texto
     out.println("");
     out.println("combo_hor_blo1  = new Array("+combo_hor_blo1+");"); // bloque inicio
     out.println("");
     out.println("combo_hor_blo2  = new Array("+combo_hor_blo2+");"); // bloque termino
     out.println("");
     out.println("combo_hor_cod  = new Array("+combo_hor_cod+");"); // cod asign
     out.println("");
     out.println("combo_hor_par  = new Array("+combo_hor_par+");"); // paralelo
     out.println("");
     out.println("combo_hor_dia  = new Array("+combo_hor_dia+");"); // numdia
     out.println("");
     out.println("combo_hor_pref  = new Array("+combo_hor_pref+");"); // preferencia
	 
  }

  if(vec_programacion.size() > 0){
   combo_progValue.append("\"0\"");
   combo_progText.append("\"-- Seleccione --\"");
   combo_prog.append("\"0\"");
   combo_progCupo.append("\"0\"");
   combo_progRama.append("\"0\"");
   combo_progProf.append("\"0\"");
   combo_progRut.append("\"0\"");
   combo_progRutDig.append("\"0\"");
   combo_progInscritos.append("\"0\"");
   for(int i = 0; i < vec_programacion.size();i++){
       combo_progValue.append(",\"");
       combo_progValue.append(((Vector)vec_programacion.get(i)).get(1)); // paralelo
       combo_progValue.append("\"");
       combo_progText.append(",\"");
       combo_progText.append(((Vector)vec_programacion.get(i)).get(1)); // paralelo
       combo_progText.append("\"");
       combo_prog.append(",\"");
       combo_prog.append(((Vector)vec_programacion.get(i)).get(0)); // cod_asign
       combo_prog.append("\"");
       combo_progCupo.append(",\"");
       combo_progCupo.append(((Vector)vec_programacion.get(i)).get(4)); // cupo
       combo_progCupo.append("\"");
       combo_progRama.append(",\"");
       combo_progRama.append(((Vector)vec_programacion.get(i)).get(7)); // rama
       combo_progRama.append("\"");
       combo_progProf.append(",\"");
       combo_progProf.append(((Vector)vec_programacion.get(i)).get(3)); // Prof
       combo_progProf.append("\"");
       combo_progRut.append(",\"");
       combo_progRut.append(((Vector)vec_programacion.get(i)).get(2)); // rut Prof
       combo_progRut.append("\"");
       combo_progRutDig.append(",\"");
       combo_progRutDig.append(((Vector)vec_programacion.get(i)).get(2)); // rut Prof
       combo_progRutDig.append("-");
       combo_progRutDig.append(((Vector)vec_programacion.get(i)).get(5)); // dig_rut Prof
       combo_progRutDig.append("\"");
       combo_progInscritos.append(",\"");
       combo_progInscritos.append(((Vector)vec_programacion.get(i)).get(6)); // inscritos
       combo_progInscritos.append("\"");
   }
   out.println("");
   out.println("combo_progValue = new Array("+combo_progValue+");"); // paralelo
   out.println("");
   out.println("combo_progText  = new Array("+combo_progText+");"); // texto
   out.println("");
   out.println("combo_prog  = new Array("+combo_prog+");"); // cod_asign
   out.println("");
   out.println("combo_progCupo  = new Array("+combo_progCupo+");"); // cupo
   out.println("");
   out.println("combo_progRama  = new Array("+combo_progRama+");"); // rama
   out.println("");
   out.println("combo_progProf  = new Array("+combo_progProf+");"); // prof
   out.println("");
   out.println("combo_progRut  = new Array("+combo_progRut+");"); // rut
   out.println("");
   out.println("combo_progRutDig  = new Array("+combo_progRutDig+");"); // dig rut
   out.println("");
   out.println("combo_progInscritos  = new Array("+combo_progInscritos+");"); // inscritos
  }
  if(vec_prof.size() > 0){
   combo_profValue.append("\"0\"");
   combo_profText.append("\"-- Seleccione --\"");
   combo_prof.append("\"-- Seleccione --\"");

   for(int i = 0; i < vec_prof.size();i++){
       combo_profValue.append(",\"");
       combo_profValue.append(((Vector)vec_prof.get(i)).get(0));
       combo_profValue.append("\"");
       combo_profText.append(",\"");
       combo_profText.append(((Vector)vec_prof.get(i)).get(0));
       combo_profText.append(" - ");
       combo_profText.append(((Vector)vec_prof.get(i)).get(1));
       combo_profText.append("\"");
       combo_prof.append(",\"");
       combo_prof.append(((Vector)vec_prof.get(i)).get(2));
       combo_prof.append("\"");
     }
   out.println("");
   out.println("combo_profValue = new Array("+combo_profValue+");"); // rut
   out.println("");
   out.println("combo_profText  = new Array("+combo_profText+");"); // rut_dig
   out.println("");
   out.println("combo_prof  = new Array("+combo_prof+");"); // nombre

  }
%>


function noenter() {
  return !(window.event && window.event.keyCode == 13);
}


function acceptNum(evt){
 if(window.event) key = evt.keyCode;
 else key = evt.which;

 return (key <= 13 || (key >= 48 && key <= 57));
}

function suma( ){
  if (document.getElementById('f2_cupo').value != "" && document.getElementById('f2_rama').value != "")
       document.getElementById('f2_normal').innerHTML = document.getElementById('f2_cupo').value - document.getElementById('f2_rama').value;
}
  function enter(e){
    if (window.event) {
      if (window.event.keyCode == 13)
             validar(document.fingresar2);

    }
    else {
      if (e.which == 13)
           validar(document.fingresar2);
    }
  }
  function validar(form){

          if(trim(form.rut_aux.value)==""){
                          alert("Debe ingresar el RUT a registrar");
                          form.rut_aux.focus();
                          return false;
                  }
                   if (!valida_rut2(form.rut_aux, form.rutnum, form.dvrut, 'RUT')) {
                          form.rut_aux.focus();
                          return false;
                  }
                 // return true;
        rut_ind = -1;
         for (i=0;i<combo_profValue.length;i++){
           if (combo_profValue[i] == form.rutnum.value)
              rut_ind = i;
         }
          if (rut_ind < 0) {
            alert ('Este Rut no se encuentra registrado en el Departamento');
            form.rut_aux.value = "";
            form.rut_aux.focus();
            return false;

          } else {
            form.rutnum.value = combo_profValue[rut_ind];
            }

       //   form.submit();
	}
<%}%>
  function Mensaje(){
   <%if(error<0){%>alert ('<%=mensaje%>');<%}%>
  }
function preCargaImg(imageURL) {
                image = new Image();
                image.src = imageURL;
                return image;
}
Pesta�aBordeIzq = preCargaImg("imagen/etiq_baseBORDEIZQ.gif");
Pesta�aBordeDer = preCargaImg("imagen/etiq_baseBORDEDER.gif");
Pesta�aBordeIzq_Des = preCargaImg("imagen/etiq_baseBORDEIZQ_des.gif");
Pesta�aBordeDer_Des = preCargaImg("imagen/etiq_baseBORDEDER_des.gif");

function CambiaIMG(i) {
  for (var j=1 ; j <= 3 ; j++){
    if (i == j) {
      // activo i
      document ['IMG'+j+'_BordeIzq'].src = Pesta�aBordeIzq.src;
      document ['IMG'+j+'_BordeDer'].src = Pesta�aBordeDer.src;
      document.getElementById('IMG'+j+'_Fondo').style.background="url(imagen/etiq_baseFONDO.gif)";

    }
    else {
      if(<%=soloconsulta%>){
      document ['IMG'+j+'_BordeIzq'].src = Pesta�aBordeIzq_Des.src;
      document ['IMG'+j+'_BordeDer'].src = Pesta�aBordeDer_Des.src;
      document.getElementById('IMG'+j+'_Fondo').style.background="url(imagen/etiq_baseFONDO_des.gif)";
      }
    }
  }
}

function escondeBloqueTodo() {
 if (<%=sala%> > 0 )
  Bloque(1);
}

function Bloque(opcion) {
  if (opcion == 1){
    escondeBloque('BLOQUE_Horario', 2); // muestra
    escondeBloque('BLOQUE_BTN_imprimir', 2); // muestra botones
    escondeBloque('BLOQUE_BTN_copiar', 1); // esconde botones

    <%if(soloconsulta) {%>
    escondeBloque('BLOQUE_MantHorario', 1); // escondo
    escondeBloque('BLOQUE_MantActividad', 1); // escondo
    escondeBloque('BLOQUE_BTN_confirmar', 1); // esconde botones
    escondeBloque('BLOQUE_BTN_copiar', 1); // muestra botones
    escondeBloque('BLOQUE_BTN_confirmar2', 1); // esconde botones
    escondeBloque('BLOQUE_BTN_eliminar', 1); // esconde botones
    escondeBloque('BLOQUE_BTN_eliminar2', 1); // esconde botones
    <%}if(soloconsulta) {%>
    CambiaIMG(1); // activo primera pesta�a
    <%}%>
  }
  if (opcion == 2){
    escondeBloque('BLOQUE_Horario', 1); // escondo
    escondeBloque('BLOQUE_MantHorario', 2); // muestra
    escondeBloque('BLOQUE_MantActividad', 1); // escondo
    escondeBloque('BLOQUE_BTN_imprimir', 1); // esconde botones
    escondeBloque('BLOQUE_BTN_copiar', 2); // esconde botones
    escondeBloque('BLOQUE_BTN_confirmar', 2);  // muestra botones
    escondeBloque('BLOQUE_BTN_confirmar2', 1);  // esconde boton confirmar
    escondeBloque('BLOQUE_BTN_eliminar', 2); // esconde botones
    escondeBloque('BLOQUE_BTN_eliminar2', 1); // esconde botones

    CambiaIMG(2); // activo segunda pesta�a
  }
  if (opcion == 3){
    escondeBloque('BLOQUE_Horario', 1); // escondo
    escondeBloque('BLOQUE_MantHorario', 1); // escondo
    escondeBloque('BLOQUE_MantActividad', 2); // muestro
    escondeBloque('BLOQUE_BTN_imprimir', 1); // esconde botones
    escondeBloque('BLOQUE_BTN_copiar', 1); // esconde botones
    escondeBloque('BLOQUE_BTN_confirmar', 1);  // esconde botone confirmar
    escondeBloque('BLOQUE_BTN_confirmar2', 2);  // muestra boton confirmar
    escondeBloque('BLOQUE_BTN_eliminar', 1); // esconde botones
    escondeBloque('BLOQUE_BTN_eliminar2', 2); // esconde botones
    if (document.fingresar2.actividad.value == 1410020 || document.fingresar2.actividad.value == 1410017){
       escondeBloque('BLOQUE_BTN_copiar', 1); // esconde botones
       escondeBloque('BLOQUE_BTN_confirmar2', 1);  // esconde  botones
       escondeBloque('BLOQUE_BTN_eliminar2', 1); // esconde botones

    }
    CambiaIMG(3); // activo tercera pesta�a
  }
}

function AbrirVentana(f,a) {
  var alto  = 550;
  var ancho = 700;
  f.target = "ventana";
  f.action = a;
  var par = 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=yes';
  var w = Centrar03(f.target,alto,ancho,par);
  w.focus();
  f.submit();
}

function Info(t,opcion,prof){
  var texto = "";
  switch (opcion){
      case "-1": texto = "Libre";
        break;
      case "0": texto = "ASEO<br>"+prof;
        break;
      case "1": texto = "Alumnos<br>"+prof;
        break;
      case "2": texto = "Funcionarios<br>"+prof;
        break;
      case "3": texto = "Rector�a<br>"+prof;
        break;
      case "4": texto = "Directores<br>"+prof;
        break;
      case "5": texto = "Docencia<br>"+prof;
      	break;  
      case "6": texto = "Rama<br>"+prof;
        break;
      case "7": texto = "Extracurricular<br>"+prof;
        break;  
      default : texto = opcion+"<br>"+prof;
        break;
 }
  CambiaPuntero(t,'default');
  Tip(texto,'#fefdd6');
}

function RevisaActividad(t){
  if (t.value == -2) DibujaInput(0); // Seleccione
  if (t.value ==  0) DibujaInput(3); // Aseo
  if (t.value >   0) DibujaInput(1); // Actividad
  return;
}
<%if(soloconsulta) {%>
function DibujaInput(opcion){
  var linea1_celda1 = "";
  var linea1_celda2 = "";
  var linea2_celda1 = "";
  var linea2_celda2 = "";
  var linea3_celda1 = "";
  var linea3_celda2 = "";
  var linea4_celda1 = "";
  var linea4_celda2 = "";
  var linea5_celda1 = "";
  var linea5_celda2 = "";
  par = "";
  

  

  
  var texto_par  = " <span class='Estilo_Gris'>Paralelo</span>";
  for (k=0;k <= <%=vec_paralelo.size()%>;k++){
      if (combo_paralValue[k] == document.fingresar3.actividad.value)
      par = par +  "<option value='"+combo_paral[k]+"'>"+combo_paralText[k]+"</option>";
  }
  var input_par  = "<select name='f3_paralelo' class='Option' id='f3_paralelo' onchange='CargaParalelo();'>"+
                   par + " </select>";
  if(par == "") input_par = "<span class=\"Estilo_Rojo\">No hay Paralelos asociados</span>";
  var texto_pref = " <span class='Estilo_Gris'>Preferencia</span>";
  var input_pref = " <select name='f3_preferencia' class='Option' id='f3_preferencia' onchange='CargaParalelo();'>" ;
  if (document.fingresar3.actividad.value == 1410020 || document.fingresar3.actividad.value == 1410017 )
    input_pref   += "<option value='5'>DOCENCIA </option></select>";
 else {
  <%for (int i=0;i < vec_tipo_reserva.size();i++){%>
                input_pref += " <option value='<%=((Vector)vec_tipo_reserva.get(i)).get(0)%>'><%=((Vector)vec_tipo_reserva.get(i)).get(1)%></option>" ;
    <%}%>
     input_pref   += " </select>";
 }
  var texto_dia  = " <span class='Estilo_Gris'>D�a</span>";
  var input_dia  = " <select name='f3_dia1' class='Option' id='f3_dia1'>" +
                   " <option value='0'>-- Seleccione --</option>" +
                   " <option value='1' selected>Lunes</option>" +
                   " <option value='2'>Martes</option>" +
                   " <option value='3'>Mi&eacute;rcoles</option>" +
                   " <option value='4'>Jueves</option>" +
                   " <option value='5'>Viernes</option>" +
                   " <option value='6'>S&aacute;bado</option>" +
                   " </select>" +
                   " <span class='Estilo_Gris'>a&nbsp;</span>" +
                   " <select name='f3_dia2' class='Option' id='f3_dia2'>" +
                   " <option value='0'>-- Seleccione --</option>" +
                   " <option value='1' selected>Lunes</option>" +
                   " <option value='2'>Martes</option>" +
                   " <option value='3'>Mi&eacute;rcoles</option>" +
                   " <option value='4'>Jueves</option>" +
                   " <option value='5'>Viernes</option>" +
                   " <option value='6'>S&aacute;bado</option>" +
                   " </select>";
  var texto_blo = " <span class='Estilo_Gris'>Bloque</span>";
  var input_blo = " <select name='f3_bloque1' class='Option' id='f3_bloque1'>" +
                  " <option value='0' selected>-- Seleccione --</option>" +
                  <% for(int i=0;i<vec_horabloque1.size();i++){%>
                  " <option value='<%=((Vector)vec_horabloque1.get(i)).get(0)+"@"+((Vector)vec_horabloque1.get(i)).get(4)%>'" +
                  " ><%=((Vector)vec_horabloque1.get(i)).get(1)+""%> -" +
                  " <%=((Vector)vec_horabloque1.get(i)).get(2)+""%></option>" +
                  <%}%>
                  " </select>" +
                  " <span class='Estilo_Gris'> a&nbsp;</span>" +
                  " <select name='f3_bloque2' class='Option' id='f3_bloque2'>" +
                  " <option value='0' selected>-- Seleccione --</option>" +
                   <% for(int i=0;i<vec_horabloque1.size();i++){%>
                  " <option value='<%=((Vector)vec_horabloque1.get(i)).get(0)+"@"+((Vector)vec_horabloque1.get(i)).get(4)%>'" +
                  " ><%=((Vector)vec_horabloque1.get(i)).get(1)+""%> -" +
                  " <%=((Vector)vec_horabloque1.get(i)).get(2)+""%></option>" +
                  <%}%>
                  " </select>";
  var texto_fe1 = " <span class='Estilo_Gris'>Vigencia</span>";
  var input_fe1 = " <table width='100%'  border='0' cellspacing='0' cellpadding='0'>" +
                  " <tr><td width='23%'>" +
                  " <input id='f3_fecha_inicio' name='f3_fecha_inicio' type='text' class='Input01' value='<%=fecha%>' size='10' maxlength='14'></td>" +
                  " <td width='77%' valign='bottom'>" +
                " <a href=\"javascript:void(0)\" onclick=\"displayCalendar(document.getElementById('f3_fecha_inicio'),'dd/mm/yyyy',this)\">" +
                  " <img src=\"imagen/gim_calendario_verde.gif\" border=\"0\" alt=\"CALENDARIO\" width=\"16\" height=\"16\"></a>" +
                  " </td></tr></table>";
  var texto_fe2 = " <span class='Estilo_Gris'>hasta</span>";
  var input_fe2 = " <table width='100%'  border='0' cellspacing='0' cellpadding='0'>" +
                  " <tr><td width='23%'>" +
                 " <input id='f3_fecha_termino' name='f3_fecha_termino' type='text' class='Input01' size='10' maxlength='14'></td>" +
                  " <td width='77%' valign='bottom'>" +
                  " <a href=\"javascript:void(0)\" onclick=\"displayCalendar(document.getElementById('f3_fecha_termino'),'dd/mm/yyyy',this)\">" +
                  " <img src=\"imagen/gim_calendario_verde.gif\" border=\"0\" alt=\"CALENDARIO\" width=\"16\" height=\"16\"></a>" +
                  " </td></tr></table>";



//}

  // dibuja paralelo, preferencia, d�a y bloque
  if (opcion == 1) { // actividad
    linea1_celda1 = texto_par;
    linea1_celda2 = input_par;
    linea2_celda1 = texto_pref;
    linea2_celda2 = input_pref;
    linea3_celda1 = texto_dia;
    linea3_celda2 = input_dia;
    linea4_celda1 = texto_blo;
    linea4_celda2 = input_blo;

  }
  // dibuja dia, bloque y fecha
  if (opcion == 3) { // aseo
    linea1_celda1 = texto_dia;
    linea1_celda2 = input_dia;
    linea2_celda1 = texto_blo;
    linea2_celda2 = input_blo;
    linea3_celda1 = texto_fe1;
    linea3_celda2 = input_fe1;
    linea4_celda1 = texto_fe2;
    linea4_celda2 = input_fe2;
    document.getElementById('TB_linea2_celda1').innerHTML = linea1_celda1;
    document.getElementById('TB_linea2_celda2').innerHTML = linea1_celda2;
    document.getElementById('TB_linea5_celda1').innerHTML = linea2_celda1;
    document.getElementById('TB_linea5_celda2').innerHTML = linea2_celda2;
    document.getElementById('TB_linea3_celda1').innerHTML = linea3_celda1;
    document.getElementById('TB_linea3_celda2').innerHTML = linea3_celda2;
    document.getElementById('TB_linea4_celda1').innerHTML = linea4_celda1;
    document.getElementById('TB_linea4_celda2').innerHTML = linea4_celda2;

  } else {

  document.getElementById('TB_linea1_celda1').innerHTML = linea1_celda1;
  document.getElementById('TB_linea1_celda2').innerHTML = linea1_celda2;
  document.getElementById('TB_linea2_celda1').innerHTML = linea2_celda1;
  document.getElementById('TB_linea2_celda2').innerHTML = linea2_celda2;
  document.getElementById('TB_linea3_celda1').innerHTML = linea3_celda1;
  document.getElementById('TB_linea3_celda2').innerHTML = linea3_celda2;
  document.getElementById('TB_linea4_celda1').innerHTML = linea4_celda1;
  document.getElementById('TB_linea4_celda2').innerHTML = linea4_celda2;
  document.getElementById('TB_linea5_celda1').innerHTML = linea2_celda1;
  document.getElementById('TB_linea5_celda2').innerHTML = linea2_celda2;
  }
   if (opcion > 0)
      CargaParalelo();
//if (opcion == 3) {
//  Calendar.setup({inputField : 'fecha_inicio', ifFormat : '%d/%m/%Y', button : 'lanzador1'});
//  Calendar.setup({inputField : 'fecha_termino', ifFormat : '%d/%m/%Y', button : 'lanzador2'});

}

function CargaParalelo(){

  par = "";
  var texto_hor  = " <span class='Estilo_Gris'>Horario</span>";
  document.fingresar3.paralelo.value = document.getElementById('f3_paralelo') != null?
                                       document.getElementById('f3_paralelo').value:0;
                                       
  document.fingresar3.preferencia.value = document.getElementById('f3_preferencia') != null?
                                          document.getElementById('f3_preferencia').value:0;

  for (k=0;k <= <%=vec_horario.size()%>;k++){
  if ((document.fingresar3.actividad.value == 0 && combo_hor_cod[k] == 0) ||
      (combo_hor_cod[k] == document.fingresar3.actividad.value &&
      combo_hor_par[k] == document.fingresar3.paralelo.value &&
      combo_hor_pref[k] == document.fingresar3.preferencia.value)){
        fec = "";
    if (combo_hor_cod[k] == 0){
      ind = combo_hor_pref[k].indexOf("@");
  //    fec = " - "+  combo_hor_pref[k].substr(0,ind);
    }
  par = par +  "<option value='"+k+"'>"+combo_horText[k]+"</option>";
  }
  }
  var input_hor= "<select name='f3_horario' class='Option' id='f3_horario' onchange='CargaHorario();'>" +
                 par + " <option value='-1'>-- Nuevo --</option>" + " </select>";
   linea5_celda1 = texto_hor;
  linea5_celda2 = input_hor;

  if (document.fingresar3.actividad.value == 0) { // aseo
    document.getElementById('TB_linea1_celda1').innerHTML = linea5_celda1;
    document.getElementById('TB_linea1_celda2').innerHTML = linea5_celda2;
  } else {
    document.getElementById('TB_linea5_celda1').innerHTML = linea5_celda1;
    document.getElementById('TB_linea5_celda2').innerHTML = linea5_celda2;
  }
  CargaHorario();
}

function CargaHorario(){
    document.fingresar3.horario.value = document.getElementById('f3_horario').value; 									
    document.fingresar3.bloque1.value = document.getElementById('f3_bloque1').value;
    document.fingresar3.bloque2.value = document.getElementById('f3_bloque2').value;
    document.fingresar3.dia1.value = document.getElementById('f3_dia1').value;
    document.fingresar3.dia2.value = document.getElementById('f3_dia2').value;
    if (document.fingresar3.fecha_inicio != null && document.getElementById('f3_fecha_inicio') != null)
    document.fingresar3.fecha_inicio.value = document.getElementById('f3_fecha_inicio').value;
    if (document.fingresar3.fecha_termino != null && document.getElementById('f3_fecha_termino') != null)
    document.fingresar3.fecha_termino.value = document.getElementById('f3_fecha_termino').value;
    
    if (document.fingresar3.actividad.value == 0){ // para el caso de aseo

    val = document.fingresar3.horario.value;
  
    if(val > 0){
      ind = combo_hor_dia[val].indexOf("@");
      dia1 = combo_hor_dia[val].substr(0,ind);
      dia2 = combo_hor_dia[val].substr(ind+1,combo_hor_dia[val].length);
      ind = combo_hor_pref[val].indexOf("@");
      fec = combo_hor_pref[val].substr(0,ind);
      id = combo_hor_pref[val].substr(ind+1,combo_hor_pref[val].length);
      if (document.fingresar3.fecha_inicio != null && document.getElementById('f3_fecha_inicio') != null) {
         document.fingresar3.fecha_inicio.value = fec;
         document.getElementById('f3_fecha_inicio').value = fec;
      }
      document.fingresar3.id_aseo.value = id;
      document.fingresar3.bloque1.value = combo_hor_blo1[val];
      document.getElementById('f3_bloque1').value = combo_hor_blo1[val];
      document.fingresar3.bloque2.value = combo_hor_blo2[val];
      document.getElementById('f3_bloque2').value = combo_hor_blo2[val];
      document.fingresar3.dia1.value = dia1;
      document.getElementById('f3_dia1').value = dia1;
      document.fingresar3.dia2.value = dia2;
      document.getElementById('f3_dia2').value = dia2;
    } else {
        if (document.fingresar3.fecha_inicio != null && document.getElementById('f3_fecha_inicio') != null) {
           document.fingresar3.fecha_inicio.value =  '<%=fecha%>';
           document.getElementById('f3_fecha_inicio').value =  '<%=fecha%>';
        }
        document.fingresar3.bloque1.value = combo_hor_blo1[0];
        document.getElementById('f3_bloque1').value = combo_hor_blo1[0];
        document.fingresar3.bloque2.value = combo_hor_blo2[0];
        document.getElementById('f3_bloque2').value = combo_hor_blo2[0];
        document.fingresar3.dia1.value = 0;
        document.getElementById('f3_dia1').value = 0;
        document.fingresar3.dia2.value = 0;
        document.getElementById('f3_dia2').value = 0;
    }

  }else {
    indice = document.fingresar3.horario.value;

    if (indice == -1){
      document.fingresar3.bloque1.value = combo_hor_blo1[0];
      document.getElementById('f3_bloque1').value = combo_hor_blo1[0];
      document.fingresar3.bloque2.value = combo_hor_blo2[0];
      document.getElementById('f3_bloque2').value = combo_hor_blo2[0];
      document.fingresar3.dia1.value = 0;
      document.getElementById('f3_dia1').value = 0;
      document.fingresar3.dia2.value = 0;
      document.getElementById('f3_dia2').value = 0;
  } else {
  document.fingresar3.bloque1.value = combo_hor_blo1[indice];
  document.getElementById('f3_bloque1').value = combo_hor_blo1[indice];
  document.fingresar3.bloque2.value = combo_hor_blo2[indice];
  document.getElementById('f3_bloque2').value = combo_hor_blo2[indice];
  document.fingresar3.dia1.value = combo_hor_dia[indice];
  document.getElementById('f3_dia1').value = combo_hor_dia[indice];
  document.fingresar3.dia2.value = combo_hor_dia[indice];
  document.getElementById('f3_dia2').value = combo_hor_dia[indice];
  }
  }
 // CargaProg();
}
function CargaProg(){
  if (document.fingresar2.actividad.value == 1410020 || document.fingresar2.actividad.value == 1410017){
    escondeBloque('BLOQUE_BTN_confirmar2', 1);  // esconde botones
    escondeBloque('BLOQUE_BTN_eliminar2', 1); // esconde botones
  } else {
    escondeBloque('BLOQUE_BTN_confirmar2', 2);  // muestra botones
    escondeBloque('BLOQUE_BTN_eliminar2', 2); //muestra botones

  }
  var BActiv_linea1_celda1 = "<span class=\"Estilo_Gris\">Paralelo</span>";
  var par = "";
  var sel = "";
    for (k=0;k <= <%=vec_programacion.size()%>;k++){
      sel = "";
      if ((document.fingresar2.actividad.value == 0 && combo_prog[k] == 0) ||
          (combo_prog[k] == document.fingresar2.actividad.value )){
          if ( combo_prog[k] == document.fingresar2.actividad.value)
              sel = "selected";
          par = par +  "<option value='"+combo_progValue[k]+"' "+sel+">"+combo_progText[k]+"</option>";
      
    }
    
    }


  var BActiv_linea1_celda2 = "<select name=\"f2_paralelo\" id=\"f2_paralelo\" class=\"Option\" onchange= \"CargaParProgHor();\">" +
                par;
  if (document.fingresar2.actividad.value == 1410020 || document.fingresar2.actividad.value == 1410017)
   BActiv_linea1_celda2  += "</select>";
  else
     BActiv_linea1_celda2 += " <option value='-1'>-- Nuevo --</option>" + " </select>";
  document.getElementById('BActiv_linea1_celda1').innerHTML = BActiv_linea1_celda1;
  document.getElementById('BActiv_linea1_celda2').innerHTML = BActiv_linea1_celda2;
  if ((document.fingresar2.actividad.value == 1410020 || document.fingresar2.actividad.value == 1410017) && par == ""){
    document.getElementById('BActiv_linea1_celda1').innerHTML = "";
    document.getElementById('BActiv_linea1_celda2').innerHTML = "<span class=\"Estilo_Rojo\">No existe programaci�n para este per�odo</span>";
    document.getElementById('BActiv_linea2_celda1').innerHTML = "";
    document.getElementById('BActiv_linea2_celda2').innerHTML = "";
    document.getElementById('BActiv_linea3_celda1').innerHTML = "";
    document.getElementById('BActiv_linea3_celda2').innerHTML = "";
    document.getElementById('BActiv_linea4_celda1').innerHTML = "";
    document.getElementById('BActiv_linea4_celda2').innerHTML = "";
  } else  CargaParProgHor();
}
function CargaParProgHor(){
 paralelo = 0;
 normal = 0;
 document.fingresar2.paralelo.value = document.getElementById('f2_paralelo').value;
 if (document.fingresar2.paralelo)
 paralelo = document.fingresar2.paralelo.value;

  var rut = 0;
  salir = true;
  lectura = "";
  ind_cupo = -1;
   if (document.fingresar2.actividad.value == 1410020 || document.fingresar2.actividad.value == 1410017) // para que no se modifique la programaci�n de esta asignatura
     lectura = "readonly";
  for (k=0;k <= <%=vec_programacion.size()%> && salir;k++){
    /*if (((document.fingresar2.actividad.value == 0 && combo_prog[k] == 0) ||
         (combo_prog[k] == document.fingresar2.actividad.value ) ) &&
         combo_progValue[k] == indice){*/
    if ((document.fingresar2.actividad.value == 0 && combo_prog[k] == 0) ||
         (combo_prog[k] == document.fingresar2.actividad.value  &&
         combo_progValue[k] == paralelo)){
    rut = combo_progRut[k];
    ind_cupo = k;
    salir = false;
  }
  }

  if (ind_cupo>0) {

    cupo = combo_progCupo[ind_cupo];
    rama =  combo_progRama[ind_cupo];
    normal = (cupo/1) - (rama/1);

  } else {
    cupo = "";
    rama = "";
    normal = "";
  }
  var BActiv_linea2_celda1 = " <span class=\"Estilo_Gris\">Profesor&nbsp;</span> ";
  var BActiv_linea2_celda2 = " <select name=\"f2_rutnum\" id=\"f2_rutnum\" class=\"Option\">";
   if (document.fingresar2.actividad.value != 1410020 && document.fingresar2.actividad.value != 1410017)
           BActiv_linea2_celda2 += "<option value=\"0\">-- Seleccione --</option>";

  var select = "";
  for(i = 1; i < combo_profValue.length ;i++){
    if (combo_profValue[i] == rut) {
      BActiv_linea2_celda2 +=  " <option value=\""+combo_profValue[i]+ "\" selected>"+combo_prof[i]+ "</option>" ;
      document.fingresar2.rutnum.value = combo_profValue[i];
     }
     else
       if (document.fingresar2.actividad.value != 1410020 && document.fingresar2.actividad.value != 1410017)
           BActiv_linea2_celda2 +=  " <option value=\""+combo_profValue[i]+ "\" >"+combo_prof[i]+ "</option>" ;

   }
    BActiv_linea2_celda2 +=  "</select>";
    

     
  var BActiv_linea3_celda1 = "<span class=\"Estilo_Gris\">Cupo</span>";
  var BActiv_linea3_celda2 = "<span class=\"Estilo_Gris\">Total</span>&nbsp;&nbsp;<input name=\"f2_cupo\" id=\"f2_cupo\" type=\"text\" class=\"Input01\" onKeyPress=\"return acceptNum(event);\" onKeyUp=\"suma()\"" +
                             "id=\"f2_cupo\" value=\""+cupo+"\" size=\"3\" maxlength=\"3\""+lectura+">";

  if (lectura == "")
       BActiv_linea3_celda2 +="&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"Estilo_Gris\">Rama</span>&nbsp;&nbsp;" +
                             "<input name=\"f2_rama\" type=\"text\" class=\"Input01\" id=\"f2_rama\" onKeyPress=\"return acceptNum(event);\" onKeyUp=\"suma()\"" +
                             "value=\""+rama+"\" size=\"3\" maxlength=\"3\""+lectura+">"+
                             "&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"Estilo_Gris\" >Normal&nbsp;&nbsp;</span>" +
                             "<span class=\"Estilo_Gris\" id=\"f2_normal\">" +normal + "</span>";
  var BActiv_linea4_celda1 = "";
  var BActiv_linea4_celda2 = "";
  if (document.fingresar2.actividad.value == 1410020 || document.fingresar2.actividad.value == 1410017){
  BActiv_linea4_celda1 = "<span class=\"Estilo_Gris\">Inscritos</span>";
  BActiv_linea4_celda2 = "<span class=\"Estilo_Gris\">" + combo_progInscritos[ind_cupo]+"</span>";
  BActiv_linea4_celda2 += "&nbsp;&nbsp;&nbsp;&nbsp;<a HREF=\"#\" onClick=\"AbrirVentana(document.flistado,'gim_genera_list.jsp')\">Lista de Curso</a>";
} else {
  BActiv_linea4_celda1 = "";
  BActiv_linea4_celda2 = "";

    }
  if(paralelo >= 0 && document.fingresar2.actividad.value != 1410020 && document.fingresar2.actividad.value != 1410017) {

            BActiv_linea2_celda2 =  "<option value=\"0\">-- Seleccione --</option>";
    var select = "";
    for(i = 1; i < combo_profValue.length ;i++){
       if ((combo_profValue[i] == rut)) {
         BActiv_linea2_celda2 +=  " <option value=\""+combo_profValue[i]+ "\" selected>"+combo_prof[i]+ "</option>" ;
      }
      else
           BActiv_linea2_celda2 +=  " <option value=\""+combo_profValue[i]+ "\" "+select+">"+combo_prof[i]+ "</option>" ;
    select = "";
    }
    BActiv_linea2_celda2 = " <select name=\"f2_rutnum\" id=\"f2_rutnum\" class=\"Option\">" + BActiv_linea2_celda2 +  "</select>";
    BActiv_linea3_celda1 = "<span class=\"Estilo_Gris\">Cupo</span>";
    BActiv_linea3_celda2 = "<span class=\"Estilo_Gris\">Total</span>&nbsp;&nbsp;<input name=\"f2_cupo\" type=\"text\" class=\"Input01\" id=\"f2_cupo\" onKeyPress=\"return acceptNum(event);\" onKeyUp=\"suma()\"" +
                           "value=\""+combo_progCupo[ind_cupo]+"\" size=\"3\" maxlength=\"3\""+lectura+">";
							
     if (lectura == "")
    BActiv_linea3_celda2 +="&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"Estilo_Gris\">Rama</span>&nbsp;&nbsp;" +
                            "<input name=\"f2_rama\" type=\"text\" class=\"Input01\" id=\"f2_rama\" onKeyPress=\"return acceptNum(event);\" onKeyUp=\"suma()\"" +
                             "value=\""+rama+"\" size=\"3\" maxlength=\"3\""+lectura+">" +
                             "&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"Estilo_Gris\" >Normal&nbsp;&nbsp;</span>" +
                             "<span class=\"Estilo_Gris\" id=\"f2_normal\">" +normal + "</span>";
    BActiv_linea4_celda1 = "";
    BActiv_linea4_celda2 = "";

  }
  document.getElementById('BActiv_linea2_celda1').innerHTML = BActiv_linea2_celda1;
  document.getElementById('BActiv_linea2_celda2').innerHTML = BActiv_linea2_celda2;
  document.getElementById('BActiv_linea3_celda1').innerHTML = BActiv_linea3_celda1;
  document.getElementById('BActiv_linea3_celda2').innerHTML = BActiv_linea3_celda2;
  document.getElementById('BActiv_linea4_celda1').innerHTML = BActiv_linea4_celda1;
  document.getElementById('BActiv_linea4_celda2').innerHTML = BActiv_linea4_celda2;
  


  if(ind_cupo > 0) {
  document.flistado.cod_asign.value = combo_prog[ind_cupo];
  document.flistado.paralelo.value = combo_progValue[ind_cupo];
  }
}

<%}%>
function Confirmar(f, accion){
  //alert('accion: '+accion);
  if (document.getElementById('f2_rama')){
  document.fingresar2.cupo.value = document.getElementById('f2_cupo').value;}
  if (document.getElementById('f2_rama')){
  document.fingresar2.rama.value = document.getElementById('f2_rama').value;}
  if (accion == 2) {{
   if (f.rutnum != null && document.getElementById('f2_rutnum') != null)
   f.rutnum.value = document.getElementById('f2_rutnum').value;}
   if (f.rutnum.value == 0){
	   alert('Debe seleccionar un Profesor\nSi el profesor no aparece en la lista seleccione al profesor NN');
	   f.rut_aux.focus();
	   return false;
   }
  }
  
  if (f.actividad.value == -2){
  alert('Debe seleccionar una Actividad');
  f.actividad.focus();
  return false;
  }
  if (accion == 1 || accion == 3) { // es mantenedor de horario
    if (f.actividad.value == 0 ){ // aseo
      f.horario.value = document.getElementById('f3_horario').value;
      if(f.horario.value == '0') {
        alert('Debe seleccionar Horario');
        document.getElementById('f3_horario').focus();
        return false;
      }
    }
   if (accion == 3){
     f.horario.value = document.getElementById('f3_horario').value;
     if(f.horario.value == -1) {
       alert('No se puede Eliminar Horario Nuevo');
       document.getElementById('f3_horario').focus();
       return false;
     }
   }
    if (accion == 1){
    f.dia1.value = document.getElementById('f3_dia1').value;
    f.dia2.value = document.getElementById('f3_dia2').value;
    f.bloque1.value = document.getElementById('f3_bloque1').value;
    f.bloque2.value = document.getElementById('f3_bloque2').value;
    
    if (f.fecha_inicio != null && document.getElementById('f3_fecha_inicio') != null)
    f.fecha_inicio.value = document.getElementById('f3_fecha_inicio').value;
    if (f.fecha_termino != null && document.getElementById('f3_fecha_termino') != null)
    f.fecha_termino.value = document.getElementById('f3_fecha_termino').value;
    
    if(f.dia1.value == 0) {
      alert('Debe seleccionar el primer d�a');
      document.getElementById('f3_dia1').focus();
      return false;
    }
    if(f.dia2.value == 0) {
      alert('Debe seleccionar el segundo d�a');
      document.getElementById('f3_dia2').focus();
      return false;
    }
    if(f.dia2.value < f.dia1.value) {
      alert('El primer d�a debe ser menor al segundo d�a');
      document.getElementById('f3_dia1').focus();
      return false;
    }

    if(f.bloque2.value == 0 || f.bloque1.value == 0 ) {
      alert('Debe seleccionar bloque');
      if (f.bloque1.value == 0)
        document.getElementById('f3_bloque1').focus();
      else
        document.getElementById('f3_bloque2').focus();
      return false;
    }
    
    var b1int = parseInt(f.bloque1.value,10);
    var b2int = parseInt(f.bloque2.value,10);
   
    var separador = f.bloque1.value.split("@");
    var separador2 = f.bloque2.value.split("@");

	var sinicial = 0;
	var stermino = 0;

    sinicial = separador[1];
    stermnino = separador2[1];

    if (parseInt(sinicial,10) > parseInt(stermnino,10)) {

      alert('El primer bloque debe ser menor al segundo bloque');
      document.getElementById('f3_bloque1').focus();
      return false;
    }
    f.bloque1.value = b1int;
    f.bloque2.value = b2int;
    if (f.actividad.value == 0 ){ // aseo
      if (f.fecha_inicio.value == ""){
        alert('Debe ingresar fecha de inicio de vigencia');
        return false;
      }

      if (f.fecha_termino.value != '') {
        var pregunta = "Ud. va a terminar la vigencia del Aseo.\n� Desea continuar ?";
        var respuesta = confirm(pregunta);
        if (respuesta == false)  return false;
      }
      if (f.fecha_inicio.value != "" && f.fecha_termino.value != ""){
        fecha_i = f.fecha_inicio.value;
        fecha_t = f.fecha_termino.value;
        d = fecha_i.substr(0,2)+"";
        m = fecha_i.substr(3,2)+"";
        a = fecha_i.substr(6,4)+"";
        fecha_inicio = a+m+d+"";
        d = fecha_t.substr(0,2)+"";
        m = fecha_t.substr(3,2)+"";
        a = fecha_t.substr(6,4)+"";
        fecha_termino = a+m+d+"";
        if ((fecha_termino/1) < (fecha_inicio/1)){
          alert('La fecha de inicio de vigencia debe ser menor a la de t�rmino' );
          return false;
        }
      }
    }
    }
//     else {
//      if(f.horario.value == -1) {
//        alert('Este Horario es nuevo no puede ser eliminado');
//        f.horario.focus();
//        return false;
//      }
//    }
  }
  borr = "";
  if (accion == 3 || accion == 4){ // es eliminar
    if (accion == 3) borr = "el Horario";
    else borr = "la Actividad";
    if (document.getElementById('f2_paralelo')!= null)
       f.paralelo.value = document.getElementById('f2_paralelo').value;
    else 
       if (document.getElementById('f3_paralelo')!= null)
       f.paralelo.value = document.getElementById('f3_paralelo').value;   
    if ((accion == 3 && f.actividad.value == -2) ||
        accion == 4 && f.paralelo.value == -1){
      alert ('No se puede eliminar si el horario es nuevo');
      return false;
    } else {
      var agrega = confirm("�Est� seguro que desea borrar "+borr+"?");
      if(!agrega) return false;
    }

  }
//  if (accion == 2 || accion == 4 ){ // mantenedor de actividad
//    if (f.rutnum.value == ""){
//      alert('Debe ingresar Profesor');
//      f.rut_aux.focus();
//      return false;
//    }
//  }
  if (accion == 1 || accion == 3){

    if (document.getElementById('f2_horario') != null)
      f.horario.value = document.getElementById('f2_horario').value;
    else
      if (document.getElementById('f3_horario') != null)
      f.horario.value = document.getElementById('f3_horario').value;
      
    indice = f.horario.value;
    if (indice >= 0){
       if (f.actividad.value != 0)

	    var hor_bloque1 = parseInt(combo_hor_blo1[indice],10);
	    var hor_bloque2 = parseInt(combo_hor_blo2[indice],10);

        f.num_dia.value =  combo_hor_dia[indice];

        f.bloque_inicio.value = hor_bloque1;
        f.bloque_termino.value = hor_bloque2;
      }
    }
   
   if (accion != 2 && accion != 4){ 
	    
	   var bloque1int = parseInt(f.bloque1.value,10);
	   var bloque2int = parseInt(f.bloque2.value,10);
	   
	    f.bloque1.value = bloque1int;
	    f.bloque2.value = bloque2int;
   }
   
	f.accion.value = accion; <%/* 1 A/M mantenedor de horario 2 A/M mantenedor de actividad */%>
	f.submit();              <%/* // 3 eliminar mantenedor de horario 4 eliminar actividad  */%>
}

function copiar(){
  var pregunta = "Ud. va a COPIAR el Horario del semestre anterior.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  if (!respuesta) return false;
  document.fingresar3.accion.value = "5";
  document.fingresar3.submit();
}
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
</head>

<body onLoad="<%=onload%>Mensaje();">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<form name="fingresar" method="post" action="gim_horario_adm.jsp">
	<input type="hidden" name="accion" value="">
	<tr>
		<td height="30" colspan="2"
			<%=(cod_perfil==28 || cod_perfil==2)?"class=\"Estilo_Verde\"":"class=\"Estilo_Naranjo\""%>>HORARIO</td>
	</tr>
	<tr>
		<td width="12%"><span class="Estilo_Gris">Semestre</span></td>
		<td width="88%"><span class="Estilo_Azul"> <select
			name="anno" class="Option" id="anno"
			onchange="document.fingresar.submit();">
			<%
				for (int i = 2006; i <= a�o; i++) {
			%>
			<option value="<%=i%>" <%=(i==anno)?"selected":""%>><%=i%></option>
			<%
				}
			%>
		</select> - <select name="semestre" class="Option" id="semestre"
			onchange="document.fingresar.submit();">
			<option value="1" <%=(semestre==1)?"selected":""%>>1</option>
			<option value="2" <%=(semestre==2)?"selected":""%>>2</option>
			<option value="3" <%=(semestre==3)?"selected":""%>>3</option>
		</select> </span></td>
	</tr>
	<tr>
		<td><span class="Estilo_Gris">Sala</span></td>
		<td><span class="Estilo_Azul"> <select name="sala"
			class="Option" onchange="document.fingresar.submit();">
			<option value="0">-- Seleccione --</option>
			<option value="10335" <%=(sala == 10335)?"selected":""%>>M&aacute;quinas</option>
			<option value="10336" <%=(sala == 10336)?"selected":""%>>Multiuso</option>
		</select> </span></td>
	</tr>
	</form>
</table>
<br>
<%
	if (sala > 0) {
%>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<%
			if (soloconsulta) {
		%>
		<td width="6%">
		<table height="20" border="0" cellpadding="0" cellspacing="0">
			<tr onClick="Bloque(1)" onMouseOver="BotonOver(this,'Horario')"
				onMouseOut="BotonOut(this)">
				<td width="10" height="20"><img id="IMG1_BordeIzq"
					name="IMG1_BordeIzq" src="imagen/etiq_baseBORDEIZQ.gif" width="10"
					height="20"></td>
				<td nowrap id="IMG1_Fondo" background="imagen/etiq_baseFONDO.gif"
					class="Estilo_Blanco">Horario</td>
				<td width="10" height="20" class="Estilo_Azulino"><img
					id="IMG1_BordeDer" name="IMG1_BordeDer"
					src="imagen/etiq_baseBORDEDER.gif" width="10" height="20"></td>
			</tr>
		</table>
		</td>

		<td width="13%">
		<table height="20" border="0" cellpadding="0" cellspacing="0">
			<tr onClick="Bloque(2)"
				onMouseOver="BotonOver(this,'Mantenci&oacute;n de Horario')"
				onMouseOut="BotonOut(this)">
				<td width="10" height="20"><img id="IMG2_BordeIzq"
					name="IMG2_BordeIzq" src="imagen/etiq_baseBORDEIZQ.gif" width="10"
					height="20"></td>
				<td nowrap id="IMG2_Fondo" background="imagen/etiq_baseFONDO.gif"
					class="Estilo_Blanco">Mantenci&oacute;n de Horario</td>
				<td width="10" height="20" class="Estilo_Azulino"><img
					id="IMG2_BordeDer" name="IMG2_BordeDer"
					src="imagen/etiq_baseBORDEDER.gif" width="10" height="20"></td>
			</tr>
		</table>
		</td>
		<td width="81%">
		<table height="20" border="0" cellpadding="0" cellspacing="0">
			<tr onClick="Bloque(3)"
				onMouseOver="BotonOver(this,'Mantenci&oacute;n de Actividades')"
				onMouseOut="BotonOut(this)">
				<td width="10" height="20"><img id="IMG3_BordeIzq"
					name="IMG3_BordeIzq" src="imagen/etiq_baseBORDEIZQ.gif" width="10"
					height="20"></td>
				<td nowrap id="IMG3_Fondo" background="imagen/etiq_baseFONDO.gif"
					class="Estilo_Blanco">Mantenci&oacute;n de Actividades</td>
				<td width="10" height="20" class="Estilo_Azulino"><img
					id="IMG3_BordeDer" name="IMG3_BordeDer"
					src="imagen/etiq_baseBORDEDER.gif" width="10" height="20"></td>
			</tr>
		</table>
		</td>
		<%
			}
		%>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td colspan="3">
		<div id="BLOQUE_MantActividad" Style="display: none">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
			<form name="fingresar2" method="post" action="gim_horario_adm.jsp">
			<tr>
				<td colspan="3" bgcolor="#FFFFFF"><input type="hidden"
					name="anno" value="<%=anno%>"> <input type="hidden"
					name="semestre" value="<%=semestre%>"> <input type="hidden"
					name="sala" value="<%=sala%>"> <input type="hidden"
					name="onload" value="Bloque(3);"> <input type="hidden"
					name="accion" value=""> <input type="hidden"
					name="paralelo" value="0"> <input type="hidden"
					name="rutnum" value="0"> <input type="hidden" name="cupo"
					value="0"> <input type="hidden" name="rama" value="0">

				<img src="imagen/spacer.gif" width="1" height="3"></td>
			</tr>
			<tr>
				<td width="1%" rowspan="5" bgcolor="#FFFFFF"><img
					src="imagen/spacer.gif" width="1" height="1"></td>
				<td width="13%" bgcolor="#FFFFFF"><span class="Estilo_Gris">Actividad</span></td>
				<td width="86%" bgcolor="#FFFFFF"><select name="actividad"
					class="Option" onchange="CargaProg();">
					<option value="-2">-- Seleccione --</option>
					<%
						for (int i = 0; i < vec_actividad.size(); i++) {
					%>
					<option value="<%=((Vector)vec_actividad.get(i)).get(0)+""%>"><%=((Vector) vec_actividad.get(i)).get(1) + ""%></option>
					<%
						}
					%>
				</select></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFFF"><span id="BActiv_linea1_celda1"></span></td>
				<td bgcolor="#FFFFFF"><span id="BActiv_linea1_celda2"></span></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFFF"><span id="BActiv_linea2_celda1"></span></td>
				<td bgcolor="#FFFFFF"><span id="BActiv_linea2_celda2"></span></td>

			</tr>
			<tr>
				<td bgcolor="#FFFFFF"><span id="BActiv_linea3_celda1"></span></td>
				<td bgcolor="#FFFFFF"><span id="BActiv_linea3_celda2"></span></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFFF"><span id="BActiv_linea4_celda1"></span></td>
				<td bgcolor="#FFFFFF"><span id="BActiv_linea4_celda2"></span></td>
			</tr>
			<tr>
				<td colspan="3" bgcolor="#FFFFFF"><img src="imagen/spacer.gif"
					width="1" height="3"></td>
			</tr>
			</form>
		</table>
		</div>
		<div id="BLOQUE_MantHorario" style="display: none">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<form name="fingresar3" method="post" action="gim_horario_adm.jsp">
			<input type="hidden" name="anno" value="<%=anno%>"> <input
				type="hidden" name="semestre" value="<%=semestre%>"> <input
				type="hidden" name="sala" value="<%=sala%>"> <input
				type="hidden" name="onload" value="Bloque(2);"> <input
				type="hidden" name="accion" value=""> <input type="hidden"
				name="cod_asign" value=""> <input type="hidden"
				name="num_dia" value=""> <input type="hidden"
				name="bloque_inicio" value=""> <input type="hidden"
				name="bloque_termino" value=""> <input type="hidden"
				name="fecha_inicio" value=""> <input type="hidden"
				name="fecha_termino" value=""> <input type="hidden"
				name="id_aseo" value=""> <input type="hidden"
				name="paralelo" value="0"> <input type="hidden"
				name="preferencia" value="0"> <input type="hidden"
				name="dia1" value="0"> <input type="hidden" name="dia2"
				value="0"> <input type="hidden" name="bloque1" value="0">
			<input type="hidden" name="bloque2" value="0"> <input
				type="hidden" name="horario" value="0">
			<tr>
				<td width="69%" valign="top" bgcolor="#FFFFFF">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td colspan="3" bgcolor="#FFFFFF"><img
							src="imagen/spacer.gif" width="1" height="3"></td>
					</tr>
					<tr>
						<td width="1%" rowspan="7" bgcolor="#FFFFFF"><img
							src="imagen/spacer.gif" width="1" height="1"></td>
						<td width="19%" bgcolor="#FFFFFF"><span class="Estilo_Gris">Actividad</span></td>
						<td bgcolor="#FFFFFF"><select name="actividad" class="Option"
							onChange="RevisaActividad(this)">
							<option value="-2" selected>-- Seleccione --</option>
							<option value="0">-- ASEO --</option>
							<%
								for (int i = 0; i < vec_mantactividad.size(); i++) {
							%>
							<option value="<%=((Vector)vec_mantactividad.get(i)).get(0)+""%>"><%=((Vector) vec_mantactividad.get(i)).get(1)
							+ ""%></option>
							<%
								}
							%>
						</select></td>
					</tr>
					<tr>
						<td bgcolor='#FFFFFF'><span id="TB_linea1_celda1"></span></td>
						<td bgcolor='#FFFFFF'><span id="TB_linea1_celda2"></span>
					</tr>
					<tr>
						<td bgcolor='#FFFFFF'><span id="TB_linea2_celda1"></span></td>
						<td bgcolor='#FFFFFF'><span id="TB_linea2_celda2"></span>
					</tr>
					<tr>
						<td bgcolor='#FFFFFF'><span id="TB_linea5_celda1"></span></td>
						<td bgcolor='#FFFFFF'><span id="TB_linea5_celda2"></span>
					</tr>
					<tr>
						<td bgcolor='#FFFFFF'><span id="TB_linea3_celda1"></span></td>
						<td bgcolor='#FFFFFF'><span id="TB_linea3_celda2"></span></td>
					</tr>
					<tr>
						<td bgcolor='#FFFFFF'><span id="TB_linea4_celda1"></span></td>
						<td bgcolor='#FFFFFF'><span id="TB_linea4_celda2"></span>
					</tr>
					<tr>
						<td colspan="3" bgcolor="#FFFFFF"><img
							src="imagen/spacer.gif" width="1" height="3"></td>
					</tr>
				</table>
				</td>
				<td width="31%" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1">
					<tr align="center">
						<td width="24%" nowrap background="imagen/boton_baseFONDO05.gif"
							class="letra6">Bloque</td>
						<td width="11%" background="imagen/boton_baseFONDO05.gif"
							class="letra6">Lu</td>
						<td width="11%" background="imagen/boton_baseFONDO05.gif"
							class="letra6">Ma</td>
						<td width="11%" background="imagen/boton_baseFONDO05.gif"
							class="letra6">Mi</td>
						<td width="11%" background="imagen/boton_baseFONDO05.gif"
							class="letra6">Ju</td>
						<td width="11%" background="imagen/boton_baseFONDO05.gif"
							class="letra6">Vi</td>
						<td width="11%" background="imagen/boton_baseFONDO05.gif"
							class="letra6">S&aacute;</td>
					</tr>

					<%
						Vector vec_hordia = new Vector();
							Vector vec_rowspan = new Vector();
							for (int i = 0; i < vec_horabloque1.size(); i++) {
								v = new Vector();
								v = (Vector) vec_horabloque1.get(i);
								int cod_bloque = Integer.parseInt(v.get(0).toString());
								int ind_cod_bloque = ind_vec_horabloque.indexOf(v.get(0)
										.toString()) + 1;
					%>
					<tr align="center" bgcolor="#FFFFFF" class="letra7">
						<td nowrap background="imagen/boton_baseFONDO05.gif"
							class="letra6" valign="top"><%=v.get(1)%> - <%=v.get(2)%></td>
						<%
							/*
																																																																																																																																																																																									0 d�a
																																																																																																																																																																																									1 bloque inicio
																																																																																																																																																																																									2 bloque fin
																																																																																																																																																																																									3 nom.tipo reserva
																																																																																																																																																																																									4 profesor
																																																																																																																																																																																									5 cod tipo reserva
																																																																																																																																																																																									6 color texto
																																																																																																																																																																																									7 bg color
																																																																																																																																																																																									8 tipo reserva
																																																																																																																																																																																									9 cod_asign / id aseo

									 */
									for (int d = 1; d < 7; d++) {
										boolean pasa = false;
										String td = "";

										for (int j = 0; j < vec_horario1.size(); j++) {
											Vector h = (Vector) vec_horario1.get(j);
											dia = Integer.parseInt(h.get(0).toString());
											int bi = Integer.parseInt(h.get(1).toString());
											int bt = Integer.parseInt(h.get(2).toString());
											int ind_bi = ind_vec_horabloque.indexOf(h.get(1)
													.toString()) + 1;
											int ind_bt = ind_vec_horabloque.indexOf(h.get(2)
													.toString()) + 1;
											cod_reserva = Integer.parseInt(h.get(5).toString());
											if (d == dia
													&& ind_cod_bloque >= ind_bi
													&& ind_cod_bloque <= ind_bt
													&& !vec_hordia.contains(dia + "-" + bi
															+ "-" + bt)) {
												td = "";
												pasa = true;
												vec_hordia
														.addElement(dia + "-" + bi + "-" + bt);
												// guarda los bloques reales con rowspan
												for (int b = ind_bi; b <= ind_bt; b++) {
													if (!vec_rowspan.contains(d + "-" + b))
														vec_rowspan.addElement(d + "-" + b);
												}

												String prof = h.get(4).toString();
												estilo = h.get(7).toString();
												String tipo = h.get(5).toString();
												if (sala == 10336) {
													if (cod_reserva != 1)
														tipo = h.get(3).toString() + "<br>"
																+ h.get(8).toString();
													else
														tipo = h.get(3).toString();
												}
												String rowspan = "";

												int dif = (ind_bt - ind_bi) + 1;
												if (dif > 1)
													rowspan = "rowspan=\"" + dif + "\"";

												td = "<td "
														+ rowspan
														+ " id=\"dia_"
														+ dia
														+ "_"
														+ i
														+ "\""
														+ " bgcolor=\""
														+ estilo
														+ "\" onmouseover=\"Info(this,'"
														+ tipo
														+ "','"
														+ prof
														+ "')\""
														+ " onMouseout=\"EscondeTip()\">&nbsp;</td>";

											}
										}
										if (!pasa
												&& !vec_rowspan.contains(d + "-"
														+ ind_cod_bloque)) {
											td = "<td background=\"imagen/boton_baseFONDO06.gif\" "
													+ "onmouseover=\"Info(this,'-1','')\""
													+ " onMouseout=\"EscondeTip()\">&nbsp;</td>";
										}
						%>
						<%=td%>
						<%
							}
						%>
					</tr>
					<%
						}
					%>
				</table>
				</td>
			</tr>
			</form>
		</table>
		</div>
		<div id="BLOQUE_Horario">
		<table width="100%" border="0" cellspacing="1" cellpadding="0">
			<tr align="center" bgcolor="#CCCCCC">
				<td width="16%" height="16"
					background="imagen/boton_baseFONDO05.gif"><span
					class="Estilo_Azul">Bloques</span></td>
				<td width="14%" background="imagen/boton_baseFONDO05.gif"><span
					class="Estilo_Azul">Lunes</span></td>
				<td width="14%" background="imagen/boton_baseFONDO05.gif"><span
					class="Estilo_Azul">Martes</span></td>
				<td width="14%" background="imagen/boton_baseFONDO05.gif"><span
					class="Estilo_Azul">Mi&eacute;rcoles</span></td>
				<td width="14%" background="imagen/boton_baseFONDO05.gif"><span
					class="Estilo_Azul">Jueves</span></td>
				<td width="14%" background="imagen/boton_baseFONDO05.gif"><span
					class="Estilo_Azul">Viernes</span></td>
				<td width="14%" background="imagen/boton_baseFONDO05.gif"><span
					class="Estilo_Azul">S&aacute;bado</span></td>
			</tr>
			<%
				vec_hordia = new Vector();
					vec_rowspan = new Vector();
					for (int i = 0; i < vec_horabloque1.size(); i++) {
						v = new Vector();
						v = (Vector) vec_horabloque1.get(i);
						int cod_bloque = Integer.parseInt(v.get(0).toString());
						int ind_cod_bloque = ind_vec_horabloque.indexOf(v.get(0)
								.toString()) + 1;
			%>
			<tr align="center" bgcolor="#FFFFFF">
				<td bgcolor="#E9E9E9" class="letra7 Estilo_Azul"><%=v.get(1)%>
				- <%=v.get(2)%></td>
				<%
					/*
																																																																																																																																																																																							0 d�a
																																																																																																																																																																																							1 bloque inicio
																																																																																																																																																																																							2 bloque fin
																																																																																																																																																																																							3 nom.tipo reserva
																																																																																																																																																																																							4 profesor
																																																																																																																																																																																							5 tipo reserva
																																																																																																																																																																																							6 color texto
																																																																																																																																																																																							7 bg color
																																																																																																																																																																																							8 tipo reserva
																																																																																																																																																																																							9 cod_asign / id aseo

							 */
							for (int d = 1; d < 7; d++) {
								boolean pasa = false;
								String td = "";
								cod_reserva = 0;
								for (int j = 0; j < vec_horario1.size(); j++) {
									Vector h = (Vector) vec_horario1.get(j);
									dia = Integer.parseInt(h.get(0).toString());
									int bi = Integer.parseInt(h.get(1).toString());
									int bt = Integer.parseInt(h.get(2).toString());
									int ind_bi = ind_vec_horabloque.indexOf(h.get(1)
											.toString()) + 1;
									int ind_bt = ind_vec_horabloque.indexOf(h.get(2)
											.toString()) + 1;

									cod_reserva = Integer.parseInt(h.get(5).toString());
									if (d == dia
											&& ind_cod_bloque >= ind_bi
											&& ind_cod_bloque <= ind_bt
											&& !vec_hordia.contains(dia + "-" + bi
													+ "-" + bt)) {
										td = "";
										pasa = true;
										vec_hordia
												.addElement(dia + "-" + bi + "-" + bt);
										// guarda los bloques reales con rowspan
										for (int b = ind_bi; b <= ind_bt; b++) {
											if (!vec_rowspan.contains(d + "-" + b))
												vec_rowspan.addElement(d + "-" + b);
										}

										String prof = h.get(4).toString();
										estilo = h.get(6).toString();
										String tipo = h.get(3).toString();
										if (sala == 10336 && cod_reserva != 1)
											prof += h.get(8).toString().equals("") ? ""
													: "<br>" + h.get(8).toString();
										String rowspan = "";

										int dif = (ind_bt - ind_bi) + 1;
										if (dif > 1)
											rowspan = "rowspan=\"" + dif + "\"";

										td = "<td " + rowspan + " id=\"dia_" + dia
												+ "_" + i + "\"" + " class=\"" + estilo
												+ "\">" + tipo + "<br>" + prof
												+ "</td>";

									}
								}
								if (!pasa
										&& !vec_rowspan.contains(d + "-"
												+ ind_cod_bloque)) {
									td = "<td bgcolor=\"#E9E9E9\" >&nbsp;</td>";
								}
				%>
				<%=td%>
				<%
					}
				%>
			</tr>
			<%
				}
			%>
		</table>
		</div>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="1">
	<tr>
		<td width="91" height="35" bgcolor="#FFFFFF">
		<DIV id="BLOQUE_BTN_confirmar" Style="display: none">
		<table width="91" height="21" border="0" cellpadding="0"
			cellspacing="0" background="imagen/boton_baseHORARIO.gif">
			<tr id="BTN_confirmar"
				onClick="javascript:Confirmar(document.fingresar3,1);"
				onMouseOver="BotonOver(this,'Registrar')"
				onMouseOut="BotonOut(this)">
				<td width="27">&nbsp;</td>
				<td width="64" class="Estilo_Azulino">Confirmar</td>
			</tr>
		</table>
		</DIV>
		<DIV id="BLOQUE_BTN_confirmar2" Style="display: none">
		<table width="91" height="21" border="0" cellpadding="0"
			cellspacing="0" background="imagen/boton_baseHORARIO.gif">
			<tr id="BTN_confirmar"
				onClick="javascript:Confirmar(document.fingresar2,2);"
				onMouseOver="BotonOver(this,'Registrar')"
				onMouseOut="BotonOut(this)">
				<td width="27">&nbsp;</td>
				<td width="64" class="Estilo_Azulino">Confirmar</td>
			</tr>
		</table>
		</DIV>
		<DIV id="BLOQUE_BTN_imprimir" Style="display: none">
		<table width="91" height="21" border="0" cellpadding="0"
			cellspacing="0" background="imagen/boton_baseIMPRIMIR.gif">
			<tr id="BTN_imprimir"
				onClick="AbrirVentana(document.form_listado,'gim_genera_list.jsp')"
				onMouseOver="BotonOver(this,'Imprimir')" onMouseOut="BotonOut(this)">
				<td width="29">&nbsp;</td>
				<td width="62" class="Estilo_Azulino">Imprimir</td>
			</tr>
		</table>
		</DIV>
		</td>
		<td width="207">
		<DIV id="BLOQUE_BTN_eliminar" Style="display: none">
		<table width="91" height="21" border="0" cellpadding="0"
			cellspacing="0" background="imagen/boton_baseELIMINAR.gif">
			<tr id="BTN_eliminar"
				onClick="javascript:Confirmar(document.fingresar3,3);"
				onMouseOver="BotonOver(this,'Eliminar')" onMouseOut="BotonOut(this)">
				<td width="27">&nbsp;</td>
				<td width="64" class="Estilo_Azulino">Eliminar</td>
			</tr>
		</table>
		</DIV>
		<DIV id="BLOQUE_BTN_eliminar2" Style="display: none">
		<table width="91" height="21" border="0" cellpadding="0"
			cellspacing="0" background="imagen/boton_baseELIMINAR.gif">
			<tr id="BTN_eliminar"
				onClick="javascript:Confirmar(document.fingresar2,4);"
				onMouseOver="BotonOver(this,'Eliminar')" onMouseOut="BotonOut(this)">
				<td width="27">&nbsp;</td>
				<td width="64" class="Estilo_Azulino">Eliminar</td>
			</tr>
		</table>
		</DIV>
		</td>
		<td width="674">
		<DIV id="BLOQUE_BTN_copiar">
		<%
			if (vec_horario_s.size() == 0 || vec_programacion.size() == 0) {
		%>
		<table height="21" border="0" cellpadding="0" cellspacing="0">
			<tr id="BTN_imprimir" onClick="copiar();"
				onMouseOver="BotonOver(this,'Imprimir')" onMouseOut="BotonOut(this)">
				<td width="21"><img src="imagen/boton_baseREG01.gif" width="21"
					height="21"></td>
				<td width="155" align="center"
					background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">Copiar
				semestre anterior</td>
				<td width="3" class="Estilo_Azulino"><img
					src="imagen/boton_baseBORDEDER.gif" width="3" height="21"></td>
			</tr>
		</table>
		<%
			}
		%>
		</DIV>
		</td>
	</tr>

</table>
<%
	}
%>
<form name="form_listado" method="post"><input type="hidden"
	name="listado" value="4"> <input type="hidden" name="anno"
	value="<%=anno%>"> <input type="hidden" name="semestre"
	value="<%=semestre%>"> <input type="hidden" name="sala"
	value="<%=sala%>"></form>
<form name="flistado" method="post"><input type="hidden"
	name="listado" value="7"> <input type="hidden" name="anno"
	value="<%=anno%>"> <input type="hidden" name="semestre"
	value="<%=semestre%>"> <input type="hidden" name="cod_asign"
	value=""> <input type="hidden" name="paralelo" value="">
</form>
<div id="dhtmltooltip"></div>
<script language="JavaScript" type="text/javascript" src="js/box.js"></script>
</body>
</html>
