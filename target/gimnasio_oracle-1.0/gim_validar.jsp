<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>
<%@ page import="cl.usm.siga.login.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 04/06/2007
   // �ltima Actualizaci�n :
   // jsp orientado a la validaci�n del rut, clave y perfil del usuario
   // 27/10/08 se cambia el login a cuenta de correo y clave USM (MM)
%>
  <jsp:useBean id="loginBeanId" class="cl.usm.siga.login.LoginLDAPBean"/>
  <jsp:setProperty name="loginBeanId" property="*" />

  <jsp:useBean id="mant" class="gimnasio.BeanUsuarios"></jsp:useBean>
  <jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%
  response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");

  int rut_usuario_intranet = session.getAttribute("rut_usuario_intranet") != null?
                             Integer.parseInt(session.getAttribute("rut_usuario_intranet")+""):0;
  String clave_usuario     = request.getParameter("passwd")!=null?
                             request.getParameter("passwd").trim():"";


  if (rut_usuario_intranet == 0 &&
      (clave_usuario == "" || clave_usuario == null)){
%>
  <jsp:forward page="index.jsp">
  <jsp:param name="msj" value="1" /></jsp:forward>
<%}
  else {

    // Valida LOGIN
    boolean valida_login = false;
    loginBeanId.setRemoteaddr(request.getRemoteAddr());
    if (rut_usuario_intranet == 0) valida_login = loginBeanId.getUsuario();
    else valida_login = true;
    boolean habilitado   = false;
    valida_login = true;  //DESARROLLO
    // si no es usuario
    if (!valida_login) {
%>
  <jsp:forward page="index.jsp" >
  <jsp:param name="msj" value="1" /></jsp:forward>
<%}
  else {
    String ldap_rut = loginBeanId.getRut();
    if (ldap_rut.length() == 0) ldap_rut = "0";
    int rut_usuario = Integer.parseInt(ldap_rut);
    rut_usuario = 13855538; //DESARROLLO

    mant.getAbrirConexion();
    mant.setGenerarConexionUsuario();
    // si es usuario se verifica si tiene perfil para el sistema
    int cod_perfil = 0;
    cod_perfil = mant.getPerfilUsuario(rut_usuario);

    if (cod_perfil != 2) {
      general.getAbrirConexion();
      general.setGenerarConexionRegistro();
      int activo = 0;
      activo = general.getActivacion();
      general.getCerrarConexion();
      if (activo == 0) {
%>
  <jsp:forward page="index.jsp" >
  <jsp:param name="msj" value="4" /></jsp:forward>
<%    }
    }

    // si no tiene acceso al sistema se ve si est� habilitado como alumno
    if (cod_perfil == 0){
      Vector v = new Vector();
      general.getAbrirConexion();
      general.setGenerarConexionDatosAl();
      v = general.getAlumno(rut_usuario);


      // si no est� como alumno  habilitado se ve si es funcionario cc
       if (v.size() == 0 ||
          (v.size() > 0 && Integer.parseInt(v.get(8)+"") == 2)) {
        v = mant.getFuncionario(rut_usuario);
        general.getCerrarConexion();
        //general.getAbrirConexionVi�a();
        // no es funcionario
        if (v.size() == 0) {  // se ve si es alumno vi�a
          //general.setGenerarConexionDatosVi�a();
          //v = general.getAlumnoVi�a(rut_usuario);
          if (v.size() == 0 ||
              (v.size() > 0 && Integer.parseInt(v.get(8)+"") == 2)) {
            // si no es alumno habilitado se ve si es funcionario vi�a
            //v = general.getFuncionarioVi�a(rut_usuario);
            //general.getCerrarConexionVi�a();
            if (v.size()== 0){
%>
  <jsp:forward page="index.jsp" >
  <jsp:param name="msj" value="2" /></jsp:forward>
<%         }
           else {

             // Administrativo vi�a
             cod_perfil = 19;
             habilitado = true;
           }
          }
          else {
           //general.getCerrarConexionVi�a();
            // Alumno vi�a
            cod_perfil = 8;
            habilitado = true;
          }
        }
        else {
          // Administrativo
          cod_perfil = 19;
          habilitado = true;
        }
        //general.getCerrarConexionVi�a();
      }
      else {
        // Alumno
        cod_perfil = 8;
        habilitado = true;
      }
      general.getCerrarConexion();
    } else habilitado = true;

    if (habilitado) {
      mant.getCerrarConexion();
      session.setMaxInactiveInterval(28800);//segundos
      session.setAttribute("rut_usuario",rut_usuario+"");
      session.setAttribute("cod_perfil",cod_perfil+"");
      response.sendRedirect("gim_inicio.jsp");
    }
    mant.getCerrarConexion();

  }
  }
%>
