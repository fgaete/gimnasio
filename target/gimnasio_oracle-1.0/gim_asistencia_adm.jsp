<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>
<%@ page import="java.net.*"%>

<%
	// Creado por           : M�nica Barrera Frez
	// Fecha                : 18/07/2007
	// �ltima Actualizaci�n :
	// Asistencia. Mantenedor de asistencia.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<jsp:useBean id="us" class="gimnasio.BeanUsuarios"></jsp:useBean>
<%
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
	response.addHeader("Cache-Control",
			"no-cache, no-store, must-revalidate");
 
	int rut_usuario = session.getAttribute("rut_usuario") != null ? Integer
			.parseInt(session.getAttribute("rut_usuario") + "")
			: 0;
	if (rut_usuario == 0){
		response.sendRedirect("index.jsp");

	}else{	
	int cod_perfil = session.getAttribute("cod_perfil") != null ? Integer
			.parseInt(session.getAttribute("cod_perfil") + "")
			: 0;
	int sala = request.getParameter("sala") != null
			&& !request.getParameter("sala").equals("null")
			&& !request.getParameter("sala").equals("NULL")
			&& !request.getParameter("sala").equals("")
			&& !request.getParameter("sala").equals(" ") ? Integer
			.parseInt(request.getParameter("sala") + "") : 0;
	String rut_aux = request.getParameter("rut_aux") != null
			&& !request.getParameter("rut_aux").equals("null")
			&& !request.getParameter("rut_aux").equals("NULL")
			&& !request.getParameter("rut_aux").equals("")
			&& !request.getParameter("rut_aux").equals(" ") ? request
			.getParameter("rut_aux")
			+ "" : "";
	int rutnum = request.getParameter("rutnum") != null
			&& !request.getParameter("rutnum").equals("null")
			&& !request.getParameter("rutnum").equals("NULL")
			&& !request.getParameter("rutnum").equals("")
			&& !request.getParameter("rutnum").equals(" ") ? Integer
			.parseInt(request.getParameter("rutnum") + "") : 0;
	String dvrut = request.getParameter("dvrut") != null
			&& !request.getParameter("dvrut").equals("null")
			&& !request.getParameter("dvrut").equals("NULL")
			&& !request.getParameter("dvrut").equals("")
			&& !request.getParameter("dvrut").equals(" ") ? request
			.getParameter("dvrut")
			+ "" : "";
	int bloque1 = request.getParameter("bloque1") != null
			&& !request.getParameter("bloque1").equals("null")
			&& !request.getParameter("bloque1").equals("NULL")
			&& !request.getParameter("bloque1").equals("")
			&& !request.getParameter("bloque1").equals(" ") ? Integer
			.parseInt(request.getParameter("bloque1") + "") : 0;
	int bloque2 = request.getParameter("bloque2") != null
			&& !request.getParameter("bloque2").equals("null")
			&& !request.getParameter("bloque2").equals("NULL")
			&& !request.getParameter("bloque2").equals("")
			&& !request.getParameter("bloque2").equals(" ") ? Integer
			.parseInt(request.getParameter("bloque2") + "") : 0;
	String fecha = request.getParameter("fecha_inicio") != null
			&& !request.getParameter("fecha_inicio").equals("null")
			&& !request.getParameter("fecha_inicio").equals("NULL")
			&& !request.getParameter("fecha_inicio").equals("")
			&& !request.getParameter("fecha_inicio").equals(" ") ? request
			.getParameter("fecha_inicio")
			+ ""
			: "";
	int dia = request.getParameter("dia") != null
			&& !request.getParameter("dia").equals("null")
			&& !request.getParameter("dia").equals("NULL")
			&& !request.getParameter("dia").equals("")
			&& !request.getParameter("dia").equals(" ") ? Integer
			.parseInt(request.getParameter("dia") + "") : 0;

	int accion = request.getParameter("accion") != null
			&& !request.getParameter("accion").equals("null")
			&& !request.getParameter("accion").equals("NULL")
			&& !request.getParameter("accion").equals("")
			&& !request.getParameter("accion").equals(" ") ? Integer
			.parseInt(request.getParameter("accion") + "") : 0;
	int bloque = request.getParameter("bloque") != null
			&& !request.getParameter("bloque").equals("null")
			&& !request.getParameter("bloque").equals("NULL")
			&& !request.getParameter("bloque").equals("")
			&& !request.getParameter("bloque").equals(" ") ? Integer
			.parseInt(request.getParameter("bloque") + "") : -1;
	int cod_asign = request.getParameter("cod_asign") != null
			&& !request.getParameter("cod_asign").equals("null")
			&& !request.getParameter("cod_asign").equals("NULL")
			&& !request.getParameter("cod_asign").equals("")
			&& !request.getParameter("cod_asign").equals(" ") ? Integer
			.parseInt(request.getParameter("cod_asign") + "") : 0;
	int paralelo = request.getParameter("paralelo") != null
			&& !request.getParameter("paralelo").equals("null")
			&& !request.getParameter("paralelo").equals("NULL")
			&& !request.getParameter("paralelo").equals("")
			&& !request.getParameter("paralelo").equals(" ") ? Integer
			.parseInt(request.getParameter("paralelo") + "") : 0;
	int tipo = request.getParameter("tipo")!=null &&
             !request.getParameter("tipo").equals("null") &&
             !request.getParameter("tipo").equals("NULL") &&
             !request.getParameter("tipo").equals("")  &&
             !request.getParameter("tipo").equals(" ")?
              Integer.parseInt(request.getParameter("tipo")+""):0;
    
	Vector v = new Vector();
	Vector vec_horabloque1 = new Vector();
	Vector vec_reserva = new Vector();
	Vector vec_periodo = new Vector();
	Vector vec_horariodia = new Vector();
	Vector vec_reg = new Vector();
	Vector vec_clase = new Vector();

	String periodo = "";
	String m = "";
	String dd = "";
	String a = "";
	String f1 = "";
	String fecha_actual = "";
	String mensaje = "";
	String rut_consulta0;

	int per1_a = 0;
	int per1_m = 0;
	int per1_d = 0;

	int per2_a = 0;
	int per2_m = 0;
	int per2_d = 0;

	StringBuffer le_rut = new StringBuffer();
	StringBuffer le_dvrut = new StringBuffer();
	StringBuffer le_pat = new StringBuffer();
	StringBuffer le_mat = new StringBuffer();
	StringBuffer le_nom = new StringBuffer();
	StringBuffer le_sex = new StringBuffer();
	StringBuffer le_tip = new StringBuffer();
	StringBuffer le_res = new StringBuffer();

	int anno = 0;
	int semestre = 0;
	int error = 0;
	int perfil = 0;
	int a�o = 0;
	int espera = 0;
	int origen_asistencia = 0;

	boolean muestra = false;
	boolean clase_cerrada = false;
	boolean clase_anulada = false;
	boolean clase_suspendida = false;
	boolean acceso_ip = true;
	boolean muestra_listado = false;

	// Si el Recepcionista accede desde otro PC que no sea el
	// del gimnasio, el sistem no le da acceso.
	// Por servidor balanceado esta opci�n esta deshabilitada, se espera poder corregir.
	/*if (cod_perfil == 28) {
		String ip = request.getRemoteAddr();
		if (!ip.trim().equals("200.1.26.221"))
			acceso_ip = false;	
	}*/

	if (acceso_ip) {
		// fecha actual
		java.util.Calendar f = new java.util.GregorianCalendar();
		a�o = f.get(java.util.Calendar.YEAR);
		int dia_actual = f.get(java.util.Calendar.DAY_OF_MONTH);
		int mes_actual = f.get(java.util.Calendar.MONTH) + 1;
		fecha_actual = (dia_actual < 10) ? "0" + dia_actual
				: dia_actual + "";
		if (mes_actual < 10)
			fecha_actual += "/0" + mes_actual + "/" + a�o;
		else
			fecha_actual += "/" + mes_actual + "/" + a�o;
		if (fecha.trim().equals("") && sala == 0)
			fecha = fecha_actual;

		m = (Integer.parseInt(fecha.substring(3, 5)) < 10) ? ("0" + Integer
				.parseInt(fecha.substring(3, 5)))
				: ("" + Integer.parseInt(fecha.substring(3, 5)));
		dd = (Integer.parseInt(fecha.substring(0, 2)) < 10) ? ("0" + Integer
				.parseInt(fecha.substring(0, 2)))
				: ("" + Integer.parseInt(fecha.substring(0, 2)));
		a = (Integer.parseInt(fecha.substring(6, 10)) < 10) ? ("0" + Integer
				.parseInt(fecha.substring(6, 10)))
				: ("" + Integer.parseInt(fecha.substring(6, 10)));

		f1 = a + m + dd;
		
		

		if (sala > 0) {

			general.getAbrirConexion();
			general.setGenerarConexionHorario();
			general.setGenerarConexionReserva();
			general.setGenerarConexionRegistro();
			general.setGenerarConexionBitacora();
			//System.out.println("getPeriodoGimnasioSala i");
			vec_periodo = general.getPeriodoGimnasioSala(sala, f1);
			//System.out.println("1 vecPeriodo: "+vec_periodo);
			//System.out.println("getPeriodoGimnasioSala o");
			periodo = (vec_periodo.size() > 0) ? vec_periodo.get(2)
					+ "" : "";
			anno = (vec_periodo.size() > 0) ? Integer
					.parseInt(vec_periodo.get(3) + "") : 0;
			semestre = (vec_periodo.size() > 0) ? Integer
					.parseInt(vec_periodo.get(4) + "") : 0;

			int fec_inicio = 0;
			int fec_termino = 0;
			int fec1 = 0;
			int f2 = 0;
			if (anno > 0 && semestre > 0 && dia > 0
					&& !periodo.trim().equals("") && bloque1 > 0
					&& bloque2 > 0 && !f1.trim().equals("")) {
				//System.out.println("getContAsistenciaGimnasio i");
				espera = general.getContAsistenciaGimnasio(anno,
						semestre, dia, periodo, bloque1, bloque2, 5,
						f1, sala);
				//System.out.println("getContAsistenciaGimnasio o");
			}

			String fec_ini = "";

			Calendar Cal = Calendar.getInstance();
			if (vec_periodo.size() > 0) {

				fec_ini = (vec_periodo.size() > 0) ? vec_periodo.get(0)
						+ "" : "";
				m = (Integer.parseInt(fec_ini.substring(3, 5)) < 10) ? ("0" + Integer
						.parseInt(fec_ini.substring(3, 5)))
						: ("" + Integer.parseInt(fec_ini
								.substring(3, 5)));
				dd = (Integer.parseInt(fec_ini.substring(0, 2)) < 10) ? ("0" + Integer
						.parseInt(fec_ini.substring(0, 2)))
						: ("" + Integer.parseInt(fec_ini
								.substring(0, 2)));
				a = (Integer.parseInt(fec_ini.substring(6, 10)) < 10) ? ("0" + Integer
						.parseInt(fec_ini.substring(6, 10)))
						: ("" + Integer.parseInt(fec_ini.substring(6,
								10)));

				per1_a = Integer.parseInt(a);
				per1_m = Integer.parseInt(m);
				per1_d = Integer.parseInt(dd);

				fec1 = Integer.parseInt(a + m + dd);
				String fec = dd + "/" + m + "/"
						+ Cal.get(Calendar.YEAR);

				String fec_ter = (vec_periodo.size() > 0) ? vec_periodo
						.get(1)
						+ "" : "";
				m = (Integer.parseInt(fec_ter.substring(3, 5)) < 10) ? ("0" + Integer
						.parseInt(fec_ter.substring(3, 5)))
						: ("" + Integer.parseInt(fec_ter
								.substring(3, 5)));
				dd = (Integer.parseInt(fec_ter.substring(0, 2)) < 10) ? ("0" + Integer
						.parseInt(fec_ter.substring(0, 2)))
						: ("" + Integer.parseInt(fec_ter
								.substring(0, 2)));
				a = (Integer.parseInt(fec_ter.substring(6, 10)) < 10) ? ("0" + Integer
						.parseInt(fec_ter.substring(6, 10)))
						: ("" + Integer.parseInt(fec_ter.substring(6,
								10)));

				per2_a = Integer.parseInt(a);
				per2_m = Integer.parseInt(m);
				per2_d = Integer.parseInt(dd);

				f2 = Integer.parseInt(a + m + dd);
				if ((Integer.parseInt(f1) >= fec1)
						&& (Integer.parseInt(f1) <= f2))
					muestra = true;
			}
			if (rutnum > 0) {
				//System.out.println("getRegistroUsuario i");
				vec_reg = general.getRegistroUsuario(rutnum);
				//System.out.println("getRegistroUsuario 0");
			}

			if (accion > 0) {
				if (accion == 1 || accion == 2 || accion == 3) {// Cerrar o anular o suspender clase
					String razon = request.getParameter("razon") != null
							&& !request.getParameter("razon").equals(
									"null")
							&& !request.getParameter("razon").equals(
									"NULL")
							&& !request.getParameter("razon")
									.equals("")
							&& !request.getParameter("razon").equals(
									" ") ? request
							.getParameter("razon")
							+ "" : "";
					int a�o_cl = request.getParameter("a�o") != null
							&& !request.getParameter("a�o").equals(
									"null")
							&& !request.getParameter("a�o").equals(
									"NULL")
							&& !request.getParameter("a�o").equals("")
							&& !request.getParameter("a�o").equals(" ") ? Integer
							.parseInt(request.getParameter("a�o") + "")
							: 0;
					int sem_cl = request.getParameter("semestre") != null
							&& !request.getParameter("semestre")
									.equals("null")
							&& !request.getParameter("semestre")
									.equals("NULL")
							&& !request.getParameter("semestre")
									.equals("")
							&& !request.getParameter("semestre")
									.equals(" ") ? Integer
							.parseInt(request.getParameter("semestre")
									+ "") : 0;
					String per_cl = request.getParameter("periodo") != null
							&& !request.getParameter("periodo").equals(
									"null")
							&& !request.getParameter("periodo").equals(
									"NULL")
							&& !request.getParameter("periodo").equals(
									"")
							&& !request.getParameter("periodo").equals(
									" ") ? request
							.getParameter("periodo")
							+ "" : "";

					v = new Vector();
					v.addElement("1");
					v.addElement(a�o_cl + "");
					v.addElement(sem_cl + "");
					v.addElement("1");
					v.addElement(dia + "");
					v.addElement(bloque1 + "");
					v.addElement(bloque2 + "");
					v.addElement(cod_asign + "");
					v.addElement(paralelo + "");
					v.addElement(f1);
					v.addElement(accion + "");
					v.addElement(razon);
					v.addElement(per_cl);
					v.addElement(rut_usuario + "");
					error = general.setCerrarAnularClase(v);
					if (error == 0){
						mensaje = "se registr� satisfactoriamente";
						Vector v2 = new Vector();
						v2.addElement(anno+"");
						v2.addElement(semestre+"");
						v2.addElement("2"); // prioridad
						v2.addElement("6"); // tipo
						v2.addElement(razon); // mensaje
						v2.addElement(fecha_actual);
						v2.addElement(rut_usuario+""); // rut_usuario
						v2.addElement("-1"); // flag fecha modificacion
						error = general.setInsertarBitacora(v);
					    if (error == 0) mensaje = "Mensaje registrado satisfactoriamente.";
					    else mensaje = "Problemas al registrar mensaje en bitacora.";
					}else
						mensaje = "Problemas al registrar";

				} else {
					/*calculo fecha de inicio y termino*/

					if (vec_periodo.size() > 0) {
						/*****/

						v = new Vector();
						v.addElement(fec1 + "");

						Vector vec_fecha_inicio = new Vector();
						Vector vec_fecha_termino = new Vector();
						while (fec1 < f2) {
							// para llenar periodo con aammdd
							vec_fecha_inicio.addElement(fec1 + "");

							Cal.set(Integer.parseInt(fec_ini.substring(
									6, 10)), (Integer.parseInt(fec_ini
									.substring(3, 5)) - 1), Integer
									.parseInt(fec_ini.substring(0, 2)));
							Cal.add(Calendar.DATE, +5);
							m = ((Cal.get(Calendar.MONTH) + 1) < 10) ? ("0" + (Cal
									.get(Calendar.MONTH) + 1))
									: ("" + (Cal.get(Calendar.MONTH) + 1));
							dd = (Cal.get(Calendar.DATE) < 10) ? ("0" + Cal
									.get(Calendar.DATE))
									: ("" + Cal.get(Calendar.DATE));

							fec1 = Integer.parseInt(Cal
									.get(Calendar.YEAR)
									+ m + dd);

							String fec = dd + "/" + m + "/"
									+ Cal.get(Calendar.YEAR);
							vec_fecha_termino.addElement(fec1 + "");

							Cal.add(Calendar.DATE, +2);
							m = ((Cal.get(Calendar.MONTH) + 1) < 10) ? ("0" + (Cal
									.get(Calendar.MONTH) + 1))
									: ("" + (Cal.get(Calendar.MONTH) + 1));
							dd = (Cal.get(Calendar.DATE) < 10) ? ("0" + Cal
									.get(Calendar.DATE))
									: ("" + Cal.get(Calendar.DATE));

							fec = dd + "/" + m + "/"
									+ Cal.get(Calendar.YEAR);
							fec_ini = fec;

							fec1 = Integer.parseInt(Cal
									.get(Calendar.YEAR)
									+ m + dd);

						}
						for (int i = 0; i < vec_fecha_inicio.size(); i++) {
							if ((Integer.parseInt(f1) >= Integer
									.parseInt(vec_fecha_inicio.get(i)
											+ ""))
									&& (Integer.parseInt(f1) <= Integer
											.parseInt(vec_fecha_termino
													.get(i)
													+ ""))) {
								fec_inicio = Integer
										.parseInt(vec_fecha_inicio
												.get(i)
												+ "");
								fec_termino = Integer
										.parseInt(vec_fecha_termino
												.get(i)
												+ "");
							}
						}

					}
				}
				if (accion == 4) { // registrar reserva
					int cod_asistencia = request
							.getParameter("cod_asistencia") != null
							&& !request.getParameter("cod_asistencia")
									.equals("null")
							&& !request.getParameter("cod_asistencia")
									.equals("NULL")
							&& !request.getParameter("cod_asistencia")
									.equals("")
							&& !request.getParameter("cod_asistencia")
									.equals(" ") ? Integer
							.parseInt(request
									.getParameter("cod_asistencia")
									+ "") : 0;
					v = new Vector();
					boolean habilitado = false;
					int cupo = 0;
					int rama = 0;
					int inscritos = 0;
					int orden = 0;
					String text = "";
					int insc_rama = 0;
					int tipo_reserva = 0;
					int tipo_alumno = 0;
					if (anno > 0 && semestre > 0 && cod_asign > 0
							&& sala > 0 && dia > 0 && bloque1 > 0
							&& bloque2 > 0)
						tipo_reserva = general.getReservaTipo(anno,
								semestre, cod_asign, paralelo, sala, 1,
								1, dia, bloque1, bloque2);
					if (vec_reg.size() == 0 && tipo_reserva != 5
							&& tipo_reserva != 7 && tipo_reserva != 1) {
						mensaje = "Este RUT no se encuentra registrado.";
						error = -1;
					} else { // si es alumno ver si est� habilitado
						if ((vec_reg.size() == 0 && (tipo_reserva == 5
								|| tipo_reserva == 7 || tipo_reserva == 1))
								|| (vec_reg.size() > 0
										&& Integer.parseInt(vec_reg
												.get(1)
												+ "") >= 1 && Integer
										.parseInt(vec_reg.get(1) + "") <= 3)) { // si es alumno cc
							general.setGenerarConexionDatosAl();
							habilitado = general.getHabilitadoReserva(
									rutnum, anno, semestre);
							if (!habilitado) {
								mensaje = "Este alumno no se encuentra habilitado.";
								error = -1;
							} else
								tipo_alumno = 1;
						}

						if ((vec_reg.size() == 0
								&& (tipo_reserva == 5
										|| tipo_reserva == 7 || tipo_reserva == 1) && !habilitado)
								|| (vec_reg.size() > 0 && (Integer
										.parseInt(vec_reg.get(1) + "") == 5 || Integer
										.parseInt(vec_reg.get(1) + "") == 6))) { // si es alumno vi�a
							/*general.getAbrirConexionVi�a();
							general.setGenerarConexionDatosVi�a();
							habilitado = general.getHabilitadoReservaVi�a(rutnum, anno, semestre);
							general.getCerrarConexionVi�a();
							 */
							habilitado = true;
							if (!habilitado) {
								mensaje = "Este alumno no se encuentra habilitado.";
								error = -1;
							} else {
								tipo_alumno = 5;
								// mensaje = "";
							}
						}
						if (vec_reg.size() > 0
								&& (Integer.parseInt(vec_reg.get(1)
										+ "") == 4 || Integer
										.parseInt(vec_reg.get(1) + "") == 7))
							habilitado = true;
						if (/*(Integer.parseInt(vec_reg.get(1)+"") == 3 ||
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														               Integer.parseInt(vec_reg.get(1)+"") == 4 ||
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														               Integer.parseInt(vec_reg.get(1)+"") == 6 ||
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														                Integer.parseInt(vec_reg.get(1)+"") == 7 ) &&*/habilitado) { // si es funcionario o alumno habilitado se ve si tiene reserva
							Vector vec_asis = new Vector();
							if (vec_reg.size() > 0) {
								//System.out.println("getAsistenciaRegistro i");
								vec_asis = general
										.getAsistenciaRegistro(anno,
												semestre, f1, periodo,
												dia, bloque1, bloque2,
												vec_reg.get(4) + "",
												sala);
								//System.out.println("getAsistenciaRegistro o");
							}
							if (tipo_reserva == 5 || tipo_reserva == 7)
								// si es docencia o extracurricular no importa el cupo, todos van al listado oficial
								cupo = 100;
							else {
								// ver vacante
								//System.out.println("getCupoGimnasio i");
								cupo = general.getCupoGimnasio(anno,
										semestre, dia, bloque1,
										bloque2, sala, cod_asign);
								//System.out.println("getCupoGimnasio o");
								
								//System.out.println("getContInscritosAsistentesGimnasio i");
								inscritos = general
										.getContInscritosAsistentesGimnasio(
												anno, semestre, dia,
												f1, periodo, bloque1,
												bloque2, sala);
								//System.out.println("getContInscritosAsistentesGimnasio o");
								
								//System.out.println("getContInscritosRamaGimnasio i");
								insc_rama = general
										.getContInscritosRamaGimnasio(
												anno, semestre, dia,
												f1, periodo, bloque1,
												bloque2, sala);
								//System.out.println("getContInscritosRamaGimnasio o");
								
								//System.out.println("getSobreCupoGimnasio i");
								rama = general.getSobreCupoGimnasio(
										anno, semestre, dia, bloque1,
										bloque2, sala);
								//System.out.println("getSobreCupoGimnasio o");

							}
							if (cupo > 0) {
								if (tipo_reserva != 5
										&& tipo_reserva != 7) {
									//                  if ((vec_reg.size()> 0 &&
									//                       (Integer.parseInt(vec_reg.get(1)+"") == 3 ||
									//                      Integer.parseInt(vec_reg.get(1)+"") == 6)  &&
									//   //                      insc_rama > (rama - 1 )) || // si es de rama y ya est� completo el cupo
									//                      ((rama - insc_rama) < 0)) || // si es de rama y ya est� completo el cupo
									//                      ((((vec_reg.size()> 0 &&
									//                       Integer.parseInt(vec_reg.get(1)+"") != 3 &&
									//                      Integer.parseInt(vec_reg.get(1)+"") != 6)) || vec_reg.size() == 0) &&
									//                      (cupo - rama ) < (inscritos +1)) // o si no es de rama y no alcanza el cupo
									//                      ) {
									//                    if (( vec_asis.size() > 0 &&
									//                          !(vec_asis.get(1)+"").trim().equals("4")) || // si no es de espera
									//                        ( vec_reg.size()> 0 &&
									//                        (Integer.parseInt(vec_reg.get(1)+"") == 3 ||
									//                        Integer.parseInt(vec_reg.get(1)+"") == 6) &&
									//                        insc_rama > (rama - 1 )))
									//                        { if (vec_asis.size() > 0)
									//                        orden = Integer.parseInt(vec_asis.get(2)+"");
									//                    else orden = 0;
									//                    }  else {
									//                      // se restan los  cupos de rama
									//                      cod_asistencia = 4;
									//                      orden = espera + 1;
									//                      text = " en lista de espera";
									//                    }
									//                  } else {
									//                    if (vec_reg.size()> 0 &&
									//                        (Integer.parseInt(vec_reg.get(1)+"") == 3 ||
									//                        Integer.parseInt(vec_reg.get(1)+"") == 6)){
									//                      if (vec_asis.size() > 0 && Integer.parseInt(vec_asis.get(1)+"") == cod_asistencia)
									//                        orden = Integer.parseInt(vec_asis.get(2)+"");
									//                      else
									//                        orden = insc_rama + 1;
									//                    } else
									//                      orden = 0;
									//                  }

									if ((cupo - inscritos - rama) <= 0) // se va a lista de espera
									{
										if (vec_reg.size() > 0
												&& (Integer
														.parseInt(vec_reg
																.get(1)
																+ "") == 3 || Integer
														.parseInt(vec_reg
																.get(1)
																+ "") == 6)
												&& (rama - insc_rama) > 0) {
											orden = insc_rama + 1;
										} else { 
											if (vec_asis.size() > 0 /*&& (vec_asis.get(1)+"").trim().equals("4")*/)
												orden = Integer
														.parseInt(vec_asis
																.get(2)
																+ "");
											else {
												cod_asistencia = 4;
												orden = espera + 1;
												text = "en lista de espera";
											}
										}
									} else { 
										// ver si no hay cupo de rama
										if (vec_reg.size() > 0
												&& (Integer
														.parseInt(vec_reg
																.get(1)
																+ "") == 3 || Integer
														.parseInt(vec_reg
																.get(1)
																+ "") == 6)
												&& (rama - insc_rama) == 0) {
											if (vec_asis.size() > 0
													&& !(vec_asis
															.get(1) + "")
															.trim()
															.equals("4"))
												orden = Integer
														.parseInt(vec_asis
																.get(2)
																+ "");
											else
												orden = 0;

										} else {
											if (vec_reg.size() > 0
													&& (Integer
															.parseInt(vec_reg
																	.get(1)
																	+ "") == 3 || Integer
															.parseInt(vec_reg
																	.get(1)
																	+ "") == 6)) { // si hay cupo de rama
												orden = insc_rama + 1;
											} else
												// es cupo normal
												orden = 0;

										}
									}
								}
								if (vec_asis.size() > 0
										&& !(vec_asis.get(0) + "")
												.trim().equals("0")) {
									// se inserta la asistencia al resgim

									v = new Vector();
									v.addElement(vec_asis.get(0) + "");
									v.addElement(f1);
									v.addElement(cod_asistencia + "");
									v.addElement(orden + "");
									v.addElement(rut_usuario + "");
									if (Integer.parseInt(vec_asis
											.get(1)
											+ "") != 0)
										error = general
												.setUpdAsistenciaGimnasio(v);
									else
										error = general
												.setAsistenciaGimnasio(v);
									// if (error == 0) mensaje = "Asistencia registrada satisfactoriamente " + text ;
									if (error == -1)
										mensaje = "Ya se encuentra registrada esta asistencia";
									if (error == -2)
										mensaje = "Problemas al registrar";
									if (error == -3)
										mensaje = "Ya se encuentra en listado de inscritos";
									if (error == -5)
										mensaje = "Ya se encuentra en listado de Espera";
									if (error == -6)
										mensaje = "Ya se encuentra en listado ";
								} else {
									// se inserta la reserva y la asistencia
                                    //System.out.println("getHorarioBloque i");
									v = general.getHorarioBloque(anno,
											semestre, sala, dia,
											bloque1, bloque2);
									//System.out.println("getHorarioBloque o");           
									if (v.size() > 0)
										text = v.get(0) + "" + "@"
												+ v.get(1) + "@" + dia
												+ "@" + bloque1 + "@"
												+ bloque2;
									v = new Vector();
									v.addElement(anno + "");// a�o
									v.addElement(semestre + "");// semestre
									v.addElement(fecha); // fecha
									v.addElement(periodo); // pergim_id
									v.addElement(text); // linea1
									v.addElement(rutnum + ""); // rut
									v.addElement(rut_usuario + ""); //rut_usuario
									v.addElement(cod_asistencia + ""); // cod_asistencia
									v.addElement(orden + ""); // orden
									v.addElement(fec_inicio + ""); // fecha_inicio
									v.addElement(fec_termino + ""); // fecha_termino;
									v.addElement(tipo_alumno + ""); // tipo_alumno
									v.addElement(sala + ""); // sala
									if (cod_asistencia == 4)
										text = " en lista de espera";
									else
										text = "";
									error = general
											.setReservaAsistenciaGimnasio(v);
									//  if (error == 0) mensaje = "Asistencia registrada satisfactoriamente " + text;
									if (error == -1)
										mensaje = "Problemas en registrar la reserva";
									if (error == -2) {
										mensaje = "Problemas al registrar la asistencia";
									}
									if (error == -4) {
										mensaje = "No puede registrar en bloque contiguo";

									}
								}
							} else {
								mensaje = "No hay cupo asignado";
								error = -1;
							}
						} else {
							/* if((Integer.parseInt(vec_reg.get(1)+"") == 1 ||
							     Integer.parseInt(vec_reg.get(1)+"") == 2 ||
							     Integer.parseInt(vec_reg.get(1)+"") == 5 ) && habilitado)*/
							mensaje = "No est� habilitado";
							error = -1;
						}

					}
					rutnum = 0;
					rut_aux = "";
					if (error != 0)
						error = -1;
				}
				if (accion == 5) // eliminar inscrito
				{
					String resgim = request.getParameter("resgim_id") != null
							&& !request.getParameter("resgim_id")
									.equals("null")
							&& !request.getParameter("resgim_id")
									.equals("NULL")
							&& !request.getParameter("resgim_id")
									.equals("")
							&& !request.getParameter("resgim_id")
									.equals(" ") ? request
							.getParameter("resgim_id")
							+ "" : "";
					v = new Vector();
					v.addElement(resgim);// resgim
					v.addElement(fecha); // fecha
					v.addElement(fec_inicio + ""); // fecha_inicio
					v.addElement(fec_termino + ""); // fecha_termino;
					v.addElement(rut_usuario + ""); // rut_usuario
					v.addElement(sala + ""); // sala
					error = general.setEliminarAsistencia(v);
					if (error == 0)
						mensaje = "Eliminaci�n registrada satisfactoriamente ";
					else if (error == -1)
						mensaje = "Esta reserva no se puede eliminar pues es de Tutor�a";
					else
						mensaje = "Problemas al eliminar " + error;
					error = -1;
				}
			}
			
			//System.out.println("getHorarioBloque i2");
			vec_horabloque1 = general.getHoraBloque(sala, anno,
					semestre);
			//System.out.println("getHorarioBloque o2");
			if (dia > 0 && bloque1 > 0 && bloque2 > 0 && anno > 0) {
				//System.out.println("getInscritosGimnasio i");
				vec_reserva = general.getInscritosGimnasio(anno,
						semestre, dia, f1, periodo, bloque1, bloque2,
						sala);
				//System.out.println("getInscritosGimnasio o");
				
				//System.out.println("getEstadoClaseGimnasio i1");
				vec_clase = general.getEstadoClaseGimnasio(f1,
						cod_asign, anno, semestre, paralelo, 1, 1, dia,
						bloque1, bloque2);
				//System.out.println("getEstadoClaseGimnasio o1");
				
				//System.out.println("getContAsistenciaGimnasio i2");
				espera = general.getContAsistenciaGimnasio(anno,
						semestre, dia, periodo, bloque1, bloque2, 5,
						f1, sala);
				//System.out.println("getContAsistenciaGimnasio o2");
			}
			if (vec_clase.size() > 0) {
				if (Integer.parseInt(vec_clase.get(0) + "") == 1)
					clase_cerrada = true;
				else if (Integer.parseInt(vec_clase.get(0) + "") == 2
						|| Integer.parseInt(vec_clase.get(0) + "") == 4)
					clase_anulada = true;
				else if (Integer.parseInt(vec_clase.get(0) + "") == 3
						|| Integer.parseInt(vec_clase.get(0) + "") == 5)
					clase_suspendida = true;

			}
			general.getCerrarConexion();
		}
	}

	/* Para activar opci�n para ingersar asistencia cuando
	   la clase ha sido cerrada, el perfil es administrador
	   y el periodo es el actual */
	boolean acceso_especial = false;
	Calendar c = Calendar.getInstance();
	int fa_d = c.get(Calendar.DATE);
	int fa_m = c.get(Calendar.MONTH) + 1;
	int fa_a = c.get(Calendar.YEAR);
	int i_fecha_actual = (fa_a * 10000) + (fa_m * 100) + (fa_d);
	int i_fecha_per1 = (per1_a * 10000) + (per1_m * 100) + (per1_d);
	int i_fecha_per2 = (per2_a * 10000) + (per2_m * 100) + (per2_d);

	if (clase_cerrada && cod_perfil == 2
			&& i_fecha_actual >= i_fecha_per1
			&& i_fecha_actual <= i_fecha_per2)
		acceso_especial = true;
	/* fin acceso especial */

	String load = "";
	if (!fecha.trim().equals(""))
		load = "BuscaDia();";
	if (muestra) {
		if (!clase_cerrada && !clase_anulada && !clase_suspendida) {
			if (espera > 0) {
				load += "tablaListaEspera(0,0);";
			}
		}
		load += "IniciaBloques();";
		if (sala > 0 && bloque1 > 0 && bloque2 > 0
				&& !fecha.trim().equals(""))
			load += "CargarIframe(document.form_cuadro);Mensaje();";
		if (rutnum > 0)
			load += "BuscarUsuario();";
	}
%>
<%@page import="javax.xml.ws.handler.MessageContext"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Gimnasio - Asistencia</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<%
	if (acceso_ip) {
%>
<!-Calendario -->

<link type="text/css" rel="stylesheet"
	href="js/dhtml_calendar/dhtml_calendar.css?random=20051112"
	media="screen"></LINK>
<SCRIPT type="text/javascript"
	src="js/dhtml_calendar/dhtml_calendar.js?random=20060118"></script>


<!-Estilos -->
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script src="js/layer.js"></script>
<script src="js/valida_login.js"></script>
<script language="JavaScript" src="js/trim.js"></script>
<script src="js/centrar.js"></script>
<script LANGUAGE="JavaScript" TYPE="text/javascript">
  Asiste    = preCargaImg("imagen/gim_asistio.gif");
  Justifica = preCargaImg("imagen/gim_justifico.gif");
  Falta     = preCargaImg("imagen/gim_falto.gif");

  var le_rut   = new Array();
  var le_dvrut = new Array();
  var le_pat   = new Array();
  var le_mat   = new Array();
  var le_nom   = new Array();
  var le_sex   = new Array();
  var le_tip   = new Array();
  var le_res   = new Array();

  var total_inscritos = 0;
  var total_ausentes = 0;

<%  if (vec_reserva.size() > 0){
le_rut.append("\"0\"");
le_dvrut.append("\"0\"");
le_pat.append("\"0\"");
le_mat.append("\"0\"");
le_nom.append("\"0\"");
le_sex.append("\"0\"");
le_tip.append("\"0\"");
le_res.append("\"0\"");
int total_inscritos = 0;
int total_ausentes = 0;

for(int i = 0; i < vec_reserva.size();i++){
  if (Integer.parseInt(((Vector) vec_reserva.get(i)).get(8)+ "") == 4){
    le_rut.append(",\"");
    le_rut.append(((Vector)vec_reserva.get(i)).get(2));
    le_rut.append("\"");
    le_dvrut.append(",\"");
    le_dvrut.append(((Vector)vec_reserva.get(i)).get(3));
    le_dvrut.append("\"");
    le_pat.append(",\"");
    le_pat.append(((Vector)vec_reserva.get(i)).get(4));
    le_pat.append("\"");
    le_mat.append(",\"");
    le_mat.append(((Vector)vec_reserva.get(i)).get(5));
    le_mat.append("\"");
    le_nom.append(",\"");
    le_nom.append(((Vector)vec_reserva.get(i)).get(6));
    le_nom.append("\"");
    le_sex.append(",\"");
    le_sex.append(((Vector)vec_reserva.get(i)).get(7));
    le_sex.append("\"");
    le_tip.append(",\"");
    le_tip.append(((Vector)vec_reserva.get(i)).get(1));
    le_tip.append("\"");
    le_res.append(",\"");
    le_res.append(((Vector)vec_reserva.get(i)).get(0));
    le_res.append("\"");

  } else {  if (Integer.parseInt(((Vector) vec_reserva.get(i)).get(8)+ "") == 2 ||
                Integer.parseInt(((Vector) vec_reserva.get(i)).get(8)+ "") == 3 ||
                Integer.parseInt(((Vector) vec_reserva.get(i)).get(8)+ "") == 0)
                 total_ausentes += 1;
          total_inscritos += 1;

  }}
  
  out.println("");
  out.println("le_rut = new Array("+le_rut+");"); // rut
  out.println("");
  out.println("le_dvrut  = new Array("+le_dvrut+");"); // dv rut
  out.println("");
  out.println("le_pat  = new Array("+le_pat+");"); // paterno
  out.println("");
  out.println("le_mat  = new Array("+le_mat+");"); // materno
  out.println("");
  out.println("le_nom  = new Array("+le_nom+");"); // nombres
  out.println("");
  out.println("le_sex  = new Array("+le_sex+");"); // sexo
  out.println("");
  out.println("le_tip  = new Array("+le_tip+");"); // tipo
  out.println("");
  out.println("le_res = new Array("+le_res+");"); // resgim_id
  out.println("");
  out.println("");
  out.println("total_inscritos = "+total_inscritos+";"); // total_inscritos
  out.println("");
  out.println("");
  out.println("total_ausentes = "+total_ausentes+";"); // total_ausentes
  out.println("");

}
%>
  function Mensaje(){
  <%if(error<0){%>alert ('<%=mensaje%>');<%}%>
    }



    function noenter(evt) {
      if(window.event) key = evt.keyCode;
      else key = evt.which;
      return !(key == 13);
    }
    function enter(evt){
      if(window.event) key = evt.keyCode;
      else key = evt.which;
      if (key == 13)
      validar(document.fagregar);
    }
/*    function enter(e){
      if (window.event) {
        if (window.event.keyCode == 13)
          validar(document.fagregar);

      }
      else {
        if (e.which == 13)
          validar(document.fagregar);
      }
    }*/


    function validar(form){
      if(trim(form.rut_aux.value)==""){
        alert("Debe ingresar el RUT a registrar");
        form.rut_aux.focus();
        return false;
      }
      if (!valida_rut2(form.rut_aux, form.rutnum, form.dvrut, 'RUT')) {
        form.rut_aux.focus();
        return false;
      }
      // return true;
      if(!checkRutField(document.fagregar.rut_aux.value, document.fagregar)){
			return false;
		}else{
			document.fagregar.submit();						
			return true;
		}
       BuscarUsuario();
       return true;
    }
    function Grabar(f){
       f.submit();
    }

    function BuscarUsuario(){      
      document.form_datos.rutnum.value = document.fagregar.rutnum.value;
      document.form_datos.rut_aux.value = document.fagregar.rut_aux.value;     
      document.form_datos.submit();

    }
    
    function BuscarUsuario(rutnum,rut_aux){    
      document.form_datos.rutnum.value = rutnum;
      document.form_datos.rut_aux.value = rut_aux;   
      document.form_datos.submit();

    }

    function RenovarCuadro(){
       document.form_cuadro.submit();
    }

    function OpcionClase(f,opcion){
      var pregunta = "";
      if (opcion == 1) pregunta = "Ud. va a CERRAR LA CLASE definitivamente.\n� Desea continuar ?";
      if (opcion == 2) pregunta = "Ud. va a ANULAR o SUSPENDER LA CLASE definitivamente.\n� Desea continuar ?";
      if (opcion == 3){

        if (document.form_razon.razon.value == ""){
          alert('Debe ingresar Raz�n de la anulaci�n o suspensi�n');
          return false;
        }
        f.razon.value = document.form_razon.razon.value;
       
       if (document.form_razon.opc[0].checked)
        f.accion.value = "2" ;
       else
         f.accion.value = "3" ;

        pregunta = "Su mensaje de ANULACI�N o SUSPENSI�N ser� enviado a la Bit�cora.\n� Desea continuar ?";
  		
  		
      } else   f.accion.value = opcion;
      var respuesta = confirm(pregunta);
	
      if (respuesta == true) {
        // Anular
        if (opcion == 2) {
          escondeBloque('BLOQUE_anular', 2);  // muestra
          escondeBloque('BLOQUE_asistenciatotal', 1); // escondo
        }
        else{
          f.submit();

        }
      }
    }

    function DesHacerAnular(){
      escondeBloque('BLOQUE_asistenciatotal' , 2); // muestra
      escondeBloque('BLOQUE_anular'  , 1); // escondo
      return true;
    }

    function preCargaImg(imageURL) {
      image = new Image();
      image.src = imageURL;
      return image;
    }

    function EliminarInscritos(f) {
      pregunta = "Su reserva ser� eliminada.\n� Desea continuar ?";
      var respuesta = confirm(pregunta);
      if (respuesta == true) {
         f.accion.value = "5"; // eliminar inscrito
         f.action = "gim_asistencia_adm.jsp";
         f.target = "_self";
         f.submit();
      }
    }

    function CambiarAsistenciaXXXXXX(i,accion,f, origen) {
      f.action = "gim_proceso.jsp";
      f.target = "iframe_inscritos";
      if (accion == 'Asiste') {
        if (origen==2){
          document.getElementById('accion'+i).onclick=
              function(){CambiarAsistencia(i,'Falta',f,origen);};
          document ['ASISTENCIA'+i].src = Falta.src;
          f.asistencia.value = 3;
        } else {
        document.getElementById('accion'+i).onclick=
            function(){CambiarAsistencia(i,'Justifica',f,origen);};
        document ['ASISTENCIA'+i].src = Justifica.src;
        f.asistencia.value = 2;
         }
      }

      if (accion == 'Justifica'){
        document.getElementById('accion'+i).onclick=
            function(){CambiarAsistencia(i,'Falta',f,origen);};
        document ['ASISTENCIA'+i].src = Falta.src;
        f.asistencia.value = 3;
      }

      if (accion == 'Falta') {
        document.getElementById('accion'+i).onclick=
            function(){CambiarAsistencia(i,'Asiste',f,origen);};
        document ['ASISTENCIA'+i].src = Asiste.src;
        f.asistencia.value = 1;
      }

      Grabar(f);
      RenovarCuadro();
      // Limpia las filas con bold
      var total_filas    = total_inscritos ;
      var total_columnas = 7;
      if (total_filas > 0){
       for (var y=0 ; y < total_filas ;y++){
         for (var x=1 ; x <= total_columnas ; x++){
          document.getElementById('insc_'+x+'_'+y).style.fontWeight='normal';
        }
      }
      }
      // Marca la fila con bold
      for (var c=1 ; c <= total_columnas ; c++){
        document.getElementById('insc_'+c+'_'+i).style.fontWeight='bold';
      }
      return true;
    }

    function CambiarAsistencia(i,accion,origen) {
     document.getElementById("formINSC_grabar"+i).action = "gim_proceso.jsp";
      document.getElementById("formINSC_grabar"+i).target = "iframe_inscritos";
      if (accion == 'Asiste') {
        if (origen==2){
          document.getElementById("formINSC_grabar"+i).asistencia.value = 3;
        } else {
          document.getElementById("formINSC_grabar"+i).asistencia.value = 2;
         }
      }

      if (accion == 'Justifica'){
        document.getElementById("formINSC_grabar"+i).asistencia.value = 3;
      }

      if (accion == 'Falta') {
        document.getElementById("formINSC_grabar"+i).asistencia.value = 1;
        origen = 1;
      }

      document.getElementById("formINSC_grabar"+i).par_i.value = i;
      document.getElementById("formINSC_grabar"+i).par_accion.value = accion;
      document.getElementById("formINSC_grabar"+i).par_origen.value = origen;
      document.getElementById("formINSC_grabar"+i).submit();

      RenovarCuadro();

      return true;
    }

    function CambiarImagen(i,accion,origen) {
     if (accion == 'Asiste') {
        if (origen==2){
          document.getElementById('accion'+i).onclick=
              function(){CambiarAsistencia(i,'Falta',origen);};
          document ['ASISTENCIA'+i].src = Falta.src;
        } else {
        document.getElementById('accion'+i).onclick=
            function(){CambiarAsistencia(i,'Justifica',origen);};
        document ['ASISTENCIA'+i].src = Justifica.src;
         }
      }

      if (accion == 'Justifica'){
        document.getElementById('accion'+i).onclick=
            function(){CambiarAsistencia(i,'Falta',origen);};
        document ['ASISTENCIA'+i].src = Falta.src;
      }

      if (accion == 'Falta') {
        document.getElementById('accion'+i).onclick=
            function(){CambiarAsistencia(i,'Asiste',origen);};
        document ['ASISTENCIA'+i].src = Asiste.src;
      }

      // Limpia las filas con bold
      var total_filas    = total_inscritos ;
      var total_columnas = 7;
      if (total_filas > 0){
       for (var y=0 ; y < total_filas ;y++){
         for (var x=1 ; x <= total_columnas ; x++){
          document.getElementById('insc_'+x+'_'+y).style.fontWeight='normal';
        }
      }
      }
      // Marca la fila con bold
      for (var c=1 ; c <= total_columnas ; c++){
        document.getElementById('insc_'+c+'_'+i).style.fontWeight='bold';
      }
      return true;
    }



   <%if(clase_cerrada || clase_anulada || clase_suspendida){%>
     function CambiarAusenciaAsistencia(i,accion) {

     document.getElementById("formASIST_grabar"+i).action = "gim_proceso.jsp";
     document.getElementById("formASIST_grabar"+i).target = "iframe_ausentes";

     if (accion == 'Justifica'){
         document.getElementById("formASIST_grabar"+i).asistencia.value = 3;
     }

     if (accion == 'Falta') {
          document.getElementById("formASIST_grabar"+i).asistencia.value = 2;
     }
     document.getElementById("formASIST_grabar"+i).par_i.value = i;
     document.getElementById("formASIST_grabar"+i).par_accion.value = accion;
     document.getElementById("formASIST_grabar"+i).submit();

     RenovarCuadro();

     return true;
    }
    function CambiarImagenAusencia(i,accion) {
      if (accion == 'Justifica'){
       document.getElementById('accion_asis'+i).onclick=
           function(){CambiarAusenciaAsistencia(i,'Falta');};
       document ['AUSENCIA'+i].src = Falta.src;
      }

     if (accion == 'Falta') {
       document.getElementById('accion_asis'+i).onclick=
           function(){CambiarAusenciaAsistencia(i,'Justifica');};
       document ['AUSENCIA'+i].src = Justifica.src;
     }

    // Limpia las filas con bold
     var total_filas    = total_ausentes;
     var total_columnas = 7;
     if (total_filas > 0){
      for (var y=1 ; y <= total_filas ;y++){
        for (var x=1 ; x <= total_columnas ; x++){
         document.getElementById('asist_'+x+'_'+y).style.fontWeight='normal';
       }
     }
     }
     // Marca la fila con bold
     for (var c=1 ; c <= total_columnas ; c++){
       document.getElementById('asist_'+c+'_'+i).style.fontWeight='bold';
     }
     return true;
    }

    function CambiarAusenciaAsistenciaxxxxxxxxx(i,accion,f) {
         f.action = "gim_proceso.jsp";
         f.target = "iframe_ausentes";

         if (accion == 'Justifica'){
           document.getElementById('accion'+i).onclick=
               function(){CambiarAusenciaAsistencia(i,'Falta',f);};
           document ['AUSENCIA'+i].src = Falta.src;
           f.asistencia.value = 3;
         }

         if (accion == 'Falta') {
           document.getElementById('accion'+i).onclick=
               function(){CambiarAusenciaAsistencia(i,'Justifica',f);};
           document ['AUSENCIA'+i].src = Justifica.src;
           f.asistencia.value = 2;
         }

         Grabar(f);
         RenovarCuadro();
         // Limpia las filas con bold
         var total_filas    = total_ausentes;
         var total_columnas = 7;
         if (total_filas > 0){
          for (var y=0 ; y <= total_filas ;y++){
            for (var x=1 ; x <= total_columnas ; x++){
             document.getElementById('insc_'+x+'_'+y).style.fontWeight='normal';
           }
         }
         }
         // Marca la fila con bold
         for (var c=1 ; c <= total_columnas ; c++){
           document.getElementById('insc_'+c+'_'+i).style.fontWeight='bold';
         }
         return true;
    }    
    
<%}%>

function MouseOver(t){
  t.style.background="#F4F4F4";
}
function MouseOut(t, color){
  t.style.background=color;
}
function tablaListaEspera(op,fila){
texto = "";
  if (le_rut.length > 1){
  texto=
      "<table width='100%'  border='0' cellspacing='0' cellpadding='2'>"+
      "<tr>"+
      "<td height='13' background='imagen/boton_baseFONDO04.gif'><span class='Estilo_Blanco'>N&deg;</span></td>"+
      "<td background='imagen/boton_baseFONDO04.gif'><span class='Estilo_Blanco'>RUT</span></td>"+
      "<td background='imagen/boton_baseFONDO04.gif'><span class='Estilo_Blanco'>Paterno</span></td>"+
      "<td background='imagen/boton_baseFONDO04.gif'><span class='Estilo_Blanco'>Materno</span></td>"+
      "<td background='imagen/boton_baseFONDO04.gif'><span class='Estilo_Blanco'>Nombres</span></td>"+
      "<td align='center' background='imagen/boton_baseFONDO04.gif'><span class='Estilo_Blanco'>Sexo</span></td>"+
      "<td align='center' background='imagen/boton_baseFONDO04.gif'><span class='Estilo_Blanco'>Usuario</span></td>"+
      "<td align='center' background='imagen/boton_baseFONDO04.gif'><span class='Estilo_Blanco'>Acci&oacute;n</span></td>"+
      "</tr>";

  for (var i=1 ; i < le_rut.length ; i++){
    var b1 = "";
    var b2 = "";
    if ((op == 1 && i == fila-1) || // Sube
        (op == 2 && i == fila+1)){  // Baja
      b1 = "<b>";
      b2 = "</b>";
    }
    texto += "<tr bgcolor=#E8E8E8 onmouseover='MouseOver(this)' onmouseout='MouseOut(this,\"#E8E8E8\")'>";
    texto += "<td width='4%'  class='Estilo_VerdeOsc letra7'>" + b1 + (i) + b2 + "</td>"+
             "<td width='15%' class='Estilo_VerdeOsc letra7'>" + b1 + le_rut[i] + "-" + le_dvrut[i] + b2 + "</td>"+
             "<td width='22%' class='Estilo_VerdeOsc letra7'>" + b1 + le_pat[i] + b2 + "</td>"+
             "<td width='20%' class='Estilo_VerdeOsc letra7'>" + b1 + le_mat[i] + b2 + "</td>"+
             "<td width='22%' class='Estilo_VerdeOsc letra7'>" + b1 + le_nom[i] + b2 + "</td>"+
             "<td width='4%' align='center' class='Estilo_VerdeOsc letra7'>" + b1 + le_sex[i] + b2 + "</td>"+
             "<td width='4%' align='center' class='Estilo_VerdeOsc letra7'>" + b1 + le_tip[i] + b2 + "</td>";

    if (i != 1 && i != le_rut.length-1){
      texto += "<td width='9%' align='right' nowrap>"+
               "<a href='javascript:void(0)' onclick='CambiaFila(1,"+ i +","+le_rut[i]+",\""+le_dvrut[i]+"\","+le_res[i]+")'>"+
               "<img src='imagen/gim_arrow_up.gif' alt='SUBIR' border=0 align='absmiddle'></a>&nbsp;"+
               "<a href='javascript:void(0)' onclick='CambiaFila(2,"+ i +","+le_rut[i]+",\""+le_dvrut[i]+"\","+le_res[i]+")'>"+
               "<img src='imagen/gim_arrow_down.gif' alt='BAJAR' border=0 align='absmiddle'></a>&nbsp;"+
               "<a href='javascript:void(0)' onclick='Eliminar("+ i +","+le_rut[i]+",\""+le_dvrut[i]+"\","+le_res[i]+")'>"+
               "<img src='imagen/gim_basurero.gif' alt='ELIMINAR' border=0 align='absmiddle'></a></td>";
    }
    else {
      texto += "<td width='9%' align='right' nowrap>";
      if(le_rut.length == 2)     texto += "<a href='javascript:void(0)' onclick='Eliminar("+ i +","+le_rut[i]+",\""+le_dvrut[i]+"\","+le_res[i]+")'><img src='imagen/gim_basurero.gif' alt='ELIMINAR' border=0 align='absmiddle'></a></td>";
      else {
        if (i == 1) texto += "<a href='javascript:void(0)' onclick='CambiaFila(2,"+ i +","+le_rut[i]+",\""+le_dvrut[i]+"\","+le_res[i]+")'><img src='imagen/gim_arrow_down.gif' alt='BAJAR' border=0 align='absmiddle'></a>&nbsp;";
        if (i == le_rut.length-1) texto += "<a href='javascript:void(0)' onclick='CambiaFila(1,"+ i +","+le_rut[i]+",\""+le_dvrut[i]+"\","+le_res[i]+")'><img src='imagen/gim_arrow_up.gif' alt='SUBIR' border=0 align='absmiddle'></a>&nbsp;";
        texto += "<a href='javascript:void(0)' onclick='Eliminar("+ i +","+le_rut[i]+",\""+le_dvrut[i]+"\","+le_res[i]+")'><img src='imagen/gim_basureroS.gif' alt='ELIMINAR' border=0 align='absmiddle'></a></td>";
      }
    }       
     texto += "</tr>";
  }
  texto += "</table>";

  }
    document.getElementById('TABLA_lista_espera').innerHTML = texto;
}

function CambiaFila(op,i,rut,dv,res){
  Mover(op,i,le_rut);
  Mover(op,i,le_dvrut);
  Mover(op,i,le_pat);
  Mover(op,i,le_mat);
  Mover(op,i,le_nom);
  Mover(op,i,le_sex);
  Mover(op,i,le_tip);
  
  document.formLE_grabar.operacion.value = op;
  document.formLE_grabar.rut_consulta.value = rut;
  document.formLE_grabar.dvrut_consulta.value = dv;
  document.formLE_grabar.par_i.value = i;
  document.formLE_grabar.resgim_id.value = res;
  document.formLE_grabar.submit();
  
  
  
  /*document.getElementById('formLE_grabar'+(i+1)).operacion.value = op;
  document.getElementById('formLE_grabar'+(i+1)).par_i.value = i;
  document.getElementById('formLE_grabar'+(i+1)).submit();*/

//tablaListaEspera(op,i);
}

function CambiaImagenFila(op,i){
   tablaListaEspera(op,i);
}

function Mover(op,i,arreglo){
  temporal = arreglo[i];
  var imas=i+1;
  var borrado;
  
  // Sube
  if (op == 1) {
    arreglo[i]=arreglo[i-1];
    arreglo[i-1]=temporal;
  }
  // Baja
  if (op == 2) {
    arreglo[i]=arreglo[i+1];
    arreglo[i+1]=temporal;
  }
  // Borra
  if (op == 3) {
  	if (imas == arreglo.length){
		borrado = arreglo.pop();
     }else{
    arreglo.splice(i,1);
    }
  }
}

function Eliminar(i,rut,dv,res){
  var pregunta = "Ud. va a ELIMINAR al alumno de la lista de espera.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  if (respuesta == true) {
    CambiaFila(3,i,rut,dv,res);
    return true;
  }
}

function IniciaBloques(){
  if (<%=sala%> > 0 && <%=bloque1%> > 0 && <%=bloque2%> > 0 && '<%=fecha.trim()%>' != '')
    escondeBloque('BLOQUE_asistenciatotal', 2); // muestro
  // escondeBloque('BLOQUE_anular' , 1); // escondo
  return true;
}
function Consultar(f){
  if (f.sala.value <= 0){
    alert ('Debe seleccionar sala a consultar.');
    f.sala.focus();
    return false;
  }
  if (f.fecha_inicio.value == ''){
    alert ('Debe ingresar fecha a consultar.');
    return false;
  }
  if (f.bloque2.value <= 0 || f.bloque1.value <= 0){
    alert ('Debe seleccionar bloque a consultar.');
    f.bloque.focus();
    return false;
  }
 f.submit();

}
function CargarIframe(f){
  f.submit();
}


arr_bloque_Cod   = new Array();
arr_bloque_Text  = new Array();
arr_bloque_Prof  = new Array();
arr_bloque_Rese  = new Array();
arr_bloque_Blo1  = new Array();
arr_bloque_Blo2  = new Array();
arr_bloque_Asign  = new Array();
arr_bloque_Par  = new Array();

<%
  StringBuffer bloque_Cod = new StringBuffer(); StringBuffer bloque_Text = new StringBuffer(); StringBuffer bloque_Prof = new StringBuffer(); StringBuffer bloque_Rese = new StringBuffer(); StringBuffer bloque_Blo1 = new StringBuffer(); StringBuffer bloque_Blo2 = new StringBuffer(); StringBuffer bloque_Asign = new StringBuffer();  StringBuffer bloque_Par = new StringBuffer();general.getAbrirConexion(); general.setGenerarConexionHorario();
for (int d = 0; d < 6; d++){
  bloque_Cod = new StringBuffer();
  bloque_Text = new StringBuffer();
  bloque_Prof = new StringBuffer();
  bloque_Rese = new StringBuffer();
  bloque_Blo1 = new StringBuffer();
  bloque_Blo2 = new StringBuffer();
  bloque_Asign = new StringBuffer();
  bloque_Par = new StringBuffer();
  vec_horariodia = general.getHorarioDia (anno, semestre,  sala, d+1, f1);
  bloque_Cod.append("\"0\"");
  if (vec_horariodia.size() == 0) bloque_Text.append("\"-- Sin asignaci�n de horario --\"");
  else bloque_Text.append("\"-- Seleccione --\"");
  bloque_Prof .append("\"\"");
  bloque_Rese.append("\"\"");
  bloque_Blo1.append("\"0\"");
  bloque_Blo2.append("\"0\"");
  bloque_Asign.append("\"0\"");
  bloque_Par.append("\"0\"");

  for (int i = 0; i < vec_horariodia.size();i++){
    bloque_Cod.append(",\"");
    bloque_Cod.append(i); // indice
    bloque_Cod.append("\"");

    bloque_Text.append(",\"");
    if (sala != 10335) {
      bloque_Text.append(((Vector)vec_horariodia.get(i)).get(7));
      bloque_Text.append(" - ");
    }
    bloque_Text.append(((Vector)vec_horariodia.get(i)).get(4));
    bloque_Text.append(" a ");
    bloque_Text.append(((Vector)vec_horariodia.get(i)).get(5));
    bloque_Text.append("\"");
    // Profesor
    bloque_Prof.append(",\"");
    bloque_Prof.append(((Vector)vec_horariodia.get(i)).get(9));
    bloque_Prof.append("\"");
    // tipo reserva
    bloque_Rese.append(",\"");
    bloque_Rese.append(((Vector)vec_horariodia.get(i)).get(8));
    bloque_Rese.append("\"");
    // bloque 1
    bloque_Blo1.append(",\"");
    bloque_Blo1.append(((Vector)vec_horariodia.get(i)).get(2));
    bloque_Blo1.append("\"");
    // bloque 2
    bloque_Blo2.append(",\"");
    bloque_Blo2.append(((Vector)vec_horariodia.get(i)).get(3));
    bloque_Blo2.append("\"");
    // cod_asign
    bloque_Asign.append(",\"");
    bloque_Asign.append(((Vector)vec_horariodia.get(i)).get(0));
    bloque_Asign.append("\"");
    // paralelo
    bloque_Par.append(",\"");
    bloque_Par.append(((Vector)vec_horariodia.get(i)).get(1));
    bloque_Par.append("\"");
  }
  out.println("arr_bloque_Cod["+d+"]  = new Array("+bloque_Cod+");"); // codigo
  out.println("arr_bloque_Text["+d+"] = new Array("+bloque_Text+");"); // texto
  out.println("arr_bloque_Prof["+d+"] = new Array("+bloque_Prof+");"); // Profesor
  out.println("arr_bloque_Rese["+d+"] = new Array("+bloque_Rese+");"); // Tipo reserva
  out.println("arr_bloque_Blo1["+d+"] = new Array("+bloque_Blo1+");"); // Bloque 1
  out.println("arr_bloque_Blo2["+d+"] = new Array("+bloque_Blo2+");"); // Bloque 2
  out.println("arr_bloque_Asign["+d+"] = new Array("+bloque_Asign+");"); // cod asign
  out.println("arr_bloque_Par["+d+"] = new Array("+bloque_Par+");"); // paralelo
} general.getCerrarConexion(); %>

  function CambiaDia(){
  var dia = (document.fingresar.dia.value/1)-1
// Limpia el combo
  document.fingresar.bloque.options.length = 0; // Ciclo que llena el combo
  if (dia >= 0) {
    for(i = 0; i < arr_bloque_Cod[dia].length; i++) {
      // Crea OPTION del SELECT
      var option = new
            Option(arr_bloque_Text[dia][i],arr_bloque_Cod[dia][i]);
      document.fingresar.bloque.options[i] = option;
      if(arr_bloque_Cod[dia][i] == <%=bloque%>){
        document.fingresar.bloque.options[i].selected = true;
      }
    }
  }
  CambiaBloque();
}

function CambiaBloque(){
  var dia = (document.fingresar.dia.value/1)-1;
  var bloque = document.fingresar.bloque.selectedIndex;
  document.getElementById("texto_tit_prof").innerHTML = "";
  document.getElementById("texto_tit_tipo").innerHTML = "";
  document.getElementById("texto_prof").innerHTML = "";
  document.getElementById("texto_tipo").innerHTML = "";
  escondeBloque('BLOQUE_BtnConsultar', 1); // escondo
  document.fingresar.bloque1.value = "0";
  document.fingresar.bloque2.value = "0";
//  document.form_accion.bloque1.value = "0";
//  document.form_accion.bloque2.value = "0";
  if (dia >= 0 && bloque > 0) {
    document.getElementById("texto_tit_prof").innerHTML = "Profesor";
    document.getElementById("texto_tit_tipo").innerHTML = ""; // Clase
    document.getElementById("texto_prof").innerHTML = arr_bloque_Prof[dia][bloque] +
                                                      " (" + arr_bloque_Rese[dia][bloque] + ")";
    document.getElementById("texto_tipo").innerHTML = ""; // arr_bloque_Rese[dia][bloque]
    document.fingresar.bloque1.value = arr_bloque_Blo1[dia][bloque];
    document.fingresar.bloque2.value = arr_bloque_Blo2[dia][bloque];
    document.form_accion.cod_asign.value = arr_bloque_Asign[dia][bloque];
    document.form_accion.paralelo.value = arr_bloque_Par[dia][bloque];
    document.form_accion.bloque1.value = arr_bloque_Blo1[dia][bloque];
    document.form_accion.bloque2.value = arr_bloque_Blo2[dia][bloque];
    document.fingresar.cod_asign.value = arr_bloque_Asign[dia][bloque];
    document.fingresar.paralelo.value = arr_bloque_Par[dia][bloque];
    document.form_datos.cod_asign.value = arr_bloque_Asign[dia][bloque];
    document.form_datos.paralelo.value = arr_bloque_Par[dia][bloque];
    escondeBloque('BLOQUE_BtnConsultar', 2); // muestro
  }
}
function BuscaDia(){
  arr_dia  = new Array("LUNES","MARTES","MI�RCOLES","JUEVES","VIERNES","S�BADO","DOMINGO");

  var fecha = document.fingresar.fecha_inicio.value;
  if (fecha > "") {
    var dd = (fecha.substring(0,2)/1);
    var mm = (fecha.substring(3,5)/1) -1;
    var aa = (fecha.substring(6,10)/1);
    var fdate = new Date (aa, mm, dd);
    var dia = fdate.getDay();
    if (dia > 0) {
      document.fingresar.dia.value = dia;
      document.getElementById("texto_dia").innerHTML = arr_dia[dia-1];
      if(document.fingresar.sala.value > 0 && document.fingresar.bloque != null)
      CambiaDia();
      document.form_cuadro.dia.value = dia;
          }
    else alert("Seleccione un d�a de LUNES a S�BADO.");
  }
  else alert("Seleccione una fecha.");

}

function validaRut(){	
		if(!checkRutField(document.fagregar.rut_aux.value, document.fagregar)){		    
			document.fagregar.rut_aux.value='';			
		}else {
			var tmpstr = "";
			var rut_val = document.fagregar.rut_aux.value;
			for ( i=0; i < rut_val.length ; i++ )
				if ( rut_val.charAt(i) != ' ' && rut_val.charAt(i) != '.' && rut_val.charAt(i) != '-' )
					tmpstr = tmpstr + rut_val.charAt(i);
			tmpstr = tmpstr.substr(0,(tmpstr.length-1))		
			document.fagregar.rutnum.value = parseInt(tmpstr);
			BuscarUsuario(document.fagregar.rutnum.value, document.fagregar.rut_aux.value);
		}
    }
// -->
</SCRIPT>
<%
	}
%>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
</head>
<%
	if (acceso_ip) {
%>
<body onload="<%=load%>">
<form name="form_cuadro" method="post"
	action="gim_asistencia_cuadro_adm.jsp" target="iframe_cuadro"><input
	type="hidden" name="sala" value="<%=sala%>"> <input
	type="hidden" name="fecha_inicio" value="<%=fecha%>"> <input
	type="hidden" name="dia" value="<%=dia%>"> <input type="hidden"
	name="bloque1" value="<%=bloque1%>"> <input type="hidden"
	name="bloque2" value="<%=bloque2%>">
	<input type="hidden" name="cod_asign" value="<%=cod_asign%>">
</form>
	<input type="hidden" name="rutnum" value="<%=rutnum%>"> 
	<input type="hidden" name="dvrut"  value="<%=dvrut%>"> 
<form name="form_datos" method="post" action="gim_datos_adm.jsp"
	target="iframe_datos">

	
	<input type="hidden" name="rutnum" value="">
	<input type="hidden" name="rut_aux" value="">
	<input type="hidden" name="periodo" value="<%=periodo%>"> <input
	type="hidden" name="a�o" value="<%=anno%>"> <input
	type="hidden" name="semestre" value="<%=semestre%>"> <input
	type="hidden" name="cod_asign" value="<%=cod_asign%>"> <input
	type="hidden" name="paralelo" value="<%=paralelo%>"></form>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td width="100%" height="37" class="Estilo_Verde">ASISTENCIA</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<form name="fingresar" method="post" action="gim_asistencia_adm.jsp"
			target="iframe">
		<td valign="top" bgcolor="#FFFFFF">
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td width="20%"><span class="Estilo_Gris">Sala</span></td>
				<td><select name="sala" class="Option" id="sala"
					onchange="BuscaDia();if(document.fingresar.fecha_inicio.value != '') {document.fingresar.submit();}">
					<option value="0">-- Seleccione --</option>
					<option value="10335" <%=(sala == 10335)?"selected":""%>>M&aacute;quinas</option>
					<option value="10336" <%=(sala == 10336)?"selected":""%>>Multiuso</option>
				</select></td>
			</tr>

			<tr>
				<td><span class="Estilo_Gris">Fecha</span></td>
				<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="33%"><input id="fecha_lanzador"
							name="fecha_inicio" type="text" class="Input01"
							value="<%=fecha%>" size="10" maxlength="14"
							onChange="document.fingresar.bloque.value='0';document.fingresar.bloque1.value='0';document.fingresar.bloque2.value='0';document.fingresar.submit();"
							readonly></td>
						<td width="67%" valign="bottom"><a href="javascript:void(0)"
							onclick="displayCalendar(document.fingresar.fecha_inicio,'dd/mm/yyyy',this)">
						<img src="imagen/gim_calendario_verde.gif" border="0"
							alt="CALENDARIO" width="16" height="16"></a> &nbsp;<b><span
							class="Estilo_Azul" id="texto_dia"></span></b> <input type='hidden'
							name='dia' value='0'></td>
					</tr>
				</table>
				</td>
			</tr>
			<%
				if (sala > 0) {
			%>
			<tr>
				<td><span class="Estilo_Gris">Bloque</span></td>
				<td><span class="Estilo_Gris"> <select name="bloque"
					class="Option" id="bloque"
					onChange="CambiaBloque();document.fingresar.submit();">
					<option value="0">-- Sin asignaci�n de horario --</option>

				</select> </span></td>
			</tr>
			<tr>
				<td height="16"><span class="Estilo_Gris" id="texto_tit_prof"></span>&nbsp;</td>
				<td height="16"><span class="Estilo_Azul" id="texto_prof"></span>&nbsp;</td>
			</tr>
			<%
				if (clase_cerrada || clase_anulada || clase_suspendida) {
			%>
			<tr>
				<td>&nbsp;</td>
				<td>
				<%
					if (clase_cerrada) {
				%> <span class='Estilo_Azul'><B>CLASE
				CERRADA</B></span> <%
 	} else {
 					if (clase_anulada) {
 %> <span class='Estilo_Rojo'><B>CLASE
				ANULADA</B></span> <%
 	} else {
 						if (clase_suspendida) {
 %><span class='Estilo_Rojo'><B>CLASE
				SUSPENDIDA</B></span>
				<%
					}
									}
								}
				%>
				</td>
			</tr>
			<%
				}
			%>
			<tr>
				<td height="15"><span class="Estilo_Gris" id="texto_tit_tipo"></span>&nbsp;</td>
				<td height="15"><span class="Estilo_Azul" id="texto_tipo"></span>
				<%
					if (clase_cerrada || clase_anulada || clase_suspendida) {
				%> <span
					id="BLOQUE_BtnConsultar">&nbsp;</span> <%
 	} else {
 %>
				<table id="BLOQUE_BtnConsultar" style="display: none" width="91"
					height="21" border="0" cellpadding="0" cellspacing="0"
					background="imagen/boton_baseLEER.gif">
					<tr onClick="Consultar(document.fingresar);"
						onMouseOver="BotonOver(this,'Consultar')"
						onMouseOut="BotonOut(this)">
						<td width="26">&nbsp;</td>
						<td width="65" class="Estilo_Azulino">Consultar</td>
					</tr>
				</table>
				<%
					}
				%>
				</td>
			</tr>

			<input type='hidden' name='bloque1' value="<%=bloque1%>">
			<input type='hidden' name='bloque2' value="<%=bloque2%>">
			<input type="hidden" name="cod_asign" value="<%=cod_asign%>">
			<input type="hidden" name="paralelo" value="<%=paralelo%>">
			<%
				}
			%>
		</table>
		</td>
		<td width="45%" align="right" valign="top"><iframe
			name="iframe_cuadro" scrolling="no" frameborder="0" width="250"
			height="95" src="gim_asistencia_cuadro_adm.jsp"></iframe></td>
		</form>
	</tr>

</table>
<%
	if (muestra) {
%>
<DIV id="BLOQUE_asistenciatotal" style="display: none"><br>
<%
	if ((!clase_cerrada || acceso_especial) && !clase_anulada
					&& !clase_suspendida) {
%>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td height="25"
			class="Estilo_<% if (clase_cerrada){%>Rojo<%}else{%>VerdeOsc<%}%>">
		Ingresar asistencia <%
			if (clase_cerrada) {
		%>rezagada<%
			}
		%>
		</td>
		<td>
		</td>
	</tr>
</table>

<table width="100%" height="86" border="0" cellpadding="2"
	cellspacing="0">
	<tr>
		<form name="fagregar" method="post" action="gim_asistencia_adm.jsp"
			target="iframe">
		<td valign="top" bgcolor="#FFFFFF">
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td width="148" bgcolor="#FFFFFF"><span class="Estilo_Gris">RUT</span></td>
				<td colspan="2" bgcolor="#FFFFFF"><input type="text"
					name="rut_aux" size="12" maxlength="12" value="<%=rut_aux%>"
					onKeyPress="enter(event,1);return noenter(event)"
					
					class="Input01">
					 <img src="imagen/gim_lupa.gif" width="16" height="18" border="0" align="absmiddle" onClick="validaRut();">
					
					<input type="hidden" name="rutnum" value="<%=rutnum%>"> 
					<input type="hidden" name="dvrut"  value="<%=dvrut%>"> 
					<input type="hidden" name="accion" value="4"></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFFF"><span class="Estilo_Gris">Estado</span></td>
				<td width="119" bgcolor="#FFFFFF"><select name="cod_asistencia"
					class="Option" id="cod_asistencia">
					<option value="0">-- Seleccione --</option>
					<option value="1" selected>Asiste</option>
					<%
						if (!clase_cerrada) {
					%>
					<option value="2">Justifica</option>
					<option value="3">Falta</option>
					<%
						}
					%>
				</select></td>
				<td width="329" bgcolor="#FFFFFF">
				<table height="21" border="0" cellpadding="0" cellspacing="0">
					<tr
						onClick="validar(document.fagregar)"
						onMouseOver="BotonOver(this,'Ingresar')"
						onMouseOut="BotonOut(this)">
						<td width="21"><img
							src="imagen/boton_base<% if (clase_cerrada){%>ACC01<%}else{%>OK01<%}%>.gif"></td>
						<td width="60" align="center" valign="middle" nowrap
							background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">
						Ingresar</td>
						<td width="3"><img src="imagen/boton_baseBORDEDER.gif"
							width="3" height="21"></td>
					</tr>
				</table>

				</td>

			</tr>
		</table>
		</td>
		<td width="45%" valign="top"><iframe name="iframe_datos"
			scrolling="no" frameborder="0" width="320" height="70"
			src="gim_datos_adm.jsp"></iframe></td>		
		<input type="hidden" name="sala" value="<%=sala%>">
		<input type="hidden" name="fecha_inicio" value="<%=fecha%>">
		<input type="hidden" name="dia" value="<%=dia%>">
		<input type="hidden" name="bloque1" value="<%=bloque1%>">
		<input type="hidden" name="bloque2" value="<%=bloque2%>">
		<input type="hidden" name="bloque" value="<%=bloque%>">
		<input type="hidden" name="cod_asign" value="<%=cod_asign%>">
		<input type="hidden" name="paralelo" value="<%=paralelo%>">
		</form>
	</tr>
</table>
<%
	}
%> <%
 	if ((vec_reserva.size() - espera) > 0) {
 %>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td width="50%" height="30" class="Estilo_VerdeOsc">Usuarios
		inscritos</td>
		<td align="right"><iframe name="iframe_inscritos" scrolling="no"
			frameborder="0" height="20" src="gim_proceso.jsp"></iframe></td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr bgcolor="#999999">
		<td width="100%" class="Estilo_Gris">
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr bgcolor="#008040" class="Estilo_GrisOsc">
				<td height="13" background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">N&deg;</span></td>
				<td background="imagen/boton_baseFONDO04.gif" bgcolor="#008040"><span
					class="Estilo_Blanco">RUT</span></td>
				<td background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">Paterno</span></td>
				<td background="imagen/boton_baseFONDO04.gif" bgcolor="#008040"><span
					class="Estilo_Blanco">Materno</span></td>
				<td background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">Nombres</span></td>
				<td align="center" background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">Sexo</span></td>
				<td align="center" background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">Usuario</span></td>
				<td align="center" background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">Asist.</span></td>
				<%
					if (!clase_cerrada && !clase_anulada
										&& !clase_suspendida) {
				%>
				<td align="center" background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">Eliminar</span></td>
				<%
					}
				%>
			</tr>
			<%
				String texto = "";
							String imagen = "";
							int cont = 1;
							String color = "";
							int muestra_icono = 0;
							for (int i = 0; i < vec_reserva.size(); i++) {
								if (Integer.parseInt(((Vector) vec_reserva.get(i))
										.get(8)
										+ "") != 4) {
									origen_asistencia = Integer
											.parseInt(((Vector) vec_reserva.get(i))
													.get(11)
													+ "");
									muestra_listado = true;
									if ((clase_cerrada || clase_anulada || clase_suspendida)
											&& Integer
													.parseInt(((Vector) vec_reserva
															.get(i)).get(8)
															+ "") != 1)
										muestra_listado = false;
									switch (Integer.parseInt(((Vector) vec_reserva
											.get(i)).get(8)
											+ "")) {
									case 1: {// justifico
										texto = "Asiste";
										imagen = "imagen/gim_asistio.gif";
										break;
									}
									case 2: {
										texto = "Justifica";
										imagen = "imagen/gim_justifico.gif";
										break;
									}
									default: {
										texto = "Falta";
										imagen = "imagen/gim_falto.gif";
										break;
									}
									}
									String estilo_color = "";
									switch (Integer.parseInt(((Vector) vec_reserva
											.get(i)).get(10)
											+ "")) {
									case 2: {// tutoria
										color = "#D2E5D2";
										estilo_color = "Estilo_MoradoClaro";
										break;
									}
									case 3: { // rama
										color = "#ECE5CC";
										estilo_color = "Estilo_Cafe";
										break;
									}
									default: { // normales
										color = "#E8E8E8";
										estilo_color = "Estilo_Azulino";
										break;
									}
									}

									if (muestra_listado) {
			%>
			<form name="formINSC_grabar<%=i%>" id="formINSC_grabar<%=i%>"
				method="post" action="" target="">
			<tr bgcolor="<%=color%>" onmouseover="MouseOver(this)"
				onmouseout="MouseOut(this, '<%=color%>')">
				<td id="insc_1_<%=i%>" width="4%" height="13"
					class="<%=estilo_color%> letra7"><%=cont%></td>


				<%
					
				%>

				<td width="14%" nowrap class="<%=estilo_color%> letra7"
					id="insc_2_<%=i%>"><%=((Vector) vec_reserva.get(i))
													.get(2) + ""%>-<%=((Vector) vec_reserva.get(i))
													.get(3) + ""%></td>
				<td id="insc_3_<%=i%>" width="22%" class="<%=estilo_color%> letra7"><%=((Vector) vec_reserva.get(i))
													.get(4) + ""%></td>
				<td id="insc_4_<%=i%>" width="20%" class="<%=estilo_color%> letra7"><%=((Vector) vec_reserva.get(i))
													.get(5) + ""%></td>
				<td id="insc_5_<%=i%>" width="29%" class="<%=estilo_color%> letra7"><%=((Vector) vec_reserva.get(i))
													.get(6) + ""%></td>
				<td id="insc_6_<%=i%>" width="3%" align="center"><%=((Vector) vec_reserva.get(i))
													.get(7) + ""%></td>
				<td id="insc_7_<%=i%>" width="4%" align="center"
					class="<%=estilo_color%> letra7"><%=((Vector) vec_reserva.get(i))
													.get(1) + ""%></td>
				<td width="4%" align="center">
				<%
					if (!clase_cerrada && !clase_anulada
													&& !clase_suspendida) {
				%><a
					HREF="javascript:void(0)" id="accion<%=i%>"
					onClick="CambiarAsistencia('<%=i%>','<%=texto%>',<%=origen_asistencia%>); return false">
				<%
					}
				%> <img id="ASISTENCIA<%=i%>" name="ASISTENCIA<%=i%>"
					src="<%=imagen%>" border="0">
				<%
					if (!clase_cerrada && !clase_anulada
													&& !clase_suspendida) {
				%>
				</a>
				<%
					}
				%> <input name='asistencia' type='hidden'
					value='<%=((Vector) vec_reserva.get(i)).get(8)+ ""%>'> <input
					name='orden' type='hidden'
					value='<%=((Vector) vec_reserva.get(i)).get(9)+ ""%>'> <input
					type='hidden' name='rut_consulta'
					value='<%=((Vector) vec_reserva.get(i)).get(2)+ ""%>'> <input
					type='hidden' name='dvrut_consulta'
					value='<%=((Vector) vec_reserva.get(i)).get(3)+ ""%>'> <input
					name='proceso' type='hidden' value='1'> <input
					name='resgim_id' type='hidden'
					value='<%=((Vector) vec_reserva.get(i)).get(0)+ ""%>'> <input
					name='fecha' type='hidden' value='<%=f1%>'> <input
					type="hidden" name="sala" value="<%=sala%>"> <input
					type="hidden" name="fecha_inicio" value="<%=fecha%>"> <input
					type="hidden" name="dia" value="<%=dia%>"> <input
					type="hidden" name="bloque1" value="<%=bloque1%>"> <input
					type="hidden" name="bloque2" value="<%=bloque2%>"> <input
					type="hidden" name="accion" value=""> <input type="hidden"
					name="bloque" value="<%=bloque%>"> <input type="hidden"
					name="par_i" value="<%=i%>"> <input type="hidden"
					name="par_accion" value="<%=texto%>"> <input type="hidden"
					name="par_origen" value=" <%=origen_asistencia%>"></td>
				<%
					if (!clase_cerrada && !clase_anulada
													&& !clase_suspendida) {
				%>
				<td id="insc_8_<%=i%>" width="4%" align="center"
					class="Estilo_Azulino letra7">
				<%
					if (origen_asistencia == 2) {
				%> <a href="javascript:void(0)"
					onClick="EliminarInscritos(document.formINSC_grabar<%=i%>); return false">
				<img src='imagen/gim_basurero.gif' alt='ELIMINAR' border=0
					align='absmiddle'></a> <%
 	} else {
 %>&nbsp;<%
 	}
 %>
				</td>
				<%
					}
				%>

			</tr>
			</form>
			<%
				cont++;
									}
								}
							}
			%>
		</table>
		</td>
	</tr>
</table>
<%
	if (clase_cerrada || clase_anulada || clase_suspendida) {
%> <%
 	texto = "";
 					imagen = "";
 					cont = 1;
 					color = "";
 					boolean muestra_titulo = true;
 					for (int i = 0; i < vec_reserva.size(); i++) {

 						if (Integer.parseInt(((Vector) vec_reserva
 								.get(i)).get(8)
 								+ "") != 4) {
 							muestra_listado = false;
 							if ((clase_cerrada || clase_anulada || clase_suspendida)
 									&& (Integer
 											.parseInt(((Vector) vec_reserva
 													.get(i)).get(8)
 													+ "") > 1 || Integer
 											.parseInt(((Vector) vec_reserva
 													.get(i)).get(8)
 													+ "") == 0))
 								muestra_listado = true;
 							switch (Integer
 									.parseInt(((Vector) vec_reserva
 											.get(i)).get(8)
 											+ "")) {
 							case 1: {// justifico
 								texto = "Asiste";
 								imagen = "imagen/gim_asistio.gif";
 								break;
 							}
 							case 2: {
 								texto = "Justifica";
 								imagen = "imagen/gim_justifico.gif";
 								break;
 							}
 							default: {
 								texto = "Falta";
 								imagen = "imagen/gim_falto.gif";
 								break;
 							}
 							}
 							switch (Integer
 									.parseInt(((Vector) vec_reserva
 											.get(i)).get(10)
 											+ "")) {
 							case 2: {// tutoria
 								color = "#D2E5D2";
 								break;
 							}
 							case 3: { // rama
 								color = "#ECE5CC";
 								break;
 							}
 							default: { // normales
 								color = "#E8E8E8";
 								break;
 							}
 							}
 							if (muestra_listado) {

 								if (muestra_titulo) {
 %>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td width="50%" height="30" class="Estilo_VerdeOsc">Lista de
		Ausentes</td>
		<td align="right"><iframe name="iframe_ausentes" scrolling="no"
			frameborder="0" height="20" src="gim_proceso.jsp"></iframe></td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr bgcolor="#999999">
		<td width="100%" height="35" class="Estilo_Gris">
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr bgcolor="#008040" class="Estilo_GrisOsc">
				<td height="13" background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">N&deg;</span></td>
				<td background="imagen/boton_baseFONDO04.gif" bgcolor="#008040"><span
					class="Estilo_Blanco">RUT</span></td>
				<td background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">Paterno</span></td>
				<td background="imagen/boton_baseFONDO04.gif" bgcolor="#008040"><span
					class="Estilo_Blanco">Materno</span></td>
				<td background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">Nombres</span></td>
				<td align="center" background="imagen/boton_baseONDO04.gif"><span
					class="Estilo_Blanco">Sexo</span></td>
				<td align="center" background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">Usuario</span></td>
				<td align="center" background="imagen/boton_baseFONDO04.gif"><span
					class="Estilo_Blanco">Asist.</span></td>
			</tr>
			<%
				}
			%>

			<form name="formASIST_grabar<%=cont%>" id="formASIST_grabar<%=cont%>"
				method="post" action="" target="">
			<tr bgcolor="<%=color%>" onmouseover="MouseOver(this)"
				onmouseout="MouseOut(this, '<%=color%>')">
				<td id="asist_1_<%=cont%>" width="4%" height="13"
					class="Estilo_Azulino letra7"><%=cont%></td>
				<td width="14%" nowrap class="Estilo_Azulino letra7"
					id="asist_2_<%=cont%>"><%=((Vector) vec_reserva.get(i))
														.get(2) + ""%>-<%=((Vector) vec_reserva.get(i))
														.get(3) + ""%></td>
				<td id="asist_3_<%=cont%>" width="22%" class="Estilo_Azulino letra7"><%=((Vector) vec_reserva.get(i))
														.get(4) + ""%></td>
				<td id="asist_4_<%=cont%>" width="20%" class="Estilo_Azulino letra7"><%=((Vector) vec_reserva.get(i))
														.get(5) + ""%></td>
				<td id="asist_5_<%=cont%>" width="29%" class="Estilo_Azulino letra7"><%=((Vector) vec_reserva.get(i))
														.get(6) + ""%></td>
				<td id="asist_6_<%=cont%>" width="3%" align="center"><%=((Vector) vec_reserva.get(i))
														.get(7) + ""%></td>
				<td id="asist_7_<%=cont%>" width="4%" align="center"
					class="Estilo_Azulino letra7"><%=((Vector) vec_reserva.get(i))
														.get(1) + ""%></td>
				<td width="4%" align="center"><a HREF="javascript:void(0)"
					id="accion_asis<%=cont%>"
					onClick="CambiarAusenciaAsistencia('<%=cont%>','<%=texto%>'); return false">
				<img id="AUSENCIA<%=cont%>" name="AUSENCIA<%=cont%>"
					src="<%=imagen%>" border="0"></a> <input name='asistencia'
					type='hidden' value='<%=((Vector) vec_reserva.get(i)).get(8)+ ""%>'>
				<input name='orden' type='hidden'
					value='<%=((Vector) vec_reserva.get(i)).get(9)+ ""%>'> <input
					type='hidden' name='rut_consulta'
					value='<%=((Vector) vec_reserva.get(i)).get(2)+ ""%>'> <input
					type='hidden' name='dvrut_consulta'
					value='<%=((Vector) vec_reserva.get(i)).get(3)+ ""%>'> <input
					name='proceso' type='hidden' value='1'> <input
					name='resgim_id' type='hidden'
					value='<%=((Vector) vec_reserva.get(i)).get(0)+ ""%>'> <input
					name='fecha' type='hidden' value='<%=f1%>'> <input
					type="hidden" name="sala" value="<%=sala%>"> <input
					type="hidden" name="fecha_inicio" value="<%=fecha%>"> <input
					type="hidden" name="dia" value="<%=dia%>"> <input
					type="hidden" name="bloque1" value="<%=bloque1%>"> <input
					type="hidden" name="bloque2" value="<%=bloque2%>"> <input
					type="hidden" name="accion" value=""> <input type="hidden"
					name="bloque" value="<%=bloque%>"> <input type="hidden"
					name="par_i" value="<%=cont%>"> <input type="hidden"
					name="par_accion" value="<%=texto%>"></td>
			</tr>
			</form>
			<%
				cont++;
			%>

			<%
				muestra_titulo = false;
										}
									}
								}
			%>
			<%
				if ((clase_cerrada || clase_anulada || clase_suspendida)
										&& cont > 0) {
			%>
		</table>
		</td>
	</tr>
</table>
<%
	}
%> <%
 	}
 %> <%
 	}
 %> <%
 	if (!clase_cerrada && !clase_anulada && !clase_suspendida) {
 %>
<%
	if (espera > 0) {
%>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td width="50%" height="30" class="Estilo_VerdeOsc">Lista de
		espera</td>
		<td align="right"><iframe id = "iframe_lespera" name="iframe_lespera" scrolling="no"
			frameborder="0" height="20" src="gim_proceso.jsp"></iframe></td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr bgcolor="#999999">
		<td width="100%" class="Estilo_Gris">
		<div id="TABLA_lista_espera"></div>
		</td>
	</tr>
</table>
<%
	}
%>
<table width="100%" height="40" border="0" cellpadding="1"
	cellspacing="0">
	<tr valign="bottom">
		<td width="20%">
		<table height="21" border="0" cellpadding="0" cellspacing="0">
			<tr onClick="OpcionClase(document.form_accion,2)"
				onMouseOver="BotonOver(this,'Anular o Suspender')"
				onMouseOut="BotonOut(this)">
				<td width="22"><img src="imagen/boton_baseCANCEL01.gif"
					width="22" height="21"></td>
				<td width="97" align="center" valign="middle" nowrap
					background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">Anular
				la clase</td>
				<td width="3"><img src="imagen/boton_baseBORDEDER.gif"></td>
			</tr>
		</table>
		</td>
		<td width="1%">&nbsp;</td>
		<td width="79%">
		<table height="21" border="0" cellpadding="0" cellspacing="0">
			<tr onClick="OpcionClase(document.form_accion,1)"
				onMouseOver="BotonOver(this,'Cerrar')" onMouseOut="BotonOut(this)">
				<td width="19"><img src="imagen/boton_baseCANDADO01.gif"
					width="19" height="21"></td>
				<td width="95" align="center" valign="middle" nowrap
					background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">Cerrar
				la clase</td>
				<td width="3"><img src="imagen/boton_baseBORDEDER.gif"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<%
	}
%>
</DIV>
<DIV id="BLOQUE_anular" style="display: none"><br>
<form name="form_razon" method="post" action="" target="">

<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td height="30" class="Estilo_Verde">RAZ&Oacute;N DE LA
		ANULACI&Oacute;N O SUSPENSI&Oacute;N DE LA CLASE</td>
	</tr>
	<tr valign="top" bgcolor="#FFFFFF">
		<td width="89%" bgcolor="#FFFFFF"><input name="opc" type="radio"
			value="2" checked>Anular <input name="opc" type="radio"
			value="3">Suspender</td>
	</tr>
	<tr valign="top" bgcolor="#FFFFFF">
		<td width="89%" bgcolor="#FFFFFF"><textarea name="razon"
			cols="60" rows="3" id="razon"></textarea></td>
	</tr>
</table>
</form>
<table width="100%" height="35" border="0" cellpadding="1"
	cellspacing="0">
	<tr valign="bottom">
		<td width="9%">
		<table height="21" border="0" cellpadding="0" cellspacing="0">
			<tr onClick="OpcionClase(document.form_accion,3)"
				onMouseOver="BotonOver(this,'Ingresar')" onMouseOut="BotonOut(this)">
				<td width="21"><img src="imagen/boton_baseOK01.gif" width="21"
					height="21"></td>
				<td width="63" align="center" valign="middle" nowrap
					background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">Registrar</td>
				<td width="3"><img src="imagen/boton_baseBORDEDER.gif"></td>
			</tr>
		</table>
		</td>
		<td width="1%"></td>
		<td width="90%">
		<table height="21" border="0" cellpadding="0" cellspacing="0">
			<tr onClick="DesHacerAnular()"
				onMouseOver="BotonOver(this,'Deshacer')" onMouseOut="BotonOut(this)">
				<td width="21"><img src="imagen/boton_baseUNDO01.gif"
					width="21" height="21"></td>
				<td width="60" valign="middle" nowrap
					background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">
				Deshacer</td>
				<td width="3"><img src="imagen/boton_baseBORDEDER.gif"
					width="3" height="21"></td>
			</tr>
		</table>
		</td>
	</tr>

</table>

</DIV>
<%
	} else {
			if (sala > 0) {
				if (vec_periodo.size() > 0) {
%>
<span class="Estilo_Rojo" align="center">La fecha seleccionada se
encuentra fuera del per�odo</span>
<%
	} else {
%>
<span class="Estilo_Rojo" align="center">Se encuentra fuera del
per�odo vigente</span>
<%
	}
%>
<%
	}
		}
%>
<form name="form_accion" method="post" action="gim_asistencia_adm.jsp"
	target="_self"><input type="hidden" name="accion"> <input
	type='hidden' name='fecha' value='<%=f1%>'> <input
	type="hidden" name="sala" value="<%=sala%>"> <input
	type="hidden" name="fecha_inicio" value="<%=fecha%>"> <input
	type="hidden" name="dia" value="<%=dia%>"> <input type="hidden"
	name="bloque1" value="<%=bloque1%>"> <input type="hidden"
	name="bloque2" value="<%=bloque2%>"> <input type="hidden"
	name="bloque" value="<%=bloque%>"> <input type="hidden"
	name="cod_asign" value="<%=cod_asign%>"> <input type="hidden"
	name="paralelo" value="<%=paralelo%>"> <input type="hidden"
	name="estado_clase" value=""> <input type="hidden"
	name="periodo" value="<%=periodo%>"> <input type="hidden"
	name="a�o" value="<%=anno%>"> <input type="hidden"
	name="semestre" value="<%=semestre%>"> <input type="hidden"
	name="razon" value=""></form>
<form name="form" method="post"><input type="hidden"
	name="listado" value="1"> <input type="hidden" name="rut"
	value="<%=rutnum%>"> <input type="hidden" name="perfil"
	value="<%=perfil%>"></form>

<form name='formLE_grabar' id='formLE_grabar'  method='post' action='gim_proceso.jsp' target='iframe_lespera'>
<input type='hidden' name='proceso' value='2'>
<input type='hidden' name='rut_consulta' value=''>
<input type='hidden' name='dvrut_consulta' value=''>
<input type='hidden' name='operacion'>
<input name='resgim_id' type='hidden' value=''>
<input name='fecha' type='hidden' value='<%=f1%>'>
<input type='hidden' name='sala' value='<%=sala%>'>
<input type='hidden' name='fecha_inicio' value='<%=fecha%>'>
<input type='hidden' name='dia' value='<%=dia%>'>
<input type='hidden' name='bloque1' value='<%=bloque1%>'>
<input type='hidden' name='bloque2' value='<%=bloque2%>'>
<input type='hidden' name='a�o' value='<%=anno%>'>
<input type='hidden' name='semestre' value='<%=semestre%>'>
<input type='hidden' name='periodo' value='<%=periodo%>'>
<input type='hidden' name='par_i' value=''>
</form>	
	
</body>
<%
	} else {
%>
<BODY>
<span class="Estilo_Rojo">No tiene permitido acceso desde este
equipo.</span>
</BODY>
<%
	}
%>
</html>
<%}%>