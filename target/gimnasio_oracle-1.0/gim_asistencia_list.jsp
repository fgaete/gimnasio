<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<%
// Creado por           : M�nica Barrera Frez
// Fecha                : 1/10/2007
// �ltima Actualizaci�n :
// Listado de asistencia.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");


  int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int cod_perfil  = session.getAttribute("cod_perfil") != null?
                    Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
  int sala        = request.getParameter("sala")!=null &&
                    !request.getParameter("sala").equals("null") &&
                    !request.getParameter("sala").equals("NULL") &&
                    !request.getParameter("sala").equals("")  &&
                    !request.getParameter("sala").equals(" ")?
                    Integer.parseInt(request.getParameter("sala")+""):0;

  int tipo_listado = request.getParameter("tipo_listado") != null &&
                     !request.getParameter("tipo_listado").trim().equals("")?
                     Integer.parseInt(request.getParameter("tipo_listado")):0;

  int bloque      = request.getParameter("bloque")!=null &&
                    !request.getParameter("bloque").equals("null") &&
                    !request.getParameter("bloque").equals("NULL") &&
                    !request.getParameter("bloque").equals("")  &&
                    !request.getParameter("bloque").equals(" ")?
                    Integer.parseInt(request.getParameter("bloque")+""):-1;
  int bloque1     = request.getParameter("bloque1")!=null &&
                    !request.getParameter("bloque1").equals("null") &&
                    !request.getParameter("bloque1").equals("NULL") &&
                    !request.getParameter("bloque1").equals("")  &&
                    !request.getParameter("bloque1").equals(" ")?
                    Integer.parseInt(request.getParameter("bloque1")+""):0;
  int bloque2     = request.getParameter("bloque2")!=null &&
                    !request.getParameter("bloque2").equals("null") &&
                    !request.getParameter("bloque2").equals("NULL") &&
                    !request.getParameter("bloque2").equals("")  &&
                    !request.getParameter("bloque2").equals(" ")?
                    Integer.parseInt(request.getParameter("bloque2")+""):0;
  int a�o         = request.getParameter("anno")!=null &&
                    !request.getParameter("anno").equals("null") &&
                    !request.getParameter("anno").equals("NULL") &&
                    !request.getParameter("anno").equals("")  &&
                    !request.getParameter("anno").equals(" ")?
                    Integer.parseInt(request.getParameter("anno")+""):0;
  int semestre     = request.getParameter("semestre")!=null &&
                     !request.getParameter("semestre").equals("null") &&
                     !request.getParameter("semestre").equals("NULL") &&
                     !request.getParameter("semestre").equals("")  &&
                     !request.getParameter("semestre").equals(" ")?
                     Integer.parseInt(request.getParameter("semestre")+""):0;
 String fecha     = request.getParameter("fecha_inicio")!=null &&
                   !request.getParameter("fecha_inicio").equals("null") &&
                   !request.getParameter("fecha_inicio").equals("NULL") &&
                   !request.getParameter("fecha_inicio").equals("")  &&
                   !request.getParameter("fecha_inicio").equals(" ")?
                   request.getParameter("fecha_inicio")+"":"";
 int dia     = request.getParameter("dia")!=null &&
                   !request.getParameter("dia").equals("null") &&
                   !request.getParameter("dia").equals("NULL") &&
                   !request.getParameter("dia").equals("")  &&
                   !request.getParameter("dia").equals(" ")?
                     Integer.parseInt(request.getParameter("dia")+""):0;
 String texto_bloque     = request.getParameter("texto")!=null &&
                  !request.getParameter("texto").equals("null") &&
                  !request.getParameter("texto").equals("NULL") &&
                  !request.getParameter("texto").equals("")  &&
                  !request.getParameter("texto").equals(" ")?
                   request.getParameter("texto")+"":"";
 String nom_dia     = request.getParameter("nom_dia")!=null &&
                 !request.getParameter("nom_dia").equals("null") &&
                 !request.getParameter("nom_dia").equals("NULL") &&
                 !request.getParameter("nom_dia").equals("")  &&
                 !request.getParameter("nom_dia").equals(" ")?
                   request.getParameter("nom_dia")+"":"";

 String m  = (Integer.parseInt(fecha.substring(3,5)) < 10)?
             ("0"+Integer.parseInt(fecha.substring(3,5))):
             (""+Integer.parseInt(fecha.substring(3,5)));
 String dd = (Integer.parseInt(fecha.substring(0,2)) < 10)?
             ("0"+Integer.parseInt(fecha.substring(0,2))):
             (""+Integer.parseInt(fecha.substring(0,2)));
 String a  = (Integer.parseInt(fecha.substring(6,10)) < 10)?
             ("0"+Integer.parseInt(fecha.substring(6,10))):
             (""+Integer.parseInt(fecha.substring(6,10)));
 String     f1 = a + m + dd;
general.getAbrirConexion();
general.setGenerarConexionRegistro();
Vector vec_listado = general.getListadoInscripciones(a�o, semestre, bloque1,
                                          bloque2, f1, dia, sala);
general.getCerrarConexion();
String texto_sala = (sala==10335)?"M�QUINAS Y PESAS":"AER�BICA";
String texto_tipo = (tipo_listado== 1)?"Inscripciones":"Asistencia";
java.util.Calendar f = new java.util.GregorianCalendar();
int dia_actual = f.get(java.util.Calendar.DAY_OF_MONTH);
int mes_actual = f.get(java.util.Calendar.MONTH) + 1;
int a�o_actual = f.get(java.util.Calendar.YEAR);
int hora_actual = f.get(java.util.Calendar.HOUR);
int minuto_actual = f.get(java.util.Calendar.MINUTE);
  %>
<html>
<head>
<title>Gimnasio - Lista de Asistentes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0">
  <tr>
    <td width="20%" height="50" class="letra8" >Gimnasio III<br>
      USM </td>
    <td width="65%" class="letra10" align="center"><span class="letra8"><%=texto_tipo%> <%=a�o%>-<%=semestre%><br>
      SALA DE <%=texto_sala%>  <br>
    </span></td>
    <td width="15%" >&nbsp;
        <div align="right"><span class="letra8"><%=dia_actual%>/<%=mes_actual%>/<%=a�o_actual%><br>
              <%=hora_actual%>:<%=minuto_actual%></span></div></td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td height="20" valign="bottom" class="letra10"><%=nom_dia%> <%=fecha%></td>
  </tr>
  <tr>
    <td width="100%" height="20" class="Estilo_Naranjo">BLOQUE <%=texto_bloque%> </td>
  </tr>
</table>
<%if (vec_listado.size() > 0){%>
<table width="100%" height="49%"  border="1" cellpadding="2" cellspacing="1">
  <tr bgcolor="#FFFFFF">
    <td class="Estilo_GrisBold">N&deg;</td>
    <td class="Estilo_GrisBold">Paterno</td>
    <td class="Estilo_GrisBold">Materno</td>
    <td class="Estilo_GrisBold">Nombres</td>
    <td height="18%" class="Estilo_GrisBold">RUT</td>
    <td align="center" class="Estilo_GrisBold">Registro</td>
    <td align="center" class="Estilo_GrisBold">Cupo</td>
  <%if (tipo_listado == 2) {%>
    <td align="center" class="Estilo_GrisBold">Asistencia</td>
  <%}%>
  </tr>
<%for (int i=0;i<vec_listado.size();i++){
  if (Integer.parseInt(((Vector) vec_listado.get(i)).get(5)+ "") > 0){
%>
  <tr bgcolor="#FFFFFF">
    <td width="5%" valign="top" class="Estilo_Azul"><%=i+1%></td>
    <td width="15%" valign="top" class="Estilo_Azul"><%=((Vector) vec_listado.get(i)).get(0)+ ""%></td>
    <td width="15%" valign="top" class="Estilo_Azul"><%=((Vector) vec_listado.get(i)).get(1)+ ""%></td>
    <td width="26%" valign="top" class="Estilo_Azul"><%=((Vector) vec_listado.get(i)).get(2)+ ""%></td>
    <td width="16%" valign="top" class="Estilo_Azul"><%=((Vector) vec_listado.get(i)).get(3)+ ""%></td>
    <td width="13%" align="center" valign="top" class="Estilo_Azul"><%=((Vector) vec_listado.get(i)).get(8)+ ""%></td>
    <td width="13%" align="center" valign="top" class="Estilo_Azul"><%=((Vector) vec_listado.get(i)).get(4)+ ""%></td>
   <%if (tipo_listado == 2) {%>
    <td width="10%" align="center" valign="top" class="Estilo_Azul"><%=((Vector) vec_listado.get(i)).get(6)+ ""%></td>
  <%}%>
  </tr>
  <%}}%>
</table>

  <%} else {%>
  <br>
  <br>
  <br>
 <div align="center"> No se encontraron registros.</div>
  <br>
  <%}%>

<div align="center"><font class="barra" color="#000000"><br>
&copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a</font>
</div>
</body>
</html>
