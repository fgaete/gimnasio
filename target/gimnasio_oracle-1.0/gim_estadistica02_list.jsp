<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*,java.text.*"%>
<%@ page import="gimnasio.*"%>
<%!private static final DecimalFormat FMT_dec = new DecimalFormat("#0.00");%>

<% // Creado por           : M�nica Barrera Frez
// Fecha                : 23/10/2007
// �ltima Actualizaci�n :
// Registro listado.    Estad�stica por uso seg�n rango de fecha .
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>


<%response.setHeader("Pragma","no-cache");
response.setDateHeader ("Expires", 0);
response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
 int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");


String fecha_desde    = request.getParameter("fecha_desde")!=null &&
!request.getParameter("fecha_desde").equals("null") &&
!request.getParameter("fecha_desde").equals("NULL") &&
!request.getParameter("fecha_desde").equals("")  &&
!request.getParameter("fecha_desde").equals(" ")?
request.getParameter("fecha_desde")+"":"";
String fecha_hasta  = request.getParameter("fecha_hasta")!=null &&
!request.getParameter("fecha_hasta").equals("null") &&
!request.getParameter("fecha_hasta").equals("NULL") &&
!request.getParameter("fecha_hasta").equals("")  &&
!request.getParameter("fecha_hasta").equals(" ")?
request.getParameter("fecha_hasta")+"":"";
int   version =  request.getParameter("version")!=null &&
!request.getParameter("version").equals("null") &&
!request.getParameter("version").equals("NULL") &&
!request.getParameter("version").equals("")  &&
!request.getParameter("version").equals(" ")
?Integer.parseInt(request.getParameter("version")+""):0;
int   sala =  request.getParameter("sala")!=null &&
!request.getParameter("sala").equals("null") &&
!request.getParameter("sala").equals("NULL") &&
!request.getParameter("sala").equals("")  &&
!request.getParameter("sala").equals(" ")
?Integer.parseInt(request.getParameter("sala")+""):0;
int anno             = request.getParameter("anno")!=null &&
!request.getParameter("anno").equals("null") &&
!request.getParameter("anno").equals("NULL") &&
!request.getParameter("anno").equals("")  &&
!request.getParameter("anno").equals(" ")?
Integer.parseInt(request.getParameter("anno")+""):0;
int semestre         = request.getParameter("semestre")!=null &&
!request.getParameter("semestre").equals("null") &&
!request.getParameter("semestre").equals("NULL") &&
!request.getParameter("semestre").equals("")  &&
!request.getParameter("semestre").equals(" ")?
Integer.parseInt(request.getParameter("semestre")+""):0;

Vector vec_bloque = new Vector();
Vector vec_asistentes = new Vector();
Vector vec_total = new Vector();
Vector vec_total_general = new Vector();
Vector vec_total_f = new Vector();

String nom_dia = "";
String nom_dia_ant = "";
String asign_ant = "";
String asign = "";
String hora_inicio = "";
String hora_termino = "";

int num_dia = 0;
int bloque = 0;
int cupo_actividad = 0;
int dia = 0;

double total = 0.00;
double sum_masc = 0.00;
double sum_fem = 0.00;
double total_fem = 0.00;
double total_mix = 0.00;

general.getAbrirConexion();
general.setGenerarConexionHorario();
general.setGenerarConexionReserva();

String m  = (Integer.parseInt(fecha_desde.substring(3,5)) < 10)?
("0"+Integer.parseInt(fecha_desde.substring(3,5))):
(""+Integer.parseInt(fecha_desde.substring(3,5)));
String dd = (Integer.parseInt(fecha_desde.substring(0,2)) < 10)?
("0"+Integer.parseInt(fecha_desde.substring(0,2))):
(""+Integer.parseInt(fecha_desde.substring(0,2)));
String a  = (Integer.parseInt(fecha_desde.substring(6,10)) < 10)?
("0"+Integer.parseInt(fecha_desde.substring(6,10))):
(""+Integer.parseInt(fecha_desde.substring(6,10)));
String   f1 = a + m + dd;
m  = (Integer.parseInt(fecha_hasta.substring(3,5)) < 10)?
("0"+Integer.parseInt(fecha_hasta.substring(3,5))):
(""+Integer.parseInt(fecha_hasta.substring(3,5)));
dd = (Integer.parseInt(fecha_hasta.substring(0,2)) < 10)?
("0"+Integer.parseInt(fecha_hasta.substring(0,2))):
(""+Integer.parseInt(fecha_hasta.substring(0,2)));
a  = (Integer.parseInt(fecha_hasta.substring(6,10)) < 10)?
("0"+Integer.parseInt(fecha_hasta.substring(6,10))):
(""+Integer.parseInt(fecha_hasta.substring(6,10)));
String   f2 = a + m + dd;


boolean aseo = false;
%>
<html>
<head>
<title>Gimnasio - Estad&iacute;sitica</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="css/estilo.css" rel="stylesheet" type="text/css">
  <link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
</head>

<body>
<%if ((sala == 10335 || sala == 3)){
  int cupo = general.getCupoGimnasio(anno, semestre, 0, 0, 0,10335,0);
  vec_bloque = general.getHoraBloque(10335, anno, semestre);
  vec_asistentes = general.getPromedioBloque(anno, semestre, f1, f2,10335 , version);
  

  %>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="20" align="center" class="letra10">PROMEDIO POR BLOQUE <%=anno%>-<%=semestre%> </td>
  </tr>
  <tr>
    <td height="20" align="center" valign="top"><%=fecha_desde%> al <%=fecha_hasta%></td>
  </tr>
</table>
<br>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="20" valign="bottom"><font  class="letra10">SALA DE M&Aacute;QUINAS</font><br>
      <%="Cupo " + cupo%></td>
  </tr>
</table>
<br>
<%if ( version == 0){%>
<table width="100%"  border="1" cellpadding="2" cellspacing="0">
  <tr valign="bottom" bgcolor="#FFFFFF">
    <td bgcolor="#CCCCCC" ><strong>D&iacute;a</strong></td>
    <%for (int i=0;i<vec_bloque.size();i++){
    vec_total.addElement("0");
    //vec_total.addElement("0");
    %>
    <td align="center" bgcolor="#CCCCCC" class="letra7" ><strong><%=((Vector)vec_bloque.get(i)).get(1)%><br>
      <%=((Vector)vec_bloque.get(i)).get(2)%>
</strong></td>
<%}   vec_total.addElement("0");
%>
<td align="center" bgcolor="#CCCCCC"><strong>Total</strong></td>
  </tr>
  <%nom_dia = "";
  num_dia = 0;
  bloque = 0;
  sum_masc = 0;
  sum_fem = 0;
  dia = 1;
  while (dia < 7){ %>
  <tr bgcolor="#FFFFFF">
    <%switch (dia){
    case 1 : nom_dia = "Lunes";
    break;
    case 2 : nom_dia = "Martes";
    break;
    case 3: nom_dia = "Mi�rcoles";
    break;
    case 4: nom_dia = "Jueves";
    break;
    case 5: nom_dia = "Viernes";
    break;
    case 6: nom_dia = "S�bado";
    break;
  }
  %>

  <td width="18%" valign="top"><%=nom_dia%></td>
  <% boolean continuar = true;
  int dif = 0;
  int bi = 0;
  int bt = 0;
  String colspan = "";
  for (int j=0;j<vec_asistentes.size() && continuar;j++){
    colspan = "";
    num_dia = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(1)+"");

    if (num_dia == dia){
      sum_masc =  Integer.parseInt(((Vector)vec_asistentes.get(j)).get(4)+"");
      if (sum_masc > 0)
      sum_masc = (double)(sum_masc)/(Integer.parseInt(((Vector)vec_asistentes.get(j)).get(6)+""));
      
      total = total + sum_masc;
      bi = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(2)+"");
      bt = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(3)+"");

      dif = (bt - bi) + 1;
      
   
      if (dif > 1){
        colspan = "colspan=\""+dif+"\"";
        int k=bi;
        while (k <= bt){
          double t = sum_masc + Double.parseDouble(vec_total.get(k-1)+"");
          vec_total.set(k-1,t+"");
          k++;
        }
      } else {
        double t = sum_masc +  Double.parseDouble(vec_total.get(bi-1)+"");
        vec_total.set(bi-1,t+"");

      }

      if (j == (vec_asistentes.size()-1)){
        double  t = total +  Double.parseDouble(vec_total.get(vec_total.size()-1)+"");
        vec_total.set((vec_total.size()-1),t+"");
        %>   <td width="6%" align="center" <%=colspan%>><%=(sum_masc>0)?FMT_dec.format(sum_masc):"&nbsp;"%></td>
        <td width="6%" align="center"><%=(total>0)?FMT_dec.format(total):"&nbsp;"%></td>
        <%} else { %>
        <td width="6%" align="center" <%=colspan%>><%=(sum_masc>0)?FMT_dec.format(sum_masc):"&nbsp;"%></td>
        <%}} else {

        if ( num_dia > dia || j == vec_asistentes.size()){
          continuar = false;
          double t = total +  Double.parseDouble(vec_total.get(vec_total.size()-1)+"");
          vec_total.set((vec_total.size()-1),t+"");
          %>
          <td width="6%" align="center"><%=(total>0)?FMT_dec.format(total):"&nbsp;"%></td>
          <%  total = 0;
          }} }%>
          </tr>
          <%dia++;
          }%>
          <tr bgcolor="#CCCCCC">
            <td valign="top"><strong>Total</strong></td>
            <%for (int n=0; n< vec_total.size();n++){%>
              <td align="center" valign="top"><%=FMT_dec.format(Double.parseDouble(vec_total.get(n)+""))%></td>
              <%}%>
          </tr>
</table>
<% } else if ( version == 1){%>
<table width="100%"  border="1" cellpadding="2" cellspacing="0">
  <tr valign="bottom" bgcolor="#FFFFFF">
    <td bgcolor="#CCCCCC" rowspan="2"><strong>D&iacute;a</strong></td>
    <%for (int i=0;i<vec_bloque.size();i++){
    vec_total.addElement("0");
    vec_total_f.addElement("0");

    %>
    <td align="center" bgcolor="#CCCCCC" class="letra7" colspan="2" ><strong><%=((Vector)vec_bloque.get(i)).get(1)%><br>
      <%=((Vector)vec_bloque.get(i)).get(2)%>
</strong></td>
<%}   vec_total.addElement("0");
vec_total_f.addElement("0");
%>
<td align="center" bgcolor="#CCCCCC" colspan="2" ><strong>Total</strong></td>
<td align="center" bgcolor="#CCCCCC" colspan="2" ><strong>Total Mes</strong></td>
  </tr>
  <tr bgcolor="#CCCCCC">
    <%for (int i=0;i<vec_bloque.size();i++){%>
      <td width="8%" align="center">M</td>
      <td width="8%" align="center">F</td>
      <%}%>
      <td width="8%" align="center">M</td>
      <td width="8%" align="center">F</td>
      <td width="8%" align="center">Mixto</td>
  </tr>
  <%nom_dia = "";
  num_dia = 0;
  bloque = 0;
  sum_masc = 0;
  sum_fem = 0;
  dia = 1;
  while (dia < 7){ %>
  <tr bgcolor="#FFFFFF">
    <%switch (dia){
    case 1 : nom_dia = "Lunes";
    break;
    case 2 : nom_dia = "Martes";
    break;
    case 3: nom_dia = "Mi�rcoles";
    break;
    case 4: nom_dia = "Jueves";
    break;
    case 5: nom_dia = "Viernes";
    break;
    case 6: nom_dia = "S�bado";
    break;
  }
  %>

  <td width="18%" valign="top"><%=nom_dia%></td>
  <% boolean continuar = true;
  int dif = 0;
  int bi = 0;
  int bt = 0;
  String colspan = "";
  for (int j=0;j<vec_asistentes.size() && continuar;j++){
    colspan = "";
    num_dia = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(1)+"");
    if (num_dia == dia){
      sum_masc =  Integer.parseInt(((Vector)vec_asistentes.get(j)).get(4)+"");
      if (sum_masc > 0)
      sum_masc = (double)(sum_masc)/(Integer.parseInt(((Vector)vec_asistentes.get(j)).get(6)+""));
      total = total + sum_masc;
      sum_fem =  Integer.parseInt(((Vector)vec_asistentes.get(j)).get(5)+"");
      if (sum_fem > 0)
      sum_fem = (double)(sum_fem)/(Integer.parseInt(((Vector)vec_asistentes.get(j)).get(6)+""));
      total_fem = total_fem + sum_fem;

      bi = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(2)+"");
      bt = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(3)+"");

      dif = (bt - bi) + 1;
      if (dif > 1){
        colspan = "colspan=\""+dif+"\"";
        int k=bi;
        while (k <= bt){
          double t = sum_masc + Double.parseDouble(vec_total.get(k-1)+"");
          vec_total.set(k-1,t+"");
          t = sum_fem + Double.parseDouble(vec_total_f.get(k-1)+"");
          vec_total_f.set(k-1,t+"");
          k++;
        }
      } else {
        double t = sum_masc +  Double.parseDouble(vec_total.get(bi-1)+"");
        vec_total.set(bi-1,t+"");
        t = sum_fem +  Double.parseDouble(vec_total_f.get(bi-1)+"");
        vec_total_f.set(bi-1,t+"");

      }
      if (j == (vec_asistentes.size()-1)){
        double  t = total +  Double.parseDouble(vec_total.get(vec_total.size()-1)+"");
        vec_total.set((vec_total.size()-1),t+"");
        t = total_fem +  Double.parseDouble(vec_total_f.get(vec_total_f.size()-1)+"");
        vec_total_f.set((vec_total_f.size()-1),t+"");
        %>   <td width="6%" align="center" <%=colspan%>><%=(sum_masc>0)?FMT_dec.format(sum_masc):"&nbsp;"%></td>
        <td width="6%" align="center"><%=(total>0)?FMT_dec.format(total):"&nbsp;"%></td>
        <td width="6%" align="center" <%=colspan%>><%=(sum_fem>0)?FMT_dec.format(sum_fem):"&nbsp;"%></td>
        <td width="6%" align="center"><%=(total_fem>0)?FMT_dec.format(total_fem):"&nbsp;"%></td>

        <%} else { %>
        <td width="6%" align="center" <%=colspan%>><%=(sum_masc>0)?FMT_dec.format(sum_masc):"&nbsp;"%></td>
        <td width="6%" align="center" <%=colspan%>><%=(sum_fem>0)?FMT_dec.format(sum_fem):"&nbsp;"%></td>
        <%}} else {
        if ( num_dia > dia || j == vec_asistentes.size()){
          continuar = false;
          double t = total +  Double.parseDouble(vec_total.get(vec_total.size()-1)+"");
          vec_total.set((vec_total.size()-1),t+"");
          t = total_fem +  Double.parseDouble(vec_total_f.get(vec_total_f.size()-1)+"");
          vec_total_f.set((vec_total_f.size()-1),t+"");
          %>
          <td width="6%" align="center"><%=(total>0)?FMT_dec.format(total):"&nbsp;"%></td>
          <td width="6%" align="center"><%=(total>0)?FMT_dec.format(total_fem):"&nbsp;"%></td>
          <%  total = 0;
          }} }  total_mix = total_mix + Double.parseDouble(vec_total.get(vec_total.size()-1)+"")+Double.parseDouble(vec_total_f.get(vec_total_f.size()-1)+"");%>
          <td width="6%" align="center"><%=FMT_dec.format(Double.parseDouble(vec_total.get(vec_total.size()-1)+"")+Double.parseDouble(vec_total_f.get(vec_total_f.size()-1)+""))+"&nbsp;"%></td>
  </tr>
  <%dia++;

}%>
<tr bgcolor="#CCCCCC">
  <td valign="top"><strong>Total</strong></td>
  <%for (int n=0; n< vec_total.size();n++){%>
    <td align="center" valign="top"><%=FMT_dec.format(Double.parseDouble(vec_total.get(n)+""))%></td>
    <td align="center" valign="top"><%=FMT_dec.format(Double.parseDouble(vec_total_f.get(n)+""))%></td>
    <%}%>
    <td align="center" valign="top"><%=FMT_dec.format(total_mix)%></td>

</tr>
</table>
<%}%>
<%}%>
<br>
<%if (sala == 10336 || sala == 3){
  vec_asistentes = general.getPromedioBloqueDet(anno, semestre, f1, f2,10336 , version);
  %>
  <table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="20" align="center" class="letra10">PROMEDIO POR BLOQUE <%=anno%>-<%=semestre%> </td>
  </tr>
  <tr>
    <td height="20" align="center" valign="top"><%=fecha_desde%> al <%=fecha_hasta%></td>
  </tr>
</table>
<br>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="20" valign="bottom"><font  class="letra10">SALA DE AER�BICA</font><br>
     </td>
  </tr>
</table>
<%if (version == 0){%>
<table width="100%"  border="1" cellpadding="2" cellspacing="0">
  <tr valign="bottom" bgcolor="#FFFFFF">
    <td bgcolor="#CCCCCC"><strong>Actividad</strong></td>
    <td bgcolor="#CCCCCC"><strong>Cupo</strong></td>
    <td bgcolor="#CCCCCC"><strong>D&iacute;a</strong></td>
    <td align="center" bgcolor="#CCCCCC" class="letra7"><strong>Bloque</strong></td>
    <td align="center" bgcolor="#CCCCCC"><strong>Promedio</strong></td>
  </tr>
  <%double total_general = 0.0;
  total = 0.0;
  for (int i=0;i<vec_asistentes.size();i++){
    asign = ((Vector)vec_asistentes.get(i)).get(0)+"";%>
    <%  if(!asign.trim().equals(asign_ant.trim()) && !asign_ant.trim().equals("") ){ %>
    <tr bgcolor="#CCCCCC">
      <td valign="top">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td align="right"><strong>Total</strong></td>
      <td align="center"><%=FMT_dec.format(total) %></td>
    </tr>
    <% }%>
    <%  cupo_actividad = Integer.parseInt(((Vector)vec_asistentes.get(i)).get(1)+"");
    nom_dia = ((Vector)vec_asistentes.get(i)).get(2)+"";
    hora_inicio = ((Vector)vec_asistentes.get(i)).get(4)+"";
    hora_termino = ((Vector)vec_asistentes.get(i)).get(5)+"";
    double prom = (Integer.parseInt(((Vector)vec_asistentes.get(i)).get(6)+"") > 0 && Integer.parseInt(((Vector)vec_asistentes.get(i)).get(8)+"")>0)? (Double.parseDouble(((Vector)vec_asistentes.get(i)).get(6)+"") / Double.parseDouble(((Vector)vec_asistentes.get(i)).get(8)+"")):0.0;
    if(!asign.trim().equals(asign_ant.trim()) && !asign_ant.trim().equals(""))
    total = prom;
    else
    total += prom;
    %>

    <tr bgcolor="#FFFFFF">
      <td valign="top"><%=(!asign.trim().equals(asign_ant.trim()))?asign:"&nbsp;" %></td>
      <td width="10%" valign="top" align="center"><%=(!asign.trim().equals(asign_ant.trim()) && !nom_dia.trim().equals(nom_dia_ant.trim()))?cupo_actividad+"":"&nbsp;" %></td>
      <td width="10%" valign="top"><%=!nom_dia.trim().equals(nom_dia_ant.trim())?nom_dia:"&nbsp;" %></td>
      <td align="center"><span class="letra7"><%=hora_inicio%> - <%=hora_termino%> </span></td>
      <td align="center"><%=FMT_dec.format(prom) %></td>
    </tr>
    <%
    total_general += prom;
    nom_dia_ant = nom_dia;
    asign_ant = asign;
  }%>

  <tr bgcolor="#CCCCCC">
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td align="right"><strong>Total</strong></td>
    <td align="center"><%=FMT_dec.format(total) %></td>
  </tr>

  <tr bgcolor="#FFFFFF">
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr bgcolor="#CCCCCC">
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td height="16%" valign="top">&nbsp;</td>
    <td align="right" valign="top"><strong>Total General </strong></td>
    <td align="center" valign="top"><%=FMT_dec.format(total_general) %></td>
  </tr>
</table>
<%}%>
<br>
<%if ((sala == 10336 || sala == 3)){%>
<%if ((sala == 10336 || sala == 3)  && version == 1){%>
<table width="100%"  border="1" cellpadding="2" cellspacing="0">
  <tr valign="bottom" bgcolor="#FFFFFF">
    <td rowspan="2" bgcolor="#CCCCCC"><strong>Actividad</strong></td>
    <td rowspan="2" bgcolor="#CCCCCC"><strong>Cupo</strong></td>
    <td rowspan="2" bgcolor="#CCCCCC"><strong>D&iacute;a</strong></td>
    <td rowspan="2" align="center" bgcolor="#CCCCCC" class="letra7"><strong>Bloque</strong></td>
    <td height="11%" colspan="2" align="center" bgcolor="#CCCCCC"><strong>Alumno</strong></td>
    <td colspan="2" align="center" bgcolor="#CCCCCC"><strong>Tutor&iacute;a</strong></td>
    <td colspan="2" align="center" bgcolor="#CCCCCC"><strong>Rama</strong></td>
    <td colspan="2" align="center" bgcolor="#CCCCCC"><strong>Funcionario</strong></td>
    <td colspan="2" align="center" bgcolor="#CCCCCC"><strong>Total</strong></td>
    <td rowspan="2" align="center" bgcolor="#CCCCCC"><strong>Total</strong></td>
  </tr>
  <tr valign="bottom" bgcolor="#FFFFFF">
    <td height="11%" align="center" bgcolor="#CCCCCC">M</td>
    <td align="center" bgcolor="#CCCCCC">F</td>
    <td align="center" bgcolor="#CCCCCC">M</td>
    <td align="center" bgcolor="#CCCCCC">F</td>
    <td align="center" bgcolor="#CCCCCC">M</td>
    <td align="center" bgcolor="#CCCCCC">F</td>
    <td align="center" bgcolor="#CCCCCC">M</td>
    <td align="center" bgcolor="#CCCCCC">F</td>
    <td align="center" bgcolor="#CCCCCC">M</td>
    <td align="center" bgcolor="#CCCCCC">F</td>
  </tr>
  <%double total_general = 0.0;
  total = 0.0;
  vec_total = new Vector();
  vec_total.addElement("0");
  vec_total.addElement("0");
  vec_total.addElement("0");
  vec_total.addElement("0");
  vec_total.addElement("0");
  vec_total.addElement("0");
  vec_total.addElement("0");
  vec_total.addElement("0");
  vec_total.addElement("0");
  vec_total.addElement("0");
  vec_total.addElement("0");
  vec_total_general = new Vector();
  vec_total_general.addElement("0");
  vec_total_general.addElement("0");
  vec_total_general.addElement("0");
  vec_total_general.addElement("0");
  vec_total_general.addElement("0");
  vec_total_general.addElement("0");
  vec_total_general.addElement("0");
  vec_total_general.addElement("0");
  vec_total_general.addElement("0");
  vec_total_general.addElement("0");
  vec_total_general.addElement("0");


  for (int i=0;i<vec_asistentes.size();i++){
    asign = ((Vector)vec_asistentes.get(i)).get(0)+"";%>
    <%  if(!asign.trim().equals(asign_ant.trim()) && !asign_ant.trim().equals("") ){ %>
    <tr bgcolor="#CCCCCC">
      <td valign="top">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td align="right"><strong>Total</strong></td>
      <%for(int j=0;j < vec_total.size();j++){
           double t = Double.parseDouble(vec_total.get(j)+"") + Double.parseDouble(vec_total_general.get(j)+"");
          vec_total_general.set(j, t+""); %>
        <td align="center"><%=FMT_dec.format(Double.parseDouble(vec_total.get(j)+"")) %></td>
        <% } %>      </tr>
        <% }%>

        <%  cupo_actividad = Integer.parseInt(((Vector)vec_asistentes.get(i)).get(1)+"");
        nom_dia = ((Vector)vec_asistentes.get(i)).get(2)+"";
        hora_inicio = ((Vector)vec_asistentes.get(i)).get(4)+"";
        hora_termino = ((Vector)vec_asistentes.get(i)).get(5)+"";
        double prom = (Integer.parseInt(((Vector)vec_asistentes.get(i)).get(6)+"") > 0 && Integer.parseInt(((Vector)vec_asistentes.get(i)).get(8)+"")>0)? (Double.parseDouble(((Vector)vec_asistentes.get(i)).get(6)+"") / Double.parseDouble(((Vector)vec_asistentes.get(i)).get(8)+"")):0.0;
        double prom_a_m = (Integer.parseInt(((Vector)vec_asistentes.get(i)).get(9)+"") > 0 && Integer.parseInt(((Vector)vec_asistentes.get(i)).get(8)+"")>0)? (Double.parseDouble(((Vector)vec_asistentes.get(i)).get(9)+"") / Double.parseDouble(((Vector)vec_asistentes.get(i)).get(8)+"")):0.0;
        double prom_a_f = (Integer.parseInt(((Vector)vec_asistentes.get(i)).get(10)+"") > 0 && Integer.parseInt(((Vector)vec_asistentes.get(i)).get(8)+"")>0)? (Double.parseDouble(((Vector)vec_asistentes.get(i)).get(10)+"") / Double.parseDouble(((Vector)vec_asistentes.get(i)).get(8)+"")):0.0;
        double prom_t_m = (Integer.parseInt(((Vector)vec_asistentes.get(i)).get(11)+"") > 0 && Integer.parseInt(((Vector)vec_asistentes.get(i)).get(8)+"")>0)? (Double.parseDouble(((Vector)vec_asistentes.get(i)).get(11)+"") / Double.parseDouble(((Vector)vec_asistentes.get(i)).get(8)+"")):0.0;
        double prom_t_f = (Integer.parseInt(((Vector)vec_asistentes.get(i)).get(12)+"") > 0 && Integer.parseInt(((Vector)vec_asistentes.get(i)).get(8)+"")>0)? (Double.parseDouble(((Vector)vec_asistentes.get(i)).get(12)+"") / Double.parseDouble(((Vector)vec_asistentes.get(i)).get(8)+"")):0.0;
        double prom_r_m = (Integer.parseInt(((Vector)vec_asistentes.get(i)).get(13)+"") > 0 && Integer.parseInt(((Vector)vec_asistentes.get(i)).get(8)+"")>0)? (Double.parseDouble(((Vector)vec_asistentes.get(i)).get(13)+"") / Double.parseDouble(((Vector)vec_asistentes.get(i)).get(8)+"")):0.0;
        double prom_r_f = (Integer.parseInt(((Vector)vec_asistentes.get(i)).get(14)+"") > 0 && Integer.parseInt(((Vector)vec_asistentes.get(i)).get(8)+"")>0)? (Double.parseDouble(((Vector)vec_asistentes.get(i)).get(14)+"") / Double.parseDouble(((Vector)vec_asistentes.get(i)).get(8)+"")):0.0;
        double prom_f_m = (Integer.parseInt(((Vector)vec_asistentes.get(i)).get(15)+"") > 0 && Integer.parseInt(((Vector)vec_asistentes.get(i)).get(8)+"")>0)? (Double.parseDouble(((Vector)vec_asistentes.get(i)).get(15)+"") / Double.parseDouble(((Vector)vec_asistentes.get(i)).get(8)+"")):0.0;
        double prom_f_f = (Integer.parseInt(((Vector)vec_asistentes.get(i)).get(16)+"") > 0 && Integer.parseInt(((Vector)vec_asistentes.get(i)).get(8)+"")>0)? (Double.parseDouble(((Vector)vec_asistentes.get(i)).get(16)+"") / Double.parseDouble(((Vector)vec_asistentes.get(i)).get(8)+"")):0.0;
        if(!asign.trim().equals(asign_ant.trim()) && !asign_ant.trim().equals("")){
          vec_total = new Vector();
          vec_total.addElement(prom_a_m+"");
          vec_total.addElement(prom_a_f+"");
          vec_total.addElement(prom_t_m+"");
          vec_total.addElement(prom_t_f+"");
          vec_total.addElement(prom_r_m+"");
          vec_total.addElement(prom_r_f+"");
          vec_total.addElement(prom_f_m+"");
          vec_total.addElement(prom_f_f+"");
          vec_total.addElement(prom_a_m+prom_t_m+prom_r_m+prom_f_m+"");
          vec_total.addElement(prom_a_f+prom_t_f+prom_r_f+prom_f_f+"");
          vec_total.addElement(prom_a_m+prom_a_f+prom_t_m+prom_t_f+prom_r_m+prom_r_f+prom_f_m+prom_f_f+"");

        } else {
          double t = prom_a_m + Double.parseDouble(vec_total.get(0)+"");
          vec_total.set(0,t+"");
          t = prom_a_f + Double.parseDouble(vec_total.get(1)+"");
          vec_total.set(1,t+"");
          t = prom_t_m + Double.parseDouble(vec_total.get(2)+"");
          vec_total.set(2,t+"");
          t = prom_t_f + Double.parseDouble(vec_total.get(3)+"");
          vec_total.set(3,t+"");
          t = prom_r_m + Double.parseDouble(vec_total.get(4)+"");
          vec_total.set(4, t+"");
          t = prom_r_f + Double.parseDouble(vec_total.get(5)+"");
          vec_total.set(5, t+"");
          t = prom_f_m + Double.parseDouble(vec_total.get(6)+"");
          vec_total.set(6, t+"");
          t = prom_f_f + Double.parseDouble(vec_total.get(7)+"");
          vec_total.set(7, t+"");
          t = prom_a_m+prom_t_m+prom_r_m+prom_f_m + Double.parseDouble(vec_total.get(8)+"");
          vec_total.set(8, t+"");
          t = prom_a_f+prom_t_f+prom_r_f+prom_f_f + Double.parseDouble(vec_total.get(9)+"");
          vec_total.set(9, t+"");
          t = prom_a_m+prom_a_f+prom_t_m+prom_t_f+prom_r_m+prom_r_f+prom_f_m+prom_f_f + Double.parseDouble(vec_total.get(10)+"");
          vec_total.set(10, t+"");
        }
        %>
        <tr bgcolor="#FFFFFF">
          <td width="15%" valign="top"><%=(!asign.trim().equals(asign_ant.trim()))?asign:"&nbsp;" %></td>
          <td width="8%" valign="top"><%=(!asign.trim().equals(asign_ant.trim()) && !nom_dia.trim().equals(nom_dia_ant.trim()))?cupo_actividad+"":"&nbsp;" %></td>
          <td width="8%" valign="top"><%=!nom_dia.trim().equals(nom_dia_ant.trim())?nom_dia:"&nbsp;" %></td>
          <td width="11%" align="center"><span class="letra7"><%=hora_inicio%> - <%=hora_termino%> </span></td>
          <td width="7%" align="center"><%=FMT_dec.format(prom_a_m)%></td>
          <td width="6%" align="center"><%=FMT_dec.format(prom_a_f)%></td>
          <td width="7%" align="center"><%=FMT_dec.format(prom_t_m)%></td>
          <td width="6%" align="center"><%=FMT_dec.format(prom_t_f)%></td>
          <td width="6%" align="center"><%=FMT_dec.format(prom_r_m)%></td>
          <td width="6%" align="center"><%=FMT_dec.format(prom_r_f)%></td>
          <td width="5%" align="center"><%=FMT_dec.format(prom_f_m)%></td>
          <td width="6%" align="center"><%=FMT_dec.format(prom_f_f)%></td>
          <td width="5%" align="center"><%=FMT_dec.format(prom_a_m+prom_t_m+prom_r_m+prom_f_m)%></td>
          <td width="5%" align="center"><%=FMT_dec.format(prom_a_f+prom_t_f+prom_r_f+prom_f_f)%></td>
          <td width="7%" align="center"><%=FMT_dec.format(prom_a_m+prom_a_f+prom_t_m+prom_t_f+prom_r_m+prom_r_f+prom_f_m+prom_f_f)%></td>
        </tr>
        <%
        total_general += prom;
        nom_dia_ant = nom_dia;
        asign_ant = asign;
      }
      %>
      <tr bgcolor="#FFFFFF">
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td align="right"><strong>Total</strong></td>
        <%for(int i=0;i < vec_total.size();i++){
          double t = Double.parseDouble(vec_total.get(i)+"") + Double.parseDouble(vec_total_general.get(i)+"");
          vec_total_general.set(i, t+"");
          %>
          <td align="center"><%=FMT_dec.format(Double.parseDouble(vec_total.get(i)+"")) %></td>
          <% } %>
      </tr>
      <tr bgcolor="#CCCCCC">
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td align="right"><strong>Total General </strong></td>
        <%for(int i=0;i < vec_total_general.size();i++){ %>
          <td align="center"><%=FMT_dec.format(Double.parseDouble(vec_total_general.get(i)+"")) %></td>
         <%} %>
      </tr>
</table>
<%}}}%>
</body>
</html>
<%general.getCerrarConexion(); %>
