function fecha(){
  var mydate = new Date();
  var year   = mydate.getYear();
  if (year < 1000) year += 1900;
  var day   = mydate.getDay();
  var month = mydate.getMonth();
  var daym  = mydate.getDate();
  if (daym < 10) daym = "0"+daym;

  var dayarray   = new Array("Domingo","Lunes","Martes","Mi�rcoles",
                             "Jueves","Viernes","S�bado");
  var montharray = new Array("Enero","Febrero","Marzo","Abril","Mayo",
                             "Junio","Julio","Agosto","Septiembre","Octubre",
                             "Noviembre","Diciembre");
  document.getElementById('fechag').innerHTML = dayarray[day] + " " + daym +
      " de " + montharray[month] + ", " + year;
  return;
}

function hora(){
  if (!document.layers&&!document.all) return;
  var Digital  = new Date();
  var horas    = Digital.getHours();
  var minutos  = Digital.getMinutes();
  var segundos = Digital.getSeconds();
  var dn       = "AM";
  if (horas>12){
    dn    = "PM";
    horas = horas - 12;
  }
  if (horas == 0) horas = 12;
  if (minutos <= 9) minutos = "0"+minutos;
  if (segundos <= 9) segundos = "0"+segundos;
  mrelog = ""+horas+":"+minutos+":"+segundos+" "+dn+"";
  if (document.layers){
    document.layers.relog.document.write(mrelog);
    document.layers.relog.document.close();
  }
  else
    if (document.all) document.getElementById('relog').innerHTML=mrelog;
  setTimeout("hora()",1000);
}