var reserva = new Array();
var TheBrowserName = navigator.appName;
var TheBrowserVersion = parseFloat(navigator.appVersion);

function MarcaCelda(t,marca){
  if (reserva.contains(marca)) {
    DesMarcaCelda(t,marca);
  }
  else {
    reserva.push(marca);
    t.bgColor='#FFFFCC';
  }
}

function DesMarcaCelda(t,marca){
  t.bgColor='#FFFFFF';
  reserva.remove(marca);
}

function CursorOn(t,stat,marca){
  if (!reserva.contains(marca)) {
    if ((TheBrowserName == 'Netscape') && (TheBrowserVersion >= 5)) {
        t.style.cursor="pointer";
    } else {
    t.style.cursor="hand";
    }
    t.bgColor='#F9EAD5';
    window.status = stat;
  }
  return true;
}

function CursorOff(t,marca){
  if (!reserva.contains(marca)) {
    t.style.cursor="default";
    t.bgColor='#FFFFFF';
    window.status = '';
  }
  return true;
}
