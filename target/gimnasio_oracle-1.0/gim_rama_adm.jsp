<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 11/06/2007
   // �ltima Actualizaci�n :
   // Rama Deportiva. se registran y mantienen todas las ramas deportivas y sus profesores.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");

  int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  int cod_perfil = session.getAttribute("cod_perfil") != null?
                  Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
   int   accion =  request.getParameter("accion")!=null &&
                      !request.getParameter("accion").equals("null") &&
                      !request.getParameter("accion").equals("NULL") &&
                      !request.getParameter("accion").equals("")  &&
                      !request.getParameter("accion").equals(" ")
                          ?Integer.parseInt(request.getParameter("accion")+""):0;
   int   fun_rut =  request.getParameter("prof_rama")!=null &&
                           !request.getParameter("prof_rama").equals("null") &&
                           !request.getParameter("prof_rama").equals("NULL") &&
                           !request.getParameter("prof_rama").equals("")  &&
                           !request.getParameter("prof_rama").equals(" ")
                          ?Integer.parseInt(request.getParameter("prof_rama")+""):0;
   int   cod_asign =  request.getParameter("rama")!=null &&
                      !request.getParameter("rama").equals("null") &&
                      !request.getParameter("rama").equals("NULL") &&
                      !request.getParameter("rama").equals("")  &&
                      !request.getParameter("rama").equals(" ")
                          ?Integer.parseInt(request.getParameter("rama")+""):0;
 StringBuffer combo_profValue = new StringBuffer();
 StringBuffer combo_profText  = new StringBuffer();
   Vector vec_prof = new Vector();
   Vector vec_rama = new Vector();

if (rut_usuario == 0)  response.sendRedirect("index.jsp");
boolean soloconsulta = false;
 if (cod_perfil==28) soloconsulta = true;
 Vector vec = new Vector();
 String mensaje = "";
 int error = 0;
 general.getAbrirConexion();
 general.setGenerarConexionRegistro();
 vec_prof = general.getProfesores();

 if (accion > 0){

 switch (accion){
       case 1: { // agregar
            error = general.setInsertaRamaDeportiva(cod_asign, fun_rut, rut_usuario);
            if(error == 0) mensaje = "Se registr� satisfactoriamente";
            else mensaje = "Problemas al registrar ";
            error = -1;
        break;
       }
     case 2: { // actualizar
      error = general.setActualizaRamaDeportiva(cod_asign, fun_rut, rut_usuario);
            if(error == 0) mensaje = "Se registr� satisfactoriamente";
      else mensaje = "Problemas al registrar ";
      error = -1;
       break;
        }
       case 3: { // eliminar
       error = general.setEliminarRamaDeportiva(cod_asign, rut_usuario);
           if(error == 0) mensaje = "Se Elimin� satisfactoriamente";
       else if(error == -4) mensaje = "No se puede eliminar, pues hay de alumno rama asociado";
      else  mensaje = "Problemas al eliminar la Rama deportiva";
       error = -1;
       break;
     }
 }
 }
 vec_rama = general.getAsigRama();
 vec = general.getRamaDeportiva();
 general.getCerrarConexion();
 %>
<html>
<head>
<title>Gimnasio - Rama Deportiva</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script language="JavaScript" src="js/trim.js"></script>
<script LANGUAGE="JavaScript" TYPE="text/javascript">
  function Mensaje(){
 <%if(error<0){%>alert ('<%=mensaje%>');<%}%>
}
 <%if(!soloconsulta){%>
 combo_profValue = new Array();
 combo_profText  = new Array();

<%
  if(vec_prof.size() > 0){
     combo_profValue.append("\"0\"");
     combo_profText.append("\"-- Seleccione --\"");
     for(int i = 0; i < vec_prof.size();i++){
         combo_profValue.append(",\"");
         combo_profValue.append(((Vector)vec_prof.get(i)).get(0));
         combo_profValue.append("\"");
         combo_profText.append(",\"");
         combo_profText.append(((Vector)vec_prof.get(i)).get(3));
         combo_profText.append(" ");
         combo_profText.append(((Vector)vec_prof.get(i)).get(4));
         combo_profText.append(" ");
         combo_profText.append(((Vector)vec_prof.get(i)).get(2));
         combo_profText.append("\"");
       }
     out.println("");
     out.println("combo_profValue = new Array("+combo_profValue+");");
     out.println("");
     out.println("combo_profText  = new Array("+combo_profText+");");
   }

%>
  par = "";
    for (k=0;k < <%=vec_prof.size()%>;k++){
    par = par +  "<option value='"+combo_profValue[k]+"'>"+combo_profText[k]+"</option>";
   }
function cargaLista(){
       setLista(document.fingresar.prof_rama, combo_profValue, combo_profText);

   }
   function setLista(lista, combo_Value, combo_Text){
// Limpia el combo
  lista.options.length = 0;
// Ciclo que llena el combo
  for(i=0; i<combo_Text.length; i++) {
// Crea OPTION del SELECT
    var option           = new Option(combo_Text[i],combo_Value[i]);
    var largo            = lista.length;
    lista.options[largo] = option;
  }
}

function validar(form, accion, i){

    if(form.rama.value ==0){
	    alert("Debe Seleccionar la Rama ");
	    form.rama.focus();
	    return false;
    }
    
    if (accion != 1) {
	  form.prof_rama.value = document.getElementById('prof_rama_mod'+i).value;

	  if(document.getElementById('prof_rama_mod'+i).value ==0){
	    alert("Debe Seleccionar Profesor");
	    form.profesor.focus();
	    return false;
	  }
	  else {
	    if(form.prof_rama.value ==0){
		    alert("Debe Seleccionar Profesor");
		    form.profesor.focus();
		    return false;
		}
	  }
    }
  return true;
	 // form.submit();
}


function Grabar(f, accion, i){
  if (accion != 1) {
	f.prof_rama.value = document.getElementById('prof_rama_mod'+i).value;
  }
  if (!validar(f, accion, i))
    return false;
  f.accion.value = accion;
  f.submit();
}

function Eliminar(f,rama){
  var pregunta = "Ud. va a ELIMINAR la rama definitivamente.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  f.accion.value = 3;
  f.rama.value = rama;
  if (respuesta == true) f.submit();
}
function Editar(i, rama, rut, cod_rama, f) {
   f.rama.value = cod_rama;
   document.getElementById('act'+i+'-2').innerHTML =
      "<select name='prof_rama_mod' id='prof_rama_mod"+i+"' class='Option'>"+
          par +
      "</select>";
  for (var j=0; j < <%=vec_prof.size()%>; j++) {
    if (document.getElementById('prof_rama_mod'+i).options[j].value == rut) {
      document.getElementById('prof_rama_mod'+i).options[j].selected = true;
      break;
    }
  }
  document.getElementById('accion'+i).onclick= function(){Grabar(f,2,i);};
  document ['EDITAR'+i].src = GrabarActivo.src;

  for (var j=0; j < <%=vec.size()%>; j++) {
    document.getElementById('IMAG_ELIMINAR'+j).innerHTML = "<img src='imagen/gim_basurero_des.gif' alt='ELIMINAR' align='absmiddle'>";
    if (j != i) document.getElementById('IMAG_EDITAR'+j).innerHTML = "<img src='imagen/gim_editar_des.gif' alt='EDITAR' align='absmiddle'>";
  }
  return true;
}
<%}%>
function preCargaImg(imageURL) {
                image = new Image();
                image.src = imageURL;
                return image;
}


GrabarActivo = preCargaImg("imagen/gim_grabar.gif");
// -->
</SCRIPT>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style></head>

<body onload=" <%if(!soloconsulta){%>cargaLista();<%}%>Mensaje();">
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td height="45" colspan="3" class="Estilo_Verde">RAMA DEPORTIVA</td>
  </tr>
   <%if(!soloconsulta){%>
    <tr bgcolor="#FFFFFF">
     <form name="fingresar" method="post" action="gim_rama_adm.jsp" target="_self">
    <input type="hidden" name="accion" value="1">
    <td class="Estilo_Gris">Rama</td>
    <td width="46%"><select name="rama" class="Input01" id="rama">
    <option value="0">-- Seleccione --</option>
    <%for (int i=0; i< vec_rama.size();i++){%>
      <option value="<%=((Vector) vec_rama.get(i)).get(0)+ ""%>"><%=((Vector) vec_rama.get(i)).get(1)+ ""%></option>
    <%}%>
    </select></td>
   
  <tr bgcolor="#FFFFFF">
    <td height="20"><span class="Estilo_Gris">Profesor</span></td>
    <td height="20"><select name="prof_rama" class="Input01" id="profesor">


        </select></td>
    <td bgcolor="#FFFFFF"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseOK.gif">
      <tr onClick="Grabar(fingresar,1,0);" onMouseOver="BotonOver(this,'Agregar')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino">Agregar</td>
      </tr>
    </table></td>
    </form>
  </tr>

  <tr bgcolor="#FFFFFF">
    <td width="11%" bgcolor="#FFFFFF" >&nbsp;</td>
    <td colspan="2" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <%}%>
  <%if(vec.size()>0) {%>
  <tr bgcolor="#CCCCCC">
    <td height="35" colspan="3" >
      <table width="100%"  border="0" cellspacing="0" cellpadding="1">
        <tr bgcolor="#999999" class="Estilo_Gris">
          <td width="100%" >
            <table width="100%"  border="0" cellspacing="0" cellpadding="2">
              <tr bgcolor="#008040" class="Estilo_GrisOsc">
                <td height="13" background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">N&deg;</span></td>
                <td background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Rama</span></td>
                <td background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Profesor</span></td>
                 <%if(!soloconsulta){%>
                <td align="center" background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Acci&oacute;n</span></td>
                <%}%>
              </tr>
              <%for(int i=0;i<vec.size();i++){%>
                <tr bgcolor="#E8E8E8" onmouseover='this.style.background="#F4F4F4"' onmouseout='this.style.background="#E8E8E8"'>
                 <%if(!soloconsulta){%>
                <form name="form_grabar<%=i%>" method="post" action="gim_rama_adm.jsp" target="_self">
                 <%}%>
                <td width="5%" height="13" class="Estilo_Azul"><%=i+1%></td>
                <td width="39%" class="Estilo_Azul"><span id="act<%=i%>-1"><%=((Vector) vec.get(i)).get(1)+ ""%></span></td>
                <td width="39%" class="Estilo_Azul"><span id="act<%=i%>-2"><%=((Vector) vec.get(i)).get(5)+ ""%></span></td>
                 <%if(!soloconsulta){%>
                <td width="17%" align="center">
                <span id="IMAG_EDITAR<%=i%>">
                <a HREF="#" id="accion<%=i%>" onClick="Editar('<%=i%>','<%=((Vector) vec.get(i)).get(1)+ ""%>',<%=((Vector) vec.get(i)).get(2)+ ""%>,<%=((Vector) vec.get(i)).get(0)+ ""%>,document.form_grabar<%=i%>); return false">
                <img src="imagen/gim_editar.gif" id="EDITAR<%=i%>" name="EDITAR<%=i%>" alt="EDITAR" border="0" align="absmiddle"></a>
                </span>&nbsp;
                <span id="IMAG_ELIMINAR<%=i%>">
                <a href="#" onClick="Eliminar(document.form_grabar<%=i%>,<%=((Vector) vec.get(i)).get(0)+ ""%>)">
                <img src="imagen/gim_basurero.gif" alt="ELIMINAR" border="0" align="absmiddle"></a>
                </span>
                <input type="hidden" name="accion" value="">
                <input type="hidden" name="rama" value="">
                <input type="hidden" name="prof_rama" value="">
                </td>
                </form><%}%></tr>
               <%}%>
               </table>
          </td>
        </tr>
    </table></td>
  </tr>
  <%}%>
</table>
  <%if(vec.size()>0 &&  !soloconsulta) {%>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="30" valign="bottom"><span class="Estilo_GrisOsc"><img src="imagen/gim_editar.gif" alt="EDITAR" width="18" height="16" align="absmiddle"> Editar&nbsp;&nbsp;&nbsp;<img src="imagen/gim_grabar.gif" alt="GRABAR" width="16" height="16" border="0" align="absmiddle"> Grabar&nbsp;&nbsp;&nbsp;<img src="imagen/gim_basurero.gif" alt="ELIMINAR" width="15" height="18" align="absmiddle"> Eliminar</span></td>
  </tr>
</table>
<%}%>
</body>
</html>