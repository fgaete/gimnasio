<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*,gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 21/08/2007
   // �ltima Actualizaci�n :
   // Bit�cora
   //System.out.print("Punto 1");
%>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%

  response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  
  //System.out.print("Punto 0");

 int rut_usuario = session.getAttribute("rut_usuario") != null?
                   Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
                   
 if (rut_usuario == 0)  {
	 response.sendRedirect("index.jsp");
 }
 else { 
 int cod_perfil  = session.getAttribute("cod_perfil") != null?
                    Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
 int mostrar = 0;
 int anno             = request.getParameter("anno")!=null &&
                         !request.getParameter("anno").equals("null") &&
                         !request.getParameter("anno").equals("NULL") &&
                         !request.getParameter("anno").equals("")  &&
                         !request.getParameter("anno").equals(" ")?
                         Integer.parseInt(request.getParameter("anno")+""):0;
 int semestre         = request.getParameter("semestre")!=null &&
                         !request.getParameter("semestre").equals("null") &&
                         !request.getParameter("semestre").equals("NULL") &&
                         !request.getParameter("semestre").equals("")  &&
                         !request.getParameter("semestre").equals(" ")?
                         Integer.parseInt(request.getParameter("semestre")+""):0;
 int tipo         = request.getParameter("tipo")!=null &&
                        !request.getParameter("tipo").equals("null") &&
                        !request.getParameter("tipo").equals("NULL") &&
                        !request.getParameter("tipo").equals("")  &&
                        !request.getParameter("tipo").equals(" ")?
                         Integer.parseInt(request.getParameter("tipo")+""):0;
 int prioridad         = request.getParameter("prioridad")!=null &&
                       !request.getParameter("prioridad").equals("null") &&
                       !request.getParameter("prioridad").equals("NULL") &&
                       !request.getParameter("prioridad").equals("")  &&
                       !request.getParameter("prioridad").equals(" ")?
                         Integer.parseInt(request.getParameter("prioridad")+""):0;
 String mensaje_bitacora       = request.getParameter("mensaje")!=null &&
                      !request.getParameter("mensaje").equals("null") &&
                      !request.getParameter("mensaje").equals("NULL") &&
                      !request.getParameter("mensaje").equals("")  &&
                      !request.getParameter("mensaje").equals(" ")?
                      request.getParameter("mensaje")+"":"";
 int accion         = request.getParameter("accion")!=null &&
                      !request.getParameter("accion").equals("null") &&
                      !request.getParameter("accion").equals("NULL") &&
                      !request.getParameter("accion").equals("")  &&
                      !request.getParameter("accion").equals(" ")?
                         Integer.parseInt(request.getParameter("accion")+""):0;
 String id_bitacora = request.getParameter("id_bitacora")!=null &&
                      !request.getParameter("id_bitacora").equals("null") &&
                      !request.getParameter("id_bitacora").equals("NULL") &&
                      !request.getParameter("id_bitacora").equals("")  &&
                      !request.getParameter("id_bitacora").equals(" ")?
                      request.getParameter("id_bitacora")+"":"";
 int id_resp = request.getParameter("id_resp")!=null &&
                      !request.getParameter("id_resp").equals("null") &&
                      !request.getParameter("id_resp").equals("NULL") &&
                      !request.getParameter("id_resp").equals("")  &&
                      !request.getParameter("id_resp").equals(" ")?
                      Integer.parseInt(request.getParameter("id_resp")+""):0;
 String respuesta   = request.getParameter("respuesta")!=null &&
                     !request.getParameter("respuesta").equals("null") &&
                     !request.getParameter("respuesta").equals("NULL") &&
                     !request.getParameter("respuesta").equals("")  &&
                     !request.getParameter("respuesta").equals(" ")?
                     request.getParameter("respuesta")+"":"";
 int mes_consulta    = request.getParameter("mes")!=null &&
                        !request.getParameter("mes").equals("null") &&
                        !request.getParameter("mes").equals("NULL") &&
                        !request.getParameter("mes").equals("")  &&
                        !request.getParameter("mes").equals(" ")?
                        Integer.parseInt(request.getParameter("mes")+""):0;

 Vector vec_tipo = new Vector();
 Vector vec_prioridad = new Vector();
 Vector vec_mensaje = new Vector();
 Vector vec_respuesta = new Vector();

 //System.out.print("Punto 1");

 Vector v = new Vector();
 String mensaje = "";
 java.util.Calendar f = new java.util.GregorianCalendar();
 int a�o = f.get(java.util.Calendar.YEAR);
 int mes = f.get(java.util.Calendar.MONTH)+1;
 int dia = f.get(java.util.Calendar.DAY_OF_MONTH);
  if (mes_consulta == 0 ) mes_consulta = mes ;


 int error = 0;
   general.getAbrirConexion();
   general.setGenerarConexionHorario();
   general.setGenerarConexionBitacora();
   int semestre_actual = 0;
   int a�o_actual = 0;
   general.getPeriodoSemestreActual(1,1);
   //System.out.print("Punto 2");
   semestre_actual = general.getSemestreActual();
   a�o_actual = general.getA�oActual();
   //System.out.println("A�oActual: "+a�o_actual+" SemestreActual: "+semestre_actual);
   if (accion > 0){
   switch (accion){
    case 1: { // agregar nuevo mensaje
      while (mensaje_bitacora.indexOf("'") > 1){
        int indice = mensaje_bitacora.indexOf("'");
        mensaje_bitacora = mensaje_bitacora.substring(0,indice) + "\"" +
                           mensaje_bitacora.substring(indice+1);
    }
      //System.out.print("Punto 3");
      v = new Vector();
      v.addElement(anno+"");
      v.addElement(semestre+"");
      v.addElement(prioridad+""); // prioridad
      v.addElement(tipo+""); // tipo
      v.addElement(mensaje_bitacora); // mensaje
      v.addElement(a�o+"/"+mes+"/"+dia);
      v.addElement(rut_usuario+""); // rut_usuario
      error = general.setInsertarBitacora(v);
      if (error == 0) mensaje = "Mensaje registrado satisfactoriamente.";
      else mensaje = "Problemas al registrar mensaje.";
    break;
   }
   case 2: { // modificar mensaje
     while (mensaje_bitacora.indexOf("'") > 1){
     int indice = mensaje_bitacora.indexOf("'");
     mensaje_bitacora = mensaje_bitacora.substring(0,indice) + "\"" +
                        mensaje_bitacora.substring(indice+1);
    }
      v = new Vector();
      v.addElement(id_bitacora);
      v.addElement(mensaje_bitacora);
      v.addElement(rut_usuario+"");
      //System.out.print("Punto 4");
      error = general.setActualizarBitacora(v);
      if (error == 0) mensaje = "Mensaje registrado satisfactoriamente.";
      else mensaje = "Problemas al registrar mensaje.";
       break;
   }
 case 3: { // eliminar mensaje
     error = general.setEliminarBitacora(id_bitacora);
     if (error == 0) mensaje = "Mensaje eliminado satisfactoriamente.";
     else mensaje = "Problemas al eliminar mensaje.";
      break;
   }
 case 4: { // eliminar respuesta
	 //System.out.print("Punto 5");
  error = general.setEliminarRespuestaBitacora(id_bitacora,id_resp );
  if (error == 0) mensaje = "Respuesta eliminada satisfactoriamente.";
  else mensaje = "Problemas al eliminar la respuesta.";
   break;
 }
 case 5: { // Agregar respuesta
   while (respuesta.indexOf("'") > 1){
   int indice = respuesta.indexOf("'");
   respuesta = respuesta.substring(0,indice) + "\"" +
               respuesta.substring(indice+1);
    }
   v = new Vector();
   v.addElement(id_bitacora);
   v.addElement(rut_usuario+"");
   v.addElement(respuesta);
   if (respuesta.trim()!="")
   		error = general.setInsertarRespuesta(v );
        else error = 1;	
  if (error == 0) mensaje = "Respuesta Registrada satisfactoriamente.";
  else if (error ==1) mensaje = "Ingrese una Respuesta";
  else mensaje = "Problemas al registrar la respuesta.";
  break;
 }
 //System.out.print("Punto 6");
case 6: { // Modificar respuesta
   while (respuesta.indexOf("'") > 1){
    int indice = respuesta.indexOf("'");
    respuesta = respuesta.substring(0,indice) + "\"" +
                respuesta.substring(indice+1);
  }
  v = new Vector();
  v.addElement(id_bitacora);
  v.addElement(id_resp+"");
  v.addElement(respuesta);
  v.addElement(rut_usuario+"");
  error = general.setActualizarRespuesta(v );
 if (error == 0) mensaje = "Respuesta Actualizada satisfactoriamente.";
 else mensaje = "Problemas al actualizar la respuesta.";
 break;
 }

   }
   error = -1;
   } else {

  if (anno == 0){
     semestre = semestre_actual;
     anno = a�o_actual;
   }
   }
   vec_tipo = general.getTipoContacto();
   vec_prioridad = general.getTipoPrioridad();
   vec_mensaje = general.getMensaje(anno, semestre, mes_consulta);
   vec_respuesta = general.getRespuestaMensaje(anno, semestre, mes_consulta);
   Vector vec_meses = new Vector();
   general.getPeriodoSemestre(anno,semestre);
   String fecha_inicio_periodo = String.valueOf(general.getFechaInicioPeriodo());
   String fecha_termino_periodo = String.valueOf(general.getFechaTerminoPeriodo());
//System.out.println("inicio="+fecha_inicio_periodo);
//System.out.println("termino="+fecha_termino_periodo);
   int mes_inicio_periodo;
   int mes_termino_periodo;
   if (fecha_inicio_periodo.equals("0")){
	   mes_inicio_periodo=0;
	   
   }else{
	   mes_inicio_periodo = Integer.parseInt(fecha_inicio_periodo.substring(4,6));	   
   }
   if (fecha_termino_periodo.equals("0")){
	   mes_termino_periodo=0;
   }else{ mes_termino_periodo = Integer.parseInt(fecha_termino_periodo.substring(4,6));
   }

   int cuenta_mes_inicio = mes_inicio_periodo;
   if (mes_inicio_periodo > mes_termino_periodo && mes_termino_periodo == 1)  mes_termino_periodo = 13;
   int cuenta_mes_termino = (mes_termino_periodo==3)?12:mes_termino_periodo;
   
//System.out.println("mes_inicio_periodo: "+mes_inicio_periodo+" / mes_termino_periodo: "+mes_termino_periodo+" / cuenta_mes_inicio : "+cuenta_mes_inicio+" / cuenta_mes_terminocuenta_mes_termino: "+cuenta_mes_termino);
   String nom_mes = "";
   //System.out.print("Punto 7");
   while (cuenta_mes_inicio <= cuenta_mes_termino){
     switch (cuenta_mes_inicio){
     case 1: nom_mes = "Enero";
     break;
     case 2: nom_mes = "Febrero";
     break;
     case 3: nom_mes = "Marzo";
     break;
     case 4: nom_mes = "Abril";
     break;
     case 5: nom_mes = "Mayo";
     break;
     case 6: nom_mes = "Junio";
     break;
     case 7: nom_mes = "Julio";
     break;
     case 8: nom_mes = "Agosto";
     break;
     case 9: nom_mes = "Septiembre";
     break;
     case 10: nom_mes = "Octubre";
     break;
     case 11: nom_mes = "Noviembre";
     break;
     case 12: nom_mes = "Diciembre";
     break;
     case 13: nom_mes = "Enero " + (anno + 1);
     break;
     default: nom_mes = "Sin registros";
   }
     v = new Vector();
     v.addElement(nom_mes);
     v.addElement(String.valueOf(cuenta_mes_inicio==13?1:cuenta_mes_inicio));
     vec_meses.addElement(v);   
     //System.out.print("Punto 8");
     cuenta_mes_inicio++;
     /*esto es un parche para el a�o 2011 semestre 2
     if(cuenta_mes_inicio == 13){
    	 cuenta_mes_inicio = 1;
    	 cuenta_mes_termino = 3;
     }*/

   }
   /* 
   if(mes_termino_periodo==1){
        v = new Vector();
     v.addElement("Enero");
     v.addElement("1");
     vec_meses.addElement(v);
   }
  if (semestre == 1){
     v = new Vector();
     v.addElement("Enero");
     v.addElement("1");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Febrero");
     v.addElement("2");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Marzo");
     v.addElement("3");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Abril");
     v.addElement("4");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Mayo");
     v.addElement("5");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Junio");
     v.addElement("6");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Julio");
     v.addElement("7");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Agosto");
     v.addElement("8");
     vec_meses.addElement(v);
   } else {
      v = new Vector();
     v.addElement("Julio");
     v.addElement("7");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Agosto");
     v.addElement("8");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Septiembre");
     v.addElement("9");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Octubre");
     v.addElement("10");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Noviembre");
     v.addElement("11");
     vec_meses.addElement(v);
     v = new Vector();
     v.addElement("Diciembre");
     v.addElement("12");
     vec_meses.addElement(v);
   }*/
   general.getCerrarConexion();
   //System.out.print("Punto 9");
%>
<html>
<head>
<title>Gimnasio - Bit�cora</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script src="js/link.js"></script>
<script src="js/layer.js"></script>
<script src="js/trim.js"></script>
<script LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
<%if (mostrar != 1){ %>
var mensajes = new Array();
var respuestas = new Array();
var resp = new Array();

<% StringBuffer mensajes = new StringBuffer();
  StringBuffer resp = new StringBuffer();
    StringBuffer respuestas = new StringBuffer();
    Vector fila_resp = new Vector();
    String id = "";
    int ind = 0;

   if (vec_mensaje.size() > 0)
   mensajes.append("'");
  for(int i=0; i < vec_mensaje.size(); i++){
    mensajes.append(((Vector)vec_mensaje.get(i)).get(4)+"");
    mensajes.append("'");
    if ((i + 1) < vec_mensaje.size()) {
      mensajes.append(",'");

      }
      resp = new StringBuffer();

      for (int j=0;j < vec_respuesta.size();j++){
        fila_resp = new Vector();
        fila_resp = (Vector)vec_respuesta.get(j);

        if ((fila_resp.get(0)+"").trim().equals(((Vector)vec_mensaje.get(i)).get(0)+"") &&
            !(fila_resp.get(1)+"").equals("0")){

          if ( id.equals(fila_resp.get(0)+""))
          {
            resp.append(",'");

            } else  resp.append("'");

          resp.append(fila_resp.get(5));
          resp.append("'");
          }
          id = fila_resp.get(0)+"";
       }
       for (int r=0; r < resp.length(); r++) {
     if (resp.indexOf("\n") == -1) break;
      resp.replace(resp.indexOf("\n")-1,resp.indexOf("\n")+1,"\\n");
       }


      out.println("");
      out.println("respuestas["+i+"] = new Array("+resp+");"); // mensaje
      out.println("");
      resp = new StringBuffer();
      resp.append("'");


  }

  /*for (int m=0; m < mensajes.length(); m++) {
       if (mensajes.indexOf("\n") == -1) break;
       mensajes.replace(mensajes.indexOf("\n")-1,mensajes.indexOf("\n")+1,"\\n");
     }
 */
  while (mensajes.indexOf("\n") > -1) {
    mensajes.replace(mensajes.indexOf("\n")-1,mensajes.indexOf("\n")+1,"\\n");
  }

  for (int m=0; m < mensajes.length(); m++) {
    if (mensajes.charAt(m) == 13)
    mensajes.replace(m,m+1,"\\n");
  }

  out.println("");
  out.println("mensajes = new Array("+mensajes+");"); // mensaje
  out.println("");

  %>


function Grabar(f,accion, id_caja, id_resp){

  if (accion == 2) // es mensaje
    f.mensaje.value = document.getElementById(id_caja).value;
  else // es respuesta
    f.respuesta.value = document.getElementById(id_caja).value;
  if (id_resp > 0) f.id_resp.value = id_resp;
  f.mes.value = document.form_agregar.mes.value;
  f.accion.value = accion;
  f.submit();
}

function Eliminar(f,tipo,id_resp){

  var texto = "";
  if (tipo == 1) {
    texto = "EL MENSAJE";
    f.accion.value = "3";
  }
  if (tipo == 2) {
    texto = "LA RESPUESTA";
     f.accion.value = "4";
     if (id_resp == ''){
      alert ('No se puede eliminar, pues no ha sido ingresado');
  }
  }

 if (tipo==2)   f.id_resp.value = id_resp;
  var pregunta = "Ud. va a ELIMINAR " + texto + " definitivamente.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  if (respuesta == true) f.submit();
}
function replaceAll( str, from, to ) {
     var idx = str.indexOf( from );
     while ( idx > -1 ) {
         str = str.replace( from, to );
         idx = str.indexOf( from );
     }

     return str;
 }

function EditarMsg(i, f, deshacer) {
  //alert('EditarMsg');
  if (deshacer == 0) {
    DisableEnableLinks(true);
    id_caja = 'tmensaje'+i;
    document.getElementById('mensaje'+i).innerHTML =
        "<textarea name='tmensaje'"+i+" id ='"+id_caja+"' maxlength='255' cols='60' rows='3' onkeypress='comillas();'>"+mensajes[i]+"</textarea>";
     document.getElementById('imagen'+i+'_1').innerHTML =
     "&nbsp;&nbsp;<a HREF='javascript:void(0)' onClick='Grabar(document.form_grabar"+i+",2,\""+id_caja+"\",0);' title=\"Modificar Mensaje\" >"+
     "<img src='imagen/gim_grabar.gif' alt='REGISTRAR' border='0' align='absmiddle'></a>";
    document.getElementById('imagen'+i+'_2').innerHTML =
        "&nbsp;<a HREF='javascript:void(0)' onClick='Eliminar(document.form_grabar"+i+",1,0)'>"+
        "<img src='imagen/gim_basurero.gif' alt='ELIMINAR' border='0' align='absmiddle'></a>";
    document.getElementById('imagen'+i+'_3').innerHTML =
        "&nbsp;<a HREF='javascript:void(0)' onClick='EditarMsg("+i+",document.form_grabar"+i+",1); return false'>"+
        "<img src='imagen/gim_deshacer.gif' alt='DESHACER' border='0' align='absmiddle'></a>";
  }
  else {
    men = mensajes[i];
    men = replaceAll(mensajes[i],"\n","<br>");
      document.getElementById('mensaje'+i).innerHTML =
        "<a HREF='javascript:void(0)' onClick='EditarMsg("+i+",document.form_grabar"+i+",0); return false' class='AIngr' title=\"Modificar Mensaje\">"+men+"</a>";
    document.getElementById('imagen'+i+'_1').innerHTML = "";
    document.getElementById('imagen'+i+'_2').innerHTML = "";
    document.getElementById('imagen'+i+'_3').innerHTML = "";
    DisableEnableLinks(false);
  }
  return true;
}
function EditarResp(i, j, id_resp, f, deshacer) {

  var num_resp = j;
  var texto    = "";
  var salto    = "&nbsp;&nbsp;";
  // si es nueva respuesta
  if (j == -1) num_resp = 'N';
  // si edita
  if (deshacer == 0) {
    // deshabilito links
    DisableEnableLinks(true);
    // si no es nueva, obtengo la respuesta
    if (j > -1) texto = respuestas[i][j];
    else salto = "<BR>"; // se le agrega un salto antes de las imagenes para resp.nueva
    id_caja = 'trespuesta'+i+'_'+num_resp;
    document.getElementById('respuesta'+i+'_'+num_resp).innerHTML =
        "<textarea name='trespuesta"+i+"_"+num_resp+"' id ='"+id_caja+"' cols='60' rows='2' maxlength='255' onkeypress='comillas();'>"+texto+"</textarea>"+
        "<input name='id_respuesta'  type='hidden' value='"+id_resp+"'>";
    if (id_resp > 0) accion = 6
      else accion = 5;
    document.getElementById('imagen'+i+"_"+num_resp+'_1').innerHTML =
         salto + "<a HREF='javascript:void(0)' onClick='Grabar(document.form_grabar"+i+","+accion+",\""+id_caja+"\","+id_resp+")' title=\"Modificar Respuesta\">"+
         "<img src='imagen/gim_grabar.gif' alt='REGISTRAR' border='0' align='absmiddle'></a>";

     if (j > -1)
       document.getElementById('imagen'+i+"_"+num_resp+'_2').innerHTML =
       "&nbsp; <a HREF='javascript:void(0)' onClick='Eliminar(document.form_grabar"+i+",2,"+id_resp+")' title=\"Modificar Respuesta\">"+
       "<img src='imagen/gim_basurero.gif' alt='ELIMINAR' border='0' align='absmiddle'></a>";

     document.getElementById('imagen'+i+"_"+num_resp+'_3').innerHTML =
         "&nbsp;<a HREF='javascript:void(0)' onClick='EditarResp("+i+", "+j+", "+id_resp+", document.form_grabar"+i+",1); return false' title=\"Modificar Respuesta\">"+
         "<img src='imagen/gim_deshacer.gif' alt='DESHACER' border='0' align='absmiddle'></a>";

  }
  //  si deshace edici�n
  else {
    // si es nueva respuesta
    if (j == -1) {
      num_resp = 'N';
      document.getElementById('respuesta'+i+'_N').innerHTML =
          "<img src='imagen/gim_responder.gif' border='0' align='absmiddle'>"+
          "<a HREF='javascript:void(0)' onClick='EditarResp("+i+", -1, 0, document.form_grabar"+i+",0); return false' class='AModif' title=\"Modificar Respuesta\">"+
          "&nbsp;Ingrese respuesta</a>";
    }
    // si se edita una respuesta
    else {
      resp = replaceAll(respuestas[i][j],"\n","<br>");

      document.getElementById('respuesta'+i+'_'+j).innerHTML =
          "<a HREF='javascript:void(0)' onClick='EditarResp("+i+", "+j+", "+id_resp+", document.form_grabar"+i+",0); return false' class='AIngr' title=\"Modificar Respuesta\">"+resp+"</a>";
    }
    document.getElementById('imagen'+i+"_"+num_resp+'_1').innerHTML = "";
    document.getElementById('imagen'+i+"_"+num_resp+'_2').innerHTML = "";
    document.getElementById('imagen'+i+"_"+num_resp+'_3').innerHTML = "";
    DisableEnableLinks(false);
  }
  return true;
}
function IniciaBloques(){
//alert("A�o: "+<%=anno%>+" = A�o actual: "+<%=a�o_actual%>+" && \n semestre: "+<%=semestre%>+" = semestreActual: "+<%=semestre_actual%>+" && \n mesConsulta: "+<%=mes_consulta%>+ " = mes: "+<%=mes%>);
  if (<%=anno%> == <%=a�o_actual%> && <%=semestre%> == <%=semestre_actual%> &&
     <%=mes_consulta%> == <%=mes%>)
       document.getElementById('TITULO_bitacora').innerHTML = "<a href='javascript:void(0);' onClick='NuevoMensaje()'><img src='imagen/gim_editar.gif'  border='0' align='absmiddle'> Nuevo Mensaje</a>";
  escondeBloque('BLOQUE_agregar' , 1); // escondo
  return true;
}
function NuevoMensaje(){
  document.getElementById('TITULO_bitacora').innerHTML = "MENSAJE";
  escondeBloque('BLOQUE_agregar' , 2); // muestro
  return true;
}
function Agregar(f){
  if (f.tipo.value == -1)
  {
    alert ('Debe seleccionar el tipo de mensaje a registrar.');
    f.tipo.focus();
    return false;
  }
  if (f.prioridad.value == -1)
{
  alert ('Debe seleccionar el tipo de prioridad que tiene el mensaje a registrar.');
  f.prioridad.focus();
  return false;
  }
  if (trim(f.mensaje.value) == "")
{
  alert ('Debe indicar el mensaje a registrar.');
  f.mensaje.focus();
  return false;
  }
  f.accion.value = "1";
 // f.mes.value = <%=mes%>;
  f.submit();
}
function Mensaje(){
<%if(error<0){%>alert ('<%=mensaje%>');<%}%>
    return;
    }
<%}%>
  function noenter() {
    //return !(window.event && window.event.keyCode == 13);
  if(window.event){
   if(window.event.keyCode == 13){
      return false;
   }
}
else{
  if(e.which == 13){
    return false;
  }
}
  }

  function comillas() {
  //return !(window.event && window.event.keyCode == 13);
if(window.event ){
   if (window.event.keyCode == 39)
   window.event.keyCode = 34;
   return false;
}
else{
  if (e.which == 39)
   e.which = 34;
  return false;

}
  }

// -->
</SCRIPT>
<style type="text/css">
<!--
body {
        margin-left: 0px;
        margin-top: 0px;
}
-->
</style></head>

<body  <%if (mostrar != 1){ %>onLoad="IniciaBloques(); Mensaje();" <%}%>>
<%if (mostrar != 1){ %>
 <form name="form_agregar" method="post" action="gim_bitacora_adm.jsp" target="_self">
 <input type="hidden" name="accion" value="">
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td height="25" class="Estilo_Verde">BIT&Aacute;CORA</td></tr>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
   <tr bgcolor="#FFFFFF">
    <td width="66" height="20" bgcolor="#FFFFFF" class="Estilo_Gris">Semestre</td>
    <td width="921" height="20" colspan="2" bgcolor="#FFFFFF">    <span class="Estilo_Azul">
      <select name="anno" class="Option" id="anno" onchange="document.form_agregar.submit();">
        <%for(int i = 2006;i<=a�o;i++){%>
        <option value="<%=i%>" <%=(i==anno)?"selected":""%>><%=i%></option>
        <%}%>
      </select>
-
<select name="semestre" class="Option" id="semestre" onchange="document.form_agregar.submit();">
  <option value="1" <%=(semestre==1)?"selected":""%>>1</option>
  <option value="2" <%=(semestre==2)?"selected":""%>>2</option>
  <option value="3" <%=(semestre==3)?"selected":""%>>3</option>
</select>
</span></td>
  </tr>
</table>

<DIV id="BLOQUE_agregar" style="display:none">
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr bgcolor="#FFFFFF">
    <td width="139" bgcolor="#FFFFFF" class="Estilo_Gris">Tipo</td>
    <td colspan="2" bgcolor="#FFFFFF"><span class="Estilo_Azul">
      <select name="tipo" class="Option" id="tipo">
        <option value="-1">-- Seleccione --</option>
        <%for (int i=0;i < vec_tipo.size();i++){%>
        <option value="<%=((Vector)vec_tipo.get(i)).get(0)%>"><%=((Vector)vec_tipo.get(i)).get(1)%></option>
        <%}%>
      </select>
    </span></td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td bgcolor="#FFFFFF" class="Estilo_Gris">Prioridad</td>
    <td colspan="2" bgcolor="#FFFFFF"><span class="Estilo_Azul">
      <select name="prioridad" class="Option" id="prioridad">
        <option value="-1">-- Seleccione --</option>
          <%for (int i=0; i <  vec_prioridad.size();i++){%>
        <option value="<%=((Vector)vec_prioridad.get(i)).get(0)%>"><%=((Vector)vec_prioridad.get(i)).get(1)%></option>
        <%}%>

      </select>
    </span></td>
  </tr>
  <tr valign="top" bgcolor="#FFFFFF">
    <td bgcolor="#FFFFFF" class="Estilo_Gris">Mensaje</td>
    <td colspan="2" bgcolor="#FFFFFF"><textarea name="mensaje" maxlength="255" cols="60" rows="3" id="mensaje" onkeypress="comillas();"></textarea></td></tr>
  <tr valign="top" bgcolor="#FFFFFF">
    <td bgcolor="#FFFFFF" class="Estilo_Gris">&nbsp;</td>
    <td width="136" bgcolor="#FFFFFF"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseOK.gif">
      <tr onClick="Agregar(document.form_agregar)" onMouseOver="BotonOver(this,'Agregar')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino">Agregar</td>
      </tr>
    </table></td>
    <td width="718" bgcolor="#FFFFFF"><table height="21"  border="0" cellpadding="0" cellspacing="0">
      <tr onClick="IniciaBloques()" onMouseOver="BotonOver(this,'Deshacer')" onMouseOut="BotonOut(this)">
        <td width="21"><img src="imagen/boton_baseUNDO01.gif" width="21" height="21"></td>
        <td width="60" valign="middle" nowrap background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino"> Deshacer</td>
        <td width="3"><img src="imagen/boton_baseBORDEDER.gif" width="3" height="21"></td>
      </tr>
    </table></td>
  </tr>

</table><br>
</DIV>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="50%" height="25" class="Estilo_Verde"><span id="TITULO_bitacora"></span></td>
    <td width="50%" align="right" class="Estilo_Azul">
<% int indice = vec_mensaje.size();
   String s_indice = "";
   if (indice < 10) s_indice = "00"+ indice;
   else
     if (indice >= 10 && indice < 100) s_indice = "0"+ indice;
   else
     s_indice = indice+"";%>
        Mes &nbsp;
          <select name="mes" class="Option" id="mes" onchange="document.form_agregar.submit();">
          <%for (int k=0;k < vec_meses.size();k++){%>
            <option value="<%=((Vector)vec_meses.get(k)).get(1)%>" <%=(Integer.parseInt(((Vector)vec_meses.get(k)).get(1)+"")==mes_consulta)?"selected":""%>><%=((Vector)vec_meses.get(k)).get(0)%></option>
          <%}%>
          </select>
    </td>
  </tr>
</table>
  </form>
  <% for (int i=0; i < vec_mensaje.size();i++){%>

<table width="100%"  border="0" cellspacing="0" cellpadding="2">
      <tr bgcolor="#CCCCCC">
	     <td width="100%">
 <form name="form_grabar<%=i%>" method="post" action="gim_bitacora_adm.jsp" target="_self">

        <table width="100%"  border="0" cellspacing="0" cellpadding="3">

          <tr bgcolor="#FFFFFF">

            <td width="11%" class="Estilo_Gris"><%=s_indice%></td>
            <td width="89%">
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="15%"><span class="Estilo_AzulinoBold"><%=((Vector)vec_mensaje.get(i)).get(1)%></span></td>
                  <td width="59%" align="center"><span class="Estilo_Azul"><%=((Vector)vec_mensaje.get(i)).get(2)%></span></td>
                  <td width="26%" align="right"><span class="Estilo_Gris">Prioridad</span> <span class="Estilo_Rojo">&nbsp;<strong><%=((Vector)vec_mensaje.get(i)).get(3)%></strong></span> </td>
                </tr>
            </table></td>
          </tr>
          <tr valign="top" bgcolor="#FFFFFF">
            <td class="Estilo_Gris">Mensaje</td>
            <td><span id="mensaje<%=i%>">
            <%String men = "";
              men = ((Vector)vec_mensaje.get(i)).get(4).toString();
              men = men.replaceAll("\n","<br>");

              if (Integer.parseInt(((Vector)vec_mensaje.get(i)).get(6)+"")== rut_usuario ||
                  cod_perfil == 2){
                 %>
            <a HREF="javascript:void(0)" onClick="EditarMsg(<%=i%>,document.form_grabar<%=i%>,0); return false" class="AIngr" title="Modificar Mensaje">
            <%}%>
            <%=men%>
              <%if (Integer.parseInt(((Vector)vec_mensaje.get(i)).get(6)+"")== rut_usuario ||
                  cod_perfil == 2){%>
              </a>
              <%}%>
            </span><br>
            <span class="Estilo_Azulino letra7"><%=((Vector)vec_mensaje.get(i)).get(5)%></span><span id="imagen<%=i%>_1"></span><span id="imagen<%=i%>_2"></span><span id="imagen<%=i%>_3"></span></td>
          </tr>
          <tr valign="top" bgcolor="#FFFFFF">
            <td class="Estilo_Gris"><p>Respuesta</p></td>
            <td>
            <table width="100%"  border="0" cellspacing="1" cellpadding="1">
           <%int ind = 0;
              for (int j=0;j < vec_respuesta.size();j++){
              Vector fila_resp = new Vector();
              fila_resp = (Vector)vec_respuesta.get(j);
              String resp = fila_resp.get(5).toString();
              resp = resp.replaceAll("\n","<br>");

               if ((fila_resp.get(0)+"").trim().equals(((Vector)vec_mensaje.get(i)).get(0)+"") &&
                   !(fila_resp.get(1)+"").equals("0")){
           %>
              <tr>
                <td><span id="respuesta<%=i%>_<%=ind%>">
                    <%if ((fila_resp.get(4)+"").equals(rut_usuario+"") ||
                      cod_perfil == 2){%>
                <a HREF="javascript:void(0)" onClick="EditarResp(<%=i%>, <%=ind%>, <%=fila_resp.get(1)+""%>, document.form_grabar<%=i%>,0); return false" class="AIngr" title ="Modificar Respuesta">
                <%}%><%=resp%><%if ((fila_resp.get(4)+"").equals(rut_usuario+"") ||
                  cod_perfil == 2){%></a><%}%></span><br>
                  <span class="Estilo_Azulino letra7"><%=fila_resp.get(3)%> (<%=fila_resp.get(2)%>)</span><span id="imagen<%=i%>_<%=ind%>_1"></span><span id="imagen<%=i%>_<%=ind%>_2"></span><span id="imagen<%=i%>_<%=ind%>_3"></span></td>
              </tr>
              <%ind++;}}%>
              <tr>
                <td height="20">
                <span id="respuesta<%=i%>_N">
                <img src="imagen/gim_responder.gif" border="0" align="absmiddle">
                <a HREF="javascript:void(0)" onClick="EditarResp(<%=i%>, -1, 0, document.form_grabar<%=i%>,0); return false" class="AModif">Ingrese respuesta</a></span>
                <span id="imagen<%=i%>_N_1"></span><span id="imagen<%=i%>_N_2"></span><span id="imagen<%=i%>_N_3"></span>
                </td>
                </tr>
            </table>
            </td>

        </tr>
        </table>
         <input name="id_bitacora" type="hidden" value="<%=((Vector)vec_mensaje.get(i)).get(0)%>">
        <input name="accion" type="hidden" value="">
        <input name="id_resp" type="hidden" value="">
         <input name="respuesta" type="hidden" value="">
         <input name="mensaje" type="hidden" value="">
         <input name="mes" type="hidden" value="<%=mes_consulta%>">
         <input name="anno" type="hidden" value="<%=anno%>">
         <input name="semestre" type="hidden" value="<%=semestre%>">
        </form>
       </td>
      </tr>
</table><br>
       <%indice = indice - 1;
          if (indice < 10) s_indice = "00"+ indice;
            else
               if (indice >= 10 && indice < 100) s_indice = "0"+ indice;
                 else
                   s_indice = indice +"";
          }%>
<%}%>
</body>
</html>
<%}%>