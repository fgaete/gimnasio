<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 26/06/2007
   // �ltima Actualizaci�n :
   // Listado.               Se listan los alumnos premiados y sancionados del per�odo.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>


<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
   int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int   a�o =  request.getParameter("anno")!=null &&
                            !request.getParameter("anno").equals("null") &&
                            !request.getParameter("anno").equals("NULL") &&
                            !request.getParameter("anno").equals("")  &&
                            !request.getParameter("anno").equals(" ")
                            ?Integer.parseInt(request.getParameter("anno")+""):0;
  int   semestre =  request.getParameter("semestre")!=null &&
                          !request.getParameter("semestre").equals("null") &&
                          !request.getParameter("semestre").equals("NULL") &&
                          !request.getParameter("semestre").equals("")  &&
                          !request.getParameter("semestre").equals(" ")
                          ?Integer.parseInt(request.getParameter("semestre")+""):0;
  String   id =  request.getParameter("id")!=null &&
                        !request.getParameter("id").equals("null") &&
                        !request.getParameter("id").equals("NULL") &&
                        !request.getParameter("id").equals("")  &&
                        !request.getParameter("id").equals(" ")
                        ?request.getParameter("id")+"":"0";
  String   reggim_id =  request.getParameter("reggim_id")!=null &&
                      !request.getParameter("reggim_id").equals("null") &&
                      !request.getParameter("reggim_id").equals("NULL") &&
                      !request.getParameter("reggim_id").equals("")  &&
                      !request.getParameter("reggim_id").equals(" ")
                        ?request.getParameter("reggim_id")+"":"0";
  int   estado =  request.getParameter("estado")!=null &&
                        !request.getParameter("estado").equals("null") &&
                        !request.getParameter("estado").equals("NULL") &&
                        !request.getParameter("estado").equals("")  &&
                        !request.getParameter("estado").equals(" ")
                          ?Integer.parseInt(request.getParameter("estado")+""):0;
   int cod_perfil = session.getAttribute("cod_perfil") != null?
                   Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
   int   accion =  request.getParameter("accion")!=null &&
                  !request.getParameter("accion").equals("null") &&
                  !request.getParameter("accion").equals("NULL") &&
                  !request.getParameter("accion").equals("")  &&
                  !request.getParameter("accion").equals(" ")
               ?Integer.parseInt(request.getParameter("accion")+""):0;
   String mensaje = "";
  int error = 0;
  int   sala =  10335;
  boolean soloconsulta = false;

 if (cod_perfil==28) soloconsulta = true;

  java.util.Vector vec = new java.util.Vector();

 // java.util.Vector vec_vi�a = new java.util.Vector();
  String titulo = (estado == 6)?"Alumnos Premiados "+  a�o +"-"+ semestre + "<br>":"Alumnos Sancionados "+  a�o +"-"+ semestre + "<br>";
  String nom_sala = (sala == 10335)?"Sala de M�quinas y Pesas":"Sala de Aer�bica";

  if (a�o > 0 && semestre > 0 ) {
    general.getAbrirConexion();
  //  general.getAbrirConexionVi�a();
    general.setGenerarConexionReserva();
  //  general.setGenerarConexionDatosVi�a();
    if (accion > 0){
         // elimina registro
          error = general.setEliminaRegistroPremiadoSancionado(id, reggim_id );
          if (error == 0) mensaje = "Eliminaci�n satisfactoria";
          else mensaje = "Problemas al eliminar";

   }

    vec = general.getPremiadosPeriodoList(a�o, semestre, id , estado);
   // vec_vi�a = general.getPremiadosPeriodoListVi�a(a�o, semestre, id, estado);
    general.getCerrarConexion();
    if (vec.size()> 0)
   titulo += ((Vector) vec.get(0)).get(5)+ " al " +((Vector) vec.get(0)).get(6)+ "<br>"+nom_sala;
// else
//   if (vec_vi�a.size()> 0)
//     titulo += ((Vector) vec_vi�a.get(0)).get(5)+ " al " +((Vector) vec_vi�a.get(0)).get(6)+ "<br>"+nom_sala;

  //  general.getCerrarConexionVi�a();
  }
java.util.Calendar f = new java.util.GregorianCalendar();
int dia = f.get(java.util.Calendar.DAY_OF_MONTH);
int mes = f.get(java.util.Calendar.MONTH) + 1;
int anno = f.get(java.util.Calendar.YEAR);
int hora = f.get(java.util.Calendar.HOUR_OF_DAY);
int minuto = f.get(java.util.Calendar.MINUTE);


String  dn       = "AM";
if (hora>12){
  dn    = "PM";
  hora = hora - 12;
}
  if (hora == 0) hora = 12;
%>
<html>
<head>
<title>Gimnasio - Lista de Premiados / Sancionados</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #000000}
-->
</style>
<script language = "javascript" >
 function Mensaje(){
<%if(error<0){%>alert ('<%=mensaje%>');<%}%>
}
function Eliminar(f){
  var pregunta = "Ud. va a ELIMINAR este registro definitivamente.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  if (respuesta == true) f.submit();
}
</script>
</head>
<body onload = "Mensaje();">

<table width="100%"  border="0" cellspacing="0" cellpadding="1">
<%if ((vec != null && vec.size()>0) /*||
      (vec_vi�a != null && vec_vi�a.size()>0)*/){%>
  <tr>
    <td height="20" ><table width="100%"  border="0">
      <tr>
        <td width="20%" height="50" class="letra8" >Gimnasio III<br>
          USM </td>
        <td width="65%" class="letra10" align="center"><%=titulo%><span class="letra8"><br>
          </span></td>
        <td width="15%" >&nbsp;
          <div align="right"><span class="letra8"><%=dia%>/<%=mes%>/<%=anno%><br>
            <%=hora%>:<%=minuto%>  <%=dn%></span></div></td>
      </tr>
    </table>      </td>
  </tr>
  <tr>
    <td width="100%" height="20" align="center" valign="top" class="letra8">&nbsp;</td>
  </tr>
  <tr>
    <td height="20" align="center" valign="top" class="letra10">&nbsp;</td>
  </tr>
</table>
<table width="100%" height="49%"  border="1" cellpadding="2" cellspacing="1">
  <tr bgcolor="#FFFFFF">
    <td width="7%" class="Estilo_GrisBold">N&deg;</td>
    <td width="16%" class="Estilo_GrisBold">Paterno</td>
    <td width="17%" class="Estilo_GrisBold">Materno</td>
    <td width="18%" class="Estilo_GrisBold">Nombres</td>
    <td  class="Estilo_GrisBold">RUT</td>
    <td width="21%" class="Estilo_GrisBold">Email</td>
	<%if (!soloconsulta){%>
	<td width="7%" align="center" class="Estilo_GrisBold">Eliminar</td>
	<%}%>
  </tr>
  <%for (int i=0;vec != null && i < vec.size(); i++){%>
  <form name="form_eliminar<%=i%>" method="post" action="gim_prem_sancionado_list.jsp" target="_self">
  <tr bgcolor="#FFFFFF">
    <td class="Estilo_Azul"><%=i+1%>
    <input type="hidden" name="id" value="<%=id%>">
    <input type="hidden" name="accion" value="1">
    <input type="hidden" name="anno" value="<%=a�o%>">
    <input type="hidden" name="semestre" value="<%=semestre%>">
    <input type="hidden" name="estado" value="<%=estado%>">
    <input type="hidden" name="reggim_id" value="<%=((Vector) vec.get(i)).get(7)+ ""%>"></td>
    <td class="Estilo_Azul"><%=((Vector) vec.get(i)).get(0)+ ""%></td>
    <td class="Estilo_Azul"><%=((Vector) vec.get(i)).get(1)+ ""%>&nbsp;</td>
    <td class="Estilo_Azul"><%=((Vector) vec.get(i)).get(2)+ ""%></td>
    <td width="14%" class="Estilo_Azul" nowrap><%=((Vector) vec.get(i)).get(3)+ ""%></td>
    <td  class="Estilo_Azul"><%=(!(((Vector) vec.get(i)).get(4)+ "").trim().equals(""))?((Vector) vec.get(i)).get(4)+ "":"&nbsp;"%></td>
     <%if (!soloconsulta){%>
	 <td align="center"  class="Estilo_Azul"> <a HREF="#"  onClick="Eliminar(document.form_eliminar<%=i%>); ">
                <img src="imagen/gim_basurero.gif" alt="ELIMINAR" width="15" height="18" align="absmiddle" border="0">
                </a></td>
				<%}%>
 </tr>
 </form>
   <%}%>

 </table>
   <%} else { if (a�o>0 && semestre>0 ){%>
  <tr bgcolor="#FFFFFF">
    <td height="16%" colspan="5" valign="top" class="Estilo_Azul"><div align="center"><br>
      <br>
      <span class="Estilo1">No se encontraron Registros</span> </div></td>
  </tr>
 <%}}%>
 </table>
<%if (vec != null && vec.size()>0){%>
<div align="center"><font class="barra" color="#000000"><br>
  <br>
  &copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a</font>
</div><%}%>
</body>
</html>
