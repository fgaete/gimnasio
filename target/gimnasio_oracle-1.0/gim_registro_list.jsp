<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 18/06/2007
   // �ltima Actualizaci�n :
   // Registro listado.    Se listan los registros del a�o-semestre y tipo funcionario seg�n corresponda.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>


<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int   a�o =  request.getParameter("anno")!=null &&
                            !request.getParameter("anno").equals("null") &&
                            !request.getParameter("anno").equals("NULL") &&
                            !request.getParameter("anno").equals("")  &&
                            !request.getParameter("anno").equals(" ")
                            ?Integer.parseInt(request.getParameter("anno")+""):0;
  int   semestre =  request.getParameter("semestre")!=null &&
                          !request.getParameter("semestre").equals("null") &&
                          !request.getParameter("semestre").equals("NULL") &&
                          !request.getParameter("semestre").equals("")  &&
                          !request.getParameter("semestre").equals(" ")
                          ?Integer.parseInt(request.getParameter("semestre")+""):0;
  int   tipo_usuario =  request.getParameter("tipo_usuario")!=null &&
                        !request.getParameter("tipo_usuario").equals("null") &&
                        !request.getParameter("tipo_usuario").equals("NULL") &&
                        !request.getParameter("tipo_usuario").equals("")  &&
                        !request.getParameter("tipo_usuario").equals(" ")
                        ?Integer.parseInt(request.getParameter("tipo_usuario")+""):0;
  if (a�o == 0 && semestre == 0){
  String periodo    = request.getParameter("periodo")!=null &&
             !request.getParameter("periodo").equals("null") &&
             !request.getParameter("periodo").equals("NULL") &&
             !request.getParameter("periodo").equals("")  &&
             !request.getParameter("periodo").equals(" ")?
                   request.getParameter("periodo")+"":"";
  if (!periodo.trim().equals("") && !periodo.trim().equals("0")){
 a�o = Integer.parseInt(periodo.substring(0,4));
 semestre = Integer.parseInt(periodo.substring(4,periodo.length()));
  }
  }
   java.util.Vector vec = new java.util.Vector();
   String titulo = "";
  switch (tipo_usuario){
    case 1 : titulo = "Alumnos " + a�o + "-" + semestre;
      break;
    case 2 : titulo = "Alumnos Tutor�a " + a�o + "-" + semestre;
      break;
    case 3 : titulo = "Alumnos Rama Deportiva " + a�o + "-" + semestre;
      break;
    case 4 : titulo = "Funcionarios " + a�o + "-" + semestre;
      break;

  }
  if (a�o > 0 && semestre > 0 && tipo_usuario > 0) {
    general.getAbrirConexion();
    general.setGenerarConexionRegistro();
    vec = general.getListadoRegistro(a�o, semestre, tipo_usuario);
    general.getCerrarConexion();
  }

java.util.Calendar f = new java.util.GregorianCalendar();
int dia = f.get(java.util.Calendar.DAY_OF_MONTH);
int mes = f.get(java.util.Calendar.MONTH) + 1;
int anno = f.get(java.util.Calendar.YEAR);
int hora = f.get(java.util.Calendar.HOUR_OF_DAY);
int minuto = f.get(java.util.Calendar.MINUTE);
String  dn       = "AM";
if (hora>12){
  dn    = "PM";
  hora = hora - 12;
}
  if (hora == 0) hora = 12;
%>
<html>
<head>
<title>Gimnasio - Lista de Registrados</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #000000}
-->
</style>
</head>
<body>

<table width="100%"  border="0" cellspacing="0" cellpadding="1">
<%if (vec != null && vec.size()>0){%>
  <tr>
    <td height="20" ><table width="100%"  border="0">
      <tr>
        <td width="20%" height="50" class="letra8" >Gimnasio III<br>
          USM </td>
        <td width="65%" class="letra10" align="center"><%=titulo%><span class="letra8"><br>
          </span></td>
        <td width="15%" >&nbsp;
          <div align="right"><span class="letra8"><%=dia%>/<%=mes%>/<%=anno%><br>
            <%=hora%>:<%=minuto%> <%=dn%></span></div></td>
      </tr>
    </table>      </td>
  </tr>
  <tr>
    <td width="100%" height="20" align="center" valign="top" class="letra8">&nbsp;</td>
  </tr>
  <tr>
    <td height="20" align="center" valign="top" class="letra10">&nbsp;</td>
  </tr>
</table>
<table width="100%" height="49%"  border="1" cellpadding="2" cellspacing="1">
  <tr bgcolor="#FFFFFF">
    <td class="Estilo_GrisBold">N&deg;</td>
    <td class="Estilo_GrisBold">Paterno</td>
    <td class="Estilo_GrisBold">Materno</td>
    <td class="Estilo_GrisBold">Nombres</td>
    <td  class="Estilo_GrisBold">RUT</td>
    <%if (tipo_usuario == 3){%>
    <td class="Estilo_GrisBold">Rama Deportiva</td>
    <td class="Estilo_GrisBold">Profesor a Cargo</td>
    <%}%>
     <%if (tipo_usuario == 4){%>
     <td class="Estilo_GrisBold">Departamento</td>
    <%}%>
      <%if (tipo_usuario == 2){%>
     <td class="Estilo_GrisBold">Horario</td>
    <%}%>
     <td class="Estilo_GrisBold">Fecha Inscr.</td>
  </tr>
  <%
  for (int i=0;vec != null && i < vec.size(); i++){
    %>
  <tr bgcolor="#FFFFFF">
    <td class="Estilo_Azul"><%=i+1%></td>
    <td class="Estilo_Azul"><%=((Vector) vec.get(i)).get(0)+ ""%></td>
    <td class="Estilo_Azul"><%=((((Vector) vec.get(i)).get(1)+ "").trim().equals(""))?"&nbsp;":((Vector) vec.get(i)).get(1)+ ""%></td>
    <td class="Estilo_Azul"><%=((Vector) vec.get(i)).get(2)+ ""%></td>
    <td width="14%" class="Estilo_Azul" nowrap="nowrap"><%=((Vector) vec.get(i)).get(3)+ ""%></td>
    <%if (tipo_usuario == 3 ){%>
    <td  class="Estilo_Azul"><%=((Vector) vec.get(i)).get(4)%></td>
    <td   class="Estilo_Azul"><%=((Vector) vec.get(i)).get(5)%></td>
    <%}%>
    <%if (tipo_usuario == 4){%>
     <td  class="Estilo_Azul"><%=((Vector) vec.get(i)).get(4)+ ""%></td>
    <%}%>
    <%if (tipo_usuario == 2){%>
     <td  class="Estilo_Azul"><%=((Vector) vec.get(i)).get(4)+ ""%></td>
    <%}%>
    <td  class="Estilo_Azul"><%=((Vector) vec.get(i)).get(6)+ ""%></td>
  </tr>
   <%}%>
   <%} else { if (a�o>0 && semestre>0 && tipo_usuario> 0){%>
  <tr bgcolor="#FFFFFF">
    <td height="16%" colspan="5" valign="top" class="Estilo_Azul"><div align="center"><br>
      <br>
      <span class="Estilo1">No se encontraron Registros</span> </div></td>
  </tr>
 <%}}%>
</table>
<%if (vec != null && vec.size()>0){%>
<div align="center"><font class="barra" color="#000000"><br>
  <br>
  &copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a</font>
</div><%}%>
</body>
</html>
