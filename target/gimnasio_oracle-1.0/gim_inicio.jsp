<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 04/06/2007
   // �ltima Actualizaci�n : 06/06/2007 incluir los include
%>
<jsp:useBean id="mant" class="gimnasio.BeanUsuarios"></jsp:useBean>
<jsp:useBean id="gen" class="gimnasio.BeanGeneral"></jsp:useBean>
<%
response.setHeader("Pragma","no-cache");
response.setDateHeader ("Expires", 0);
response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");

int usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
int perfil = session.getAttribute("cod_perfil") != null?
                  Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
if(usuario==0) response.sendRedirect("index.jsp");
String nom_usuario = "";
Vector v = new Vector();
String nom_perfil = "";
String pag_inicio = "";
switch (perfil){
  case 2 : { nom_perfil = "Administrador";
             pag_inicio = "gim_bitacora_adm.jsp";
    break;
  }
  case 28 : {nom_perfil = "Recepcionista";
             pag_inicio = "gim_bitacora_adm.jsp";
    break;
  }
  case 19 : {nom_perfil = "Funcionario";
             pag_inicio = "gim_ficha.jsp";
   break;
  }
 case 8 : {nom_perfil = "Alumno";
           pag_inicio = "gim_ficha.jsp";
 break;
 }

}

if (usuario>0){
  mant.getAbrirConexion();
  mant.setGenerarConexionUsuario();
  v = mant.getFuncionario(usuario);
  mant.getCerrarConexion();
  if (v.size()>0) nom_usuario = v.get(2)+" "+v.get(3)+" "+v.get(4); // funcionario cc
  else { // puede ser alumno CC
    gen.getAbrirConexion();
    gen.setGenerarConexionDatosAl();
    v = gen.getAlumno(usuario);
    gen.getCerrarConexion();
    /*if (v.size() == 0){ // puede ser alumno Vi�a
      gen.getAbrirConexionVi�a();
      gen.setGenerarConexionDatosVi�a();
      v = gen.getAlumnoVi�a(usuario);
      if (v.size() == 0){ // puede ser funcionario Vi�a
        v = gen.getFuncionarioVi�a(usuario);
        if (v.size() > 0) nom_usuario = v.get(0)+" "+v.get(1)+" "+v.get(2); // funcionario Vi�a
      }
      gen.getCerrarConexionVi�a();
    }*/
    if (v.size() > 0 && nom_usuario.length() == 0)
      nom_usuario = v.get(1)+"";

  }

}

%>
<html>
<head>
<title>Gimnasio III</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<script src="js/valida_login.js"></script>
<script src="js/fecha.js"></script>
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function Mensaje(){
  alert ('Opci�n temporalmente deshabilitada.');
  return false;
}
</script>

<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
body {
	background-color: #E7E7E7;
}
-->
</style>

</head>
<body onload="fecha();hora();">
<table width="800"  height="570" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#001930">
  <tr>
    <td valign="middle">
    <table width="800" height="145" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
      <tr>
        <td height="145" align="center" background="imagen/gim_logo_borde_i.gif">&nbsp;</td>
        <td width="788" height="145" align="center" valign="top"><img name="logo" src="imagen/gim_logo.gif" width="788" height="145" border="0" alt=""></td>
        <td height="145" align="center" background="imagen/gim_logo_borde.gif">&nbsp;</td>
      </tr>
     </table>
     <table width="800" height="21" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
      <tr>
        <td height="20">&nbsp;<a href="http://www.usm.cl" target="new_win">
        <img src="imagen/gim_link_usm.gif" alt="" name="usm" width="131" height="7" border="0"></a>
        </td>
        <td width="540" height="20" valign="bottom"><table width="100%"  border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td align="right"><span class="Estilo_AzulinoClaro letra8"><%=nom_usuario%></span> <span class="Estilo_Gris letra8">|</span> <span class="Estilo_NaranjoOsc letra8"><%=nom_perfil%></span>&nbsp;&nbsp;<%if (usuario>0){%><a href="index.jsp" target="_parent"><img src="imagen/gim_finsesion.gif" alt="SALIR DEL SISTEMA" width="10" height="9" border="0"><span class="Estilo_AzulinoClaro letra8"> Cerrar</span></a><%}%>&nbsp;</td>
  </tr>
</table></td>
       </tr>
      <tr>
        <td height="1" colspan="2" align="center"><img name="linea" src="imagen/gim_linea_azul.gif" width="100%" height="1" border="0" alt=""></td>
      </tr>
</table>

    <table width="800" height="370"  border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
      <tr valign="top">
        <td colspan="3" align="center">
           <table width="100%" height="370" border="0" align="center" cellpadding="0" cellspacing="3">
            <tr>
              <td width="18%" align="center" valign="top"><%@ include file="gim_menu.jsp"%></td>
              <td width="82%" valign="top">
<script language="JavaScript">
if (window.innerHeight){
   //navegadores basados en mozilla
   espacio_iframe = window.innerHeight - 250 //110
}else{
   if (document.body.clientHeight){
       //Navegadores basados en IExplorer, es que no tengo innerheight
       espacio_iframe = document.body.clientHeight - 250 //110
   }else{
       //otros navegadores
       espacio_iframe = 600 //478
   }
}
document.write ('<iframe frameborder="0" name="iframe" src="<%=pag_inicio%>" width="100%" height="' + espacio_iframe + '">')
document.write ('</iframe>')
</script>
              </td>
            </tr>
        </table></td>
      </tr>
    </table>
    <%@ include file="gim_pie_pagina.jsp" %></td>
  </tr>
</table>

