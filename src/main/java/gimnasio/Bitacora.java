package gimnasio;
import java.io.*;
import java.util.*;
import java.sql.*;

public class Bitacora {
  private Conexion_gim con;


public Bitacora(Conexion_gim con){
  this.con = con;
}
//**************************************************************************
// Metodo              : Permite capturar los tipos de contacto
// Programadora        : Monica B.
// Fecha creacion      : 3/09/2007
// Fecha modificacion  :
//****************************************************************************
  public Vector getTipoContacto (){
    java.util.Vector v = new java.util.Vector();
    java.util.Vector a = new java.util.Vector();
    String query = " SELECT  cod_contacto, nom_contacto" +
                   " FROM tipo_contacto" +
                   " ORDER BY nom_contacto";
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        a = new java.util.Vector();
        a.addElement(res.getString(1)); // cod_contacto
        a.addElement(res.getString(2)); // nom_contacto
        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Bitacora.getTipoContacto: "+e);
    }
    return v;
  }
  //**************************************************************************
// Metodo              : Permite capturar los tipos de prioridad
// Programadora        : Monica B.
// Fecha creacion      : 3/09/2007
// Fecha modificacion  :
//****************************************************************************
  public Vector getTipoPrioridad (){
    java.util.Vector v = new java.util.Vector();
    java.util.Vector a = new java.util.Vector();
    String query = " SELECT cod_prioridad, nom_prioridad" +
                   " FROM tipo_prioridad" +
                   " WHERE cod_prioridad > 0";
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        a = new java.util.Vector();
        a.addElement(res.getString(1)); // cod_prioridad
        a.addElement(res.getString(2)); // nom_prioridad
        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Bitacora.getTipoPrioridad: "+e);
    }
    return v;
  }

  //**************************************************************************
  // Metodo              : Permite capturar las noticias actuales
  // Programadora        : Marcela M.
  // Fecha creacion      : 08/05/2008
  // Fecha modificacion  :
  //****************************************************************************
  public Vector getNoticias (){
    java.util.Vector v = new java.util.Vector();
    java.util.Vector a = new java.util.Vector();
    String query = " SELECT p.cod_prioridad, p.nom_prioridad," +
                   " a.act_descripcion," +
                   " a.act_respuesta," +
                   " TO_CHAR(a.act_fecha_inicio,'dd/mm/yyyy'),"+
                   " TO_CHAR(a.act_fecha_inicio,'hh:mi:ss'),"+
                   " a.per_año, a.per_semestre, a.act_num_actividad" +
                   " FROM actividad a, tipo_prioridad p" +
                   " WHERE a.cod_prioridad = p.cod_prioridad" +
                   " AND a.cod_sistema = 47" +
                   " ORDER BY a.cod_prioridad, a.act_fecha_inicio DESC";
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        a = new java.util.Vector();
        a.addElement(res.getString(1)); // p.cod_prioridad
        a.addElement(res.getString(2)); // p.nom_prioridad
        a.addElement(res.getString(3)); // a.act_descripcion
        a.addElement(res.getString(4)); // a.act_respuesta
        a.addElement(res.getString(5)); // fecha
        a.addElement(res.getString(6)); // hora
        a.addElement(res.getString(7)); // a.per_año
        a.addElement(res.getString(8)); // a.per_semestre
        a.addElement(res.getString(9)); // a.act_num_actividad
        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Bitacora.getNoticias: "+e);
    }
    return v;
  }

//**************************************************************************
  // Metodo              : Permite capturar el �ltimo correlativo de las noticias actuales
  // Programadora        : Marcela M.
  // Fecha creacion      : 08/05/2008
  // Fecha modificacion  :
  //****************************************************************************
  public int getCorrelativoNoticias (){
    String query = " SELECT MAX(a.act_num_actividad)"+
                   " FROM actividad a" +
                   " WHERE a.cod_sistema = 47";
    int corr = 0;
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if(res.next()) corr = res.getInt(1);
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Bitacora.getCorrelativoNoticias: "+e);
    }
    return corr;
  }

  //**************************************************************************
  // Metodo              : Permite ingresar noticias
  // Programadora        : Marcela M.
  // Fecha creacion      : 08/05/2008
  // Fecha modificacion  :
  //****************************************************************************

  public int setNoticias
  (int accion, int prioridad , int correlativo, int año, int semestre,
  String titulo, String mensaje, int rut_usuario)  {

    String query = "";
    if (accion == 1) {
      query = " INSERT INTO actividad ( cod_actividad, sed_cod_sede,"+
              " per_año, per_semestre, cod_jornada, cod_sistema,"+
              " act_num_actividad, cod_vigencia, act_fecha_inicio,"+
              " act_descripcion, act_respuesta, act_fecha_termino,"+
              " fecha_modificacion, rut_usuario, cod_prioridad )" +
              " VALUES (1,1," + año + "," + semestre + ",1,47," +
              (getCorrelativoNoticias()+1) + ",1,SYSDATE,'" + titulo + "','" +
              mensaje + "',SYSDATE,SYSDATE," + rut_usuario + "," +
              prioridad + ")";
    }
    if (accion == 2) {
      query = " DELETE actividad" +
              " WHERE cod_actividad = 1" +
              " AND sed_cod_sede = 1" +
              " AND per_año = " + año +
              " AND per_semestre = " + semestre +
              " AND cod_jornada = 1" +
              " AND cod_sistema = 47" +
              " AND act_num_actividad = " + correlativo;
    }
// System.out.println(query);
    int error  = -1;
    try{
      Statement sent = con.conexion().createStatement();
      error  = sent.executeUpdate(query);
      if (error > 0) error = 0;
      sent.close();
    }
    catch (SQLException e){
      System.out.println("Error en insc_datos.setNoticias : " + e );
    }
    return error;
  }


  //**************************************************************************
// Metodo              : Permite capturar los mensajes de bit�coras
// Programadora        : Monica B.
// Fecha creacion      : 3/09/2007
// Fecha modificacion  : 2/11/2007 agregu� filtro del mes
//****************************************************************************
public Vector getMensaje (int año, int semestre, int mes){
  java.util.Vector v = new java.util.Vector();
  java.util.Vector a = new java.util.Vector();
  String fecha = "";
  String prioridad = "";
  String query = " SELECT b.bit_id, tc.nom_contacto, TO_CHAR(b.bit_fecha_origen,'dd/mm/yyyy'),"+
                 " TO_CHAR(b.bit_fecha_origen,'hh:mi:ss'), tp.nom_prioridad," +
                 " bit_mensaje, a.alu_nombres || ' ' || a.alu_ap_paterno, b.rut_usuario,"+
                 " b.cod_prioridad" +
                 " FROM bitacora b, tipo_contacto tc, tipo_prioridad tp, alumno a" +
                 " WHERE  bit_año =" + año +
                 " AND bit_semestre =" + semestre +
                 " AND b.cod_prioridad = tp.cod_prioridad" +
                 " AND b.cod_contacto = tc.cod_contacto"+
                 " AND b.rut_usuario = a.alu_rut" +
                 " AND TO_CHAR(b.bit_fecha_origen,'mm') = " + mes +
                 " ORDER BY b.bit_fecha_origen desc ";
  try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    while(res.next()){
      fecha = res.getString(3)+" "+res.getString(4);
      a = new java.util.Vector();
      a.addElement(res.getString(1)); // 0 id
      a.addElement(res.getString(2)); // 1 nom_contacto
      a.addElement(fecha);            // 2 fecha
      switch (res.getInt(9)){
        case 1: prioridad = "<span class='Estilo_Rojo'><strong>";
          break;
        case 2: prioridad = "<span class='Estilo_Naranjo'><strong>";
          break;
        case 3: prioridad = "<span class='Estilo_Azul'><strong>";
          break;
       default:
          prioridad = "<span class='Estilo_Gris'><strong>";
      }

      prioridad += res.getString(5) +"</strong></span>";
      a.addElement(prioridad); // 3 nom_prioridad
      a.addElement(res.getString(6)); // 4 mensaje
      a.addElement(res.getString(7)); // 5 nom_ usuario
      a.addElement(res.getString(8));    // 6 rut_usuario
      v.addElement(a);
    }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Bitacora.getMensaje: "+e);
  }
  return v;
  }
  //**************************************************************************
// Metodo              : Permite capturar las respuestas de los mensjes de bit�coras
// Programadora        : Monica B.
// Fecha creacion      : 4/09/2007
// Fecha modificacion  :  2/11/2007 agregu� filtro del mes
//****************************************************************************
public Vector getRespuestaMensaje (int año, int semestre, int mes){
java.util.Vector v = new java.util.Vector();
java.util.Vector a = new java.util.Vector();
String fecha = "";
String id = "";
String query = " SELECT b.bit_id, NVL(rb.resbit_id,0)," +
               " NVL(a.alu_nombres,' ')||' '|| NVL(a.alu_ap_paterno,' '),"+
               " NVL(rb.rut_usuario,0), " +
               " NVL(rb.resbit_respuesta,' ')," +
               " NVL(TO_CHAR(rb.resbit_fecha_origen,'dd/mm/yyyy'), ' ')," +
               " NVL(TO_CHAR(rb.resbit_fecha_origen,'hh:mi:ss'), ' ')" +
               " FROM bitacora b LEFT JOIN respuesta_bitacora rb ON" +
               " b.bit_id = rb.bit_id LEFT JOIN  alumno a ON" +
               " rb.rut_usuario = a.alu_rut" +
               " WHERE  bit_año = " + año +
               " AND bit_semestre = " + semestre +
               " AND TO_CHAR(b.bit_fecha_origen,'mm') = " + mes +
               " ORDER BY b.bit_fecha_origen desc, b.bit_id, rb.resbit_id";
// System.out.println("q"+query);
try{
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  while(res.next()){
    if (res.getInt(2) > 0){
    fecha = res.getString(6)+ " " + res.getString(7);

    } else fecha = "";
    a = new java.util.Vector();
    a.addElement(res.getString(1)); // 0 id
    a.addElement(res.getString(2)); // 1 id de la respuesta
    a.addElement(fecha);            // 2 fecha
    a.addElement(res.getString(3)); // 3 nombre del usuario
    a.addElement(res.getString(4)); // 4 rut_usuario de la respuesta
    a.addElement(res.getString(5)); // 5 respuesta
    v.addElement(a);
  }
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Bitacora.getRespuestaMensaje: "+e);
}
return v;
  }

//**************************************************************************
// Metodo              : Permite insertar bitacora
//
// Programadora        : Monica B.
// Fecha creacion      : 3/09/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setInsertarBitacora(Vector vec)throws SQLException{
   String query = "";

   String par = vec.get(0)+"," + // año
                vec.get(1)+","+ // semestre
                vec.get(2)+","+ // prioridad
                vec.get(3)+",'" + // tipo
                vec.get(4)+"',"+ // mensaje
                null+"," + //fecha hoy
                "1"+"," +// fecha y vigencia
                vec.get(6)+"";

       Connection con2 = con.conexion();
   
//query = "EXEC spu_ins_bitacora "+ par;
//System.out.println("query: "+ query);
           
   int error = -1;
   try{
	   CallableStatement cstmt = con2.prepareCall("{call spu_ins_bitacora(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
	   cstmt.setInt(1, Integer.parseInt((String) vec.get(0)));
	   cstmt.setInt(2, Integer.parseInt((String) vec.get(1)));
	   cstmt.setInt(3, Integer.parseInt((String) vec.get(2)));
	   cstmt.setInt(4, Integer.parseInt((String) vec.get(3)));
	   cstmt.setString(5,(String) vec.get(4));
       cstmt.setString(6,null);
	   cstmt.setInt(7, 1);
	   cstmt.setInt(8, Integer.parseInt((String) vec.get(6)));
       cstmt.registerOutParameter(9, Types.INTEGER);//PARAMETRO DE SALIDA
	   cstmt.executeQuery();
	   error = cstmt.getInt(9); // ES EL VALOR QUE RETORNA
	   cstmt.close();

       } catch (Exception e) {
          System.out.println("Error Bitacora.setInsertarBitacora: " +
			e.getMessage());
          //con2.rollback();
	   }
	return error;
  }
  //**************************************************************************
// Metodo              : Permite actualizar el mensaje de la bitacora
//
// Programadora        : Monica B.
// Fecha creacion      : 4/09/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setActualizarBitacora(Vector vec) throws SQLException{
   String query = "";
   String par = vec.get(0)+",'" + // id
                vec.get(1)+"',"+ // mensaje
                vec.get(2)+""; // rut_usuario


  // query = "EXEC spu_upd_bitacora "+ par;
//System.out.println("query: "+ query);
   
   Connection con2 = con.conexion();
   int error = -1;
   try{
	   CallableStatement cstmt = con2.prepareCall("{call spu_upd_bitacora(?, ?, ?, ?)}");
	   cstmt.setLong(1, Long.parseLong((String) vec.get(0)));
	   cstmt.setString(2,(String) vec.get(1));
	   cstmt.setInt(3, Integer.parseInt((String) vec.get(2)));
	   cstmt.registerOutParameter(4, Types.INTEGER);//PARAMETRO DE SALIDA
	   cstmt.executeQuery();
	   error = cstmt.getInt(4); // ES EL VALOR QUE RETORNA
	   cstmt.close();
       } catch (Exception e) {
          System.out.println("Error Bitacora.setActualizarBitacora: " +
			e.getMessage());
          //con2.rollback();
	   }
	return error;
  }
  //**************************************************************************
// Metodo              : Permite eliminar el mensaje de la bitacora
//
// Programadora        : Monica B.
// Fecha creacion      : 4/09/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setEliminarBitacora(String id){
   String query = "";

//query = "EXEC spu_dlt_bitacora "+  id;
//System.out.println("query: "+ query);
   
   Connection con2 = con.conexion();
   int error = -1;
   try{
	   CallableStatement cstmt = con2.prepareCall("{call spu_dlt_bitacora(?,?)}");
	   cstmt.setLong(1,Long.parseLong(id));
	   cstmt.registerOutParameter(2,Types.INTEGER);
	   cstmt.executeQuery();
	   error = cstmt.getInt(2);
	   cstmt.close();
   }
   catch(SQLException e){
     System.out.println("Error en Bitacora.setEliminarBitacora: "+e);
   }
   return error;
  }

  //**************************************************************************
// Metodo              : Permite eliminar la respuesta del mensaje de la bitacora
// Programadora        : Monica B.
// Fecha creacion      : 4/09/2007
// Fecha modificacion  :
//****************************************************************************
synchronized public int setEliminarRespuestaBitacora(String id, int id_resp){
 String query = "";

 //query = "EXEC spu_dlt_resp_bitacora "+  id +","+ id_resp;
//System.out.println("query: "+ query);
 Connection con2 = con.conexion();
 int error = -1;
 try{
	   CallableStatement cstmt = con2.prepareCall("{call spu_dlt_resp_bitacora(?,?,?)}");
	   cstmt.setLong(1,Long.parseLong(id));
	   cstmt.setInt(2, id_resp);
	   cstmt.registerOutParameter(3,Types.INTEGER);
	   cstmt.executeQuery();
	   error = cstmt.getInt(3);
	   cstmt.close();
 }
 catch(SQLException e){
   System.out.println("Error en Bitacora.setEliminarRespuestaBitacora: "+e);
 }
 return error;
  }
  //**************************************************************************
// Metodo              : Permite Insertar la respuesta del mensaje de la bitacora
// Programadora        : Monica B.
// Fecha creacion      : 4/09/2007
// Fecha modificacion  :
//****************************************************************************
   synchronized public int setInsertarRespuesta(Vector vec) throws SQLException{
    String query = "";
    String par = vec.get(0)+"," + // id
                 vec.get(1)+",'"+ // rut_usuario
                 vec.get(2)+"'"; // respuesta


   // query = "EXEC spu_ins_resp_bitacora "+ par;
//System.out.println("spu_ins_resp_bitacora: "+ query);
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_ins_resp_bitacora(?,?,?,?)}");
    	cstmt.setLong(1, Long.parseLong((String) vec.get(0)));
        cstmt.setInt(2, Integer.parseInt((String) vec.get(1)));
        cstmt.setString(3, (String) vec.get(2));
        cstmt.registerOutParameter(4, Types.INTEGER);
 	    cstmt.executeQuery();
 	    error = cstmt.getInt(4);
 	    cstmt.close();
    }
    catch(SQLException e){
    	 System.out.println("Error Bitacora.setInsertarRespuesta: " +
          e.getMessage());
    	 //con2.rollback();
    }
    return error;
  }
  //**************************************************************************
// Metodo              : Permite actualizar la respuesta del mensaje de la bitacora
// Programadora        : Monica B.
// Fecha creacion      : 10/09/2007
// Fecha modificacion  :
//****************************************************************************
 synchronized public int setActualizarRespuesta(Vector vec)throws SQLException {
  String query = "";
  String par = vec.get(0)+"," + // id_mensaje
               vec.get(1)+",'"+ // id-respuesta
               vec.get(2)+"',"+ // respuesta
               vec.get(3)+""; // rut_usuario


  //query = "EXEC spu_upd_resp_bitacora "+ par;
//System.out.println("spu_upd_resp_bitacora: "+ query);
  Connection con2 = con.conexion();
  int error = -1;
  try{
	  CallableStatement cstmt = con2.prepareCall("{call spu_upd_resp_bitacora(?,?,?,?,?)}");
	  cstmt.setLong(1, Long.parseLong((String) vec.get(0)));
      cstmt.setInt(2, Integer.parseInt((String) vec.get(1)));
      cstmt.setString(3, (String) vec.get(2));
      cstmt.setInt(4, Integer.parseInt((String) vec.get(3)));
      cstmt.registerOutParameter(5, Types.INTEGER);
	  cstmt.executeQuery();
	  error = cstmt.getInt(5);
	  cstmt.close();
  }
  catch(SQLException e){
	  System.out.println("Error en Bitacora.setActualizarRespuesta: " + e.getMessage());
	  // con2.rollback();
  }
  return error;
  }
}
