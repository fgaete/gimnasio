package gimnasio;
import java.sql.*;
import java.util.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author Monica Barrera
 * fecha : 2 junio de 2007
 * Modificacion : 11 de junio de 2007
 * @version 1.0
 */

public class BeanGeneral {
  private Datos_alumno      da = null;
  private Datos_vina        dav = null;
  private Conexion_gim      con = null;
  // private Conexion_vina   con_v = null;
  private Horario             h = null;
  private Registro           re = null;
  private Reserva             r = null;
  private Bitacora            b = null;


  public  BeanGeneral(){

  }
  public void getAbrirConexion(){
    con = new Conexion_gim();
  }
  public void getCerrarConexion(){
    con.close();
  }
  /*public void getAbrirConexionViña(){
    con_v = new Conexion_vina();
  }
  public void getCerrarConexionViña(){
    con_v.close();
  }*/
  public void setGenerarConexionDatosAl(){
    da = new Datos_alumno(con);
  }
  /*
  public void setGenerarConexionDatosViña(){
    dav = new Datos_vina(con);
  }*/
  public void setGenerarConexionHorario(){
    h = new Horario(con);
  }
  public void setGenerarConexionRegistro(){
    re = new Registro(con);
  }
  public void setGenerarConexionReserva(){
    r = new Reserva(con);
  }
  public void setGenerarConexionBitacora(){
    b = new Bitacora(con);
  }


  // m�todos Datos _alumno
  public Vector getAlumno (int rut_recep){
    Vector v = new Vector();
    v = da.getAlumno(rut_recep);
    return v;
  }
  public Vector getCalidadesAlu (int rut_alumno){
    Vector v = new Vector();
    v = da.getCalidadesAlu(rut_alumno);
    return v;
  }
  public Vector getNombreAlumno (int rut_usuario){
    Vector v = new Vector();
    v = da.getNombreAlumno(rut_usuario);
    return v;
  }
  public boolean getHabilitado (int cod_calidad){
    boolean habilitado;
    habilitado = da.getHabilitado(cod_calidad);
    return habilitado;
  }
  public java.util.Vector getAlumnoRol(int rut, int sede){
    Vector v = new Vector();
    v = da.getAlumnoRol(rut, sede);
    return v;
  }
  public String getIngresoAlumno (int rut_usuario){
    String fecha = "";
    fecha = da.getIngresoAlumno(rut_usuario);
    return fecha;
  }
  public boolean getHabilitadoReserva (int rut, int año, int semestre){
    boolean habilitado = false;
    habilitado = da.getHabilitadoReserva(rut, año, semestre);
    return habilitado;

  }
  public void getEstadoAsistencia (String id_reg, String id_per){
    da.getEstadoAsistencia(id_reg, id_per);
  }

  public int getEstadoAsistenciaCod (){
    return da.getEstadoAsistenciaCod();
  }

  public String getEstadoAsistenciaNom (){
    return da.getEstadoAsistenciaNom();
  }
  public Vector getDatosAlumno ( String reggim_id){
    Vector v = da.getDatosAlumno(reggim_id);
    return v;
  }
  // M�todos de Horario

  public Vector getActividad (){
    Vector v = new Vector();
    v = h.getActividad();
    return v;
  }
  public Vector getMantActividad (int año, int semestre){
    Vector v = new Vector();
    v = h.getMantActividad(año, semestre);
    return v;
  }
  public Vector getHoraBloque (int num_sala,
                               int año,
                               int semestre){
    Vector v = new Vector();
    v = h.getHoraBloque(num_sala, año, semestre);
    return v;
  }
 public int getMaxSecuencia ( int sala, int año, int semestre){
   int secuencia = h.getMaxSecuencia(sala, año, semestre);
   return secuencia;
 }
 public int getSecuenciaBloque ( int sala, int año, int semestre, int cod_bloque){
   int secuencia = h.getSecuenciaBloque(sala, año, semestre, cod_bloque);
   return secuencia;
 }
 public Vector getSecuenciaBloque ( int sala, int año, int semestre, int cod_bloque1, int cod_bloque2){
	 Vector vec_secuencia  = h.getSecuenciaBloque(sala, año, semestre, cod_bloque1, cod_bloque2);
	   return vec_secuencia;
 }

   public Vector getSecuencia ( int sala, String id){
     Vector v = h.getSecuencia(sala, id);
     return v;
   }
  public Vector getHorarioSala (int año, int semestre, int num_sala){
    Vector v = new Vector();
    v = h.getHorarioSala (año, semestre, num_sala);
    return v;
  }
  public Vector getHorarioAseo (int num_sala, int año, int semestre){
    Vector v = new Vector();
    v = h.getHorarioAseo ( num_sala, año, semestre);
    return v;
  }

  public Vector getBloqueCodigoSede (int cod_sede, int cod_jornada,
                                     int cod_bloque){
    Vector v = new Vector();
    v = h.getBloqueCodigoSede (cod_sede, cod_jornada, cod_bloque);
    return v;

  }
  public int getBloqueSala (String bloque, int num_sala){
    int cod_bloque = 0;
    cod_bloque = h.getBloqueSala(bloque, num_sala);
    return cod_bloque;
  }
  public Vector getParalelo (int año, int semestre){
    Vector v = new Vector();
    v = h.getParalelo(año, semestre);
    return v;
  }
  public Vector getHorario ( int año, int semestre, int num_sala){
    Vector v = new Vector();
    v = h.getHorario ( año,  semestre, num_sala);
    return v;
  }
  public Vector getProgramacion (int año, int semestre){
    Vector v = new Vector();
    v = h.getProgramacion(año,semestre);
    return v;
  }
  public Vector getProgramacionPeriodo (){
    Vector v = new Vector();
    v = h.getProgramacionPeriodo();
    return v;
  }
  public Vector getProfesorGimnasio (){
    Vector v = new Vector();
    v = h.getProfesorGimnasio();
    return v;
  }
  public Vector getHorarioBloque ( int año, int semestre, int num_sala, int dia,
                                   int bloque1, int bloque2){
    Vector v = new Vector();
    v = h.getHorarioBloque(año, semestre, num_sala, dia, bloque1, bloque2);
    return v;
  }
  public Vector getHorarioDia (int año, int semestre, int num_sala, int dia, String fecha){
    Vector v = new Vector();
    v = h.getHorarioDia (año, semestre, num_sala, dia, fecha);
    return v;
  }

  public int getFechaInicioPeriodo(){
    int fecha_inicio_per = h.getFechaInicioPeriodo();
    return fecha_inicio_per;
  }
  public int getFechaTerminoPeriodo(){
    int fecha_termino_per = h.getFechaTerminoPeriodo();
    return fecha_termino_per;
  }

  public void getPeriodoSemestre
     (int año, int semestre){
  h.getPeriodoSemestre(año, semestre);
}
  public void getPeriodoSemestreActual
   (int cod_jornada, int cod_sede){
    h.getPeriodoSemestreActual(cod_jornada, cod_sede);
  }

  public int getAñoActual(){
    int año = h.getAñoActual();
      return año;
  }
   public int getSemestreActual(){
       int sem = h.getSemestreActual();
       return sem;
   }
   public int getFechaInicioActual(){
     int fecha_inicio = h.getFechaInicioActual();
       return fecha_inicio;
   }
   public int getFechaTerminoActual(){
     int fecha_termino = h.getFechaTerminoActual();
       return fecha_termino;
   }
   public Vector getListaCurso (int año, int semestre, int cod_asign, int paralelo){
     Vector vec = h.getListaCurso(año, semestre, cod_asign, paralelo);
     return vec;
   }
   public Vector getInscritoDocencia (int año, int semestre, int cod_asign, int rut){
     Vector vec = h.getInscritoDocencia(año, semestre, cod_asign, rut);
     return vec;
   }
  synchronized public int setInsertarActividad(String nom_asign,
      int rut_usuario){
    int err = h.setInsertarActividad(nom_asign, rut_usuario);
    return err;
  }

  synchronized public int setActualizarActividad( String nom_asign,
      int rut_usuario,
      int cod_asign){
    int err = h.setActualizarActividad(nom_asign, rut_usuario,cod_asign);
    return err;
  }

  synchronized public int setEliminaActividad( int cod_asign){
    int err = h.setEliminaActividad(cod_asign);
    return err;
  }
  synchronized public int setInsertarHoraBloque(int sede,
      int cod_jornada,
      String inicio,
      String termino,
      int rut_usuario,
      int num_sala,
      int secuencia,
      int año,
      int semestre){
    int err = h.setInsertarHoraBloque(sede, cod_jornada, inicio, termino,
                                      rut_usuario, num_sala, secuencia, año, semestre);
    return err;
  }
  synchronized public int setEliminarHoraBloque(int rut_usuario, String id){
    int err = h.setEliminarHoraBloque(rut_usuario, id);
    return err;
  }

  synchronized public int setAgregarAseo(int sede,
      int año,
      int semestre,
      int jornada,
      int num_sala,
      int bloque_inicio,
      int bloque_termino,
      String fecha_inicio,
      String fecha_termino,
      int rut_usuario,
      int dia_inicio,
      int dia_termino,
      String id_aseo){
    int err = h.setAgregarAseo(sede,
                               año,
                               semestre,
                               jornada,
                               num_sala,
                               bloque_inicio,
                               bloque_termino,
                               fecha_inicio,
                               fecha_termino,
                               rut_usuario,
                               dia_inicio,
                               dia_termino,
                               id_aseo);
    return err;

  }
  synchronized public int setAgregarHorario(int sede,
      int año,
      int semestre,
      int num_sala,
      int bloque_inicio,
      int bloque_termino,
      int dia_inicio,
      int dia_termino,
      int cod_reserva,
      int paralelo,
      int cod_asign,
      int jornada,
      int rut_usuario){
    int err = h.setAgregarHorario(sede,
                                  año,
                                  semestre,
                                  num_sala,
                                  bloque_inicio,
                                  bloque_termino,
                                  dia_inicio,
                                  dia_termino,
                                  cod_reserva,
                                  paralelo,
                                  cod_asign,
                                  jornada,
                                  rut_usuario);
    return err;
  }
  synchronized public int  setEliminaHorario (int sede,
      int año,
      int semestre,
      int num_sala,
      int bloque_inicio,
      int bloque_termino,
      int dia_inicio,
      int rut_usuario){
    int err = h.setEliminaHorario(sede, año, semestre, num_sala, bloque_inicio,
                                  bloque_termino,dia_inicio, rut_usuario);
    return err;
  }
  synchronized public int setEliminaAseo(String id_aseo){
    int err = h.setEliminaAseo(id_aseo);
    return err;
  }
  synchronized public int setModificaHorario(int sede,
      int año,
      int semestre,
      int num_sala,
      int bloque_inicio,
      int bloque_termino,
      int dia_inicio,
      int dia_termino,
      int cod_reserva,
      int paralelo,
      int cod_asign,
      int jornada,
      int bl_inicio_or,
      int bl_termino_or,
      int d_inicio_or,
      int d_termino_or,
      int rut_usuario){
    int err = h.setModificaHorario(sede, año, semestre, num_sala, bloque_inicio,
                                   bloque_termino, dia_inicio, dia_termino,
                                   cod_reserva, paralelo, cod_asign, jornada,
                                   bl_inicio_or, bl_termino_or, d_inicio_or,
                                   d_termino_or, rut_usuario);
    return err;
  }
  synchronized public int setAgregaProgramacion(int año, int semestre,
      int cod_asign, int paralelo,
      int fun_rut, int rut_usuario,
      int cupo,
      int rama){
    int err = h.setAgregaProgramacion(año, semestre, cod_asign, paralelo,fun_rut,
                                      rut_usuario, cupo, rama);
    return err;

  }
  synchronized public int setEliminaProgramacion(int año, int semestre,
      int cod_asign, int paralelo){
    int err = h.setEliminaProgramacion(año, semestre,cod_asign, paralelo);
    return err;
  }
  synchronized public int setCopiarHoraBloque(  int rut_usuario,
                                              int num_sala,
                                              int año,
                                                int semestre){
    int err = h.setCopiarHoraBloque(rut_usuario,num_sala,año, semestre);
    return err;
  }
  synchronized public int setEliminarHoraBloqueSemestral(int año,
                                                       int semestre,
                                                       int sala){
    int err = h.setEliminarHoraBloqueSemestral(año, semestre, sala);
    return err;
  }
  synchronized public int setModificarHoraBloque(int rut_usuario,
                                              String id,
                                               int secuencia){
    int err = h.setModificarHoraBloque(rut_usuario,id,secuencia);
    return err;
  }
// m�todos de Registro

  public Vector getProfesores (){
    Vector v = new Vector();
    v = re.getProfesores();
    return v;
  }
  public Vector getAsigRama (){
    Vector v = new Vector();
    v = re.getAsigRama();
    return v;
  }
  public Vector getRamaDeportiva (){
    Vector v = new Vector();
    v = re.getRamaDeportiva();
    return v;
  }

  public Vector getAlumnoRama (int rut){
    Vector v = new Vector();
    v = re.getAlumnoRama(rut);
    return v;
  }
  public int getActivacion (){
    int activacion = 0;
    activacion = re.getActivacion();
    return activacion;
}
  public Vector getRegistroUsuario (int rut){
    Vector v = new Vector();
    v = re.getRegistroUsuario(rut);
    return v;
  }

  public Vector getListadoRegistro (int año, int semestre, int tipo_usuario){
    Vector v = new Vector();
    v = re.getListadoRegistro (año, semestre, tipo_usuario);
    return v;
  }
  public Vector getListadoInscripciones (int año,
                                          int semestre,
                                          int bloque_inicio,
                                          int bloque_fin,
                                          String fecha,
                                          int num_dia,
                                          int sala){
    Vector vec = new Vector();
    vec = re.getListadoInscripciones(año, semestre, bloque_inicio, bloque_fin,
                                     fecha, num_dia, sala);
    return vec;
  }
  synchronized public int setEliminarRamaDeportiva(int cod_asign ,
      int rut_usuario){
    int err = re.setEliminarRamaDeportiva(cod_asign , rut_usuario);
    return err;
  }

  synchronized public int setInsertaRamaDeportiva(int cod_asign ,
      int fun_rut,
      int rut_usuario){
    int err = re.setInsertaRamaDeportiva(cod_asign , fun_rut, rut_usuario);
    return err;
  }

  synchronized public int setActualizaRamaDeportiva(int cod_asign ,
      int fun_rut,
      int rut_usuario){
    int err = re.setActualizaRamaDeportiva(cod_asign , fun_rut, rut_usuario);
    return err;
  }

synchronized public int setActivacion(String tipo, int activo){
  int err = re.setActivacion(tipo, activo);
  return err;
}

  synchronized public int setEliminarRegistro(String id,
      int rut_usuario){
    int err = re.setEliminarRegistro
    (id, rut_usuario);
    return err;

  }
  synchronized public int setInsertaRegistro(int rut ,
                                             int tipo,
                                             int rut_usuario,
                                             String rama_id,
                                             Vector vec_reserva,
                                             int sexo){
    int err = re.setInsertaRegistro(rut , tipo, rut_usuario, rama_id, vec_reserva, sexo);
    return err;
  }

  synchronized public int setActualizarRegistro(int rut ,
      int tipo,
      int rut_usuario,
      String rama_id,
      String id,
      Vector vec_reserva){
    int err = re.setActualizarRegistro(rut , tipo, rut_usuario, rama_id, id, vec_reserva);
    return err;
  }
  public Vector getReservaTutoria (String id_reg, String id_per){
    Vector v = new Vector();
    v = r.getReservaTutoria (id_reg, id_per);
    return v;
  }
// m�todos de Reserva
  public Vector getPeriodoGimnasio (int año, int semestre, int sala){
    Vector v = new Vector();
    v = r.getPeriodoGimnasio(año, semestre, sala);
    return v;
  }
 public Vector getPeriodoGimnasioSala(int sala, String fecha){
   Vector v = new Vector();
   v = r.getPeriodoGimnasioSala( sala, fecha);
   return v;
 }
  public Vector getPremiadosPeriodoList (int año, int semestre, String periodo, int cod_estado){
    Vector v = new Vector();
    v = r.getPremiadosPeriodoList(año, semestre, periodo, cod_estado);
    return v;
  }
  public Vector getPeriodoGimnasioVigente(int sala){
    Vector v = new Vector();
    v = r.getPeriodoGimnasioVigente (sala);
    return v;
  }
  public Vector getBloqueReserva (int rut, String periodo, int fecha_inicio, int fecha_termino){
    Vector v = new Vector();
    v = r.getBloqueReserva (rut, periodo, fecha_inicio, fecha_termino);
    return v;
  }
  public Vector getAsistenciaGimnasio(int rut, String fec_inicio, String fec_termino,
                                      String pergim_id){
    Vector v = new Vector();
    v = r.getAsistenciaGimnasio (rut, fec_inicio, fec_termino, pergim_id);
    return v;

  }
  public Vector getAsistenciaGimnasio2(int rut, String fec_inicio, String fec_termino,
          String pergim_id){
	Vector v = new Vector();
	v = r.getAsistenciaGimnasio2 (rut, fec_inicio, fec_termino, pergim_id);
	return v;
	
  }
  public Vector getPeriodoAnulado (String periodo){
    Vector v = new Vector();
     v = r.getPeriodoAnulado (periodo);
    return v;
  }
  public int getCantidadReservados (int año, int semestre, int cod_asign ,
                                    int paralelo, int dia,
                                    int sala, int blo1, int blo2,
                                    int inicio, int termino){
    int cantidad = 0;
    cantidad = r.getCantidadReservados(año, semestre, cod_asign, paralelo,
                                       dia, sala, blo1, blo2, inicio, termino);
    return cantidad;
  }
  public Vector getFechasReservados (int año, int semestre, int cod_asign ,
                                     int paralelo, int dia,
                                     int sala, int blo1, int blo2,
                                     int fecha_inicio, int fecha_termino){
    Vector v = new Vector();
    v = r.getFechasReservados(año, semestre, cod_asign, paralelo, dia, sala,
                              blo1, blo2, fecha_inicio, fecha_termino);
    return v;
  }
  public int getContInscritosGimnasio (int año, int semestre, int dia, String fecha,
                                       String periodo, int bloque1, int bloque2,
                                       int sala){
    int cantidad = 0;
    cantidad = r.getContInscritosGimnasio(año, semestre, dia, fecha, periodo,
        bloque1, bloque2, sala);
    return cantidad;

  }
  public int getContInscritosRamaGimnasio (int año, int semestre, int dia, String fecha,
      String periodo, int bloque1, int bloque2, int sala){

    int cantidad = 0;
    cantidad = r.getContInscritosRamaGimnasio(año, semestre, dia, fecha, periodo,
        bloque1, bloque2, sala);
    return cantidad;

  }
  public int getCupoGimnasio (int año, int semestre, int dia, int bloque1,
                              int bloque2, int sala, int codAsign){
    int cantidad = 0;
    cantidad = r.getCupoGimnasio(año, semestre, dia, bloque1, bloque2, sala, codAsign);
    return cantidad;

  }
  public int getSobreCupoGimnasio (int año, int semestre, int dia, int bloque1,
                                   int bloque2, int sala){
    int cantidad = 0;
    cantidad = r.getSobreCupoGimnasio(año, semestre, dia, bloque1, bloque2, sala);
    return cantidad;


  }
  public int getContAsistenciaGimnasio (int año, int semestre, int dia,
                                        String periodo,
                                        int bloque1, int bloque2,
                                        int cod_registro, String fecha,
                                        int sala){
    int cantidad = 0;
    cantidad = r.getContAsistenciaGimnasio(año, semestre, dia, periodo, bloque1,
        bloque2, cod_registro, fecha, sala);
    return cantidad;
  }
  public boolean getAseoBloque (     String horblo_id,
                                     int dia,
                                   String fecha){
    boolean aseo = false;
    aseo = r.getAseoBloque ( horblo_id,
                                   dia,
                                   fecha);
    return aseo;
  }
  public int getContInscritosEstadistica (int año,
                                         int semestre,
                                         int cod_asign,
                                         int paralelo,
                                         int jornada,
                                         int sede,
                                         int num_dia,
                                         String fecha,
                                         int bloque1,
                                         int bloque2,
                                         int sala){
    int cantidad = 0;
    cantidad = r.getContInscritosEstadistica(año, semestre, cod_asign, paralelo,
                                          jornada, sede, num_dia, fecha ,
                                          bloque1, bloque2, sala);
    return cantidad;
  }
  public Vector getPorcentajeUsoDetalle (int año, int semestre, String fecha_inicio,
                                   String fecha_termino, int sala, int version){
    Vector v = r.getPorcentajeUsoDetalle ( año, semestre, fecha_inicio,
                                   fecha_termino, sala, version);
    return v;
  }
  public Vector getCodigosInscrEstadistica (int año, int semestre, String fecha, int sala){
    Vector v = new Vector();
    v = r.getCodigosInscrEstadistica(año, semestre, fecha, sala);
    return v;
  }
  public Vector getPorcentajeUso (int año, int semestre, String fecha_inicio,
                                   String fecha_termino, int sala, int detalle,
                                   int reg_usuario){
    Vector v = new Vector();
    v = r.getPorcentajeUso(año, semestre, fecha_inicio, fecha_termino, sala, detalle, reg_usuario);
    return v;
  }
  public Vector getInscritosGimnasio (int año, int semestre, int dia, String fecha,
                                      String periodo, int bloque1, int bloque2,
                                      int sala){
    Vector v = new Vector();
    v = r.getInscritosGimnasio(año, semestre, dia, fecha, periodo, bloque1, bloque2,
                               sala);
    return v;
  }
  public int getContInscritosAsistentesGimnasio (int año, int semestre, int dia, String fecha,
                                                 String periodo, int bloque1, int bloque2,
                                                 int sala){

    int cont = r.getContInscritosAsistentesGimnasio(año, semestre, dia, fecha,
                                                    periodo, bloque1, bloque2,
                                                    sala);
    return cont;
  }

  public Vector getAsistenciaRegistro (int año, int semestre, String fecha,
                                       String periodo, int dia, int bloque1,
                                       int bloque2, String reggim_id, int sala){
    Vector v = new Vector();
    v = r.getAsistenciaRegistro(año, semestre, fecha, periodo, dia, bloque1,
                                bloque2,reggim_id, sala );
    return v;
  }
  public Vector getEstadoClaseGimnasio (String fecha, int cod_asign, int año,
                                      int semestre, int paralelo, int jornada,
                                      int sede, int dia, int blo1, int blo2){
    Vector v = new Vector();
    v = r.getEstadoClaseGimnasio(fecha, cod_asign, año, semestre, paralelo,
                                jornada, sede, dia, blo1, blo2);
    return v;
  }

  public int getEstadoClaseGimnasioSemana (String fecha_inicio, String fecha_termino, int cod_asign, int año,
                                      int semestre, int paralelo, int jornada,
                                      int sede, int dia, int blo1, int blo2){
    int estado = 0;
    estado = r.getEstadoClaseGimnasioSemana (fecha_inicio, fecha_termino, cod_asign, año, semestre, paralelo,
                                jornada, sede, dia, blo1, blo2);
    return estado;
  }


  public boolean getRegistroPremiadoSancionado (String periodo, String reggim_id,
      int cond){
  boolean sancionado = false;
  sancionado = r.getRegistroPremiadoSancionado( periodo, reggim_id, cond);
  return sancionado;
  }

  public Vector getTipoReserva (){
    Vector vec_tipo = r.getTipoReserva();
    return vec_tipo;
  }
  public int getReservaTipo (int año, int semestre, int cod_asign, int paralelo,
                              int sala, int jornada, int sede, int num_dia,
                                int blo1, int blo2){
    int reserva = r.getReservaTipo(año, semestre, cod_asign, paralelo,
                              sala, jornada, sede, num_dia, blo1, blo2);
    return reserva;
  }
  public Vector getPremiadoSancionado (String pergim_id){
    Vector vec_premiadosancionado = r.getPremiadoSancionado(pergim_id);
    return vec_premiadosancionado;
  }
  public Vector getReservaAluTutoria (String id_reg){
    Vector v = r.getReservaAluTutoria(id_reg);
    return v;
  }
  public Vector getReservaTutoriaPeriodo (String id_reg, int año, int semestre){
    Vector v = r.getReservaTutoriaPeriodo(id_reg, año, semestre);
   return v;
  }
  public Vector getReservaTutoriaPeriodoList (String id_reg, int año, int semestre){
    Vector v = r.getReservaTutoriaPeriodoList(id_reg, año, semestre);
return v;
  }

  public Vector getAsistenciaBloque(int rut, String fec_inicio, String fec_termino,
                                      String pergim_id, int blo1, int blo2){
  Vector v = r.getAsistenciaBloque(rut, fec_inicio, fec_termino, pergim_id, blo1, blo2);
 return v;
  }

  public int getSecuenciaHoraBloque (String horblo_id){
    int err = r.getSecuenciaHoraBloque(horblo_id);
    return err ;
  }

  public Vector getPromedioBloque (int año, int semestre, String fecha_inicio,
                                  String fecha_termino, int sala, int detalle){
    Vector v = r.getPromedioBloque(año,  semestre, fecha_inicio, fecha_termino,
                                   sala, detalle);
    return v;
  }
  public Vector getPromedioSemana (int año, int semestre, String fecha_inicio,
                                          String fecha_termino, int sala, int detalle){
    Vector v = r.getPromedioSemana(año,  semestre, fecha_inicio, fecha_termino,
                                   sala, detalle);
    return v;
  }
  public Vector getPromedioBloqueDet (int año, int semestre, String fecha_inicio,
                                String fecha_termino, int sala, int detalle){
  Vector v = r.getPromedioBloqueDet(año,  semestre, fecha_inicio, fecha_termino,
                                 sala, detalle);
  return v;
}

  synchronized public int setEliminaReserva (String id_reg) throws SQLException{
    int err = r.setEliminaReserva(id_reg);
    return err;
  }
  synchronized public int setPeriodoGimnasio(Vector vec) throws SQLException{
    int err = r.setPeriodoGimnasio(vec);
    return err;
  }
  synchronized public int setEstadoClasePeriodo(Vector vec){
    int err = r.setEstadoClasePeriodo(vec);
    return err;
  }
  synchronized public int setUpdEstadoClasePeriodo(Vector vec){
    int err = r.setUpdEstadoClasePeriodo(vec);
    return err;
  }
  synchronized public int setDltEstadoClasePeriodo(Vector vec) throws SQLException{
    int err = r.setDltEstadoClasePeriodo(vec);
    return err;
  }
  synchronized public int setCopiarHorario (Vector vec) throws SQLException{
    int err = r.setCopiarHorario(vec);
    return err;
  }
  synchronized public int setReservaGimnasio(Vector vec) throws SQLException{

    int err = r.setReservaGimnasio(vec);
    return err;
  }
  synchronized public String SetFechaFormato(String f1)  {
    String fecha = r.SetFechaFormato(f1);
    return fecha;
  }
  synchronized public int setAsistenciaGimnasio(Vector vec)throws SQLException{
	    int err = r.setAsistenciaGimnasio(vec);
	    return err;
	  }
  synchronized public int setUpdAsistenciaGimnasio(Vector vec)throws SQLException{
    int err = r.setUpdAsistenciaGimnasio(vec);
    return err;
  }
  synchronized public int setRegistraAsistenciaGimnasio(Vector vec) throws SQLException{
    int err = r.setRegistraAsistenciaGimnasio(vec);
    return err;
  }
  synchronized public int setReservaAsistenciaGimnasio(Vector vec) throws SQLException{
    int err = r.setReservaAsistenciaGimnasio(vec);
    return err;
  }
  synchronized public int setEliminarAsistencia(Vector vec) throws SQLException{
    int err = r.setEliminarAsistencia(vec);
    return err;
  }
  synchronized public int setListaEspera(Vector vec) throws SQLException{
    int err = r.setListaEspera(vec);
    return err;
  }
  synchronized public int setCerrarAnularClase(Vector vec)throws SQLException{
    int err = r.setCerrarAnularClase(vec);
    return err;
  }
   synchronized public Vector setGeneraPremiadoSancionado(Vector vec)throws SQLException{
     Vector v = r.setGeneraPremiadoSancionado(vec);
     return v;
   }
   synchronized public int setEliminaPremiadoSancionado(String pergim_id)throws SQLException{
     int err = r.setEliminaPremiadoSancionado(pergim_id);
     return err;
   }
   synchronized public int setEliminaRegistroPremiadoSancionado(String pergim_id,
                                                                String reggim_id) throws SQLException{
     int err = r.setEliminaRegistroPremiadoSancionado(pergim_id, reggim_id );
     return err;
   }
/* metodos de Datos_viña
  public Vector getPremiadosPeriodoListViña (int año, int semestre, String periodo , int cod_estado){
    Vector v = new Vector();
    v = dav.getPremiadosPeriodoListViña(año, semestre, periodo , cod_estado);
    return v;
 }
  public Vector getAlumnoViña (int rut_recep){
    Vector v = new Vector();
    v = dav.getAlumnoViña(rut_recep);
    return v;
  }
  public Vector getFuncionarioViña (int rut_usuario){
    Vector v = new Vector();
    v = dav.getFuncionarioViña(rut_usuario);
    return v;
  }
  public String getMailUsuarioViña (int rut){
    String mail = "";
    mail = dav.getMailUsuarioViña(rut);
    return mail;
  }
  public String getIngresoAlumnoViña (int rut_usuario){
    String fecha = "";
    fecha = dav.getIngresoAlumnoViña(rut_usuario);
    return fecha;
  }
  public boolean getHabilitadoReservaViña (int rut, int año, int semestre){
    boolean habilitado = false;
    habilitado = dav.getHabilitadoReservaViña(rut, año, semestre);
    return habilitado;

  }*/
  // metodos de Bitacora

  public Vector getTipoContacto (){
    Vector v = new Vector();
    v = b.getTipoContacto();
    return v;
  }
  public Vector getTipoPrioridad (){
    Vector v = new Vector();
    v = b.getTipoPrioridad();
    return v;
  }
  public Vector getMensaje (int año, int semestre, int mes){
    Vector v = new Vector();
    v = b.getMensaje(año, semestre, mes);
    return v;
  }
  public Vector getRespuestaMensaje (int año, int semestre, int mes){
    Vector v = new Vector();
    v = b.getRespuestaMensaje(año, semestre, mes);
    return v;
  }

  public Vector getNoticias (){
  Vector v = new Vector();
  v = b.getNoticias();
  return v;
  }
  public int getCorrelativoNoticias (){
    int corr = b.getCorrelativoNoticias();
    return corr;
  }
  public int setNoticias (int accion, int prioridad , int correlativo,
                          int año, int semestre,
                          String titulo, String mensaje, int rut_usuario){
    return b.setNoticias(accion, prioridad , correlativo,
                         año, semestre,
                         titulo, mensaje, rut_usuario);
  }
  synchronized public int setInsertarBitacora(Vector vec)throws SQLException{
    int err = b.setInsertarBitacora(vec);
    return err;
  }
  synchronized public int setActualizarBitacora(Vector vec)throws SQLException{
    int err = b.setActualizarBitacora(vec);
    return err;
  }
  synchronized public int setActualizarRespuesta(Vector vec) throws SQLException{
    int err = b.setActualizarRespuesta(vec);
    return err;
  }
  synchronized public int setEliminarBitacora(String id){
    int err = b.setEliminarBitacora(id);
    return err;
  }
  synchronized public int setEliminarRespuestaBitacora(String id, int id_resp){
    int err = b.setEliminarRespuestaBitacora(id, id_resp);
    return err;
  }
  synchronized public int setInsertarRespuesta(Vector vec) throws SQLException{
    int err = b.setInsertarRespuesta(vec);
    return err;
  }

}
