package gimnasio;
import java.io.*;
import java.util.*;
import java.sql.*;


/**
 * <p>Title: </p>
 * <p>Description: Conecci�n a la BD</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: UTFSM </p>
 * @author Monica Barrera
 * @fecha  11 de junio de 2007
 * @version 1.0
 */

public class Registro {
  private Conexion_gim con;

public Registro() {
}

public Registro(Conexion_gim con) {
this.con = con;
  }
  //**************************************************************************
// Metodo              : Permite capturar la nombre de los profesores
//
// Programadora        : Monica B.
// Fecha creaci�n      : 11/6/2007
// Fecha modificaci�n  :
//****************************************************************************
  public Vector getProfesores (){
  java.util.Vector v = new java.util.Vector();
  java.util.Vector a = null;
  String /* MB se cambia por cambio de estructura de funcionario 15/11/2010
          query = " SELECT f.fun_rut, f.fun_dig_rut, f.fun_nombres, " +
                 " f.fun_ap_paterno, ISNULL(f.fun_ap_materno,' ') "+
                 " FROM funcionario f " +
                 " WHERE sed_cod_sede = 1" +
                 " AND fun_tipo_funcionario = 2" +
                 " AND fun_vigente = 1" +
                 " ORDER BY f.fun_ap_paterno, f.fun_ap_materno, f.fun_nombres";*/


         query = " SELECT DISTINCT  f.fun_rut, f.fun_dig_rut, f.fun_nombres, " +
                 " f.fun_ap_paterno, NVL(f.fun_ap_materno,' ') "+
                 " FROM funcionario_cargo fc, funcionario f " +
                 " WHERE fc.cod_planta_funcionario = 32" +
                 " AND fc.sed_cod_sede = 1" +
                 " AND fc.cod_vigencia = 1" +
                 " AND f.fun_rut = fc.fun_rut " +
                 " ORDER BY 5, 4, 3";


   try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    while(res.next()){
      a = new java.util.Vector();
      a.addElement(res.getString(1)); // rut
      a.addElement(res.getString(2)); // dig rut
      a.addElement(res.getString(3)); // nombres
      a.addElement(res.getString(4)); // ap paterno
      a.addElement(res.getString(5)); // ap materno
      v.addElement(a);
    }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Registro.getProfesores: "+e);
  }
  return v;
  }
  //**************************************************************************
// Metodo              : Permite capturar las asignaturas del depto 41 menos las
//                       del gimnasio y las de deporte
// Programadora        : Monica B.
// Fecha creaci�n      : 11/6/2007
// Fecha modificaci�n  :
//****************************************************************************
public Vector getAsigRama (){
java.util.Vector v = new java.util.Vector();
java.util.Vector a = null;
String query = " SELECT a.cod_asign, a.nom_asign" +
               " FROM asignatura a LEFT JOIN rama_deportiva rd ON" +
               " rd.cod_asign = a.cod_asign AND" +
               " rd.ramdep_fecha_termino IS NULL" +
               " WHERE a.dep_cod_departamento = 41" +
               " AND cod_nivel_asignatura <> 3" +
               " AND a.cod_asign NOT IN (1410026,1410000,1410003,1410025,1410004)" +
               " AND rd.cod_asign IS NULL" +
               " ORDER BY nom_asign";
 try{
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  while(res.next()){
    a = new java.util.Vector();
    a.addElement(res.getString(1)); // cod_asign
    a.addElement(res.getString(2)); // nom_asign
    v.addElement(a);
  }
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Registro.getAsigRama: "+e);
}
return v;
  }

  //**************************************************************************
// Metodo              : Permite capturar las asignaturas de la rama deportiva
//                       con los profesores a cargo de la rama
// Programadora        : Monica B.
// Fecha creaci�n      : 12/6/2007
// Fecha modificaci�n  :
//****************************************************************************
public Vector getRamaDeportiva (){
java.util.Vector v = new java.util.Vector();
java.util.Vector a = null;
String query = " SELECT  r.cod_asign,  a.nom_asign, f.fun_rut," +
			   " SUBSTR(f.fun_nombres,1,1) || '. '|| f.fun_ap_paterno ||" +
			   " ' ' || NVL(SUBSTR(f.fun_ap_materno,1,1),' ') || '. '  " +
			   " , r.ramdep_id, f.fun_ap_paterno ||' '|| NVL(f.fun_ap_materno,' ')" +
			   " ||' ' || f.fun_nombres" +
			   " FROM rama_deportiva r, asignatura a, funcionario f" +
			   " WHERE r.cod_asign = a.cod_asign" +
			   " AND r.fun_rut = f.fun_rut  " +
			   " AND ramdep_fecha_termino IS NULL" +
			   " ORDER BY a.nom_asign";
 try{
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  while(res.next()){
    a = new java.util.Vector();
    a.addElement(res.getString(1)); // 0 cod_asign
    a.addElement(res.getString(2)); // 1  nom_asign
    a.addElement(res.getString(3)); // 2 fun_rut
    a.addElement(res.getString(4)); // 3 nombre funcionario abrev
    a.addElement(res.getString(5)); // 4 id
    a.addElement(res.getString(6)); // 5 nom profesor
    v.addElement(a);
  }
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Registro.getRamaDeportiva : "+e);
}
return v;
  }

//**************************************************************************
// Metodo              : Permite capturar la asignatura de la rama deportiva
//                       del alumno
// Programadora        : Monica B.
// Fecha creaci�n      : 13/6/2007
// Fecha modificaci�n  :
//****************************************************************************
public Vector getAlumnoRama (int rut){
java.util.Vector v = new java.util.Vector();

String query = " SELECT  r.cod_asign,  a.nom_asign, f.fun_rut," +
               " f.fun_ap_paterno ||' ' || NVL(f.fun_ap_materno,' ') || ' ' || " +
               " f.fun_nombres, r.ramdep_id" +
               " FROM alumno_rama ar, rama_deportiva r, asignatura a, funcionario f" +
               " WHERE ar.alu_rut = " + rut +
               " AND ar.aluram_fecha_termino IS NULL" +
               " AND ar.ramdep_id = r.ramdep_id" +
               " AND r.cod_asign = a.cod_asign" +
               " AND r.fun_rut = f.fun_rut" +
               " AND ramdep_fecha_termino IS NULL";
// System.out.println(query);
 try{
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  while(res.next()){
    v.addElement(res.getString(1)); // 0-cod_asign
    v.addElement(res.getString(2)); // 1-nom_asign
    v.addElement(res.getString(3)); // 2-fun_rut
    v.addElement(res.getString(4)); // 3-nombre funcionario
    v.addElement(res.getString(5)); // 4-id
   }
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Registro.getAlumnoRama : "+e);
}
return v;
  }
  //**************************************************************************
// Metodo              : Permite capturar los datos de registro
//                       del usuario del gimnasio
// Programadora        : Monica B.
// Fecha creaci�n      : 14/6/2007
// Fecha modificaci�n  :
//****************************************************************************
public Vector getRegistroUsuario (int rut){
java.util.Vector v = new java.util.Vector();
String query = "";

  query = " SELECT TO_CHAR(rg.reggim_fecha_inicio,'dd/mm/yyyy')," +
          " tr.cod_registro_gimnasio, tr.nom_registro_gimnasio, " +
          "  SUBSTR(a.alu_nombres,1,1) ||'. '|| a.alu_ap_paterno ||" +
          " ' ' || NVL(SUBSTR(a.alu_ap_materno,1,1),' ') || '.' ," +
          " rg.reggim_id" +
          " FROM registro_gimnasio rg, registro_gimnasio_alumno rga ," +
          " alumno a, tipo_registro_gimnasio  tr" +
          " WHERE rg.reggim_fecha_termino is null" +
          " AND rga.alu_rut = " + rut +
          " AND rg.rut_usuario = a.alu_rut" +
          " AND rga.reggim_id = rg.reggim_id" +
          " AND rg.cod_registro_gimnasio = tr.cod_registro_gimnasio" +
          " UNION " +
          " SELECT TO_CHAR(rg.reggim_fecha_inicio,'dd/mm/yyyy')," +
          " tr.cod_registro_gimnasio, tr.nom_registro_gimnasio, " +
          "  SUBSTR(a.alu_nombres,1,1) ||'. '|| a.alu_ap_paterno ||" +
          " ' ' || NVL(SUBSTR(a.alu_ap_materno,1,1),' ') || '.' ," +
          " rg.reggim_id" +
          " FROM registro_gimnasio rg, registro_gimnasio_funcionario rgf ," +
          " alumno a, tipo_registro_gimnasio  tr" +
          " WHERE rg.reggim_fecha_termino is null" +
          " AND rgf.fun_rut =" + rut +
          " AND rg.rut_usuario = a.alu_rut" +
          " AND rgf.reggim_id = rg.reggim_id" +
          " AND rg.cod_registro_gimnasio = tr.cod_registro_gimnasio" +
          " UNION " +
          " SELECT TO_CHAR(rg.reggim_fecha_inicio,'dd/mm/yyyy')," +
          " tr.cod_registro_gimnasio, tr.nom_registro_gimnasio, " +
          " SUBSTR(a.alu_nombres,1,1) ||'. '|| a.alu_ap_paterno ||" +
          " ' ' ||  NVL(SUBSTR(a.alu_ap_materno,1,1),' ') || '.' ," +
          " rg.reggim_id" +
          " FROM registro_gimnasio rg, registro_gimnasio_externo rge ," +
          " tipo_registro_gimnasio  tr, alumno a" +
          " WHERE rg.reggim_fecha_termino is null" +
          " AND rge.regext_rut =" + rut +
          " AND rg.rut_usuario = a.alu_rut" +
          " AND rge.reggim_id = rg.reggim_id" +
          " AND rg.cod_registro_gimnasio = tr.cod_registro_gimnasio" ;

Vector vec = new Vector();
String nombre = "";
//System.out.println("getRegistroUsuario: "+ query);
 try{
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  while(res.next()){
    v.addElement(res.getString(1)); // 0-fecha_inicio
    v.addElement(res.getString(2)); // 1-cod_registro_gimnasio
    v.addElement(res.getString(3)); // 2-nom_registro_gimnasio
    nombre = res.getString(4);
    v.addElement(nombre); // 3-nombre alumno usuario
    v.addElement(res.getString(5)); // 4-reggim_id


    }
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Registro.getRegistroUsuario  : "+e);
}
return v;
  }
//**************************************************************************
// Metodo              : Permite capturar si se encuentra habilitado el gimnasio
// Programadora        : Monica B.
// Fecha creaci�n      : 30/08/2007
// Fecha modificaci�n  :
//****************************************************************************
public int getActivacion (){
int activo = 0;

String query = "";

  query = " SELECT activo" +
          " FROM activacion" +
          " WHERE  tipo = 'G'" ;
 try{
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  while(res.next()){
    activo = res.getInt(1); // activo

  }
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Registro.getActivacion  : "+e);
}
return activo;
  }

//**************************************************************************
// Metodo              : Permite capturar las fechas de inicio y t�rmino de vigencia
//                       desde periodo_gimnasio, se sacar� de la sala de aer�bica
// Programadora        : Monica B.
// Fecha creaci�n      : 18/6/2007
// Fecha modificaci�n  :
//****************************************************************************
public Vector getPeriodoGimnasio (int año, int semestre){
java.util.Vector v = new java.util.Vector();

String query = "";

  query = " SELECT TO_CHAR(pergim_fecha_inicio_vig,'yyyymmdd')," +
          " TO_CHAR(pergim_fecha_termino_vig,'yyyymmdd')" +
          " FROM periodo_gimnasio" +
          " WHERE  pergim_año = " + año +
          " AND  pergim_semestre = " + semestre +
          " AND num_sala = 10336" +
          " AND pergim_fecha_cierre IS NULL";
 try{
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  while(res.next()){
    v.addElement(res.getString(1)); // 0-fecha_inicio vigencia
    v.addElement(res.getString(2)); // 1-fecha_termino vigencia
  }
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Registro.getPeriodoGimnasio  : "+e);
}
return v;
  }
  /**
   * Entrega datos necesarios para el listado de registros del gimnasio.
   * <P>
   *
   * <P>
   * Registro de versiones:<UL>
   * <LI>1.0 16/07/2007 Monica Barrera Frez: versi�n inicial.
   * <LI>1.0a 30/07/2007 Monica Barrera Frez
   * <LI>1.0b 05/03/2008 Monica Barrera F cambi� period_semestreActual por periodo_semestre
   * <li>1.0c 09/03/2009 Monica Barrera comentari� el filtro del semestre, para permitir un listado
   *                     con los del semestre adem�s de los anteriores.
   *          20/05/2009 MB agregar para mostrar alumnos de tutoria que pueda ser en ambas salas
  * </UL><P>
   *
   * @return Un vector con todos los datos necesarios para el listado de personas
   * registradas en el gimnasio.
   * @since  1.0
     */

public Vector getListadoRegistro (int año, int semestre, int tipo_usuario){
java.util.Vector v = new java.util.Vector();
java.util.Vector vec_rama = new java.util.Vector();
java.util.Vector a = null;
Vector vec_al = new Vector();
Vector vec_fun = new Vector();
Vector vec_tutoria = new Vector();
java.util.Vector v_asistencia = new java.util.Vector();
String paterno = "";
String materno = "";
String nombres = "";
String rut = "";
String depto = "";
String tutoria = "";
String pergim = "0";
int fecha_inicio = 0;
int fecha_termino = 0;
int tipo_viña = 0;
switch (tipo_usuario){
  case 1 : tipo_viña = 5;
    break;
  case 3 : tipo_viña = 6;
    break;
  }

//gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
//b.getAbrirConexionViña();
//b.setGenerarConexionDatosViña();

gimnasio.BeanGeneral gen = new gimnasio.BeanGeneral();
gen.getAbrirConexion();
gen.setGenerarConexionHorario();
gen.getPeriodoSemestre(año,semestre);
fecha_inicio = gen.getFechaInicioPeriodo();
fecha_termino = gen.getFechaTerminoPeriodo();

if(fecha_inicio > 0 && fecha_termino > 0){
String query = "";
if (tipo_usuario == 4 || tipo_usuario == 7) // es funcionario � funcionario viña
	/*
	query = " SELECT f.fun_ap_paterno as paterno, ISNULL(f.fun_ap_materno,' ') as materno," +
	        " f.fun_nombres as nombres," +
	        " CONVERT(VARCHAR,f.fun_rut) + '-' + f.fun_dig_rut, f.fun_rut," +
	        " d.dep_nom_departamento, s.sed_nom_sede,  CONVERT(VARCHAR(10),rg.reggim_fecha_inicio ,103)" +
	        " FROM registro_gimnasio rg, registro_gimnasio_funcionario rga, funcionario f " +
	        " departamento d, sede s" +
	        " WHERE rg.reggim_id = rga.reggim_id" +
	        " AND   rga.fun_rut = f.fun_rut" +
	        " AND   (rg.reggim_fecha_termino is null" +
	        "        OR (rg.reggim_fecha_termino <> NULL" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_inicio ,112) >= '" +
	        fecha_inicio + "'" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_termino ,112) <=  '" +
	        fecha_termino + "'" + "))" +
	        " AND   rg.cod_registro_gimnasio = 4" +
	        " AND   f.dep_cod_departamento = d.dep_cod_departamento " +
	        " AND   f.sed_cod_sede = s.sed_cod_sede" +
	        " AND   s.sed_cod_sede = 1" +
	        " UNION" +
	        " SELECT 'Z' as paterno, 'Z' as materno, 'Z' as nombres,' ', rge.regext_rut," +
	        " ' ', 'Viña del Mar',  CONVERT(VARCHAR(10),rg.reggim_fecha_inicio ,103)" +
	        " FROM registro_gimnasio rg, registro_gimnasio_externo rge " +
	        " WHERE rg.reggim_id = rge.reggim_id" +
	        " AND   (rg.reggim_fecha_termino is null" +
	        "        OR (rg.reggim_fecha_termino <> NULL" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_inicio ,112) >= '" +
	        fecha_inicio + "'" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_termino ,112) <=  '" +
	        fecha_termino + "'" + "))" +
	        " AND   rg.cod_registro_gimnasio = 7" +
	        " ORDER BY paterno, materno, nombres";
	  else if (tipo_usuario == 3 || tipo_usuario == 6) //  alumno rama
	    query =
	        " SELECT a.alu_ap_paterno as paterno, ISNULL(a.alu_ap_materno,' ') as materno," +
	        " a.alu_nombres as nombres," +
	        " CONVERT(VARCHAR,a.alu_rut) + '-' + a.alu_dig_rut, a.alu_rut, rg.reggim_id,  CONVERT(VARCHAR(10),rg.reggim_fecha_inicio ,103)" +
	        " FROM registro_gimnasio rg, registro_gimnasio_alumno rga, alumno a" +
	        " WHERE rg.reggim_id = rga.reggim_id" +
	        " AND   rga.alu_rut = a.alu_rut" +
	        " AND   (rg.reggim_fecha_termino is null" +
	        "        OR (rg.reggim_fecha_termino <> NULL" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_inicio ,112) >= '" +
	        fecha_inicio + "'" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_termino ,112) <=  '" +
	        fecha_termino + "'" + "))" +
	        " AND   cod_registro_gimnasio = " + tipo_usuario +
	        " UNION " +
	        " SELECT 'Z' as paterno, 'Z' as materno, 'Z' as nombres, ' ', rge.regext_rut, rg.reggim_id,  CONVERT(VARCHAR(10),rg.reggim_fecha_inicio ,103)" +
	        " FROM registro_gimnasio rg, registro_gimnasio_externo rge " +
	        " WHERE rg.reggim_id = rge.reggim_id" +
	        " AND   (rg.reggim_fecha_termino is null" +
	        "        OR (rg.reggim_fecha_termino <> NULL" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_inicio ,112) >= '" +
	        fecha_inicio + "'" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_termino ,112) <=  '" +
	        fecha_termino + "'" + "))" +
	        " AND   cod_registro_gimnasio = " + tipo_viña +
	        " UNION " +
	        " SELECT a.alu_ap_paterno as paterno, ISNULL(a.alu_ap_materno,' ') as materno," +
	        " a.alu_nombres as nombres," +
	        " CONVERT(VARCHAR,a.alu_rut) + '-' + a.alu_dig_rut, a.alu_rut, rg.reggim_id,  CONVERT(VARCHAR(10),rg.reggim_fecha_inicio ,103)" +
	        " FROM registro_gimnasio rg, registro_gimnasio_alumno rga, alumno a, alumno_rama ar" +
	        " WHERE rg.reggim_id = rga.reggim_id" +
	        " AND   rga.alu_rut = a.alu_rut" +
	        " AND   (rg.reggim_fecha_termino is null" +
	        "        OR (rg.reggim_fecha_termino <> NULL" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_inicio ,112) >= '" +
	        fecha_inicio + "'" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_termino ,112) <=  '" +
	        fecha_termino + "'" + "))" +
	        " AND   ar.alu_rut = a.alu_rut" +
	        " AND   (('" + fecha_inicio +  "' >= CONVERT(VARCHAR(8),ar.aluram_fecha_inicio ,112) " +
	        " AND '" + fecha_inicio +     "' <= CONVERT(VARCHAR(8),ar.aluram_fecha_termino  ,112))" +
	        " OR   ('" + fecha_termino +  "' >= CONVERT(VARCHAR(8),ar.aluram_fecha_inicio ,112) " +
	        " AND '" + fecha_termino +    "' <= CONVERT(VARCHAR(8),ar.aluram_fecha_termino  ,112)))" +
	        " UNION " +
	        " SELECT 'Z' as paterno, 'Z' as materno, 'Z' as nombres, ' ', rge.regext_rut, rg.reggim_id,  CONVERT(VARCHAR(10),rg.reggim_fecha_inicio ,103)" +
	        " FROM registro_gimnasio rg, registro_gimnasio_externo rge, alumno_rama ar " +
	        " WHERE rg.reggim_id = rge.reggim_id" +
	        " AND   (rg.reggim_fecha_termino is null" +
	        "        OR (rg.reggim_fecha_termino <> NULL" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_inicio ,112) >= '" +
	        fecha_inicio + "'" +
	        "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_termino ,112) <=  '" +
	        fecha_termino + "'" + "))" +
	        " AND   ar.alu_rut = rge.regext_rut" +
	        " AND   (('" + fecha_inicio +  "' >= CONVERT(VARCHAR(8),ar.aluram_fecha_inicio ,112) " +
	        " AND '" + fecha_inicio +     "' <= CONVERT(VARCHAR(8),ar.aluram_fecha_termino  ,112))" +
	        " OR   ('" + fecha_termino +  "' >= CONVERT(VARCHAR(8),ar.aluram_fecha_inicio ,112) " +
	        " AND '" + fecha_termino +    "' <= CONVERT(VARCHAR(8),ar.aluram_fecha_termino  ,112)))" +

	        " ORDER BY paterno, materno, nombres ";
	  else
	    // es alumno , tutor�a
	 query = " SELECT a.alu_ap_paterno as paterno, ISNULL(a.alu_ap_materno,' ') as materno," +
	       " a.alu_nombres as nombres," +
	       " CONVERT(VARCHAR,a.alu_rut) + '-' + a.alu_dig_rut, a.alu_rut, rg.reggim_id,  CONVERT(VARCHAR(10),rg.reggim_fecha_inicio ,103)" +
	       " FROM registro_gimnasio rg, registro_gimnasio_alumno rga, alumno a" +
	       " WHERE rg.reggim_id = rga.reggim_id" +
	       " AND   rga.alu_rut = a.alu_rut" +
	       " AND   (rg.reggim_fecha_termino is null" +
	       "        OR (rg.reggim_fecha_termino <> NULL" +
	       "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_inicio ,112) >= '" + fecha_inicio +"'" +
	       "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_termino ,112) <=  '" + fecha_termino +"'" + "))" +
	       " AND   cod_registro_gimnasio = " + tipo_usuario +
	       " UNION " +
	       " SELECT 'Z' as paterno, 'Z' as materno, 'Z' as nombres, ' ', rge.regext_rut, rg.reggim_id,  CONVERT(VARCHAR(10),rg.reggim_fecha_inicio ,103)" +
	       " FROM registro_gimnasio rg, registro_gimnasio_externo rge " +
	       " WHERE rg.reggim_id = rge.reggim_id" +
	       " AND   (rg.reggim_fecha_termino is null" +
	       "        OR (rg.reggim_fecha_termino <> NULL" +
	       "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_inicio ,112) >= '" +  fecha_inicio + "'" +
	       "            AND  CONVERT(VARCHAR(8),rg.reggim_fecha_termino ,112) <=  '" + fecha_termino + "'" + "))" +
	       " AND   cod_registro_gimnasio = " + tipo_viña +
	       " ORDER BY paterno, materno, nombres ";
	 MB modific� por cambio de estructura de funcionario 16/11/2010
	 */

query = " SELECT f.fun_ap_paterno as paterno, NVL(f.fun_ap_materno,' ') as materno," +
        " f.fun_nombres as nombres," +
        " TO_CHAR(f.fun_rut) || '-' || f.fun_dig_rut, f.fun_rut," +
        " d.dep_nom_departamento, s.sed_nom_sede,  TO_CHAR(rg.reggim_fecha_inicio ,'dd/mm/yyyy')" +
        " FROM registro_gimnasio rg, registro_gimnasio_funcionario rga, funcionario f, funcionario_cargo fc, " +
        " departamento d, sede s" +
        " WHERE rg.reggim_id = rga.reggim_id" +
        " AND rga.fun_rut = f.fun_rut" +
        " AND (rg.reggim_fecha_termino is null" +
        " OR(rg.reggim_fecha_termino IS NOT NULL" +
        " AND TO_CHAR(rg.reggim_fecha_inicio ,'yyyymmdd') >= " +fecha_inicio + 
        " AND TO_CHAR(rg.reggim_fecha_termino ,'yyyymmdd') <=  " +fecha_termino + "))" +
        " AND rg.cod_registro_gimnasio = 4" +
        " AND f.fun_rut = fc.fun_rut" +
        " AND fc.funcar_principal = 1" +
        " AND fc.cod_vigencia = 1 " +
        " AND fc.dep_cod_departamento = d.dep_cod_departamento " +
        " AND fc.sed_cod_sede = s.sed_cod_sede" +
        " AND s.sed_cod_sede = 1" +
        " UNION" +
        " SELECT 'Z' as paterno, 'Z' as materno, 'Z' as nombres,' ', rge.regext_rut," +
        " ' ', 'Viña del Mar', TO_CHAR(rg.reggim_fecha_inicio ,'dd/mm/yyyy')" +
        " FROM registro_gimnasio rg, registro_gimnasio_externo rge " +
        " WHERE rg.reggim_id = rge.reggim_id" +
        " AND (rg.reggim_fecha_termino is null" +
        " OR (rg.reggim_fecha_termino IS NOT NULL" +
        " AND TO_CHAR (rg.reggim_fecha_inicio ,'yyyymmdd') >= " +fecha_inicio + 
        " AND TO_CHAR (rg.reggim_fecha_termino ,'yyyymmdd') <= " +fecha_termino +  "))" +
        " AND rg.cod_registro_gimnasio = 7" +
        " ORDER BY paterno, materno, nombres";

  else if (tipo_usuario == 3 || tipo_usuario == 6) //  alumno rama
    query =
        " SELECT a.alu_ap_paterno as paterno, NVL(a.alu_ap_materno,' ') as materno," +
        " a.alu_nombres as nombres," +
        " TO_CHAR(a.alu_rut) || '-' || a.alu_dig_rut, a.alu_rut, rg.reggim_id, TO_CHAR(rg.reggim_fecha_inicio ,'dd/mm/yyyy')" +
        " FROM registro_gimnasio rg, registro_gimnasio_alumno rga, alumno a" +
        " WHERE rg.reggim_id = rga.reggim_id" +
        " AND rga.alu_rut = a.alu_rut" +
        " AND (rg.reggim_fecha_termino is null" +
        " OR (rg.reggim_fecha_termino IS NOT NULL" +
        " AND TO_CHAR(rg.reggim_fecha_inicio ,'yyyymmdd') >= " +fecha_inicio + 
        " AND TO_CHAR(rg.reggim_fecha_termino ,'yyyymmdd') <= " +fecha_termino + "))" +
        " AND cod_registro_gimnasio = " + tipo_usuario +
        " UNION " +
        " SELECT 'Z' as paterno, 'Z' as materno, 'Z' as nombres, ' ', rge.regext_rut, rg.reggim_id, TO_CHAR(rg.reggim_fecha_inicio ,'dd/mm/yyyy')" +
        " FROM registro_gimnasio rg, registro_gimnasio_externo rge " +
        " WHERE rg.reggim_id = rge.reggim_id" +
        " AND (rg.reggim_fecha_termino is null" +
        " OR (rg.reggim_fecha_termino IS NOT NULL" +
        " AND TO_CHAR(rg.reggim_fecha_inicio ,'yyyymmdd') >= " +fecha_inicio + 
        " AND TO_CHAR(rg.reggim_fecha_termino ,'yyyymmdd') <= " +fecha_termino + "))" +
        " AND   cod_registro_gimnasio = " + tipo_viña +
        " UNION " +
        " SELECT a.alu_ap_paterno as paterno, NVL(a.alu_ap_materno,' ') as materno," +
        " a.alu_nombres as nombres," +
        " TO_CHAR(a.alu_rut) || '-' || a.alu_dig_rut, a.alu_rut, rg.reggim_id, TO_CHAR(rg.reggim_fecha_inicio ,'dd/mm/yyyy')" +
        " FROM registro_gimnasio rg, registro_gimnasio_alumno rga, alumno a, alumno_rama ar" +
        " WHERE rg.reggim_id = rga.reggim_id" +
        " AND rga.alu_rut = a.alu_rut" +
        " AND (rg.reggim_fecha_termino is null" +
        " OR (rg.reggim_fecha_termino IS NOT NULL" +
        " AND TO_CHAR(rg.reggim_fecha_inicio ,'yyyymmdd') >= " + fecha_inicio + 
        " AND TO_CHAR(rg.reggim_fecha_termino ,'yyyymmdd') <= " +fecha_termino + "))" +
        " AND ar.alu_rut = a.alu_rut" +
        " AND ((" + fecha_inicio +  " >= TO_CHAR(ar.aluram_fecha_inicio ,'yyyymmdd') " +
        " AND " + fecha_inicio +     " <= TO_CHAR(ar.aluram_fecha_termino  ,'yyyymmdd'))" +
        " OR  (" + fecha_termino +  " >= TO_CHAR(ar.aluram_fecha_inicio ,'yyyymmdd') " +
        " AND " + fecha_termino +    " <= TO_CHAR(ar.aluram_fecha_termino  ,'yyyymmdd')))" +
        " UNION " +
        " SELECT 'Z' as paterno, 'Z' as materno, 'Z' as nombres, ' ', rge.regext_rut, rg.reggim_id, TO_CHAR(rg.reggim_fecha_inicio,'dd/mm/yyyy')" +
        " FROM registro_gimnasio rg, registro_gimnasio_externo rge, alumno_rama ar " +
        " WHERE rg.reggim_id = rge.reggim_id" +
        " AND (rg.reggim_fecha_termino is null" +
        " OR (rg.reggim_fecha_termino IS NOT NULL" +
        " AND TO_CHAR(rg.reggim_fecha_inicio ,'yyyymmdd') >= " +fecha_inicio + 
        " AND TO_CHAR(rg.reggim_fecha_termino ,'yyyymmdd') <= " +fecha_termino + "))" +
        " AND ar.alu_rut = rge.regext_rut" +
        " AND ((" + fecha_inicio +  " >= TO_CHAR(ar.aluram_fecha_inicio ,'yyyymmdd') " +
        " AND " + fecha_inicio +    " <= TO_CHAR(ar.aluram_fecha_termino  ,'yyyymmdd'))" +
        " OR  (" + fecha_termino +  " >= TO_CHAR(ar.aluram_fecha_inicio ,'yyyymmdd') " +
        " AND " + fecha_termino +   " <= TO_CHAR(ar.aluram_fecha_termino  ,'yyyymmdd')))" +
        " ORDER BY paterno, materno, nombres ";
  else
    // es alumno , tutor�a
 query = " SELECT a.alu_ap_paterno as paterno, NVL(a.alu_ap_materno,' ') as materno," +
       " a.alu_nombres as nombres," +
       " TO_CHAR(a.alu_rut) || '-' || a.alu_dig_rut, a.alu_rut, rg.reggim_id, TO_CHAR(rg.reggim_fecha_inicio,'dd/mm/yyyy')" +
       " FROM registro_gimnasio rg, registro_gimnasio_alumno rga, alumno a" +
       " WHERE rg.reggim_id = rga.reggim_id" +
       " AND rga.alu_rut = a.alu_rut" +
       " AND (rg.reggim_fecha_termino is null" +
       " OR (rg.reggim_fecha_termino IS NOT NULL" +
       " AND TO_CHAR(rg.reggim_fecha_inicio ,'yyyymmdd') >= " + fecha_inicio +
       " AND TO_CHAR(rg.reggim_fecha_termino ,'yyyymmdd') <= " + fecha_termino + "))" +
       " AND cod_registro_gimnasio = " + tipo_usuario +
       " UNION " +
       " SELECT 'Z' as paterno, 'Z' as materno, 'Z' as nombres, ' ', rge.regext_rut, rg.reggim_id, TO_CHAR(rg.reggim_fecha_inicio,'dd/mm/yyyy')" +
       " FROM registro_gimnasio rg, registro_gimnasio_externo rge " +
       " WHERE rg.reggim_id = rge.reggim_id" +
       " AND (rg.reggim_fecha_termino is null" +
       " OR (rg.reggim_fecha_termino IS NOT NULL" +
       " AND TO_CHAR(rg.reggim_fecha_inicio ,'yyyymmdd') >= " +  fecha_inicio + 
       " AND TO_CHAR(rg.reggim_fecha_termino ,'yyyymmdd') <= " + fecha_termino +  "))" +
       " AND cod_registro_gimnasio = " + tipo_viña +
       " ORDER BY paterno, materno, nombres ";

//System.out.println("getListadoRegistro: "+ query);
// System.out.println(tipo_usuario);
 try{
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  while(res.next()){
    paterno = "";
    materno = "";
    nombres = "";
    rut     = "";
    depto   = "";
    tutoria = "";
    /*if ( res.getString(1).trim().equals("Z")){
//System.out.println("x1");
      // es de viña
      vec_al = b.getAlumnoViña(res.getInt(5));
      if (vec_al.size() > 0){
        paterno = vec_al.get(14)+"";
        materno = vec_al.get(15)+"";
        nombres = vec_al.get(13)+"";
        rut     = vec_al.get(12)+"";
      }
      vec_fun = b.getFuncionarioViña(res.getInt(5));
      if (vec_fun.size() > 0){
        paterno = vec_fun.get(2)+"";
        materno = vec_fun.get(3)+"";
        nombres = vec_fun.get(4)+"";
        rut     = vec_fun.get(0)+"-"+vec_fun.get(1);
        depto   = "Viña del Mar ";
    }
    } else {*/
//System.out.println("x2");
      paterno = res.getString(1); // 0-paterno
      materno = res.getString(2); // 1-materno
      nombres = res.getString(3); // 2-nombres
      rut     = res.getString(4); // 3-rut

    //}
    a = new java.util.Vector();
    a.addElement(paterno); // 0-paterno
    a.addElement(materno); // 1-materno
    a.addElement(nombres); // 2-nombres
    a.addElement(rut); // 3-rut
    if(tipo_usuario == 3){
//System.out.println("x3");
      vec_rama = getAlumnoRama(res.getInt(5));
//System.out.println(vec_rama );
      if (vec_rama.size()  > 0){
        a.addElement(vec_rama.get(1)+""); // 4 - nombre rama
        a.addElement(vec_rama.get(3)+""); // 5 - profesor a cargo
        a.addElement(res.getString(7)); // 6 fecha registro
      } else {
        a.addElement(""); // 4 - nombre rama
        a.addElement(""); // 5 - profesor a cargo
        a.addElement(res.getString(7)); // 6 fecha registro

      }

    }
    if(tipo_usuario == 4){
      depto = res.getString(6)+", " + res.getString(7);

       a.addElement(depto); // 4 - departamento sede
       a.addElement(res.getString(7)); // 5 pergim_id
       a.addElement(res.getString(8)); // 6 fecha registro
     }

     if (tipo_usuario == 2){
      gen.setGenerarConexionReserva();
       vec_tutoria = gen.getReservaTutoriaPeriodoList(res.getString(6),año,semestre);
       if (vec_tutoria.size() > 0){
         pergim = "0";
         for (int i=0; i < vec_tutoria.size();i++){
          // System.out.println("pergim: "+pergim +"tutoria: "+tutoria);
           if (!pergim.trim().equals((((Vector)vec_tutoria.get(i)).get(5)+"").trim())) tutoria += ((Vector)vec_tutoria.get(i)).get(0)+"<br>";
           tutoria +=  ((Vector)vec_tutoria.get(i)).get(1)+"<br>";
           pergim = ((Vector)vec_tutoria.get(i)).get(5)+"";
         }
       }
       if (tutoria.trim().equals("")) tutoria = "&nbsp;";
       a.addElement(tutoria); // 4 horario de tutoria

      a.addElement(res.getString(6)); // 5 pergim_id
      a.addElement(res.getString(7)); // 6 fecha registro
     }
     if(tipo_usuario == 1 ){
       a.addElement(depto); // 4 - departamento sede
       a.addElement(res.getString(6)); // 5 pergim_id
       a.addElement(res.getString(7)); // 6 fecha registro

     }
     v.addElement(a);

    }
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Registro.getListadoRegistro  : "+e);
}
} else v = null;
gen.getCerrarConexion();
//b.getCerrarConexionViña();
//System.out.println("vec "+ v);
return v;

  }
//**************************************************************************
// Metodo              : Permite listar los inscritos para la fecha y d�a
//
// Programadora        : Monica B.
// Fecha creaci�n      : 1/10/2007
// Fecha modificaci�n  :
//****************************************************************************
  public Vector getListadoInscripciones (int año,
                                          int semestre,
                                          int bloque_inicio,
                                          int bloque_fin,
                                          String fecha,
                                          int num_dia,
                                          int sala){
  java.util.Vector v = new java.util.Vector();
  java.util.Vector a = null;
  Vector vec_al = new Vector();
  Vector vec_fun = new Vector();
  String paterno = "";
  String materno = "";
  String nombres = "";
  String rut = "";
  String tipo_inscripcion = "";
  gimnasio.BeanGeneral b2 = new gimnasio.BeanGeneral();
  b2.getAbrirConexion();
  b2.setGenerarConexionHorario();
  int sec_inicio = b2.getSecuenciaBloque( sala, año, semestre, bloque_inicio);
  int sec_termino = b2.getSecuenciaBloque( sala, año, semestre, bloque_fin);
  b2.getCerrarConexion();

  gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
  //b.getAbrirConexionViña();
  //b.setGenerarConexionDatosViña();


  String query = "";

//  query = " SELECT f.fun_ap_paterno as paterno, ISNULL(f.fun_ap_materno,' ') as materno,"+
//          " f.fun_nombres as nombres, f.fun_rut," +
//          " CONVERT(VARCHAR,f.fun_rut) + '-' + f.fun_dig_rut," +
//          " tc.nom_cupo, isnull(ag.cod_asistencia,-1), isnull(ta.nom_asistencia,'&nbsp;')," +
//          " rg.cod_registro_gimnasio, tr.nom_registro_gimnasio" +
//          " FROM registro_gimnasio rg, registro_gimnasio_funcionario rga, funcionario f," +
//          " reserva_gimnasio reg LEFT JOIN asistencia_gimnasio ag ON" +
//          " ag.resgim_id = reg.resgim_id" +
//          " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha +"'" +
//          " LEFT JOIN tipo_asistencia ta ON" +
//          " ag.cod_asistencia = ta.cod_asistencia" +
//          " , tipo_cupo tc, tipo_registro_gimnasio tr, periodo_gimnasio pg" +
//          " , hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
//          " WHERE (ag.cod_asistencia <= 3 OR ag.resgim_id is null )" +
//          " AND rg.reggim_id = rga.reggim_id" +
//          " AND   rga.fun_rut = f.fun_rut" +
//          " AND   rg.reggim_fecha_termino is null" +
//          " AND   rg.reggim_id = reg.reggim_id" +
//          " AND   reg.pro_año = " + año +
//          " AND   reg.pro_semestre = " + semestre +
//          " AND   '" + fecha + "' >= CONVERT(VARCHAR(8),reg.resgim_fecha_inicio ,112)" +
//          " AND   '" + fecha + "' <= CONVERT(VARCHAR(8),reg.resgim_fecha_termino ,112)" +
//          " AND  ((" +  bloque_inicio + " >= reg.cod_bloque_inicio"+
//          " AND " +  bloque_inicio + " <= reg.cod_bloque_termino ) OR "+
//          " ( " +  bloque_fin + " >= reg.cod_bloque_inicio"+
//          " AND " +  bloque_fin + " <= reg.cod_bloque_termino))"+
//          " AND   reg.num_dia = " + num_dia +
//          " AND   reg.cod_cupo = tc.cod_cupo" +
//          " AND  rg.cod_registro_gimnasio = tr.cod_registro_gimnasio" +
//          " AND  reg.pergim_id = pg.pergim_id" +
//          " AND  pg.num_sala = " + sala +
//          " UNION" +
//          " SELECT a.alu_ap_paterno as paterno, ISNULL(a.alu_ap_materno,' ') as materno," +
//          " a.alu_nombres as nombres,  a.alu_rut," +
//          " CONVERT(VARCHAR,a.alu_rut) + '-' + a.alu_dig_rut," +
//          " tc.nom_cupo, isnull(ag.cod_asistencia,-1), isnull(ta.nom_asistencia,'&nbsp;')," +
//          " rg.cod_registro_gimnasio,  tr.nom_registro_gimnasio" +
//          " FROM registro_gimnasio rg, registro_gimnasio_alumno rga, alumno a," +
//          " reserva_gimnasio reg LEFT JOIN asistencia_gimnasio ag ON" +
//          " ag.resgim_id = reg.resgim_id" +
//          " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha + "'" +
//          " LEFT JOIN tipo_asistencia ta ON" +
//          " ag.cod_asistencia = ta.cod_asistencia" +
//          " , tipo_cupo tc , tipo_registro_gimnasio tr, periodo_gimnasio pg" +
//          " WHERE (ag.cod_asistencia <= 3 OR ag.resgim_id is null )" +
//          " AND rg.reggim_id = rga.reggim_id" +
//          " AND   rga.alu_rut = a.alu_rut" +
//          " AND   rg.reggim_fecha_termino is null" +
//          " AND   rg.reggim_id = reg.reggim_id" +
//          " AND   reg.pro_año = " + año +
//          " AND   reg.pro_semestre = " + semestre +
//          " AND   '" + fecha + "' >= CONVERT(VARCHAR(8),reg.resgim_fecha_inicio ,112)" +
//          " AND   '" + fecha + "' <= CONVERT(VARCHAR(8),reg.resgim_fecha_termino ,112)" +
//          " AND  ((" +  bloque_inicio + " >= reg.cod_bloque_inicio"+
//          " AND " +  bloque_inicio + " <= reg.cod_bloque_termino ) OR "+
//          " ( " +  bloque_fin + " >= reg.cod_bloque_inicio"+
//          " AND " +  bloque_fin + " <= reg.cod_bloque_termino))"+
//          " AND   reg.num_dia = " + num_dia +
//          " AND   reg.cod_cupo = tc.cod_cupo" +
//          " AND  rg.cod_registro_gimnasio = tr.cod_registro_gimnasio" +
//          " AND  reg.pergim_id = pg.pergim_id" +
//          " AND  pg.num_sala = " + sala +
//          " UNION" +
//          " SELECT 'Z' as paterno, 'Z' as materno, 'Z' as nombres, rge.regext_rut," +
//          " ' ',  tc.nom_cupo, isnull(ag.cod_asistencia,-1), isnull(ta.nom_asistencia,'&nbsp;')," +
//          " rg.cod_registro_gimnasio,  tr.nom_registro_gimnasio" +
//          " FROM registro_gimnasio rg, registro_gimnasio_externo rge," +
//          " reserva_gimnasio reg LEFT JOIN asistencia_gimnasio ag ON" +
//          " ag.resgim_id = reg.resgim_id" +
//          " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha + "'" +
//          " LEFT JOIN tipo_asistencia ta ON" +
//          " ag.cod_asistencia = ta.cod_asistencia" +
//          " , tipo_cupo tc , tipo_registro_gimnasio tr, periodo_gimnasio pg" +
//          " WHERE (ag.cod_asistencia <= 3 OR ag.resgim_id is null )" +
//          " AND rg.reggim_id = rge.reggim_id" +
//          " AND   rg.reggim_fecha_termino is null" +
//          " AND   rg.reggim_id = reg.reggim_id" +
//          " AND   reg.pro_año = " + año +
//          " AND   reg.pro_semestre = " + semestre +
//          " AND   '" + fecha + "' >= CONVERT(VARCHAR(8),reg.resgim_fecha_inicio ,112)" +
//          " AND   '" + fecha + "' <= CONVERT(VARCHAR(8),reg.resgim_fecha_termino ,112)" +
//          " AND  ((" +  bloque_inicio + " >= reg.cod_bloque_inicio"+
//          " AND " +  bloque_inicio + " <= reg.cod_bloque_termino ) OR "+
//          " ( " +  bloque_fin + " >= reg.cod_bloque_inicio"+
//          " AND " +  bloque_fin + " <= reg.cod_bloque_termino))"+
//          " AND   reg.num_dia = " + num_dia +
//          " AND   reg.cod_cupo = tc.cod_cupo" +
//          " AND  rg.cod_registro_gimnasio = tr.cod_registro_gimnasio" +
//          " AND  reg.pergim_id = pg.pergim_id" +
//          " AND  pg.num_sala = " + sala +
//          " ORDER BY paterno, materno, nombres" ;
  query = " SELECT f.fun_ap_paterno as paterno, NVL(f.fun_ap_materno,' ') as materno,"+
          " f.fun_nombres as nombres, f.fun_rut," +
          " TO_CHAR(f.fun_rut) || '-' || f.fun_dig_rut," +
          " tc.nom_cupo, NVL(ag.cod_asistencia,-1), NVL(ta.nom_asistencia,'&nbsp;')," +
          " rg.cod_registro_gimnasio, tr.nom_registro_gimnasio" +
          " FROM registro_gimnasio rg, registro_gimnasio_funcionario rga, funcionario f," +
          " reserva_gimnasio reg LEFT JOIN asistencia_gimnasio ag ON" +
          " ag.resgim_id = reg.resgim_id" +
          " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = " + fecha +
          " LEFT JOIN tipo_asistencia ta ON" +
          " ag.cod_asistencia = ta.cod_asistencia" +
          " , tipo_cupo tc, tipo_registro_gimnasio tr, periodo_gimnasio pg" +
          " , hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
          " WHERE (ag.cod_asistencia <= 3 OR ag.resgim_id is null )" +
          " AND rg.reggim_id = rga.reggim_id" +
          " AND   rga.fun_rut = f.fun_rut" +
          " AND   rg.reggim_fecha_termino is null" +
          " AND   rg.reggim_id = reg.reggim_id" +
          " AND   reg.pro_año = " + año +
          " AND   reg.pro_semestre = " + semestre +
          " AND   " + fecha + " >= TO_CHAR(reg.resgim_fecha_inicio,'yyyymmdd')" +
          " AND   " + fecha + " <= TO_CHAR(reg.resgim_fecha_termino,'yyyymmdd')" +
          " AND reg.cod_bloque_inicio = hbs.cod_bloque" +
          " AND hbs.horblo_año =  " + año +
          " AND hbs.horblo_semestre = " + semestre +
          " AND hbs.num_sala = " + sala +
          " AND hbs.sed_cod_sede = 1" +
          " AND hbs.cod_jornada = 1" +
          " AND hbs.horblo_fecha_termino is null" +
          " AND reg.cod_bloque_termino = hbs2.cod_bloque" +
          " AND hbs2.horblo_año =  " + año +
          " AND hbs2.horblo_semestre = " + semestre +
          " AND hbs2.num_sala = " + sala +
          " AND hbs2.sed_cod_sede = 1" +
          " AND hbs2.cod_jornada = 1" +
          " AND hbs2.horblo_fecha_termino is null" +
          " AND  ((" +  sec_inicio+ " >= hbs.horblo_secuencia"+
          " AND " +  sec_inicio + " <= hbs2.horblo_secuencia ) OR "+
          " ( " +  sec_termino + " >= hbs.horblo_secuencia"+
          " AND " +  sec_termino + " <= hbs2.horblo_secuencia))"+
          " AND   reg.num_dia = " + num_dia +
          " AND   reg.cod_cupo = tc.cod_cupo" +
          " AND  rg.cod_registro_gimnasio = tr.cod_registro_gimnasio" +
          " AND  reg.pergim_id = pg.pergim_id" +
          " AND  pg.num_sala = " + sala +
          " UNION" +
          " SELECT a.alu_ap_paterno as paterno, NVL(a.alu_ap_materno,' ') as materno," +
          " a.alu_nombres as nombres,  a.alu_rut," +
          " TO_CHAR(a.alu_rut) || '-' || a.alu_dig_rut," +
          " tc.nom_cupo, NVL(ag.cod_asistencia,-1), NVL(ta.nom_asistencia,'&nbsp;')," +
          " rg.cod_registro_gimnasio,  tr.nom_registro_gimnasio" +
          " FROM registro_gimnasio rg, registro_gimnasio_alumno rga, alumno a," +
          " reserva_gimnasio reg LEFT JOIN asistencia_gimnasio ag ON" +
          " ag.resgim_id = reg.resgim_id" +
          " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = " + fecha + 
          " LEFT JOIN tipo_asistencia ta ON" +
          " ag.cod_asistencia = ta.cod_asistencia" +
          " , tipo_cupo tc , tipo_registro_gimnasio tr, periodo_gimnasio pg" +
          " , hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
          " WHERE (ag.cod_asistencia <= 3 OR ag.resgim_id is null )" +
          " AND rg.reggim_id = rga.reggim_id" +
          " AND   rga.alu_rut = a.alu_rut" +
          " AND   rg.reggim_fecha_termino is null" +
          " AND   rg.reggim_id = reg.reggim_id" +
          " AND   reg.pro_año = " + año +
          " AND   reg.pro_semestre = " + semestre +
          " AND   " + fecha + " >= TO_CHAR(reg.resgim_fecha_inicio ,'yyyymmdd')" +
          " AND   " + fecha + " <= TO_CHAR(reg.resgim_fecha_termino,'yyyymmdd')" +
          " AND reg.cod_bloque_inicio = hbs.cod_bloque" +
          " AND hbs.horblo_año =  " + año +
          " AND hbs.horblo_semestre = " + semestre +
          " AND hbs.num_sala = " + sala +
          " AND hbs.sed_cod_sede = 1" +
          " AND hbs.cod_jornada = 1" +
          " AND hbs.horblo_fecha_termino is null" +
          " AND reg.cod_bloque_termino = hbs2.cod_bloque" +
          " AND hbs2.horblo_año =  " + año +
          " AND hbs2.horblo_semestre = " + semestre +
          " AND hbs2.num_sala = " + sala +
          " AND hbs2.sed_cod_sede = 1" +
          " AND hbs2.cod_jornada = 1" +
          " AND hbs2.horblo_fecha_termino is null" +
          " AND  ((" +  sec_inicio+ " >= hbs.horblo_secuencia"+
          " AND " +  sec_inicio + " <= hbs2.horblo_secuencia ) OR "+
          " ( " +  sec_termino + " >= hbs.horblo_secuencia"+
          " AND " +  sec_termino + " <= hbs2.horblo_secuencia))"+
          " AND   reg.num_dia = " + num_dia +
          " AND   reg.cod_cupo = tc.cod_cupo" +
          " AND  rg.cod_registro_gimnasio = tr.cod_registro_gimnasio" +
          " AND  reg.pergim_id = pg.pergim_id" +
          " AND  pg.num_sala = " + sala +
          " UNION" +
          " SELECT 'Z' as paterno, 'Z' as materno, 'Z' as nombres, rge.regext_rut," +
          " ' ',  tc.nom_cupo, NVL(ag.cod_asistencia,-1), NVL(ta.nom_asistencia,'&nbsp;')," +
          " rg.cod_registro_gimnasio,  tr.nom_registro_gimnasio" +
          " FROM registro_gimnasio rg, registro_gimnasio_externo rge," +
          " reserva_gimnasio reg LEFT JOIN asistencia_gimnasio ag ON" +
          " ag.resgim_id = reg.resgim_id" +
          " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = " + fecha + 
          " LEFT JOIN tipo_asistencia ta ON" +
          " ag.cod_asistencia = ta.cod_asistencia" +
          " , tipo_cupo tc , tipo_registro_gimnasio tr, periodo_gimnasio pg" +
          " , hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
          " WHERE (ag.cod_asistencia <= 3 OR ag.resgim_id is null )" +
          " AND rg.reggim_id = rge.reggim_id" +
          " AND   rg.reggim_fecha_termino is null" +
          " AND   rg.reggim_id = reg.reggim_id" +
          " AND   reg.pro_año = " + año +
          " AND   reg.pro_semestre = " + semestre +
          " AND   " + fecha + " >= TO_CHAR(reg.resgim_fecha_inicio,'yyyymmdd')" +
          " AND   " + fecha + " <= TO_CHAR(reg.resgim_fecha_termino,'yyyymmdd')" +
          " AND reg.cod_bloque_inicio = hbs.cod_bloque" +
          " AND hbs.horblo_año =  " + año +
          " AND hbs.horblo_semestre = " + semestre +
          " AND hbs.num_sala = " + sala +
          " AND hbs.sed_cod_sede = 1" +
          " AND hbs.cod_jornada = 1" +
          " AND hbs.horblo_fecha_termino is null" +
          " AND reg.cod_bloque_termino = hbs2.cod_bloque" +
          " AND hbs2.horblo_año =  " + año +
          " AND hbs2.horblo_semestre = " + semestre +
          " AND hbs2.num_sala = " + sala +
          " AND hbs2.sed_cod_sede = 1" +
          " AND hbs2.cod_jornada = 1" +
          " AND hbs2.horblo_fecha_termino is null" +
          " AND  ((" +  sec_inicio+ " >= hbs.horblo_secuencia"+
          " AND " +  sec_inicio + " <= hbs2.horblo_secuencia ) OR "+
          " ( " +  sec_termino + " >= hbs.horblo_secuencia"+
          " AND " +  sec_termino + " <= hbs2.horblo_secuencia))"+
          " AND   reg.num_dia = " + num_dia +
          " AND   reg.cod_cupo = tc.cod_cupo" +
          " AND  rg.cod_registro_gimnasio = tr.cod_registro_gimnasio" +
          " AND  reg.pergim_id = pg.pergim_id" +
          " AND  pg.num_sala = " + sala +
          " ORDER BY paterno, materno, nombres" ;

//System.out.println("getListadoInscripciones: "+ query);
   try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    while(res.next()){
      paterno = "";
      materno = "";
      nombres = "";
      rut     = "";
      /*if ( res.getString(1).trim().equals("Z")){
        // es de viña
        if (res.getInt(9) == 5 || res.getInt(9) == 6) { // es alumno viña
        vec_al = b.getAlumnoViña(res.getInt(4));
        if (vec_al.size() > 0){
          paterno = vec_al.get(14)+"";
          materno = vec_al.get(15)+"";
          nombres = vec_al.get(13)+"";
          rut     = vec_al.get(12)+"";
        }
        } else { // es funcionario viña
        vec_fun = b.getFuncionarioViña(res.getInt(4));
        if (vec_fun.size() > 0){
          paterno = vec_fun.get(1)+"";
          materno = vec_fun.get(2)+"";
          nombres = vec_fun.get(0)+"";
          rut     = vec_fun.get(13)+"";
        }
      }
      } else {*/

        paterno = res.getString(1); // 0-paterno
        materno = res.getString(2); // 1-materno
        nombres = res.getString(3); // 2-nombres
        rut     = res.getString(5); // 3-rut

      /*}*/
      a = new java.util.Vector();
      a.addElement(paterno); // 0-paterno
      a.addElement(materno); // 1-materno
      a.addElement(nombres); // 2-nombres
      a.addElement(rut); // 3-rut
      a.addElement(res.getString(6));//4 - nom_cupo
      a.addElement(res.getString(7));//5 - cod_asistencia
      a.addElement(res.getString(8));//6 - nom_asistencia
      a.addElement(res.getString(9));//7 - cod_registro
      a.addElement(res.getString(10));//8 - nom_registro
      v.addElement(a);

      }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Registro.getListadoInscripciones  : "+e);
  }


  //b.getCerrarConexionViña();
  return v;

  }

//**************************************************************************
// Metodo              : Permite eliminar rama deportiva
//
// Programadora        : Monica B.
// Fecha creaci�n      : 8/6/2007
// Fecha modificaci�n  :
//****************************************************************************
synchronized public int setEliminarRamaDeportiva(int cod_asign , int rut_usuario){
  //String query = "";
  //String par = cod_asign +","+ rut_usuario;
 // query = "spu_dlt_rama_deportiva " + par;
// System.out.println("setEliminarRamaDeportiva: "+ query);

	 Connection con2 = con.conexion();
	 int error = -1;
		
  try{
	  CallableStatement cstmt = con2.prepareCall("{call spu_dlt_rama_deportiva (?,?,?)}");
	  cstmt.setInt(1, cod_asign); 
      cstmt.setInt(2, rut_usuario); 
      cstmt.registerOutParameter(3,Types.INTEGER);
	  cstmt.executeQuery();
	  error = cstmt.getInt(3);
	  cstmt.close();
  }
  catch(SQLException e){
    System.out.println("Error en Registro.setEliminarRamaDeportiva: "+e);
  }
  return error;
  }
  //**************************************************************************
// Metodo              : Permite inserta rama deportiva
//
// Programadora        : Monica B.
// Fecha creaci�n      : 8/6/2007
// Fecha modificaci�n  :
//****************************************************************************
  synchronized public int setInsertaRamaDeportiva(int cod_asign ,
                                                  int fun_rut,
                                                  int rut_usuario){
   // String query = "";
    //String par = cod_asign +","+ fun_rut +","+ rut_usuario ;
    //query = "EXEC spu_ins_rama_deportiva " + par;
// System.out.println("setInsertaRamaDeportiva: "+ query);
	
	Connection con2 = con.conexion();  
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_ins_rama_deportiva (?,?,?,?)}");
  	    cstmt.setInt(1, cod_asign); 
        cstmt.setInt(2, fun_rut); 
        cstmt.setInt(3, rut_usuario);
        cstmt.registerOutParameter(4,Types.INTEGER);
  	    cstmt.executeQuery();
  	    error = cstmt.getInt(4);
  	    //con2.close();
  	    cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Registro.setInsertaRamaDeportiva: "+e);
    }
    return error;
  }
  //**************************************************************************
// Metodo              : Permite actualiza rama deportiva
//
// Programadora        : Monica B.
// Fecha creaci�n      : 8/6/2007
// Fecha modificaci�n  :
//****************************************************************************
synchronized public int setActualizaRamaDeportiva(int cod_asign ,
                                                  int fun_rut,
                                                  int rut_usuario){
  /*String query = "";
  String par = cod_asign +","+ fun_rut +","+ rut_usuario ;
   query = "EXEC spu_upd_rama_deportiva " + par;*/
// System.out.println("setActualizaRamaDeportiva: "+ query);
	
  Connection con2 = con.conexion();  
  int error = -1;
  try{
	  CallableStatement cstmt = con2.prepareCall("{call spu_upd_rama_deportiva (?,?,?,?)}");
	  cstmt.setInt(1, cod_asign); 
      cstmt.setInt(2, fun_rut); 
      cstmt.setInt(3, rut_usuario);
      cstmt.registerOutParameter(4,Types.INTEGER);
	  cstmt.executeQuery();
	  error = cstmt.getInt(4);
	  //con2.close();
	  cstmt.close();
  }
  catch(SQLException e){
    System.out.println("Error en Registro.setActualizaRamaDeportiva: "+e);
  }
  return error;
  }

//**************************************************************************
// Metodo              : Permite eliminar registro
//
// Programadora        : Monica B.
// Fecha creaci�n      : 15/6/2007
// Fecha modificaci�n  :
//****************************************************************************
synchronized public int setEliminarRegistro(String id, int rut_usuario){
//String query = "";
//String par = id +","+ rut_usuario ;
// query = "EXEC spu_dlt_registro_gimnasio " + par;
//System.out.println("setEliminarRegistro; "+query);
Connection con2 = con.conexion();  
int error = -1;
try{
	CallableStatement cstmt = con2.prepareCall("{call spu_dlt_registro_gimnasio (?,?,?)}");
	cstmt.setLong(1, Long.parseLong(id)); 
    cstmt.setInt(2, rut_usuario); 
    cstmt.registerOutParameter(3,Types.INTEGER);
	cstmt.executeQuery();
	error = cstmt.getInt(3);
	//con2.close();
	cstmt.close();
}
catch(SQLException e){
  System.out.println("Error en Registro.setliminarRegistro: "+e);
}
return error;
}
//**************************************************************************
// Metodo              : Permite insertar registro
//
// Programadora        : Monica B.
// Fecha creaci�n      : 15/6/2007
// Fecha modificaci�n  :
//****************************************************************************
synchronized public int setInsertaRegistro(int rut ,
                                           int tipo,
                                           int rut_usuario,
                                           String rama_id,
                                           Vector vec_reserva,
                                           int sexo){

//String query = "";
//String par = rut +","+tipo +","+rut_usuario ;
  int error = -1;
  if (tipo != 2){

//par = par +","+rama_id+",0,"+sexo;
//query = "EXEC spu_ins_registro_gimnasio " + par;
	  //System.out.println("spu_ins_registro_gimnasio("+rut+","+tipo+","+rut_usuario+",'"+rama_id+"',0,"+sexo+",:error);");
	  Connection con2 = con.conexion();  
	  try{
		CallableStatement cstmt = con2.prepareCall("{call spu_ins_registro_gimnasio (?,?,?,?,?,?,?)}");
		cstmt.setInt(1, rut); 
		cstmt.setInt(2, tipo); 
		cstmt.setInt(3, rut_usuario);
		cstmt.setLong(4, Long.parseLong(rama_id));
		cstmt.setInt(5, 0);
		cstmt.setInt(6, sexo);
		cstmt.registerOutParameter(7,Types.INTEGER);
		cstmt.executeQuery();
		error = cstmt.getInt(7);
		//con2.close();
		cstmt.close();
	  }
	  catch(SQLException e){
	    System.out.println("Error en Registro.setInsertaRegistro: "+e.getMessage());
	    
	    return error;
	  }
  }else{
      if (vec_reserva.size() > 0){  // es de tutoria
          String fec_ter1 = null;
          String fec_ter2 = null;

           if(Integer.parseInt(vec_reserva.get(3)+"") == 0)
              fec_ter1 = null; // fecha_termino
           else
               fec_ter1 = vec_reserva.get(3)+"";// fecha_termino1
              if(Integer.parseInt(vec_reserva.get(9)+"") == 0)
                  fec_ter2 = null; // fecha_termino2
              else
                  fec_ter2 = vec_reserva.get(9)+"";// fecha_termino2
/*par = par +","+
			            vec_reserva.get(0) + "," + // año
			            vec_reserva.get(1) + ",'" + // semestre
			            vec_reserva.get(2) + "'," + // fecha_inicio1
			            fec_ter1 + "," + // fecha termino1
			            vec_reserva.get(4) + ",'" + // pergim_id1
			            vec_reserva.get(5) + "','" + // linea1
			            vec_reserva.get(6) + "','" + // linea2
			            vec_reserva.get(7) + "'," + // linea3
			            "2" + ",'" + // cod_cupo_tutor�a
			            vec_reserva.get(8) + "'," + // fecha_inicio2
			            fec_ter2 + "," + // fecha termino2
			            vec_reserva.get(10) + ",'" + // pergim_id2
			            vec_reserva.get(11) + "','" + // linea4
			            vec_reserva.get(12) + "','" + // linea5
			            vec_reserva.get(13) + "'" ; // linea6

              query = "EXEC spu_tran_registro_gimnasio " + par;
*/
              
              Connection con2 = con.conexion();  
        	  try{
        		CallableStatement cstmt = con2.prepareCall("{call spu_tran_registro_gimnasio (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
        		cstmt.setInt(1, rut); //Rut
        		cstmt.setInt(2, tipo); //Tipo
        		cstmt.setInt(3, rut_usuario); //Rut Usuario
        		cstmt.setInt(4, Integer.parseInt((String) vec_reserva.get(0))); //año
        		cstmt.setInt(5, Integer.parseInt((String) vec_reserva.get(1)));//semestre
        		cstmt.setString(6, (String) vec_reserva.get(2));//fecha_inicio1
        		cstmt.setString(7, fec_ter1);//fecha_termino1
        		cstmt.setLong(8, Long.parseLong((String) vec_reserva.get(4)));//pergim_id1
        		cstmt.setString(9, (String) vec_reserva.get(5));//linea1
        		cstmt.setString(10, (String) vec_reserva.get(6));//linea2
        		cstmt.setString(11, (String) vec_reserva.get(7));//linea3
        		cstmt.setInt(12, 2);//cod_cupo_tutor�a
        		cstmt.setString(13, (String) vec_reserva.get(8)); // fecha_inicio2
        		cstmt.setString(14, fec_ter2);// fecha termino2
        		cstmt.setLong(15, Long.parseLong((String) vec_reserva.get(10)));// pergim_id2
        		cstmt.setString(16, (String) vec_reserva.get(11));// linea4
        		cstmt.setString(17, (String) vec_reserva.get(12)); // linea5
        		cstmt.setString(18, (String) vec_reserva.get(13)); // linea6
        		cstmt.registerOutParameter(19,Types.INTEGER);//Parametro salida Error
        		cstmt.executeQuery();
        		error = cstmt.getInt(19);
        		//con2.close();
        		cstmt.close();
        	  }
        	  catch(SQLException e){
        	    System.out.println("Error en Registro.setInsertaRegistro: "+e.getMessage());
        	    return error;
        	  }
      }
  }

  return error;

/*
     int error = -1;

	try{
	  Statement sent = con.conexion().createStatement();
	  ResultSet res  = sent.executeQuery(query);
	  if(res.next()) error = res.getInt(1);
	  res.close();
	  sent.close();
	}catch(SQLException e){
	   System.out.println("Error en Registro.setInsertaRegistro: "+e); 
	}
*/	
}

//**************************************************************************
// Metodo              : Permite actualizar registro
//
// Programadora        : Monica B.
// Fecha creaci�n      : 15/6/2007
// Fecha modificaci�n  :
//****************************************************************************
synchronized public int setActualizarRegistro(int rut ,
                                           int tipo,
                                           int rut_usuario,
                                           String rama_id,
                                           String id,
                                           Vector vec_reserva){
  /*String query = "";
  String par = rut +","+
               tipo +","+
               rut_usuario +","+
               rama_id +","+
               id;*/
  int error = 0;
  
  if (vec_reserva.size() > 0)
  {
    String f1 = (Integer.parseInt(vec_reserva.get(2)+"") == 0)?null:""+vec_reserva.get(2)+"";
    String f2 = (Integer.parseInt(vec_reserva.get(3)+"") == 0)?null:""+vec_reserva.get(3)+"";
    String f3 = (Integer.parseInt(vec_reserva.get(8)+"") == 0)?null:""+vec_reserva.get(8)+"";
    String f4 = (Integer.parseInt(vec_reserva.get(9)+"") == 0)?null:""+vec_reserva.get(9)+"";
 
    /*
    par = par + ","+
               vec_reserva.get(0)+","+ // año
               vec_reserva.get(1)+","+ // semestre
               f1+","+ // fecha_inicio1
               f2+","+ // fecha_termino1
               vec_reserva.get(4) + ",'" + // pergim_id
               vec_reserva.get(5) + "','" + // linea1
               vec_reserva.get(6) + "','" + // linea2
               vec_reserva.get(7) + "'," + // linea3
               f3 + "," + // fecha_inicio2
               f4 + "," + // fecha_termino2
               vec_reserva.get(10) + ",'" + // pergim_id2
               vec_reserva.get(11) + "','" + // linea4
               vec_reserva.get(12) + "','" + // linea5
               vec_reserva.get(13) + "'"; // linea6
	*/
    Connection con2 = con.conexion();
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_upd_registro_gimnasio (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
    	cstmt.setInt(1,rut);
    	cstmt.setInt(2,tipo);
    	cstmt.setInt(3,rut_usuario);
    	cstmt.setLong(4, Long.parseLong(rama_id));
    	cstmt.setLong(5,Long.parseLong(id));
    	cstmt.setInt(6,Integer.parseInt( (String) vec_reserva.get(0)));//A�o
    	cstmt.setInt(7,Integer.parseInt( (String) vec_reserva.get(1)));//Semestre
    	cstmt.setString(8,f1);//fecha_inicio1
    	cstmt.setString(9,f2);// fecha_termino1
    	cstmt.setInt(10,Integer.parseInt((String) vec_reserva.get(4)));// pergim_id
    	cstmt.setString(11,(String)vec_reserva.get(5));// linea1
    	cstmt.setString(12,(String)vec_reserva.get(6));// linea2
    	cstmt.setString(13,(String)vec_reserva.get(7));// linea3
    	cstmt.setString(14,f3);//fecha_inicio2
    	cstmt.setString(15,f4);//fecha_termino2
    	cstmt.setInt(16,Integer.parseInt( (String) vec_reserva.get(10)));//pergim_id2
    	cstmt.setString(17,(String) vec_reserva.get(11));//linea4
    	cstmt.setString(18,(String) vec_reserva.get(12));//linea5
    	cstmt.setString(19,(String) vec_reserva.get(13));//linea6
    	cstmt.registerOutParameter(20, Types.INTEGER);//Parametro devuelto v_cod_error
    	cstmt.executeQuery();
		error = cstmt.getInt(20);
		//con2.close();
		cstmt.close();
    	
    }catch(Exception e){
    	System.out.println("Error en Registro.setActualizarRegistro: "+e.getMessage());
    	return error;
    }
    
    

  } else {
	  //par = par + ",0,0,null,null,0,' ',''";
	  Connection con2 = con.conexion();
	    try{
	    	CallableStatement cstmt = con2.prepareCall("{call spu_upd_registro_gimnasio (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
	    	cstmt.setInt(1,rut);
	    	cstmt.setInt(2,tipo);
	    	cstmt.setInt(3,rut_usuario);
	    	cstmt.setInt(4, Integer.parseInt(rama_id));
	    	cstmt.setInt(5,Integer.parseInt(id));
	    	cstmt.setInt(6,0);//A�o
	    	cstmt.setInt(7,0);//Semestre
	    	cstmt.setString(8,null);//fecha_inicio1
	    	cstmt.setString(9,null);// fecha_termino1
	    	cstmt.setInt(10,0);// pergim_id
	    	cstmt.setString(11, " ");// linea1
	    	cstmt.setString(12, " ");// linea2	
	    	cstmt.setString(13, " ");// linea3
	    	cstmt.setString(14, null);//fecha_inicio2
	    	cstmt.setString(15, null);//fecha_termino2
	    	cstmt.setInt(16,0);//pergim_id2
	    	cstmt.setString(17," ");//linea4
	    	cstmt.setString(18," ");//linea5
	    	cstmt.setString(19," ");//linea6
	    	cstmt.registerOutParameter(20, Types.INTEGER);//Parametro devuelto v_cod_error
	    	cstmt.executeQuery();
			error = cstmt.getInt(20);
			//con2.close();
			cstmt.close();
	    	
	    }catch(Exception e){
	    	System.out.println("Error en Registro.setActualizarRegistro: "+e.getMessage());
	    	return error;
	    }
  
  }


  // query = "EXEC spu_upd_registro_gimnasio " + par;
  // System.out.println("query"+query);

     
/*int error = -1;
try{
  Statement sent = con.conexion().createStatement();
  ResultSet res  = sent.executeQuery(query);
  if(res.next()) error = res.getInt(1);
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Registro.setActualizarRegistro: "+e); 
}
*/
return error;
}

//**************************************************************************
// Metodo              : Permite registrar la activacion
// Programadora        : Monica B.
// Fecha creaci�n      : 31/08/2007
// Fecha modificaci�n  :
//****************************************************************************
synchronized public int setActivacion(String tipo, int activo){
/*String query = "";
String par = "'"+tipo +"',"+ activo ;
 query = "EXEC spu_ins_activacion  " + par;*/
//System.out.println("ssetActivacion; "+query);
Connection con2 = con.conexion();  
int error = -1;
try{
	CallableStatement cstmt = con2.prepareCall("{call spu_ins_activacion (?,?,?)}");
	cstmt.setString(1, tipo); 
    cstmt.setInt(2, activo); 
    cstmt.registerOutParameter(3,Types.INTEGER);
	cstmt.executeQuery();
	error = cstmt.getInt(3);
	//con2.close();
	cstmt.close();
}
catch(SQLException e){
  System.out.println("Error en Registro.setActivacion: "+e);
}
return error;
}

}
