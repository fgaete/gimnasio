package gimnasio;
import java.io.*;
import java.util.*;
import java.sql.*;

public class Datos_alumno {
  private Conexion_gim con;

public Datos_alumno () {
}

public Datos_alumno (Conexion_gim con) {
  this.con = con;
  }

//**************************************************************************
// Metodo              : Permite capturar el año actual
// Programadora        : Monica B.
// Fecha creacion      : 19/11/2010
// Fecha modificacion  :
//****************************************************************************

  public int getañoActual(){
    int añoActual = 0;
   String query = " SELECT TO_CHAR(SYSDATE,'yyyymmdd')";
    try {
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     while (res.next()) {
       añoActual =Integer.parseInt(res.getString(1).substring(0,4));
     }
     res.close();
     sent.close();
   }

   catch (SQLException e ) {
     System.out.println("Error en Datos_alumno.getañoActual : " + e );
   }
  return añoActual;
}
//**************************************************************************
//Metodo              : Permite capturar el año del periodo semestre actual
//Programadora        : Monica B.
//Fecha creacion      : 4/01/2012
//Fecha modificacion  :
//****************************************************************************

 public int getañoPeriodoActual(){
   int añoActual = 0;
 /* String query = " SELECT per_año FROM periodo_semestre" +
  				 " WHERE sed_cod_sede = 1" +
  				 " AND cod_jornada = 1" +
  				 " AND per_vigencia = 1";
  MB 14/03/2012 debido a que en estos momentos se est� en preinscripcion se tiene que cambiar a per_vigencia_mant,
  ya que el per_vigencia apunta al periodo de inscripciones y per_vigencia_mant apunta al periodo real actual
  esto lo indic� Ximena celis
  *
  */
   String query = " SELECT per_año FROM periodo_semestre" +
				  " WHERE sed_cod_sede = 1" +
				  " AND cod_jornada = 1" +
				  " AND per_vigencia_mant = 1";
   try {
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    while (res.next()) {
      añoActual =res.getInt(1);
    }
    res.close();
    sent.close();
  }

  catch (SQLException e ) {
    System.out.println("Error en Datos_alumno.getañoPeriodoActual : " + e );
  }
 return añoActual;
}
  //**************************************************************************
// Metodo              : Permite capturar los datos del recepcionista (alumno) del
//                       gimnasio
// Programadora        : Monica B.
// Fecha creacion      : 5/6/2007
// Fecha modificacion  :
//****************************************************************************
  public Vector getAlumno (int rut_recep){
  java.util.Vector v = new java.util.Vector();
  String query = " SELECT a.alu_rut, a.alu_nombres || ' ' || a.alu_ap_paterno || ' ' || NVL(a.alu_ap_materno,' ')," +
                 " a.alu_fono_personal ,  a.alu_dir_personal || ', ' || nom_comuna, a.alu_email_personal," +
                 " p.nom_nacionalidad, TO_CHAR(a.alu_rut)||'-'||a.alu_dig_rut" +
                 " , a.alu_nombres, a.alu_ap_paterno, NVL(a.alu_ap_materno,' '),  " +
                 " a.alu_sexo, a.alu_dig_rut" +
                 " FROM alumno a, comuna c, pais p" +
                 " WHERE a.alu_rut = "+ rut_recep +
                 " AND a.cod_comuna_personal = c.cod_comuna" +
                 " AND a.cod_pais_nacionalidad = p.cod_pais";
 // System.out.println("getAlumno: "+query);
  Vector vec_carrera = new Vector();
  Vector vec_matricula = new Vector();
  Vector vec_rol = new Vector();
  String carrera = "";
  String calidad = "";
  String ingreso = "";
  boolean habilitado = false;
  int cod_calidad = 0;
  int año = 0;
  int semestre = 0;
  int cod_carrera = 0;
  int cod_mencion = 0;
  int cod_sede = 0;
  int cod_sede_alumno = 0;
  int añoActual = getañoPeriodoActual();
 // System.out.println("query: "+query +"añoActual: "+añoActual);
   try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    while(res.next()){
      vec_carrera = getCarreraAlumno(rut_recep);
      if (vec_carrera.size()>0) {
        for(int indice = 0;indice < vec_carrera.size();indice++){
          año = 0;
          semestre = 0;
          /*ahora se muestran todas las carreras con sus calidades para ver si est� habilitado para alguna carrera,
           solicitado el 20/10/2008*/
        //  indice = vec_carrera.size() - 1;
          carrera += ( (java.util.Vector) vec_carrera.get(indice)).get(3) + "";
          if ( ( ( (java.util.Vector) vec_carrera.get(indice)).get(7) + "").
              trim().toUpperCase().equals("SIN MENCI�N"))
            carrera += ", " +
                ( (java.util.Vector) vec_carrera.get(indice)).get(1);
          else
            carrera += ", Mencion " +
                ( (java.util.Vector) vec_carrera.get(indice)).get(7) + ", " +
                ( (java.util.Vector) vec_carrera.get(indice)).get(1);
          cod_carrera = Integer.parseInt( ( (java.util.Vector) vec_carrera.get(
              indice)).get(4) + "");
          cod_mencion = Integer.parseInt( ( (java.util.Vector) vec_carrera.get(
              indice)).get(6) + "");
          cod_sede = Integer.parseInt( ( (java.util.Vector) vec_carrera.get(
              indice)).get(2) + "");
          cod_sede_alumno = Integer.parseInt( ( (java.util.Vector) vec_carrera.
                                               get(indice)).get(0) + "");

          vec_matricula = getUltimaMatricula(rut_recep, cod_carrera,
                                             cod_mencion,
                                             cod_sede, cod_sede_alumno);

          // calidad del alumno
          getCalidadAlumno(rut_recep, cod_carrera, cod_mencion, cod_sede,
                           cod_sede_alumno);
          calidad = getCalidadAlumnoNombre();

          cod_calidad = getCalidadAlumnoCodigo();

          if(!habilitado)
               habilitado = getHabilitado(cod_calidad);
          if ( /*habilitado && */vec_matricula.size() > 0) {
            año = Integer.parseInt(vec_matricula.get(1) + "");
            semestre = Integer.parseInt(vec_matricula.get(0) + "");

          }
          if(año>0 && semestre > 0)
              calidad += ", �ltima matr�cula " + año+"-"+semestre ;
          carrera += ", " + calidad+"<br>";
          }
      }
      boolean condicion = (añoActual == año)?true:false ;
      // esta condicion la agregu� por el caso especial del semestre 2-2011, pues est� vigente junto con 2012-1
      if(añoActual == 2011)
    	  condicion = (((añoActual + 1) == año) || (añoActual == año))?true:false ;
      //System.out.println("condicion "+condicion+"-"+añoActual+" == "+año);
      if(!condicion) {
    	  habilitado = false;
    	  condicion = true;
      }
      
      
      if(condicion /*&& (cod_sede_alumno == 1 || cod_sede_alumno == 2)*/){
      //  System.out.println("entra");
        vec_rol = getAlumnoRol(rut_recep, cod_sede_alumno);
        ingreso = getIngresoAlumno(rut_recep);
        v.addElement(res.getString(1)); // 0 - rut alumno
        v.addElement(res.getString(2)); // 1 - nombre completo
        v.addElement(res.getString(3)); // 2 - fono personal
        v.addElement(res.getString(4)); // 3 - direccion , comuna personal
        String mail = "";
        gimnasio.BeanUsuarios b = new gimnasio.BeanUsuarios();
        b.getAbrirConexion();
        b.setGenerarConexionUsuario();
        mail = b.getMailUsuario(rut_recep);
        b.getCerrarConexion();
        if (mail.trim().equals(""))
          v.addElement(res.getString(5)); // 4 - email personal
        else
          v.addElement(mail); // 4 - email personal
        v.addElement(carrera); // 5 - �ltima carrera vigente
        v.addElement(calidad); // 6 - �ltima calidad vigente
        if (año > 0 && semestre > 0)
          v.addElement(año + "-" + semestre); // 7 - año semestre �ltima matr�cula
        else
          v.addElement("0");
        if (habilitado)
          v.addElement("1"); // 8 - habilitado al sistema
        else
          v.addElement("2"); // 8 - no habilitado al sistema

        if (vec_rol.size() > 0) {
          v.addElement(vec_rol.get(0) + "-" + vec_rol.get(1)); // 9 - rol
        }
        else
          v.addElement(""); //9 - rol
        v.addElement(ingreso); // 10 - ingreso a USM
        v.addElement(res.getString(6)); // 11- nacionalidad
        v.addElement(res.getString(7)); // 12 - rut completo
        v.addElement(res.getString(8)); // 13 - nombres
        v.addElement(res.getString(9)); // 14 - paterno
        v.addElement(res.getString(10)); // 15 - materno
        if (res.getInt(11) == 1)
          v.addElement("M"); // 16 - sexo
        else
          v.addElement("F"); // 16 - sexo
        v.addElement(res.getString(12)); // 17 digito
        v.addElement("1"); // 18 sede
      }

    }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Datos_alumno.getAlumno: "+e);
  }
  return v;
  }
  //***********************************************************************
// Metodo      : obtiene la �ltima calidad de un alumno en esa carrera
// Programador : MM
// Fecha       : 03/05/2004
// Modificado  : MA 17/05/2006
//***********************************************************************

String nom_calidad = "";
int cod_calidad    = 0;
String fecha_calidad = "";

public String getCalidadAlumnoNombre(){
  return nom_calidad;
}
public int getCalidadAlumnoCodigo(){
  return cod_calidad;
}
public String getCalidadAlumnoFecha(){
return fecha_calidad;
}

public void getCalidadAlumno
(int rut_alumno, int cod_carrera, int cod_mencion,
int cod_sede, int cod_sede_alumno){

  nom_calidad   = "";
  cod_calidad   = 0;
  fecha_calidad = "";
  String query =       " SELECT ca.cod_calidad, c.nom_calidad, TO_CHAR(ca.cal_fecha,'dd/mm/yyyy')"+
                       " FROM calidad_alumno ca, calidad c"+
                       " WHERE alu_rut = " + rut_alumno +
                       " AND cod_carrera = " + cod_carrera +
                    // " AND cod_mencion = " + cod_mencion +
                       " AND sed_cod_sede = " + cod_sede +
                       " AND cod_sede_alumno = " + cod_sede_alumno +
                       " AND ca.cod_calidad = c.cod_calidad" +
                       " AND cal_fecha = (SELECT MAX(cal_fecha)" +
                       "                  FROM calidad_alumno" +
                       "                  WHERE alu_rut = "+ rut_alumno +
                       "                  AND cod_carrera = " + cod_carrera +
                    // "                  AND cod_mencion = " + cod_mencion +
                       "                  AND sed_cod_sede = " + cod_sede +
                       "                  AND cod_sede_alumno = "+ cod_sede_alumno +
                       "                  AND TO_CHAR(cal_fecha,'yyyymmdd') <= TO_CHAR(SYSDATE,'yyyymmdd'))";
  try {
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    while (res.next()) {
      cod_calidad = res.getInt(1);
      nom_calidad = res.getString(2);
      fecha_calidad = res.getString(3);
    }
    res.close();
    sent.close();
  }

  catch (SQLException e ) {
    System.out.println("Error en Datos_alumno.getCalidadAlumno : " + e );
  }
  }

  //***************************************************************************
  // Metodo      : obtiene las carreras que tiene vigente el alumno y todos los
  //               datos asociados a la carrera.
  // Programador : HC
  // Fecha       : 06/05/2004
  // Modificado  : 31/08/2004 (MM se agrega cod_clasificacion)
  //               21/05/2005 (MV se agrega car_fecha_ingreso dd/mm/año)
  //               28/02/2008 (MB se copi� desde insc_datos y adecu� a la necesidad)
  //***************************************************************************
  public Vector getCarreraAlumno (
       int rut_alumno){

     String query = " SELECT ca.car_paralelo_carrera,"+
                    " TO_CHAR(ca.car_fecha_ingreso,'yyyy') AS año_ingreso,"+
                    " s.sed_cod_sede, s.sed_nom_sede, cs.sed_cod_sede," +
                    " cs.abreviacion, cs.cod_carrera, cs.nom_carrera,"+
                    " cm.cod_mencion, cm.nom_mencion, ca.pla_cod_plan,"+
                    " cs.dep_cod_departamento, cm.cod_jornada," +
                    " cs.cod_clasificacion,"+
                    " TO_CHAR(ca.car_fecha_ingreso,'yyyymmdd'), "+
                    " ca.car_vigente" +
                    " FROM carrera_alumno ca, carrera_sede cs, "+
                    " carrera_mencion cm, "+
                    " sede s"+
                    " WHERE ca.alu_rut = " + rut_alumno +
                    " AND ca.cod_sede_alumno = s.sed_cod_sede "+
                    " AND ca.car_vigente = 1 " +
                    " AND ca.cod_sede_carrera = cs.sed_cod_sede "+
                    " AND ca.cod_carrera = cs.cod_carrera "+
                    " AND ca.cod_mencion = cm.cod_mencion "+
                    " AND ca.cod_sede_carrera = cm.sed_cod_sede "+
                    " AND cm.cod_carrera = ca.cod_carrera" +
                    " ORDER BY ca.car_fecha_ingreso ";
//System.out.println("getCarreraAlumno : "+ query);
     Vector vec_datacad = new Vector();
     Vector detalle = null;

     try {
       Statement sent = con.conexion().createStatement();
       ResultSet res = sent.executeQuery(query);
       int carr_ant = 0;
       int carr_act = 0;
       while (res.next()) {
         carr_act = Integer.parseInt(res.getString(7)+res.getString(3));
         if (carr_act != carr_ant) {
           detalle = new Vector();
           detalle.addElement(res.getString(3)); //  0  - cod_sede_alumno
           detalle.addElement(res.getString(4)); //  1  - nom_sede_alumno
           detalle.addElement(res.getString(5)); //  2  - cod_sede_carrera
           detalle.addElement(res.getString(6)); //  3  - abreviacion
           detalle.addElement(res.getString(7)); //  4  - cod_carrera
           detalle.addElement(res.getString(8)); //  5  - nom_carrera
           detalle.addElement(res.getString(9)); //  6  - cod_mencion
           detalle.addElement(res.getString(10));//  7  - nom_mencion
            vec_datacad.addElement(detalle);
         }

         carr_ant = Integer.parseInt(res.getString(7)+res.getString(3));
       }
       res.close();
       sent.close();

     }
     catch (SQLException e ) {
       System.out.println("Error en Datos_alumno.getCarreraAlumno : " + e );
     }
     return vec_datacad;
   }

   //***************************************************************************
    // Metodo      : obtiene el nombre y rut del alumno desde el reggim_id
    // Programador : MB
    // Fecha       : 15/04/2008
    //***************************************************************************
    public Vector getDatosAlumno (
         String reggim_id){
      String query = " SELECT 0, alu_rut as rut"+
                      " FROM registro_gimnasio_alumno "+
                      " WHERE reggim_id = " + reggim_id +
                      " UNION "+
                      " SELECT 1, regext_rut as rut " +
                      " FROM registro_gimnasio_externo  "+
                      " WHERE reggim_id = " + reggim_id ;

      Vector vec_alumno = new Vector();
        Vector vec = new Vector();

       try {
         Statement sent = con.conexion().createStatement();
         ResultSet res = sent.executeQuery(query);
         while (res.next()) {
           vec_alumno = new Vector();
           //if (res.getInt(1) == 0) // es de CC
           //{
             vec_alumno = getAlumno(res.getInt(2));
           //} else {
           //  gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
           //  b.getAbrirConexionViña();
           //  b.setGenerarConexionDatosViña();
           //  vec_alumno = b.getAlumno(res.getInt(2));
           //  b.getCerrarConexionViña();

          // }
           if (vec_alumno.size() > 0){
             vec = new Vector();
             vec.addElement(vec_alumno.get(12)); //  0  - rut completo
             vec.addElement(vec_alumno.get(13)); //  1  - nombre_alumno
             vec.addElement(vec_alumno.get(14)); //  2  - ap paterno alumno
             vec.addElement(vec_alumno.get(15)); //  3  - ap materno alumno
            }

           }
         res.close();
         sent.close();

       }
       catch (SQLException e ) {
         System.out.println("Error en Datos_alumno.getCarreraAlumno : " + e );
       }
       return vec;
   }

//**************************************************************************
// Metodo              : Permite capturar todas las calidades y carreras
//                       del alumno
// Programadora        : adaptado desde matricula Monica B.
// Fecha creacion      : 6/6/2007
// Fecha modificacion  :
//****************************************************************************
 public java.util.Vector getCalidadesAlu(int rut) {

   java.util.Vector v = new java.util.Vector();
   java.util.Vector a = null;
   String query       = "";

   query =
      " SELECT distinct "+
      " ca.alu_rut "+             // 0
      " ,ca.cal_fecha "+          // 1
      " ,ca.sed_cod_sede "+       // 2
      " ,s2.sed_nom_sede "+       // 3
      " ,ca.cod_carrera "+        // 4
      " ,cs.abreviacion "+        // 5
      " ,ca.cod_mencion "+        // 6
      " ,cm.nom_mencion "+        // 7
      " ,ca.cod_calidad "+        // 8
      " ,c.nom_calidad "+         // 9
      " ,ca.cod_sede_alumno "+    // 10
      " ,s.sed_nom_sede "+     // 11
      " FROM calidad_alumno ca,carrera_sede cs, "+
      " carrera_mencion cm, sede s, calidad c, sede s2 "+
      " WHERE ca.alu_rut = "+rut+
      " AND ca.sed_cod_sede = cs.sed_cod_sede "+
      " AND ca.cod_carrera = cs.cod_carrera "+
      " AND ca.sed_cod_sede = cm.sed_cod_sede "+
      " AND ca.cod_carrera = cm.cod_carrera "+
      " AND ca.cod_mencion = cm.cod_mencion "+
      " AND ca.cod_sede_alumno = s.sed_cod_sede "+
      " AND ca.cod_calidad = c.cod_calidad "+
      " AND ca.sed_cod_sede = s2.sed_cod_sede "+
      " AND TO_CHAR(ca.cal_fecha,'yyyymmdd') <= TO_CHAR(SYSDATE+1,'yyyymmdd')" +
      " ORDER BY ca.cal_fecha ";

//System.out.println("query calidad "+query);
   try{
     Statement sent =  con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);

     while (res.next()) {
      a = new java.util.Vector();
       a.addElement(res.getString(1)); // 0-rut alumno
       a.addElement(res.getString(2)); // 1-fecha
       a.addElement(res.getString(3)); // 2-sede de la carrera
       a.addElement(res.getString(4)); // 3-abreviacion de la sede
       a.addElement(res.getString(5)); // 4-carrera
       a.addElement(res.getString(6)); // 5-nombre carrera
       a.addElement(res.getString(7)); // 6-mencion
       a.addElement(res.getString(8)); // 7-nombre mencion
       a.addElement(res.getString(9)); // 8-calidad
       a.addElement(res.getString(10)); //9-nombre calidad
       a.addElement(res.getString(11)); //10-sede alumno
       a.addElement(res.getString(12)); //11-abreviacion de la sede del alumno

       v.addElement(a);
     }
     res.close();
     sent.close();
   }
   catch (SQLException e){
   System.out.println("Error Datos_alumno.getCalidadesAlu : " + e );}

   return v;
 }
//**************************************************************************
// Metodo              : Permite capturar año y semestre de �ltima matr�cula
//                       del alumno
// Programadora        : adaptado desde insc_datos Monica B.
// Fecha creacion      : 6/6/2007
// Fecha modificacion  :
//****************************************************************************

  public Vector getUltimaMatricula
  (int rut_alumno, int cod_carrera, int cod_mencion,
  int cod_sede, int cod_sede_alumno){

    String query = " SELECT m.mat_semestre, m.mat_año "+
                   " FROM matricula m " +
                   " WHERE m.alu_rut = "+ rut_alumno +
                   " AND m.cod_carrera = "+ cod_carrera +
                   " AND m.cod_mencion = "+ cod_mencion +
                   " AND m.cod_sede = "+ cod_sede +
                   " AND m.cod_sede_alumno = " + cod_sede_alumno +
                   " AND m.mat_estado_matricula IN (0,4,10)" +
                   " AND to_number(to_char(m.mat_año)||to_char(m.mat_semestre)) = " +
                   " ( SELECT MAX (to_number(to_char(m2.mat_año)|| to_char(m2.mat_semestre)))  " + 
                   " FROM matricula m2 " +              
                   " WHERE m2.alu_rut = m.alu_rut " +               
                   " AND m2.cod_carrera= m.cod_carrera " +                
                   " AND m2.cod_mencion= m.cod_mencion " +                    
                   " AND m2.cod_sede=m.cod_sede " +                  
                   " AND m2.cod_sede_alumno=m.cod_sede_alumno " +                   
                   " AND m2.mat_estado_matricula IN (0,4,10))";
    
    Vector vec_mat = new Vector();
//System.out.println("getUltimaMatricula: "+ query);
    try {
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if (res.next()) {
        vec_mat.addElement(res.getString(1));
        vec_mat.addElement(res.getString(2));
      }
      res.close();
      sent.close();
    }

    catch (SQLException e ) {
      System.out.println("Error en Datos_alumno.getUltimaMatricula : " + e );
    }
    return vec_mat;
  }
  //**************************************************************************
// Metodo              : Permite capturar la nombre del alumno usuario del sistema de
//                       gimnasio
// Programadora        : Monica B.
// Fecha creacion      : 6/6/2007
// Fecha modificacion  :
//****************************************************************************
  public Vector getNombreAlumno (int rut_usuario){
  java.util.Vector v = new java.util.Vector();
  String query = "SELECT a.alu_nombres, a.alu_ap_paterno, NVL(a.alu_ap_materno,' ') "+
                 "FROM alumno a "+
                 "WHERE a.alu_rut  = "+rut_usuario;
   try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    while(res.next()){
      v.addElement(res.getString(1)); // nombres
      v.addElement(res.getString(2)); // ap paterno
      v.addElement(res.getString(3)); // ap materno

    }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Datos_alumno.getNombreAlumno: "+e);
  }
  return v;
  }

  //**************************************************************************
// Metodo              : Permite la fecha de ingreso a la USM
// Programadora        : Monica B.
// Fecha creacion      : 27/6/2007
// Fecha modificacion  :
//****************************************************************************
  public String getIngresoAlumno (int rut_usuario){
  String fecha ="";
  String query = " SELECT TO_CHAR(ing_fecha,'dd/mm/yyyy') FROM ingreso_alumno" +
                 " WHERE alu_rut = " + rut_usuario +
                 " AND ing_fecha = (SELECT MAX(ing_fecha) FROM ingreso_alumno" +
                 " WHERE alu_rut = " + rut_usuario +") ";
   try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    if (res.next()){
      fecha = res.getString(1); // fecha de ingreso
    }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Datos_alumno.getIngresoAlumno: "+e);
  }
  return fecha;
  }
//**************************************************************************
// Metodo              : Permite habilitar a un alumno ingresar al sistema de
//                       gimnasio
// Programadora        : Monica B.
// Fecha creacion      : 7/6/2007
// Fecha modificacion  : 24/11/2009 MB ahora se permite ingreso a alumnos titulados con matricula vigente
//****************************************************************************
  public boolean getHabilitado (int cod_calidad){
    boolean habilitado = false;
/*
0	Sin informacion
1 HAB	Regular
2 HAB	regular jornada parcial
3 HAB	especial
4 HAB	Oyente
5 HAB	Egresado
6 HAB	Memorista
7 HAB	Titulado
8	Renuncia Universidad
9	Congelado (Retiro temporal)
10	Suspendido
11	Retiro Definitivo Acad�mico
12	Eliminado
13	Abandona Estudios
14	Eliminado por expulsi�n
15 HAB	100% cr�ditos aprobados sin pr�ctica
16 HAB	No ha solicitado egreso
17 HAB	de intercambio Internacional
18	con situacion acad�mica pendiente
19	Fallecido
20	Condicional
21	Situacion desconocida
22	Abandona Estudios
23	Cambio de Carrera para solicitud Certificado
24	Retiro Definitivo Voluntario
25 HAB	en intercambio
26 HAB	en intercambio nacional
27	No Regular
28	Retiro de Documentos
29 HAB	de intercambio nacional
30 HAB	Tesista
31	Graduado
*/

    // calidades que habilitan son 1,2,3,4,5,6,,15,16,17,25,26,29
    // MM - 10/09/09 se agrega calidad 30
    // MB - 24/11/2009 se agrega calidad 7

   if (cod_calidad == 1 || cod_calidad == 2 ||
       cod_calidad == 3 || cod_calidad == 4 ||
       cod_calidad == 5 || cod_calidad == 6 ||
       cod_calidad == 15 || cod_calidad == 16 ||
       cod_calidad == 17 || cod_calidad == 25 ||
       cod_calidad == 26 || cod_calidad == 29 ||
       cod_calidad == 30 || cod_calidad == 7) habilitado = true;
  return habilitado;
  }

//**************************************************************************
// Metodo              : Permite habilitar a un alumno para reservar
//                       gimnasio
// Programadora        : Monica B.
// Fecha creacion      : 27/7/2007
// Fecha modificacion  :
//****************************************************************************
  public boolean getHabilitadoReserva (int rut, int año, int semestre){
    boolean habilitado = false;
    String query = " SELECT m.alu_rut" +
                   " FROM matricula m" +
                   " WHERE m.alu_rut = " + rut +
                   " AND m.mat_año = " + año +
                   " AND m.mat_semestre = " + semestre +
                   " AND (m.mat_estado_matricula = 0 OR" +
                   " m.mat_estado_matricula = 4)" +
                   " UNION" +
                   " SELECT alu_rut" +
                   " FROM inscripcion_asignatura i " +
                   " WHERE i.pro_año = " + año +
                   " AND i.pro_semestre = " + semestre +
                   " AND i.alu_rut = " + rut +
                   " AND i.ina_situacion = 1";
    // esto se agrega excepcionalmente,pues el periodo vigente es 2011-2 y ya estan matriculados los alumnos 2012-1 
    if(semestre == 2 && año == 2011) {
    año = año + 1;
    semestre = 1;
    
    	query = query + " UNION SELECT m.alu_rut" +
        " FROM matricula m" +
        " WHERE m.alu_rut = " + rut +
        " AND m.mat_año = " + año + 
        " AND m.mat_semestre = " + semestre +
        " AND (m.mat_estado_matricula = 0 OR" +
        " m.mat_estado_matricula = 4)" +
        " UNION" +
        " SELECT alu_rut" +
        " FROM inscripcion_asignatura i " +
        " WHERE i.pro_año = " + año +
        " AND i.pro_semestre = " + semestre +
        " AND i.alu_rut = " + rut +
        " AND i.ina_situacion = 1";
    }
  //  System.out.println(query);
    try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     if (res.next()){
       habilitado = true; // habilitado
     }
    Vector vec_carrera = getCarreraAlumno(rut);
   if (vec_carrera.size()>0) {

     for(int indice=0;indice < vec_carrera.size();indice++){
       int cod_carrera = Integer.parseInt( ( (java.util.Vector) vec_carrera.get(
           indice)).get(4) + "");
       int cod_mencion = Integer.parseInt( ( (java.util.Vector) vec_carrera.get(
           indice)).get(6) + "");
       int cod_sede = Integer.parseInt( ( (java.util.Vector) vec_carrera.get(indice)).
                                       get(2) + "");
       int cod_sede_alumno = Integer.parseInt( ( (java.util.Vector) vec_carrera.get(
           indice)).get(0) + "");

       // calidad del alumno
       getCalidadAlumno(rut, cod_carrera, cod_mencion, cod_sede, cod_sede_alumno);
       cod_calidad = getCalidadAlumnoCodigo();
       if(!habilitado)
          habilitado = getHabilitado(cod_calidad);
     }
    }
     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Datos_alumno.getHabilitadoReserva: "+e);
   }
   return habilitado;
  }

//**************************************************************************
  // Metodo              : Estado de la asistencia del alumno
  // Programadora        : Monica B.
  // Fecha creacion      : 27/7/2007
  // Fecha modificacion  :

//**************************************************************************
  int getEstadoAsistencia_codigo = 0;
  String getEstadoAsistencia_nombre = "";
  public int getEstadoAsistenciaCod(){
    return  getEstadoAsistencia_codigo;
  }
  public String getEstadoAsistenciaNom(){
    return  getEstadoAsistencia_nombre;
  }

  public void getEstadoAsistencia (String id_reg, String id_per){
    getEstadoAsistencia_codigo = 0;
    getEstadoAsistencia_nombre = "";
    String query = " SELECT e.cod_asistencia, t.nom_asistencia" +
                   " FROM estado_asistencia_gimnasio e, tipo_asistencia t" +
                   " WHERE e.reggim_id = " + id_reg +
                   " AND e.pergim_id = " + id_per +
                   " AND e.cod_asistencia = t.cod_asistencia";
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if (res.next()){
        getEstadoAsistencia_codigo = res.getInt(1);
        getEstadoAsistencia_nombre = res.getString(2);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Datos_alumno.getEstadoAsistencia: "+e);
    }
  }


//**************************************************************************
// Metodo              : Permite capturar el �ltimo rol del alumno
//
// Programadora        : adaptacion de matricula Monica B.
// Fecha creacion      : 13/6/2007
// Fecha modificacion  :
//****************************************************************************
  public java.util.Vector getAlumnoRol(int rut, int sede)
  {
    String query = "";
    java.util.Vector v = new java.util.Vector();
    String qsede1 = "";
    String qsede2 = "";

    if (sede == 1 || sede == 2 || sede == 5 || sede == 6) {
      qsede1 = "(ar1.cod_sede_alumno = 1 OR" +
               " ar1.cod_sede_alumno = 2 OR" +
               " ar1.cod_sede_alumno = 5 OR" +
               " ar1.cod_sede_alumno = 6)";
      qsede2 = "(ar2.cod_sede_alumno = 1 OR" +
               " ar2.cod_sede_alumno = 2 OR" +
               " ar2.cod_sede_alumno = 5 OR" +
               " ar2.cod_sede_alumno = 6)";
    }
    else {
      qsede1 = "ar1.cod_sede_alumno = " + sede;
      qsede2 = "ar2.cod_sede_alumno = " + sede;
    }

    query = "SELECT ar1.rol, ar1.dig_rol FROM alumno_rol ar1" +
            " WHERE ar1.alu_rut = " + rut +
            " AND " + qsede1 +
            " AND ar1.fecha_rol = ( SELECT MAX(ar2.fecha_rol)" +
            "                       FROM   alumno_rol ar2" +
            "                       WHERE  ar2.alu_rut = " + rut +
            "                       AND   " + qsede2 +
            ")";

    try{
      Statement sent =  con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      // busca el �ltimo rol para la sede consultada
      if (res.next()) {
        v.addElement(res.getString(1));
        v.addElement(res.getString(2));
      }
      res.close();
      sent.close();
    }
    catch (SQLException e){
    System.out.println("Error Datos_alumno.getAlumnoRol : " + e );}
    return v;
  }


}
