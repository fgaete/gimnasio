package gimnasio;
import java.io.*;
import java.util.*;
import java.sql.*;

public class Usuario {
  private Conexion_gim con;

public Usuario() {
}

public Usuario(Conexion_gim con) {
  this.con = con;
  }
//**************************************************************************
// Metodo              : Permite capturar la clave del usuario del sistema de
//                       gimnasio
// Programadora        : Monica B.
// Fecha creacion      : 2/6/2007
// Fecha modificacion  :
//****************************************************************************
  public Vector getValidarUsuario (int rut_usuario){
    java.util.Vector v = new java.util.Vector();
    String query = " SELECT U.rut_usuario, clave "+
                   " FROM usuario AS U"+
                   " WHERE U.rut_usuario = "+rut_usuario;
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        v.addElement(res.getString(1));
        v.addElement(res.getString(2));
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Usuario.getValidarUsuario: "+e);
    }
    return v;
  }
//**************************************************************************
// Metodo              : Permite capturar el perfil del usuario del sistema de
//                       gimnasio
// Programadora        : Monica B.
// Fecha creacion      : 2/6/2007
// Fecha modificacion  :
//****************************************************************************
  public int getPerfilUsuario (int rut_usuario){
    int cod_perfil = 0;
    String query = " SELECT cod_perfil" +
                   " FROM perfil_usuario" +
                   " WHERE cod_sistema = 47" +
                   " AND rut_perfil_usuario = " + rut_usuario;
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if(res.next()){
        cod_perfil = res.getInt(1);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Usuario.getPerfilUsuario: "+e);
    }
    return cod_perfil;
  }

//**************************************************************************
// Metodo              : Permite capturar la nombre del usuario del sistema de
//                       gimnasio
// Programadora        : Monica B.
// Fecha creacion      : 2/6/2007
// Fecha modificacion  : 2/08/2009 MB debido a cambio estructura de funcionario
//****************************************************************************
  public Vector getFuncionario (int rut_usuario){
  java.util.Vector v = new java.util.Vector();
  String depto = "";
 /* String  query = " SELECT fun_rut, fun_dig_rut, fun_ap_paterno, fun_ap_materno, fun_nombres, "+
      " fun_sexo, CONVERT(VARCHAR,fun_fecha_nacimiento,103), fun_direccion," +
      " fun_fono, fun_celular, CONVERT(VARCHAR,fun_fecha_ingreso,103), " +
      " fun_estado_civil, fun_vigente, tjf.nom_jerarquia_funcionario, " +
      " tcgf.nom_contrato_funcionario, dep_nom_departamento, cod_jornada_funcionario," +
      " s.sed_nom_sede, tpf.nom_planta_funcionario, tcf.nom_cargo_funcionario," +
      " f.cod_contrato_funcionario" +
      " FROM funcionario f LEFT JOIN tipo_contrato_funcionario tcgf ON" +
      "      f.cod_contrato_funcionario = tcgf.cod_contrato_funcionario" +
      "      LEFT JOIN tipo_planta_funcionario tpf ON" +
      "       f.cod_planta_funcionario = tpf.cod_planta_funcionario" +
      "                  LEFT JOIN tipo_cargo_funcionario tcf ON" +
      "                   f.cod_cargo_funcionario = tcf.cod_cargo_funcionario" +
      "    AND f.cod_planta_funcionario = tcf.cod_planta_funcionario" +
      "      LEFT JOIN tipo_jerarquia_funcionario tjf ON" +
      "      f.cod_jerarquia_funcionario = tjf.cod_jerarquia_funcionario, " +
      "departamento d, sede s" +
      " WHERE f.fun_rut = " + rut_usuario +
      " AND f.dep_cod_departamento = d.dep_cod_departamento" +
      " AND f.sed_cod_sede = s.sed_cod_sede ";*/

 String query =  "  SELECT f.fun_rut, f.fun_dig_rut, fun_ap_paterno, fun_ap_materno, fun_nombres," +
 				 "  fun_sexo, TO_CHAR(fun_fecha_nacimiento,'dd/mm/yyyy'), fun_direccion," +
				 "  fun_fono, TO_CHAR(fun_fecha_ingreso,'dd/mm/yyyy')," +
				 "  fun_estado_civil, fun_vigente,CASE when tjf.cod_jerarquia_funcionario = 0 then ' ' else tjf.nom_jerarquia_funcionario end ," +
				 "  tcgf.nom_contrato_funcionario, dep_nom_departamento, cod_jornada_funcionario,"+
				 "  s.sed_cod_sede, tpf.nom_planta_funcionario, tcf.nom_cargo_funcionario," +
				 "  f.cod_contrato_funcionario, s.sed_nom_sede" +
				 "  FROM funcionario f LEFT JOIN tipo_contrato_funcionario tcgf ON" +
				 "  f.cod_contrato_funcionario = tcgf.cod_contrato_funcionario" +
				 "  LEFT JOIN tipo_jerarquia_funcionario tjf ON" +
				 "  f.cod_jerarquia_funcionario = tjf.cod_jerarquia_funcionario," +
				 "  funcionario_cargo fc, tipo_cargo_funcionario tcf,tipo_planta_funcionario tpf," +
				 "  departamento d, sede s" +
				 "  WHERE f.fun_rut = " + rut_usuario +
				 "  AND f.fun_rut = fc.fun_rut" +
				 "  AND fc.dep_cod_departamento = d.dep_cod_departamento" +
				 "  AND fc.sed_cod_sede = s.sed_cod_sede " +
				 "  AND fc.cod_vigencia = 1" +
				 "  AND fc.funcar_principal = 1 "   +
				 "  AND fc.cod_planta_funcionario = tcf.cod_planta_funcionario" +
				 "  AND fc.cod_cargo_funcionario = tcf.cod_cargo_funcionario" +
				 "  AND fc.cod_planta_funcionario = tpf.cod_planta_funcionario";

 //System.out.println("getFuncionario query: "+ query);
 try{
   Statement sent = con.conexion().createStatement();
   ResultSet res  = sent.executeQuery(query);

   if(res.next()){
     v.addElement(res.getString(1)); // 0.  fun_rut
     v.addElement(res.getString(2)); // 1.  fun_dig_rut
     v.addElement(res.getString(3)); // 2.  fun_ap_paterno
     v.addElement(res.getString(4)); // 3.  fun_ap_materno
     v.addElement(res.getString(5)); // 4.  fun_nombres
     v.addElement(res.getString(6)); // 5.  fun_sexo
     v.addElement(res.getString(7)); // 6.  fun_fecha_nacimiento
     v.addElement(res.getString(8)); // 7.  fun_direccion
     v.addElement(res.getString(9)); // 8.  fun_fono
     v.addElement(res.getString(10)); // 9.  fun_fecha_ingreso
     v.addElement(res.getString(11)); // 10. fun_estado_civil
     v.addElement(res.getString(12)); // 11. fun_vigente
     v.addElement(res.getString(13)); // 12. nom_jerarquia_funcionario
     v.addElement(res.getString(14)); // 13. nom_contrato_funcionario
     v.addElement(res.getString(15)); // 14. nom_departamento
     v.addElement(res.getString(16)); // 15. cod_jornada_funcionario
     v.addElement(res.getString(17)); // 16. cod_sede
     v.addElement(res.getString(18)); // 17. nom_planta_funcionario
     v.addElement(res.getString(19)); // 18. nom_cargo_funcionario
     v.addElement(res.getString(20)); // 19. cod_contrato
     v.addElement(res.getString(21)); // 20. nom_sede
    }
   res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Usuario.getFuncionario: "+e);
  }
  return v;
  }

//**************************************************************************
// Metodo              : Permite capturar los datos del usuario del gimnasio:
//                       alumno, tutor�a, rama o funcionario
// Programadora        : Monica B.
// Fecha creacion      : 13/6/2007
// Fecha modificacion  :
//****************************************************************************


public Vector getUsuarioGimnasio (int rut){
java.util.Vector v = new java.util.Vector();

java.util.Vector vec_usuario = new java.util.Vector();
gimnasio.BeanGeneral general = new gimnasio.BeanGeneral();
general.getAbrirConexion();
general.setGenerarConexionDatosAl();
vec_usuario = general.getAlumno(rut);
general.getCerrarConexion();
if (vec_usuario.size() == 0 ||  // no es alumno
    (vec_usuario.size() > 0 &&  Integer.parseInt(v.get(8)+"")==2)) {
     // si no es alumno � es alumno y no est� habilitado, ver si es funcionario
    gimnasio.BeanUsuarios usuario = new gimnasio.BeanUsuarios();
    usuario.getAbrirConexion();
    vec_usuario = usuario.getFuncionario(rut);
    usuario.getCerrarConexion();
    v.addElement("1"); // es funcionario
} else v.addElement("2"); // es alumno

v.addElement(vec_usuario);

return v;
  }
//**************************************************************************
// Metodo              : Permite el departamento del funcionario
//
// Programadora        : Monica B.
// Fecha creacion      : 23/07/2007
// Fecha modificacion  :
//****************************************************************************


public String getDepartamentoCargo (int rut){
   String depto = "";
   String query  = "";
   String query1 = "";
   String query2 = "";
   String query3 = "";
   Vector v = new Vector();
   Vector a = new Vector();

   query1 = " SELECT ds.dep_cod_departamento, " +
            " d.dep_nom_departamento, ds.sed_cod_sede, '9', 0" +
            " FROM departamento_sede ds, departamento d" +
            " WHERE ds.fun_rut = " + rut +
            " AND ds.dep_cod_departamento = d.dep_cod_departamento";

   query2 = " SELECT DISTINCT cs.dep_cod_departamento, " +
            " d.dep_nom_departamento, cs.sed_cod_sede, '10', 0" +
            " FROM carrera_sede cs, departamento_sede ds, departamento d" +
            " WHERE cs.fun_rut = " + rut +
            " AND cs.dep_cod_departamento = ds.dep_cod_departamento" +
            " AND ds.dep_cod_departamento = d.dep_cod_departamento";

   query3 = " SELECT c.dep_cod_departamento, " +
            " d.dep_nom_departamento, c.sed_cod_sede, '0', c.cod_cargo " +
            " FROM cargo_funcionario c, departamento_sede ds, departamento d" +
            " WHERE c.fun_rut = " + rut +
            " AND c.dep_cod_departamento = ds.dep_cod_departamento" +
            " AND ds.dep_cod_departamento = d.dep_cod_departamento" +
            " AND c.fecha_termino IS NULL";

   query = query1 + " UNION " + query2 + " UNION " + query3;

   try{
     Statement sent = con.conexion().createStatement();
     ResultSet res  = sent.executeQuery(query);
     while (res.next()) {
       depto = res.getString(2); // dep_nom_departamento

     }
     res.close();
     sent.close();
   }
   catch (SQLException e){
   System.out.println("Error Usuario.getDepartamentoCargo: "+e );}


  return depto;

  }

  //**************************************************************************
// Metodo              : Permite capturar el mail de usuarios tanto alumnos como
//                       funcionarios
// Programadora        : Monica B.
// Fecha creacion      : 20/6/2007
// Fecha modificacion  :
//****************************************************************************


public String getMailUsuario (int rut){
  String mail = "";
  String query = " SELECT usucas_casilla || '@' || tc.nom_casilla" +
                 " FROM usuario_casilla uc, tipo_casilla tc" +
                 " WHERE uc.rut_usuario = " + rut +
                 " AND uc.cod_casilla = tc.cod_casilla";
  try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    if(res.next()){
      mail = res.getString(1);
    }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Usuario.getMailUsuario: "+e);
  }
  return mail;
  }

  synchronized public int setInsertarPerfilUsuario(int alu_rut,
                                                   int rut_usuario,
                                                   int cod_perfil){
     String query = "";
    // String par = "47," + alu_rut + "," + cod_perfil + "," + rut_usuario;
    // query = "spu_ins_perfil_usuario "+ par;
     Connection con2 = con.conexion();
     int error = -1;
     try{
    	 CallableStatement cstmt = con2.prepareCall("{call spu_ins_perfil_usuario (?,?,?,?,?)");
    	 cstmt.setInt(1,47);
    	 cstmt.setInt(2,alu_rut);
    	 cstmt.setInt(3,cod_perfil);
    	 cstmt.setInt(4,rut_usuario);
    	 cstmt.registerOutParameter(5,Types.INTEGER);
    	 cstmt.executeQuery();
    	 error = cstmt.getInt(5);
    	 cstmt.close();
     }
     catch(SQLException e){
       System.out.println("Error en Usuario.setInsertarPerfilUsuario: "+e);
     }
     return error;
  }
  synchronized public int setBorrarPerfilUsuario(int alu_rut){
    String query = "";
    //query = "spu_dlt_perfil_usuario 47,"+ alu_rut ;
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_dlt_perfil_usuario (?,?,?)");
    	cstmt.setInt(1,47);
    	cstmt.setInt(2,alu_rut);
   	    cstmt.registerOutParameter(3,Types.INTEGER);
   	    cstmt.executeQuery();
   	    error = cstmt.getInt(3);
   	    cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Usuario.setBorrarPerfilUsuario: "+e);
    }
    return error;
  }
}
