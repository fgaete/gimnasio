package gimnasio;
import java.io.*;
import java.util.*;
import java.sql.*;


/**
 * <p>Title: </p>
 * <p>Description: Coneccion a la BD</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: UTFSM </p>
 * @author M�nica Barrera
 * @fecha  2 de junio de 2007
 * @version 1.0
 * �ltima modificacion : 12 de junio de 2007
 */

public class Horario {
  private Conexion_gim con;

public Horario() {
}

public Horario(Conexion_gim con) {
this.con = con;
  }

//**************************************************************************
// Metodo              : Permite capturar las actividades del sistema de
//                       gimnasio
// Programadora        : M�nica B.
// Fecha creacion      : 7/6/2007
// Fecha modificacion  :
//****************************************************************************
  public Vector getActividad (){
    java.util.Vector v = new java.util.Vector();
    java.util.Vector a = new java.util.Vector();
    String query = " SELECT cod_asign, nom_asign" +
                   " FROM asignatura" +
                   " WHERE tipo_asignatura = 8"+
                   " AND estado_nulo = 1 " +
                   " AND cod_asign <> 1410030" +
                   " ORDER BY nom_asign";
   // System.out.println("getActividad: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        a = new java.util.Vector();
        a.addElement(res.getString(1)); // cod_asign
        a.addElement(res.getString(2)); // nom_asign
        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Horario.getActividad: "+e);
    }
    return v;
  }
//**************************************************************************
// Metodo              : Permite capturar las actividades del sistema de
//                       gimnasio
// Programadora        : M�nica B.
// Fecha creacion      : 7/6/2007
// Fecha modificacion  :
//****************************************************************************
  public Vector getMantActividad (int año, int semestre){
    java.util.Vector v = new java.util.Vector();
    java.util.Vector a = new java.util.Vector();
    String query = " SELECT DISTINCT a.cod_asign, a.nom_asign" +
                   " FROM programacion p, asignatura a " +
                   " WHERE p.pro_año = " + año +
                   " AND p.pro_semestre = " + semestre +
                   " AND p.cod_asign = a.cod_asign" +
                   " AND a.tipo_asignatura = 8"+
                   " AND a.estado_nulo = 1 " +
                   " AND a.cod_asign <> 1410030" +
                   " UNION" +
                   " SELECT DISTINCT a.cod_asign, a.nom_asign" +
                   " FROM programacion p, asignatura a " +
                   " WHERE p.pro_año = " + año +
                   " AND p.pro_semestre = " + semestre +
                   " AND p.cod_asign = a.cod_asign" +
                   " AND a.cod_asign IN (1410020,1410017)"+
                   " AND a.estado_nulo = 1 " +
                   " ORDER BY 2";
//System.out.println("getMantActividad: "+ query);

    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        a = new java.util.Vector();
        a.addElement(res.getString(1)); // cod_asign
        if (res.getInt(1) == 1410020 || res.getInt(1) == 1410017)
          a.addElement(res.getString(2)+ " (Docencia)"); // nom_asign
        else
          a.addElement(res.getString(2)); // nom_asign
        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Horario.getActividad: "+e);
    }
    return v;
  }

//**************************************************************************
// Metodo              : Permite capturar los bloques de las salas del
//                       gimnasio
// Programadora        : M�nica B.
// Fecha creacion      : 8/6/2007
// Fecha modificacion  :
//****************************************************************************
 public Vector getHoraBloque (int num_sala,
                              int año,
                              int semestre){

   java.util.Vector v = new java.util.Vector();
   java.util.Vector a = new java.util.Vector();
   String query = " SELECT hb.cod_bloque, " +
                  " hb.hora_inicio, hb.hora_termino, horblo_id," +
                  " hbs.horblo_secuencia" +
                  " FROM hora_bloque hb, hora_bloque_sala hbs" +
                  " WHERE hb.sed_cod_sede = hbs.sed_cod_sede" +
                  " AND hb.cod_jornada = hbs.cod_jornada" +
                  " AND hb.cod_bloque = hbs.cod_bloque" +
                  " AND hb.cod_asignacion = 2" +
                  " AND hbs.num_sala = " + num_sala +
                  " AND hbs.horblo_fecha_termino is null" +
                  " AND hbs.horblo_año = " + año +
                  " AND hbs.horblo_semestre = " + semestre +
                  " ORDER BY hbs.horblo_secuencia";
//System.out.println("getHoraBloque: "+ query);
   try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     while(res.next()){
       a = new java.util.Vector();
       a.addElement(res.getString(1)); // 0 -cod_bloque
       a.addElement(res.getString(2)); // 1 -hora_inicio
       a.addElement(res.getString(3)); // 2 -hora_termino
       a.addElement(res.getString(4)); // 3 -horblo_id
       a.addElement(res.getString(5)); // 4 secuencia
       v.addElement(a);
     }
     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Horario.getHoraBloque: "+e);
   }
   return v;
  }

//**************************************************************************
// Metodo              : Permite capturar el horario de la sala, mostrando los
//                       datos necesarios del horario
// Programadora        : M�nica B.
// Fecha creacion      : 3/7/2007
// Fecha modificacion  : 09/05/2008 se agreg� fecha de vigencia de horario_asignatura,
//                       solicitado por Jorge M
//****************************************************************************
 public Vector getHorarioSala (int año, int semestre, int num_sala){
   java.util.Vector v = new java.util.Vector();
   java.util.Vector a = new java.util.Vector();
   String query = "";
   if(año > 0 && semestre > 0 && num_sala > 0){
     if (num_sala == 10335)
       query = " SELECT ha.num_dia, ha.cod_bloque_inicio, cod_bloque_termino," +
               " tr.nom_reserva, SUBSTR(f.fun_nombres,1,1) || '. '|| f.fun_ap_paterno," +
	           " ha.cod_reserva, ' ' , ha.cod_asign, p.pro_cupo, ha.pro_paralelo_asignatura, p.pro_sobrecupo" +
	           " FROM horario_asignatura ha, tipo_reserva  tr, programacion p, " +
	           " programacion_profesor pp, funcionario f " +
	           " WHERE ha.pro_año = " + año +
	           " AND ha.pro_semestre = " + semestre +
	           " AND ha.cod_jornada = 1" +
	           " AND ha.sed_cod_sede = 1" +
	           " AND ha.num_sala = " + num_sala +
	           " AND ha.horasi_fecha_termino is NULL" +
	           " AND ha.cod_motivo = 6" +
	           " AND ha.cod_reserva = tr.cod_reserva" +
	           " AND p.pro_año = " + año +
	           " AND p.pro_semestre = " + semestre +
	           " AND ha.cod_asign = p.cod_asign " +
	           " AND ha.pro_paralelo_asignatura = p.pro_paralelo_asignatura" +
	           " AND pp.pro_año = p.pro_año" +
	           " AND pp.pro_semestre = p.pro_semestre" +
	           " AND pp.cod_asign = p.cod_asign" +
	           " AND pp.pro_paralelo_asignatura = p.pro_paralelo_asignatura" +
	           " AND pp.fun_rut = f.fun_rut" +
	           " ORDER BY ha.num_dia, ha.cod_bloque_inicio";
     else
       query =
           " SELECT ha.num_dia, ha.cod_bloque_inicio, cod_bloque_termino, a.nom_asign," +
           " SUBSTR(f.fun_nombres,1,1) || '. '|| f.fun_ap_paterno, " +
           " ha.cod_reserva, tr.nom_reserva , ha.cod_asign, p.pro_cupo, ha.pro_paralelo_asignatura," +
           " p.pro_sobrecupo" +
           " FROM horario_asignatura ha, programacion p, " +
           " programacion_profesor pp, funcionario f, asignatura a , tipo_reserva  tr" +
           " WHERE ha.pro_año = " + año +
           " AND ha.pro_semestre = " + semestre +
           " AND ha.cod_jornada = 1" +
           " AND ha.sed_cod_sede = 1" +
           " AND ha.num_sala = " + num_sala +
           " AND ha.horasi_fecha_termino is NULL" +
           " AND ha.cod_motivo = 6" +
           " AND ha.cod_reserva = tr.cod_reserva" +
           " AND p.pro_año = " + año +
           " AND p.pro_semestre = " + semestre +
           " AND ha.cod_asign = p.cod_asign " +
           " AND ha.pro_paralelo_asignatura = p.pro_paralelo_asignatura" +
           " AND pp.pro_año = p.pro_año" +
           " AND pp.pro_semestre = p.pro_semestre" +
           " AND pp.cod_asign = p.cod_asign" +
           " AND pp.pro_paralelo_asignatura = p.pro_paralelo_asignatura" +
           " AND pp.fun_rut = f.fun_rut" +
           " AND ha.cod_asign = a.cod_asign" +
           " ORDER BY ha.num_dia, ha.cod_bloque_inicio";
//System.out.println("*********getHorarioSala; "+query);
     try {
       Statement sent = con.conexion().createStatement();
       ResultSet res = sent.executeQuery(query);
       while (res.next()) {
         a = new java.util.Vector();
         a.addElement(res.getString(1)); // 0 - num dia
         a.addElement(res.getString(2)); // 1 - cod_bloque inicio
         a.addElement(res.getString(3)); // 2 - cod_bloque termino
         a.addElement(res.getString(4)); // 3 - nom_asign / tipo-reserva
         a.addElement(res.getString(5)); // 4 - profesor a cargo
         a.addElement(res.getString(6)); // 5 - cod_reserva
         a.addElement(res.getString(7)); // 6 - nom tipo reserva
         a.addElement(res.getString(8)); // 7 - cod_asign
         a.addElement(res.getString(9)); // 8 - cupo
         a.addElement(res.getString(10)); // 9 - paralelo
         a.addElement(res.getString(11)); // 10- rama
         v.addElement(a);
       }
       res.close();
       sent.close();
     }
     catch (SQLException e) {
       System.out.println("Error en Horario.getHorarioSala: " + e);
     }
   }
   return v;
  }
//**************************************************************************
// Metodo              : Permite capturar el horario del Aseo
// Programadora        : M�nica B.
// Fecha creacion      : 3/7/2007
// Fecha modificacion  : 26/03/2008  agregar comparacion con hora_bloque_sala
//****************************************************************************
 public Vector getHorarioAseo (int num_sala, int año, int semestre){
   java.util.Vector v = new java.util.Vector();
   java.util.Vector a = new java.util.Vector();
   int blo_inicio = 0;
   int blo_fin = 0;
   String query = "";
//   query = " SELECT asegim_dia_inicio, asegim_dia_termino, ag.horblo_inicio_id," +
//           " ag.horblo_termino_id, ag.asegim_id, TO_CHAR(ag.asegim_fecha_inicio,'dd/mm/yyyy')" +
//           " FROM aseo_gimnasio ag" +
//           " WHERE ag.asegim_fecha_termino is null" ;
   query = " SELECT asegim_dia_inicio, asegim_dia_termino, hbs.cod_bloque," +
           " hbs2.cod_bloque, ag.asegim_id, TO_CHAR(ag.asegim_fecha_inicio,'dd/mm/yyyy')" +
           " FROM aseo_gimnasio ag, periodo_semestre p, hora_bloque_sala hbs," +
           " hora_bloque_sala hbs2" +
           " WHERE p.per_año = " + año +
           " AND p.per_semestre = " + semestre +
           " AND p.sed_cod_sede = 1" +
           " AND p.cod_jornada = 1" +
           " AND ((ag.asegim_fecha_inicio >= p.per_fecha_inicio" +
           " AND ag.asegim_fecha_inicio <= p.per_fecha_termino ) OR" +
           " ( NVL(ag.asegim_fecha_termino,SYSDATE) >= p.per_fecha_inicio" +
           " AND NVL(ag.asegim_fecha_termino,SYSDATE) <= p.per_fecha_termino  ))"+
           " AND ag.horblo_inicio_id = hbs.horblo_id" +
           " AND hbs.horblo_año = " + año +
           " AND hbs.horblo_semestre = " + semestre +
           " AND hbs.sed_cod_sede = 1" +
           " AND hbs.cod_jornada = 1" +
           " AND hbs.num_sala = " + num_sala +
           " AND ag.horblo_termino_id = hbs2.horblo_id" +
           " AND hbs2.horblo_año = " + año +
           " AND hbs2.horblo_semestre = " + semestre +
           " AND hbs2.sed_cod_sede = 1" +
           " AND hbs2.cod_jornada = 1" +
           " AND hbs2.num_sala = " + num_sala +
           " AND ag.asegim_fecha_termino is null";
//System.out.println("getHorarioAseo: "+ query);
    try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     while(res.next()){
       a = new java.util.Vector();
       a.addElement(res.getString(1)); // 0 -dia inicio
       a.addElement(res.getString(2)); // 1 -dia termino
       a.addElement(res.getString(3)); // 2 -bloque_inicio
       a.addElement(res.getString(4)); // 3 -bloque_termino
       a.addElement(res.getString(5)); // 4 id del aseo
       a.addElement(res.getString(6)); // 5 fecha
       a.addElement(getSecuenciaBloque (num_sala, año, semestre,Integer.parseInt(res.getString(3))));//6 secuencia inicio
       a.addElement(getSecuenciaBloque (num_sala, año, semestre,Integer.parseInt(res.getString(4))));//7 secuencia termino
       v.addElement(a);

     }
     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Horario.getHorarioAseo: "+e);
   }
   return v;
  }
  //**************************************************************************
// Metodo              : Permite capturar el horario de la sala y periodo
// Programadora        : M�nica B.
// Fecha creacion      : 10/7/2007
// Fecha modificacion  : 12/05/2008 se agreg� fecha de vigencia de horario_asignatura,
//                       solicitado por Jorge M
//****************************************************************************
 public Vector getHorario ( int año, int semestre, int num_sala){
   java.util.Vector v = new java.util.Vector();
   java.util.Vector a = new java.util.Vector();
   java.util.Vector blo_inicio = new java.util.Vector();
   java.util.Vector blo_fin = new java.util.Vector();
   String b1 = "";
   String b2 = "";
   String dia = "";
   String query = "";

   query = " SELECT cod_asign, pro_paralelo_asignatura, num_dia, cod_bloque_inicio," +
           " cod_bloque_termino, cod_reserva" +
           " FROM horario_asignatura " +
           " WHERE pro_año = " + año +
           " AND   pro_semestre = " + semestre +
           " AND   cod_jornada = 1"+
           " AND   sed_cod_sede = 1" +
           " AND   num_sala = " + num_sala +
           " AND   horasi_fecha_termino is NULL";

//System.out.println(query);
    try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     while(res.next()){
       a = new java.util.Vector();
       a.addElement(res.getString(1)); // 0 - cod_asign
       a.addElement(res.getString(2)); // 1 - paralelo
       a.addElement(res.getString(3)); // 2 - num_dia
       a.addElement(res.getString(4)); // 3 - cod_bloque_inicio
       a.addElement(res.getString(5)); // 4 - cod_bloque_termino
       blo_inicio = getBloqueCodigoSede(1, 1,res.getInt(4));
       blo_fin = getBloqueCodigoSede(1, 1, res.getInt(5));

       /*cambi� para que se muestre la primera hora y la �ltima*/
//       b1 = (blo_inicio.size()>0)?blo_inicio.get(0)+"":"";
//       b2 =  (blo_inicio.size()>0)?blo_inicio.get(1)+"":"";
//       a.addElement(b1+"-"+b2); // 5 -bloque_inicio
//       b1 = (blo_fin.size()>0)?blo_fin.get(0)+"":"";
//       b2 =  (blo_fin.size()>0)?blo_fin.get(1)+"":"";
//       a.addElement(b1+"-"+b2); // 6 -bloque_termino
       b1 = (blo_inicio.size()>0)?blo_inicio.get(0)+"":"";
       a.addElement(b1); // 5 -bloque_inicio
       b2 =  (blo_fin.size()>0)?blo_fin.get(1)+"":"";
       a.addElement(b2); // 6 -bloque_termino
       switch (res.getInt(3)){
         case 1: dia = "Lu";
           break;
         case 2: dia = "Ma";
           break;
         case 3: dia = "Mi";
           break;
         case 4: dia = "Ju";
           break;
         case 5: dia = "Vi";
           break;
         case 6: dia = "S�";
           break;
       }
       a.addElement(dia); // 7 - dia con palabra
       a.addElement(res.getString(6)); // 8 - cod_reserva
       a.addElement(getSecuenciaBloque (num_sala, año, semestre,Integer.parseInt(res.getString(4))));//9 - Sequencia Inicio
       a.addElement(getSecuenciaBloque (num_sala, año, semestre,Integer.parseInt(res.getString(5))));//10 - Sequencia termino
       v.addElement(a);
     }
     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Horario.getHorario: "+e);
   }
   return v;
  }
//**************************************************************************
// Metodo              : Permite capturar el horario de la sala y periodo y bloque
// Programadora        : M�nica B.
// Fecha creacion      : 10/7/2007
// Fecha modificacion  : 01/04/2008 debido a que se agregaron los campos:
//                       año, semestre y secuencia a hora_bloque_sala
// Fecha modificacion  : 09/05/2008 se agreg� fecha de vigencia de horario_asignatura,
//                       solicitado por Jorge M
//****************************************************************************
public Vector getHorarioBloque ( int año, int semestre, int num_sala, int dia,
                                 int bloque1, int bloque2){
 java.util.Vector v = new java.util.Vector();
 String query = "";
 int sec_inicio  = getSecuenciaBloque( num_sala, año, semestre, bloque1);
 int sec_termino = getSecuenciaBloque( num_sala, año, semestre, bloque2);
 query = " SELECT cod_asign, pro_paralelo_asignatura" +
         " FROM horario_asignatura ha " +
         " , hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
         " WHERE ha.pro_año = " + año +
         " AND   ha.pro_semestre = " + semestre +
         " AND   ha.cod_jornada = 1"+
         " AND   ha.sed_cod_sede = 1" +
         " AND   ha.num_sala = " + num_sala +
         " AND   ha.num_dia = "+ dia +
         " AND   ha.horasi_fecha_termino is NULL" +
         " AND   ha.cod_bloque_inicio = hbs.cod_bloque" +
         " AND   hbs.horblo_año =  " + año +
         " AND   hbs.horblo_semestre = " + semestre +
         " AND   hbs.num_sala = " + num_sala +
         " AND   hbs.sed_cod_sede = 1" +
         " AND   hbs.cod_jornada = 1" +
         " AND   hbs.horblo_fecha_termino is null" +
         " AND   ha.cod_bloque_termino = hbs2.cod_bloque" +
         " AND   hbs2.horblo_año =  " + año +
         " AND   hbs2.horblo_semestre = " + semestre +
         " AND   hbs2.num_sala = " + num_sala +
         " AND   hbs2.sed_cod_sede = 1" +
         " AND   hbs2.cod_jornada = 1" +
         " AND   hbs2.horblo_fecha_termino is null" +
         " AND   ((" + sec_inicio + " >= hbs.horblo_secuencia " +
         " AND   " + sec_inicio + " <= hbs2.horblo_secuencia) or" +
         " (  " + sec_termino + " >= hbs.horblo_secuencia " +
         " AND   " + sec_termino + " <= hbs2.horblo_secuencia))" ;
  try{
   Statement sent = con.conexion().createStatement();
   ResultSet res = sent.executeQuery(query);
   while(res.next()){
     v.addElement(res.getString(1)); // 0 - cod_asign
     v.addElement(res.getString(2)); // 1 - paralelo
      }
   res.close();
   sent.close();
 }
 catch(SQLException e){
   System.out.println("Error en Horario.getHorario: "+e);
 }
 return v;
  }
  //**************************************************************************
// Metodo              : Permite capturar el bloque desde hora_bloque_sala
// Programadora        : M�nica B.
// Fecha creacion      : 4/7/2007
// Fecha modificacion  :
//****************************************************************************
public int getBloqueSala (String bloque, int num_sala){
 int cod_bloque = 0;
 String query = "";
 query = " SELECT cod_bloque" +
         " FROM hora_bloque_sala " +
         " WHERE horblo_id = " + bloque+
         " AND num_sala = " + num_sala;
  try{
   Statement sent = con.conexion().createStatement();
   ResultSet res = sent.executeQuery(query);
   if(res.next()){
     cod_bloque = res.getInt(1); // 0 - cod_bloque
     }
   res.close();
   sent.close();
 }
 catch(SQLException e){
   System.out.println("Error en Horario.getBloqueSala: "+e);
 }
 return cod_bloque;
  }

//***********************************************************************
// Metodo      : recupera la hora de inicio y termino de un bloque
// Programador : MB
// Fecha       : 4/7/2007

//***********************************************************************
public Vector getBloqueCodigoSede
(int cod_sede, int cod_jornada, int cod_bloque){

  String query = " SELECT  hora_inicio, hora_termino " +
                 " FROM hora_bloque " +
                 " WHERE sed_cod_sede = " + cod_sede +
                 " AND cod_jornada = " + cod_jornada +
                 " AND cod_bloque  = " + cod_bloque +
                 " AND cod_asignacion = 2 ";

  Vector a =  new Vector();
  try {
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);

    while (res.next()) {
      a.addElement(res.getString(1)); // inicio
      a.addElement(res.getString(2)); // termino
    }
    res.close();
    sent.close();
  }
  catch (SQLException e ) {
    System.out.println("Error en Horario.getBloqueCodigoSede: " + e );
  }
  return a;
  }
  //***********************************************************************
   // Metodo      : obtiene el semestre y año actual
   // Programador : MM
   // Fecha       : 27/04/2004
   // Modificado  : 04/06/2004
   //***********************************************************************

   int año_actualpsa = 0;
   int sem_actualpsa = 0;
   int fecha_inicio = 0;
   int fecha_termino = 0;
   public int getAñoActual(){return año_actualpsa;}
   public int getSemestreActual(){return sem_actualpsa;}
   public int getFechaInicioActual(){return fecha_inicio;}
   public int getFechaTerminoActual(){return fecha_termino;}

   public void getPeriodoSemestreActual
   (int cod_jornada, int cod_sede){

     año_actualpsa = 0;
     sem_actualpsa = 0;
     int semestre = 0;
     String query = " SELECT per_año, per_semestre," +
                    " TO_CHAR(per_fecha_inicio,'yyyymmdd'),"+
                    " TO_CHAR(per_fecha_termino,'yyyymmdd')"+
                    " FROM periodo_semestre" +
                    " WHERE cod_jornada = " + cod_jornada +
                    " AND sed_cod_sede = " + cod_sede +
                    " AND TO_CHAR(per_fecha_inicio,'yyyymmdd') <=" +
                    " TO_CHAR(SYSDATE,'yyyymmdd')" +
                    " AND TO_CHAR(per_fecha_termino,'yyyymmdd') >=" +
                    " TO_CHAR(SYSDATE,'yyyymmdd')";

     //System.out.println("getPeriodoSemestreActual query: "+query);
     try {
       Statement sent =  con.conexion().createStatement();
       ResultSet res = sent.executeQuery(query);

       while (res.next())  {
         año_actualpsa = res.getInt(1);
         sem_actualpsa = res.getInt(2);
         fecha_inicio = res.getInt(3);
         fecha_termino = res.getInt(4);
       }
       res.close();
       sent.close();
     }
     catch (SQLException e) {System.out.println("Error Horario.getPeriodoSemestreActual : " + e);}
   }
   //***********************************************************************
     // Metodo      : obtiene la fecha de periodo semestre
     // Programador : MB
     // Fecha       : 05/03/2008
     // Modificado  :
     //***********************************************************************
     int año_per = 0;
     int semestre_per = 0;
     int fecha_inicio_per = 0;
     int fecha_termino_per = 0;
     public int getFechaInicioPeriodo(){return fecha_inicio_per;}
     public int getFechaTerminoPeriodo(){return fecha_termino_per;}

     public void getPeriodoSemestre
     (int año, int semestre){

       String query = " SELECT per_año, per_semestre," +
                      " TO_CHAR(per_fecha_inicio,'yyyymmdd'),"+
                      " TO_CHAR(per_fecha_termino,'yyyymmdd')"+
                      " FROM periodo_semestre" +
                      " WHERE cod_jornada = 1" +
                      " AND sed_cod_sede = 1 " +
                      " AND per_año = " + año +
                      " AND per_semestre = " + semestre ;

       try {
         Statement sent =  con.conexion().createStatement();
         ResultSet res = sent.executeQuery(query);

         while (res.next())  {
           año_per = res.getInt(1);
           semestre_per = res.getInt(2);
           fecha_inicio_per = res.getInt(3);
           fecha_termino_per = res.getInt(4);
         }
         res.close();
         sent.close();
       }
       catch (SQLException e) {System.out.println("Error Horario.getPeriodoSemestre : " + e);}
     }

  //***********************************************************************
// Metodo      : Recupera los paralelos y profesores de una asignatura del per�odo
// Programador : MB
// Fecha       : 10/7/2007

//***********************************************************************
public Vector getParalelo (int año, int semestre){

  String query = " SELECT p.cod_asign, p.pro_paralelo_asignatura, NVL(f.fun_ap_paterno,' '),"+
                 " NVL(f.fun_ap_materno,' '), NVL(f.fun_nombres,' '), NVL(pp.fun_rut,0)" +
                 " FROM programacion p LEFT JOIN programacion_profesor pp ON" +
                 " pp.pro_año = " + año +
                 " AND pp.pro_semestre = " + semestre +
                 " AND p.cod_asign = pp.cod_asign"+
                 " AND p.pro_paralelo_asignatura = pp.pro_paralelo_asignatura" +
                 " LEFT JOIN funcionario f ON " +
                 " pp.fun_rut = f.fun_rut" +
                 " WHERE p.pro_año = " + año +
                 " AND p.pro_semestre = " + semestre +
                 " AND p.cod_nivel_asignatura = 3"+
                 " UNION"+
                 " SELECT p.cod_asign, p.pro_paralelo_asignatura, NVL(f.fun_ap_paterno,' '),"+
                 " NVL(f.fun_ap_materno,' '), NVL(f.fun_nombres,' '), NVL(pp.fun_rut,0)" +
                 " FROM programacion p LEFT JOIN programacion_profesor pp ON" +
                 " pp.pro_año = " + año +
                 " AND pp.pro_semestre = " + semestre +
                 " AND p.cod_asign = pp.cod_asign"+
                 " AND p.pro_paralelo_asignatura = pp.pro_paralelo_asignatura" +
                 " LEFT JOIN funcionario f ON " +
                 " pp.fun_rut = f.fun_rut" +
                 " WHERE p.pro_año = " + año +
                 " AND p.pro_semestre = " + semestre +
                 " AND p.cod_asign IN (1410020,1410017)"+
                 " ORDER BY pro_paralelo_asignatura";
//System.out.println("getParalelo: "+ query);
  Vector a =  new Vector();
  Vector v =  new Vector();
  try {
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);

    while (res.next()) {
      a =  new Vector();
      a.addElement(res.getString(1)); // cod_asign
      a.addElement(res.getString(2)); // paralelo
      a.addElement(res.getString(3)+' '+res.getString(4)+' ' + res.getString(5)); // profesor
      a.addElement(res.getString(6)); // fun_rut
      v.addElement(a);
    }
    res.close();
    sent.close();
  }
  catch (SQLException e ) {
    System.out.println("Error en Horario.getParalelo: " + e );
  }
  return v;
  }
  //***********************************************************************
// Metodo      : Recupera los paralelos y profesores de una asignatura del per�odo
// Programador : MB
// Fecha       : 10/7/2007

//***********************************************************************
public Vector getProgramacion (int año, int semestre){

  String query = " SELECT distinct  p.cod_asign, p.pro_paralelo_asignatura, NVL(f.fun_rut,0)," +
                 " NVL(f.fun_ap_paterno,' ') || ' ' ||NVL(f.fun_ap_materno,' ')||' '|| NVL(f.fun_nombres,' ')," +
                 " p.pro_cupo , NVL(f.fun_dig_rut,' '), p.pro_sobrecupo" +
                 " FROM programacion p LEFT JOIN programacion_profesor pf ON" +
                 " p.pro_año = pf.pro_año" +
                 " AND p.pro_semestre = pf.pro_semestre" +
                 " AND p.cod_asign = pf.cod_asign" +
                 " AND p.pro_paralelo_asignatura = pf.pro_paralelo_asignatura" +
                 " LEFT JOIN funcionario f ON" +
                 " pf.fun_rut = f.fun_rut" +
                 " WHERE p.pro_año = " + año +
                 " AND p.pro_semestre = " + semestre +
                 " AND p.estado_nulo = 1" +
                 " AND p.cod_nivel_asignatura = 3 " +
                 " AND p.sed_cod_sede = 1"+
                 " UNION " +   // union la asignatura de docencia dew125
                 " SELECT distinct  p.cod_asign, p.pro_paralelo_asignatura, NVL(f.fun_rut,0)," +
                 " NVL(f.fun_ap_paterno,' ') || ' ' ||NVL(f.fun_ap_materno,' ')||' '|| NVL(f.fun_nombres,' ')," +
                 " p.pro_cupo , NVL(f.fun_dig_rut,' '), p.pro_sobrecupo" +
                 " FROM programacion p LEFT JOIN programacion_profesor pf ON"+
                 " p.pro_año = pf.pro_año" +
                 " AND p.pro_semestre = pf.pro_semestre" +
                 " AND p.cod_asign = pf.cod_asign" +
                 " AND p.pro_paralelo_asignatura = pf.pro_paralelo_asignatura" +
                 " LEFT JOIN funcionario f ON" +
                 " pf.fun_rut = f.fun_rut" +
                 " WHERE p.pro_año = " + año +
                 " AND p.pro_semestre = " + semestre +
                 " AND p.estado_nulo = 1" +
                 " AND p.cod_asign IN (1410020,1410017)" +
                 " AND p.sed_cod_sede = 1";
//System.out.println("getProgramacion: "+ query);
  Vector a =  new Vector();
  Vector v =  new Vector();
  Vector vec_inscritos =  new Vector();
  try {
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);

    while (res.next()) {
      a =  new Vector();
      a.addElement(res.getString(1)); // 0 cod asign
      a.addElement(res.getString(2)); // 1 paralelo
      a.addElement(res.getString(3)); // 2 rut funcionario
      a.addElement(res.getString(4)); // 3 profesor
      a.addElement(res.getString(5)); // 4 cupo
      a.addElement(res.getString(6)); // 5 dig rut prof
      if (res.getInt(1) == 1410020 || res.getInt(1) == 1410017){
        vec_inscritos = getListaCurso(año, semestre,res.getInt(1), res.getInt(2));
        a.addElement(vec_inscritos.size()+""); // 6 total de inscritos
        }
        else a.addElement("0"); // 6 total de inscritos
      a.addElement(res.getString(7));// 7 cupo de rama
      v.addElement(a);
    }
    res.close();
    sent.close();
  }
  catch (SQLException e ) {
    System.out.println("Error en Horario.getProgramacion: " + e );
  }
  return v;
  }
//***********************************************************************
// Metodo      : Recupera los alumnos que pertenecen a un paralelo en un per�odo
// Programador : MB
// Fecha       : 10/08/2007

//***********************************************************************
public Vector getListaCurso (int año, int semestre, int cod_asign, int paralelo){

  String query = " SELECT i.alu_rut, a.alu_dig_rut, a.alu_ap_paterno, " +
                 " NVL(a.alu_ap_materno,' '), a.alu_nombres, asi.nom_asign" +
                 " FROM inscripcion_asignatura i, alumno a, asignatura asi" +
                 " WHERE i.pro_año =" + año +
                 " AND i.pro_semestre =" + semestre +
                 " AND i.cod_asign =" + cod_asign +
                 " AND i.pro_paralelo_asignatura =" + paralelo +
                 " AND i.ina_situacion = 1" +
                 " AND i.alu_rut = a.alu_rut" +
                 " AND i.cod_asign = asi.cod_asign " +
                 " ORDER BY a.alu_ap_paterno, " +
                 " a.alu_ap_materno, a.alu_nombres ";
//System.out.println("getListaCurso: "+ query);
Vector a =  new Vector();
Vector v =  new Vector();
try {
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);

  while (res.next()) {
    a =  new Vector();
    a.addElement(res.getString(1)); // 0 RUT
    a.addElement(res.getString(2)); // 1 dig_rut
    a.addElement(res.getString(3)); // 2 paterno
    a.addElement(res.getString(4)); // 3 materno
    a.addElement(res.getString(5)); // 4 nombres
    a.addElement(res.getString(6)); // 5 nom asignatura
    v.addElement(a);
  }
  res.close();
  sent.close();
}
catch (SQLException e ) {
  System.out.println("Error en Horario.getListaCurso: " + e );
}
return v;
  }
  //***********************************************************************
// Metodo      : Recupera los per�odos de programaciones del gimnasio
// Programador : MB
// Fecha       : 28/09/2007

//***********************************************************************
public Vector getProgramacionPeriodo (){

  String query = " SELECT distinct pro_año, pro_semestre" +
                 " FROM programacion" +
                 " WHERE cod_nivel_asignatura = 3" +
                 " ORDER BY 1, 2";
//System.out.println("getProgramacionPeriodo: "+ query);
Vector a =  new Vector();
Vector v =  new Vector();
try {
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);

  while (res.next()) {
    a =  new Vector();
    a.addElement(res.getString(1)); // 0 año
    a.addElement(res.getString(2)); // 1 semestre
    v.addElement(a);
  }
  res.close();
  sent.close();
}
catch (SQLException e ) {
  System.out.println("Error en Horario.getProgramacionPeriodo: " + e );
}
return v;
  }

//***********************************************************************
// Metodo      : Recupera los datos de la docencia que pertenece el alumno
// Programador : MB
// Fecha       : 10/08/2007

//***********************************************************************
public Vector getInscritoDocencia (int año, int semestre, int cod_asign, int rut){

  String query = " SELECT asi.nom_asign, i.pro_paralelo_asignatura" +
                 " FROM inscripcion_asignatura i, alumno a, asignatura asi" +
                 " WHERE i.pro_año =" + año +
                 " AND i.pro_semestre =" + semestre +
                 " AND i.cod_asign =" + cod_asign +
                 " AND i.alu_rut = " + rut +
                 " AND i.ina_situacion = 1" +
                 " AND i.alu_rut = a.alu_rut" +
                 " AND i.cod_asign = asi.cod_asign " +
                 " ORDER BY a.alu_ap_paterno, " +
                 " a.alu_ap_materno, a.alu_nombres ";
//System.out.println("getInscritoDocencia: "+ query);

Vector v =  new Vector();
try {
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);

  while (res.next()) {
    v.addElement(res.getString(1)); // 0 nom asignatura
    v.addElement(res.getString(2)); // 1 paralelo

  }
  res.close();
  sent.close();
}
catch (SQLException e ) {
  System.out.println("Error en Horario.getInscritoDocencia: " + e );
}
return v;
  }
  //***********************************************************************
// Metodo      : Recupera los profesores que pertenecen al gimnasio
// Programador : MB
// Fecha       : 10/7/2007

//***********************************************************************
public Vector getProfesorGimnasio (){
/*
  String query = " SELECT DISTINCT f.fun_rut, f.fun_dig_rut, f.fun_ap_paterno" +
                 " || ' '|| NVL(f.fun_ap_materno,' ')||' ' || f.fun_nombres" +
                 " FROM funcionario f, cargo_funcionario cf" +
                 " WHERE f.fun_rut = cf.fun_rut" +
                 " AND f.fun_vigente = 1" +
                 " AND f.fun_tipo_funcionario = 2" +
                 " AND cf.dep_cod_departamento = 41" +
                 " ORDER BY f.fun_ap_paterno || NVL(f.fun_ap_materno,' ') || f.fun_nombres" ;*/


	String query = " SELECT DISTINCT f.fun_rut, f.fun_dig_rut, f.fun_ap_paterno ||' '||"+
				   " NVL(f.fun_ap_materno,' ') ||' '||  f.fun_nombres" +
                   " FROM funcionario f, funcionario_cargo fc"+
                   " WHERE f.fun_rut = fc.fun_rut"+ 
                   " AND f.fun_vigente = 1"+
                   " AND fc.cod_planta_funcionario in (29,32,33,34)"+
                   " AND fc.dep_cod_departamento = 41"+
                   " AND fc.cod_vigencia = 1"+
                   " ORDER BY 3";
//System.out.println("getProfesorGimnasio: "+ query);
Vector a =  new Vector();
Vector v =  new Vector();
try {
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);

  while (res.next()) {
    a =  new Vector();
    a.addElement(res.getString(1)); // 0 fun_rut
    a.addElement(res.getString(2)); // 1 dig_rut
    a.addElement(res.getString(3)); // 2 nom funcionario
    v.addElement(a);
  }
  res.close();
  sent.close();
}
catch (SQLException e ) {
  System.out.println("Error en Horario.getProfesorGimnasio: " + e );
}
return v;
  }
  //***********************************************************************
// Metodo      : Recupera la fechas del periodo semestre consultado
// Programador : MB
// Fecha       : 26/09/2007

//***********************************************************************
public Vector getFechaPeriodoSemestre (int año, int semestre){

  String query = " SELECT  per_fecha_inicio ,  per_fecha_termino" +
                 " FROM periodo_semestre" +
                 " WHERE  per_año = " + año +
                 " AND per_semestre = " + semestre +
                 " AND sed_cod_sede = 1" +
                 " AND cod_jornada = 1" ;
//System.out.println("getFechaPeriodoSemestre: "+ query);
Vector a =  new Vector();
Vector v =  new Vector();
try {
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);

  while (res.next()) {
    a =  new Vector();
    a.addElement(res.getString(1)); // 0 fun_rut
    a.addElement(res.getString(2)); // 1 dig_rut
    a.addElement(res.getString(3)); // 2 nom funcionario
    v.addElement(a);
  }
  res.close();
  sent.close();
}
catch (SQLException e ) {
  System.out.println("Error en Horario.getProfesorGimnasio: " + e );
}
return v;
  }

//**************************************************************************
  // Metodo              : Permite capturar el horario de la sala de un d�a
  // Programadora        : MMR
  // Fecha creacion      : 31/07/2007
  // Fecha modificacion  : 09/05/2008 se agreg� fecha de vigencia de horario_asignatura,
//                       solicitado por Jorge M

//****************************************************************************
  public Vector getHorarioDia ( int año, int semestre, int num_sala, int dia, String fecha){
    java.util.Vector v = new java.util.Vector();
    java.util.Vector a = new java.util.Vector();
    java.util.Vector blo_ini = new java.util.Vector();
    java.util.Vector blo_fin = new java.util.Vector();
    String b1 = "";
    String b2 = "";
    String query = "";
    query = " SELECT h.cod_asign, h.pro_paralelo_asignatura,"+
            " h.cod_bloque_inicio, h.cod_bloque_termino, h.cod_reserva," +
            " a.nom_asign, t.nom_reserva, NVL(f.fun_ap_paterno,' '),"+
            " NVL(f.fun_ap_materno,' '), NVL(f.fun_nombres,' ')"+
            " FROM horario_asignatura h" +
            " LEFT JOIN programacion_profesor pp" +
            " ON pp.pro_año = h.pro_año" +
            " AND pp.pro_semestre = h.pro_semestre" +
            " AND pp.cod_asign = h.cod_asign" +
            " AND pp.pro_paralelo_asignatura = h.pro_paralelo_asignatura" +
            " LEFT JOIN funcionario f" +
            " ON f.fun_rut = pp.fun_rut" +
            ", asignatura a, tipo_reserva t, hora_bloque_sala hbs" +
            " WHERE h.pro_año = " + año +
            " AND h.pro_semestre = " + semestre +
            " AND h.cod_jornada = 1"+
            " AND h.sed_cod_sede = 1" +
            " AND h.num_sala = " + num_sala +
            " AND h.num_dia = " + dia +
            " AND TO_CHAR(h.horasi_fecha_inicio,'yyyymmdd') <= '" + fecha +
            "' AND ( h.horasi_fecha_termino is null OR  '" + fecha +
            "' <= TO_CHAR(h.horasi_fecha_termino,'yyyymmdd'))" +
            " AND h.cod_asign = a.cod_asign" +
            " AND h.cod_reserva = t.cod_reserva" +
            " AND h.pro_año = hbs.horblo_año" +
            " AND h.pro_semestre = hbs.horblo_semestre" +
            " AND h.cod_jornada = hbs.cod_jornada" +
            " AND h.sed_cod_sede = hbs.sed_cod_sede" +
            " AND h.num_sala = hbs.num_sala" +
            " AND h.cod_bloque_inicio = hbs.cod_bloque" +
            " ORDER BY hbs.horblo_secuencia" ;
//System.out.println("getHorarioDia : "+ query);
    try{
	     Statement sent = con.conexion().createStatement();
	     ResultSet res = sent.executeQuery(query);
	     while(res.next()){
	       a = new java.util.Vector();
	       a.addElement(res.getString(1)); // 0 - cod_asign
	       a.addElement(res.getString(2)); // 1 - paralelo
	       a.addElement(res.getString(3)); // 2 - cod_bloque_inicio
	       a.addElement(res.getString(4)); // 3 - cod_bloque_termino

/*  lo cambi� a hora inicio y hora de fin del bloque*/
//       blo_ini = getBloqueCodigoSede(1, 1, res.getInt(3));
//       blo_fin = getBloqueCodigoSede(1, 1, res.getInt(4));
//       b1 = (blo_ini.size()>0)?blo_ini.get(0)+"":"";
//       b2 = (blo_ini.size()>0)?blo_ini.get(1)+"":"";
//       a.addElement(b1+"-"+b2); // 4 - bloque_inicio
//
//       b1 = (blo_fin.size()>0)?blo_fin.get(0)+"":"";
//       b2 =  (blo_fin.size()>0)?blo_fin.get(1)+"":"";
//       a.addElement(b1+"-"+b2); // 5 - bloque_termino
       blo_ini = getBloqueCodigoSede(1, 1, res.getInt(3));
       blo_fin = getBloqueCodigoSede(1, 1, res.getInt(4));
       b1 = (blo_ini.size()>0)?blo_ini.get(0)+"":"";
       a.addElement(b1); // 4 - bloque_inicio

       b2 =  (blo_fin.size()>0)?blo_fin.get(1)+"":"";
       a.addElement(b2); // 5 - bloque_termino

       a.addElement(res.getString(5)); // 6 - cod_reserva
       a.addElement(res.getString(6)); // 7 - nom_asign
       a.addElement(res.getString(7)); // 8 - nom_reserva
       a.addElement(res.getString(8) + " " +
                   res.getString(9) + " " +
                   res.getString(10)); // 9 - profesor
       v.addElement(a);
     }
     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Horario.getHorarioDia: "+e);
   }
   return v;
  }

//**************************************************************************
  // Metodo              : Permite capturar la m�xima secuencia de hora_bloque_sala
  // Programadora        : MBF
  // Fecha creacion      : 11/03/2008
  // Fecha modificacion  : 13/03/2008 agrega campos año y semestre a la tabla

//****************************************************************************
  public int getMaxSecuencia ( int sala, int año, int semestre){
    int secuencia = 0;
    String query = " SELECT MAX(horblo_secuencia)" +
		           " FROM hora_bloque_sala" +
		           " WHERE num_sala = " + sala +
		           " AND horblo_año = " + año +
		           " AND horblo_semestre = " + semestre;
  //  System.out.println("getMaxSecuencia : "+ query);
    try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     while(res.next()){
       secuencia = res.getInt(1); // max secuencia
     }
     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Horario.getMaxSecuencia: "+e);
   }
   return secuencia;
  }

//**************************************************************************
  // Metodo              : Permite capturar la secuencia de hora_bloque_sala
  // Programadora        : MBF
  // Fecha creacion      : 12/03/2008
  // Fecha modificacion  :

//****************************************************************************
  public Vector getSecuencia ( int sala, String id){
   Vector v = new Vector();
    String query = " SELECT horblo_secuencia" +
		           " FROM hora_bloque_sala" +
		           " WHERE num_sala = " + sala +
		           " AND horblo_id = " + id;
  //  System.out.println("getSecuencia : "+ query);
    try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     while(res.next()){
       v.addElement(res.getString(1)); // secuencia
     }
     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Horario.getSecuencia: "+e);
   }
   return v;
  }
  //**************************************************************************
  // Metodo              : Permite capturar la secuencia de hora_bloque_sala desde el bloque
  // Programadora        : MBF
  // Fecha creacion      : 01/04/2008
  // Fecha modificacion  :

//****************************************************************************
  public int getSecuenciaBloque ( int sala, int año, int semestre, int cod_bloque){
   int secuencia = 0;
   String query = " SELECT horblo_secuencia" +
                  " FROM hora_bloque_sala" +
                  " WHERE num_sala = " + sala +
                  " AND horblo_año = " + año +
                  " AND horblo_semestre = " + semestre +
                  " AND cod_bloque = " + cod_bloque +
                  " AND sed_cod_sede = 1" +
                  " AND cod_jornada = 1"  +
                  " AND horblo_fecha_termino IS NULL";
  //  System.out.println("getSecuenciaBloque : "+ query);
    try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     while(res.next()){
       secuencia = res.getInt(1); // secuencia
     }
     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Horario.getSecuenciaBloque: "+e);
   }
   return secuencia ;
  }
  
  public Vector getSecuenciaBloque ( int sala, int año, int semestre, int cod_bloque1, int cod_bloque2){
	   Vector vec_secuencia = new Vector();
	   vec_secuencia.addElement("0");
	   vec_secuencia.addElement("0");
	   String query = " SELECT cod_bloque, horblo_secuencia" +
	                  " FROM hora_bloque_sala" +
	                  " WHERE num_sala = " + sala +
	                  " AND horblo_año = " + año +
	                  " AND horblo_semestre = " + semestre +
	                  " AND cod_bloque IN (" + cod_bloque1 + "," + cod_bloque2 + ")" +
	                  " AND sed_cod_sede = 1" +
	                  " AND cod_jornada = 1"  +
	                  " AND horblo_fecha_termino IS NULL" +
	                  " ORDER BY cod_bloque";
	   
	    //System.out.println("getSecuenciaBloque : "+ query);
	    try{
	     Statement sent = con.conexion().createStatement();
	     ResultSet res = sent.executeQuery(query);
	     while(res.next()){
	    	 if (res.getInt(1) == cod_bloque1) {vec_secuencia.setElementAt(res.getString(2), 0);}
	    	 if (res.getInt(1) == cod_bloque2) {vec_secuencia.setElementAt(res.getString(2), 1);}
	     }
	     res.close();
	     sent.close();
	   }
	   catch(SQLException e){
	     System.out.println("Error en Horario.getSecuenciaBloque: "+e);
	   }
	   return vec_secuencia ;
  }
//**************************************************************************
// Metodo              : Permite insertar actividad deportiva
// Programadora        : M�nica B.
// Fecha creacion      : 8/6/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setInsertarActividad(String nom_asign,
                                                int rut_usuario){
   String query = "";
   /*String par = "'" + nom_asign + "'," + rut_usuario;
   query = "EXEC spu_ins_actividad "+ par;
   System.out.println(query);*/
   Connection con2 = con.conexion();
   int error = -1;
   try{
	   CallableStatement cstmt = con2.prepareCall("{call spu_ins_actividad (?,?,?)}");
	   cstmt.setString(1,nom_asign);
	   cstmt.setInt(2,rut_usuario);
	   cstmt.registerOutParameter(3,Types.INTEGER);
	   cstmt.executeQuery();
	   error = cstmt.getInt(3);
	   cstmt.close();
   }
   catch(SQLException e){
     System.out.println("Error en Horario.setInsertarActividad: "+e);
   }
   return error;
  }

//**************************************************************************
// Metodo              : Permite capturar actualizar actividad deportiva
//
// Programadora        : M�nica B.
// Fecha creacion      : 8/6/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setActualizarActividad(String nom_asign,
                                                 int rut_usuario,
                                                 int cod_asign){
 String query = "";
 String par = cod_asign + ", '" + nom_asign + "'," + rut_usuario;
 Connection con2 = con.conexion();
 /*query = "EXEC spu_upd_actividad "+ par;
 System.out.println(query);*/
 int error = -1;
 try{
	 CallableStatement cstmt = con2.prepareCall("{call spu_upd_actividad (?,?,?,?)}");
	 cstmt.setInt(1,cod_asign);
	 cstmt.setString(2,nom_asign);
	 cstmt.setInt(3,rut_usuario);
	 cstmt.registerOutParameter(4,Types.INTEGER);
	 cstmt.executeQuery();
	 error = cstmt.getInt(4);
	 cstmt.close();
 }
 catch(SQLException e){
   System.out.println("Error en Horario.setActualizarActividad: "+e);
 }
 return error;
  }

//**************************************************************************
// Metodo              : Permite capturar aeliminar actividad deportiva
//
// Programadora        : M�nica B.
// Fecha creacion      : 8/6/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setEliminaActividad(int cod_asign){
    String query = "";
    String par = cod_asign +"";
    //query = "EXEC spu_dlt_actividad "+ par;
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_dlt_actividad(?,?)}");
	   	cstmt.setInt(1,cod_asign);
	  	cstmt.registerOutParameter(2,Types.INTEGER);
	   	cstmt.executeQuery();
	   	error = cstmt.getInt(2);
	   	cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Horario.setEliminaActividad: "+e);
    }
    return error;
  }

//**************************************************************************
// Metodo              : Permite insertar bloque
//
// Programadora        : M�nica B.
// Fecha creacion      : 8/6/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setInsertarHoraBloque(int sede,
                                                int cod_jornada,
                                                String inicio,
                                                String termino,
                                                int rut_usuario,
                                                int num_sala,
                                                int secuencia,
                                                int año,
                                                int semestre){
   String query = "";
   String par = sede+"," +cod_jornada+",'"+ inicio+"','"+ termino+"'," +
                rut_usuario+","+ num_sala+","+secuencia+","+año+","+semestre;
   //query = "EXEC spu_ins_horabloque_sala "+ par;
//System.out.println("setInsertarHoraBloque; "+ query);
   Connection con2 = con.conexion();
   int error = -1;
   try{
	   CallableStatement cstmt = con2.prepareCall("{call spu_ins_horabloque_sala (?,?,?,?,?,?,?,?,?,?)}");
	     cstmt.setInt(1,sede);
	     cstmt.setInt(2,cod_jornada);
	     cstmt.setString(3,inicio);
	     cstmt.setString(4,termino);
	     cstmt.setInt(5,rut_usuario);
		 cstmt.setInt(6,num_sala);
		 cstmt.setInt(7,secuencia);
		 cstmt.setInt(8,año);
		 cstmt.setInt(9,semestre);
		 cstmt.registerOutParameter(10,Types.INTEGER);
		 cstmt.executeQuery();
		 error = cstmt.getInt(10);
		 cstmt.close();
   }
   catch(SQLException e){
     System.out.println("Error en Horario.setInsertarHoraBloque: "+e);
   }
   return error;
  }
  //**************************************************************************
// Metodo              : Permite insertar bloque
//
// Programadora        : M�nica B.
// Fecha creacion      : 8/6/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setCopiarHoraBloque(  int rut_usuario,
                                                int num_sala,
                                                int año,
                                                int semestre){
   String query = "";
   String par = rut_usuario+","+
                num_sala+","+
                año+","+
                semestre;
 //  query = "EXEC spu_ins_horabloque_semestral  "+ par;
//System.out.println("setCopiarHoraBloque; "+ query);
   Connection con2 = con.conexion();
   int error = -1;
   try{
       CallableStatement cstmt = con2.prepareCall("{call spu_ins_horabloque_semestral (?,?,?,?,?)}");
	   cstmt.setInt(1,rut_usuario);
	   cstmt.setInt(2,num_sala);
	   cstmt.setInt(3,año);
	   cstmt.setInt(4,semestre);
	   cstmt.registerOutParameter(5,Types.INTEGER);
	   cstmt.executeQuery();
	   error = cstmt.getInt(5);
	   cstmt.close();
   }
   catch(SQLException e){
     System.out.println("Error en Horario.setInsertarHoraBloque: "+e);
   }
   return error;
  }
  //**************************************************************************
// Metodo              : Permite eliminar bloque
//
// Programadora        : M�nica B.
// Fecha creacion      : 8/6/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setEliminarHoraBloque(int rut_usuario,
                                                String id){
    String query = "";
    String par = rut_usuario+","+ id ;
   // query = "EXEC spu_dlt_horabloque_sala " + par;
 //System.out.println("setEliminarHoraBloque: "+ query);
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_dlt_horabloque_sala (?,?,?)");
 	    cstmt.setInt(1,rut_usuario);
 	    cstmt.setString(2,id);
 	    cstmt.registerOutParameter(3,Types.INTEGER);
 	    cstmt.executeQuery();
 	    error = cstmt.getInt(3);
 	    cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Horario.setEliminarHoraBloque: "+e);
    }
    return error;
  }
  //**************************************************************************
// Metodo              : Permite modificar numero del bloque
//
// Programadora        : M�nica B.
// Fecha creacion      : 24/03/2008
// Fecha modificacion  :
//****************************************************************************
 synchronized public int setModificarHoraBloque(int rut_usuario,
                                               String id,
                                               int secuencia){
   String query = "";
   String par = rut_usuario+","+ id+","+secuencia;
  // query = "EXEC spu_upd_horabloque_sala " + par;
//System.out.println("setModificarHoraBloque: "+ query);
   Connection con2 = con.conexion();
   int error = -1;
   try{
	   CallableStatement cstmt = con2.prepareCall("{call spu_upd_horabloque_sala (?,?,?,?)");
	   cstmt.setInt(1,rut_usuario);
	   cstmt.setString(2,id);
	   cstmt.setInt(3,secuencia);
	   cstmt.registerOutParameter(4,Types.INTEGER);
	   cstmt.executeQuery();
	   error = cstmt.getInt(4);
	   cstmt.close();
   }
   catch(SQLException e){
     System.out.println("Error en Horario.setModificarHoraBloque: "+e);
   }
   return error;
  }
  //**************************************************************************
// Metodo              : Permite eliminar hora_bloque_sala semestral
//
// Programadora        : M�nica B.
// Fecha creacion      : 14/03/2008
// Fecha modificacion  :
//****************************************************************************
synchronized public int setEliminarHoraBloqueSemestral(int año,
                                                       int semestre,
                                                       int sala){
  String query = "";
  String par = año+","+
               semestre+","+
               sala;
  //query = "EXEC spu_dlt_horabloque_sala_semes " + par;
//System.out.println("setEliminarHoraBloqueSemestral: "+ query);
  Connection con2 = con.conexion();
  int error = -1;
  try{
	  CallableStatement cstmt = con2.prepareCall("{call spu_dlt_horabloque_sala_semes (?,?,?,?)");
	  cstmt.setInt(1,año);
	  cstmt.setInt(2,semestre);
	  cstmt.setInt(3,sala);
	  cstmt.registerOutParameter(4,Types.INTEGER);
	  cstmt.executeQuery();
	  error = cstmt.getInt(4);
	  cstmt.close();
  }
  catch(SQLException e){
    System.out.println("Error en Horario.setEliminarHoraBloqueSemestral: "+e);
  }
  return error;
  }

  //**************************************************************************
// Metodo              : Permite mantener aseo_gimnasio
// Programadora        : M�nica B.
// Fecha creacion      : 9/7/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setAgregarAseo(int sede,
                                         int año,
                                         int semestre,
                                         int jornada,
                                         int num_sala,
                                         int bloque_inicio,
                                         int bloque_termino,
                                         String fecha_inicio,
                                         String fecha_termino,
                                         int rut_usuario,
                                         int dia_inicio,
                                         int dia_termino,
                                         String id_aseo){
    gimnasio.BeanGeneral general = new gimnasio.BeanGeneral();
    general.getAbrirConexion();
    general.setGenerarConexionReserva();
    String f1 = general.SetFechaFormato(fecha_inicio);
    String f2 = general.SetFechaFormato(fecha_termino);
    if (f1.trim().equals("")) f1 = null;
    if (f2.trim().equals("")) f2 = null;
    general.getCerrarConexion();
    String query = "";
    String par = sede+"," +
                 año+"," +
                 semestre+"," +
                 jornada+"," +
                 num_sala+"," +
                 bloque_inicio+"," +
                 bloque_termino+"," +
                 f1+"," +
                 f2+"," +
                 rut_usuario+","+
                 dia_inicio+"," +
                 dia_termino+","+ 
                 id_aseo+"";
  //  query = "EXEC spu_tran_aseo_gimnasio " + par;
// System.out.println("spu_tran_aseo_gimnasio: "+ query);
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_tran_aseo_gimnasio (?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
  	    cstmt.setInt(1,sede);
  	    cstmt.setInt(2,año);
  	    cstmt.setInt(3,semestre);
  	    cstmt.setInt(4,jornada);
  	    cstmt.setInt(5,num_sala);
  	    cstmt.setInt(6,bloque_inicio);
  	    cstmt.setInt(7,bloque_termino);
  	    cstmt.setString(8,f1);
  	    cstmt.setString(9,f2);
  	    cstmt.setInt(10,rut_usuario);
     	cstmt.setInt(11,dia_inicio);
  	    cstmt.setInt(12,dia_termino);
  	    cstmt.setLong(13,Long.parseLong(id_aseo));
  	    cstmt.registerOutParameter(14,Types.INTEGER);
  	    cstmt.executeQuery();
  	    error = cstmt.getInt(14);
  	    cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Horario.setAgregarAseo: "+e);
    }
    return error;
  }

//**************************************************************************
// Metodo              : Permite mantener horario_asignatura
// Programadora        : M�nica B.
// Fecha creacion      : 9/7/2007
// Fecha modificacion  :
//****************************************************************************
synchronized public int setAgregarHorario(int sede,
	                                      int año,
	                                      int semestre,
	                                      int num_sala,
	                                      int bloque_inicio,
	                                      int bloque_termino,
	                                      int dia_inicio,
	                                      int dia_termino,
	                                      int cod_reserva,
	                                      int paralelo,
	                                      int cod_asign,
	                                      int jornada,
	                                      int rut_usuario){

  String query = "";
  String par = sede+"," +
               año+"," +
               semestre+"," +
               num_sala+"," +
               bloque_inicio+"," +
               bloque_termino+"," +
               dia_inicio+"," +
               dia_termino+","+
               cod_reserva+","+
               paralelo+","+
               cod_asign+","+
               jornada+","+
               rut_usuario+",0";
 // query = " EXEC spu_ins_horario_gimnasio " + par;
//System.out.println("setAgregarHorario: "+ query);
  Connection con2 = con.conexion();
  int error = -1;
  try{
	  CallableStatement cstmt = con2.prepareCall("{call spu_ins_horario_gimnasio(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
	  cstmt.setInt(1,sede);
	  cstmt.setInt(2,año);
	  cstmt.setInt(3,semestre);
	  cstmt.setInt(4,num_sala );
	  cstmt.setInt(5,bloque_inicio);
	  cstmt.setInt(6,bloque_termino);
	  cstmt.setInt(7,dia_inicio);
	  cstmt.setInt(8,dia_termino);
	  cstmt.setInt(9,cod_reserva);
	  cstmt.setInt(10,paralelo);
	  cstmt.setInt(11,cod_asign);
	  cstmt.setInt(12,jornada);
	  cstmt.setInt(13,rut_usuario);
	  cstmt.setInt(14, 0);//accion indica al procedimiento 
	  					//que es llamado desde c�digo java
	  cstmt.registerOutParameter(15,Types.INTEGER);
	  cstmt.executeQuery();
	  error = cstmt.getInt(15);
	  cstmt.close();
  }
  catch(SQLException e){
    System.out.println("Error en Horario.setAgregarHorario: "+e);
  }
  return error;
}
//**************************************************************************
// Metodo              : Permite modificar horario_asignatura
// Programadora        : M�nica B.
// Fecha creacion      : 11/7/2007
// Fecha modificacion  :
//****************************************************************************
synchronized public int setModificaHorario(int sede,
		                                   int año,
		                                   int semestre,
		                                   int num_sala,
		                                   int bloque_inicio,
		                                   int bloque_termino,
		                                   int dia_inicio,
		                                   int dia_termino,
		                                   int cod_reserva,
		                                   int paralelo,
		                                   int cod_asign,
		                                   int jornada,
		                                   int bl_inicio_or,
		                                   int bl_termino_or,
		                                   int d_inicio_or,
					                       int d_termino_or,
		                                   int rut_usuario){

  String query = "";
  String par = sede+"," +
               año+"," +
               semestre+"," +
               num_sala+"," +
               bloque_inicio+"," +
               bloque_termino+"," +
               dia_inicio+"," +
               dia_termino+","+
               cod_reserva+","+
               paralelo+","+
               cod_asign+","+
               jornada+","+
               bl_inicio_or+","+
               bl_termino_or+","+
               d_inicio_or+","+
               d_termino_or+","+
               rut_usuario+"";
  //query = " EXEC spu_upd_horario_gimnasio " + par;
  //System.out.println("setModificaHorario: "+ query);
  
  Connection con2 = con.conexion();
  int error = -1;
  try{
	  CallableStatement cstmt = con2.prepareCall("{call spu_upd_horario_gimnasio(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
	  cstmt.setInt(1,sede);
	  cstmt.setInt(2,año);
	  cstmt.setInt(3,semestre);
	  cstmt.setInt(4,num_sala );
	  cstmt.setInt(5,bloque_inicio);
	  cstmt.setInt(6,bloque_termino);
	  cstmt.setInt(7,dia_inicio);
	  cstmt.setInt(8,dia_termino);
	  cstmt.setInt(9,cod_reserva);
	  cstmt.setInt(10,paralelo);
	  cstmt.setInt(11,cod_asign);
	  cstmt.setInt(12,jornada);
	  cstmt.setInt(13,bl_inicio_or);
	  cstmt.setInt(14,bl_termino_or);
	  cstmt.setInt(15,d_inicio_or);
	  cstmt.setInt(16,d_termino_or);
	  cstmt.setInt(17,rut_usuario);
	  cstmt.registerOutParameter(18,Types.INTEGER);
	  cstmt.executeQuery();
	  error = cstmt.getInt(18);
	  cstmt.close();
  }
  catch(SQLException e){
    System.out.println("Error en Horario.setModificaHorario: "+e);
  }
  return error;
}

  //**************************************************************************
// Metodo              : Permite eliminar aso gimnasio
// Programadora        : M�nica B.
// Fecha creacion      : 12/7/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setEliminaAseo(String id_aseo){

    String query = "";

 //   query = " EXEC spu_dlt_aseo_gimnasio " +id_aseo;
//System.out.println("setEliminaAseo: "+ query);
    
    Connection con2 = con.conexion();
    int error = -1;
    try{
        CallableStatement cstmt = con2.prepareCall("{call spu_dlt_aseo_gimnasio(?,?)");
        cstmt.setInt(1,Integer.parseInt(id_aseo));
        cstmt.registerOutParameter(2,Types.INTEGER);
        cstmt.executeQuery();
        error = cstmt.getInt(2);
        cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Horario.setEliminaAseo: "+e);
    }
    return error;
  }
  //**************************************************************************
// Metodo              : Permite eliminar horario de asignatura
// Programadora        : M�nica B.
// Fecha creacion      : 12/7/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int  setEliminaHorario (int sede,
                                              int año,
                                              int semestre,
                                              int num_sala,
                                              int bloque_inicio,
                                              int bloque_termino,
                                              int dia_inicio,
                                              int rut_usuario){
    String query = "";
    String par =  sede+","+
                  año+","+
                  semestre+","+
                  num_sala+","+
                  bloque_inicio+","+
                  bloque_termino+","+
                  dia_inicio+","+
                  rut_usuario+"";

  //  query = " EXEC spu_dlt_horario_gimnasio " +par;
//System.out.println("setEliminaHorario: "+ query);
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_dlt_horario_gimnasio(?,?,?,?,?,?,?,?,?)");
        cstmt.setInt(1,sede);
        cstmt.setInt(2,año);
        cstmt.setInt(3,semestre);
        cstmt.setInt(4,num_sala);
        cstmt.setInt(5,bloque_inicio);
        cstmt.setInt(6,bloque_termino);
        cstmt.setInt(7,dia_inicio);
        cstmt.setInt(8,rut_usuario);
        cstmt.registerOutParameter(9,Types.INTEGER);
        cstmt.executeQuery();
        error = cstmt.getInt(9);
        cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Horario.setEliminaHorario: "+e);
    }
    return error;
  }
  //**************************************************************************
// Metodo              : Permite agregar/modificar actividad del mantenedor
//                       (programacion y programacion_profesor )
// Programadora        : M�nica B.
// Fecha creacion      : 17/07/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setAgregaProgramacion(int año, 
		  										int semestre,
                                                int cod_asign,
                                                int paralelo,
                                                int fun_rut, 
                                                int rut_usuario,
                                                int cupo,
                                                int rama){
    String query = "";
    String par = año +","+
                 semestre +","+
                 cod_asign +","+
                 paralelo +","+
                 fun_rut +","+
                 rut_usuario +","+
                 cupo+","+
                 rama+"";
  //  query = "EXEC spu_tran_programacion "+ par;
   // System.out.println("setAgregaProgramacion: "+ query);
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_tran_programacion(?,?,?,?,?,?,?,?,?)");
        cstmt.setInt(1,año);
        cstmt.setInt(2,semestre);
        cstmt.setInt(3,cod_asign);
        cstmt.setInt(4,paralelo);
        cstmt.setInt(5,fun_rut);
        cstmt.setInt(6,rut_usuario);
        cstmt.setInt(7,cupo);
        cstmt.setInt(8,rama);
        cstmt.registerOutParameter(9,Types.INTEGER);
        cstmt.executeQuery();
        error = cstmt.getInt(9);
        cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Horario.setAgregaProgramacion: "+e);
    }
    return error;
  }
  //**************************************************************************
// Metodo              : Permite eliminar actividad del mantenedor
//                       (programacion y programacion_profesor )
// Programadora        : M�nica B.
// Fecha creacion      : 17/07/2007
// Fecha modificacion  :
//****************************************************************************
  synchronized public int setEliminaProgramacion(int año, 
		  										 int semestre,
                                                 int cod_asign, 
                                                 int paralelo){
    String query = "";
    String par = año +","+
                 semestre +","+
                 cod_asign +","+
                 paralelo +"";
   // query = "EXEC spu_dlt_programacion "+ par;
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_dlt_programacion(?,?,?,?,?)}");
        cstmt.setInt(1,año);
        cstmt.setInt(2,semestre);
        cstmt.setInt(3,cod_asign);
        cstmt.setInt(4,paralelo);
        cstmt.registerOutParameter(5,Types.INTEGER);
        cstmt.executeQuery();
        error = cstmt.getInt(5);
        cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Horario.setEliminaProgramacion: "+e.getMessage());
    }
    return error;
  }
}
