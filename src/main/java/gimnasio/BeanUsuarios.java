package gimnasio;
import java.sql.*;
import java.util.*;
//import gimnasio.*;

public class BeanUsuarios {
  private Usuario       us = null;
  private Conexion_gim   con = null;

  public BeanUsuarios() {
  }
  public void getAbrirConexion(){
    con = new Conexion_gim();
  }
  public void getCerrarConexion(){
    con.close();
  }
  public void setGenerarConexionUsuario(){
    us = new Usuario(con);
  }
  // metodos de Usuario
  public Vector getValidarUsuario (int rut_usuario){
    Vector v = new Vector();
    v = us.getValidarUsuario(rut_usuario);
    return v;
  }
  public int getPerfilUsuario  (int rut){
    int cod_perfil = 0;
    cod_perfil = us.getPerfilUsuario (rut);
    return cod_perfil;
  }
  public Vector getFuncionario (int rut_usuario){
  Vector v = new Vector();
  v = us.getFuncionario(rut_usuario);
  return v;
  }
  public Vector getUsuarioGimnasio (int rut){
    Vector v = new Vector();
  v = us.getUsuarioGimnasio(rut);
  return v;
  }
  public String getMailUsuario (int rut){
    String mail = "";
    mail = us.getMailUsuario(rut);
    return mail;
  }
  synchronized public int setBorrarPerfilUsuario(int alu_rut){
  int err = us.setBorrarPerfilUsuario(alu_rut);
  return err;
  }

  synchronized public int setInsertarPerfilUsuario(int alu_rut,
                                                   int rut_usuario,
                                                   int cod_perfil){
  int err = us.setInsertarPerfilUsuario(alu_rut, rut_usuario, cod_perfil);
  return err;
  }
}