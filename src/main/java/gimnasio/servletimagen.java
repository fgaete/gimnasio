package gimnasio;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class for Servlet: servletimagen
 *
 */
 public class servletimagen extends javax.servlet.http.HttpServlet /*implements javax.servlet.Servlet*/ {
   static final long serialVersionUID = 1L;
   
    /* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
/*	public servletimagen() {
		super();
	}   */	
   public void init(ServletConfig config) throws ServletException {
	   super.init(config); 
	   
   }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {

		HttpSession session = request.getSession(true);
		int rut_consulta = session.getAttribute("rut_consulta") != null ?
		    Integer.parseInt((String) session.getAttribute("rut_consulta")):0;
		int f = request.getParameter("f") != null ?
			Integer.parseInt(request.getParameter("f")):0;
        int cod_perfil = session.getAttribute("cod_perfil") != null?
	        Integer.parseInt(session.getAttribute("cod_perfil")+""):0;

		// rut 7777 es sesion comodin para poder enviar el rut por request
		boolean accesoGET = (f > 0 && rut_consulta == 77777777 && (cod_perfil == 2 || cod_perfil == 28));
		//System.out.println(accesoGET+"-"+rut_consulta+"-"+f);
		if (accesoGET)	 {
			rut_consulta = f;
		}
		

		if (rut_consulta > 0){

		   response.setContentType("image/gif");
		   byte [] imag = getImagen(rut_consulta);
		   if(imag != null) {
		   ServletOutputStream out = response.getOutputStream();
		   out.write(imag);
		   }
		}

	}  	
	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	} 
	
	Conexion_gim con = null;

	  public void getAbrirConexion() {
	    con = new Conexion_gim();
	  }
	  public void getCerrarConexion() {
	    con.close();
	  }
	  
	
	 public byte[] getImagen(int rut)  {
  
		 byte[] buffer = null;

		   try {	
			   //System.out.println("Abriendo conexion...");
			   getAbrirConexion();
			   
		      String sql = "SELECT FOTIND_FOTO FROM foto_individuo WHERE IND_IDENTIFICADOR = "+rut+" and cod_vigencia=1 order by tipofoto asc";
		      //System.out.println(sql);
		      Statement sent = con.conexion().createStatement();
		      ResultSet res = sent.executeQuery(sql);
		      
		      if (res.next()){
		    	  
		         Blob bin = res.getBlob("fotind_foto");
		         if (bin != null) {
		           InputStream inStream = bin.getBinaryStream();
		           int size = (int) bin.length();
		           //System.out.println(" El tamano en bytes " + size);
		           buffer = new byte[size];
		           try {
		               inStream.read(buffer, 0, size);
		           } catch (IOException ex) {
		                 ex.printStackTrace();
		           }
		         }
		         
		      }
		       res.close();
		       sent.close();
		       getCerrarConexion();
			   //System.out.println("Cerrando conexion...");

		   } 
		   catch (SQLException ex) {
			   System.out.println("ERROR " + ex);
		   return null;
		   } finally {
		   }
		   return buffer;
	 } 


	 
}