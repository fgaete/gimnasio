package gimnasio;
import java.io.*;
import java.util.*;
import java.sql.*;

import oracle.jdbc.oracore.OracleType;
import oracle.jdbc.OracleTypes;

/**
 * <p>Title: </p>
 * <p>Description: Coneccion a la BD</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: UTFSM </p>
 * @author Monica Barrera
 * @fecha  2 de junio de 2007
 * @version 1.0
 */

public class Reserva {
  private Conexion_gim con;

  public Reserva() {
  }

  public Reserva(Conexion_gim con) {
    this.con = con;
  }

  //**************************************************************************
  // Metodo              : Permite capturar los per�odos del gimnasio
  //                       correspondientes a un per�odo
  // Programadora        : Monica B.
  // Fecha creacion      : 21/6/2007
  // Fecha modificacion  :
  //****************************************************************************
  public Vector getPeriodoGimnasio (int año, int semestre, int sala){
    java.util.Vector v = new java.util.Vector();
    java.util.Vector a = null;
    String query = " SELECT TO_CHAR(pergim_fecha_inicio_vig,'dd/mm/yyyy'), " +
                   " TO_CHAR(pergim_fecha_termino_vig,'dd/mm/yyyy')," +
                   " TO_CHAR(pergim_fecha_inicio_insc,'dd/mm/yyyy')," +
                   " TO_CHAR(pergim_fecha_termino_insc,'dd/mm/yyyy')," +
                   " pergim_fecha_cierre, pergim_id," +
                   " TO_CHAR(pergim_fecha_inicio_vig,'yyyymmdd'), " +
                   " TO_CHAR(pergim_fecha_termino_vig,'yyyymmdd')" +
                   " FROM periodo_gimnasio" +
                   " WHERE  pergim_año = " + año +
                   " AND  pergim_semestre = " + semestre +
                   " AND  num_sala = " + sala +
                   " ORDER BY pergim_fecha_inicio_vig desc" ;
//System.out.println("getPeriodoGimnasio: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        a = new java.util.Vector();
        a.addElement(res.getString(1)); // 0 fecha_inicio_vig
        a.addElement(res.getString(2)); // 1 fecha_termino_vig
        a.addElement(res.getString(3)); // 2 fecha_inicio_insc
        a.addElement(res.getString(4)); // 3 fecha_termino_insc
        a.addElement(res.getString(5)); // 4 fecha_cierre
        a.addElement(res.getString(6)); // 5 id
        a.addElement(res.getString(7)); // 6 fecha_inicio_vig aaaammdd
        a.addElement(res.getString(8)); // 7 fecha_termino_vig aaaammdd

        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getPeriodoGimnasio : "+e);
    }
    return v;
  }
  //**************************************************************************
// Metodo              : Consulta si existe algun registro de sancionado o
//                       premiado correspondientes a un per�odo.
// Programadora        : Monica B.
// Fecha creacion      : 24/09/2007
// Fecha modificacion  :
//****************************************************************************
public Vector getPremiadoSancionado (String pergim_id){
  java.util.Vector v = new java.util.Vector();
  java.util.Vector a = null;
  String query = " SELECT reggim_id, cod_asistencia" +
                 " FROM estado_asistencia_gimnasio" +
                 " WHERE pergim_id = " + pergim_id +
                 " AND (cod_asistencia = 5 OR cod_asistencia = 6)" ;
//System.out.println("getPremiadoSancionado: "+ query);
  try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    while(res.next()){
      a = new java.util.Vector();
      a.addElement(res.getString(1)); // 0 reggim_id
      a.addElement(res.getString(2)); // 1 cod_asistencia
      v.addElement(a);
    }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Reserva.getPremiadoSancionado : "+e);
  }
  return v;
  }

  //**************************************************************************
  // Metodo              : Permite capturar los tipos de reserva del gimnasio

  // Programadora        : Monica B.
  // Fecha creacion      : 10/08/2007
  // Fecha modificacion  :
  //****************************************************************************
  public Vector getTipoReserva (){
    java.util.Vector v = new java.util.Vector();
    java.util.Vector a = null;
    String query = " SELECT cod_reserva,nom_reserva" +
                   " FROM tipo_reserva" +
                   " WHERE cod_reserva > 0";
//System.out.println("getTipoReserva: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        a = new java.util.Vector();
        a.addElement(res.getString(1)); // 0 cod_reserva
        a.addElement(res.getString(2)); // 1 nom_reserva
        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getTipoReserva : "+e);
    }
    return v;
  }

  //**************************************************************************
  // Metodo              : Permite capturar el nombre del tipo de reserva

  // Programadora        : Monica B.
  // Fecha creacion      : 24/08/2007
  // Fecha modificacion  :
  //****************************************************************************
  public Vector getNomTipoReserva (String resgim_id){
    java.util.Vector v = new java.util.Vector();
    String query = " SELECT tr.cod_reserva, tr.nom_reserva" +
                   " FROM reserva_gimnasio rg, horario_asignatura ha, tipo_reserva tr" +
                   " WHERE rg.resgim_id =" + resgim_id +
                   " AND rg.pro_año = ha.pro_año" +
                   " AND rg.pro_semestre = ha.pro_semestre" +
                   " AND rg.cod_asign = ha.cod_asign" +
                   " AND rg.pro_paralelo_asignatura = ha.pro_paralelo_asignatura" +
                   " AND rg.cod_jornada = ha.cod_jornada" +
                   " AND rg.sed_cod_sede = ha.sed_cod_sede" +
                   " AND rg.num_dia = ha.num_dia" +
                   " AND rg.cod_bloque_inicio = ha.cod_bloque_inicio" +
                   " AND rg.cod_bloque_termino = ha.cod_bloque_termino" +
                   " AND ha.cod_reserva = tr.cod_reserva";
//System.out.println("getNomTipoReserva: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        v.addElement(res.getString(1)); // 0 cod_reserva
        v.addElement(res.getString(2)); // 1 nom_reserva
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getNomTipoReserva : "+e);
    }
    return v;
  }

  //**************************************************************************
  // Metodo              : Permite capturar los per�odos del gimnasio
  //                       correspondientes vigentes
  // Programadora        : Monica B.
  // Fecha creacion      : 18/7/2007
  // Fecha modificacion  :
  //****************************************************************************
  public Vector getPeriodoGimnasioVigente(int sala){
    java.util.Vector v = new java.util.Vector();
    String query = " SELECT TO_CHAR(pergim_fecha_inicio_vig,'dd/mm/yyyy'), " +
                   " TO_CHAR(pergim_fecha_termino_vig,'dd/mm/yyyy')," +
                   " pergim_id, pergim_año, pergim_semestre," +
                   " TO_CHAR(pergim_fecha_inicio_vig,'yyyymmdd')," +
                   " TO_CHAR(pergim_fecha_termino_vig,'yyyymmdd')," +
                   " TO_CHAR(pergim_fecha_inicio_insc,'yyyymmdd')," +
                   " TO_CHAR(pergim_fecha_termino_insc,'yyyymmdd')" +
                   " FROM periodo_gimnasio" +
                   " WHERE TO_CHAR(pergim_fecha_inicio_vig,'yyyymmdd' )  <=" +
                   " TO_CHAR(SYSDATE,'yyyymmdd')" +
                   " AND TO_CHAR(SYSDATE,'yyyymmdd') <= " +
                   " TO_CHAR(pergim_fecha_termino_vig,'yyyymmdd' ) " +
                   " AND num_sala = " + sala +
                   " ORDER BY pergim_fecha_inicio_vig desc" ;
    
//System.out.println("getPeriodoGimnasioVigente: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        v.addElement(res.getString(1)); // 0 fecha_inicio_vig
        v.addElement(res.getString(2)); // 1 fecha_termino_vig
        v.addElement(res.getString(3)); // 2 id
        v.addElement(res.getString(4)); // 3 año
        v.addElement(res.getString(5)); // 4 semestre
        v.addElement(res.getString(6)); // 5 inicio aammdd
        v.addElement(res.getString(7)); // 6 termino aammdd
        v.addElement(res.getString(8)); // 7 inicio insc aammdd
        v.addElement(res.getString(9)); // 8 termino insc aammdd
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getPeriodoGimnasioVigentes : "+e);
    }
    return v;
  }
  //**************************************************************************
 // Metodo              : Permite capturar el periodo del gimnasio segun fecha y sala
 // Programadora        : Monica B.
 // Fecha creacion      : 13/11/2007
 // Fecha modificacion  :
 //****************************************************************************
 public Vector getPeriodoGimnasioSala(int sala, String fecha){
   java.util.Vector v = new java.util.Vector();
   String query = " SELECT  TO_CHAR(pergim_fecha_inicio_vig,'dd/mm/yyyy')," +
                  " TO_CHAR(pergim_fecha_termino_vig,'dd/mm/yyyy')," +
                  " pergim_id, pergim_año, pergim_semestre," +
                  " TO_CHAR(pergim_fecha_inicio_vig,'yyyymmdd')," +
                  " TO_CHAR(pergim_fecha_termino_vig,'yyyymmdd')," +
                  " TO_CHAR(pergim_fecha_inicio_insc,'yyyymmdd')," +
                  " TO_CHAR(pergim_fecha_termino_insc,'yyyymmdd')" +
                  " FROM periodo_gimnasio" +
                  " WHERE '" + fecha + "' >= TO_CHAR(pergim_fecha_inicio_vig,'yyyymmdd')" +
                  " AND  '" + fecha + "' <= TO_CHAR(pergim_fecha_termino_vig,'yyyymmdd')" +
                  " AND num_sala = " + sala +
                  " ORDER BY pergim_fecha_inicio_vig desc" ;
//System.out.println("getPeriodoGimnasio: "+ query);
   try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     while(res.next()){
       v.addElement(res.getString(1)); // 0 fecha_inicio_vig
       v.addElement(res.getString(2)); // 1 fecha_termino_vig
       v.addElement(res.getString(3)); // 2 id
       v.addElement(res.getString(4)); // 3 año
       v.addElement(res.getString(5)); // 4 semestre
       v.addElement(res.getString(6)); // 5 inicio aammdd
       v.addElement(res.getString(7)); // 6 termino aammdd
       v.addElement(res.getString(8)); // 7 inicio insc aammdd
       v.addElement(res.getString(9)); // 8 termino insc aammdd
     }
     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Reserva.getPeriodoGimnasioVigentes : "+e);
   }
   return v;
  }
  //**************************************************************************
  // Metodo              : Permite capturar la asistencia dentro del per�odo
  //                       vigente
  // Programadora        : Monica B.
  // Fecha creacion      : 25/7/2007
  // Fecha modificacion  :
  //****************************************************************************
  public Vector getAsistenciaGimnasio(int rut, String fec_inicio, String fec_termino,
                                      String pergim_id){
    java.util.Vector v = new java.util.Vector();
    java.util.Vector a = new java.util.Vector();
    String dia = "";
    String query = " SELECT rg.num_dia, TO_CHAR(a.asigim_fecha,'dd/mm/yyyy') as fecha," +
                   " a.cod_asistencia, rg.cod_bloque_inicio, rg.cod_bloque_termino," + 
                   " rg.sed_cod_sede, rg.cod_jornada," +
                   " TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy'), " +
                   " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy') " +
                   " FROM asistencia_gimnasio a, reserva_gimnasio rg,  "+
                   " registro_gimnasio_alumno rga, registro_gimnasio regg" +
                   " WHERE to_date('" + fec_inicio + "','yyyymmdd') <= a.asigim_fecha" +
                   " AND a.asigim_fecha <= to_date('" + fec_termino+ "','yyyymmdd')" +
                   " AND rg.reggim_id = rga.reggim_id" +
                   " AND rga.alu_rut = " + rut +
                   " AND a.resgim_id = rg.resgim_id" +
                   " AND rg.pergim_id = " + pergim_id +
                   " AND regg.reggim_id = rga.reggim_id" +
                   " AND regg.reggim_fecha_termino IS NULL"+
                   " UNION " +
                   " SELECT rg.num_dia, TO_CHAR(a.asigim_fecha,'dd/mm/yyyy')  as fecha," +
                   " a.cod_asistencia, rg.cod_bloque_inicio, rg.cod_bloque_termino," + 
                   " rg.sed_cod_sede, rg.cod_jornada," +
                   " TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy'), " +
                   " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy') " +
                   " FROM asistencia_gimnasio a, reserva_gimnasio rg," +
                   " registro_gimnasio_funcionario rgf," +
                   " registro_gimnasio regg" +
                   " WHERE to_date('" + fec_inicio + "','yyyymmdd') <= a.asigim_fecha" +
                   " AND a.asigim_fecha <= to_date('" + fec_termino+ "','yyyymmdd')" +
                   " AND rg.reggim_id = rgf.reggim_id" +
                   " AND rgf.fun_rut = " + rut +
                   " AND a.resgim_id = rg.resgim_id" +
                   " AND rg.pergim_id = " + pergim_id +
                   " AND regg.reggim_id = rgf.reggim_id" +
                   " AND regg.reggim_fecha_termino IS NULL"+
                   " UNION "+
                   " SELECT rg.num_dia, TO_CHAR(a.asigim_fecha,'dd/mm/yyyy')  as fecha," +
                   " a.cod_asistencia, rg.cod_bloque_inicio, rg.cod_bloque_termino," + 
                   " rg.sed_cod_sede, rg.cod_jornada," +
                   " TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy'), " +
                   " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy') " +
                   " FROM asistencia_gimnasio a, reserva_gimnasio rg,  "+
                   " registro_gimnasio_externo rge, registro_gimnasio regg" +
                   " WHERE to_date('" + fec_inicio + "','yyyymmdd') <= a.asigim_fecha" +
                   " AND a.asigim_fecha <= to_date('" + fec_termino+ "','yyyymmdd')" +
                   " AND rg.reggim_id = rge.reggim_id" +
                   " AND rge.regext_rut = " + rut +
                   " AND a.resgim_id = rg.resgim_id" +
                   " AND rg.pergim_id = " + pergim_id +
                   " AND regg.reggim_id = rge.reggim_id" +
                   " AND regg.reggim_fecha_termino IS NULL" +
                   " ORDER BY fecha";
    //System.out.println("query "+query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
    	  
        a = new java.util.Vector();
        a.addElement(res.getString(1)); // 0 num_dia
        a.addElement(res.getString(2)); // 1 fecha
        a.addElement(res.getString(3)); // 2 cod_asistencia
        String hi = getHoraBloque(res.getInt(4), res.getInt(6), res.getInt(7), 1);
        String ht = getHoraBloque(res.getInt(5), res.getInt(6), res.getInt(7), 2);
        
        a.addElement(hi); // 3 hora_inicio
        a.addElement(ht); // 4 hora_termino
        switch (res.getInt(1)){
          case 1 : dia = "Lunes";
            break;
          case 2 : dia = "Martes";
            break;
          case 3 : dia = "Mi�rcoles";
            break;
          case 4 : dia = "Jueves";
            break;
          case 5 : dia = "Viernes";
            break;
          case 6 : dia = "S�bado";
            break;
        }
        a.addElement(dia); // 5 dia en palabras
        a.addElement(res.getString(8)); // 6 fecha_inicio
        a.addElement(res.getString(9)); // 7 fecha_termino
        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getAsistenciaGimnasio : "+e);
    }
    
    return v;
    
  }
  
  public Vector getAsistenciaGimnasio2(int rut, String fec_inicio,
			String fec_termino, String pergim_id) {
		java.util.Vector v = new java.util.Vector();
		java.util.Vector a = new java.util.Vector();
		String dia = "";
		String query = " SELECT rg.num_dia,  NVL(TO_CHAR(A.asigim_fecha,'dd/mm/yyyy'),'Pendiente') as fecha,"
				+ " NVL(A.cod_asistencia,0), rg.cod_bloque_inicio, rg.cod_bloque_termino,"
				+ " rg.sed_cod_sede, rg.cod_jornada,"
				+ " TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy'), "
				+ " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy'), "
				+ " NVL(alu.alu_ap_paterno,NVL(fun.fun_ap_paterno,' ')), NVL(alu.alu_ap_materno,NVL(fun.fun_ap_materno,' ')), NVL(alu.alu_nombres,NVL(fun.fun_nombres,' ')), TO_CHAR(rg.fecha_modificacion,'dd/mm/yyyy'), "
				+ " NVL(alu2.alu_ap_paterno,NVL(fun2.fun_ap_paterno,' ')), NVL(alu2.alu_ap_materno,NVL(fun2.fun_ap_materno,' ')), NVL(alu2.alu_nombres,NVL(fun2.fun_nombres,' ')), NVL(TO_CHAR(A.fecha_modificacion,'dd/mm/yyyy'),' ') "
                + " FROM reserva_gimnasio rg  "
				+ " LEFT JOIN asistencia_gimnasio A"
				+ " ON A.asigim_fecha BETWEEN to_date('"+ fec_inicio+ "','yyyymmdd') AND to_date('"+ fec_termino+ "','yyyymmdd')"
				+ " AND A.resgim_id = rg.resgim_id "
				+ " LEFT JOIN alumno alu" 
				+ " ON alu.alu_rut = rg.rut_usuario"
				+ " LEFT JOIN funcionario fun" 
				+ " ON fun.fun_rut = rg.rut_usuario"
				+ " LEFT JOIN alumno alu2" 
				+ " ON alu2.alu_rut = A.rut_usuario"
				+ " LEFT JOIN funcionario fun2" 
				+ " ON fun2.fun_rut = A.rut_usuario,"
				+ " registro_gimnasio_alumno rga, registro_gimnasio regg"
				+ " WHERE rg.reggim_id = rga.reggim_id"
				+ " AND rga.alu_rut = "	+ rut
				+ " AND rg.pergim_id = "+ pergim_id
				+ " AND regg.reggim_id = rga.reggim_id"
				+ " AND regg.reggim_fecha_termino IS NULL"
				+ " UNION "
				+ " SELECT rg.num_dia, TO_CHAR(a.asigim_fecha,'dd/mm/yyyy')  as fecha,"
				+ " a.cod_asistencia, rg.cod_bloque_inicio, rg.cod_bloque_termino,"
				+ " rg.sed_cod_sede, rg.cod_jornada,"
				+ " TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy'), "
				+ " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy'), "
				+ " NVL(alu.alu_ap_paterno,NVL(fun.fun_ap_paterno,' ')), NVL(alu.alu_ap_materno,NVL(fun.fun_ap_materno,' ')), NVL(alu.alu_nombres,NVL(fun.fun_nombres,' ')), TO_CHAR(rg.fecha_modificacion,'dd/mm/yyyy'), "
				+ " NVL(alu2.alu_ap_paterno,NVL(fun2.fun_ap_paterno,' ')), NVL(alu2.alu_ap_materno,NVL(fun2.fun_ap_materno,' ')), NVL(alu2.alu_nombres,NVL(fun2.fun_nombres,' ')), NVL(TO_CHAR(A.fecha_modificacion,'dd/mm/yyyy'),' ') "
				+ " FROM reserva_gimnasio rg  "
				+ " LEFT JOIN asistencia_gimnasio A"
				+ " ON A.asigim_fecha BETWEEN to_date('"+ fec_inicio+ "','yyyymmdd') AND to_date('"+ fec_termino+ "','yyyymmdd')"
				+ " AND A.resgim_id = rg.resgim_id "
				+ " LEFT JOIN alumno alu" 
				+ " ON alu.alu_rut = rg.rut_usuario"
				+ " LEFT JOIN funcionario fun" 
				+ " ON fun.fun_rut = rg.rut_usuario"
				+ " LEFT JOIN alumno alu2" 
				+ " ON alu2.alu_rut = A.rut_usuario"
				+ " LEFT JOIN funcionario fun2" 
				+ " ON fun2.fun_rut = A.rut_usuario,"
				+ " registro_gimnasio_funcionario rgf,"
				+ " registro_gimnasio regg"
				+ " WHERE rg.reggim_id = rgf.reggim_id"
				+ " AND rgf.fun_rut = "	+ rut
				+ " AND rg.pergim_id = "+ pergim_id
				+ " AND regg.reggim_id = rgf.reggim_id"
				+ " AND regg.reggim_fecha_termino IS NULL"
				+ " UNION "
				+ " SELECT rg.num_dia, TO_CHAR(a.asigim_fecha,'dd/mm/yyyy')  as fecha,"
				+ " a.cod_asistencia, rg.cod_bloque_inicio, rg.cod_bloque_termino,"
				+ " rg.sed_cod_sede, rg.cod_jornada,"
				+ " TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy'), "
				+ " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy'), "
				+ " NVL(alu.alu_ap_paterno,NVL(fun.fun_ap_paterno,' ')), NVL(alu.alu_ap_materno,NVL(fun.fun_ap_materno,' ')), NVL(alu.alu_nombres,NVL(fun.fun_nombres,' ')), TO_CHAR(rg.fecha_modificacion,'dd/mm/yyyy'), "
				+ " NVL(alu2.alu_ap_paterno,NVL(fun2.fun_ap_paterno,' ')), NVL(alu2.alu_ap_materno,NVL(fun2.fun_ap_materno,' ')), NVL(alu2.alu_nombres,NVL(fun2.fun_nombres,' ')), NVL(TO_CHAR(A.fecha_modificacion,'dd/mm/yyyy'),' ') "
				+ " FROM reserva_gimnasio rg  "
				+ " LEFT JOIN asistencia_gimnasio A"
				+ " ON A.asigim_fecha BETWEEN to_date('"+ fec_inicio+ "','yyyymmdd') AND to_date('"+ fec_termino+ "','yyyymmdd')"
				+ " AND A.resgim_id = rg.resgim_id "
				+ " LEFT JOIN alumno alu" 
				+ " ON alu.alu_rut = rg.rut_usuario"
				+ " LEFT JOIN funcionario fun" 
				+ " ON fun.fun_rut = rg.rut_usuario"
				+ " LEFT JOIN alumno alu2" 
				+ " ON alu2.alu_rut = A.rut_usuario"
				+ " LEFT JOIN funcionario fun2" 
				+ " ON fun2.fun_rut = A.rut_usuario,"
				+ " registro_gimnasio_externo rge, registro_gimnasio regg"
				+ " WHERE rg.reggim_id = rge.reggim_id"
				+ " AND rge.regext_rut = "+ rut
				+ " AND rg.pergim_id = "+ pergim_id
				+ " AND regg.reggim_id = rge.reggim_id"
				+ " AND regg.reggim_fecha_termino IS NULL" 
				+ " ORDER BY fecha";
		//System.out.println("query "+query);
		try {
			Statement sent = con.conexion().createStatement();
			ResultSet res = sent.executeQuery(query);
			while (res.next()) {

				a = new java.util.Vector();
				a.addElement(res.getString(1)); // 0 num_dia
				a.addElement(res.getString(2)); // 1 fecha
				a.addElement(res.getString(3)); // 2 cod_asistencia
				String hi = getHoraBloque(res.getInt(4), res.getInt(6), res.getInt(7), 1);
				String ht = getHoraBloque(res.getInt(5), res.getInt(6), res.getInt(7), 2);

				a.addElement(hi); // 3 hora_inicio
				a.addElement(ht); // 4 hora_termino
				switch (res.getInt(1)) {
				case 1:
					dia = "Lunes";
					break;
				case 2:
					dia = "Martes";
					break;
				case 3:
					dia = "Mi�rcoles";
					break;
				case 4:
					dia = "Jueves";
					break;
				case 5:
					dia = "Viernes";
					break;
				case 6:
					dia = "S�bado";
					break;
				}
				a.addElement(dia); // 5 dia en palabras
				a.addElement(res.getString(8)); // 6 fecha_inicio
				a.addElement(res.getString(9)); // 7 fecha_termino
				a.addElement(res.getString(10).trim().length() > 0?((res.getString(12).trim().length() > 0?res.getString(12).substring(0, 1) + ". ":"") + res.getString(10) + " " + (res.getString(11).trim().length() > 0?res.getString(11).substring(0, 1)+". ":"")+res.getString(13)):""); // 8 usuario
				a.addElement(res.getString(14).trim().length() > 0?((res.getString(16).trim().length() > 0?res.getString(16).substring(0, 1) + ". ":"") + res.getString(14) + " " + (res.getString(15).trim().length() > 0?res.getString(15).substring(0, 1)+". ":"")+res.getString(17)):""); // 8 usuario
				v.addElement(a);
			}
			res.close();
			sent.close();
		} catch (SQLException e) {
			System.out.println("Error en Reserva.getAsistenciaGimnasio2 : " + e);
		}

		return v;

	}
  
  public String getHoraBloque(int bloque, int sede, int jornada, int tipo){
	  String hora = "";
	  String query = "";

      query = " SELECT hora_inicio, hora_termino FROM hora_bloque" +
		  " WHERE cod_bloque = " + bloque +
          " AND sed_cod_sede = " + sede +
          " AND cod_jornada = " + jornada;
	  
      try{
    	  Statement sent = con.conexion().createStatement();
    	  ResultSet res = sent.executeQuery(query);
    	  if(res.next()){
    		  //hora_inicio
    		  if (tipo == 1) hora = res.getString(1);
    		  //hora_termino
    		  if (tipo == 2) hora = res.getString(2);

    	  }
    	  res.close();
    	  sent.close();
      }
      catch(SQLException e){
    	  System.out.println("Error en Reserva.getHoraBloque : "+e);
      }
      return hora;

}
  //**************************************************************************
   // Metodo              : Permite capturar la asistencia dentro del per�odo
   //                       y bloque
   // Programadora        : Monica B.
   // Fecha creacion      : 19/10/2007
   // Fecha modificacion  :
   //****************************************************************************
  public Vector getAsistenciaBloque(int rut, String fec_inicio, String fec_termino,
                                      String pergim_id, int blo1, int blo2){
    java.util.Vector v = new java.util.Vector();
    java.util.Vector a = new java.util.Vector();
    String dia = "";
    String query = " SELECT rg.num_dia, TO_CHAR(a.asigim_fecha,'dd/mm/yyyy') as fecha," +
                   " a.cod_asistencia, h1.hora_inicio, h2.hora_termino," +
                   " TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy'), " +
                   " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy') " +
                   " FROM  asistencia_gimnasio a, reserva_gimnasio rg, registro_gimnasio_alumno rga "+
                   " , hora_bloque h1, hora_bloque h2, registro_gimnasio regg" +
                   " WHERE '" + fec_inicio + "'<= TO_CHAR(a.asigim_fecha,'yyyymmdd')" +
                   " AND TO_CHAR(a.asigim_fecha,'yyyymmdd') <= '" + fec_termino+ "'" +
                   " AND rg.reggim_id = rga.reggim_id" +
                   " AND rga.alu_rut = " + rut +
                   " AND a. resgim_id = rg.resgim_id" +
                   " AND rg.cod_bloque_inicio = h1.cod_bloque" +
                   " AND rg.sed_cod_sede = h1.sed_cod_sede" +
                   " AND rg.cod_jornada = h1.cod_jornada" +
                   " AND rg.cod_bloque_termino = h2.cod_bloque" +
                   " AND rg.sed_cod_sede = h2.sed_cod_sede" +
                   " AND rg.cod_jornada = h2.cod_jornada" +
                   " AND rg.pergim_id = " + pergim_id +
                   " AND regg.reggim_id = rga.reggim_id" +
                   " AND regg.reggim_fecha_termino IS NULL"+
                   " AND rg.cod_bloque_inicio = " + blo1 +
                   " AND rg.cod_bloque_termino = " + blo2 +
                   " UNION " +
                   " SELECT rg.num_dia, TO_CHAR(a.asigim_fecha,'dd/mm/yyyy') as fecha," +
                   " a.cod_asistencia, h1.hora_inicio, h2.hora_termino," +
                   " TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy'), " +
                   " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy') " +
                   " FROM  asistencia_gimnasio a, reserva_gimnasio rg," +
                   " registro_gimnasio_funcionario rgf, hora_bloque h1, hora_bloque h2" +
                   " , registro_gimnasio regg" +
                   " WHERE '" + fec_inicio + "'<= TO_CHAR(a.asigim_fecha,'yyyymmdd')" +
                   " AND TO_CHAR(a.asigim_fecha,'yyyymmdd') <= '" + fec_termino+ "'" +
                   " AND rg.reggim_id = rgf.reggim_id" +
                   " AND rgf.fun_rut = " + rut +
                   " AND a. resgim_id = rg.resgim_id" +
                   " AND rg.cod_bloque_inicio = h1.cod_bloque" +
                   " AND rg.sed_cod_sede = h1.sed_cod_sede" +
                   " AND rg.cod_jornada = h1.cod_jornada" +
                   " AND rg.cod_bloque_termino = h2.cod_bloque" +
                   " AND rg.sed_cod_sede = h2.sed_cod_sede" +
                   " AND rg.cod_jornada = h2.cod_jornada" +
                   " AND rg.pergim_id = " + pergim_id +
                   " AND regg.reggim_id = rgf.reggim_id" +
                   " AND regg.reggim_fecha_termino IS NULL"+
                   " AND rg.cod_bloque_inicio = " + blo1 +
                   " AND rg.cod_bloque_termino = " + blo2 +
                   " UNION "+
                   " SELECT rg.num_dia, TO_CHAR(a.asigim_fecha,'dd/mm/yyyy') as fecha," +
                   " a.cod_asistencia, h1.hora_inicio, h2.hora_termino," +
                   " TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy'), " +
                   " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy') " +
                   " FROM  asistencia_gimnasio a, reserva_gimnasio rg, registro_gimnasio_externo rge "+
                   " , hora_bloque h1, hora_bloque h2 , registro_gimnasio regg" +
                   " WHERE '" + fec_inicio + "'<= TO_CHAR(a.asigim_fecha,'yyyymmdd')" +
                   " AND TO_CHAR(a.asigim_fecha,'yyyymmdd') <= '" + fec_termino+ "'" +
                   " AND rg.reggim_id = rge.reggim_id" +
                   " AND rge.regext_rut = " + rut +
                   " AND a. resgim_id = rg.resgim_id" +
                   " AND rg.cod_bloque_inicio = h1.cod_bloque" +
                   " AND rg.sed_cod_sede = h1.sed_cod_sede" +
                   " AND rg.cod_jornada = h1.cod_jornada" +
                   " AND rg.cod_bloque_termino = h2.cod_bloque" +
                   " AND rg.sed_cod_sede = h2.sed_cod_sede" +
                   " AND rg.cod_jornada = h2.cod_jornada" +
                   " AND rg.pergim_id = " + pergim_id +
                   " AND regg.reggim_id = rge.reggim_id" +
                   " AND regg.reggim_fecha_termino IS NULL" +
                   " AND rg.cod_bloque_inicio = " + blo1 +
                   " AND rg.cod_bloque_termino = " + blo2 +
                   " ORDER BY fecha";
//System.out.println("getAsistenciaGimnasio: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        a = new java.util.Vector();
        a.addElement(res.getString(1)); // 0 num_dia
        a.addElement(res.getString(2)); // 1 fecha
        a.addElement(res.getString(3)); // 2 cod_asistencia
        a.addElement(res.getString(4)); // 3 hora_inicio
        a.addElement(res.getString(5)); // 4 hora_termino
        switch (res.getInt(1)){
          case 1 : dia = "Lunes";
            break;
          case 2 : dia = "Martes";
            break;
          case 3 : dia = "Mi�rcoles";
            break;
          case 4 : dia = "Jueves";
            break;
          case 5 : dia = "Viernes";
            break;
          case 6 : dia = "S�bado";
            break;
        }
        a.addElement(dia); // 5 dia en palabras
        a.addElement(res.getString(6)); // 6 fecha_inicio
        a.addElement(res.getString(7)); // 7 fecha_termino
        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getAsistenciaGimnasio : "+e);
    }
    return v;
  }
  //**************************************************************************
  // Metodo              : Permite capturar los alumnos premiados del periodo o
  //                       sancionados
  // Programadora        : Monica B.
  // Fecha creacion      : 26/6/2007
  // Fecha modificacion  :
  //****************************************************************************
  public Vector getPremiadosPeriodoList (int año, int semestre,
      String periodo, int cod_estado){
    java.util.Vector v = new java.util.Vector();
    java.util.Vector a = null;

    String cond =(periodo.trim().equals("0"))?"":" AND pg.pergim_id = " + periodo;

    String query = " SELECT a.alu_ap_paterno as paterno, NVL(a.alu_ap_materno,' ') as materno," +
                   " a.alu_nombres as nombres, TO_CHAR(a.alu_rut) || '-' || a.alu_dig_rut," +
                   " a.alu_email_personal," +
                   " TO_CHAR(pg.pergim_fecha_inicio_vig,'dd/mm/yyyy')," +
                   " TO_CHAR(pg.pergim_fecha_termino_vig,'dd/mm/yyyy')," +
                   " eag.reggim_id , rga.alu_rut, pg.pergim_id" +
                   " FROM periodo_gimnasio pg,  estado_asistencia_gimnasio eag," +
                   " registro_gimnasio regg,  registro_gimnasio_alumno rga, alumno a" +
                   " WHERE pg.pergim_año = " + año + " AND" +
                   " pg.pergim_semestre = " + semestre +
                   cond +
                   " AND " +
                   " eag.pergim_id = pg.pergim_id AND" +
                   " eag.cod_asistencia = " + cod_estado + " AND" +
                   " eag.reggim_id = regg.reggim_id AND" +
                   " regg.reggim_id = rga.reggim_id AND" +
                 //  " regg.cod_registro_gimnasio = 1 AND" +
                   " rga.alu_rut = a.alu_rut" +
                   " UNION " +
                   " SELECT ' ' as paterno, ' ' as materno,' ' as nombres, ' ',' '," +
                  " TO_CHAR(pg.pergim_fecha_inicio_vig,'dd/mm/yyyy')," +
                  " TO_CHAR(pg.pergim_fecha_termino_vig,'dd/mm/yyyy')," +
                  " eag.reggim_id, rge.regext_rut, pg.pergim_id" +
                  " FROM periodo_gimnasio pg,  estado_asistencia_gimnasio eag," +
                  " registro_gimnasio regg,  registro_gimnasio_externo rge" +
                  " WHERE pg.pergim_año = " + año + " AND" +
                  " pg.pergim_semestre = " + semestre +
                  cond + " AND" +
                  " eag.pergim_id = pg.pergim_id AND" +
                  " eag.cod_asistencia = " + cod_estado + " AND" +
                  " eag.reggim_id = regg.reggim_id AND" +
                  " regg.reggim_id = rge.reggim_id AND" +
                  " regg.cod_registro_gimnasio = 5 " +
                  " ORDER BY paterno, materno, nombres" ;//9 = pg.pergim_id
    //gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
    //b.getAbrirConexionVi�a();
    //b.setGenerarConexionDatosVi�a();
    Vector vec_al = new Vector();
    String paterno = "";
    String materno = "";
    String nombres = "";
    String rut = "";
    String email = "";

//System.out.println("getPremiadosPeriodoList: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        /*if (res.getString(1).equals("")) {// es de vi�a
        // es de vi�a
        vec_al = b.getAlumnoVi�a(res.getInt(5));
        if (vec_al.size() > 0){
          paterno = vec_al.get(14)+"";
          materno = vec_al.get(15)+"";
          nombres = vec_al.get(13)+"";
          rut     = vec_al.get(12)+"";
          email   = vec_al.get(4)+"";
        }
        } else {*/
          paterno = res.getString(1);
          materno = res.getString(2);
          nombres = res.getString(3);
          rut     = res.getString(4);
          email   = res.getString(5);
        //}
        a = new java.util.Vector();
        a.addElement(paterno); // 0 paterno
        a.addElement(materno); // 1 materno
        a.addElement(nombres); // 2 nombres
        a.addElement(rut); // 3 rut
        a.addElement(email); // 4 email
        a.addElement(res.getString(6)); // 5 fecha Incio vigencia
        a.addElement(res.getString(7)); // 6 fecha termino vigencia
        a.addElement(res.getString(8)); // 7 reggim_id
        a.addElement(res.getString(10)); // 8 pergim_id
        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getPremiadosPeriodoList : "+e);
    }
    //b.getCerrarConexionVi�a();
    return v;
  }

  //**************************************************************************
 // Metodo              : Permite capturar las reservas del alumno de las fechas
 //                       solo alumno tiene reservas, funcionario no
 // Programadora        : Monica B.
 // Fecha creacion      : 21/07/2007
 // Fecha modificacion  : 17/03/2008 por peticion no se deben incluir las asignaciones libres
 //****************************************************************************
 public Vector getBloqueReserva (int rut, String periodo, int fecha_inicio,
                                 int fecha_termino){
   java.util.Vector v = new java.util.Vector();
   java.util.Vector a = null;
   java.util.Vector vec_res = null;
   java.util.Vector vec_asis = new Vector();
   String dia = "";
   String finicio = "";
   String ftermino = "";
   boolean seguir = true;
   String query = " SELECT rg.resgim_id, rg.cod_bloque_inicio , rg.cod_bloque_termino," +
                  " rg.num_dia, TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy')," +
                  " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy'), a.nom_asign" +
                  " FROM registro_gimnasio_alumno rgf, reserva_gimnasio rg,"+
                  " asignatura a, registro_gimnasio regg" +
                  " WHERE rgf.alu_rut = " + rut +
                  " AND rgf.reggim_id = rg.reggim_id" +
                  " AND ((TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd') >= " +
                  fecha_inicio +
                  " AND TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd') <= " +
                  fecha_termino +
                  ") OR (TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd') <= " +
                  fecha_inicio+
                  "  AND TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd') >= " +
                  fecha_termino+ "))" +
                  " AND rg.pergim_id  = " + periodo +
                  " AND regg.reggim_id = rg.reggim_id" +
                  " AND regg.reggim_fecha_termino is null"+
                  " AND rg.cod_asign = a.cod_asign" +
                  " AND a.estado_nulo = 1" +
                  " UNION " +
                  " SELECT rg.resgim_id, rg.cod_bloque_inicio , rg.cod_bloque_termino," +
                  " rg.num_dia, TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy')," +
                  " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy'), a.nom_asign" +
                  " FROM registro_gimnasio_externo rgf, reserva_gimnasio rg,"+
                  " asignatura a, registro_gimnasio regg" +
                  " WHERE rgf.regext_rut = " + rut +
                  " AND rgf.reggim_id = rg.reggim_id" +
                  " AND ((TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd') >= " +
                  fecha_inicio +
                  " AND TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd') <= " +
                  fecha_termino +
                  ") OR (TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd') <= " +
                  fecha_inicio+
                  "  AND TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd') >= " +
                  fecha_termino+ "))" +
                  " AND rg.pergim_id  = " + periodo +
                  " AND regg.reggim_id = rg.reggim_id" +
                  " AND regg.reggim_fecha_termino is null"+
                  " AND rg.cod_asign = a.cod_asign" +
                  " AND a.estado_nulo = 1"+
                  " ORDER BY 4";//4 = rg.num_dia

//System.out.println("getBloqueReserva: "+ query);
   try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     while(res.next()){
       seguir = true;
       vec_asis = new Vector();
       vec_asis = getAsistenciaReserva (res.getString(1),fecha_inicio,fecha_termino);
       if (vec_asis.size() > 0) {
         for (int i=0;i < vec_asis.size();i++){
         if (Integer.parseInt(vec_asis.get(i)+"") == 2)
            seguir = false;
         }
       } else seguir = true;
       if (seguir){
         /*esto significa que no toma en cuenta las reservas que se toman en forma directa en la asistencia*/
       a = new java.util.Vector();
       a.addElement(res.getString(1)); // 0 id_reserva
       a.addElement(res.getString(2)); // 1 hora inicio
       a.addElement(res.getString(3)); // 2 hora termino
       a.addElement(res.getString(4)); // 3 d�a
       switch (res.getInt(4)){
         case 1: dia = "Lunes";
           break;
         case 2: dia = "Martes";
           break;
         case 3: dia = "Mi�rcoles";
           break;
         case 4: dia = "Jueves";
           break;
         case 5: dia = "Viernes";
           break;
         case 6: dia = "S�bado";
       }
       a.addElement(dia);   // 4 d�a en palabras
       finicio = res.getString(5);
       ftermino = res.getString(6);
       a.addElement(finicio.substring(0,5)); // 5 fecha_inicio
       a.addElement(ftermino.substring(0,5)); // 6 fecha_termino
       a.addElement(res.getString(7)); // 7 actividad
       vec_res = getNomTipoReserva(res.getString(1));
       if (vec_res.size() > 0)
         a.addElement(vec_res.get(1)); // 8 tipo reserva
       else
         a.addElement(""); // 8 tipo reserva
       v.addElement(a);
       }
     }
     res.close();
     sent.close();
   }
   catch(SQLException e){
   System.out.println("Error en Reserva.getBloqueReserva : "+e); } return v;
 }
 //**************************************************************************
// Metodo              : Permite capturar los datos de la asistencia
// Programadora        : Monica B.
// Fecha creacion      : 18/03/2008
//****************************************************************************
public Vector getAsistenciaReserva (String id, int fecha_inicio, int fecha_termino){
 java.util.Vector v = new java.util.Vector();
 String query = " SELECT  cod_origen_asistencia" +
                " FROM asistencia_gimnasio" +
                " WHERE resgim_id = " + id +
                " AND  TO_CHAR(asigim_fecha,'yyyymmdd') >= '" + fecha_inicio +
                " ' AND TO_CHAR(asigim_fecha,'yyyymmdd') <= '" + fecha_termino+"'";

//System.out.println("getAsistenciaReserva: "+ query);
 try{
   Statement sent = con.conexion().createStatement();
   ResultSet res = sent.executeQuery(query);
   while(res.next()){
     v = new java.util.Vector();
     v.addElement(res.getString(1)); // 0 cod_origen_asistencia
   }
   res.close();
   sent.close();
 }
 catch(SQLException e){
 System.out.println("Error en Reserva.getAsistenciaReserva : "+e); 
 
 }
   return v;
  }
  //**************************************************************************
  // Metodo              : Permite la cantidad total de reservas de un per�odo
  //                       sin tomar en LOS DE LISTA DE ESPERA
  // Programadora        : Monica B.
  // Fecha creacion      : 23/07/2007
  // Fecha modificacion  :
  //****************************************************************************
  public int getCantidadReservados (int año,
		  							int semestre, 
		  							int cod_asign,
                                    int paralelo,
                                    int dia,
                                    int sala, 
                                    int blo1,
                                    int blo2,
                                    int fecha_inicio,
                                    int fecha_termino){
    int cantidad = 0;

//    String query = " SELECT count(rg.resgim_id)" +
//                   " FROM reserva_gimnasio rg LEFT JOIN asistencia_gimnasio ag ON" +
//                   " ag.resgim_id = rg.resgim_id AND" +
//                   " (datepart(weekday,ag.asigim_fecha) - 1) = rg.num_dia " +
//                // " AND ag.asigim_orden_llegada = 0" +
//                   " AND ag.cod_asistencia = 4" +
//                   " AND ag.asigim_fecha  >= rg.resgim_fecha_inicio" +
//                   " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
//                   " , registro_gimnasio reg, periodo_gimnasio pg" +
//                   " WHERE rg.pro_año = " + año +
//                   " AND rg.pro_semestre = " + semestre +
//                   " AND rg.cod_asign = " + cod_asign +
//                   " AND rg.pro_paralelo_asignatura = " + paralelo +
//                   " AND rg.num_dia = " + dia +
//                   " AND ((CONVERT(VARCHAR(8),rg.resgim_fecha_inicio,112) >= '" + fecha_inicio +
//                   "' AND CONVERT(VARCHAR(8),rg.resgim_fecha_inicio,112) <= '" + fecha_termino +
//                   "') OR (CONVERT(VARCHAR(8),rg.resgim_fecha_termino,112) >= '" + fecha_inicio+
//                   "'  AND CONVERT(VARCHAR(8),rg.resgim_fecha_termino,112) <= '" + fecha_termino+
//                   "')) AND rg.pergim_id = pg.pergim_id" +
//                   " AND rg.reggim_id = reg.reggim_id " +
//                 //  " AND reg.cod_registro_gimnasio in (3,6) " +
//                   " AND pg.num_sala = " + sala +
//                   " AND rg.cod_bloque_inicio = " + blo1 +
//                   " AND rg.cod_bloque_termino = " + blo2 + " AND" +
//                   " ag.resgim_id IS NULL" +
//                   " AND  reggim_fecha_termino  is null";
  /*  String query = "exec spu_list_cantReservadosGim " + año +"," +
                    semestre + "," +
                    cod_asign + "," +
                    paralelo + ",'" +
                    fecha_inicio + "','" +
                    fecha_termino + "'," +
                    sala + "," +
                    blo1 + "," +
                    blo2 + "," +
                    dia + "" ;*/
//System.out.println("getCantidadReservados: "+ query);
    Connection con2 = con.conexion();
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_list_cantReservadosGim (?,?,?,?,?,?,?,?,?,?,?)}");
	 	cstmt.setInt(1,año);
	 	cstmt.setInt(2,semestre);
	 	cstmt.setInt(3,cod_asign);
	 	cstmt.setInt(4,paralelo);
	 	cstmt.setString(5, Integer.toString(fecha_inicio));
	 	cstmt.setString(6, Integer.toString(fecha_termino));
	 	cstmt.setInt(7,sala);
	 	cstmt.setInt(8,blo1);
	 	cstmt.setInt(9,blo2);
	 	cstmt.setInt(10,dia);
	 	cstmt.registerOutParameter(11,Types.INTEGER);
	 	cstmt.executeQuery();
	 	cantidad = cstmt.getInt(11);
	 	cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getCantidadReservados : "+e);
    }
    return cantidad;
  }

  //**************************************************************************
  // Metodo              : Permite la fechas de reservas de un per�odo

  // Programadora        : Monica B.
  // Fecha creacion      : 24/07/2007
  // Fecha modificacion  :
  //****************************************************************************
  public Vector getFechasReservados (int año, int semestre, int cod_asign ,
                                     int paralelo, int dia,
                                     int sala, int blo1, int blo2,
                                     int fecha_inicio, int fecha_termino){
    Vector v = new Vector();
    Vector a = new Vector();

    String query = " SELECT distinct TO_CHAR(rg.resgim_fecha_inicio,'dd/mm/yyyy'), " +
                   " TO_CHAR(rg.resgim_fecha_termino,'dd/mm/yyyy')" +
                   " FROM reserva_gimnasio rg, periodo_gimnasio pg" +
                   " WHERE rg.pro_año = " + año +
                   " AND rg.pro_semestre = " + semestre +
                   " AND rg.cod_asign = " + cod_asign +
                   " AND rg.pro_paralelo_asignatura = " + paralelo +
                   " AND rg.num_dia = " + dia +
                   " AND rg.resgim_fecha_inicio <= pg.pergim_fecha_inicio_vig" +
                   " AND rg.resgim_fecha_termino <= pg.pergim_fecha_termino_vig" +
                   " AND ((TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd') >= '" + fecha_inicio +
                   "' AND TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd') <= '" + fecha_termino +
                   "') OR (TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd') >= '" + fecha_inicio+
                   "'  AND TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd') <= '" + fecha_termino+
                   "')) AND rg.pergim_id = pg.pergim_id" +
                   " AND rg.pergim_id = pg.pergim_id " +
                   " AND pg.num_sala = " + sala +
                   " AND rg.cod_bloque_inicio = " + blo1 +
                   " AND rg.cod_bloque_termino = " + blo2;

//System.out.println("getFechasReservados: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while (res.next()){
        a = new Vector();
        a.addElement(res.getString(1)); // 0 fecha_inicio
        a.addElement(res.getString(2)); // 0 fecha_termino
        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getFechasReservados : "+e);
    }
    return v;
  }
  
  int secuenciaInicio = 0;
  int secuenciaTermino = 0;
  public int getSecuenciaInicio (){
	  return secuenciaInicio;
  }
  public int getSecuenciaTermino (){
	  return secuenciaTermino;
  }
  
  public void getSecuencia (int sala, int año, int semestre, int bloque1, int bloque2){
	  gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
	  b.getAbrirConexion();
	  b.setGenerarConexionHorario();
	    
	  secuenciaInicio = 0;
	  secuenciaTermino = 0;
	  if (bloque1 == bloque2) {
		  secuenciaInicio = b.getSecuenciaBloque(sala, año, semestre, bloque1);
		  secuenciaTermino = secuenciaInicio;
	  }
	  else {
	  	Vector vec_secuencia  = b.getSecuenciaBloque(sala, año, semestre, bloque1, bloque2);
	  	if (vec_secuencia.size() > 0) {
	  		secuenciaInicio = Integer.parseInt(vec_secuencia.get(0).toString());
	  		secuenciaTermino = Integer.parseInt(vec_secuencia.get(1).toString());
	  	}
	  }
	  //int secuencia_inicio = b.getSecuenciaBloque(sala, año, semestre, bloque1);
	  //int secuencia_termino = b.getSecuenciaBloque(sala, año, semestre, bloque2);
	  b.getCerrarConexion();
  }


  //**************************************************************************
  // Metodo              : Permite capturar la asistencia y reserva de un registro
  //
  // Programadora        : Monica B.
  // Fecha creacion      : 27/07/2007
  // Fecha modificacion  : 1/04/2008 debido a que se agregaron los sig. campos:
  //                       secuencia, año y semestre en hora_bloque_sala
  //****************************************************************************
  public Vector getAsistenciaRegistro (int año, int semestre, String fecha,
                                       String periodo, int dia, int bloque1,
                                       int bloque2, String reggim_id, int sala){
    java.util.Vector v = new java.util.Vector();
    
	  
	getSecuencia (sala, año, semestre, bloque1, bloque2);
	int sec_inicio = getSecuenciaInicio();
	int sec_termino = getSecuenciaTermino();
    
    /*gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
    b.getAbrirConexion();
    b.setGenerarConexionHorario();
    int sec_inicio = b.getSecuenciaBloque( sala, año, semestre, bloque1);
    int sec_termino = b.getSecuenciaBloque( sala, año, semestre, bloque2);
    b.getCerrarConexion();*/
    String query = " SELECT rg.resgim_id, NVL(ag.cod_asistencia,0), " +
                   " NVL(ag.asigim_orden_llegada,0) as orden" +
                   " FROM registro_gimnasio regg, reserva_gimnasio rg  LEFT JOIN" +
                   " asistencia_gimnasio ag ON" +
                   " ag.resgim_id = rg.resgim_id" +
                   " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = " + fecha +
                   " , hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
                   " WHERE  rg.reggim_id = " + reggim_id +
                   " AND regg.reggim_id = rg.reggim_id" +
                   " AND regg.reggim_fecha_termino is null" +
                   " AND rg.pro_año = " + año +
                   " AND rg.pro_semestre = " + semestre +
                   " AND rg.num_dia = " + dia +
                   " AND rg.pergim_id = " + periodo +
                   " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
                   " AND hbs.horblo_año =  " + año +
                   " AND hbs.horblo_semestre = " + semestre +
                   " AND hbs.num_sala = " + sala +
                   " AND hbs.sed_cod_sede = 1" +
                   " AND hbs.cod_jornada = 1" +
                   " AND hbs.horblo_fecha_termino is null" +
                   " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
                   " AND hbs2.horblo_año =  " + año +
                   " AND hbs2.horblo_semestre = " + semestre +
                   " AND hbs2.num_sala = " + sala +
                   " AND hbs2.sed_cod_sede = 1" +
                   " AND hbs2.cod_jornada = 1" +
                   " AND hbs2.horblo_fecha_termino is null" +
                   //" AND " + fecha + " >= TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd')" +
                   //" AND " + fecha + " <= TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd')" +
                   " AND to_date('" + fecha +"','yyyymmdd') between rg.resgim_fecha_inicio and rg.resgim_fecha_termino" +
                   
                   //" AND ((" + sec_inicio +" >= hbs.horblo_secuencia" +
                   //" AND " + sec_inicio + " <=  hbs2.horblo_secuencia) or "+
                   //"  (" + sec_termino +" >= hbs.horblo_secuencia" +
                   //" AND " + sec_termino + " <= hbs2.horblo_secuencia) ) "+
                   " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                   " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" +
                   
                   " ORDER BY orden";

//System.out.println("getAsistenciaRegistro: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while(res.next()){
        v.addElement(res.getString(1)); // 0 resgim
        v.addElement(res.getString(2)); // 1 cod_asistencia
        v.addElement(res.getString(3)); // 2 orden de llegada
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getAsistenciaRegistro : "+e);
    }
    return v;
  }

  //**************************************************************************
  // Metodo              : Permite contar los alumnos inscritos en periodo
  // Programadora        : Monica B.
  // Fecha creacion      : 26/07/2007
  // Fecha modificacion  : 01/04/2008 debido aque se agregaron campos a hora_bloque_sala
  //                       año, semestre y secuencia
  //****************************************************************************
  public int getContInscritosGimnasio (int año, int semestre, int dia, String fecha,
                                       String periodo, int bloque1, int bloque2,
                                       int sala){
    int cantidad = 0;
    
    getSecuencia (sala, año, semestre, bloque1, bloque2);
	int sec_inicio = getSecuenciaInicio();
	int sec_termino = getSecuenciaTermino();
	
    /*gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
    b.getAbrirConexion();
    b.setGenerarConexionHorario();
    int sec_inicio = b.getSecuenciaBloque( sala, año, semestre, bloque1);
    int sec_termino = b.getSecuenciaBloque( sala, año, semestre, bloque2);
    b.getCerrarConexion();*/
	
    String query = " SELECT count(rg.resgim_id)" +
                   " FROM reserva_gimnasio rg, registro_gimnasio regg" +
                   " , hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
                   " WHERE rg.pro_año = " + año +
                   " AND rg.pro_semestre = " + semestre +
                   " AND rg.num_dia = " + dia +
                   " AND rg.pergim_id = " + periodo +
                   " AND rg.reggim_id = regg.reggim_id" +
                   " AND regg.reggim_fecha_termino is null" +
                   //" AND " + fecha + " >= TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd')" +
                   //" AND " + fecha + " <= TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd')" +
                   " AND to_date('" + fecha +"','yyyymmdd') between rg.resgim_fecha_inicio and rg.resgim_fecha_termino" +
                   
                   " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
                   " AND hbs.horblo_año =  " + año +
                   " AND hbs.horblo_semestre = " + semestre +
                   " AND hbs.num_sala = " + sala +
                   " AND hbs.sed_cod_sede = 1" +
                   " AND hbs.cod_jornada = 1" +
                   " AND hbs.horblo_fecha_termino is null" +
                   " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
                   " AND hbs2.horblo_año =  " + año +
                   " AND hbs2.horblo_semestre = " + semestre +
                   " AND hbs2.num_sala = " + sala +
                   " AND hbs2.sed_cod_sede = 1" +
                   " AND hbs2.cod_jornada = 1" +
                   " AND hbs2.horblo_fecha_termino is null" +
                  
                   //" AND ((" + sec_inicio +" >= hbs.horblo_secuencia" +
                   //" AND " + sec_inicio + " <=  hbs2.horblo_secuencia) or "+
                   //"  (" + sec_termino +" >= hbs.horblo_secuencia" +
                   //" AND " + sec_termino + " <= hbs2.horblo_secuencia) ) "+
                   " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                   " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)";

//System.out.println("getInscritosGimnasio: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if(res.next()){
        cantidad = res.getInt(1); // 0 cantidad
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getContInscritosGimnasio : "+e);
    }
    return cantidad;
  }
  //**************************************************************************
  // Metodo              : Permite contar los alumnos inscritos asistentes
  // Programadora        : Monica B.
  // Fecha creacion      : 14/08/2007
  // Fecha modificacion  : 01/04/2008 debido a que se agregaron campos a la tabla
  //                       hora_bloque_sala: año, semestre y secuencia
  //****************************************************************************
  public int getContInscritosAsistentesGimnasio (int año, int semestre, int dia, String fecha,
      String periodo, int bloque1, int bloque2, int sala){
    int cantidad = 0;

//    String query = " SELECT count(rg.resgim_id)" +
//                   " FROM reserva_gimnasio rg, registro_gimnasio regg left join " +
//                   " asistencia_gimnasio ag ON rg.resgim_id = ag.resgim_id"+
//                   " AND ag.cod_asistencia = 4" +
//                   " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha +
//                   "'" +
//                   " WHERE ag.resgim_id is null" +
//                   " AND rg.pro_año = " + año +
//                   " AND rg.pro_semestre = " + semestre +
//                   " AND rg.num_dia = " + dia +
//                   " AND rg.pergim_id = " + periodo +
//                   " AND rg.reggim_id = regg.reggim_id" +
//                   " AND regg.reggim_fecha_termino is null" +
//                   " AND '" +  fecha + "' >= CONVERT(VARCHAR(8),rg.resgim_fecha_inicio,112)" +
//                   " AND '" + fecha + "' <= CONVERT(VARCHAR(8),rg.resgim_fecha_termino,112) " +
//                   " AND ((" + bloque1 + " >= rg.cod_bloque_inicio"  +
//                   " AND " + bloque1 + " <= rg.cod_bloque_termino) or"+
//                   " ( " + bloque2 + " >= rg.cod_bloque_inicio"  +
//                   " AND " + bloque2 + " <= rg.cod_bloque_termino))" +
//                   " AND rg.cod_cupo != 3";
    
    getSecuencia (sala, año, semestre, bloque1, bloque2);
	int sec_inicio = getSecuenciaInicio();
	int sec_termino = getSecuenciaTermino();
	
    /*gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
    b.getAbrirConexion();
    b.setGenerarConexionHorario();
    int sec_inicio = b.getSecuenciaBloque( sala, año, semestre, bloque1);
    int sec_termino = b.getSecuenciaBloque( sala, año, semestre, bloque2);
    b.getCerrarConexion();*/
	
    /*String query = " SELECT count(rg.resgim_id)" +
                   " FROM reserva_gimnasio rg, registro_gimnasio regg left join " +
                   " asistencia_gimnasio ag ON rg.resgim_id = ag.resgim_id"+*/
    String query = " SELECT count(rg.resgim_id)" +
                   " FROM reserva_gimnasio rg, registro_gimnasio regg left join " +
                   " asistencia_gimnasio ag ON regg.reggim_id = ag.resgim_id"+               
                   " AND ag.cod_asistencia = 4" +
                   " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = '" + fecha +
                   "',  hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
                   " WHERE ag.resgim_id is null" +
                   " AND rg.pro_año = " + año +
                   " AND rg.pro_semestre = " + semestre +
                   " AND rg.num_dia = " + dia +
                   " AND rg.pergim_id = " + periodo +
                   " AND rg.reggim_id = regg.reggim_id" +
                   " AND regg.reggim_fecha_termino is null" +
                   //" AND '" +  fecha + "' >= TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd')" +
                   //" AND '" + fecha + "' <= TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd') " +
                   " AND to_date('" + fecha +"','yyyymmdd') between rg.resgim_fecha_inicio and rg.resgim_fecha_termino" +
                   
                   " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
                   " AND hbs.horblo_año =  " + año +
                   " AND hbs.horblo_semestre = " + semestre +
                   " AND hbs.num_sala = " + sala +
                   " AND hbs.sed_cod_sede = 1" +
                   " AND hbs.cod_jornada = 1" +
                   " AND hbs.horblo_fecha_termino is null" +
                   " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
                   " AND hbs2.horblo_año =  " + año +
                   " AND hbs2.horblo_semestre = " + semestre +
                   " AND hbs2.num_sala = " + sala +
                   " AND hbs2.sed_cod_sede = 1" +
                   " AND hbs2.cod_jornada = 1" +
                   " AND hbs2.horblo_fecha_termino is null" +
                   //" AND ((" + sec_inicio + " >= hbs.horblo_secuencia"  +
                   //" AND " + sec_inicio + " <= hbs2.horblo_secuencia) or"+
                   //" ( " + sec_termino + " >= hbs.horblo_secuencia"  +
                   //" AND " + sec_termino + " <= hbs.horblo_secuencia))" +
                   
                   " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                   " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" +
                   
                   " AND rg.cod_cupo != 3";

//System.out.println("getContInscritosAsistentesGimnasio: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if(res.next()){
        cantidad = res.getInt(1); // 0 cantidad
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getContInscritosAsistentesGimnasio : "+e);
    }
    return cantidad;
  }
  //**************************************************************************
 // Metodo              : Permite contar los alumnos asistentes para estad�sticas
  //                      la fecha la recibe aaaammdd
 // Programadora        : Monica B.
 // Fecha creacion      : 2/11/2007
  // Fecha modificacion  : 03/04/2008 debido a que se agregaron campos a la tabla
  //                       hora_bloque_sala: año, semestre y secuencia
 //****************************************************************************
  public int getContInscritosEstadistica (int año,
                                          int semestre,
                                          int cod_asign,
                                          int paralelo,
                                          int jornada,
                                          int sede,
                                          int num_dia,
                                          String fecha,
                                          int bloque1,
                                          int bloque2,
                                          int sala){
   int cantidad = 0;
   String query = " SELECT NVL(count(rg.resgim_id),-1)" +
                  " FROM reserva_gimnasio rg, asistencia_gimnasio ag, periodo_gimnasio pg" +
                  " WHERE rg.pro_año = " + año +
                  " AND rg.pro_semestre = " + semestre +
                  " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = '" + fecha + "'" +
                  " AND rg.resgim_id = ag.resgim_id"+
                  " AND ag.cod_asistencia = 1" +
                  " AND rg.num_dia = " + num_dia +
                  " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
                  " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
                  " AND ((" + bloque1 + " >= rg.cod_bloque_inicio"  +
                  " AND " + bloque1 + " <= rg.cod_bloque_termino) or"+
                  " ( " + bloque2 + " >= rg.cod_bloque_inicio"  +
                  " AND " + bloque2 + " <= rg.cod_bloque_termino))" +
                  " AND rg.cod_asign = " + cod_asign +
                  " AND rg.pro_paralelo_asignatura = " + paralelo +
                  " AND rg.cod_jornada = "+ jornada +
                  " AND rg.sed_cod_sede = "+ sede +
                  " AND rg.pergim_id = pg.pergim_id" +
                  " AND pg.num_sala = " + sala;

//System.out.println("getContInscritosEstadistica: "+ query);
   try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     if(res.next()){
       cantidad = res.getInt(1); // 0 cantidad
     }
     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Reserva.getContInscritosEstadistica : "+e);
   }
   return cantidad;
 }
 //**************************************************************************
// Metodo              : Permite contar los alumnos asistentes para estad�sticas
//                      la fecha la recibe aaaammdd
// Programadora        : Monica B.
// Fecha creacion      : 2/11/2007
 // Fecha modificacion  : 03/04/2008 debido a que se agregaron campos a la tabla
 //                       hora_bloque_sala: año, semestre y secuencia
//****************************************************************************
public Vector getContInscritosDetalleEstadistica (int año,
                                       int semestre,
                                       int cod_asign,
                                       int paralelo,
                                       int jornada,
                                       int sede,
                                       int num_dia,
                                       String fecha,
                                       int bloque1,
                                       int bloque2,
                                       int cod_registro,
                                       int sala){
 int mujer = 0;
 int hombre = 0;
 Vector v = new Vector();
 Vector datos = new Vector();
 String query = "";

// if (cod_registro == 1 || cod_registro == 2 || cod_registro == 3)
//   query =    " SELECT a.alu_sexo as sexo" +
//              " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
//              " registro_gimnasio rgg, registro_gimnasio_alumno rga, alumno a" +
//              " WHERE rg.pro_año = " + año +
//              " AND rg.pro_semestre = " + semestre +
//              " AND rg.num_dia = " + num_dia +
//              " AND ((" + bloque1 + " >= rg.cod_bloque_inicio"  +
//              " AND " + bloque1 + " <= rg.cod_bloque_termino) or"+
//              " ( " + bloque2 + " >= rg.cod_bloque_inicio"  +
//              " AND " + bloque2 + " <= rg.cod_bloque_termino))" +
//              " AND rg.cod_asign = " + cod_asign +
//              " AND rg.pro_paralelo_asignatura = " + paralelo +
//              " AND rg.cod_jornada = "+ jornada +
//              " AND rg.sed_cod_sede = "+ sede +
//              " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha + "'" +
//              " AND rg.resgim_id = ag.resgim_id"+
//              " AND ag.cod_asistencia = 1" +
//              " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
//              " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
//              " AND rg.reggim_id = rgg.reggim_id" +
//              " AND rgg.reggim_id = rga.reggim_id" +
//              " AND rga.alu_rut = a.alu_rut" ;
// if (cod_registro == 4)
//      query=    " SELECT f.fun_sexo as sexo" +
//                " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
//                " registro_gimnasio rgg, registro_gimnasio_funcionario rgf, funcionario f" +
//                " WHERE rg.pro_año = " + año +
//                " AND rg.pro_semestre = " + semestre +
//                " AND rg.num_dia = " + num_dia +
//                " AND ((" + bloque1 + " >= rg.cod_bloque_inicio"  +
//                " AND " + bloque1 + " <= rg.cod_bloque_termino) or"+
//                " ( " + bloque2 + " >= rg.cod_bloque_inicio"  +
//                " AND " + bloque2 + " <= rg.cod_bloque_termino))" +
//                " AND rg.cod_asign = " + cod_asign +
//                " AND rg.pro_paralelo_asignatura = " + paralelo +
//                " AND rg.cod_jornada = "+ jornada +
//                " AND rg.sed_cod_sede = "+ sede +
//                " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha + "'" +
//                " AND rg.resgim_id = ag.resgim_id"+
//                " AND ag.cod_asistencia = 1" +
//                " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
//                " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
//
//                " AND rg.reggim_id = rgg.reggim_id" +
//                " AND rgg.reggim_id = rgf.reggim_id" +
//                " AND rgf.fun_rut = f.fun_rut" ;
// if (cod_registro == 5 || cod_registro == 6 || cod_registro == 7)
//      query =   " SELECT rge.regext_rut" +
//                " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
//                " registro_gimnasio rgg, registro_gimnasio_externo rge" +
//                " WHERE rg.pro_año = " + año +
//                " AND rg.pro_semestre = " + semestre +
//                " AND rg.num_dia = " + num_dia +
//                " AND ((" + bloque1 + " >= rg.cod_bloque_inicio"  +
//                " AND " + bloque1 + " <= rg.cod_bloque_termino) or"+
//                " ( " + bloque2 + " >= rg.cod_bloque_inicio"  +
//                " AND " + bloque2 + " <= rg.cod_bloque_termino))" +
//                " AND rg.cod_asign = " + cod_asign +
//                " AND rg.pro_paralelo_asignatura = " + paralelo +
//                " AND rg.cod_jornada = "+ jornada +
//                " AND rg.sed_cod_sede = "+ sede +
//                " AND rg.resgim_id = ag.resgim_id"+
//                " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha + "'" +
//                " AND ag.cod_asistencia = 1" +
//                " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
//                " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
//                " AND rg.reggim_id = rgg.reggim_id" +
//                " AND rgg.reggim_id = rge.reggim_id" ;

  getSecuencia (sala, año, semestre, bloque1, bloque2);
  int sec_inicio = getSecuenciaInicio();
  int sec_termino = getSecuenciaTermino();
 
 /*gimnasio.BeanGeneral b2 = new gimnasio.BeanGeneral();
b2.getAbrirConexion();
b2.setGenerarConexionHorario();
int sec_inicio = b2.getSecuenciaBloque( sala, año, semestre, bloque1);
int sec_termino = b2.getSecuenciaBloque( sala, año, semestre, bloque2);
b2.getCerrarConexion();*/

 //gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
 //b.getAbrirConexionVi�a();
 //b.setGenerarConexionDatosVi�a();
 if (cod_registro == 1 || cod_registro == 2 || cod_registro == 3)
   query =
   " SELECT a.alu_sexo as sexo" +
   " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
   " registro_gimnasio rgg, registro_gimnasio_alumno rga, alumno a" +
   " ,  hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
   " WHERE rg.pro_año = " + año +
   " AND rg.pro_semestre = " + semestre +
   " AND rg.num_dia = " + num_dia +
   " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
   " AND hbs.horblo_año =  " + año +
   " AND hbs.horblo_semestre = " + semestre +
   " AND hbs.num_sala = " + sala +
   " AND hbs.sed_cod_sede = 1" +
   " AND hbs.cod_jornada = 1" +
   " AND hbs.horblo_fecha_termino is null" +
   " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
   " AND hbs2.horblo_año =  " + año +
   " AND hbs2.horblo_semestre = " + semestre +
   " AND hbs2.num_sala = " + sala +
   " AND hbs2.sed_cod_sede = 1" +
                   " AND hbs2.cod_jornada = 1" +
                   " AND hbs2.horblo_fecha_termino is null" +
                   //" AND ((" + sec_inicio + " >= hbs.horblo_secuencia"  +
                   //" AND " + sec_inicio + " <= hbs2.horblo_secuencia) or"+
                   //" ( " + sec_termino + " >= hbs.horblo_secuencia"  +
                   //" AND " + sec_termino + " <= hbs2.horblo_secuencia))" +                   

                   " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                   " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" +
                   
                   " AND rg.cod_asign = " + cod_asign +
                   " AND rg.pro_paralelo_asignatura = " + paralelo +
                   " AND rg.cod_jornada = "+ jornada +
                   " AND rg.sed_cod_sede = "+ sede +
                   " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = '" + fecha + "'" +
                   " AND rg.resgim_id = ag.resgim_id"+
                   " AND ag.cod_asistencia = 1" +
                   " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
                   " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
                   " AND rg.reggim_id = rgg.reggim_id" +
                   " AND rgg.reggim_id = rga.reggim_id" +
                   " AND rga.alu_rut = a.alu_rut" ;
 if (cod_registro == 4)
   query=    " SELECT f.fun_sexo as sexo" +
             " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
             " registro_gimnasio rgg, registro_gimnasio_funcionario rgf, funcionario f" +
             " ,  hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
             " WHERE rg.pro_año = " + año +
             " AND rg.pro_semestre = " + semestre +
             " AND rg.num_dia = " + num_dia +
             " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
             " AND hbs.horblo_año =  " + año +
             " AND hbs.horblo_semestre = " + semestre +
             " AND hbs.num_sala = " + sala +
             " AND hbs.sed_cod_sede = 1" +
             " AND hbs.cod_jornada = 1" +
             " AND hbs.horblo_fecha_termino is null" +
             " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
             " AND hbs2.horblo_año =  " + año +
             " AND hbs2.horblo_semestre = " + semestre +
             " AND hbs2.num_sala = " + sala +
             " AND hbs2.sed_cod_sede = 1" +
             " AND hbs2.cod_jornada = 1" +
             " AND hbs2.horblo_fecha_termino is null" +
             //" AND ((" + sec_inicio + " >= hbs.horblo_secuencia"  +
             //" AND " + sec_inicio + " <= hbs2.horblo_secuencia) or"+
             //" ( " + sec_termino + " >= hbs.horblo_secuencia"  +
             //" AND " + sec_termino + " <= hbs2.horblo_secuencia))" +

             " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
             " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" +
             
             " AND rg.cod_asign = " + cod_asign +
             " AND rg.pro_paralelo_asignatura = " + paralelo +
             " AND rg.cod_jornada = "+ jornada +
             " AND rg.sed_cod_sede = "+ sede +
             " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = '" + fecha + "'" +
             " AND rg.resgim_id = ag.resgim_id"+
             " AND ag.cod_asistencia = 1" +
             " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
             " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
             " AND rg.reggim_id = rgg.reggim_id" +
             " AND rgg.reggim_id = rgf.reggim_id" +
             " AND rgf.fun_rut = f.fun_rut" ;
 if (cod_registro == 5 || cod_registro == 6 || cod_registro == 7)
   query =     " SELECT rge.regext_rut" +
               " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
               " registro_gimnasio rgg, registro_gimnasio_externo rge" +
               " ,  hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
               " WHERE rg.pro_año = " + año +
               " AND rg.pro_semestre = " + semestre +
               " AND rg.num_dia = " + num_dia +
               " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
               " AND hbs.horblo_año =  " + año +
               " AND hbs.horblo_semestre = " + semestre +
               " AND hbs.num_sala = " + sala +
               " AND hbs.sed_cod_sede = 1" +
               " AND hbs.cod_jornada = 1" +
               " AND hbs.horblo_fecha_termino is null" +
               " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
               " AND hbs2.horblo_año =  " + año +
               " AND hbs2.horblo_semestre = " + semestre +
               " AND hbs2.num_sala = " + sala +
               " AND hbs2.sed_cod_sede = 1" +
               " AND hbs2.cod_jornada = 1" +
               " AND hbs2.horblo_fecha_termino is null" +
               //" AND ((" + sec_inicio + " >= hbs.horblo_secuencia"  +
               //" AND " + sec_inicio + " <= hbs2.horblo_secuencia) or"+
               //" ( " + sec_termino + " >= hbs.horblo_secuencia"  +
               //" AND " + sec_termino + " <= hbs2.horblo_secuencia))" +

               " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
               " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" +
               
               " AND rg.cod_asign = " + cod_asign +
               " AND rg.pro_paralelo_asignatura = " + paralelo +
               " AND rg.cod_jornada = "+ jornada +
               " AND rg.sed_cod_sede = "+ sede +
               " AND rg.resgim_id = ag.resgim_id"+
               " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = '" + fecha + "'" +
               " AND ag.cod_asistencia = 1" +
               " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
               " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
               " AND rg.reggim_id = rgg.reggim_id" +
               " AND rgg.reggim_id = rge.reggim_id" ;
//System.out.println("getContInscritosDetalleEstadistica: "+ query);
 try{
   Statement sent = con.conexion().createStatement();
   ResultSet res = sent.executeQuery(query);
   if(res.next()){
     //if (cod_registro <= 4)
       if (res.getInt(1) == 1) hombre += 1;
       else mujer += 1;
     /*else {
       if (cod_registro == 5 || cod_registro == 6) { // si es alumno vi�a
         datos = b.getAlumnoVi�a(res.getInt(1));
         if (datos.size() > 0){
           if (Integer.parseInt(datos.get(19)+"") == 1 )
             hombre += 1;
           else
             mujer += 1;
         }
       } else { // es funcionario
         datos = b.getFuncionarioVi�a(res.getInt(1));
         if (datos.size()>0){
         if (Integer.parseInt(datos.get(17)+"") == 1 )
           hombre += 1;
         else
           mujer += 1;
         }
       }
     }*/
     v.addElement(String.valueOf(hombre));
     v.addElement(String.valueOf(mujer));
   }
   res.close();
   sent.close();
 }
 catch(SQLException e){
   System.out.println("Error en Reserva.getContInscritosDetalleEstadistica : "+e);
 }
 //b.getCerrarConexionVi�a();
 return v;
}
//**************************************************************************
// Metodo              : Permite contar los alumnos asistentes para estad�sticas
//                      la fecha la recibe aaaammdd por sexo
// Programadora        : Monica B.
// Fecha creacion      : 6/03/2008
// Fecha modificacion  : 03/04/2008 debido a que se agregaron campos a la tabla
 //                       hora_bloque_sala: año, semestre y secuencia

//****************************************************************************
public int getContInscritosDetalleSexo (int año,
                                      int semestre,
                                      int cod_asign,
                                      int paralelo,
                                      int jornada,
                                      int sede,
                                      int num_dia,
                                      String fecha,
                                      int bloque1,
                                      int bloque2,
                                      int cod_registro,
                                      int sexo,
                                      int sala){
int num_sexo = 0;
Vector v = new Vector();
Vector datos = new Vector();
String query = "";
//if (cod_registro == 1 || cod_registro == 2 || cod_registro == 3)
//    query =    " SELECT count(a.alu_sexo)" +
//               " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
//               " registro_gimnasio rgg, registro_gimnasio_alumno rga, alumno a" +
//               " WHERE rg.pro_año = " + año +
//               " AND rg.pro_semestre = " + semestre +
//               " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha + "'" +
//               " AND rg.resgim_id = ag.resgim_id"+
//               " AND ag.cod_asistencia = 1" +
//               " AND rg.num_dia = " + num_dia +
//               " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
//               " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
//               " AND ((" + bloque1 + " >= rg.cod_bloque_inicio"  +
//               " AND " + bloque1 + " <= rg.cod_bloque_termino) or"+
//               " ( " + bloque2 + " >= rg.cod_bloque_inicio"  +
//               " AND " + bloque2 + " <= rg.cod_bloque_termino))" +
//               " AND rg.cod_asign = " + cod_asign +
//               " AND rg.pro_paralelo_asignatura = " + paralelo +
//               " AND rg.cod_jornada = "+ jornada +
//               " AND rg.sed_cod_sede = "+ sede +
//               " AND rg.reggim_id = rgg.reggim_id" +
//               " AND rgg.reggim_id = rga.reggim_id" +
//               " AND rga.alu_rut = a.alu_rut" +
//               " AND a.alu_rut = " + sexo   ;
//if (cod_registro == 4)
//     query=    " SELECT count(f.fun_sexo) " +
//               " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
//               " registro_gimnasio rgg, registro_gimnasio_funcionario rgf, funcionario f" +
//               " WHERE rg.pro_año = " + año +
//               " AND rg.pro_semestre = " + semestre +
//               " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha + "'" +
//               " AND rg.resgim_id = ag.resgim_id"+
//               " AND ag.cod_asistencia = 1" +
//               " AND rg.num_dia = " + num_dia +
//               " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
//               " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
//               " AND ((" + bloque1 + " >= rg.cod_bloque_inicio"  +
//               " AND " + bloque1 + " <= rg.cod_bloque_termino) or"+
//               " ( " + bloque2 + " >= rg.cod_bloque_inicio"  +
//               " AND " + bloque2 + " <= rg.cod_bloque_termino))" +
//               " AND rg.cod_asign = " + cod_asign +
//               " AND rg.pro_paralelo_asignatura = " + paralelo +
//               " AND rg.cod_jornada = "+ jornada +
//               " AND rg.sed_cod_sede = "+ sede +
//               " AND rg.reggim_id = rgg.reggim_id" +
//               " AND rgg.reggim_id = rgf.reggim_id" +
//               " AND rgf.fun_rut = f.fun_rut" +
//               " AND f.fun_sexo = " + sexo ;
//if (cod_registro == 5 || cod_registro == 6 || cod_registro == 7)
//     query =   " SELECT rge.regext_rut" +
//               " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
//               " registro_gimnasio rgg, registro_gimnasio_externo rge" +
//               " WHERE rg.pro_año = " + año +
//               " AND rg.pro_semestre = " + semestre +
//               " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha + "'" +
//               " AND rg.resgim_id = ag.resgim_id"+
//               " AND ag.cod_asistencia = 1" +
//               " AND rg.num_dia = " + num_dia +
//               " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
//               " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
//               " AND ((" + bloque1 + " >= rg.cod_bloque_inicio"  +
//               " AND " + bloque1 + " <= rg.cod_bloque_termino) or"+
//               " ( " + bloque2 + " >= rg.cod_bloque_inicio"  +
//               " AND " + bloque2 + " <= rg.cod_bloque_termino))" +
//               " AND rg.cod_asign = " + cod_asign +
//               " AND rg.pro_paralelo_asignatura = " + paralelo +
//               " AND rg.cod_jornada = "+ jornada +
//               " AND rg.sed_cod_sede = "+ sede +
//               " AND rg.reggim_id = rgg.reggim_id" +
//               " AND rgg.reggim_id = rge.reggim_id" ;

getSecuencia (sala, año, semestre, bloque1, bloque2);
int sec_inicio = getSecuenciaInicio();
int sec_termino = getSecuenciaTermino();

/*gimnasio.BeanGeneral b2 = new gimnasio.BeanGeneral();
b2.getAbrirConexion();
b2.setGenerarConexionHorario();
int sec_inicio = b2.getSecuenciaBloque( sala, año, semestre, bloque1);
int sec_termino = b2.getSecuenciaBloque( sala, año, semestre, bloque2);
b2.getCerrarConexion();*/

/*gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
b.getAbrirConexionVi�a();
b.setGenerarConexionDatosVi�a();*/

if (cod_registro == 1 || cod_registro == 2 || cod_registro == 3)
       query =    " SELECT count(a.alu_sexo)" +
                  " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
                  " registro_gimnasio rgg, registro_gimnasio_alumno rga, alumno a" +
                  " ,  hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
                  " WHERE rg.pro_año = " + año +
                  " AND rg.pro_semestre = " + semestre +
                  " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = '" + fecha + "'" +
                  " AND rg.resgim_id = ag.resgim_id"+
                  " AND ag.cod_asistencia = 1" +
                  " AND rg.num_dia = " + num_dia +
                  " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
                  " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
                  " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
                  " AND hbs.horblo_año =  " + año +
                  " AND hbs.horblo_semestre = " + semestre +
                  " AND hbs.num_sala = " + sala +
                  " AND hbs.sed_cod_sede = 1" +
                  " AND hbs.cod_jornada = 1" +
                  " AND hbs.horblo_fecha_termino is null" +
                  " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
                  " AND hbs2.horblo_año =  " + año +
                  " AND hbs2.horblo_semestre = " + semestre +
                  " AND hbs2.num_sala = " + sala +
                  " AND hbs2.sed_cod_sede = 1" +
                  " AND hbs2.cod_jornada = 1" +
                  " AND hbs2.horblo_fecha_termino is null" +
                  //" AND ((" + sec_inicio + " >= hbs.horblo_secuencia"  +
                  //" AND " + sec_inicio + " <= hbs2.horblo_secuencia) or"+
                  //" ( " + sec_termino + " >= hbs.horblo_secuencia"  +
                  //" AND " + sec_termino + " <= hbs2.horblo_secuencia))" +

                  " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                  " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" +
                  
                  " AND rg.cod_asign = " + cod_asign +
                  " AND rg.pro_paralelo_asignatura = " + paralelo +
                  " AND rg.cod_jornada = "+ jornada +
                  " AND rg.sed_cod_sede = "+ sede +
                  " AND rg.reggim_id = rgg.reggim_id" +
                  " AND rgg.reggim_id = rga.reggim_id" +
                  " AND rga.alu_rut = a.alu_rut" +
                  " AND a.alu_rut = " + sexo   ;
if (cod_registro == 4)
     query=    " SELECT count(f.fun_sexo) " +
               " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
               " registro_gimnasio rgg, registro_gimnasio_funcionario rgf, funcionario f" +
               " ,  hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
               " WHERE rg.pro_año = " + año +
               " AND rg.pro_semestre = " + semestre +
               " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = '" + fecha + "'" +
               " AND rg.resgim_id = ag.resgim_id"+
               " AND ag.cod_asistencia = 1" +
               " AND rg.num_dia = " + num_dia +
               " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
               " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
               " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
               " AND hbs.horblo_año =  " + año +
               " AND hbs.horblo_semestre = " + semestre +
               " AND hbs.num_sala = " + sala +
               " AND hbs.sed_cod_sede = 1" +
               " AND hbs.cod_jornada = 1" +
               " AND hbs.horblo_fecha_termino is null" +
               " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
               " AND hbs2.horblo_año =  " + año +
               " AND hbs2.horblo_semestre = " + semestre +
               " AND hbs2.num_sala = " + sala +
               " AND hbs2.sed_cod_sede = 1" +
               " AND hbs2.cod_jornada = 1" +
               " AND hbs2.horblo_fecha_termino is null" +
               " AND ((" + sec_inicio + " >= hbs.horblo_secuencia"  +
               " AND " + sec_inicio + " <= hbs2.horblo_secuencia) or"+
               " ( " + sec_termino + " >= hbs.horblo_secuencia"  +
               " AND " + sec_termino + " <= hbs2.horblo_secuencia))" +
               " AND rg.cod_asign = " + cod_asign +
               " AND rg.pro_paralelo_asignatura = " + paralelo +
               " AND rg.cod_jornada = "+ jornada +
               " AND rg.sed_cod_sede = "+ sede +
               " AND rg.reggim_id = rgg.reggim_id" +
               " AND rgg.reggim_id = rgf.reggim_id" +
               " AND rgf.fun_rut = f.fun_rut" +
               " AND f.fun_sexo = " + sexo ;
if (cod_registro == 5 || cod_registro == 6 || cod_registro == 7)
     query =   " SELECT rge.regext_rut" +
               " FROM reserva_gimnasio rg, asistencia_gimnasio ag," +
               " registro_gimnasio rgg, registro_gimnasio_externo rge" +
               " ,  hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
               " WHERE rg.pro_año = " + año +
               " AND rg.pro_semestre = " + semestre +
               " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = '" + fecha + "'" +
               " AND rg.resgim_id = ag.resgim_id"+
               " AND ag.cod_asistencia = 1" +
               " AND rg.num_dia = " + num_dia +
               " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
               " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
               " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
               " AND hbs.horblo_año =  " + año +
               " AND hbs.horblo_semestre = " + semestre +
               " AND hbs.num_sala = " + sala +
               " AND hbs.sed_cod_sede = 1" +
               " AND hbs.cod_jornada = 1" +
               " AND hbs.horblo_fecha_termino is null" +
               " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
               " AND hbs2.horblo_año =  " + año +
               " AND hbs2.horblo_semestre = " + semestre +
               " AND hbs2.num_sala = " + sala +
               " AND hbs2.sed_cod_sede = 1" +
               " AND hbs2.cod_jornada = 1" +
               " AND hbs2.horblo_fecha_termino is null" +
               " AND ((" + sec_inicio + " >= hbs.horblo_secuencia"  +
               " AND " + sec_inicio + " <= hbs2.horblo_secuencia) or"+
               " ( " + sec_termino + " >= hbs.horblo_secuencia"  +
               " AND " + sec_termino + " <= hbs2.horblo_secuencia))" +
               " AND rg.cod_asign = " + cod_asign +
               " AND rg.pro_paralelo_asignatura = " + paralelo +
               " AND rg.cod_jornada = "+ jornada +
               " AND rg.sed_cod_sede = "+ sede +
               " AND rg.reggim_id = rgg.reggim_id" +
               " AND rgg.reggim_id = rge.reggim_id" ;

//System.out.println("getContInscritosDetalleEstadistica: "+ query);
try{
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  if(res.next()){
   // if (cod_registro <= 4)
   num_sexo = res.getInt(1);
    /*else {
      if (cod_registro == 5 || cod_registro == 6) { // si es alumno vi�a
        datos = b.getAlumnoVi�a(res.getInt(1));
        if (datos.size() > 0){
          if (Integer.parseInt(datos.get(19)+"") == sexo )
            num_sexo += num_sexo;
        }
      } else { // es funcionario
        datos = b.getFuncionarioVi�a(res.getInt(1));
        if (datos.size()>0){
        if (Integer.parseInt(datos.get(17)+"") == sexo )
         num_sexo += num_sexo;
        }
      }
    }*/

  }
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Reserva.getContInscritosDetalleEstadistica : "+e);
}
//b.getCerrarConexionVi�a();
return num_sexo;
}

 //**************************************************************************
// Metodo              : Permite obtener los c�digos para contar para estad�sticas
 //                      la fecha la recibe aaaammdd
// Programadora        : Monica B.
// Fecha creacion      : 2/11/2007
// Fecha modificacion  :
//****************************************************************************
public Vector getCodigosInscrEstadistica (int año,
                                       int semestre,
                                       String fecha,
                                       int sala){
 Vector v = new Vector();
 Vector a = new Vector();
 int cont = 0;
 String dia = "";

//  String query = " SELECT distinct  rg.pro_año, rg.pro_semestre, rg.cod_bloque_inicio," +
//                 " rg.cod_bloque_termino, rg.num_dia as num_dia, " +
//                 " rg.cod_asign, rg.pro_paralelo_asignatura, rg.cod_jornada, rg.sed_cod_sede" +
//                 " FROM reserva_gimnasio rg, asistencia_gimnasio ag" +
//                 " WHERE rg.pro_año = " + año +
//                 " AND rg.pro_semestre = " + semestre +
//                 " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha + "'" +
//                 " AND rg.resgim_id = ag.resgim_id" +
//                 " AND ag.cod_asistencia = 1" +
//                 " AND ag.asigim_fecha >= rg.resgim_fecha_inicio" +
//                 " AND ag.asigim_fecha <= rg.resgim_fecha_termino" +
//                 " UNION" +
//                 " SELECT distinct  rg.pro_año, rg.pro_semestre, rg.cod_bloque_inicio," +
//                 " rg.cod_bloque_termino, rg.num_dia as num_dia,  rg.cod_asign," +
//                 " rg.pro_paralelo_asignatura, rg.cod_jornada, rg.sed_cod_sede" +
//                 " FROM reserva_gimnasio rg, estado_clase_gimnasio ecg" +
//                 " WHERE rg.pro_año =" + año +
//                 " AND rg.pro_semestre =" + semestre +
//                 " AND CONVERT(VARCHAR(8),ecg.estcla_fecha,112) = '" + fecha + "'"+
//                 " AND rg.pro_año = ecg.pro_año" +
//                 " AND rg.pro_semestre = ecg.pro_semestre" +
//                 " AND rg.cod_asign = ecg.cod_asign" +
//                 " AND rg.pro_paralelo_asignatura = ecg.pro_paralelo_asignatura" +
//                 " AND rg.cod_jornada = ecg.cod_jornada" +
//                 " AND rg.sed_cod_sede = ecg.sed_cod_sede" +
//                 " AND rg.cod_bloque_inicio = ecg.cod_bloque_inicio" +
//                 " AND rg.cod_bloque_termino = ecg.cod_bloque_termino" +
//                 " AND rg.num_dia = (DATEPART(weekday,ecg.estcla_fecha) - 1)" +
//                 " AND ecg.estcla_fecha >= rg.resgim_fecha_inicio" +
//                 " AND ecg.estcla_fecha <= rg.resgim_fecha_termino" +
//                 " AND ecg.cod_estado_clase > 1 " +
//                 " ORDER BY num_dia" ;3
 // System.out.println("getCodigosInscrEstadistica: "+ query);
// String query = "exec spu_list_codigosEstadisticaGim " + año +", " + semestre + ", '" +
  //                                       fecha + "',"+ sala;
//System.out.println("getCodigosInscrEstadistica: "+ query);
  try{
    Statement sent = con.conexion().createStatement();
 //   ResultSet res = sent.executeQuery(query);
    while (v != null){

    }

  }
  catch(SQLException e){
    System.out.println("Error en Reserva.getCodigosInscrEstadistica : "+e);
  }
  return v;
}
//**************************************************************************
// Metodo              : Permite el porcentaje de uso por fechas para estad�sticas
//                      la fecha la recibe aaaammdd
// Programadora        : Monica B.
// Fecha creacion      : 5/11/2007
// Fecha modificacion  :
//****************************************************************************
//public Vector getPorcentajeUso (int año, int semestre, String fecha_inicio,
//                                   String fecha_termino, int sala){
//Vector v = new Vector();
//Vector a = new Vector();
//Vector d = new Vector();
//Vector f = new Vector();
//
//Vector vec_inscritos = new Vector();
//Vector vec_estado = new Vector();
//int cont = 0;
//String dia = "";
//String total = "";
//String fecha = "";
//int cod_asign = 0;
//int paralelo = 0;
//int jornada = 0;
//int sede = 0;
//
//
//
//String query = "exec spu_list_FechaAsignEstadisGim " + año +"," +
//               semestre + ",'" +
//               fecha_inicio + "','" +
//               fecha_termino + "'," +
//               sala;
// System.out.println("getPorcentajeUso: "+ query);
// try{
//   Statement sent = con.conexion().createStatement();
//   ResultSet res = sent.executeQuery(query);
//   while (res.next()){
//     int num_dia = 0;
//     int blo1 = 0;
//     int blo2 = 0;
//     cont = 0;
//     f = new Vector();
//     switch (res.getInt(2)){
//       case 1: dia = "Lunes";
//         break;
//       case 2: dia = "Martes";
//         break;
//       case 3: dia = "Mi�rcoles";
//         break;
//       case 4: dia = "Jueves";
//         break;
//       case 5: dia = "Viernes";
//         break;
//       case 6: dia = "S�bado";
//     }
//     vec_inscritos = new Vector();
//     vec_inscritos = getCodigosInscrEstadistica (año, semestre, res.getString(1), sala);
//     if (vec_inscritos.size() > 0){
//     for (int i=0;i<vec_inscritos.size();i++){
//       fecha = res.getString(1);
//       cod_asign = Integer.parseInt(((Vector)vec_inscritos.get(i)).get(5)+"");
//       paralelo = Integer.parseInt(((Vector)vec_inscritos.get(i)).get(6)+"");
//       jornada = Integer.parseInt(((Vector)vec_inscritos.get(i)).get(7)+"");
//       sede = Integer.parseInt(((Vector)vec_inscritos.get(i)).get(8)+"");
//       num_dia = Integer.parseInt(((Vector)vec_inscritos.get(i)).get(4)+"");
//       blo1 = Integer.parseInt(((Vector)vec_inscritos.get(i)).get(2)+"");
//       blo2 = Integer.parseInt(((Vector)vec_inscritos.get(i)).get(3)+"");
//
//      vec_estado = getEstadoClaseGimnasio (fecha, cod_asign, año, semestre,
//        paralelo, jornada, sede, num_dia, blo1, blo2);
//        if( vec_estado.size() > 0 && Integer.parseInt(vec_estado.get(0)+"") > 1){
//          switch (Integer.parseInt(vec_estado.get(0)+"")){
//            case 2: case 4: total = "Anul.";
//              break;
//            case 3: case 5: total = "Susp.";
//              break;
//          }
//        } else {
//               cont = getContInscritosEstadistica ( año, semestre, cod_asign, paralelo,
//                jornada, sede, num_dia, fecha, blo1, blo2, sala);
//               total = String.valueOf(cont);
//
//
//        }
//          d = new Vector();
//          d.addElement(total); // inscritos
//          d.addElement(String.valueOf(blo1)); // bloque_inicio
//          d.addElement(String.valueOf(blo2)); // bloque termino
//          f.addElement(d);
//          total = "";
//          blo1 = 0;
//          blo2 = 0;
//     }
//     } else {
//       d = new Vector();
//       d.addElement("-1"); // no hay ning�n registro en bloques
//       f.addElement(d);
//     }
//
//     a = new Vector();
//     a.addElement(dia);              // 0 d�a
//     a.addElement(res.getString(3)); // 1 fecha
//     a.addElement(f);                // 2  total asistentes o anulacion
//     a.addElement(res.getString(1)); // 3 fecha aaaammdd
//     a.addElement(res.getString(2));    // 4 num_dia
//     v.addElement(a);
//
//   }
//   res.close();
//   sent.close();
// }
// catch(SQLException e){
//   System.out.println("Error en Reserva.getPorcentajeUso : "+e);
// }
// return v;
//}
 //**************************************************************************
// Metodo              : Permite el porcentaje de uso por fechas para estad�sticas
//                      la fecha la recibe aaaammdd
// Programadora        : Monica B.
// Fecha creacion      : 28/04/2008
// Fecha modificacion  :
//****************************************************************************
public Vector getPorcentajeUso (int año,int semestre,String fecha_inicio,
                                String fecha_termino,int sala,int detalle,
                                int registro_usuario){
Vector v = new Vector();
Vector a = new Vector();
int error = -1;
 // "exec spu_list_PorcUsoDetalleGim "
/*String query =  "exec spu_list_PorcentajeGim "+ año +"," +
               semestre + ",'" +
               fecha_inicio + "','" +
               fecha_termino + "'," +
               sala +"," +
               detalle +"," + registro_usuario;*/
// System.out.println("Reserva.getPorcentajeUso: "+ query);
Connection con2 = con.conexion();
 try{
   //Statement sent = con.conexion().createStatement();
   //ResultSet res = sent.executeQuery(query);
	 
	 CallableStatement cstmt = con2.prepareCall("{call spu_list_PorcentajeGim (?,?,?,?,?,?,?,?,?)}");
	 cstmt.setInt(1,año);
	 cstmt.setInt(2,semestre);
	 cstmt.setString(3,fecha_inicio);
	 cstmt.setString(4,fecha_termino);
	 cstmt.setInt(5,sala);
	 cstmt.setInt(6,detalle);
	 cstmt.setInt(7,registro_usuario);
	 cstmt.registerOutParameter(8, Types.INTEGER);
	 cstmt.registerOutParameter(9, OracleTypes.CURSOR);
	 cstmt.executeQuery();
	 error = cstmt.getInt(8);
	 
	 ResultSet res = (ResultSet) cstmt.getObject(9);
  
    while (res.next()){
     a = new Vector();
     a.addElement(res.getString(1)); // 0 nom_d�a
     a.addElement(res.getString(2)); // 1 fecha
     a.addElement(res.getString(3)); // 2 num_dia
     a.addElement(res.getString(4)); // 3 sec_ini
     a.addElement(res.getString(5)); // 4 sec_fin
     a.addElement(res.getString(6)); // 5 sum_masc
     a.addElement(res.getString(7)); // 6 sum_fem
     a.addElement(res.getString(8)); // 7 fecha_emision
     v.addElement(a);
    }
   res.close();
   cstmt.close();
 }
 catch(SQLException e){
   System.out.println("Error en Reserva.getPorcentajeUso : "+e);
 }
 //System.out.println(v);
 return v;
}
//**************************************************************************
// Metodo              : Permite entregar el promedio diario por bloque por fechas
//                       para estad�sticas, la fecha la recibe aaaammdd
// Programadora        : Monica B.
// Fecha creacion      : 14/05/2008
// Fecha modificacion  :
//****************************************************************************
public Vector getPromedioBloque (int año, int semestre, String fecha_inicio,
                                  String fecha_termino, int sala, int detalle){
Vector v = new Vector();
Vector d = new Vector();
/*
String query = "exec spu_list_PromedioBloqueGim " + año +"," +
              semestre + ",'" +
              fecha_inicio + "','" +
              fecha_termino + "'," +
              sala + ", " + detalle;
// System.out.println("getPromedioBloque: "+ query);*/
Connection con2 = con.conexion();
try{
  
  //Statement sent = con.conexion().createStatement();
  //ResultSet res = sent.executeQuery(query);
  CallableStatement cstmt = con2.prepareCall("{call spu_list_PromedioBloqueGim (?,?,?,?,?,?,?,?)}");
  cstmt.setInt(1,año);
  cstmt.setInt(2,semestre);
  cstmt.setString(3,fecha_inicio);
  cstmt.setString(4,fecha_termino);
  cstmt.setInt(5, sala);
  cstmt.setInt(6, detalle);
  cstmt.registerOutParameter(7, Types.INTEGER);
  cstmt.registerOutParameter(8, OracleTypes.CURSOR);
  
  cstmt.executeQuery();
  ResultSet res = (ResultSet) cstmt.getObject(8);
  
  while (res.next()){
      d = new Vector();
      d.addElement(res.getString(1)); // nombre del d�a
      d.addElement(res.getString(2)); // numero de dia
      d.addElement(res.getString(3)); // secuencia_inicio
      d.addElement(res.getString(4)); // secuencia_termino
      d.addElement(res.getString(5)); // sum_dia_masc
      d.addElement(res.getString(6));  // sum_dia_fem
      d.addElement(res.getString(7));// cont_dia
      v.addElement(d);
  }
  res.close();
  cstmt.close();
}
catch(SQLException e){
  System.out.println("Error en Reserva.getPromedioBloque : "+e);
}
return v;
}

//**************************************************************************
// Metodo              : Permite entregar el promedio diario por bloque por fechas
//                       para estad�sticas, la fecha la recibe aaaammdd
//                       para la sala 10336
// Programadora        : Monica B.
// Fecha creacion      : 23/07/2008
// Fecha modificacion  :
//****************************************************************************
    public Vector getPromedioBloqueDet (int año, int semestre, String fecha_inicio,
                                      String fecha_termino, int sala, int detalle){
    Vector v = new Vector();
    Vector d = new Vector();
    /*
    String query =  "exec spu_list_PromedioBloqueDetGim " + año +"," +
                  semestre + ",'" +
                  fecha_inicio + "','" +
                  fecha_termino + "'," +
                  sala + ", " + detalle;
	*/
   // System.out.println("getPromedioBloqueDet: "+ query);
    Connection con2 = con.conexion();
    try{
      //Statement sent = con.conexion().createStatement();
      //ResultSet res = sent.executeQuery(query);
      CallableStatement cstmt = con2.prepareCall("{call spu_list_PromedioBloqueDetGim(?,?,?,?,?,?,?)}");
      cstmt.setInt(1, año);
      cstmt.setInt(2, semestre);
      cstmt.setString(3, fecha_inicio);
      cstmt.setString(4, fecha_termino);
      cstmt.setInt(5, sala);
      cstmt.setInt(6, detalle);
      cstmt.registerOutParameter(7, OracleTypes.CURSOR);

      cstmt.executeQuery();
    	
      ResultSet res = (ResultSet) cstmt.getObject(7);
      
      while (res.next()){
          d = new Vector();
          d.addElement(res.getString(1)); // nombre actividad
          d.addElement(res.getString(2)); // cupo
          d.addElement(res.getString(3)); // nom_dia
          d.addElement(res.getString(4)); // num_dia
          d.addElement(res.getString(5)); // hora_inicio
          d.addElement(res.getString(6)); // hora_fin
          d.addElement(res.getString(7));// sum_dia_masc
          d.addElement(res.getString(8));// sum_dia_fem
          d.addElement(res.getString(9));// cont
          d.addElement(res.getString(10));// alumno_m
          d.addElement(res.getString(11));// alumno_f
          d.addElement(res.getString(12));// tutoria_m
          d.addElement(res.getString(13));// tutoria_f
          d.addElement(res.getString(14));// rama_m
          d.addElement(res.getString(15));// rama_f
          d.addElement(res.getString(16));// funcionario_m
          d.addElement(res.getString(17));// funcionario_f
          v.addElement(d);
      }
      res.close();
      cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getPromedioBloqueDet : "+e);
    }
    return v;
    }
//**************************************************************************
// Metodo              : Permite entregar el promedio diario por semanas
//                       para estad�sticas, la fecha la recibe aaaammdd
//                       para la sala 10335
// Programadora        : Monica B.
// Fecha creacion      : 23/07/2008
// Fecha modificacion  :
//****************************************************************************
        public Vector getPromedioSemana (int año, int semestre, String fecha_inicio,
                                          String fecha_termino, int sala, int detalle){
        Vector v = new Vector();
        Vector d = new Vector();
        
        /*
        String query =  "exec spu_list_PromedioSemanaGim " + año +"," +
                      semestre + ",'" +
                      fecha_inicio + "','" +
                      fecha_termino + "'," +
                      sala + ", " + detalle;
//       System.out.println("getPromedioBloqueDet: "+ query);
        */
        
        Connection con2 = con.conexion();
        
        try{
        	CallableStatement cstmt = con2.prepareCall("{call spu_list_PromedioSemanaGim(?,?,?,?,?,?,?)}");
          //Statement sent = con.conexion().createStatement();
          //ResultSet res = sent.executeQuery(query);
          cstmt.setInt(1, año);
          cstmt.setInt(2, semestre);
          cstmt.setString(3, fecha_inicio);
          cstmt.setString(4,fecha_termino);
          cstmt.setInt(5,sala);
          cstmt.setInt(6,detalle);
          cstmt.registerOutParameter(7, OracleTypes.CURSOR);
          	
          cstmt.executeQuery();
      	
          ResultSet res = (ResultSet) cstmt.getObject(7);
          
    	  while (res.next()){
              d = new Vector();
              d.addElement(res.getString(1)); // nom_dia
              d.addElement(res.getString(2)); // num_dia
              d.addElement(res.getString(3)); // fecha_inicio
              d.addElement(res.getString(4)); // fecha_fin
              d.addElement(res.getString(5)); // sum_masc
              d.addElement(res.getString(6)); // sum_fem
              d.addElement(res.getString(7));// fecha
              d.addElement(res.getString(8));// nom_usuario
/*            d.addElement(res.getString(9));// cont
              d.addElement(res.getString(10));// alumno_m
              d.addElement(res.getString(11));// alumno_f
              d.addElement(res.getString(12));// tutoria_m
              d.addElement(res.getString(13));// tutoria_f
              d.addElement(res.getString(14));// rama_m
              d.addElement(res.getString(15));// rama_f
              d.addElement(res.getString(16));// funcionario_m
              d.addElement(res.getString(17));// funcionario_f
  */            v.addElement(d);
          }
          res.close();
          cstmt.close();
        }
        catch(SQLException e){
          System.out.println("Error en Reserva.getPromedioSemana : "+e);
        }
        //System.out.println("v:"+v);
        return v;
        }

//**************************************************************************
// Metodo              : Permite el porcentaje de uso por fechas para estad�sticas
//                      la fecha la recibe aaaammdd, con detalle
// Programadora        : Monica B.
// Fecha creacion      : 4/03/2008
// Fecha modificacion  : 24/07/2008
//****************************************************************************
public Vector getPorcentajeUsoDetalle (int año, int semestre, String fecha_inicio,
                                   String fecha_termino, int sala, int detalle){
Vector v = new Vector();
Vector a = new Vector();
/*
 String query =  "exec spu_list_PorcentajeGimDet "+ año +"," +
                semestre + ",'" +
                fecha_inicio + "','" +
                fecha_termino + "'," +
                sala +"," +
                detalle;

System.out.println("Reserva.getPorcentajeUsoDetalle: "+ query);*/
Connection con2 = con.conexion();
 try{
	 //Statement sent = con.conexion().createStatement();
	 //ResultSet res = sent.executeQuery(query);
	 CallableStatement cstmt = con2.prepareCall("{call spu_list_porcentajegimdet(?,?,?,?,?,?,?)}");
	 
	 cstmt.setInt(1, año);
	 cstmt.setInt(2, semestre);
	 cstmt.setString(3, fecha_inicio);
	 cstmt.setString(4, fecha_termino);
	 cstmt.setInt(5, sala);
	 cstmt.setInt(6, detalle);
	 cstmt.registerOutParameter(7, OracleTypes.CURSOR);
	 cstmt.executeQuery();

	 
	 ResultSet res = (ResultSet) cstmt.getObject(7);
	 int numRows =  res.getRow();
     while (numRows > 0 && res.next()){

     a = new Vector();
     a.addElement(res.getString(1)); // 0 nom_d�a
     a.addElement(res.getString(2)); // 1 fecha
     a.addElement(res.getString(3)); // 2 num_dia
     a.addElement(res.getString(4)); // 3 sec_ini
     a.addElement(res.getString(5)); // 4 sec_fin
     a.addElement(res.getString(6)); // 5 sum_masc
     a.addElement(res.getString(7)); // 6 sum_fem
     a.addElement(res.getString(8)); // 7 fecha_emision
     a.addElement(res.getString(9)); // 8 actividad
     a.addElement(res.getString(10)); // 9 cupo
     a.addElement(res.getString(11)); // 10  hora inicio
     a.addElement(res.getString(12)); // 11 hora fin
     v.addElement(a);
   }

   res.close();

   cstmt.close();

 }
 catch(SQLException e){
   System.out.println("Error en Reserva.getPorcentajeUsoDetalle : "+e);
 }
 return v;
}
  //**************************************************************************
  // Metodo              : Permite contar los alumnos de rama inscritos en periodo
  // Programadora        : Monica B.
  // Fecha creacion      : 2/08/2007
  // Fecha modificacion  : 1/04/2008 debido a que se agregaron los sig. campos:
  //                       secuencia, año y semestre en hora_bloque_sala
  //****************************************************************************
  public int getContInscritosRamaGimnasio (int año, int semestre, int dia, String fecha,
      String periodo, int bloque1, int bloque2, int sala){
    int cantidad = 0;
//    String query = " SELECT count(rg.resgim_id)" +
//                   " FROM reserva_gimnasio rg LEFT JOIN asistencia_gimnasio ag ON "+
//                   " ag.resgim_id = rg.resgim_id "+
//                   " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha +
//                   "' " +
//                   " , registro_gimnasio regg" +
//                   " WHERE rg.pro_año = " + año +
//                   " AND rg.pro_semestre = " + semestre +
//                   " AND rg.num_dia = " + dia +
//                   " AND rg.pergim_id = " + periodo +
//                   " AND rg.reggim_id = regg.reggim_id" +
//                   " AND regg.reggim_fecha_termino is null" +
//                   " AND '" +  fecha + "' >= CONVERT(VARCHAR(8),rg.resgim_fecha_inicio,112)" +
//                   " AND '" + fecha + "' <= CONVERT(VARCHAR(8),rg.resgim_fecha_termino,112) " +
//                   " AND ((" + bloque1 + " >= rg.cod_bloque_inicio"  +
//                   " AND " + bloque1 + " <= rg.cod_bloque_termino) or"+
//                   " ( " + bloque2 + " >= rg.cod_bloque_inicio"  +
//                   " AND " + bloque2 + " <= rg.cod_bloque_termino))"+
//                   " AND (regg.cod_registro_gimnasio = 3 OR regg.cod_registro_gimnasio = 6)" +
//                   " AND  rg.cod_cupo = 3" ; // cupo de rama
    
    
    getSecuencia (sala, año, semestre, bloque1, bloque2);
	int sec_inicio = getSecuenciaInicio();
	int sec_termino = getSecuenciaTermino();
	
    /*gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
    b.getAbrirConexion();
    b.setGenerarConexionHorario();
    int sec_inicio = b.getSecuenciaBloque( sala, año, semestre, bloque1);
    int sec_termino = b.getSecuenciaBloque( sala, año, semestre, bloque2);
    b.getCerrarConexion();*/
	
    String query = " SELECT count(rg.resgim_id)" +
                   " FROM reserva_gimnasio rg LEFT JOIN asistencia_gimnasio ag ON "+
                   " ag.resgim_id = rg.resgim_id "+
                   " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = '" + fecha +
                   "' " +
                   " , registro_gimnasio regg, hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
                   " WHERE rg.pro_año = " + año +
                   " AND rg.pro_semestre = " + semestre +
                   " AND rg.num_dia = " + dia +
                   " AND rg.pergim_id = " + periodo +
                   " AND rg.reggim_id = regg.reggim_id" +
                   " AND regg.reggim_fecha_termino is null" +
                   //" AND '" +  fecha + "' >= TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd')" +
                   //" AND '" + fecha + "' <= TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd') " +
                   
                   " AND to_date('" + fecha +"','yyyymmdd') between rg.resgim_fecha_inicio and rg.resgim_fecha_termino" +
                   
                   " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
                   " AND hbs.horblo_año =  " + año +
                   " AND hbs.horblo_semestre = " + semestre +
                   " AND hbs.num_sala = " + sala +
                   " AND hbs.sed_cod_sede = 1" +
                   " AND hbs.cod_jornada = 1" +
                   " AND hbs.horblo_fecha_termino is null" +
                   " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
                   " AND hbs2.horblo_año =  " + año +
                   " AND hbs2.horblo_semestre = " + semestre +
                   " AND hbs2.num_sala = " + sala +
                   " AND hbs2.sed_cod_sede = 1" +
                   " AND hbs2.cod_jornada = 1" +
                   " AND hbs2.horblo_fecha_termino is null" +
                   //" AND ((" + sec_inicio + " >= hbs.horblo_secuencia"  +
                   //" AND " + sec_inicio + " <= hbs2.horblo_secuencia) or"+
                   //" ( " + sec_termino + " >= hbs.horblo_secuencia"  +
                   //" AND " + sec_termino + " <= hbs2.horblo_secuencia))"+

                   " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                   " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" +
                   
                   " AND (regg.cod_registro_gimnasio = 3 OR regg.cod_registro_gimnasio = 6)" +
                   " AND  rg.cod_cupo = 3" ; // cupo de rama

//System.out.println("getContInscritosRamaGimnasio: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if(res.next()){
        cantidad = res.getInt(1); // 0 cantidad
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getContInscritosRamaGimnasio : "+e);
    }
    return cantidad;
  }
  //**************************************************************************
  // Metodo              : Permite capturar el rut para luego tener los datos
  // Programadora        : Monica B.
  // Fecha creacion      : 26/07/2007
  // Fecha modificacion  : 01/04/2008 debido aque se agregaron campos a hora_bloque_sala
  //                       año, semestre y secuencia
  //****************************************************************************
  public Vector getInscritosGimnasio (int año, int semestre, int dia, String fecha,
                                      String periodo, int bloque1, int bloque2,
                                      int sala){
    Vector v = new Vector();
    Vector a = new Vector();
    Vector datos = new Vector();
    String rut = "";
    String digito = "";
    String paterno = "";
    String materno = "";
    String nombres = "";
    String sexo = "";
    String tipo = "";
    String tipo_reserva = "";


//    String query = " SELECT rg.resgim_id, regg.cod_registro_gimnasio,  rgf.fun_rut," +
//                   " f.fun_dig_rut, f.fun_ap_paterno, f.fun_ap_materno, f.fun_nombres," +
//                   " CASE f.fun_sexo WHEN 1 THEN 'M' WHEN 2 THEN 'F' END ," +
//                   " ISNULL(ag.cod_asistencia,0), ISNULL(ag.asigim_orden_llegada,0) as orden," +
//                   " rg.cod_cupo, ISNULL(ag.cod_origen_asistencia,0)" +
//                   " FROM reserva_gimnasio rg  LEFT JOIN asistencia_gimnasio ag ON" +
//                   " ag.resgim_id = rg.resgim_id" +
//                   " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha +
//                   " ', registro_gimnasio regg, registro_gimnasio_funcionario rgf ," +
//                   " funcionario f" +
//                   " WHERE rg.pro_año = " + año +
//                   " AND rg.pro_semestre = " + semestre +
//                   " AND rg.num_dia = " + dia +
//                   " AND rg.pergim_id =" + periodo +
//                   " AND rgf.reggim_id = regg.reggim_id" +
//                   " AND rgf.fun_rut = f.fun_rut" +
//                   " AND '" + fecha +"' >= CONVERT(VARCHAR(8),rg.resgim_fecha_inicio,112)" +
//                   " AND '" + fecha +"' <= CONVERT(VARCHAR(8),rg.resgim_fecha_termino,112)" +
//                   " AND ((" + bloque1 + " >= rg.cod_bloque_inicio" +
//                   " AND " + bloque1 + " <= rg.cod_bloque_termino) or" +
//                   " ( " + bloque2 + " >= rg.cod_bloque_inicio" +
//                   " AND " + bloque2 + " <= rg.cod_bloque_termino))" +
//                   " AND rg.reggim_id = regg.reggim_id" +
//                   " AND regg.reggim_fecha_termino is null" +
//                   " UNION" +
//                   " SELECT rg.resgim_id, regg.cod_registro_gimnasio,  rga.alu_rut," +
//                   " a.alu_dig_rut, a.alu_ap_paterno, a.alu_ap_materno, a.alu_nombres," +
//                   " CASE a.alu_sexo WHEN 1 THEN 'M' WHEN 2 THEN 'F' END ," +
//                   " ISNULL(ag.cod_asistencia,0), ISNULL(ag.asigim_orden_llegada,0) as orden," +
//                   " rg.cod_cupo, ISNULL(ag.cod_origen_asistencia,0)" +
//                   " FROM reserva_gimnasio rg  LEFT JOIN asistencia_gimnasio ag ON" +
//                   " ag.resgim_id = rg.resgim_id" +
//                   " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha + "',"+
//                   " registro_gimnasio regg, registro_gimnasio_alumno rga , alumno a" +
//                   " WHERE rg.pro_año = " + año +
//                   " AND rg.pro_semestre =" + semestre +
//                   " AND rg.num_dia = " + dia +
//                   " AND rg.pergim_id =" + periodo +
//                   " AND rga.reggim_id = regg.reggim_id" +
//                   " AND rga.alu_rut = a.alu_rut" +
//                   " AND '" + fecha +"' >= CONVERT(VARCHAR(8),rg.resgim_fecha_inicio,112)" +
//                   " AND '" + fecha +"' <= CONVERT(VARCHAR(8),rg.resgim_fecha_termino,112)" +
//                   " AND ((" + bloque1 + " >= rg.cod_bloque_inicio" +
//                   " AND " + bloque1 + " <= rg.cod_bloque_termino) or" +
//                   " ( " + bloque2 + " >= rg.cod_bloque_inicio" +
//                   " AND  " + bloque2 + " <= rg.cod_bloque_termino))" +
//                   " AND rg.reggim_id = regg.reggim_id" +
//                   " AND regg.reggim_fecha_termino is null" +
//                   " UNION" +
//                   " SELECT rg.resgim_id, regg.cod_registro_gimnasio,  rge.regext_rut," +
//                   " ' ', 'Z', 'Z', 'Z', ' ' , ISNULL(ag.cod_asistencia,0)," +
//                   " ISNULL(ag.asigim_orden_llegada,0) as orden, rg.cod_cupo," +
//                   " ISNULL(ag.cod_origen_asistencia,0)" +
//                   " FROM reserva_gimnasio rg  LEFT JOIN asistencia_gimnasio ag ON" +
//                   " ag.resgim_id = rg.resgim_id" +
//                   " AND CONVERT(VARCHAR(8),ag.asigim_fecha,112) = '" + fecha + "'," +
//                   " registro_gimnasio regg, registro_gimnasio_externo rge" +
//                   " WHERE rg.pro_año = " + año +
//                   " AND rg.pro_semestre = " + semestre +
//                   " AND rg.num_dia = " + dia +
//                   " AND rg.pergim_id = " + periodo +
//                   " AND rge.reggim_id = regg.reggim_id" +
//                   " AND '" + fecha +"' >= CONVERT(VARCHAR(8),rg.resgim_fecha_inicio,112)" +
//                   " AND '" + fecha +"' <= CONVERT(VARCHAR(8),rg.resgim_fecha_termino,112)" +
//                   " AND ((" + bloque1 + " >= rg.cod_bloque_inicio" +
//                   " AND " + bloque1 + " <= rg.cod_bloque_termino) or" +
//                   " ( " + bloque2 + " >= rg.cod_bloque_inicio" +
//                   " AND " + bloque2 + " <= rg.cod_bloque_termino))" +
//                   " AND rg.reggim_id = regg.reggim_id" +
//                   " AND regg.reggim_fecha_termino is null" +
//                   " ORDER BY orden";
    
    getSecuencia (sala, año, semestre, bloque1, bloque2);
	int sec_inicio = getSecuenciaInicio();
	int sec_termino = getSecuenciaTermino();
	
    /*gimnasio.BeanGeneral b1 = new gimnasio.BeanGeneral();
    b1.getAbrirConexion();
    b1.setGenerarConexionHorario();
    int sec_inicio = b1.getSecuenciaBloque( sala, año, semestre, bloque1);
    int sec_termino = b1.getSecuenciaBloque( sala, año, semestre, bloque2);
    b1.getCerrarConexion();*/
	
    String query = " SELECT rg.resgim_id, regg.cod_registro_gimnasio,  rgf.fun_rut," +
                   " NVL(f.fun_dig_rut,'0') AS fun_dig_rut,f.fun_ap_paterno, f.fun_ap_materno, f.fun_nombres," +
                   " CASE f.fun_sexo WHEN 1 THEN 'M' WHEN 2 THEN 'F' END ," +
                   " NVL(ag.cod_asistencia,0), NVL(ag.asigim_orden_llegada,0) as orden," +
                   " rg.cod_cupo, NVL(ag.cod_origen_asistencia,0)" +
                   " FROM reserva_gimnasio rg  LEFT JOIN asistencia_gimnasio ag ON" +
                   " ag.resgim_id = rg.resgim_id" +
                   " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = " + fecha +
                   " , registro_gimnasio regg, registro_gimnasio_funcionario rgf ," +
                   " funcionario f, hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
                   " WHERE rg.pro_año = " + año +
                   " AND rg.pro_semestre = " + semestre +
                   " AND rg.num_dia = " + dia +
                   " AND rg.pergim_id =" + periodo +
                   " AND rgf.reggim_id = regg.reggim_id" +
                   " AND rgf.fun_rut = f.fun_rut" +
                   //" AND " + fecha +" >= TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd')" +
                   //" AND " + fecha +" <= TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd')" +
                   
                   " AND to_date('" + fecha +"','yyyymmdd') between rg.resgim_fecha_inicio and rg.resgim_fecha_termino" +
                   
                   " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
                   " AND hbs.horblo_año =  " + año +
                   " AND hbs.horblo_semestre = " + semestre +
                   " AND hbs.num_sala = " + sala +
                   " AND hbs.sed_cod_sede = 1" +
                   " AND hbs.cod_jornada = 1" +
                   " AND hbs.horblo_fecha_termino is null" +
                   " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
                   " AND hbs2.horblo_año =  " + año +
                   " AND hbs2.horblo_semestre = " + semestre +
                   " AND hbs2.num_sala = " + sala +
                   " AND hbs2.sed_cod_sede = 1" +
                   " AND hbs2.cod_jornada = 1" +
                   " AND hbs2.horblo_fecha_termino is null" +
                   //" AND ((" + sec_inicio + " >= hbs.horblo_secuencia" +
                   //" AND " + sec_inicio + " <= hbs2.horblo_secuencia) or" +
                   //" ( " + sec_termino + " >= hbs.horblo_secuencia" +
                   //" AND " + sec_termino + " <= hbs2.horblo_secuencia))" +

                   " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                   " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" +
                   
                   " AND rg.reggim_id = regg.reggim_id" +
                   " AND regg.reggim_fecha_termino is null" +
                   " UNION" +
                   " SELECT rg.resgim_id, regg.cod_registro_gimnasio,  rga.alu_rut," +
                   " a.alu_dig_rut, a.alu_ap_paterno, a.alu_ap_materno, a.alu_nombres," +
                   " CASE a.alu_sexo WHEN 1 THEN 'M' WHEN 2 THEN 'F' END ," +
                   " NVL(ag.cod_asistencia,0), NVL(ag.asigim_orden_llegada,0) as orden," +
                   " rg.cod_cupo, NVL(ag.cod_origen_asistencia,0)" +
                   " FROM reserva_gimnasio rg  LEFT JOIN asistencia_gimnasio ag ON" +
                   " ag.resgim_id = rg.resgim_id" +
                   " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = " + fecha + ","+
                   " registro_gimnasio regg, registro_gimnasio_alumno rga , alumno a" +
                   " , hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
                   " WHERE rg.pro_año = " + año +
                   " AND rg.pro_semestre =" + semestre +
                   " AND rg.num_dia = " + dia +
                   " AND rg.pergim_id =" + periodo +
                   " AND rga.reggim_id = regg.reggim_id" +
                   " AND rga.alu_rut = a.alu_rut" +
                   //" AND " + fecha +" >= TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd')" +
                   //" AND " + fecha +" <= TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd')" +
                   
                   " AND to_date('" + fecha +"','yyyymmdd') between rg.resgim_fecha_inicio and rg.resgim_fecha_termino" +

                   " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
                   " AND hbs.horblo_año =  " + año +
                   " AND hbs.horblo_semestre = " + semestre +
                   " AND hbs.num_sala = " + sala +
                   " AND hbs.sed_cod_sede = 1" +
                   " AND hbs.cod_jornada = 1" +
                   " AND hbs.horblo_fecha_termino is null" +
                   " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
                   " AND hbs2.horblo_año =  " + año +
                   " AND hbs2.horblo_semestre = " + semestre +
                   " AND hbs2.num_sala = " + sala +
                   " AND hbs2.sed_cod_sede = 1" +
                   " AND hbs2.cod_jornada = 1" +
                   " AND hbs2.horblo_fecha_termino is null" +
                   //" AND ((" + sec_inicio + " >= hbs.horblo_secuencia" +
                   //" AND " + sec_inicio + " <= hbs2.horblo_secuencia) or" +
                   //" ( " + sec_termino + " >= hbs.horblo_secuencia" +
                   //" AND " + sec_termino + " <= hbs2.horblo_secuencia))" +

                   " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                   " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" +
                   
                   " AND rg.reggim_id = regg.reggim_id" +
                   " AND regg.reggim_fecha_termino is null" +
                   " UNION" +
                   " SELECT rg.resgim_id, regg.cod_registro_gimnasio,  rge.regext_rut," +
                   " ' ', 'Z', 'Z', 'Z', ' ' , NVL(ag.cod_asistencia,0)," +
                   " NVL(ag.asigim_orden_llegada,0) as orden, rg.cod_cupo," +
                   " NVL(ag.cod_origen_asistencia,0)" +
                   " FROM reserva_gimnasio rg  LEFT JOIN asistencia_gimnasio ag ON" +
                   " ag.resgim_id = rg.resgim_id" +
                   " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = " + fecha + "," +
                   " registro_gimnasio regg, registro_gimnasio_externo rge" +
                   " , hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
                   " WHERE rg.pro_año = " + año +
                   " AND rg.pro_semestre = " + semestre +
                   " AND rg.num_dia = " + dia +
                   " AND rg.pergim_id = " + periodo +
                   " AND rge.reggim_id = regg.reggim_id" +
                   " AND " + fecha +" >= TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd')" +
                   " AND " + fecha +" <= TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd')" +
                   " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
                   " AND hbs.horblo_año =  " + año +
                   " AND hbs.horblo_semestre = " + semestre +
                   " AND hbs.num_sala = " + sala +
                   " AND hbs.sed_cod_sede = 1" +
                   " AND hbs.cod_jornada = 1" +
                   " AND hbs.horblo_fecha_termino is null" +
                   " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
                   " AND hbs2.horblo_año =  " + año +
                   " AND hbs2.horblo_semestre = " + semestre +
                   " AND hbs2.num_sala = " + sala +
                   " AND hbs2.sed_cod_sede = 1" +
                   " AND hbs2.cod_jornada = 1" +
                   " AND hbs2.horblo_fecha_termino is null" +
                   " AND ((" + sec_inicio + " >= hbs.horblo_secuencia" +
                   " AND " + sec_inicio + " <= hbs2.horblo_secuencia) or" +
                   " ( " + sec_termino + " >= hbs.horblo_secuencia" +
                   " AND " + sec_termino + " <= hbs2.horblo_secuencia))" +
                   " AND rg.reggim_id = regg.reggim_id" +
                   " AND regg.reggim_fecha_termino is null" +
                   " ORDER BY orden";

    /*gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
    b.getAbrirConexionVi�a();
    b.setGenerarConexionDatosVi�a();*/


//System.out.println("getInscritosGimnasio: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      while (res.next()){

        rut = "";
        digito = "";
        paterno = "";
        materno = "";
        nombres = "";
        sexo = "";
        tipo = "";
        if (res.getInt(2) < 5){
          rut = res.getString(3);
          digito = res.getString(4);
          paterno = res.getString(5);
          materno = res.getString(6);
          nombres = res.getString(7);
          sexo = res.getString(8);
        }
        switch (res.getInt(2)){
          case 1 : case 5: case 6: {// es alumno 
            tipo = "<span class='Estilo_Azulino'>Alumno</span>";
            break;
          }
          case 2: {// es alumno tutor�a cc
            tipo = "<span class='Estilo_MoradoClaro'>Tutor�a</span>";
            break;
          }
          case 3: {// es alumno rama cc
            tipo = "<span class='Estilo_Cafe'>Rama</span>";
            break;
          }
          case 4: case 7:{ // es funcionario CC
            tipo = "<span class='Estilo_Azul'>Funcionario</span>";
            break;
          }
          /*case 5: case 6: { // alumno vi�a
            datos = b.getAlumnoVi�a(res.getInt(3));
            if (datos.size() > 0) {
              rut = datos.get(0)+"";
              digito = datos.get(17)+"";
              paterno = datos.get(14)+"";
              materno = datos.get(15)+"";
              nombres = datos.get(13)+"";
              sexo = datos.get(16)+"";
            }
            tipo = "<span class='Estilo_Azulino'>Alumno</span>";
            if (res.getInt(2) == 6)
              tipo = "<span class='Estilo_Cafe'>Rama</span>";
            break;
          }*/
        /*case 7: { // funcionario vi�a
          datos = b.getFuncionarioVi�a(res.getInt(3));
          if (datos.size() > 0) {
            rut = datos.get(3)+"";
            paterno = datos.get(1)+"";
            materno = datos.get(2)+"";
            nombres = datos.get(0)+"";
            sexo = datos.get(14)+"";
            digito= datos.get(15)+"";
          }
          tipo = "<span class='Estilo_Azul'>Funcionario</span>";
          break;
        }*/
        }

        a = new Vector();
        a.addElement(res.getString(1)); // 0 regg_id
        a.addElement(tipo); // 1 nombre cod_registro
        a.addElement(rut); // 2 rut
        a.addElement(digito); // 3 digito
        a.addElement(paterno); // 4 paterno
        a.addElement(materno); // 5 materno
        a.addElement(nombres); // 6 nombres
        a.addElement(sexo); // 7 sexo
        a.addElement(res.getString(9)); // 8 cod_asistencia
        a.addElement(res.getString(10)); // 9 orden de llegada
        a.addElement(res.getString(11));// 10 cod_cupo
        a.addElement(res.getString(12)); // 11 origen asistencia
        v.addElement(a);
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getInscritosGimnasio : "+e);
    }

/*
    b.getCerrarConexionVi�a();
    b.getCerrarConexionVi�a();
*/
    return v;
  }
  //**************************************************************************
  // Metodo              : Permite capturar la reserva del alumno tutor�a
  // Programadora        : MMR
  // Fecha creacion      : 27/07/2007
  // Fecha modificacion  : 18/05/2009 es para que permita cualquier sala
  //****************************************************************************
  public Vector getReservaTutoria (String id_reg, String id_per){
    java.util.Vector v = new java.util.Vector();

    String query = "";
    String dia = "";

    query = " SELECT pg.num_sala, rg.cod_asign, rg.pro_paralelo_asignatura,"+
            " rg.num_dia, rg.cod_bloque_inicio, rg.cod_bloque_termino" +
            " FROM reserva_gimnasio rg, periodo_gimnasio pg" +
            " WHERE rg.reggim_id = " + id_reg +
            " AND pg.pergim_id = " + id_per +
            " AND rg.pergim_id = pg.pergim_id" +
            " AND rg.cod_cupo  = 2";

 //System.out.println("getReservaTutoria: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      int cont = 0;
      while(res.next()){
        cont ++;
        if (cont == 1) v.addElement(res.getString(1)); // 0 sala
        v.addElement(res.getString(2)+"@"+
                     res.getString(3)+"@"+
                     res.getString(4)+"@"+
                     res.getString(5)+"@"+
                     res.getString(6)); // 1 reserva
        switch (res.getInt(4)){
          case 1: dia = "Lu";
            break;
          case 2: dia = "Ma";
            break;
          case 3: dia = "Mi";
            break;
          case 4: dia = "Ju";
            break;
          case 5: dia = "Vi";
            break;
          case 6: dia = "S�";
        }
        dia = dia + res.getString(5) + " - " + res.getString(6);
        v.addElement(dia); // 2 detalle
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Registro.getReservaTutoria  : "+e);
    }
    return v;
  }
//**************************************************************************
// Metodo              : Permite capturar la reserva del alumno tutor�a,
//                       independiente del per�odo, se captura el �ltimo semestre
// Programadora        : MBF
// Fecha creacion      : 27/07/2007
// Fecha modificacion  :
//****************************************************************************
public Vector getReservaAluTutoria (String id_reg){
  java.util.Vector v = new java.util.Vector();
  java.util.Vector a = new java.util.Vector();
  java.util.Vector v_existe = new java.util.Vector();
  String query = "";
  String sala = "";
  String dia = "";


  query = " SELECT pg.num_sala, rg.num_dia, hb1.hora_inicio," +
          " hb2.hora_termino" +
          " FROM reserva_gimnasio rg, periodo_gimnasio pg,  hora_bloque hb1," +
          " hora_bloque hb2" +
          " WHERE rg.reggim_id = " + id_reg +
          " AND rg.pergim_id = pg.pergim_id" +
          " AND rg.cod_cupo  = 2" +
          " AND rg.cod_bloque_inicio = hb1.cod_bloque" +
          " AND rg.sed_cod_sede = hb1.sed_cod_sede" +
          " AND rg.cod_jornada = hb1.cod_jornada"+
          " AND rg.cod_bloque_termino = hb2.cod_bloque" +
          " AND rg.sed_cod_sede = hb2.sed_cod_sede" +
          " AND rg.cod_jornada = hb2.cod_jornada" +
          " ORDER BY rg.pergim_id DESC";
      /*
          " AND  rg.pergim_id = (SELECT MAX(resg.pergim_id)" +
          " FROM reserva_gimnasio resg where resg.pro_año = rg.pro_año" +
          " AND resg.pro_semestre = rg.pro_semestre and rg.reggim_id = resg.reggim_id)";*/
// System.out.println("getReservaAluTutoria: "+ query);
  try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
     while(res.next()){

       if (!v_existe.contains(res.getString(1)+"-"+res.getString(2)+"-"+
              res.getString(3)+"-"+res.getString(4))) {

         v_existe.addElement(res.getString(1) + "-" + res.getString(2) + "-" +
                             res.getString(3) + "-" + res.getString(4));

         dia = "";
         a = new java.util.Vector();
         if (res.getInt(1) == 10335)
           sala = "Sala de M�quinas";
         else
           sala = "Aer�bica";
         switch (res.getInt(2)) {
           case 1:
             dia = "Lu";
             break;
           case 2:
             dia = "Ma";
             break;
           case 3:
             dia = "Mi";
             break;
           case 4:
             dia = "Ju";
             break;
           case 5:
             dia = "Vi";
             break;
           case 6:
             dia = "S�";
             break;
         }
         a.addElement(sala); //0 sala
         a.addElement(dia + " - " + res.getString(3) + " a " + res.getString(4)); // 1 reserva
         v.addElement(a);
       }
     }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Registro.getReservaAluTutoria  : "+e);
  }
  return v;
  }
  //**************************************************************************
// Metodo              : Permite capturar la reserva del alumno tutor�a,
//                       del per�odoespecificado
// Programadora        : MBF
// Fecha creacion      : 19/10/2007
// Fecha modificacion  :
//****************************************************************************
public Vector getReservaTutoriaPeriodo (String id_reg, int año, int semestre){
java.util.Vector v = new java.util.Vector();
java.util.Vector a = new java.util.Vector();
java.util.Vector v_asistencia = new java.util.Vector();
String query = "";
String sala = "";
String dia = "";
String condicion = "";
String asistencia = "";
String asis = "";

if (año != 0 && semestre != 0){
  condicion = " AND rg.pro_año = " + año +
              " AND rg.pro_semestre = " + semestre;

}

query = " SELECT  distinct  pg.num_sala, rg.num_dia, hb1.hora_inicio," +
         " hb2.hora_termino, rg.cod_bloque_inicio, rg.cod_bloque_termino," +
         " TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd')," +
         " TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd'), rg.pergim_id," +
         " rga.alu_rut" +
         " FROM reserva_gimnasio rg, periodo_gimnasio pg,  hora_bloque hb1," +
         " hora_bloque hb2, registro_gimnasio_alumno rga" +
         " WHERE rg.reggim_id = " + id_reg +
         " AND rg.pergim_id = pg.pergim_id" +
         " AND rg.cod_cupo  = 2" +
         " AND rg.cod_bloque_inicio = hb1.cod_bloque" +
         " AND rg.sed_cod_sede = hb1.sed_cod_sede" +
         " AND rg.cod_jornada = hb1.cod_jornada"+
         " AND rg.cod_bloque_termino = hb2.cod_bloque" +
         " AND rg.sed_cod_sede = hb2.sed_cod_sede" +
         " AND rg.cod_jornada = hb2.cod_jornada" +
         " AND rg.reggim_id = rga.reggim_id" +
       condicion ;
//System.out.println("getReservaTutoriaPeriodo: "+ query);
try{
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
   while(res.next()){
     dia = "";
    a = new java.util.Vector();
    if (res.getInt(1) == 10335) sala = "Sala de M�quinas";
    else sala = "Aer�bica";
    switch (res.getInt(2)){
      case 1: dia = "Lu";
        break;
      case 2: dia = "Ma";
        break;
      case 3: dia = "Mi";
        break;
      case 4: dia = "Ju";
        break;
      case 5: dia = "Vi";
        break;
      case 6: dia = "S�";
        break;
    }

    a.addElement(sala); //0 sala
    a.addElement(dia+" - "+res.getString(3) + " a "+res.getString(4)); // 1 reserva
    a.addElement(res.getString(10)); // 2 rut
    a.addElement(res.getString(7)); // 3 fec_inicio
    a.addElement(res.getString(8)); // 4 fec termino
    a.addElement(res.getString(9)); // 5 pergim_id
    a.addElement(res.getString(5)); // 6 bloque inicio
    a.addElement(res.getString(6)); // 7 bloque termino

    v.addElement(a);
   }
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Registro.ggetReservaTutoriaPeriodo  : "+e);
}
return v;
  }
  //**************************************************************************
// Metodo              : Permite capturar la reserva del alumno tutor�a,
//                       del per�odoespecificado
// Programadora        : MBF
// Fecha creacion      : 19/10/2007
// Fecha modificacion  : 20/05/2009 para quie muestre todos los periodos del semestre
//****************************************************************************
    public Vector getReservaTutoriaPeriodoList(String id_reg, int año,
                                               int semestre) {
      java.util.Vector v = new java.util.Vector();
      java.util.Vector a = new java.util.Vector();
      java.util.Vector v_asistencia = new java.util.Vector();
      java.util.Vector v_existe = new java.util.Vector();
      String query = "";
      String sala = "";
      String dia = "";
      String condicion = "";
      String asistencia = "";
      String asis = "";

      if (año != 0 && semestre != 0) {
        condicion = " AND rg.pro_año = " + año +
            " AND rg.pro_semestre = " + semestre
            /*+
                         " AND  rg.pergim_id = (SELECT MAX(resg.pergim_id)" +
             " FROM reserva_gimnasio resg where resg.pro_año = rg.pro_año" +
             " AND resg.pro_semestre = rg.pro_semestre and rg.reggim_id = resg.reggim_id)"*/
            ;

      }

      query = " SELECT pg.num_sala, rg.num_dia, hb1.hora_inicio," +
          " hb2.hora_termino, rg.cod_bloque_inicio, rg.cod_bloque_termino," +
          " TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd')," +
          " TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd'), rg.pergim_id," +
          " rga.alu_rut" +
          " FROM reserva_gimnasio rg, periodo_gimnasio pg,  hora_bloque hb1," +
          " hora_bloque hb2, registro_gimnasio_alumno rga" +
          " WHERE rg.reggim_id = " + id_reg +
          " AND rg.pergim_id = pg.pergim_id" +
          " AND rg.cod_cupo  = 2" +
          " AND rg.cod_bloque_inicio = hb1.cod_bloque" +
          " AND rg.sed_cod_sede = hb1.sed_cod_sede" +
          " AND rg.cod_jornada = hb1.cod_jornada" +
          " AND rg.cod_bloque_termino = hb2.cod_bloque" +
          " AND rg.sed_cod_sede = hb2.sed_cod_sede" +
          " AND rg.cod_jornada = hb2.cod_jornada" +
          " AND rg.reggim_id = rga.reggim_id" +
          condicion +
          " ORDER BY rg.pergim_id DESC";
//System.out.println("getReservaTutoriaPeriodoList: "+ query);
      try {
        Statement sent = con.conexion().createStatement();
        ResultSet res = sent.executeQuery(query);
        while (res.next()) {
          if (!v_existe.contains(res.getString(1)+"-"+res.getString(2)+"-"+
              res.getString(3)+"-"+res.getString(4)+"-"+
              res.getString(5)+"-"+res.getString(6))) {

            v_existe.addElement(res.getString(1) + "-" + res.getString(2) + "-" +
                                res.getString(3) + "-" + res.getString(4) + "-" +
                                res.getString(5) + "-" + res.getString(6));

            dia = "";
            a = new java.util.Vector();
            if (res.getInt(1) == 10335)
              sala = "Sala de M�quinas";
            else
              sala = "Aer�bica";
            switch (res.getInt(2)) {
              case 1:
                dia = "Lu";
                break;
              case 2:
                dia = "Ma";
                break;
              case 3:
                dia = "Mi";
                break;
              case 4:
                dia = "Ju";
                break;
              case 5:
                dia = "Vi";
                break;
              case 6:
                dia = "S�";
                break;
            }

            a.addElement(sala); //0 sala
            a.addElement(dia + " - " + res.getString(3) + " a " +
                         res.getString(4)); // 1 reserva
            a.addElement(res.getString(10)); // 2 rut
            a.addElement(res.getString(7)); // 3 fec_inicio
            a.addElement(res.getString(8)); // 4 fec termino
            a.addElement(res.getString(9)); // 5 pergim_id
            a.addElement(res.getString(5)); // 6 bloque inicio
            a.addElement(res.getString(6)); // 7 bloque termino

            v.addElement(a);
          }
        }
        res.close();
        sent.close();
      }
      catch (SQLException e) {
        System.out.println(
            "Error en Registro.ggetReservaTutoriaPeriodoList  : " + e);
      }
      return v;
    }
  //**************************************************************************
  // Metodo              : Permite capturar el tipo de reserva del horario
  // Programadora        : MB
  // Fecha creacion      : 13/08/2007
  // Fecha modificacion  : 12/05/2008 se agreg� fecha de vigencia de horario_asignatura,
  //                       solicitado por Jorge M
  //****************************************************************************
  public int getReservaTipo (int año, int semestre, int cod_asign, int paralelo,
                             int sala, int jornada, int sede, int num_dia,
                             int blo1, int blo2){
    java.util.Vector v = new java.util.Vector();

    String query = "";
    int reserva = 0;
    query = " SELECT cod_reserva" +
            " FROM  horario_asignatura" +
            " WHERE cod_asign = " + cod_asign +
            " AND pro_año = " + año +
            " AND pro_semestre = " + semestre +
            " AND pro_paralelo_asignatura = " + paralelo +
            " AND num_sala = " + sala +
            " AND cod_jornada = " + jornada +
            " AND sed_cod_sede = " + sede +
            " AND num_dia = " + num_dia +
            " AND cod_bloque_inicio = " + blo1 +
            " AND cod_bloque_termino = " + blo2 +
            " AND cod_motivo = 6" +
            " AND horasi_fecha_termino IS NULL ";
//System.out.println("getReservaTipo: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      int cont = 0;
      while(res.next()){
        reserva = res.getInt(1); // reserva
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Registro.getReservaTipo  : "+e);
    }
    return reserva;
  }

  //**************************************************************************
  // Metodo              : Permite capturar los cupos en periodo
  // Programadora        : Monica B.
  // Fecha creacion      : 26/07/2007
  // Fecha modificacion  : 1/04/2008 debido a que se agregaron los sig. campos:
  //                       secuencia, año y semestre en hora_bloque_sala
  //                     : 12/05/2008 se agreg� fecha de vigencia de horario_asignatura,
  //                       solicitado por Jorge M
  //****************************************************************************
  public int getCupoGimnasio (int año, int semestre, int dia, int bloque1,
                              int bloque2, int sala, int codAsign){
    int cantidad = 0;
    String cond1 = (dia > 0)?(" AND ha.num_dia = " + dia) :"" ;
    String cond2 = "";
    String condicionAsignatura = "";
    if (bloque1 > 0 && bloque2 > 0) {
//      cond2 =   " AND ((" + bloque1 + " >= ha.cod_bloque_inicio" +
//                " AND " + bloque1 +  "<= ha.cod_bloque_termino) or " +
//                " ( " + bloque2 + " >= ha.cod_bloque_inicio" +
//                " AND " + bloque2 +  "<= ha.cod_bloque_termino))" ;
     
     getSecuencia (sala, año, semestre, bloque1, bloque2);
     int sec_inicio = getSecuenciaInicio();
     int sec_termino = getSecuenciaTermino();
    	
     /*gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
     b.getAbrirConexion();
     b.setGenerarConexionHorario();
     int sec_inicio = b.getSecuenciaBloque(sala, año, semestre, bloque1);
     int sec_termino = b.getSecuenciaBloque(sala, año, semestre, bloque2);

     b.getCerrarConexion();*/

      cond2 =   //" AND ((" + sec_inicio + " >= hbs.horblo_secuencia" +
                //" AND " + sec_inicio +  "<= hbs2.horblo_secuencia) or " +
                //" ( " + sec_termino + " >= hbs.horblo_secuencia" +
                //" AND " + sec_termino +  "<= hbs2.horblo_secuencia))" 

                " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" ;
    }
    String cond3 = "";
    if (sala > 0)
      cond3 = " AND ha.num_sala = " + sala ;
    
    if(codAsign == 0)condicionAsignatura = "";
    else condicionAsignatura = " AND ha.cod_asign = "+codAsign ;


    String query = " SELECT p.pro_cupo" +
                   " FROM horario_asignatura ha, programacion p," +
                   " hora_bloque_sala hbs, hora_bloque_sala hbs2 " +
                   " WHERE ha.sed_cod_sede = 1" +
                   " AND ha.pro_año = " + año +
                   " AND ha.pro_semestre = " + semestre +
                   " AND ha.cod_jornada = 1" +
                   condicionAsignatura+
                //   " AND ha.horasi_fecha_termino IS NULL" +
                   " AND hbs.cod_bloque = ha.cod_bloque_inicio" +
                   " AND hbs.sed_cod_sede = 1 " +
                   " AND hbs.cod_jornada = 1" +
                   " AND hbs.horblo_año = " + año +
                   " AND hbs.horblo_semestre = " + semestre +
                   " AND hbs.horblo_fecha_termino IS NULL" +
                   " AND hbs.num_sala = " + sala +
                   " AND hbs2.cod_bloque = ha.cod_bloque_termino" +
                   " AND hbs2.sed_cod_sede = 1 " +
                   " AND hbs2.cod_jornada = 1" +
                   " AND hbs2.horblo_año = " + año +
                   " AND hbs2.horblo_semestre = " + semestre +
                   " AND hbs2.horblo_fecha_termino IS NULL" +
                   " AND hbs2.num_sala = " + sala +
                   cond1 +
                   cond2 +
                   cond3 +
                   " AND p.pro_año = ha.pro_año" +
                   " AND p.pro_semestre = ha.pro_semestre" +
                   " AND p.cod_asign = ha.cod_asign" +
                   " AND p.pro_paralelo_asignatura = ha.pro_paralelo_asignatura";

//System.out.println("getCupoGimnasio: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if(res.next()){
        cantidad = res.getInt(1); // 0 cantidad
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getCupoGimnasio : "+e);
    }
    return cantidad;
  }
  //**************************************************************************
 // Metodo              : Permite capturar los cupos de rama
 // Programadora        : Monica B.
 // Fecha creacion      : 24/08/2007
 // Fecha modificacion  : 01/04/2008 debido a que se agregaron los campos:
 //                       año, semestre y secuencia a hora_bloque_sala
 // Fecha modificacion  : 12/05/2008 se agreg� fecha de vigencia de horario_asignatura,
 //                       solicitado por Jorge M
 //****************************************************************************
 public int getSobreCupoGimnasio (int año, int semestre, int dia, int bloque1,
                             int bloque2, int sala){
   
	
   getSecuencia (sala, año, semestre, bloque1, bloque2);
   int sec_inicio = getSecuenciaInicio();
   int sec_termino = getSecuenciaTermino();
		
   /*gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
   b.getAbrirConexion();
   b.setGenerarConexionHorario();
   int sec_inicio = b.getSecuenciaBloque( sala, año, semestre, bloque1);
   int sec_termino = b.getSecuenciaBloque( sala, año, semestre, bloque2);
   b.getCerrarConexion();*/
   int cantidad = 0;
   String query = " SELECT p.pro_sobrecupo" +
                  " FROM horario_asignatura ha, programacion p" +
                  " , hora_bloque_sala hbs,  hora_bloque_sala hbs2" +
                  " WHERE ha.sed_cod_sede = 1" +
                  " AND ha.pro_año = " + año +
                  " AND ha.pro_semestre = " + semestre +
                  " AND ha.cod_jornada = 1" +
                  " AND ha.num_dia = " + dia +
                  " AND ha.horasi_fecha_termino IS NULL" +
                  " AND ha.cod_bloque_inicio = hbs.cod_bloque" +
                  " AND hbs.horblo_año =  " + año +
                  " AND hbs.horblo_semestre = " + semestre +
                  " AND hbs.num_sala = " + sala +
                  " AND hbs.sed_cod_sede = 1" +
                  " AND hbs.cod_jornada = 1" +
                  " AND hbs.horblo_fecha_termino is null" +
                  " AND ha.cod_bloque_termino = hbs2.cod_bloque" +
                  " AND hbs2.horblo_año =  " + año +
                  " AND hbs2.horblo_semestre = " + semestre +
                  " AND hbs2.num_sala = " + sala +
                  " AND hbs2.sed_cod_sede = 1" +
                  " AND hbs2.cod_jornada = 1" +
                  " AND hbs2.horblo_fecha_termino is null" +
                  //" AND ((" + sec_inicio + " >= hbs.horblo_secuencia" +
                  //" AND " + sec_inicio +  "<= hbs2.horblo_secuencia) or " +
                  //" ( " + sec_termino + " >= hbs.horblo_secuencia" +
                  //" AND " + sec_termino +  "<= hbs2.horblo_secuencia))" +

                  " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                  " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" +
                  
                  " AND ha.num_sala = " + sala +
                  " AND p.pro_año = ha.pro_año" +
                  " AND p.pro_semestre = ha.pro_semestre" +
                  " AND p.cod_asign = ha.cod_asign" +
                  " AND p.pro_paralelo_asignatura = ha.pro_paralelo_asignatura";

//System.out.println("getSobreCupoGimnasio: "+ query);
   try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     if(res.next()){
       cantidad = res.getInt(1); // 0 cantidad
     }
     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Reserva.getSobreCupoGimnasio : "+e);
   }
   return cantidad;
  }
  //**************************************************************************
  // Metodo              : Permite capturar EL ESTADO DE LA CLASE
  // Programadora        : Monica B.
  // Fecha creacion      : 2/08/2007
  // Fecha modificacion  :
  //****************************************************************************
  public Vector getEstadoClaseGimnasio (String fecha, int cod_asign, int año,
                                        int semestre, int paralelo, int jornada,
                                        int sede, int dia, int blo1, int blo2){
    Vector v = new Vector();
    String query = " SELECT cod_estado_clase" +
                   " FROM  estado_clase_gimnasio" +
                   " WHERE TO_CHAR(estcla_fecha,'yyyymmdd') = '" + fecha +
                   "' AND  cod_asign = " + cod_asign +
                   " AND pro_año = " + año  +
                   " AND pro_semestre = " + semestre +
                   " AND pro_paralelo_asignatura = " + paralelo +
                   " AND cod_jornada = " + jornada +
                   " AND sed_cod_sede = " + sede +
                   " AND num_dia = " + dia +
                   " AND cod_bloque_inicio = " + blo1 +
                   " AND cod_bloque_termino ="+ blo2;

//System.out.println("getEstadoClaseGimnasio: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if(res.next()){
        v.addElement(res.getString(1)); // 0 cod_estado_clase
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getEstadoClaseGimnasio : "+e);
    }
    return v;
  }

  //**************************************************************************
  // Metodo              : Permite capturar EL ESTADO DE LA CLASE para la muestra de reserva
  // Programadora        : MMR.
  // Fecha creacion      : 16/08/2007
  // Fecha modificacion  :
  //****************************************************************************
   public int getEstadoClaseGimnasioSemana (
         String fecha_inicio,
         String fecha_termino,
         int cod_asign, int año,
         int semestre, int paralelo, int jornada,
         int sede, int dia, int blo1, int blo2){

       int estado = 0;
       String query = " SELECT cod_estado_clase" +
                      " FROM  estado_clase_gimnasio" +
                      //" WHERE DATEDIFF(day, estcla_fecha, '" + fecha_termino + "') <= 6" +
                      " WHERE TO_CHAR(estcla_fecha,'yyyymmdd') <= '" + fecha_termino + "'" +
                      " AND TO_CHAR(estcla_fecha,'yyyymmdd') >= '" + fecha_inicio + "'" +
                      " AND cod_asign = " + cod_asign +
                      " AND pro_año = " + año  +
                      " AND pro_semestre = " + semestre +
                      " AND pro_paralelo_asignatura = " + paralelo +
                      " AND cod_jornada = " + jornada +
                      " AND sed_cod_sede = " + sede +
                      " AND num_dia = " + dia +
                      " AND cod_bloque_inicio = " + blo1 +
                      " AND cod_bloque_termino = "+ blo2;


 //System.out.println("getEstadoClaseGimnasioSemana: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if(res.next()) estado = res.getInt(1); // cod_estado_clase
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getEstadoClaseGimnasioSemana : "+e);
    }
    return estado;
  }

  //**************************************************************************
  // Metodo              : Permite contar la asistencia de: alumnos, tutor�a, de rama,
  //                       funcionario y en espera.
  // Programadora        : Monica B.
  // Fecha creacion      : 26/07/2007
  // Fecha modificacion  : 01/04/2008 debido aque se agregaron campos a hora_bloque_sala
   //                       año, semestre y secuencia
  //****************************************************************************
  public int getContAsistenciaGimnasio (int año, int semestre, int dia, String periodo,
                                        int bloque1, int bloque2, int cod_registro,
                                        String fecha, int sala){
    String condicion = "";
    String condicion2 =  " AND ag.cod_asistencia = 1 ";
    switch (cod_registro){
      case 1 : // es asistencia de alumnos
        condicion =  " AND regg.cod_registro_gimnasio IN (1,5)";
        break;
      case 2 : // es asistencia de alumnos tutoria
        condicion =  " AND regg.cod_registro_gimnasio = 2 ";
        break;
        // es asistencia de alumnos de rama deportiva
      case 3 : condicion =  " AND regg.cod_registro_gimnasio IN (3,6)";
      break;
    case 4 : // es asistencia de funcionarios
      condicion =  " AND regg.cod_registro_gimnasio IN (4,7)";
      break;
    case 5 : { // es asistencia lista de espera
      condicion =  "";
      condicion2 = " AND ag.cod_asistencia = 4 ";
      break;
    }
    }
    int cantidad = 0;
//    String query = " SELECT count(rg.resgim_id)" +
//                   " FROM reserva_gimnasio rg, registro_gimnasio regg," +
//                   " asistencia_gimnasio ag" +
//                   " WHERE rg.sed_cod_sede = 1" +
//                   " AND rg.pro_año = " + año +
//                   " AND rg.pro_semestre = " + semestre +
//                   " AND rg.num_dia = " + dia +
//                   " AND rg.pergim_id = " + periodo +
//                   " AND '" +  fecha + "' >= CONVERT(VARCHAR(8),rg.resgim_fecha_inicio,112) " +
//                   " AND '" + fecha + "' <= CONVERT(VARCHAR(8),rg.resgim_fecha_termino,112) " +
//                   " AND ((" + bloque1 + " >= rg.cod_bloque_inicio "  +
//                   " AND " + bloque1 + " <= rg.cod_bloque_termino) or" +
//                   "  (" + bloque2 + " >= rg.cod_bloque_inicio "  +
//                   " AND " + bloque2 + " <= rg.cod_bloque_termino))" +
//                   " AND rg.reggim_id = regg.reggim_id" +
//                   " AND regg.reggim_fecha_termino is null"+
//                   condicion +
//                   " AND rg.resgim_id = ag.resgim_id" +
//                   " AND ag.asigim_fecha = '" + fecha + "'" +
//                   condicion2 ;

    getSecuencia (sala, año, semestre, bloque1, bloque2);
	int sec_inicio = getSecuenciaInicio();
	int sec_termino = getSecuenciaTermino();
	
    /*gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
    b.getAbrirConexion();
    b.setGenerarConexionHorario();
    int secuencia_inicio = b.getSecuenciaBloque(sala, año, semestre, bloque1);
    int secuencia_termino = b.getSecuenciaBloque(sala, año, semestre, bloque2);
    b.getCerrarConexion();*/
	
	//System.out.println(bloque1+"-"+bloque2+"-"+sec_inicio+"-"+sec_termino);
	
    String query = " SELECT count(rg.resgim_id)" +
                   " FROM reserva_gimnasio rg, registro_gimnasio regg," +
                   " asistencia_gimnasio ag, hora_bloque_sala hbs, hora_bloque_sala hbs2" +
                   " WHERE rg.sed_cod_sede = 1" +
                   " AND rg.pro_año = " + año +
                   " AND rg.pro_semestre = " + semestre +
                   " AND rg.num_dia = " + dia +
                   " AND rg.pergim_id = " + periodo +
                   //" AND " +  fecha + " >= TO_CHAR(rg.resgim_fecha_inicio,'yyyymmdd') " +
                   //" AND " + fecha + " <= TO_CHAR(rg.resgim_fecha_termino,'yyyymmdd') " +
                   
                   " AND to_date('" + fecha +"','yyyymmdd') between rg.resgim_fecha_inicio and rg.resgim_fecha_termino" +
                   
                   " AND rg.cod_bloque_inicio = hbs.cod_bloque" +
                   " AND hbs.horblo_año =  " + año +
                   " AND hbs.horblo_semestre = " + semestre +
                   " AND hbs.num_sala = " + sala +
                   " AND hbs.sed_cod_sede = 1" +
                   " AND hbs.cod_jornada = 1" +
                   " AND hbs.horblo_fecha_termino is null" +
                   " AND rg.cod_bloque_termino = hbs2.cod_bloque" +
                   " AND hbs2.horblo_año =  " + año +
                   " AND hbs2.horblo_semestre = " + semestre +
                   " AND hbs2.num_sala = " + sala +
                   " AND hbs2.sed_cod_sede = 1" +
                   " AND hbs2.cod_jornada = 1" +
                   " AND hbs2.horblo_fecha_termino is null" +
                   //" AND ((" + sec_inicio + " >= hbs.horblo_secuencia "  +
                   //" AND " + sec_inicio + " <= hbs2.horblo_secuencia) or" +
                   //"  (" + bloque2 + " >= hbs.horblo_secuencia "  +
                   //" AND " + bloque2 + " <= hbs2.horblo_secuencia))" +
                   
                   " AND (" + sec_inicio +" between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                   " OR " + sec_termino + " between hbs.horblo_secuencia and hbs2.horblo_secuencia)" +

                   " AND rg.reggim_id = regg.reggim_id" +
                   " AND regg.reggim_fecha_termino is null"+
                   condicion +
                   " AND ag.resgim_id = rg.resgim_id" +
                   " AND TO_CHAR(ag.asigim_fecha,'yyyymmdd') = " + fecha + "" +
                   condicion2 ;

//System.out.println("getAsistenciaGimnasio: "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if(res.next()){
        cantidad = res.getInt(1); // 0 cantidad
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getContAsistenciaGimnasio : "+e);
    }
    return cantidad;
  }
  //**************************************************************************
  // Metodo              : Permite saber si el alumno es premiado o sancionado,
  //                       para el periodo
  // Programadora        : Monica B.
  // Fecha creacion      : 26/07/2007
  // Fecha modificacion  :
  //****************************************************************************
  public boolean getRegistroPremiadoSancionado (String periodo, String reggim_id,
      int cond){



    boolean muestra = false;
    String query = " SELECT reggim_id" +
                   " FROM estado_asistencia_gimnasio" +
                   " WHERE reggim_id = " + reggim_id +
                   " AND pergim_id = " + periodo +
                   " AND cod_asistencia = " + cond;

//System.out.println("getRegistroPremiadoSancionado : "+ query);
    try{
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if(res.next()){
        muestra = true; // es premiado o sancionado
      }
      res.close();
      sent.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.getRegistroPremiadoSancionado : "+e);
    }
    return muestra;
  }
  //**************************************************************************
// Metodo              : Permite saber la secuencia del bloque segun pa�metros
// Programadora        : Monica B.
// Fecha creacion      : 22/04/2008
// Fecha modificacion  :
//****************************************************************************
public int getSecuenciaHoraBloque (String horblo_id){



  int secuencia = 0;
  String query = " SELECT  horblo_secuencia" +
                 " FROM hora_bloque_sala" +
                 " WHERE horblo_id = " + horblo_id;

//System.out.println("getSecuenciaHoraBloque : "+ query);
  try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    if(res.next()){
      secuencia = res.getInt(1); // secuencia
    }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Reserva.getSecuenciaHoraBloque : "+e);
  }
  return secuencia;
  }
  //**************************************************************************
// Metodo              : Permite saber si est� reservado con aseo el bloque
// Programadora        : Monica B.
// Fecha creacion      : 22/04/2008
// Fecha modificacion  :
//****************************************************************************
public boolean getAseoBloque (     String horblo_id,
                                   int dia,
                                   String fecha){
  int secuencia = 0;
  secuencia = getSecuenciaHoraBloque(horblo_id);
  boolean aseo = false;
  String query = " SELECT ag.asegim_id " +
                 " FROM aseo_gimnasio ag, hora_bloque_sala hbs, hora_bloque_sala  hbs2" +
                 " WHERE "+
                 //" TO_CHAR(ag.asegim_fecha_inicio,'yyyymmdd') <= '" + fecha + "'" +
                 //" AND '" + fecha + "' <= TO_CHAR(NVL(ag.asegim_fecha_termino,SYSDATE),'yyyymmdd')" +
                 
                 " AND to_date('" + fecha +"','yyyymmdd') between ag.asegim_fecha_inicio and NVL(ag.asegim_fecha_termino,SYSDATE)" +
                 
                 " AND ag.horblo_inicio_id = hbs.horblo_id" +
                 " AND ag.horblo_termino_id = hbs2.horblo_id" +
                 //" AND  " + secuencia + " >= hbs.horblo_secuencia " +
                 //" AND  " + secuencia  + " <= hbs2.horblo_secuencia" +
                 
                 " AND  " + secuencia + " between hbs.horblo_secuencia and hbs2.horblo_secuencia" +
                 
                 " AND  " + dia + " >= ag.asegim_dia_inicio " +
                 " AND  " + dia + " <=  ag.asegim_dia_termino"  ;

//System.out.println("getAseoBloque : "+ query);
  try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    if(res.next()){
      aseo = true; // si hay aseo
    }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Reserva.getAseoBloque : "+e);
  }
  return aseo;
  }
  //**************************************************************************
// Metodo              : Permite saber si el alumno es premiado o sancionado,
//                       para el periodo
// Programadora        : Monica B.
// Fecha creacion      : 26/07/2007
// Fecha modificacion  :
//****************************************************************************
public Vector getPeriodoAnulado (String periodo){

Vector v = new Vector();
Vector a = new Vector();
String query = " SELECT distinct TO_CHAR(ecg.estcla_fecha,'dd/mm/yyyy'), ecg.cod_estado_clase" +
                 " FROM periodo_gimnasio pg, horario_asignatura ha, estado_clase_gimnasio ecg" +
                 " WHERE pg.pergim_id =" + periodo +
                 " AND pg.pergim_año = ha.pro_año" +
                 " AND pg.pergim_semestre = ha.pro_semestre" +
                 " AND pg.num_sala = ha.num_sala" +
                 " AND ecg.pro_año = ha.pro_año" +
                 " AND ecg.pro_semestre = ha.pro_semestre" +
                 " AND ecg.cod_asign = ha.cod_asign" +
                 " AND ecg.pro_paralelo_asignatura = ha.pro_paralelo_asignatura" +
                 " AND ecg.cod_jornada = ha.cod_jornada" +
                 " AND ecg.sed_cod_sede = ha.sed_cod_sede" +
                 " AND ecg.num_dia = ha.num_dia" +
                 " AND ecg.cod_bloque_inicio = ha.cod_bloque_inicio" +
                 " AND ecg.cod_bloque_termino = ha.cod_bloque_termino" +
                 " AND (ecg.cod_estado_clase = 4 or ecg.cod_estado_clase = 5)" +
                 " AND ecg.estcla_fecha >= pg.pergim_fecha_inicio_vig" +
                 " AND ecg.estcla_fecha <= pg.pergim_fecha_termino_vig" +
                 " ORDER BY 1";

//System.out.println("getPeriodoAnulado : "+ query);
  try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    while (res.next()){
      a = new Vector();
      a.addElement(res.getString(1)); // fecha
      a.addElement(res.getString(2)); // cod_estado_clase
      v.addElement(a);
    }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Reserva.getPeriodoAnulado : "+e);
  }
  return v;
  }

  //**************************************************************************
  // Metodo              : Permite insertar registro en per�odo gimnasio
  //
  // Programadora        : Monica B.
  // Fecha creacion      : 21/6/2007
  // Fecha modificacion  : 26/6/2007 pues un� todos los procedimientos en uno
  //****************************************************************************
  synchronized public int setPeriodoGimnasio(Vector vec)  throws SQLException{
    String par = "";
    String query = "";
    String fecha_inicio_vig = "";
    String fecha_termino_vig = "";
    String fecha_inicio_insc = "";
    String fecha_termino_insc = "";
    fecha_inicio_vig = (!(vec.get(3)+"").trim().equals(""))? SetFechaFormato(vec.get(3)+""):null;
    fecha_termino_vig = (!(vec.get(4)+"").trim().equals(""))?SetFechaFormato(vec.get(4)+""):null;
    fecha_inicio_insc = (!(vec.get(5)+"").trim().equals(""))?SetFechaFormato(vec.get(5)+""):null;
    fecha_termino_insc = (!(vec.get(6)+"").trim().equals(""))?SetFechaFormato(vec.get(6)+""):null;
    
    par = vec.get(0)+", "+ // año
          vec.get(1)+", "+ // semestre
          vec.get(2)+", "+ // num_sala
          fecha_inicio_vig+", "+ // fecha_inicio_vig
          fecha_termino_vig+", "+ // fecha_termino_vig
          fecha_inicio_insc+", "+ // fecha_inicio_insc
          fecha_termino_insc+", "+ // fecha_termino_insc
          vec.get(8)+", " + // pergim_id
          vec.get(7)+", " + // rut_usuario
          vec.get(9)+""; // accion

    //query = "exec spu_tran_periodo_gimnasio " + par;
    //System.out.println("setPeriodoGimnasio: "+ par);
	Connection con2 = con.conexion();
    int error = -1;
    
    try{
    	 CallableStatement cstmt = con2.prepareCall("{call spu_tran_periodo_gimnasio(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
         cstmt.setInt(1, Integer.parseInt((String) vec.get(0)));// año
         cstmt.setInt(2, Integer.parseInt((String) vec.get(1)));// semestre
         cstmt.setInt(3, Integer.parseInt((String) vec.get(2)));// num_sala
         cstmt.setString(4, fecha_inicio_vig);// fecha_inicio_vig
         cstmt.setString(5, fecha_termino_vig);// fecha_termino_vig
         cstmt.setString(6, fecha_inicio_insc);// fecha_inicio_insc
         cstmt.setString(7, fecha_termino_insc);// fecha_termino_insc
         cstmt.setInt(8, Integer.parseInt((String) vec.get(8)));// pergim_id
         cstmt.setInt(9, Integer.parseInt((String) vec.get(7)));// rut_usuario
         cstmt.setInt(10, Integer.parseInt((String) vec.get(9)));// accion
         cstmt.registerOutParameter(11, Types.INTEGER);
         cstmt.executeQuery();
         error = cstmt.getInt(11);
         cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.setPeriodoGimnasio: "+e);
    }
    return error;
  }
  //**************************************************************************
// Metodo              : Permite insertar registro en per�odo gimnasio
//
// Programadora        : Monica B.
// Fecha creacion      : 21/6/2007
// Fecha modificacion  : 26/6/2007 pues un� todos los procedimientos en uno
//****************************************************************************

synchronized public int setEstadoClasePeriodo(Vector vec){
  String par = "";
  String query = "";
  String fecha_inicio = "";
  String fecha_termino = "";
  fecha_inicio = (!(vec.get(1)+"").trim().equals(""))? SetFechaFormato(vec.get(1)+""):null;
  fecha_termino = (!(vec.get(2)+"").trim().equals(""))?SetFechaFormato(vec.get(2)+""):null;
  /*par = vec.get(0) + "," + //pergim_id
        fecha_inicio + "," + // @fecha_inicio
        fecha_termino + "," + //fecha_termino
        vec.get(3)+","+// cod_estado_clase
	vec.get(4)+""; // rut_usuario
  query = "exec   spu_tran_estado_clase_periodo " + par;*/

//System.out.println("setEstadoClasePeriodo: "+ query);

	//Connection con2 = con.conexion();
	
  int error = -1;
  Connection con2 = con.conexion();
  try{  
	  CallableStatement cstmt = con2.prepareCall("{call spu_tran_estado_clase_periodo(?, ?, ?, ?, ?, ?)}");
      cstmt.setInt(1, Integer.parseInt((String) vec.get(0)));
      cstmt.setString(2, (String) vec.get(1));
      cstmt.setString(3, (String) vec.get(2));
      cstmt.setInt(4, Integer.parseInt((String) vec.get(3)));
      cstmt.setInt(5, Integer.parseInt((String) vec.get(4)));
      cstmt.registerOutParameter(6, Types.INTEGER);
      cstmt.executeQuery();
      error = cstmt.getInt(6);
      cstmt.close();
  }
  catch(SQLException e){
    System.out.println("Error en Reserva.setEstadoClasePeriodo: "+e);
  }
  return error;
  }
	  
//**************************************************************************
// Metodo              : Permite actualizar registro en estado_clase_gimnasio
//
// Programadora        : Monica B.
// Fecha creacion      : 13/09/2007
// Fecha modificacion  :
//****************************************************************************
synchronized public int setUpdEstadoClasePeriodo(Vector vec){
  String par = "";
  String query = "";
  String fecha = "";
  fecha = (!(vec.get(1)+"").trim().equals(""))? SetFechaFormato(vec.get(1)+""):null;

 /* par = vec.get(0) + "," + //pergim_id
        fecha + "," + // @fecha
        vec.get(2)+","+// cod_estado_clase
        vec.get(3)+""; // rut_usuario
  query = "exec   spu_upd_estado_clase_periodo " + par;*/
//System.out.println("setUpdEstadoClasePeriodo: "+ query);
  int error = -1;
  Connection con2 = con.conexion();
  try{  
    //Statement sent = con.conexion().createStatement();
    //ResultSet res  = sent.executeQuery(query);
     CallableStatement cstmt = con2.prepareCall("{call spu_upd_estado_clase_periodo(?,?,?,?,?)}");
     cstmt.setLong(1, Long.parseLong((String) vec.get(0)));
     cstmt.setString(2, fecha);
     cstmt.setInt(3, Integer.parseInt((String) vec.get(2)));
     cstmt.setInt(4, Integer.parseInt((String) vec.get(3)));
     cstmt.registerOutParameter(5, Types.INTEGER);  
     cstmt.executeQuery();
	 error = cstmt.getInt(5);
	 cstmt.close();
  }
  catch(SQLException e){
    System.out.println("Error en Reserva.setUpdEstadoClasePeriodo: "+e);
  }
  return error;
  }
  //**************************************************************************
// Metodo              : Permite eliminar registro en estado_clase_gimnasio
//
// Programadora        : Monica B.
// Fecha creacion      : 13/09/2007
// Fecha modificacion  :
//****************************************************************************
synchronized public int setDltEstadoClasePeriodo(Vector vec) throws SQLException {
  //String par = "";
  //String query = "";
  String fecha = "";
  fecha = (!(vec.get(1)+"").trim().equals(""))? SetFechaFormato(vec.get(1)+""):null;

 /* par = vec.get(0) + "," + //pergim_id
        fecha + "" ; // @fecha
 query = "exec   spu_dlt_estado_clase_periodo " + par;*/
//System.out.println("setDltEstadoClasePeriodo: "+ query);
  Connection con2 = con.conexion();
 int error = -1;
  try{
	  CallableStatement cstmt = con2.prepareCall("{call SPU_DLT_ESTADO_CLASE_PERIODO(?, ?, ?)}");
      cstmt.setInt(1, Integer.parseInt((String) vec.get(0)));
      cstmt.setString(2, fecha);
      cstmt.registerOutParameter(3, Types.INTEGER);//PARAMETRO DE SALIDA
      cstmt.executeQuery();
      error = cstmt.getInt(3); // ES EL VALOR QUE RETORNA
      cstmt.close();
  }catch (Exception e) {
      System.out.println("Error Reserva.setDltEstadoClasePeriodo(): " + e.getMessage());
      //con2.rollback();
  }
  return error;
  }
  //**************************************************************************
// Metodo              : Permite Copiar el horario al semestre actual
//
// Programadora        : Monica B.
// Fecha creacion      : 26/09/2007
// Fecha modificacion  :
//****************************************************************************
synchronized public int setCopiarHorario (Vector vec) throws SQLException{
String par = "";
String query = "";
/*par = vec.get(0) + "," + //año
      vec.get(1) + "," + // semestre
      vec.get(2) + "," + // num_sala
      vec.get(3) + ""; // rut_usuario
query = "exec   spu_proc_copiar_horario_gim " + par;*/
//System.out.println("setCopiarHorario: "+ query);

Connection con2 = con.conexion();
int error = -1;
try{
	CallableStatement cstmt = con2.prepareCall("{call spu_proc_copiar_horario_gim(?, ?, ?, ?, ?)}");
	cstmt.setInt(1, Integer.parseInt((String) vec.get(0)));
	cstmt.setInt(2, Integer.parseInt((String) vec.get(1)));
	cstmt.setInt(3, Integer.parseInt((String) vec.get(2)));
	cstmt.setInt(4, Integer.parseInt((String) vec.get(3)));
	cstmt.registerOutParameter(5, Types.INTEGER);//PARAMETRO DE SALIDA
	cstmt.executeQuery();
	error = cstmt.getInt(5); // ES EL VALOR QUE RETORNA
	cstmt.close();
}catch (Exception e) {
    System.out.println("Error Reserva.setCopiarHorario(): " + e.getMessage());
    //con2.rollback();
}
return error;
  }

  //**************************************************************************
  // Metodo              : Permite insertar registro en reserva_gimnasio
  //
  // Programadora        : Monica B.
  // Fecha creacion      : 24/07/2007
  //****************************************************************************
  synchronized public int setReservaGimnasio(Vector vec)throws SQLException {
    String par = "";
    String query = "";
    String fecha_inicio = "";
    String fecha_termino = "";
    fecha_inicio = (!(vec.get(2)+"").trim().equals(""))? SetFechaFormato(vec.get(2)+""):null;
    fecha_termino = (!(vec.get(3)+"").trim().equals(""))?SetFechaFormato(vec.get(3)+""):null;
    /*par = vec.get(0)+", "+ // año
          vec.get(1)+", "+ // semestre
          fecha_inicio +", "+ // fecha_inicio
          fecha_termino+", "+ // fecha_termino
          vec.get(4) +", '" + // pergim_id
          vec.get(5) +"','" + // linea1
          vec.get(6)+"', " + // linea2
          vec.get(7)+","+ //rut
          vec.get(8)+","+// rut_usuario
          vec.get(9)+"," + // cod_asistencia
          "' '," + // linea3
          vec.get(10)+","+ // tipo_alumno
          vec.get(11)+"";// cod_cupo
    query = " exec spu_tran_reserva_gimnasio  " + par;
System.out.println("setReservaGimnasio: "+ query);*/


    Connection con2 = con.conexion();
    int error = -1;
    try{
    	//System.out.println("EXEC spu_tran_reserva_gimnasio("+vec.get(0)+","+vec.get(1)+",'"+fecha_inicio+"','"+fecha_termino+"',"+vec.get(4)+",'"+vec.get(5)+"','"+
    //													   vec.get(6)+"','"+vec.get(7)+"',"+vec.get(8)+","+vec.get(9)+",' ',"+vec.get(10)+","+vec.get(11)+","+vec.get(12)+",:v_cod_error);");
    	CallableStatement cstmt = con2.prepareCall("{call spu_tran_reserva_gimnasio(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
    	cstmt.setInt(1, Integer.parseInt((String) vec.get(0)));
    	cstmt.setInt(2, Integer.parseInt((String) vec.get(1)));
    	cstmt.setString(3, fecha_inicio);
    	cstmt.setString(4, fecha_termino);
    	cstmt.setInt(5, Integer.parseInt((String) vec.get(4)));
    	cstmt.setString(6, (String) vec.get(5));
    	cstmt.setString(7, (String) vec.get(6));
    	cstmt.setInt(8, Integer.parseInt((String) vec.get(7)));
    	cstmt.setInt(9, Integer.parseInt((String) vec.get(8)));
    	cstmt.setInt(10, Integer.parseInt((String) vec.get(9)));
    	cstmt.setString(11, " ");
    	cstmt.setInt(12, Integer.parseInt((String) vec.get(10)));
    	cstmt.setInt(13, Integer.parseInt((String) vec.get(11)));
    	cstmt.setInt(14, Integer.parseInt((String) vec.get(12)));
    	cstmt.registerOutParameter(15, Types.INTEGER);//PARAMETRO DE SALIDA
    	cstmt.executeQuery();
    	error = cstmt.getInt(15); // ES EL VALOR QUE RETORNA
    	cstmt.close();
    }catch(SQLException e){
      System.out.println("Error en Reserva.setReservaGimnasio: "+e.getMessage());
      //con2.rollback();
    }
    return error;
  }
  //**************************************************************************
  // Metodo              : Permite cerrar o anular clase
  //
  // Programadora        : Monica B.
  // Fecha creacion      : 02/08/2007
  //****************************************************************************
  synchronized public int setCerrarAnularClase(Vector vec) throws SQLException{
    String par = "";
    String query = "";
    /*par = vec.get(0)+", "+ // sede
          vec.get(1)+", "+ // año
          vec.get(2) +", "+ // semestre
          vec.get(3)+", "+ // jornada
          vec.get(4) +", " + // dia
          vec.get(5) +"," + // bloque1
          vec.get(6)+", " + // bloque2
          vec.get(7)+","+ //cod_asign
          vec.get(8)+",'"+// paralelo
          vec.get(9)+"'," + // fecha
          vec.get(10)+",'"+ // estado
          vec.get(11)+"',"+ // mensaje
          vec.get(12)+","+//pergim
          vec.get(13)+""; // rut_usuario
    query = " exec spu_tran_estado_clase_gimnasio  " + par;
System.out.println("setCerrarAnularClase: "+ query);*/
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_tran_estado_clase_gimnasio(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
    	cstmt.setInt(1, Integer.parseInt((String) vec.get(0)));// sede
    	cstmt.setInt(2, Integer.parseInt((String) vec.get(1))); // año
    	cstmt.setInt(3, Integer.parseInt((String) vec.get(2)));// semestre
    	cstmt.setInt(4, Integer.parseInt((String) vec.get(3)));// jornada
    	cstmt.setInt(5, Integer.parseInt((String) vec.get(4))); // dia
    	cstmt.setInt(6, Integer.parseInt((String) vec.get(5)));// bloque1
    	cstmt.setInt(7, Integer.parseInt((String) vec.get(6)));// bloque2
    	cstmt.setInt(8, Integer.parseInt((String) vec.get(7)));//cod_asign
    	cstmt.setInt(9, Integer.parseInt((String) vec.get(8)));// paralelo
    	cstmt.setString(10, (String) vec.get(9));// fecha
    	cstmt.setInt(11, Integer.parseInt((String) vec.get(10)));// estado
    	cstmt.setString(12, (String) vec.get(11)); // mensaje
    	cstmt.setString(13, (String) vec.get(12));//pergim
    	cstmt.setInt(14, Integer.parseInt((String) vec.get(13)));// rut_usuario
    	cstmt.registerOutParameter(15, Types.INTEGER);//PARAMETRO DE SALIDA
    	cstmt.executeQuery();
    	error = cstmt.getInt(15); // ES EL VALOR QUE RETORNA
    	cstmt.close();
         }
    catch(SQLException e){
      System.out.println("Error en Reserva.setCerrarAnularClase: "+ e.getMessage());
      // con2.rollback();      
    }
    return error;
  }

  //**************************************************************************
  // Metodo              : Permite insertar registro en reserva_gimnasio y asistencia
  //
  // Programadora        : Monica B.
  // Fecha creacion      : 31/07/2007
  //****************************************************************************
  synchronized public int setReservaAsistenciaGimnasio(Vector vec) throws SQLException{
    String par = "";
    String query = "";
    String fecha_inicio = "";
    String fecha_termino = "";
    fecha_inicio = (!(vec.get(2)+"").trim().equals(""))? SetFechaFormato(vec.get(2)+""):null;
    int cod_cupo = 0;
    if ((Integer.parseInt(vec.get(7)+"") < 4 && Integer.parseInt(vec.get(8)+"") > 0))
      cod_cupo = 3;
    else
      cod_cupo = 1;

    /*par = vec.get(0)+", "+ // año
          vec.get(1)+", "+ // semestre
          fecha_inicio +", "+ // fecha
          vec.get(3) +", '" + // pergim_id
          vec.get(4) +"'," + // linea1
          vec.get(5)+","+ //rut
          vec.get(6)+","+// rut_usuario
          vec.get(7)+"," + // cod_asistencia
          vec.get(8)+",'"+ // orden
          vec.get(9)+"','" + // fecha_inicio
          vec.get(10)+"'," + // fecha_termino
          vec.get(11)+","+ // tipo_alumno
          cod_cupo +", " +// cod_cupo
          vec.get(12)+""; // sala
    query = " exec spu_ins_reserva_asist_gimnasio  " + par;
 System.out.println("setReservaAsistenciaGimnasio: "+ query);*/

    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_ins_reserva_asist_gimnasio(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
    	cstmt.setInt(1, Integer.parseInt((String) vec.get(0)));
    	cstmt.setInt(2, Integer.parseInt((String) vec.get(1)));
    	cstmt.setString(3, fecha_inicio);
    	cstmt.setString(4, (String) vec.get(3));
    	cstmt.setString(5, (String) vec.get(4));
    	cstmt.setInt(6, Integer.parseInt((String) vec.get(5)));
    	cstmt.setInt(7, Integer.parseInt((String) vec.get(6)));
    	cstmt.setInt(8, Integer.parseInt((String) vec.get(7)));
    	cstmt.setInt(9, Integer.parseInt((String) vec.get(8)));
    	cstmt.setString(10, (String) vec.get(9));
    	cstmt.setString(11, (String) vec.get(10));
    	cstmt.setInt(12, Integer.parseInt((String) vec.get(11)));
    	cstmt.setInt(13, cod_cupo);
    	cstmt.setInt(14, Integer.parseInt((String) vec.get(12)));
    	cstmt.registerOutParameter(15, Types.INTEGER);//PARAMETRO DE SALIDA
    	cstmt.executeQuery();
    	error = cstmt.getInt(15); // ES EL VALOR QUE RETORNA
    	cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.setReservaAsistenciaGimnasio: "+ e.getMessage());
      //con2.rollback();   
    }
    return error;
  }
  //**************************************************************************
   // Metodo              : Permite eliminar registro en reserva_gimnasio y asistencia
   //
   // Programadora        : Monica B.
   // Fecha creacion      : 27/08/2007
   //****************************************************************************
   synchronized public int setEliminarAsistencia(Vector vec) throws SQLException{
     String par = "";
     String query = "";
     String fecha = "";
      fecha = (!(vec.get(1)+"").trim().equals(""))? SetFechaFormato(vec.get(1)+""):null;
/*     par = vec.get(0)+", "+ // resgim
           fecha+", '"+ //fecha
           vec.get(2) +"','"+ // fecha_inicio
           vec.get(3) + "',"+ // fecha_termino
           vec.get(4)+"," + // rut_usuario
           vec.get(5)+""; // sala
     query = " exec spu_dlt_reserva_asist_gimnasio  " + par;*/
   //  System.out.println("setEliminarAsistencia: "+ query);

      Connection con2 = con.conexion();
     int error = -1;
     try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_dlt_reserva_asist_gimnasio(?,?,?,?,?,?,?)}");
     	cstmt.setLong(1, Long.parseLong((String) vec.get(0)));
     	cstmt.setString(2, fecha);
     	cstmt.setString(3, (String) vec.get(2));
     	cstmt.setString(4, (String) vec.get(3));
     	cstmt.setInt(5, Integer.parseInt((String) vec.get(4)));
     	cstmt.setInt(6, Integer.parseInt((String) vec.get(5)));
     	cstmt.registerOutParameter(7, Types.INTEGER);//PARAMETRO DE SALIDA
     	cstmt.executeQuery();
     	error = cstmt.getInt(7); // ES EL VALOR QUE RETORNA
     	cstmt.close();
     }
     catch(SQLException e){
       System.out.println("Error en Reserva.setEliminarAsistencia: "+ e.getMessage());
       //con2.rollback(); 
     }
     return error;
  }
  //**************************************************************************
  // Metodo              : Permite insertar registro en asistencia_gimnasio
  //
  // Programadora        : Monica B.
  // Fecha creacion      : 27/07/2007
  //****************************************************************************
  synchronized public int setAsistenciaGimnasio(Vector vec) throws SQLException{
    String par = "";
    String query = "";
/*    par = vec.get(0)+", '"+ // resgim_id
          vec.get(1)+"', "+ // fecha
          vec.get(2) +", "+ // cod_asistencia
          vec.get(3)+", "+ // orden_llegada
          vec.get(4)+","+ // rut_usuario
          1; // @cod_origen_asistencia normal
    query = " exec spu_ins_asistencia_gimnasio  " + par;*/
// System.out.println("setAsistenciaGimnasio: "+ query);
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_ins_asistencia_gimnasio(?,?,?,?,?,?,?)}");
     	cstmt.setLong(1, Long.parseLong((String) vec.get(0)));
     	cstmt.setString(2, (String) vec.get(1));
     	cstmt.setInt(3, Integer.parseInt((String) vec.get(2)));
     	cstmt.setInt(4, Integer.parseInt((String) vec.get(3)));
     	cstmt.setInt(5, Integer.parseInt((String) vec.get(4)));
     	cstmt.setInt(6, 1);
     	cstmt.registerOutParameter(7, Types.INTEGER);//PARAMETRO DE SALIDA
     	cstmt.executeQuery();
     	error = cstmt.getInt(7); // ES EL VALOR QUE RETORNA
     	cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.setAsistenciaGimnasio: "+ e.getMessage());
      //con2.rollback(); 
    }
    return error;
  }
  //**************************************************************************
  // Metodo              : Permite insertar/actualiza    en asistencia_gimnasio
  //
  // Programadora        : Monica B.
  // Fecha creacion      : 31/07/2007
  //****************************************************************************
  synchronized public int setRegistraAsistenciaGimnasio(Vector vec) throws SQLException{
    String par = "";
    String query = "";
/*    par = vec.get(0)+", '"+ // resgim_id
          vec.get(1)+"', "+ // fecha
          vec.get(2) +", "+ // cod_asistencia
          vec.get(3)+", "+ // orden_llegada
          vec.get(4)+"";// rut_usuario
    query = " exec spu_tran_asistencia_gimnasio  " + par;*/
// System.out.println("setRegistraAsistenciaGimnasio: "+ query);
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_tran_asistencia_gimnasio(?,?,?,?,?,?)}");
     	cstmt.setLong(1, Long.parseLong((String) vec.get(0)));
     	cstmt.setString(2, (String) vec.get(1));
     	cstmt.setInt(3, Integer.parseInt((String) vec.get(2)));
     	cstmt.setInt(4, Integer.parseInt((String) vec.get(3)));
     	cstmt.setInt(5, Integer.parseInt((String) vec.get(4)));
      	cstmt.registerOutParameter(6, Types.INTEGER);//PARAMETRO DE SALIDA
     	cstmt.executeQuery();
     	error = cstmt.getInt(6); // ES EL VALOR QUE RETORNA
     	cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.setRegistraAsistenciaGimnasio: "+ e.getMessage());
      // con2.rollback(); 
    }
    return error;
  }

  //**************************************************************************
  // Metodo              : Permite insertar registro en asistencia_gimnasio
  //
  // Programadora        : Monica B.
  // Fecha creacion      : 27/07/2007
  //****************************************************************************
  synchronized public int setUpdAsistenciaGimnasio(Vector vec) throws SQLException{
    String par = "";
    String query = "";
/*    par = vec.get(0)+", '"+ // resgim_id
          vec.get(1)+"', "+ // fecha
          vec.get(2) +", "+ // cod_asistencia
          vec.get(3)+", "+ // orden_llegada
          vec.get(4)+"";// rut_usuario
    query = " exec spu_upd_asistencia_gimnasio  " + par;*/
//  System.out.println("setUpdAsistenciaGimnasio: "+ query);

    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_upd_asistencia_gimnasio(?,?,?,?,?,?)}");
     	cstmt.setLong(1, Long.parseLong((String) vec.get(0)));
     	cstmt.setString(2, (String) vec.get(1));
     	cstmt.setInt(3, Integer.parseInt((String) vec.get(2)));
     	cstmt.setInt(4, Integer.parseInt((String) vec.get(3)));
     	cstmt.setInt(5, Integer.parseInt((String) vec.get(4)));
     	cstmt.registerOutParameter(6, Types.INTEGER);//PARAMETRO DE SALIDA
     	cstmt.executeQuery();
     	error = cstmt.getInt(6); // ES EL VALOR QUE RETORNA
    }catch(SQLException e){
      System.out.println("Error en Reserva.setAsistenciaGimnasio: "+ e.getMessage());
      //con2.rollback(); 
    }
    return error;
  }

  //**************************************************************************
  // Metodo              : Permite insertar registro en asistencia_gimnasio
  //
  // Programadora        : Monica B.
  // Fecha creacion      : 27/07/2007
  //****************************************************************************
  synchronized public int setListaEspera(Vector vec) throws SQLException{
    String par = "";
    String query = "";
/*    par = vec.get(0)+", '"+ // resgim_id
          vec.get(1)+"', "+ //fecha
          vec.get(2)+","+// rut_usuario
          vec.get(3)+","+ // accion
          vec.get(4)+","+ // año
          vec.get(5)+","+ // semestre
          vec.get(6)+","+ // bloque1
          vec.get(7)+","+ // bloque2
          vec.get(8)+","+ // periodo
          vec.get(9)+", "+ // dia
          vec.get(10)+""; // sala
    query = " exec spu_tran_espera_gimnasio  " + par;*/
  //System.out.println("setListaEspera: "+ query);
     
    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_tran_espera_gimnasio(?,?,?,?,?,?,?,?,?,?,?,?)}");
     	cstmt.setLong(1, Long.parseLong((String) vec.get(0)));
     	cstmt.setString(2, (String) vec.get(1));
     	cstmt.setInt(3, Integer.parseInt((String) vec.get(2)));
     	cstmt.setInt(4, Integer.parseInt((String) vec.get(3)));
     	cstmt.setInt(5, Integer.parseInt((String) vec.get(4)));
     	cstmt.setInt(6, Integer.parseInt((String) vec.get(5)));
     	cstmt.setInt(7, Integer.parseInt((String) vec.get(6)));
     	cstmt.setInt(8, Integer.parseInt((String) vec.get(7)));
     	cstmt.setInt(9, Integer.parseInt((String) vec.get(8)));
     	cstmt.setInt(10, Integer.parseInt((String) vec.get(9)));
     	cstmt.setInt(11, Integer.parseInt((String) vec.get(10)));
     	cstmt.registerOutParameter(12, Types.INTEGER);//PARAMETRO DE SALIDA
     	cstmt.executeQuery();
     	error = cstmt.getInt(12); // ES EL VALOR QUE RETORNA
     	cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.setListaEspera: "+ e.getMessage());
      // con2.rollback(); 
    }
    return error;
  }

  //**************************************************************************
  // Metodo              : Permite eliminar registro en reserva gimnasio
  //
  // Programadora        : Monica B.
  // Fecha creacion      : 24/07/2007
  // Fecha modificacion  :
//****************************************************************************
  synchronized public int setEliminaReserva (String id_reg) throws SQLException{
    String query = "";
//    query = "exec spu_dlt_reserva_gimnasio "+id_reg ;
//System.out.println("setEliminaReserva: "+ query);

    Connection con2 = con.conexion();
    int error = -1;
    try{
    	CallableStatement cstmt = con2.prepareCall("{call spu_dlt_reserva_gimnasio (?,?)}");
	   	cstmt.setLong(1,Long.parseLong(id_reg));
	   	cstmt.registerOutParameter(2,Types.INTEGER);
	   	cstmt.executeQuery();
	   	error = cstmt.getInt(2);
	   	cstmt.close();
    }
    catch(SQLException e){
      System.out.println("Error en Reserva.setEliminaReserva: "+ e.getMessage());
      // con2.rollback(); 
    }
    return error;
  }
  //**************************************************************************
 // Metodo              : Permite insertar registros de premiados y sancionados
 //
 // Programadora        : Monica B.
 // Fecha creacion      : 24/09/2007
 //****************************************************************************
 synchronized public Vector setGeneraPremiadoSancionado(Vector vec){
   String par = "";
   String query = "";
   Vector vec_datos = new Vector();
   Vector vec_alumno = new Vector();
   Vector v = new Vector();
/*   par = vec.get(0)+", "+ // pergim_id
         vec.get(1)+"";// rut_usuario
   query = " exec spu_proc_premiados_gim  " + par;*/
//System.out.println("setGeneraPremiadoSancionado: "+ query);

   int error = -1;
   
   Connection con2 = con.conexion();
   try{
     //Statement sent = con.conexion().createStatement();
     //ResultSet res  = sent.executeQuery(query);
	 CallableStatement cstmt = con2.prepareCall("{call spu_proc_premiados_gim(?,?,?,?)}");
	 cstmt.setLong(1, Long.parseLong((String) vec.get(0)));
	 cstmt.setInt(2, Integer.parseInt((String) vec.get(1)));
	 cstmt.registerOutParameter(3, Types.INTEGER);
	 cstmt.registerOutParameter(4, OracleTypes.CURSOR);
	 cstmt.executeQuery();
     	
	 ResultSet res = (ResultSet) cstmt.getObject(4);
	 
     while (res.next() ){
       v = new Vector();
      if (res.getString(1).equals("0") || res.getString(1).equals("-1") ||
          res.getString(1).equals("-2") || res.getString(1).equals("-3")){
       v.addElement(res.getString(1));
      } else {
        gimnasio.BeanGeneral b = new gimnasio.BeanGeneral();
        b.getAbrirConexion();
        b.setGenerarConexionDatosAl();
        vec_datos = b.getDatosAlumno(res.getString(1));
        b.getCerrarConexion();
        if (vec_datos.size()>0){
         v.addElement(vec_datos.get(0));// rut
          v.addElement(vec_datos.get(1)); // nombre
          v.addElement(vec_datos.get(2)); // ap_paterno
          v.addElement(vec_datos.get(3)); // ap_materno
        }

      }
      if (v.size() > 0)
       vec_alumno.addElement(v);
     }
     res.close();
     cstmt.close();
   }
   catch(SQLException e){
     System.out.println("Error en Reserva.setGeneraPremiadoSancionado: "+ e);
   }
   return vec_alumno;
  }
  //**************************************************************************
// Metodo              : Permite eliminar registros de premiados y sancionados
//
// Programadora        : Monica B.
// Fecha creacion      : 24/09/2007
//****************************************************************************
synchronized public int setEliminaPremiadoSancionado(String pergim_id) throws SQLException{

 String query = "";
// query = " exec spu_dlt_premiados_gim  " + pergim_id;
//System.out.println("setEliminaPremiadoSancionado: "+ query);
 Connection con2 = con.conexion();
 int error = -1;
 try{
	 CallableStatement cstmt = con2.prepareCall("{call spu_dlt_premiados_gim (?,?)");
	 cstmt.setLong(1,Long.parseLong(pergim_id));
	 cstmt.registerOutParameter(2,Types.INTEGER);
	 cstmt.executeQuery();
	 error = cstmt.getInt(2);
	 cstmt.close();
 } catch(SQLException e){
   System.out.println("Error en Reserva.setEliminaPremiadoSancionado: "+ e.getMessage());
   // con2.rollback(); 
 }
 return error;
  }
  //**************************************************************************
// Metodo              : Permite eliminar el registro de premiados y sancionados
//
// Programadora        : Monica B.
// Fecha creacion      : 24/09/2007
//****************************************************************************
synchronized public int setEliminaRegistroPremiadoSancionado(String pergim_id,
                                                             String reggim_id) throws SQLException{

 String query = "";
// query = " exec spu_dlt_estado_asistencia_gim   " + pergim_id+", "+reggim_id;
//System.out.println("setEliminaRegistroPremiadoSancionado: "+ query);
  
 Connection con2 = con.conexion();
 int error = -1;
 try{
	 CallableStatement cstmt = con2.prepareCall("{call spu_dlt_estado_asistencia_gim (?,?,?)");
	 cstmt.setLong(1,Long.parseLong(pergim_id));
	 cstmt.setLong(2,Long.parseLong(reggim_id));
	 cstmt.registerOutParameter(3,Types.INTEGER);
	 cstmt.executeQuery();
	 error = cstmt.getInt(2);   
	 cstmt.close();
 }
 catch(SQLException e){
   System.out.println("Error en Reserva.setEliminaRegistroPremiadoSancionado: "+ e.getMessage());
   //con2.rollback(); 
 }
 return error;
  }

  //**************************************************************************
  // Metodo              : m�todo que formatea una fecha ingresada ddmmaa a
  //                       aammdd
  // Programadora        : Monica B.
  // Fecha creacion      : 21/6/2007
  // Fecha modificacion  :
  //****************************************************************************
  synchronized public String SetFechaFormato( String f1 )  {

    String f_1 = "";
    if (f1 != null && !f1.trim().equals("") && !f1.trim().equals("null")){
      java.util.StringTokenizer tokens = new java.util.StringTokenizer(f1,"/");
      String d1 = tokens.nextToken();
      if(tokens.hasMoreTokens()){
        String m1 = tokens.nextToken();
        String a1 = tokens.nextToken();
        f_1= a1+m1+d1;
      } //else f_1 = "TO_CHAR('" +f1+"','dd/mm/yyyy')";
    }
    return f_1;
  }
}
