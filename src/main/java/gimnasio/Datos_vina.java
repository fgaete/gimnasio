package gimnasio;
import java.io.*;
import java.util.*;
import java.sql.*;

public class Datos_vina {
  private Conexion_gim con;

public Datos_vina () {
}

public Datos_vina (Conexion_gim con) {
  this.con = con;
  }

//**************************************************************************
//M�todo              : Permite capturar el año del periodo semestre actual
//Programadora        : M�nica B.
//Fecha creacion      : 4/01/2012
//Fecha modificacion  :
//****************************************************************************

public int getañoPeriodoActual(){
 int añoActual = 0;

/* String query = " SELECT per_año FROM periodo_semestre" +
	 " WHERE sed_cod_sede = 3" +
	 " AND cod_jornada = 1" +
	 " AND per_vigencia = 1";
MB 14/03/2012 debido a que en estos momentos se est� en preinscripcion se tiene que cambiar a per_vigencia_mant,
ya que el per_vigencia apunta al periodo de inscripciones y per_vigencia_mant apunta al periodo real actual
esto lo indic� Ximena celis
*
*/
String query = " SELECT per_año FROM periodo_semestre" +
			   " WHERE sed_cod_sede = 3" +
			   " AND cod_jornada = 1" +
			   " AND per_vigencia_mant = 1";

 try {
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  while (res.next()) {
    añoActual =res.getInt(1);
  }
  res.close();
  sent.close();
}

catch (SQLException e ) {
  System.out.println("Error en Datos_alumno.getañoPeriodoActual : " + e );
}
return añoActual;
}
 //**************************************************************************
// M�todo              : Permite capturar el año actual 
// Programadora        : M�nica B.
// Fecha creacion      : 19/11/2010
// Fecha modificacion  :
//****************************************************************************

    public int getañoActual(){
      int añoActual = 0;
     String query = " SELECT TO_CHAR(SYSDATE,'yyyymmdd')";
      try {
       Statement sent = con.conexion().createStatement();
       ResultSet res = sent.executeQuery(query);
       while (res.next()) {
         añoActual =Integer.parseInt(res.getString(1).substring(0,4));
       }
       res.close();
       sent.close();
     }

     catch (SQLException e ) {
       System.out.println("Error en Datos_alumno.getañoActual : " + e );
     }
    return añoActual;
}
  //**************************************************************************
// M�todo              : Permite capturar los datos del recepcionista (alumno) del
//                       gimnasio
// Programadora        : M�nica B.
// Fecha creacion      : 5/6/2007
// Fecha modificacion  :
//****************************************************************************
  public Vector getAlumnoViña (int rut_recep){
    java.util.Vector v = new java.util.Vector();
    String query = " SELECT a.alu_rut, a.alu_nombres || ' ' || a.alu_ap_paterno || ' ' || NVL(a.alu_ap_materno,' ')," +
			       " a.alu_fono_personal ,  a.alu_dir_personal || ', ' || nom_comuna, a.alu_email_personal," +
			       " p.nom_nacionalidad, TO_CHAR(a.alu_rut) ||'-' || a.alu_dig_rut" +
			       " , a.alu_nombres, a.alu_ap_paterno, NVL(a.alu_ap_materno,' ')," +
			       " a.alu_sexo, a.alu_dig_rut" +
			       " FROM alumno a, comuna c, pais p" +
			       " WHERE a.alu_rut = " + rut_recep +
			       " AND a.cod_comuna_personal = c.cod_comuna" +
			       " AND a.cod_pais_nacionalidad = p.cod_pais";
    
    Vector vec_carrera = new Vector();
    Vector vec_matricula = new Vector();
    Vector vec_rol = new Vector();
    String carrera = "";
    String calidad = "";
    String ingreso = "";
    boolean habilitado = false;
    int cod_calidad = 0;
    int año = 0;
    int semestre = 0;
    //  int indice = 0;
    int cod_carrera = 0;
    int cod_mencion = 0;
    int cod_sede = 0;
    int cod_sede_alumno = 0;
  int añoActual = getañoPeriodoActual();
  
  //System.out.print(añoActual);   
  
   try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    while(res.next()){
      vec_carrera = getCarreraAlumnoViña(rut_recep);
      if (vec_carrera.size()>0) {
        for(int indice = 0;indice < vec_carrera.size();indice++){
          año = 0;
          semestre = 0;
          /*ahora se muestran todas las carreras con sus calidades para ver si est� habilitado para alguna carrera,
           solicitado el 20/10/2008*/
          //  indice = vec_carrera.size() - 1;

          carrera += ( (java.util.Vector) vec_carrera.get(indice)).get(3) + "";
          if ( ( ( (java.util.Vector) vec_carrera.get(indice)).get(7) + "").
              trim().toUpperCase().equals("SIN MENCI�N"))
            carrera += ", " +
                ( (java.util.Vector) vec_carrera.get(indice)).get(1);
          else
            carrera += ", Mencion " +
                ( (java.util.Vector) vec_carrera.get(indice)).get(7) + ", " +
                ( (java.util.Vector) vec_carrera.get(indice)).get(1);
          cod_carrera = Integer.parseInt( ( (java.util.Vector) vec_carrera.get(
              indice)).get(4) + "");
          cod_mencion = Integer.parseInt( ( (java.util.Vector) vec_carrera.get(
              indice)).get(6) + "");
          cod_sede = Integer.parseInt( ( (java.util.Vector) vec_carrera.get(
              indice)).get(2) + "");
          cod_sede_alumno = Integer.parseInt( ( (java.util.Vector) vec_carrera.
                                               get(indice)).get(0) + "");

          vec_matricula = getUltimaMatriculaViña(rut_recep, cod_carrera,
                                                 cod_mencion,
                                                 cod_sede, cod_sede_alumno);

          // calidad del alumno
          getCalidadAlumnoViña(rut_recep, cod_carrera, cod_mencion, cod_sede,
                               cod_sede_alumno);
          calidad = getCalidadAlumnoNombreViña();
          cod_calidad = getCalidadAlumnoCodigoViña();
          if(!habilitado)
            habilitado = getHabilitadoViña(cod_calidad);
          if (habilitado && vec_matricula.size() > 0) {
            año = Integer.parseInt(vec_matricula.get(1) + "");
            semestre = Integer.parseInt(vec_matricula.get(0) + "");

          }
          if(año>0 && semestre > 0)
          calidad += ", �ltima matr�cula " + año+"-"+semestre ;
          carrera += ", " + calidad+"<br>";
        }
         }
      boolean condicion = (añoActual == año)?true:false ;
      // esta condicion la agregu� por el caso especial del semestre 2-2011, pues est� vigente junto con 2012-1
      if(añoActual == 2011)
    	  condicion = (((añoActual + 1) == año) || (añoActual == año))?true:false ;
      if(condicion && cod_sede_alumno == 3 ){
         vec_rol = getAlumnoRolViña(rut_recep,3);
         ingreso = getIngresoAlumnoViña(rut_recep);
         v.addElement(res.getString(1)); // 0 - rut alumno
         v.addElement(res.getString(2)); // 1 - nombre completo
         v.addElement(res.getString(3)); // 2 - fono personal
         v.addElement(res.getString(4)); // 3 - direccion , comuna personal
         String mail = "";
         gimnasio.BeanUsuarios b = new gimnasio.BeanUsuarios();
         b.getAbrirConexion();
         b.setGenerarConexionUsuario();
         mail = b.getMailUsuario(rut_recep);
         b.getCerrarConexion();
         if( mail.trim().equals(""))
             v.addElement(res.getString(5)); // 4 - email personal
         else
             v.addElement(mail); // 4 - email personal
         v.addElement(carrera);          // 5 - �ltima carrera vigente
         v.addElement(calidad);          // 6 - �ltima calidad vigente
         if(año > 0 && semestre > 0)
         v.addElement(año+"-"+semestre); // 7 - año semestre �ltima matr�cula
         else
           v.addElement("0");
         if (habilitado)
           v.addElement("1"); // 8 - habilitado al sistema
          else
           v.addElement("2"); // 8 - no habilitado al sistema

         if (vec_rol.size()> 0){
           v.addElement(vec_rol.get(0)+"-"+vec_rol.get(1)); // 9 - rol
         } else v.addElement("");//9 - rol
         v.addElement(ingreso); // 10 - ingreso a USM
         v.addElement(res.getString(6)); // 11- nacionalidad
         v.addElement(res.getString(7)); // 12 - rut completo
         v.addElement(res.getString(8)); // 13 - nombres
         v.addElement(res.getString(9)); // 14 - paterno
         v.addElement(res.getString(10)); // 15 - materno
         if (res.getInt(11) == 1)
           v.addElement("M"); // 16 - sexo
         else
           v.addElement("F"); // 16 - sexo
         v.addElement(res.getString(12)); // 17 digito
         v.addElement("3"); // 18 sede
         v.addElement(res.getString(11)); // 19 codigo sexo
         }
       }
       res.close();
       sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Datos_vi�a.getAlumnoViña: "+e);
  }
  return v;
  }
  //***********************************************************************
// M�todo      : obtiene la �ltima calidad de un alumno en esa carrera
// Programador : MM
// Fecha       : 03/05/2004
// Modificado  : MA 17/05/2006
//***********************************************************************

String nom_calidad = "";
int cod_calidad    = 0;
String fecha_calidad = "";

public String getCalidadAlumnoNombreViña(){
return nom_calidad;
}
public int getCalidadAlumnoCodigoViña(){
return cod_calidad;
}
public String getCalidadAlumnoFechaViña(){
return fecha_calidad;
}

public void getCalidadAlumnoViña
(int rut_alumno, int cod_carrera, int cod_mencion,
int cod_sede, int cod_sede_alumno){

nom_calidad   = "";
cod_calidad   = 0;
fecha_calidad = "";
String query =       " SELECT ca.cod_calidad, c.nom_calidad, TO_CHAR(ca.cal_fecha,'dd/mm/yyyy')"+
                     " FROM calidad_alumno ca , calidad c"+
                     " WHERE alu_rut = " + rut_alumno +
                     " AND cod_carrera = " + cod_carrera +
                  // " AND cod_mencion = " + cod_mencion +
                     " AND sed_cod_sede = " + cod_sede +
                     " AND cod_sede_alumno = " + cod_sede_alumno +
                     " AND ca.cod_calidad = c.cod_calidad" +
                     " AND cal_fecha = (SELECT MAX(cal_fecha)" +
                     "                  FROM calidad_alumno" +
                     "                  WHERE alu_rut = "+ rut_alumno +
                     "                  AND cod_carrera = " + cod_carrera +
                  // "                  AND cod_mencion = " + cod_mencion +
                     "                  AND sed_cod_sede = " + cod_sede +
                     "                  AND cod_sede_alumno = "+ cod_sede_alumno +
                     "                  AND TO_CHAR(cal_fecha,'yyyymmdd') <= TO_CHAR(SYSDATE,'yyyymmdd'))";
try {
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  while (res.next()) {
    cod_calidad = res.getInt(1);
    nom_calidad = res.getString(2);
    fecha_calidad = res.getString(3);
  }
  res.close();
  sent.close();
}

catch (SQLException e ) {
  System.out.println("Error en Datos_alumno.getCalidadAlumno : " + e );
}
  }
  //***************************************************************************
 // M�todo      : obtiene las carreras que tiene vigente el alumno y todos los
 //               datos asociados a la carrera.
 // Programador : HC
 // Fecha       : 06/05/2004
 // Modificado  : 31/08/2004 (MM se agrega cod_clasificacion)
 //               21/05/2005 (MV se agrega car_fecha_ingreso dd/mm/año)
 //               28/02/2008 (MB se copi� desde insc_datos y adecu� a la necesidad)
 //***************************************************************************
 public Vector getCarreraAlumnoViña (
      int rut_alumno){

    String query = " SELECT ca.car_paralelo_carrera,"+
                   " TO_CHAR(ca.car_fecha_ingreso,'yyyy') AS año_ingreso,"+
                   " s.sed_cod_sede, s.sed_nom_sede, cs.sed_cod_sede," +
                   " cs.abreviacion, cs.cod_carrera, cs.nom_carrera,"+
                   " cm.cod_mencion, cm.nom_mencion, ca.pla_cod_plan,"+
                   " cs.dep_cod_departamento, cm.cod_jornada," +
                   " cs.cod_clasificacion,"+
                   " TO_CHAR(ca.car_fecha_ingreso,'yyyymmdd'), "+
                   " ca.car_vigente" +
                   " FROM carrera_alumno ca, carrera_sede cs, "+
                   " carrera_mencion cm, "+
                   " sede s"+
                   " WHERE ca.alu_rut = " + rut_alumno +
                   " AND ca.cod_sede_alumno = s.sed_cod_sede "+
                   " AND ca.car_vigente = 1 " +
                   " AND ca.cod_sede_carrera = cs.sed_cod_sede "+
                   " AND ca.cod_carrera = cs.cod_carrera "+
                   " AND ca.cod_mencion = cm.cod_mencion "+
                   " AND ca.cod_sede_carrera = cm.sed_cod_sede "+
                   " AND cm.cod_carrera = ca.cod_carrera" +
                   " ORDER BY ca.car_fecha_ingreso ";

    Vector vec_datacad = new Vector();
    Vector detalle = null;

    try {
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      int carr_ant = 0;
      int carr_act = 0;
      while (res.next()) {
        carr_act = Integer.parseInt(res.getString(7)+res.getString(3));
        if (carr_act != carr_ant) {
          detalle = new Vector();
          detalle.addElement(res.getString(3)); //  0  - cod_sede_alumno
          detalle.addElement(res.getString(4)); //  1  - nom_sede_alumno
          detalle.addElement(res.getString(5)); //  2  - cod_sede_carrera
          detalle.addElement(res.getString(6)); //  3  - abreviacion
          detalle.addElement(res.getString(7)); //  4  - cod_carrera
          detalle.addElement(res.getString(8)); //  5  - nom_carrera
          detalle.addElement(res.getString(9)); //  6  - cod_mencion
          detalle.addElement(res.getString(10));//  7  - nom_mencion
           vec_datacad.addElement(detalle);
        }

        carr_ant = Integer.parseInt(res.getString(7)+res.getString(3));
      }
      res.close();
      sent.close();

    }
    catch (SQLException e ) {
      System.out.println("Error en Datos_vina.getCarreraAlumnoViña : " + e );
    }
    return vec_datacad;
  }

  //**************************************************************************
// M�todo              : Permite capturar todas las calidades y carreras
//                       del alumno
// Programadora        : adaptado desde matricula M�nica B.
// Fecha creacion      : 6/6/2007
// Fecha modificacion  :
//****************************************************************************
 public java.util.Vector getCalidadesAluViña(int rut) {

   java.util.Vector v = new java.util.Vector();
   java.util.Vector a = null;
   String query       = "";

   query =
      " SELECT distinct "+
      " ca.alu_rut "+             // 0
      " ,ca.cal_fecha "+          // 1
      " ,ca.sed_cod_sede "+       // 2
      " ,s2.sed_nom_sede "+       // 3
      " ,ca.cod_carrera "+        // 4
      " ,cs.abreviacion "+        // 5
      " ,ca.cod_mencion "+        // 6
      " ,cm.nom_mencion "+        // 7
      " ,ca.cod_calidad "+        // 8
      " ,c.nom_calidad "+         // 9
      " ,ca.cod_sede_alumno "+    // 10
      " ,s.sed_nom_sede "+     // 11
      " FROM calidad_alumno ca,carrera_sede cs, "+
      " carrera_mencion cm, sede s, calidad c, sede s2 "+
      " WHERE ca.alu_rut = "+rut+
      " AND ca.sed_cod_sede = cs.sed_cod_sede "+
      " AND ca.cod_carrera = cs.cod_carrera "+
      " AND ca.sed_cod_sede = cm.sed_cod_sede "+
      " AND ca.cod_carrera = cm.cod_carrera "+
      " AND ca.cod_mencion = cm.cod_mencion "+
      " AND ca.cod_sede_alumno = s.sed_cod_sede "+
      " AND ca.cod_calidad = c.cod_calidad "+
      " AND ca.sed_cod_sede = s2.sed_cod_sede "+
      " AND TO_CHAR(ca.cal_fecha,'yyyymmdd') <= TO_CHAR(SYSDATE + 1,'yyyymmdd')" +
      " ORDER BY ca.cal_fecha ";

//System.out.println("query calidad "+query);
   try{
     Statement sent =  con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);

     while (res.next()) {
      a = new java.util.Vector();
       a.addElement(res.getString(1)); // 0-rut alumno
       a.addElement(res.getString(2)); // 1-fecha
       a.addElement(res.getString(3)); // 2-sede de la carrera
       a.addElement(res.getString(4)); // 3-abreviacion de la sede
       a.addElement(res.getString(5)); // 4-carrera
       a.addElement(res.getString(6)); // 5-nombre carrera
       a.addElement(res.getString(7)); // 6-mencion
       a.addElement(res.getString(8)); // 7-nombre mencion
       a.addElement(res.getString(9)); // 8-calidad
       a.addElement(res.getString(10)); //9-nombre calidad
       a.addElement(res.getString(11)); //10-sede alumno
       a.addElement(res.getString(12)); //11-abreviacion de la sede del alumno

       v.addElement(a);
     }
     res.close();
     sent.close();
   }
   catch (SQLException e){
   System.out.println("Error Datos_vi�a.getCalidadesAluViña : " + e );}

   return v;
 }
 //**************************************************************************
// M�todo              : Permite habilitar a un alumno ingresar al sistema de
//                       gimnasio
// Programadora        : M�nica B.
// Fecha creacion      : 7/6/2007
// Fecha modificacion  : 24/11/2009 se habilitan los titulados
//****************************************************************************
  public boolean getHabilitadoViña (int cod_calidad){
    boolean habilitado = false;
    // calidades que habilitan son 1,2,3,4,5,6,,15,16,17,25,26,29
   if (cod_calidad == 1 || cod_calidad == 2 ||
       cod_calidad == 3 || cod_calidad == 4 ||
       cod_calidad == 5 || cod_calidad == 6 ||
       cod_calidad == 15 || cod_calidad == 16 ||
       cod_calidad == 17 || cod_calidad == 25 ||
       cod_calidad == 26 || cod_calidad == 29 ||
       cod_calidad == 7) habilitado = true;
  return habilitado;
  }
  //**************************************************************************
// M�todo              : Permite habilitar a un alumno para reservar
//                       gimnasio
// Programadora        : M�nica B.
// Fecha creacion      : 27/7/2007
// Fecha modificacion  :
//****************************************************************************
  public boolean getHabilitadoReservaViña (int rut, int año, int semestre){
    boolean habilitado = false;
    String query = " SELECT m.alu_rut" +
                   " FROM matricula m" +
                   " WHERE m.alu_rut = " + rut +
                   " AND m.mat_año = " + año +
                   " AND m.mat_semestre = " + semestre +
                   " AND (m.mat_estado_matricula = 0 OR" +
                   " m.mat_estado_matricula = 4)" +
                   " UNION" +
                   " SELECT alu_rut" +
                   " FROM inscripcion_asignatura i " +
                   " WHERE i.pro_año = " + año +
                   " AND i.pro_semestre = " + semestre +
                   " AND i.alu_rut = " + rut +
                   " AND i.ina_situacion = 1";
    try{
     Statement sent = con.conexion().createStatement();
     ResultSet res = sent.executeQuery(query);
     if (res.next()){
       habilitado = true; // habilitado
     }
     Vector vec_carrera = getCarreraAlumnoViña(rut);
     if (vec_carrera.size()>0) {
       for(int indice=0;indice < vec_carrera.size();indice++){
         int cod_carrera = Integer.parseInt( ( (java.util.Vector) vec_carrera.
                                              get(indice)).get(4) + "");
         int cod_mencion = Integer.parseInt( ( (java.util.Vector) vec_carrera.
                                              get(indice)).get(6) + "");
         int cod_sede = Integer.parseInt( ( (java.util.Vector) vec_carrera.get(
             indice)).get(2) + "");
         int cod_sede_alumno = Integer.parseInt( ( (java.util.Vector)
                                                  vec_carrera.get(indice)).get(
             0) + "");

         // calidad del alumno
         getCalidadAlumnoViña(rut, cod_carrera, cod_mencion, cod_sede,
                              cod_sede_alumno);
         cod_calidad = getCalidadAlumnoCodigoViña();
         if(!habilitado)
             habilitado = getHabilitadoViña(cod_calidad);
       }
    }

     res.close();
     sent.close();
   }
   catch(SQLException e){
     System.out.println("Error en Datos_alumno.getHabilitadoReserva: "+e);
   }
   return habilitado;
  }

  //**************************************************************************
// M�todo              : Permite capturar año y semestre de �ltima matr�cula
//                       del alumno
// Programadora        : adaptado desde insc_datos M�nica B.
// Fecha creacion      : 6/6/2007
// Fecha modificacion  :
//****************************************************************************

  public Vector getUltimaMatriculaViña
  (int rut_alumno, int cod_carrera, int cod_mencion,
  int cod_sede, int cod_sede_alumno){

    String query = " SELECT mat_semestre, mat_año "+
                   " FROM matricula" +
                   " WHERE alu_rut = "+ rut_alumno +
                   " AND cod_carrera = "+ cod_carrera +
                   " AND cod_mencion = "+ cod_mencion +
                   " AND cod_sede = "+ cod_sede +
                   " AND cod_sede_alumno = " + cod_sede_alumno +
                   " AND mat_estado_matricula IN (0,4,10)" +
                   " AND mat_fecha = (SELECT MAX(mat_fecha)"+
                   "                  FROM matricula" +
                   "                  WHERE alu_rut = " + rut_alumno +
                   "                  AND cod_carrera= " + cod_carrera +
                   "                  AND cod_mencion= " + cod_mencion +
                   "                  AND cod_sede=" + cod_sede +
                   "                  AND cod_sede_alumno=" + cod_sede_alumno +
                   "                  AND mat_estado_matricula IN (0,4,10))";
    Vector vec_mat = new Vector();
  //  System.out.println("getUltimaMatriculaViña: "+ query);
    try {
      Statement sent = con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      if (res.next()) {
        vec_mat.addElement(res.getString(1));
        vec_mat.addElement(res.getString(2));
      }
      res.close();
      sent.close();
    }

    catch (SQLException e ) {
      System.out.println("Error en Datos_vi�a.getUltimaMatriculaViña : " + e );
    }
    return vec_mat;
  }
  //**************************************************************************
// M�todo              : Permite capturar el �ltimo rol del alumno
//
// Programadora        : adaptacion de matricula M�nica B.
// Fecha creacion      : 13/6/2007
// Fecha modificacion  :
//****************************************************************************
  public java.util.Vector getAlumnoRolViña(int rut, int sede)
  {
    String query = "";
    java.util.Vector v = new java.util.Vector();
    String qsede1 = "";
    String qsede2 = "";

    if (sede == 1 || sede == 2 || sede == 5 || sede == 6) {
      qsede1 = "(ar1.cod_sede_alumno = 1 OR" +
               " ar1.cod_sede_alumno = 2 OR" +
               " ar1.cod_sede_alumno = 5 OR" +
               " ar1.cod_sede_alumno = 6)";
      qsede2 = "(ar2.cod_sede_alumno = 1 OR" +
               " ar2.cod_sede_alumno = 2 OR" +
               " ar2.cod_sede_alumno = 5 OR" +
               " ar2.cod_sede_alumno = 6)";
    }
    else {
      qsede1 = "ar1.cod_sede_alumno = " + sede;
      qsede2 = "ar2.cod_sede_alumno = " + sede;
    }

    query = "SELECT ar1.rol, ar1.dig_rol FROM alumno_rol ar1" +
            " WHERE ar1.alu_rut = " + rut +
            " AND " + qsede1 +
            " AND ar1.fecha_rol = ( SELECT MAX(ar2.fecha_rol)" +
            "                       FROM   alumno_rol ar2" +
            "                       WHERE  ar2.alu_rut = " + rut +
            "                       AND   " + qsede2 +
            ")";
//System.out.println("query: "+query);
    try{
      Statement sent =  con.conexion().createStatement();
      ResultSet res = sent.executeQuery(query);
      // busca el �ltimo rol para la sede consultada
      if (res.next()) {
        v.addElement(res.getString(1));
        v.addElement(res.getString(2));
      }
      res.close();
      sent.close();
    }
    catch (SQLException e){
    System.out.println("Error Datos_vi�a.getAlumnoRolViña : " + e );}
    return v;
  }
//**************************************************************************
// M�todo              : Permite capturar los alumnos premiados del periodo
// Programadora        : M�nica B.
// Fecha creacion      : 26/6/2007
// Fecha modificacion  :
//****************************************************************************
//public Vector getPremiadosPeriodoListViña (int año, int semestre, String periodo, int cod_estado){
//java.util.Vector v = new java.util.Vector();
//java.util.Vector a = null;
//String query = " SELECT a.alu_ap_paterno, NVL(a.alu_ap_materno,' ')," +
//                  " a.alu_nombres, TO_CHAR(a.alu_rut) || '-' || a.alu_dig_rut," +
//                  " a.alu_email_personal," +
//                  " TO_CHAR(pg.pergim_fecha_inicio_vig,'dd/mm/yyyy')," +
//                  " TO_CHAR(pg.pergim_fecha_termino_vig,'dd/mm/yyyy')," +
//                  " eag.reggim_id" +
//                  " FROM periodo_gimnasio pg,  estado_asistencia_gimnasio eag," +
//                  " registro_gimnasio regg,  registro_gimnasio_externo rge, alumno a" +
//                  " WHERE pg.pergim_año = " + año + " AND" +
//                  " pg.pergim_semestre = " + semestre + " AND" +
//                  " pg.pergim_id = " + periodo + " AND " +
//                  " eag.pergim_id = pg.pergim_id AND" +
//                  " eag.cod_asistencia = " + cod_estado + " AND" +
//                  " eag.reggim_id = regg.reggim_id AND" +
//                  " regg.reggim_id = rge.reggim_id AND" +
//                  " regg.cod_registro_gimnasio = 5 AND" +
//                  " rge.regext_rut = a.alu_rut" +
//                  " ORDER BY a.alu_ap_paterno, a.alu_ap_materno, a.alu_nombres" ;
//
//System.out.println("getPremiadosPeriodoList  vi�a: "+ query);
//try{
//Statement sent = con.conexion().createStatement();
//ResultSet res = sent.executeQuery(query);
//while(res.next()){
//  a = new java.util.Vector();
//  a.addElement(res.getString(1)); // 0 paterno
//  a.addElement(res.getString(2)); // 1 materno
//  a.addElement(res.getString(3)); // 2 nombres
//  a.addElement(res.getString(4)); // 3 rut
//  a.addElement(res.getString(5)); // 4 email
//  a.addElement(res.getString(6)); // 5 fecha Incio vigencia
//  a.addElement(res.getString(7)); // 6 fecha termino vigencia
//  a.addElement(res.getString(8)); // 7 reggim_id
//  v.addElement(a);
//}
//res.close();
//sent.close();
//}
//catch(SQLException e){
//System.out.println("Error en Datos_vi�a.getPremiadosPeriodoListViña : "+e);
//}
//return v;
//}

//**************************************************************************
// M�todo              : Permite capturar la nombre del usuario del sistema de
//                       gimnasio
// Programadora        : M�nica B.
// Fecha creacion      : 2/6/2007
// Fecha modificacion  :
//****************************************************************************
  public Vector getFuncionarioViña (int rut_usuario){
  java.util.Vector v = new java.util.Vector();
  String depto = "";
  /*
  String query = " SELECT f.fun_nombres, f.fun_ap_paterno, NVL(f.fun_ap_materno,' '), "+
                 " f.fun_rut, f.sed_cod_sede , s.sed_nom_sede, f.fun_anexo," +
                 " f.fun_email_personal, f.fun_tipo_funcionario, f.fun_planta, f.cod_tipo_contrato," +
                 " t.nom_tipo_contrato, case f.fun_planta when 1 then 'Si' " +
                 " ELSE 'No' end , TO_CHAR(f.fun_rut) ||'-'||f.fun_dig_rut" +
                 " , f.fun_sexo , f.fun_dig_rut" +
                 " FROM funcionario f ,  sede s, tipo_contrato t"+
                 " WHERE f.fun_rut  = "+rut_usuario +
                 " AND f.fun_vigente = 1" +
                 " AND f.sed_cod_sede = s.sed_cod_sede" +
                 " AND f.cod_tipo_contrato = t.cod_tipo_contrato";*/
  /* este query debe ir hasta que se cambie la estructura de funcionario*/
  String query  = "SELECT f.fun_rut, f.fun_dig_rut, f.fun_ap_paterno, f.fun_ap_materno, f.fun_nombres," +
                  " f.fun_sexo,to_char(fun_fecha_nacimiento,'dd/mm/yyyy'), fun_direccion," +
                  " fun_fono, to_char(fun_fecha_ingreso,'dd/mm/yyyy'),  fun_estado_civil, fun_vigente," +
                  " ' ',  case f.fun_planta when 1 then 'Si'" +
                  " else 'No' end, d.dep_nom_departamento, 0,  s.sed_cod_sede, t.nom_tipo_contrato , ' '," +
                  " 0, s.sed_nom_sede" +
                  " FROM funcionario f ,  sede s, tipo_contrato t, departamento d" +
                  " WHERE f.fun_rut  =" + rut_usuario +
                  " AND f.fun_vigente = 1" +
                  " AND f.sed_cod_sede = s.sed_cod_sede" +
                  " AND f.cod_tipo_contrato = t.cod_tipo_contrato" +
                  " AND f.dep_cod_departamento = d.dep_cod_departamento" ;



/*
  String query =  "  SELECT f.fun_rut, f.fun_dig_rut, fun_ap_paterno, fun_ap_materno, fun_nombres," +
 "  fun_sexo, TO_CHAR(fun_fecha_nacimiento,'dd/mm/yyyy'), fun_direccion," +
 "  fun_fono,  TO_CHAR(fun_fecha_ingreso,'dd/mm/yyyy')," +
 "  fun_estado_civil, fun_vigente,CASE tjf.cod_jerarquia_funcionario when 0 then ' ' else tjf.nom_jerarquia_funcionario end ," +
 "  tcgf.nom_contrato_funcionario, dep_nom_departamento, cod_jornada_funcionario,"+
 "  s.sed_cod_sede, tpf.nom_planta_funcionario, tcf.nom_cargo_funcionario," +
 "  f.cod_contrato_funcionario, s.sed_nom_sede" +
 "  FROM funcionario f LEFT JOIN tipo_contrato_funcionario tcgf ON" +
 "  f.cod_contrato_funcionario = tcgf.cod_contrato_funcionario" +
 "  LEFT JOIN tipo_jerarquia_funcionario tjf ON" +
 "  f.cod_jerarquia_funcionario = tjf.cod_jerarquia_funcionario," +
 "  funcionario_cargo fc, tipo_cargo_funcionario tcf,tipo_planta_funcionario tpf," +
 "  departamento d, sede s" +
 "  WHERE f.fun_rut = " + rut_usuario +
 "  AND f.fun_rut = fc.fun_rut" +
 "  AND fc.dep_cod_departamento = d.dep_cod_departamento" +
 "  AND fc.sed_cod_sede = s.sed_cod_sede " +
 "  AND fc.cod_vigencia = 1" +
 "  AND fc.funcar_principal = 1 "   +
 "  AND fc.cod_planta_funcionario = tcf.cod_planta_funcionario" +
 "  AND fc.cod_cargo_funcionario = tcf.cod_cargo_funcionario" +
"  AND fc.cod_planta_funcionario = tpf.cod_planta_funcionario";*/

//System.out.println("q"+ query);
 try{
   Statement sent = con.conexion().createStatement();
   ResultSet res  = sent.executeQuery(query);

   if(res.next()){
       v.addElement(res.getString(1)); // 0.  fun_rut
       v.addElement(res.getString(2)); // 1.  fun_dig_rut
       v.addElement(res.getString(3)); // 2.  fun_ap_paterno
       v.addElement(res.getString(4)); // 3.  fun_ap_materno
       v.addElement(res.getString(5)); // 4.  fun_nombres
       v.addElement(res.getString(6)); // 5.  fun_sexo
       v.addElement(res.getString(7)); // 6.  fun_fecha_nacimiento
       v.addElement(res.getString(8)); // 7.  fun_direccion
       v.addElement(res.getString(9)); // 8.  fun_fono
       v.addElement(res.getString(10)); // 9.  fun_fecha_ingreso
       v.addElement(res.getString(11)); // 10. fun_estado_civil
       v.addElement(res.getString(12)); // 11. fun_vigente
       v.addElement(res.getString(13)); // 12. nom_jerarquia_funcionario
       v.addElement(res.getString(14)); // 13. nom_contrato_funcionario
       v.addElement(res.getString(15)); // 14. nom_departamento
       v.addElement(res.getString(16)); // 15. cod_jornada_funcionario
       v.addElement(res.getString(17)); // 16. cod_sede
       v.addElement(res.getString(18)); // 17. nom_planta_funcionario
       v.addElement(res.getString(19)); // 18. nom_cargo_funcionario
       v.addElement(res.getString(20)); // 19. cod_contrato
       v.addElement(res.getString(21)); // 20. nom_sede
    }
   res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Datos_vi�a.getFuncionarioViña: "+e);
  }
  return v;
  }

  //**************************************************************************
// M�todo              : Permite el departamento del funcionario
//
// Programadora        : M�nica B.
// Fecha creacion      : 23/07/2007
// Fecha modificacion  :
//****************************************************************************
/* no se usa con nueva estructura de funcionario MB 16/11/2010

  public String getDepartamentoCargo (int rut){
     String depto = "";
     String query  = "";
     String query1 = "";
     String query2 = "";
     String query3 = "";
     Vector v = new Vector();
     Vector a = new Vector();

     query1 = " SELECT ds.dep_cod_departamento, " +
              " d.dep_nom_departamento, ds.sed_cod_sede, '9', 0" +
              " FROM departamento_sede ds, departamento d" +
              " WHERE ds.fun_rut = " + rut +
              " AND ds.dep_cod_departamento = d.dep_cod_departamento";

     query2 = " SELECT DISTINCT cs.dep_cod_departamento, " +
              " d.dep_nom_departamento, cs.sed_cod_sede, '10', 0" +
              " FROM carrera_sede cs, departamento_sede ds, departamento d" +
              " WHERE cs.fun_rut = " + rut +
              " AND cs.dep_cod_departamento = ds.dep_cod_departamento" +
              " AND ds.dep_cod_departamento = d.dep_cod_departamento";

     query3 = " SELECT c.dep_cod_departamento, " +
              " d.dep_nom_departamento, c.sed_cod_sede, '0', c.cod_cargo " +
              " FROM cargo_funcionario c, departamento_sede ds, departamento d" +
              " WHERE c.fun_rut = " + rut +
              " AND c.dep_cod_departamento = ds.dep_cod_departamento" +
              " AND ds.dep_cod_departamento = d.dep_cod_departamento" +
              " AND c.fecha_termino IS NULL";

     query = query1 + " UNION " + query2 + " UNION " + query3;

     try{
       Statement sent = con.conexion().createStatement();
       ResultSet res  = sent.executeQuery(query);
       while (res.next()) {
         depto = res.getString(2); // dep_nom_departamento

       }
       res.close();
       sent.close();
     }
  catch(SQLException e){
  System.out.println("Error en Usuario.getDepartamentoCargo: "+e);
}
  return depto;

  }
*/
  //**************************************************************************
// M�todo              : Permite capturar el mail de usuarios tanto alumnos como
//                       funcionarios
// Programadora        : M�nica B.
// Fecha creacion      : 20/6/2007
// Fecha modificacion  :
//****************************************************************************


public String getMailUsuarioViña (int rut){
  String mail = "";
  String query = " SELECT usucas_casilla || '@' || tc.nom_casilla" +
                 " FROM usuario_casilla uc, tipo_casilla tc" +
                 " WHERE uc.rut_usuario = " + rut +
                 " AND uc.cod_casilla = tc.cod_casilla";
  try{
    Statement sent = con.conexion().createStatement();
    ResultSet res = sent.executeQuery(query);
    if(res.next()){
      mail = res.getString(1);
    }
    res.close();
    sent.close();
  }
  catch(SQLException e){
    System.out.println("Error en Datos_vi�a.getMailUsuario: "+e);
  }
  return mail;
  }
  //**************************************************************************
// M�todo              : Permite la fecha de ingreso a la USM
// Programadora        : M�nica B.
// Fecha creacion      : 27/6/2007
// Fecha modificacion  :
//****************************************************************************
public String getIngresoAlumnoViña (int rut_usuario){
String fecha ="";
String query = "SELECT TO_CHAR(ing_fecha,'dd/mm/yyyy') FROM ingreso_alumno" +
               " WHERE alu_rut = " + rut_usuario +
               " AND ing_fecha = (SELECT MAX(ing_fecha) FROM ingreso_alumno" +
               " WHERE alu_rut = " + rut_usuario +") ";
 try{
  Statement sent = con.conexion().createStatement();
  ResultSet res = sent.executeQuery(query);
  if (res.next()){
    fecha = res.getString(1); // fecha de ingreso
  }
  res.close();
  sent.close();
}
catch(SQLException e){
  System.out.println("Error en Datos_vi�a.getIngresoAlumno: "+e);
}
return fecha;
  }
}
