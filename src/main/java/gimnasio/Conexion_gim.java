package gimnasio;

import java.sql.*;
import java.util.Locale;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import oracle.jdbc.driver.OracleDriver;

public class Conexion_gim
{

	private String driver,url,user,password;
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private ResultSetMetaData rsmd = null;
	private boolean _escapeProcessing = true;
	Context context = null;
    DataSource dataSource = null;
	

	public Conexion_gim()
	{

	  /*  this.driver = "oracle.jdbc.driver.OracleDriver";
		

		
		this.url = "jdbc:oracle:thin:@orcl-test.dti.utfsm.cl:1521:orcl";
        this.user = "dbo_db_utfsm";
        this.password = "";

		try
		{
			DriverManager.registerDriver(new OracleDriver());
			Class.forName(driver).newInstance();

			Properties connAttr = new Properties();

			Properties props = new Properties();

			connAttr.put("USER",this.user);
			connAttr.put("PASSWORD",this.password);
			

			Locale.setDefault(new Locale("es","ES"));

			//System.out.println("SIGA2 ConexionSQL3***");

			DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver()); 
			//conexion=DriverManager.getConnection(url,connAttr) ;
			con=DriverManager.getConnection(url,this.user ,this.password);
		    stmt = con.createStatement();
		    stmt.setEscapeProcessing(_escapeProcessing);
		    con.clearWarnings();
		}
		catch (Exception exc)
		{
			System.out.println("Error al tratar de abrir la base de Datos"+" : "+exc);
			exc.printStackTrace();
		}*/


		try{
            context = new InitialContext();
            dataSource = (DataSource) context.lookup("java:/OracleDS"); // server 63
           // dataSource = (DataSource) context.lookup("java:comp/env/jdbc/OracleDS");  //local
            // dataSource = (DataSource) context.lookup("java:jboss/datasources/OracleDS"); // JBOSS
            con = dataSource.getConnection();
            stmt = con.createStatement();
        }
		catch (NamingException e) {
            System.out.println("NamingException db_oracle: " + e);
            e.printStackTrace();
        }
		catch(SQLException e){
			System.out.println("Error al tratar de abrir db_oracle: "+e);
			e.printStackTrace();
        }
	}

	public ResultSet retornarConsulta(String Rtquery)

	{
		try
		{
			String user=System.getProperty("user.region");
			System.setProperty("user.region","es");
			Statement query = con.createStatement();
			ResultSet nombres = query.executeQuery (Rtquery);
			con.commit();
			if (user!=null)
				System.setProperty("user.region",user);
			return nombres;
		}

		catch (Exception exc)
		{
			System.out.println("Error al hacer la consulta"+" : "+exc);
			return null;
		}
	}

	/**
	 * 
	 * @webmethod 
	 */
	public int ejecutarConsulta(String Stquery)
	{
		try
		{
			String user=System.getProperty("user.region");
			System.setProperty("user.region","es");
			Statement query = con.createStatement();
			query.executeUpdate(Stquery);
			con.commit();
			if (user!=null)
				System.setProperty("user.region",user);
			return 0;
		}

		catch (Exception exc)
		{
			System.out.println("Error al hacer la consulta"+" : "+exc);
			return -1;
		}
	}


	public Connection getConexion()
	//Permite retornar la conexi�n
	{
		return con;
	}

	/**
	 * 
	 * @webmethod 
	 */
	public int CerrarConexion()
	{
		try
		{
			con.close(); 
			return 0;
		}
		catch(Exception exc)
		{
			System.out.println("Error al cerrar la conexion: "+" : "+exc);
			return -1;
		}
	}

	public void close(){
	      try
	      {
	          if (con != null)
	          {
	              stmt.close();
	              con.close();
	              con = null;
	          }
	      }
	      catch (SQLException sqe)
	      {
	          System.out.println("Unexpected exception : " +
	              sqe.toString() + ", sqlstate = " + sqe.getSQLState());
	          sqe.printStackTrace();
	      }
	  }

	public Statement sentencia(){
		return stmt;
	}
	
	public Connection conexion(){
		return con;
	}
	
	private void jbInit() throws Exception {

	}
}
