<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 13/06/2007
   // �ltima Actualizaci�n :
   // Registro. se registran y mantienen todos los usuarios del gimnasio, funcionarios y alumnos.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<jsp:useBean id="us" class="gimnasio.BeanUsuarios"></jsp:useBean>

<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  int cod_perfil = session.getAttribute("cod_perfil") != null?
                 Integer.parseInt(session.getAttribute("cod_perfil")+""):0;

  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int rutnum      =  request.getParameter("rutnum")!=null &&
                     !request.getParameter("rutnum").equals("null") &&
                     !request.getParameter("rutnum").equals("NULL") &&
                     !request.getParameter("rutnum").equals("")  &&
                     !request.getParameter("rutnum").equals(" ")?
                     Integer.parseInt(request.getParameter("rutnum")+""):0;
  String dvrut    =  request.getParameter("dvrut")!=null &&
                     !request.getParameter("dvrut").equals("null") &&
                     !request.getParameter("dvrut").equals("NULL") &&
                     !request.getParameter("dvrut").equals("")  &&
                     !request.getParameter("dvrut").equals(" ")?
                     request.getParameter("dvrut")+"":"";
  String rut_aux  =  request.getParameter("rut_aux")!=null &&
                     !request.getParameter("rut_aux").equals("null") &&
                     !request.getParameter("rut_aux").equals("NULL") &&
                     !request.getParameter("rut_aux").equals("")  &&
                     !request.getParameter("rut_aux").equals(" ")?
                     request.getParameter("rut_aux")+"":"";
  int accion      =  request.getParameter("accion")!=null &&
                     !request.getParameter("accion").equals("null") &&
                     !request.getParameter("accion").equals("NULL") &&
                     !request.getParameter("accion").equals("")  &&
                     !request.getParameter("accion").equals(" ")?
                     Integer.parseInt(request.getParameter("accion")+""):0;
  int usuario_gim =  request.getParameter("usuario")!=null &&
                     !request.getParameter("usuario").equals("null") &&
                     !request.getParameter("usuario").equals("NULL") &&
                     !request.getParameter("usuario").equals("")  &&
                     !request.getParameter("usuario").equals(" ")?
                     Integer.parseInt(request.getParameter("usuario")+""):0;
  int sexo =  request.getParameter("sexo")!=null &&
                    !request.getParameter("sexo").equals("null") &&
                    !request.getParameter("sexo").equals("NULL") &&
                    !request.getParameter("sexo").equals("")  &&
                    !request.getParameter("sexo").equals(" ")?
                    Integer.parseInt(request.getParameter("sexo")+""):0;

  boolean soloconsulta = false;
  Vector vec_per_anulado = new Vector();
 if (cod_perfil==28) soloconsulta = true;
   String nombre = "";
   String rut_foto = "";
   String carrera = "";
   String situacion = "";
   String rol = "";
   String anexo = "";
   String email = "";
   String ultmat = "";
   String mensaje = "";
   String planta = "";
   String onload = "document.fingresar.rut_aux.focus();";
   String id_registro = "0";
   boolean esalumno = false ;
   boolean esrama = false;
   boolean habilitado = false;
   boolean esvi�a = false;
   String id_rama = "0";
   String etiqueta1 = "";
   String etiqueta2 = "";
   String etiqueta3 = "";
   String etiqueta4 = "";
   java.util.Vector vec_alumno = new java.util.Vector();
   java.util.Vector vec_funcionario = new java.util.Vector();
   java.util.Vector vec_rama = new java.util.Vector();
   java.util.Vector vec_ramadep = new java.util.Vector();
   java.util.Vector vec_registro = new java.util.Vector();
   java.util.Vector vec_bloque_s1 = new java.util.Vector();
   java.util.Vector vec_bloque_s2 = new java.util.Vector();
   java.util.Vector vec_periodo = new java.util.Vector();
   java.util.Vector vec_reserva = new java.util.Vector();
   java.util.Vector vec_tutoria1 = new java.util.Vector();
   java.util.Vector vec_tutoria2 = new java.util.Vector();
   int a�o_s1 = 0;
   int semestre_s1 = 0;
   String id_per_s1 = "0";
   String fini_s1 = "";
   String fter_s1 = "";
   int a�o_s2 = 0;
   int semestre_s2 = 0;
   String id_per_s2 = "0";
   String fini_s2 = "";
   String fter_s2 = "";

   String selected_sala1 = "";
   String selected_sala2 = "";
   String selected_b1 = "";
   String selected_b2 = "";
   String selected_b3 = "";
   String selected_b4 = "";
   String selected_b5 = "";
   String selected_b6 = "";
   String disable = "";
   String cargo = "";
   String jerarquia = "";
   String contrato = "";
   if(soloconsulta) disable = "disabled";
   int origen = 0;
   int codContrato = 0;
   int fono = 0;

  if (rutnum > 0) {
     rut_foto = rutnum + dvrut;
     if (rut_foto.length() == 8) rut_foto = "00"+ rut_foto;
      else rut_foto = "0" + rut_foto;
   }


   int error = 0;
   if(rutnum>0){
     general.getAbrirConexion();
     general.setGenerarConexionDatosAl();
     general.setGenerarConexionRegistro();
     if (accion > 0){
       String rama =  request.getParameter("rama")!=null &&
                      !request.getParameter("rama").equals("null") &&
                      !request.getParameter("rama").equals("NULL") &&
                      !request.getParameter("rama").equals("")  &&
                      !request.getParameter("rama").equals(" ")?
                      request.getParameter("rama")+"":"0";

       String id   =  request.getParameter("id_registro")!=null &&
                      !request.getParameter("id_registro").equals("null") &&
                      !request.getParameter("id_registro").equals("NULL") &&
                      !request.getParameter("id_registro").equals("")  &&
                      !request.getParameter("id_registro").equals(" ")?
                      request.getParameter("id_registro")+"":"0";
                      
		//System.out.println("id: "+id);

       // Tutor�a
       String tut_sala1 = request.getParameter("sala1")!=null &&
                         !request.getParameter("sala1").equals("null") &&
                         !request.getParameter("sala1").equals("NULL") &&
                         !request.getParameter("sala1").equals("")  &&
                         !request.getParameter("sala1").equals(" ")?
                         request.getParameter("sala1")+"":"0";
       String tut_b1   = request.getParameter("bloque1")!=null &&
                         !request.getParameter("bloque1").equals("null") &&
                         !request.getParameter("bloque1").equals("NULL") &&
                         !request.getParameter("bloque1").equals("")  &&
                         !request.getParameter("bloque1").equals(" ")?
                         request.getParameter("bloque1")+"":"";
       String tut_b2   = request.getParameter("bloque2")!=null &&
                         !request.getParameter("bloque2").equals("null") &&
                         !request.getParameter("bloque2").equals("NULL") &&
                         !request.getParameter("bloque2").equals("")  &&
                         !request.getParameter("bloque2").equals(" ")?
                         request.getParameter("bloque2")+"":"";
       String tut_b3   = request.getParameter("bloque3")!=null &&
                      !request.getParameter("bloque3").equals("null") &&
                      !request.getParameter("bloque3").equals("NULL") &&
                      !request.getParameter("bloque3").equals("")  &&
                      !request.getParameter("bloque3").equals(" ")?
                         request.getParameter("bloque3")+"":"";
       String tut_sala2 = request.getParameter("sala2")!=null &&
                         !request.getParameter("sala2").equals("null") &&
                         !request.getParameter("sala2").equals("NULL") &&
                         !request.getParameter("sala2").equals("")  &&
                         !request.getParameter("sala2").equals(" ")?
                         request.getParameter("sala2")+"":"0";
       String tut_b4   = request.getParameter("bloque4")!=null &&
                         !request.getParameter("bloque4").equals("null") &&
                         !request.getParameter("bloque4").equals("NULL") &&
                         !request.getParameter("bloque4").equals("")  &&
                         !request.getParameter("bloque4").equals(" ")?
                         request.getParameter("bloque4")+"":"";
       String tut_b5   = request.getParameter("bloque5")!=null &&
                         !request.getParameter("bloque5").equals("null") &&
                         !request.getParameter("bloque5").equals("NULL") &&
                         !request.getParameter("bloque5").equals("")  &&
                         !request.getParameter("bloque5").equals(" ")?
                         request.getParameter("bloque5")+"":"";
       String tut_b6   = request.getParameter("bloque6")!=null &&
                      !request.getParameter("bloque6").equals("null") &&
                      !request.getParameter("bloque6").equals("NULL") &&
                      !request.getParameter("bloque6").equals("")  &&
                      !request.getParameter("bloque6").equals(" ")?
                         request.getParameter("bloque6")+"":"";

       String tut_a�o  = request.getParameter("tut_a�o")!=null &&
                         !request.getParameter("tut_a�o").equals("null") &&
                         !request.getParameter("tut_a�o").equals("NULL") &&
                         !request.getParameter("tut_a�o").equals("")  &&
                         !request.getParameter("tut_a�o").equals(" ")?
                         request.getParameter("tut_a�o")+"":"0";
       String tut_sem  = request.getParameter("tut_semestre")!=null &&
                         !request.getParameter("tut_semestre").equals("null") &&
                         !request.getParameter("tut_semestre").equals("NULL") &&
                         !request.getParameter("tut_semestre").equals("")  &&
                         !request.getParameter("tut_semestre").equals(" ")?
                         request.getParameter("tut_semestre")+"":"0";
       String tut_per  = request.getParameter("tut_pergim_id1")!=null &&
                         !request.getParameter("tut_pergim_id1").equals("null") &&
                         !request.getParameter("tut_pergim_id1").equals("NULL") &&
                         !request.getParameter("tut_pergim_id1").equals("")  &&
                         !request.getParameter("tut_pergim_id1").equals(" ")?
                         request.getParameter("tut_pergim_id1")+"":"0";
      String tut_per2  = request.getParameter("tut_pergim_id2")!=null &&
                         !request.getParameter("tut_pergim_id2").equals("null") &&
                         !request.getParameter("tut_pergim_id2").equals("NULL") &&
                         !request.getParameter("tut_pergim_id2").equals("")  &&
                         !request.getParameter("tut_pergim_id2").equals(" ")?
                         request.getParameter("tut_pergim_id2")+"":"0";

       String tut_f1   = request.getParameter("tut_fecha_inicio1")!=null &&
                         !request.getParameter("tut_fecha_inicio1").equals("null") &&
                         !request.getParameter("tut_fecha_inicio1").equals("NULL") &&
                         !request.getParameter("tut_fecha_inicio1").equals("")  &&
                         !request.getParameter("tut_fecha_inicio1").equals(" ")?
                         request.getParameter("tut_fecha_inicio1")+"":"0";
       String tut_f2   = request.getParameter("tut_fecha_termino1")!=null &&
                         !request.getParameter("tut_fecha_termino1").equals("null") &&
                         !request.getParameter("tut_fecha_termino1").equals("NULL") &&
                         !request.getParameter("tut_fecha_termino1").equals("")  &&
                         !request.getParameter("tut_fecha_termino1").equals(" ")?
                         request.getParameter("tut_fecha_termino1")+"":"0";
      String tut_f3   = request.getParameter("tut_fecha_inicio2")!=null &&
                         !request.getParameter("tut_fecha_inicio2").equals("null") &&
                         !request.getParameter("tut_fecha_inicio2").equals("NULL") &&
                         !request.getParameter("tut_fecha_inicio2").equals("")  &&
                         !request.getParameter("tut_fecha_inicio2").equals(" ")?
                         request.getParameter("tut_fecha_inicio2")+"":"0";
       String tut_f4   = request.getParameter("tut_fecha_termino2")!=null &&
                         !request.getParameter("tut_fecha_termino2").equals("null") &&
                         !request.getParameter("tut_fecha_termino2").equals("NULL") &&
                         !request.getParameter("tut_fecha_termino2").equals("")  &&
                         !request.getParameter("tut_fecha_termino2").equals(" ")?
                         request.getParameter("tut_fecha_termino2")+"":"0";


       vec_reserva.addElement(tut_a�o); // a�o
       vec_reserva.addElement(tut_sem); // semestre
       vec_reserva.addElement(tut_f1); // fecha_inicio1
       vec_reserva.addElement(tut_f2); // fecha_termino1
       vec_reserva.addElement(tut_per); // pergim_id1
       vec_reserva.addElement(tut_b1); // linea1
       vec_reserva.addElement(tut_b2); // linea2
       vec_reserva.addElement(tut_b3); // linea3
       vec_reserva.addElement(tut_f3); // fecha_inicio2
       vec_reserva.addElement(tut_f4); // fecha_termino2
       vec_reserva.addElement(tut_per2); // pergim_id2
       vec_reserva.addElement(tut_b4); // linea4
       vec_reserva.addElement(tut_b5); // linea5
       vec_reserva.addElement(tut_b6); // linea6

       //vec_reserva.addElement(rutnum+""); // rut
       //vec_reserva.addElement(rut_usuario+""); // rut_usuario

      switch (accion){
       case 1: { // agregar
            error = general.setInsertaRegistro(rutnum, usuario_gim, rut_usuario, rama, vec_reserva, sexo);

            if(error == 0) mensaje = "Se registr� satisfactoriamente";
            else mensaje = "Problemas al registrar ";
            error = -1;
        break;
       }
       case 2: { // eliminar
             error = general.setEliminarRegistro(id, rut_usuario);
             if(error == 0) {
               mensaje = "Se elimin� el registro satisfactoriamente";
               rutnum = 0;
               rut_aux = "";
               dvrut = "";
             } else mensaje = "Problemas al eliminar";
             error = -1;
       break;
       }
     case 3: { // actualizar
       error = general.setActualizarRegistro(rutnum, usuario_gim, rut_usuario, rama, id, vec_reserva);
       if(error == 0) mensaje = "Se actualiz� el registro satisfactoriamente";
       else mensaje = "Problemas al actualizar";
       error = -1;
       break;
     }
      }

     }
     vec_registro = general.getRegistroUsuario(rutnum);
     if (vec_registro.size()> 0) {
          id_registro = vec_registro.get(4)+"";
          usuario_gim = Integer.parseInt(vec_registro.get(1)+"");
     }
     // inicio tutoria
     general.setGenerarConexionHorario();
     general.setGenerarConexionReserva();
     general.getPeriodoSemestreActual(1,1);
     vec_periodo = general.getPeriodoGimnasioVigente(10335);

     if (vec_periodo.size() > 0){
       a�o_s1      = Integer.parseInt(vec_periodo.get(3)+"");
       semestre_s1 = Integer.parseInt(vec_periodo.get(4)+"");
       id_per_s1   = vec_periodo.get(2)+"";
       a�o_s2      = Integer.parseInt(vec_periodo.get(3)+"");
       String fec_ini = vec_periodo.get(0)+"";
       String mm1  = (Integer.parseInt(fec_ini.substring(3,5)) < 10)?
                     ("0"+Integer.parseInt(fec_ini.substring(3,5))):
                     (""+Integer.parseInt(fec_ini.substring(3,5)));
       String dd1  = (Integer.parseInt(fec_ini.substring(0,2)) < 10)?
                     ("0"+Integer.parseInt(fec_ini.substring(0,2))):
                     (""+Integer.parseInt(fec_ini.substring(0,2)));
       String aa1  = (Integer.parseInt(fec_ini.substring(6,10)) < 10)?
                     ("0"+Integer.parseInt(fec_ini.substring(6,10))):
                     (""+Integer.parseInt(fec_ini.substring(6,10)));
       fini_s1 = aa1 + mm1 + dd1;

       String fec_ter = (vec_periodo.size()>0)?vec_periodo.get(1)+"":"";
       String mm = (Integer.parseInt(fec_ter.substring(3,5)) < 10)?
                   ("0"+Integer.parseInt(fec_ter.substring(3,5))):
                   (""+Integer.parseInt(fec_ter.substring(3,5)));
       String dd = (Integer.parseInt(fec_ter.substring(0,2)) < 10)?
                   ("0"+Integer.parseInt(fec_ter.substring(0,2))):
                   (""+Integer.parseInt(fec_ter.substring(0,2)));
       String aa = (Integer.parseInt(fec_ter.substring(6,10)) < 10)?
                   ("0"+Integer.parseInt(fec_ter.substring(6,10))):
                   (""+Integer.parseInt(fec_ter.substring(6,10)));;
       fter_s1 = aa + mm + dd;

     }

     vec_periodo = general.getPeriodoGimnasioVigente(10336);
     if (vec_periodo.size() > 0){
       a�o_s2      = Integer.parseInt(vec_periodo.get(3)+"");
       semestre_s2 = Integer.parseInt(vec_periodo.get(4)+"");
       id_per_s2   = vec_periodo.get(2)+"";
       String fec_ini = vec_periodo.get(0)+"";
       String mm1  = (Integer.parseInt(fec_ini.substring(3,5)) < 10)?
                     ("0"+Integer.parseInt(fec_ini.substring(3,5))):
                     (""+Integer.parseInt(fec_ini.substring(3,5)));
       String dd1  = (Integer.parseInt(fec_ini.substring(0,2)) < 10)?
                     ("0"+Integer.parseInt(fec_ini.substring(0,2))):
                     (""+Integer.parseInt(fec_ini.substring(0,2)));
       String aa1  = (Integer.parseInt(fec_ini.substring(6,10)) < 10)?
                     ("0"+Integer.parseInt(fec_ini.substring(6,10))):
                     (""+Integer.parseInt(fec_ini.substring(6,10)));
       fini_s2 = aa1 + mm1 + dd1;

       String fec_ter = (vec_periodo.size()>0)?vec_periodo.get(1)+"":"";
       String mm = (Integer.parseInt(fec_ter.substring(3,5)) < 10)?
                   ("0"+Integer.parseInt(fec_ter.substring(3,5))):
                   (""+Integer.parseInt(fec_ter.substring(3,5)));
       String dd = (Integer.parseInt(fec_ter.substring(0,2)) < 10)?
                   ("0"+Integer.parseInt(fec_ter.substring(0,2))):
                   (""+Integer.parseInt(fec_ter.substring(0,2)));
       String aa = (Integer.parseInt(fec_ter.substring(6,10)) < 10)?
                   ("0"+Integer.parseInt(fec_ter.substring(6,10))):
                   (""+Integer.parseInt(fec_ter.substring(6,10)));;
       fter_s2 = aa + mm + dd;
 }

     if (a�o_s1 > 0) vec_bloque_s1 = general.getHorario (a�o_s1, semestre_s1, 10335); // M�quinas
     if (a�o_s2 > 0) vec_bloque_s2 = general.getHorario (a�o_s2, semestre_s2, 10336); // Aer�bica
        // fin tutor�a

     vec_ramadep = general.getRamaDeportiva();
     
     vec_alumno = general.getAlumno(rutnum);
     /*if (vec_alumno.size()== 0 || //que no exista como alumno CC
        (vec_alumno.size() > 0 &&
        (vec_alumno.get(5).equals("")) //|| (vec_alumno.get(8)+"").trim().equals("2")
        )){ // o que exista como alumno CC pero no tenga carrera vigente
       // ver si es alumno vi�a
       general.getAbrirConexionVi�a();
       general.setGenerarConexionDatosVi�a();
       vec_alumno = general.getAlumnoVi�a(rutnum);
       sexo = (vec_alumno.size()>0)?Integer.parseInt(vec_alumno.get(19)+""):0;
       general.getCerrarConexionVi�a();
//System.out.println("vec_alumno: vi�a "+vec_alumno);
     }*/
//System.out.println("vec_alumno.size(): "+vec_alumno.size()+" POS8: "+vec_alumno.get(8));
     if ((vec_alumno.size() == 0 ) || // no es alumno
         (vec_alumno.size() > 0 &&  Integer.parseInt(vec_alumno.get(8)+"")==2)) {
       //si no es alumno � si es alumno y no est� habilitado, ver si es funcionario
        if (vec_alumno.size() > 0)  esalumno = true;

     } else {
       esalumno = true;
       habilitado = true;
       //System.out.println("habilitado 1 : "+habilitado);
       if (Integer.parseInt(vec_alumno.get(18)+"") != 1) esvi�a = true;

     }
     
     if(vec_alumno.size()>0 &&  esalumno  ){ // si es alumno
        // si es alumno
         vec_rama = general.getAlumnoRama(rutnum);
         if (usuario_gim == 2 && !id_registro.trim().equals("0")  && !id_per_s1.trim().equals("0")
             && !id_registro.trim().equals("")  && !id_per_s1.trim().equals("")) {
           vec_tutoria1 = general.getReservaTutoria(id_registro, id_per_s1);
           vec_tutoria2 = general.getReservaTutoria(id_registro, id_per_s2);
         } else if (usuario_gim == 2 ) {
                 mensaje = "No existe per�odo Vigente";
                 error = -1;
                 //System.out.println(vec_alumno.size()+"-"+esalumno+"-"+usuario_gim+"-"+id_registro+"-"+id_per_s1);
         }

         nombre = vec_alumno.get(1)+"";
         carrera = vec_alumno.get(5)+"";
         situacion =(Integer.parseInt(vec_alumno.get(8)+"") == 1)?"HABILITADO":"INHABILITADO";
         email = vec_alumno.get(4)+"";
         ultmat = vec_alumno.get(7)+"";
         rol = vec_alumno.get(9)+"";
         etiqueta1 = "Carrera";
         etiqueta2 = "Rol";
         etiqueta3 = "E-Mail";
         etiqueta4 = "Situaci�n";
     /*   if (!ultmat.trim().equals("0"))
           situacion = situacion + ", �ltima Matr�cula "+ultmat;*/
         if (vec_rama.size()> 0) {
           esrama = true;
           id_rama = vec_rama.get(4)+"";
           onload += "escondeBloque('bloque_rama', 1);";
         } else  onload += "escondeBloque('bloque_rama', 0);";

         if (vec_tutoria1.size() > 0 || vec_tutoria2.size() > 0) {
           int cont = 0;
           for (int y = 0; y < vec_tutoria1.size(); y++) {
             cont ++;
             if (cont == 1) selected_sala1 = vec_tutoria1.get(y)+"";
             if (cont == 2) selected_b1 = vec_tutoria1.get(y)+"";
             if (cont == 4) selected_b2 = vec_tutoria1.get(y)+"";
             if (cont == 6) selected_b3 = vec_tutoria1.get(y)+"";
           }
           cont = 0;
            for (int y = 0; y < vec_tutoria2.size(); y++) {
             cont ++;
             if (cont == 1) selected_sala2 = vec_tutoria2.get(y)+"";
             if (cont == 2) selected_b4 = vec_tutoria2.get(y)+"";
             if (cont == 4) selected_b5 = vec_tutoria2.get(y)+"";
             if (cont == 6) selected_b6 = vec_tutoria2.get(y)+"";
            }


           onload += "CambiaBloque1();"+
                     "escondeBloque('bloque_tutoria0', 1);" +
                     "escondeBloque('bloque_tutoria1', 1);" +
                     "escondeBloque('bloque_tutoria2', 1);"+
                     "escondeBloque('bloque_tutoria3', 1);"+
                     "CambiaBloque2();"+
                     "escondeBloque('bloque_tutoria4', 1);" +
                     "escondeBloque('bloque_tutoria5', 1);" +
                     "escondeBloque('bloque_tutoria6', 1);"+
                     "escondeBloque('bloque_tutoria7', 1);";
         } else
           onload += "escondeBloque('bloque_tutoria0', 0);" +
           "escondeBloque('bloque_tutoria1', 0);" +
           "escondeBloque('bloque_tutoria2', 0);"+
           "escondeBloque('bloque_tutoria3', 0);" +
           "escondeBloque('bloque_tutoria4', 0);" +
           "escondeBloque('bloque_tutoria5', 0);"+
           "escondeBloque('bloque_tutoria6', 0);"+
           "escondeBloque('bloque_tutoria7', 0);";

         if(!habilitado && vec_funcionario.size() == 0) situacion = "Acad�micamente Inhabilitado ";

     }
     us.getAbrirConexion();
     us.setGenerarConexionUsuario();
     vec_funcionario = us.getFuncionario(rutnum);
     //System.out.println("vec_funcionario: "+vec_funcionario);
     //System.out.println("habilitado  : "+habilitado);
     if(vec_funcionario.size()>0 && !habilitado ){ // si es funcionario y no alumno habilitado
     origen = 1;
     nombre  = vec_funcionario.get(4)+" "+vec_funcionario.get(2)+" "+vec_funcionario.get(3)+"";
     carrera = vec_funcionario.get(14)+", " + vec_funcionario.get(20);
     planta = vec_funcionario.get(17)+"";
     cargo = vec_funcionario.get(18)+"";
     contrato = vec_funcionario.get(13)+"";
     jerarquia = vec_funcionario.get(12)+"";
     codContrato =(vec_funcionario.get(19) != null)?Integer.parseInt(vec_funcionario.get(19)+""):0;
     fono =(vec_funcionario.get(8) != null)?Integer.parseInt(vec_funcionario.get(8)+""):0;
     etiqueta1 = "Departamento";
       }
     /*if (vec_funcionario.size() == 0 && vec_alumno.size() == 0 ){
       general.getAbrirConexionVi�a();
       general.setGenerarConexionDatosVi�a();
       vec_funcionario = general.getFuncionarioVi�a(rutnum);
       general.getCerrarConexionVi�a();
    if(vec_funcionario.size()>0 && !habilitado ){ // si es funcionario y no alumno habilitado

     nombre  = vec_funcionario.get(0)+" "+vec_funcionario.get(1)+" "+vec_funcionario.get(2)+"";
     carrera = vec_funcionario.get(4)+", " + vec_funcionario.get(5);
     rol   = vec_funcionario.get(6)+""; // corresponde al anexo
     email   = vec_funcionario.get(7)+"";
     situacion  = vec_funcionario.get(10)+"";  // tipo contrato
     ultmat  = vec_funcionario.get(11)+""; // nombre contrato
     etiqueta1 = "Departamento";
     etiqueta2 = "Anexo";
     etiqueta3 = "E-Mail";
     etiqueta4 = "Tipo de Contrato";
     if (situacion.trim().equals("2"))  // es profesor
       situacion = "Profesor, " + ultmat; // nombre contrato
    else
      situacion = ultmat; // nombre contrato
     planta =  vec_funcionario.get(12)+""; // si es de planta
     if (Integer.parseInt(vec_funcionario.get(16)+"") != 1) esvi�a = true;
     if (esvi�a)
       sexo = Integer.parseInt(vec_funcionario.get(17)+"");
       }
     }*/

     us.getCerrarConexion();


       if(vec_alumno.size()== 0 &&  vec_funcionario.size() == 0  ){
         etiqueta1 = "";
         etiqueta2 = "";
         etiqueta3 = "";
         etiqueta4 = "";
         situacion = "Este RUT no se encuentra con matr�cula vigente como alumno.";
       }

    general.getCerrarConexion();

   }

 onload += "Mensaje();";
 
 if (rut_usuario > 0){
	  session.setAttribute("rut_consulta","77777777");
 }
%>
<html>
<head>
<title>Gimnasio - Registro</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script src="js/centrar.js"></script>
<script src="js/valida_login.js"></script>
<script language="JavaScript" src="js/trim.js"></script>
<script language="JavaScript">
function Mensaje(){
 <%if(error<0){%>alert ('<%=mensaje%>');<%}%>
}
function noenter() {
  return !(window.event && window.event.keyCode == 13);
}
function enter(e){
  if (window.event) {
    if (window.event.keyCode == 13)
      validar(document.fingresar);
  }
  else {
    if (e.which == 13)
      validar(document.fingresar);
  }
}
function validar(form){
  if(trim(form.rut_aux.value)==""){
    alert("Debe ingresar el RUT a registrar");
    form.rut_aux.focus();
    return false;
  }
  if (!valida_rut2(form.rut_aux, form.rutnum, form.dvrut, 'RUT')) {
    form.rut_aux.focus();
    return false;
  }
  if (document.fingresar.usuario != null)
    document.fingresar.usuario.value = "0";
  // return true;
 // document.fingresar.usuario.value = "0";
  form.submit();
}

function AbrirVentana(f,a, alto, ancho) {
  f.target = "ventana";
  f.action = a;
  var par = 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=yes';
  var w = Centrar03(f.target,alto,ancho,par);
  w.focus();
  f.submit();
}

function escondeBloque(idBloque, opcion){
  if (opcion==0){
    if (document.getElementById(idBloque) != null)
    document.getElementById(idBloque).style.display = 'none';
  }
  if (opcion==1){
    if (document.getElementById(idBloque) != null)
    document.getElementById(idBloque).style.display = '';
  }
}

function Cambiar(){
  escondeBloque('bloque_tutoria0', 0);
  escondeBloque('bloque_tutoria1', 0);
  escondeBloque('bloque_tutoria2', 0);
  escondeBloque('bloque_tutoria3', 0);
  escondeBloque('bloque_tutoria4', 0);
  escondeBloque('bloque_tutoria5', 0);
  escondeBloque('bloque_tutoria6', 0);
  escondeBloque('bloque_tutoria7', 0);
  escondeBloque('bloque_rama', 0);
  if (document.fingresar.usuario.value != 4 && <%=esalumno%> == true) {
    if (document.fingresar.usuario.value == 3) {
      if(<%=vec_ramadep.size()%> > 0)
      escondeBloque('bloque_rama', 1);
      else {
        alert ('No existe registro de Rama Deportiva');
        return false;
      }
    } else {
      if (document.fingresar.usuario.value == 2) { // es tutor�a
        if (<%=vec_bloque_s1.size()%> > 0 ){
        escondeBloque('bloque_tutoria0', 1);
        escondeBloque('bloque_tutoria1', 1);
        escondeBloque('bloque_tutoria2', 1);
        escondeBloque('bloque_tutoria3', 1);
        escondeBloque('bloque_tutoria4', 1);
        escondeBloque('bloque_tutoria5', 1);
        escondeBloque('bloque_tutoria6', 1);
        escondeBloque('bloque_tutoria7', 1);
        CambiaBloque1();
        CambiaBloque2();

        } else {
          if (<%=a�o_s1%> == 0)
               alert ('No existe registro de Per�odo');
          else
               alert ('No existe registro de Horario');
        return false;}
      } else {
        escondeBloque('bloque_tutoria0', 0);
        escondeBloque('bloque_tutoria1', 0);
        escondeBloque('bloque_tutoria2', 0);
        escondeBloque('bloque_tutoria3', 0);
        escondeBloque('bloque_tutoria4', 0);
        escondeBloque('bloque_tutoria5', 0);
        escondeBloque('bloque_tutoria6', 0);
        escondeBloque('bloque_tutoria7', 0);
        escondeBloque('bloque_rama', 0);

      }
    }
  }
  else {
    if (document.fingresar.usuario.value == 4  && <%=id_rama%> > 0) {
    escondeBloque('bloque_rama', 0);
    escondeBloque('bloque_tutoria0', 0);
    escondeBloque('bloque_tutoria1', 0);
    escondeBloque('bloque_tutoria2', 0);
    escondeBloque('bloque_tutoria3', 0);
    escondeBloque('bloque_tutoria4', 0);
    escondeBloque('bloque_tutoria5', 0);
    escondeBloque('bloque_tutoria6', 0);
    escondeBloque('bloque_tutoria7', 0);
    }
  }
}

function Grabar(registra){
  if (trim(document.fingresar.rut_aux.value)==""){
    alert("Debe ingresar el RUT a registrar");
    document.fingresar.rut_aux.focus();
    return false;
  }
  if (!valida_rut2(document.fingresar.rut_aux,
                   document.fingresar.rutnum,
                   document.fingresar.dvrut, 'RUT')) {
    document.fingresar.rut_aux.focus();
    return false;
  }
  if (document.fingresar.usuario.value == 0){
    alert("Debe seleccionar el usuario");
    document.fingresar.usuario.focus();
    return false;
  }

  if (document.fingresar.usuario.value == 3 && ( <%=vec_ramadep.size()%> == 0 ||
      document.fingresar.rama.value == 0)){
    if (<%=vec_ramadep.size()%> == 0)
      alert("No hay registrada Rama Deportiva");
    else {
        alert("Debe seleccionar la Rama Deportiva");
    document.fingresar.rama.focus();
    }
    return false;
  }

  if (document.fingresar.usuario.value == 2) {
    if (document.fingresar.sala1.value == 0 && document.fingresar.sala2.value == 0){
      if (<%=vec_bloque_s1.size()%> > 0){
      alert("Debe seleccionar una Sala");
      document.fingresar.sala1.focus();
      } else alert("No existe registro de Horario");
      return false;
    }

    if (document.fingresar.bloque1.value == "" &&
        document.fingresar.bloque2.value == "" &&
        document.fingresar.bloque3.value == "" &&
        document.fingresar.bloque4.value == "" &&
        document.fingresar.bloque5.value == "" &&
        document.fingresar.bloque6.value == ""){
      alert("Debe seleccionar una Reserva");
      if (document.fingresar.bloque1.value == "")
        document.fingresar.bloque1.focus();
      else   if (document.fingresar.bloque2.value == "")
              document.fingresar.bloque2.focus();
              else
               if (document.fingresar.bloque3.value == "")
                   document.fingresar.bloque3.focus();
              else
                 if (document.fingresar.bloque4.value == "")
                 document.fingresar.bloque4.focus();
                 else
                  if (document.fingresar.bloque5.value == "")
                  document.fingresar.bloque5.focus();
                  else
                  document.fingresar.bloque6.focus();

      return false;
    }
  }

 switch (registra ){
  case 0 : {
    document.fingresar.accion.value = 1; // agrega
    GuardarReserva();
 //   document.fingresar.submit();
    break;
  }
  case -1 : {
    document.fingresar.accion.value = 2; // elimina
    document.fingresar.submit();
    break;
  }
  default: {
   // if (<%=usuario_gim%> != document.fingresar.usuario.value)||
   //    (('<%=id_rama%>' != '') && ('<%=id_rama%>' != document.fingresar.rama.value))) {
    document.fingresar.accion.value = 3; // actualiza
    GuardarReserva();
  //  document.fingresar.submit();
   // } else alert ('No se han registrado cambios');
    break;
  }
}
}

function Eliminar(){
  var pregunta = "Ud. va a ELIMINAR el registro definitivamente.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  if (respuesta == true) Grabar(-1);
}

arr_bloque_Cod   = new Array();
arr_bloque_Text  = new Array();
<%
StringBuffer bloque_Cod1 = new StringBuffer();
StringBuffer bloque_Text1 = new StringBuffer();
StringBuffer bloque_Cod2 = new StringBuffer();
StringBuffer bloque_Text2 = new StringBuffer();

if (vec_bloque_s1.size() > 0 ||
    vec_bloque_s2.size() > 0){
  bloque_Cod1.append("\"\"");
  bloque_Text1.append("\"-- Seleccione --\"");
  bloque_Cod2.append("\"\"");
  bloque_Text2.append("\" -- Seleccione --\"");

  for (int i = 0; i < vec_bloque_s1.size();i++){
    if (((Vector)vec_bloque_s1.get(i)).get(8).equals("1")) {
      bloque_Cod1.append(",\"");
      bloque_Cod1.append(((Vector)vec_bloque_s1.get(i)).get(0)); // cod_asign
      bloque_Cod1.append("@");
      bloque_Cod1.append(((Vector)vec_bloque_s1.get(i)).get(1)); // paralelo
      bloque_Cod1.append("@");
      bloque_Cod1.append(((Vector)vec_bloque_s1.get(i)).get(2)); // dia
      bloque_Cod1.append("@");
      bloque_Cod1.append(((Vector)vec_bloque_s1.get(i)).get(3)); // codigo bi
      bloque_Cod1.append("@");
      bloque_Cod1.append(((Vector)vec_bloque_s1.get(i)).get(4)); // codigo bt
      bloque_Cod1.append("\"");

      bloque_Text1.append(",\"");
      bloque_Text1.append(((Vector)vec_bloque_s1.get(i)).get(7));
      bloque_Text1.append(" - ");
      bloque_Text1.append(((Vector)vec_bloque_s1.get(i)).get(5));
      bloque_Text1.append(" a ");
      bloque_Text1.append(((Vector)vec_bloque_s1.get(i)).get(6));
      bloque_Text1.append("\"");
    }
  }

  for (int i = 0; i < vec_bloque_s2.size();i++){
    if (((Vector)vec_bloque_s2.get(i)).get(8).equals("1")) {
      bloque_Cod2.append(",\"");
      bloque_Cod2.append(((Vector)vec_bloque_s2.get(i)).get(0)); // cod_asign
      bloque_Cod2.append("@");
      bloque_Cod2.append(((Vector)vec_bloque_s2.get(i)).get(1)); // paralelo
      bloque_Cod2.append("@");
      bloque_Cod2.append(((Vector)vec_bloque_s2.get(i)).get(2)); // dia
      bloque_Cod2.append("@");
      bloque_Cod2.append(((Vector)vec_bloque_s2.get(i)).get(3)); // codigo bi
      bloque_Cod2.append("@");
      bloque_Cod2.append(((Vector)vec_bloque_s2.get(i)).get(4)); // codigo bt
      bloque_Cod2.append("\"");

      bloque_Text2.append(",\"");
      bloque_Text2.append(((Vector)vec_bloque_s2.get(i)).get(7));
      bloque_Text2.append(" - ");
      bloque_Text2.append(((Vector)vec_bloque_s2.get(i)).get(5));
      bloque_Text2.append(" a ");
      bloque_Text2.append(((Vector)vec_bloque_s2.get(i)).get(6));
      bloque_Text2.append("\"");
    }
  }

  out.println("arr_bloque_Cod[0]  = new Array("+bloque_Cod1+");"); // codigo
  out.println("arr_bloque_Text[0] = new Array("+bloque_Text1+");"); // texto
  out.println("arr_bloque_Cod[1]  = new Array("+bloque_Cod2+");"); // bloque inicio
  out.println("arr_bloque_Text[1] = new Array("+bloque_Text2+");"); // bloque termino
}
%>
function CambiaBloque1(){
   var sala = 0;
// Limpia el combo
  document.fingresar.bloque1.options.length = 0;
  document.fingresar.bloque2.options.length = 0;
  document.fingresar.bloque3.options.length = 0;
// Ciclo que llena el combo
  if (sala >= 0) {
    for(i = 0; i < arr_bloque_Cod[sala].length; i++) {
      // Crea OPTION del SELECT
      var option = new Option(arr_bloque_Text[sala][i],arr_bloque_Cod[sala][i]);
      document.fingresar.bloque1.options[i] = option;
      if (arr_bloque_Cod[sala][i] == "<%=selected_b1%>")
        document.fingresar.bloque1.options[i].selected = true;
    }
    for(i = 0; i < arr_bloque_Cod[sala].length; i++) {
      // Crea OPTION del SELECT
      var option = new Option(arr_bloque_Text[sala][i],arr_bloque_Cod[sala][i]);
      document.fingresar.bloque2.options[i] = option;
      if (arr_bloque_Cod[sala][i] == "<%=selected_b2%>")
        document.fingresar.bloque2.options[i].selected = true;
    }
    for(i = 0; i < arr_bloque_Cod[sala].length; i++) {
      // Crea OPTION del SELECT
      var option = new Option(arr_bloque_Text[sala][i],arr_bloque_Cod[sala][i]);
      document.fingresar.bloque3.options[i] = option;
  if (arr_bloque_Cod[sala][i] == "<%=selected_b3%>")
    document.fingresar.bloque3.options[i].selected = true;
    }
  }
  if (<%=usuario_gim%> == 2)
    {
    if (document.fingresar.sala1.value == 10335) {
    document.fingresar.tut_fecha_inicio1.value = '<%=fini_s1%>';
    document.fingresar.tut_fecha_termino1.value = '<%=fter_s1%>';
    }
    }
}
function CambiaBloque2(){
  var sala = 1;
// Limpia el combo
  document.fingresar.bloque4.options.length = 0;
  document.fingresar.bloque5.options.length = 0;
  document.fingresar.bloque6.options.length = 0;
// Ciclo que llena el combo
  if (sala >= 0) {
    for(i = 0; i < arr_bloque_Cod[sala].length; i++) {
      // Crea OPTION del SELECT
      var option = new Option(arr_bloque_Text[sala][i],arr_bloque_Cod[sala][i]);
      document.fingresar.bloque4.options[i] = option;
      if (arr_bloque_Cod[sala][i] == "<%=selected_b4%>")
        document.fingresar.bloque4.options[i].selected = true;
    }
    for(i = 0; i < arr_bloque_Cod[sala].length; i++) {
      // Crea OPTION del SELECT
      var option = new Option(arr_bloque_Text[sala][i],arr_bloque_Cod[sala][i]);
      document.fingresar.bloque5.options[i] = option;
      if (arr_bloque_Cod[sala][i] == "<%=selected_b5%>")
        document.fingresar.bloque5.options[i].selected = true;
    }
    for(i = 0; i < arr_bloque_Cod[sala].length; i++) {
      // Crea OPTION del SELECT
      var option = new Option(arr_bloque_Text[sala][i],arr_bloque_Cod[sala][i]);
      document.fingresar.bloque6.options[i] = option;
  if (arr_bloque_Cod[sala][i] == "<%=selected_b6%>")
    document.fingresar.bloque6.options[i].selected = true;
    }
  }
  if (<%=usuario_gim%> == 2)
    {
    if (document.fingresar.sala2.value == 10336) {
          document.fingresar.tut_fecha_inicio2.value = '<%=fini_s2%>';
          document.fingresar.tut_fecha_termino2.value = '<%=fter_s2%>';
    }
    }
}
function GuardarReserva(){
  var sala1 = document.fingresar.sala1.value;
  var sala2 = document.fingresar.sala2.value;
   if (sala1 > 0 || sala2 > 0) {
    if (sala1 == 10335) {

     document.fingresar.tut_a�o.value = <%=a�o_s1%>;
     document.fingresar.tut_semestre.value = <%=semestre_s1%>;
     document.fingresar.tut_pergim_id1.value = '<%=id_per_s1%>';
     document.fingresar.tut_fecha_inicio1.value = '<%=fini_s1%>';
     document.fingresar.tut_fecha_termino1.value = '<%=fter_s1%>';
    }
    if (sala2 == 10336) {
    if ((<%=a�o_s2%> == 0 || <%=semestre_s2%> == 0 || '<%=id_per_s2%>' == '0' ) &&
        (<%=usuario_gim%> == 2 ||  document.fingresar.usuario.value == 2 /*||
          document.fingresar.usuario.value == 6*/
        ))
      {
           alert ('No hay registrado per�odo para la tutor�a');
           return false;
      }
     document.fingresar.tut_a�o.value = <%=a�o_s2%>;
     document.fingresar.tut_semestre.value = <%=semestre_s2%>;
     document.fingresar.tut_pergim_id2.value = '<%=id_per_s2%>';
     document.fingresar.tut_fecha_inicio2.value = '<%=fini_s2%>';
     document.fingresar.tut_fecha_termino2.value = '<%=fter_s2%>';
    }

  } else {
       if ((<%=a�o_s1%> == 0 || <%=semestre_s1%> == 0 || '<%=id_per_s1%>' == '0' ||
         <%=a�o_s2%> == 0 || <%=semestre_s2%> == 0 || '<%=id_per_s2%>' == '0' ) &&
        (<%=usuario_gim%> == 2 || document.fingresar.usuario.value == 2 /*||
          document.fingresar.usuario.value == 6*/ ))
      {
           alert ('No hay registrado per�odo para la tutor�a');
           return false;
      }
  }
    document.fingresar.submit();
}
function changeImageSrc(whichImage,newGraphic) {
   if (document.images) {
       document.images[whichImage].src = newGraphic;
   }
}
function SinFoto(foto) {
   changeImageSrc(foto,"imagen/sinfoto.jpg");
}
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style></head>

<body onLoad="<%=onload%>">
<form name="form" method="post">
<input type="hidden" name="listado" value="3">
</form>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="37" class="Estilo_Verde">REGISTRO</td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="1" cellpadding="1">
  <tr>
   <form name="fingresar" method="post">
    <td width="995" valign="top" bgcolor="#FFFFFF">
        <table width="100%" height="0%"  border="0" cellpadding="1" cellspacing="1">
        <tr>
          <input type="hidden" name="accion" value="">
          <input type="hidden" name="id_registro" value="<%=id_registro%>">
          <td width="22%" height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">RUT</span></td>
          <td width="78%" height="20" bgcolor="#FFFFFF"><input type="text" name="rut_aux" size="13" maxlength="13" value ="<%=rut_aux%>" onKeyPress="enter(event,1);return noenter()" onChange="checkRutField(document.fingresar.rut_aux.value, document.fingresar);" class="Input01">
                <input type="hidden" name="rutnum" value="<%=rutnum%>">
                <input type="hidden" name="dvrut" value="<%=dvrut%>">
            <a href="#"  onClick="validar(document.fingresar);" class="AIngr"><img src="imagen/gim_lupa.gif" width="16" height="18" border="0" align="absmiddle"> Buscar</a> </td>

        </tr>
        <%if (rutnum>0){%>
        <tr bgcolor="#FFFFFF">
          <td height="20"><span class="Estilo_Gris">Nombre</span></td>
          <td height="20"><span class="Estilo_Azul"><%=nombre%></span></td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=etiqueta1%></span></td>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(!carrera.trim().equals("null"))?carrera:""%></span></td>
        </tr>
        <%if(origen == 0){%>
        <tr>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=etiqueta2%></span></td>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(!rol.trim().equals("null"))?rol:""%></span></td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=etiqueta3%></span></td>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(!email.trim().equals("null"))?email:""%></span></td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=etiqueta4%></span></td>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=situacion%> </span></td>
        </tr>
        <% }
          if(vec_funcionario.size()> 0 && !habilitado){
          if(origen == 1) {%>
        <tr>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Contrato</span></td>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=contrato%></span></td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Planta</span></td>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=planta%></span></td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Jerarqu�a</span></td>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=jerarquia%> </span></td>
        </tr>  <tr>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Cargo</span></td>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=cargo%> </span></td>
        </tr>
        <%} else  { %>
          <tr>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Planta</span></td>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=planta%> </span></td>
        </tr>
        <%}}%>
        <%if(habilitado || vec_funcionario.size()>0) {%>
        <tr>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Usuario</span></td>
          <td height="20" bgcolor="#FFFFFF">
          <select name="usuario" class="Option" onchange="Cambiar();"
            <%=(soloconsulta && (usuario_gim == 2 || usuario_gim == 3 || usuario_gim == 6))?"disabled":""%>>
              <option value="0">--Seleccione --</option>
              <%if(!esvi�a){%>
              <%if(esalumno && habilitado){%>
              <option value="1" <%=(usuario_gim==1)?"selected":""%>>Alumno</option>
              <%if (!soloconsulta /*&& usuario_gim == 0*/){%>
              <option value="2" <%=(usuario_gim==2)?"selected":""%>>Tutor&iacute;a</option>
              <option value="3" <%=(usuario_gim==3)?"selected":""%>>Rama Deportiva</option>
              <%}} if (vec_funcionario.size()>0) {%>
              <option value="4" <%=(usuario_gim==4)?"selected":""%>>Funcionario</option>
              <%}%>
              <%} else {%>
              <%if(esalumno && habilitado){%>
              <option value="5" <%=(usuario_gim==5)?"selected":""%>>Alumno</option>
              <%if (!soloconsulta && usuario_gim == 0){%>
              <option value="6" <%=(usuario_gim==6)?"selected":""%>>Rama Deportiva</option>
              <%}} if (vec_funcionario.size()>0) {%>
              <option value="7" <%=(usuario_gim==7)?"selected":""%>>Funcionario</option>
              <%}%>
              <%}%>
          </select></td>
        </tr>
        <%}%>
         <%if(esalumno ){%>
        <tr id="bloque_rama">
          <%if (vec_ramadep.size()>0) {%>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Rama</span></td>
          <td height="20" bgcolor="#FFFFFF"><select name="rama" class="Option" <%=disable%>>
              <option value="0">--Seleccione --</option>
               <% for (int i=0;i < vec_ramadep.size();i++){
                  String id = ((Vector) vec_ramadep.get(i)).get(4)+ "";
                %>
              <option value="<%=id%>" <%=(id.trim().equals(id_rama.trim()))?"selected":""%>><%=((Vector) vec_ramadep.get(i)).get(1)+ ""%> - <%=((Vector) vec_ramadep.get(i)).get(3)+ ""%></option>
               <%}%>
             </select></td>
             <%}%>
        </tr>
        <%}%>

        <tr id="bloque_tutoria0" Style = "display:none">
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Sala 1</span></td>
          <td height="20" bgcolor="#FFFFFF"> M&aacute;quinas
              <input type="hidden" name="sala1" value="10335" >
        </td>
        </tr>
        <tr id="bloque_tutoria1" Style = "display:none">
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Reserva 1 </span></td>
          <td height="20" bgcolor="#FFFFFF">
          <select name="bloque1" class="Option" id="bloque1" <%=disable%>></select>
          </td>
        </tr>
        <tr id="bloque_tutoria2" Style = "display:none">
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Reserva 2</span></td>
          <td height="20" bgcolor="#FFFFFF">
          <select name="bloque2" class="Option" id="bloque2" <%=disable%>></select>
         <input type="hidden" name="tut_a�o" value="">
         <input type="hidden" name="tut_semestre" value="">
         <input type="hidden" name="tut_pergim_id1" value="">
         <input type="hidden" name="tut_fecha_inicio1" value="">
         <input type="hidden" name="tut_fecha_termino1" value="">
         </td>
        </tr>
        <tr id="bloque_tutoria3" Style = "display:none">
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Reserva 3 </span></td>
          <td height="20" bgcolor="#FFFFFF">
          <select name="bloque3" class="Option" id="bloque3" <%=disable%>></select>
          </td>
        </tr>
       <tr id="bloque_tutoria4" Style = "display:none">
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Sala 2</span></td>
          <td height="20" bgcolor="#FFFFFF">  Multiuso
            <input type="hidden" name="sala2" value="10336" >
         </td>
        </tr>
        <tr id="bloque_tutoria5" Style = "display:none">
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Reserva 1 </span></td>
          <td height="20" bgcolor="#FFFFFF">
          <select name="bloque4" class="Option" id="bloque4" <%=disable%>></select>
          </td>
        </tr>
        <tr id="bloque_tutoria6" Style = "display:none">
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Reserva 2</span></td>
          <td height="20" bgcolor="#FFFFFF">
          <select name="bloque5" class="Option" id="bloque5" <%=disable%>></select>
         <input type="hidden" name="tut_pergim_id2" value="">
         <input type="hidden" name="tut_fecha_inicio2" value="">
         <input type="hidden" name="tut_fecha_termino2" value="">
         </td>
        </tr>
        <tr id="bloque_tutoria7" Style = "display:none">
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Reserva 3 </span></td>
          <td height="20" bgcolor="#FFFFFF">
          <select name="bloque6" class="Option" id="bloque6" <%=disable%>></select>
          </td>
        </tr>

        <%if(vec_registro.size()>0) {
        String modo = (rutnum==rut_usuario)?", V�A DIRECTA":", por "+vec_registro.get(3)+ "";
        %>
        <tr>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_Gris">Ingresado el </span></td>
          <td height="20" bgcolor="#FFFFFF"><span class="Estilo_MoradoClaro"><%=vec_registro.get(0)+ ""%><%=modo%> </span></td>
        </tr>
        <%}}%>
    </table>
    </td>
    <% rut_foto = rutnum+""; 
    %>
    <td width="122" align="center" valign="top" bgcolor="#FFFFFF">
    <% if (rutnum == 0){%>&nbsp;
    <%} else { %>
    <img name="foto" src="foto?f=<%=rutnum%>"
align="absmiddle" onError="SinFoto('foto');" width="110" height="115" border="1">
    <%}%>
    </td>
      <input type="hidden" name="sexo" value="<%=sexo%>">
</form>
</tr>
</table>

<table width="100%"  border="0" cellspacing="0" cellpadding="3">

  <tr><%if(habilitado || vec_funcionario.size()>0) {%>
      <%if (( (esalumno && habilitado) || vec_registro.size()==0 || vec_funcionario.size()>0) && rutnum > 0 ) {
        if (soloconsulta && (usuario_gim == 2 || usuario_gim == 3 || usuario_gim == 6)) { } else {%>
    <td width="9%" height="35"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseOK.gif">
      <tr onClick="Grabar(<%=vec_registro.size()%>);" onMouseOver="BotonOver(this,'<%=(vec_registro.size()>0)?"Modificar":"Agregar"%>')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino"><%if(vec_registro.size()>0) { %>Modificar <%} else {%>Agregar<%}%></td>
      </tr>
    </table></td><%}}%>
    <%if(vec_registro.size()>0 ) {
if (soloconsulta && (usuario_gim == 2 || usuario_gim == 3 || usuario_gim == 6)) {} else {%>
    <td width="9%"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseELIMINAR.gif">
      <tr onClick="Eliminar();" onMouseOver="BotonOver(this,'Eliminar')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino">Eliminar</td>
      </tr>
    </table></td><%}}%><%}%>
    <td width="25%">
     <table height="21"  border="0" cellpadding="0" cellspacing="0">
      <tr onClick="AbrirVentana(document.form,'gim_genera_list.jsp',600,700)" onMouseOver="BotonOver(this,'Listar Usuarios Registrados')" onMouseOut="BotonOut(this)">
          <td width="21"><img src="imagen/boton_baseLEER01.gif" width="23" height="21"></td>
          <td width="92" align="center" valign="middle" nowrap background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">Ver Registros</td>
          <td width="3"><img src="imagen/boton_baseBORDEDER.gif" width="3" height="21"></td>
        </tr>
    </table>
	</td>
	<% if (rutnum > 0 && (habilitado || vec_funcionario.size()>0)) {%>
	<td align="right">
     <table height="21"  border="0" cellpadding="0" cellspacing="0">
      <tr onClick="AbrirVentana(document.form,'gim_asistencia.jsp?consulta=<%=rutnum%>', 400, 800)" onMouseOver="BotonOver(this,'Ver Reservas y Asistencias')" onMouseOut="BotonOut(this)">
          <td width="21"><img src="imagen/boton_baseLEER01.gif" width="23" height="21"></td>
          <td width="92" align="center" valign="middle" nowrap background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">Ver Reservas y Asistencias</td>
          <td width="3"><img src="imagen/boton_baseBORDEDER.gif" width="3" height="21"></td>
        </tr>
    </table>
	</td>
	<%} %>
  </tr>
</table>

</body>
</html>
