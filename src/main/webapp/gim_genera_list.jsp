<%@ page language="java" import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 18/06/2007
   // �ltima Actualizaci�n : 09/08/2007 agregar a�o-semestre del periodo semestre actual
%>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>

<%
  response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-store");
  int rut_usuario = session.getAttribute("rut_usuario") != null?
                     Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int listado   = request.getParameter("listado")!=null &&
                 !request.getParameter("listado").equals("")?
                  Integer.parseInt(request.getParameter("listado")):0;
  int rut   = request.getParameter("rut")!=null &&
                !request.getParameter("rut").equals("")?
                  Integer.parseInt(request.getParameter("rut")):0;
  int perfil   = request.getParameter("perfil")!=null &&
              !request.getParameter("perfil").equals("")?
                  Integer.parseInt(request.getParameter("perfil")):0;

  String nom_pagina = "";
  String tama�o     = "430";
  String titulo     = "";
  String subtitulo  = "";
  java.util.Calendar f = new java.util.GregorianCalendar();
  int a�oActual = f.get(java.util.Calendar.YEAR);
  general.getAbrirConexion();
  general.setGenerarConexionHorario();
  general.getPeriodoSemestreActual(1,1);
  int a�o = general.getA�oActual();
  int sem = general.getSemestreActual();
  general.getCerrarConexion();

  // FICHA
  if (listado == 1) {
      titulo     = "FICHA DEL ALUMNO";
      nom_pagina = "gim_ficha_adm.jsp?rut="+rut+"&perfil="+perfil;
  }
  // LISTA DE ASISTENCIA
  if (listado == 2) {
      nom_pagina = "gim_asistencia_list.jsp";
  }
  // LISTA DE USUARIOS REGISTRADOS
  if (listado == 3) {
      subtitulo  = "Generar listado";
      nom_pagina = "gim_registro_list.jsp";
  }
  // LISTA DE HORARIO
  if (listado == 4) {
    int   anno =  request.getParameter("anno")!=null &&
                  !request.getParameter("anno").equals("null") &&
                  !request.getParameter("anno").equals("NULL") &&
                  !request.getParameter("anno").equals("")  &&
                  !request.getParameter("anno").equals(" ")
               ?Integer.parseInt(request.getParameter("anno")+""):0;
    int   semestre =  request.getParameter("semestre")!=null &&
                      !request.getParameter("semestre").equals("null") &&
                      !request.getParameter("semestre").equals("NULL") &&
                      !request.getParameter("semestre").equals("")  &&
                      !request.getParameter("semestre").equals(" ")
                   ?Integer.parseInt(request.getParameter("semestre")+""):0;
    int   sala =  request.getParameter("sala")!=null &&
                  !request.getParameter("sala").equals("null") &&
                  !request.getParameter("sala").equals("NULL") &&
                  !request.getParameter("sala").equals("")  &&
                  !request.getParameter("sala").equals(" ")
               ?Integer.parseInt(request.getParameter("sala")+""):0;

      nom_pagina = "gim_horario_list.jsp?anno="+anno+"&semestre="+semestre+"&sala="+sala;
  }


  // LISTA DE premiados o sancionados
  if (listado == 5) {
    int anno   = request.getParameter("anno")!=null &&
                 !request.getParameter("anno").equals("")?
                 Integer.parseInt(request.getParameter("anno")):0;
    int semestre = request.getParameter("semestre")!=null &&
                !request.getParameter("semestre").equals("")?
                 Integer.parseInt(request.getParameter("semestre")):0;
    int tipo   = request.getParameter("tipo")!=null &&
                 !request.getParameter("tipo").equals("")?
                 Integer.parseInt(request.getParameter("tipo")):0;
    String id   = request.getParameter("id")!=null &&
                 !request.getParameter("id").equals("")?
                 request.getParameter("id"):"0";
    nom_pagina = "gim_prem_sancionado_list.jsp?anno="+anno+"&semestre="+semestre+"&estado="+tipo+"&id="+id;
  }
  // Documento de Condiciones de Uso
  if (listado == 6) {
    nom_pagina = "gim_documentacion.html";
    titulo = "Condiciones de Uso";

  }
  if (listado == 7) { // lista de curso
    int anno   = request.getParameter("a�o")!=null &&
                 !request.getParameter("a�o").equals("")?
                 Integer.parseInt(request.getParameter("a�o")):0;
    int semestre = request.getParameter("semestre")!=null &&
                !request.getParameter("semestre").equals("")?
                 Integer.parseInt(request.getParameter("semestre")):0;
    int cod_asign = request.getParameter("cod_asign")!=null &&
              !request.getParameter("cod_asign").equals("")?
                 Integer.parseInt(request.getParameter("cod_asign")):0;
    int paralelo = request.getParameter("paralelo")!=null &&
           !request.getParameter("paralelo").equals("")?
                 Integer.parseInt(request.getParameter("paralelo")):0;
    nom_pagina = "gim_lista_curso_list.jsp?anno="+ a�o+"&semestre="+ semestre +"&cod_asign="+cod_asign+"&paralelo="+paralelo;
    titulo = "Lista de Curso";

  }
  // Documento de Reglamento sala de m�quinas
if (listado == 8) {
  nom_pagina = "gim_Reglamento_SM.html";
  titulo = "Reglamento Sala de M�quinas";

  }
  // Documento de Reglamento aer�bica
if (listado == 9) {
nom_pagina = "gim_Reglamento_SA.html";
titulo = "Reglamento Sala de Aer�bica";

  }
  // Listado de premiados sin reserva para per�odo siguiente
if (listado == 10) {
nom_pagina = "gim_premiados_s_reserva_list.jsp";
titulo = "Listado de Premiados sin reserva para el siguiente per�odo.";

  }
%>
<html>
<head>
<LINK href="css/estilo.css" rel=stylesheet type=text/css>
<title>Ventana de impresi&oacute;n</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<SCRIPT LANGUAGE="JavaScript">
  <!-- Begin
  var da = (document.all) ? 1 : 0;
  var pr = (window.print) ? 1 : 0;
  var mac = (navigator.userAgent.indexOf("Mac") != -1);

  function printPage(frame, arg) {
    if (frame == window) {
      printThis();
    } else {
      link = arg; // a global variable
      printFrame(frame);
    }
    return false;
  }

  function printThis() {
    if (pr) { // NS4, IE5
      window.print();
    } else if (da && !mac) { // IE4 (Windows)
      vbPrintPage();
    } else { // otros browsers
      alert("Su Browser no soporta esta caracter�stica");
    }
  }

  function printFrame(frame) {
    if (pr && da) { // IE5
      frame.focus();
      window.print();
      link.focus();
    } else if (pr) { // NS4
      frame.print();
    } else if (da && !mac) { // IE4 (Windows)
      frame.focus();
      setTimeout("vbPrintPage(); link.focus();", 100);
    } else { // otros browsers
      alert("Su Browser no soporta esta caracter�stica");
    }
  }

  if (da && !pr && !mac) with (document) {
    writeln('<OBJECT ID="WB" WIDTH="0" HEIGHT="0" CLASSID="clsid:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>');
    writeln('<' + 'SCRIPT LANGUAGE="VBScript">');
    writeln('Sub window_onunload');
    writeln('  On Error Resume Next');
    writeln('  Set WB = nothing');
    writeln('End Sub');
    writeln('Sub vbPrintPage');
    writeln('  OLECMDID_PRINT = 6');
    writeln('  OLECMDEXECOPT_DONTPROMPTUSER = 2');
    writeln('  OLECMDEXECOPT_PROMPTUSER = 1');
    writeln('  On Error Resume Next');
    writeln('  WB.ExecWB OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER');
    writeln('End Sub');
    writeln('<' + '/SCRIPT>');
  }

function Cerrar() {
  parent.self.close();
}
// End -->
</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<center>
<% if (!titulo.equals("")) {%>
<div align="center"><span class="letraAzul"><%=titulo%></span></div>
<br>
<%}%>

<%
 // LISTA DE USUARIOS REGISTRADOS
 if (listado == 3) {%>
<table width="600" border="0" cellspacing="0" cellpadding="1">
  <tr valign="top">
    <td height="20" colspan="2" class="letra8"><span class="letraAzul"><%=subtitulo%></span></td>
  </tr>
  <tr>
    <td height="20" colspan="2" class="letra8">Seleccione el Per&iacute;odo de registro y el tipo de usuario:</td>
    </tr>
  <form name="fingresar" method="post" action="<%=nom_pagina%>" target="cuerpo">
  <tr>
    <td width="13%"><span class="letra8 Estilo1">Per&iacute;odo</span></td>
    <td width="87%"><select name="anno" class="Option" id="a�o" onChange="document.fingresar.submit();">
        <%for (int i=2007;i<=a�oActual;i++){%>
        <option value="<%=i%>" <%=(i==a�o)?"selected":""%>><%=i%></option>
		<%}%>
      </select>
      -
      <select name="semestre" class="Option" id="semestre" onChange="document.fingresar.submit();">
        <option value="1" <%=(1==sem)?"selected":""%>>1</option>
        <option value="2" <%=(2==sem)?"selected":""%>>2</option>
        <option value="3" <%=(3==sem)?"selected":""%>>3</option>
                        </select></td>
  </tr>
  <tr>
    <td><span class="letra8 Estilo1">Usuario</span></td>
    <td><select name="tipo_usuario" class="Option" onChange="document.fingresar.submit();">
      <option value="0" selected>-- Seleccione --</option>
      <option value="1">Alumno</option>
      <option value="2" >Tutor&iacute;a</option>
      <option value="3">Rama Deportiva</option>
      <option value="4">Funcionario</option>
      </select></td>
  </tr>
  </form>
</table>
<%}%>
<br>
<iframe name="cuerpo" src="<%=nom_pagina%>" scrolling="auto" width="600" height="<%=tama�o%>" border="0" frameborder="1"></iframe>

<table width="600"  border="0" cellpadding="3" cellspacing="0">
  <tr>
    <td width="9%" height="35" align="right"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseCERRAR.gif">
        <tr onClick="Cerrar()"  onMouseOver="BotonOver(this,'Registrar')" onMouseOut="BotonOut(this)">
          <td width="32">&nbsp;</td>
          <td width="59" class="Estilo_Azulino letra8">Cerrar</td>
        </tr>
    </table></td>
    <td width="9%" height="40"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseIMPRIMIR.gif">
        <tr onClick="return printPage(parent.cuerpo, this)" onMouseOver="BotonOver(this,'Imprimir')" onMouseOut="BotonOut(this)">
          <td width="29">&nbsp;</td>
          <td width="62" class="Estilo_Azulino letra8">Imprimir</td>
        </tr>
    </table></td>
    </tr>
</table>
<p>&nbsp;</p>
</center>
</body>
</html>
