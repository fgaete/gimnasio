<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*,java.text.*"%>
<%@ page import="gimnasio.*"%>
<%!private static final DecimalFormat FMT_dec = new DecimalFormat("#0.00");%>

<%
// Creado por           : M�nica Barrera Frez
// Fecha                : 23/07/2008
// �ltima Actualizaci�n :
// Registro listado.    Estad�stica por uso seg�n rango de fecha .
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>


<%response.setHeader("Pragma","no-cache");
response.setDateHeader ("Expires", 0);
response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
 int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
String fecha_desde    = request.getParameter("fecha_desde")!=null &&
                        !request.getParameter("fecha_desde").equals("null") &&
                        !request.getParameter("fecha_desde").equals("NULL") &&
                        !request.getParameter("fecha_desde").equals("")  &&
                        !request.getParameter("fecha_desde").equals(" ")?
                        request.getParameter("fecha_desde")+"":"";
String fecha_hasta  = request.getParameter("fecha_hasta")!=null &&
                        !request.getParameter("fecha_hasta").equals("null") &&
                        !request.getParameter("fecha_hasta").equals("NULL") &&
                        !request.getParameter("fecha_hasta").equals("")  &&
                        !request.getParameter("fecha_hasta").equals(" ")?
                        request.getParameter("fecha_hasta")+"":"";
int   version =  request.getParameter("version")!=null &&
                        !request.getParameter("version").equals("null") &&
                        !request.getParameter("version").equals("NULL") &&
                        !request.getParameter("version").equals("")  &&
                        !request.getParameter("version").equals(" ")
                        ?Integer.parseInt(request.getParameter("version")+""):0;
int   sala =  request.getParameter("sala")!=null &&
                        !request.getParameter("sala").equals("null") &&
                        !request.getParameter("sala").equals("NULL") &&
                        !request.getParameter("sala").equals("")  &&
                        !request.getParameter("sala").equals(" ")
                        ?Integer.parseInt(request.getParameter("sala")+""):0;
int anno             = request.getParameter("anno")!=null &&
                        !request.getParameter("anno").equals("null") &&
                        !request.getParameter("anno").equals("NULL") &&
                        !request.getParameter("anno").equals("")  &&
                        !request.getParameter("anno").equals(" ")?
                        Integer.parseInt(request.getParameter("anno")+""):0;
int semestre         = request.getParameter("semestre")!=null &&
                        !request.getParameter("semestre").equals("null") &&
                        !request.getParameter("semestre").equals("NULL") &&
                        !request.getParameter("semestre").equals("")  &&
                        !request.getParameter("semestre").equals(" ")?
                        Integer.parseInt(request.getParameter("semestre")+""):0;

//version = 0;

Vector vec_dia = new Vector();
Vector vec_asistentes = new Vector();
Vector vec_asist_detalle = new Vector();
Vector vec_total = new Vector();
Vector vec_total_f = new Vector();
Vector vec_fecha = new Vector();
Vector vec_usuario = new Vector();

String nom_dia = "";
String nom_dia_ant = "";
String asign_ant = "";
String asign = "";
String fecha_inicio = "";
String fecha_termino = "";
String nom_usuario = "";
String nom_usuario_ant = "";

int num_dia = 0;
int num_dia_ant = 0;
int cupo_actividad = 0;
int dia = 0;
int fecha = 0;
int sum_masc = 0;
int sum_fem = 0;
int total_fem = 0;
int total_masc = 0;
int colspan = 1;

general.getAbrirConexion();
general.setGenerarConexionHorario();
general.setGenerarConexionReserva();
String m  = (Integer.parseInt(fecha_desde.substring(3,5)) < 10)?
("0"+Integer.parseInt(fecha_desde.substring(3,5))):
(""+Integer.parseInt(fecha_desde.substring(3,5)));
String dd = (Integer.parseInt(fecha_desde.substring(0,2)) < 10)?
("0"+Integer.parseInt(fecha_desde.substring(0,2))):
(""+Integer.parseInt(fecha_desde.substring(0,2)));
String a  = (Integer.parseInt(fecha_desde.substring(6,10)) < 10)?
("0"+Integer.parseInt(fecha_desde.substring(6,10))):
(""+Integer.parseInt(fecha_desde.substring(6,10)));
String   f1 = a + m + dd;
m  = (Integer.parseInt(fecha_hasta.substring(3,5)) < 10)?
("0"+Integer.parseInt(fecha_hasta.substring(3,5))):
(""+Integer.parseInt(fecha_hasta.substring(3,5)));
dd = (Integer.parseInt(fecha_hasta.substring(0,2)) < 10)?
("0"+Integer.parseInt(fecha_hasta.substring(0,2))):
(""+Integer.parseInt(fecha_hasta.substring(0,2)));
a  = (Integer.parseInt(fecha_hasta.substring(6,10)) < 10)?
("0"+Integer.parseInt(fecha_hasta.substring(6,10))):
(""+Integer.parseInt(fecha_hasta.substring(6,10)));
String   f2 = a + m + dd;

boolean aseo = false;
%>
<html>
<head>
<title>Gimnasio - Estad&iacute;sitica</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
</head>

<body>

<%String nom_sala = "";
boolean salir = true;
int cupo = 0;
int sw=0;


if (version == 0){
  while (salir) {
    vec_dia = new Vector();
    vec_asistentes = new Vector();
    vec_asist_detalle = new Vector();
    vec_total = new Vector();
    vec_total_f = new Vector();
    vec_fecha = new Vector();
    nom_dia = "";
    nom_dia_ant = "";
    asign_ant = "";
    asign = "";
    fecha_inicio = "";
    fecha_termino = "";

    num_dia = 0;
    num_dia_ant = 0;
    dia = 0;
    fecha = 0;

    sum_masc = 0;
    sum_fem = 0;
    total_fem = 0;
    total_masc = 0;
    colspan = 1;
    if ((sala == 3 || sala == 10335) && sw==0){
      cupo = general.getCupoGimnasio(anno, semestre, 0, 0, 0,10335,0);
      vec_asistentes = general.getPromedioSemana(anno, semestre, f1, f2,10335 , version);
      if (sala == 10335) salir = false;
      nom_sala = "SALA DE M�QUINAS Y PESAS";
      sw = 1;
    } else sw = 0;
  if ((sala == 3 || sala == 10336) && sw==0 ){
    if(!salir) salir = true;
    cupo=-1;
    vec_asistentes = general.getPromedioSemana(anno, semestre, f1, f2,10336 , version);
    vec_asist_detalle = general.getPromedioSemana(anno, semestre, f1, f2,10336 , version);
    salir = false;
    nom_sala = "SALA DE AER�BICA";
  }

for(int i=0;i<vec_asistentes.size();i++){
  fecha_inicio = ((Vector)vec_asistentes.get(i)).get(2)+"";
  fecha_termino = ((Vector)vec_asistentes.get(i)).get(3)+"";
  fecha = Integer.parseInt(((Vector)vec_asistentes.get(i)).get(6)+"");
  if (!vec_dia.contains(String.valueOf(fecha))) {
    vec_fecha.addElement(fecha_inicio + " al "+ fecha_termino);
    vec_dia.addElement(String.valueOf(fecha));
    vec_total.addElement("0");
  }}
  vec_total.addElement("0");

  colspan = (vec_dia.size()>0)?vec_dia.size():1;


  %>
  <table width="100%"  border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td width="100%" height="20" align="center" class="letra10">PROMEDIO POR BLOQUE <%=anno%>-<%=semestre%> </td>
    </tr>
    <tr>
      <td height="20" align="center" valign="top"><%=fecha_desde%> al <%=fecha_hasta%></td>
    </tr>
  </table>
  <br>
  <table width="100%"  border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td width="100%" height="20" valign="bottom"><font  class="letra10"><%=nom_sala%></font><br>
        <%=(cupo >= 0)?"Cupo " + cupo:""%></td>
    </tr>
  </table>
  <br>
  <table width="100%"  border="1" cellpadding="2" cellspacing="0">
    <tr valign="bottom" bgcolor="#FFFFFF">
      <td rowspan="2" bgcolor="#CCCCCC"><strong>D&iacute;a</strong></td>
      <td height="11%" colspan="<%=colspan%>" align="center" bgcolor="#CCCCCC"><strong>Semana</strong></td>
      <td rowspan="2" align="center" bgcolor="#CCCCCC"><strong>Total</strong></td>
    </tr>
    <tr valign="bottom" bgcolor="#FFFFFF">
      <%for(int i=0;i<vec_fecha.size();i++){%>
        <td align="center" bgcolor="#CCCCCC" class="letra7"><strong><%=vec_fecha.get(i)%></strong></td>
        <% } %>
    </tr>
    <tr bgcolor="#FFFFFF">
      <%int j=0;
      dia = 1;
      while (dia < 7) {
        total_masc = 0;
        for(int i=0;i<vec_asistentes.size();i++){
          num_dia = Integer.parseInt(((Vector)vec_asistentes.get(i)).get(1)+"");
          fecha_inicio = ((Vector)vec_asistentes.get(i)).get(2)+"";
          sum_masc = Integer.parseInt(((Vector)vec_asistentes.get(i)).get(4)+"");
          fecha = Integer.parseInt(((Vector)vec_asistentes.get(i)).get(6)+"");
          if (num_dia == dia){
            if (num_dia != num_dia_ant){
              nom_dia = ((Vector)vec_asistentes.get(i)).get(0)+"";
              %>
              <td width="18%" valign="top"><%=nom_dia%></td>
              <%j=0; %>
              <%total_masc = sum_masc;
              } else
              total_masc += sum_masc;

              while (fecha < Integer.parseInt(vec_dia.get(j)+"")){ %>
              <td width="6%" align="center">&nbsp;</td>
              <%j++;
              } %>
              <td width="6%" align="center"><%=sum_masc%></td>
              <%num_dia_ant = num_dia;
              if (fecha == Integer.parseInt(vec_dia.get(vec_dia.size()-1)+"")){%>
              <td align="center" valign="top"><%=total_masc %></td>
              <%int  t = total_masc + Integer.parseInt(vec_total.get(vec_total.size()-1)+"");
              vec_total.set(vec_total.size()-1,t+"");
            } int t = sum_masc + Integer.parseInt(vec_total.get(j)+"");
            vec_total.set(j,t+"");

            j++; }%>
            <%
            } %>
            </tr>
            <%
            dia++;
          } %>
          <tr bgcolor="#CCCCCC">
            <td valign="top"><strong>Total</strong></td>
            <%for (int i=0;i<vec_total.size();i++){ %>
              <td align="center" valign="top"><%=vec_total.get(i) %></td>
              <%} %>

          </tr>
  </table>
  <br>
 <% }}%>
 <%salir = true;
  if (version == 1) {
    while (salir) {
    vec_dia = new Vector();
    vec_asistentes = new Vector();
    vec_asist_detalle = new Vector();
    vec_total = new Vector();
    vec_total_f = new Vector();
    vec_fecha = new Vector();
    nom_dia = "";
    nom_dia_ant = "";
    asign_ant = "";
    asign = "";
    fecha_inicio = "";
    fecha_termino = "";

    num_dia = 0;
    num_dia_ant = 0;
    dia = 0;
    fecha = 0;

    sum_masc = 0;
    sum_fem = 0;
    total_fem = 0;
    total_masc = 0;
    colspan = 1;
    if ((sala == 3 || sala == 10335) && sw==0){
      cupo = general.getCupoGimnasio(anno, semestre, 0, 0, 0,10335,0);
      vec_asistentes = general.getPromedioSemana(anno, semestre, f1, f2,10335 , version);
      if (sala == 10335) salir = false;
      nom_sala = "SALA DE M�QUINAS Y PESAS";

      sw = 1;
    } else sw = 0;
  if ((sala == 3 || sala == 10336) && sw==0 ){
    if(!salir) salir = true;
    cupo=-1;
    vec_asistentes = general.getPromedioSemana(anno, semestre, f1, f2,10336 , version);
    vec_asist_detalle = general.getPromedioSemana(anno, semestre, f1, f2,10336 , version);
    salir = false;
    nom_sala = "SALA DE AER�BICA";
  }

for(int i=0;i<vec_asistentes.size();i++){
  fecha_inicio = ((Vector)vec_asistentes.get(i)).get(2)+"";
  fecha_termino = ((Vector)vec_asistentes.get(i)).get(3)+"";
  fecha = Integer.parseInt(((Vector)vec_asistentes.get(i)).get(6)+"");
  if (!vec_dia.contains(String.valueOf(fecha))) {
    vec_fecha.addElement(fecha_inicio + " al "+ fecha_termino);
    vec_dia.addElement(String.valueOf(fecha));
    vec_total.addElement("0");
    vec_total_f.addElement("0");
  }
  if(!vec_usuario.contains(((Vector)vec_asistentes.get(i)).get(7)+"")){
    vec_usuario.addElement(((Vector)vec_asistentes.get(i)).get(7)+"");
  }
}
  vec_total.addElement("0");
  vec_total_f.addElement("0");

  colspan = (vec_dia.size()>0)?(vec_dia.size()*2):2;
   
       %>
  <table width="100%"  border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td width="100%" height="20" align="center" class="letra10">PROMEDIO POR BLOQUE <%=anno%>-<%=semestre%> </td>
    </tr>
    <tr> 
    	
      <td height="20" align="center" valign="top"><%=fecha_desde%> al <%=fecha_hasta%></td>
    </tr>
    
  </table>
  <br>
  <table width="100%"  border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td width="100%" height="20" valign="bottom"><font  class="letra10"><%=nom_sala%></font><br>
        <%=(cupo >= 0)?"Cupo " + cupo:""%></td>
    </tr>
  </table>
<br>
<table width="100%"  border="1" cellpadding="2" cellspacing="0">
  <tr valign="bottom" bgcolor="#FFFFFF">
    <td rowspan="3" bgcolor="#CCCCCC"><strong>Usuarios</strong></td>
    <td rowspan="3" bgcolor="#CCCCCC"><strong>D&iacute;a</strong></td>
    <td height="11%" colspan="<%=colspan%>" align="center" bgcolor="#CCCCCC"><strong>Semana</strong></td>
    <td colspan="2" rowspan="2" align="center" bgcolor="#CCCCCC"><strong>Total</strong></td>
    <td rowspan="3" align="center" bgcolor="#CCCCCC"><strong>Total</strong></td>
  </tr>
  <tr valign="bottom" bgcolor="#FFFFFF">
    <%for(int i=0;i<vec_fecha.size();i++){%>
        <td align="center" colspan="2" bgcolor="#CCCCCC" class="letra7"><strong><%=vec_fecha.get(i)%></strong></td>
        <% } %>
   </tr>
  <tr bgcolor="#CCCCCC">
   <%for(int i=0;i<vec_fecha.size();i++){%>
     <td width="8%" align="center">M</td>
     <td width="8%" align="center">F</td>
     <%} %>
     <td width="8%" align="center">M</td>
     <td width="8%" align="center">F</td>
  </tr>
    <%   for (int k=0;k<vec_usuario.size();k++){
      boolean pasa = false;

   int j=0;%>
  <tr bgcolor="#FFFFFF">

  <%dia = 1;
      while (dia < 7) {
        total_masc = 0;

        for(int i=0;i<vec_asistentes.size();i++){
          nom_usuario = ((Vector)vec_asistentes.get(i)).get(7)+"";
          num_dia = Integer.parseInt(((Vector)vec_asistentes.get(i)).get(1)+"");
          fecha_inicio = ((Vector)vec_asistentes.get(i)).get(2)+"";
          sum_masc = Integer.parseInt(((Vector)vec_asistentes.get(i)).get(4)+"");
          sum_fem = Integer.parseInt(((Vector)vec_asistentes.get(i)).get(5)+"");
          fecha = Integer.parseInt(((Vector)vec_asistentes.get(i)).get(6)+"");

          if (num_dia == dia && nom_usuario.trim().equals(vec_usuario.get(k)+"".trim())){
             if (!nom_usuario.trim().equals(nom_usuario_ant.trim())){%>
          <td width="11%" valign="top"><%=nom_usuario %></td>
          <%num_dia_ant = 0;
          pasa = true;
            }
           if(!pasa){ %>
               <td width="11%" align="center">&nbsp;</td>
             <%}if (num_dia != num_dia_ant){
              nom_dia = ((Vector)vec_asistentes.get(i)).get(0)+"";%>

              <td width="18%" valign="top"><%=nom_dia%></td>
              <%j=0; %>
              <%total_masc = sum_masc;
                total_fem = sum_fem;
                pasa = true;
              } else {
              total_masc += sum_masc;
              total_fem += sum_fem;
              }
              while (fecha < Integer.parseInt(vec_dia.get(j)+"")){ %>
              <td width="6%" align="center">&nbsp;</td>
              <td width="6%" align="center">&nbsp;</td>
              <%j++;
              } %>
              <td width="6%" align="center"><%=sum_masc%></td>
              <td width="6%" align="center"><%=sum_fem%></td>
              <%num_dia_ant = num_dia;
               nom_usuario_ant = nom_usuario ;
              if (fecha == Integer.parseInt(vec_dia.get(vec_dia.size()-1)+"")){%>
              <td align="center" valign="top"><%=total_masc %></td>
              <td align="center" valign="top"><%=total_fem %></td>
              <td align="center" valign="top"><%=total_fem+total_masc %></td>
              <%int  t = total_masc + Integer.parseInt(vec_total.get(vec_total.size()-1)+"");
              vec_total.set(vec_total.size()-1,t+"");
              t = total_fem + Integer.parseInt(vec_total_f.get(vec_total_f.size()-1)+"");
              vec_total_f.set(vec_total.size()-1,t+"");
              pasa = false;
            }
            int t = sum_masc + Integer.parseInt(vec_total.get(j)+"");
            vec_total.set(j,t+"");
             t = sum_fem + Integer.parseInt(vec_total_f.get(j)+"");
              vec_total_f.set(j,t+"");

            j++; } %>
            <%
            } %>
            </tr>
            <%
            dia++;
          } %>

          <tr bgcolor="#CCCCCC">
            <td width="11%" align="center">&nbsp;</td>
            <td valign="top"><strong>Total</strong></td>
            <%int suma_total = 0;
               for (int i=0;i<vec_total.size();i++){ %>
              <td align="center" valign="top"><%=vec_total.get(i) %></td>
              <td align="center" valign="top"><%=vec_total_f.get(i) %></td>
              <%suma_total += Integer.parseInt(vec_total.get(i)+"")+ Integer.parseInt(vec_total_f.get(i)+"");
              vec_total.set(i,"0");
              vec_total_f.set(i,"0");
            } %>
              <td align="center" valign="top"><%=suma_total%></td>
          </tr>
   <%} %>
</table>
<%}}%>

</body>
</html>
<%general.getCerrarConexion(); %>
