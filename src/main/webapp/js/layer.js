var TheBrowserName = navigator.appName;
var TheBrowserVersion = parseFloat(navigator.appVersion);

function escondeBloque(idBloque, opcion){
  var accion = 'block';
  if (opcion == 1) accion = 'none';
  if (document.layers) document.layers[idBloque].display = accion;
  else if (document.all) document.all[idBloque].style.display = accion;
  else if (document.getElementById) document.getElementById(idBloque).style.display = accion;
}

function estadoBloque(idBloque){
  if (document.layers) return document.layers[idBloque].display;
  else if (document.all) return document.all[idBloque].style.display;
  else if (document.getElementById) return document.getElementById(idBloque).style.display;
}

function CambiaPuntero(t,forma){
  if ((TheBrowserName == 'Netscape') && (TheBrowserVersion >= 5)) {
    t.style.cursor="default";
  } else {
    t.style.cursor=forma;
  }
}