 function TrimLeft( str ) {
    var resultStr = "";
    var i = len = 0;
    
    // Return immediately if an invalid value was passed in
    if (str+"" == "undefined" || str == null)	
      return null;
    
    // Make sure the argument is a string
    str += "";
    
    if (str.length == 0) 
      resultStr = "";
    else {	
      // Loop through string starting at the beginning as long as there
      // are spaces.
      //	  	len = str.length - 1;
      len = str.length;
      
      while ((i <= len) && (str.charAt(i) == " "))
			i++;
      
      // When the loop is done, we're sitting at the first non-space char,
      // so return that char plus the remaining chars of the string.
      resultStr = str.substring(i, len);
    }
    
    return resultStr;
  }
  function TrimRight( str ) {
    var resultStr = "";
    var i = 0;

    // Return immediately if an invalid value was passed in
    if (str+"" == "undefined" || str == null)	
      return null;
    
    // Make sure the argument is a string
    str += "";
    
    if (str.length == 0) 
      resultStr = "";
    else {
  		// Loop through string starting at the end as long as there
  		// are spaces.
      i = str.length - 1;
      while ((i >= 0) && (str.charAt(i) == " "))
	i--;
      
      // When the loop is done, we're sitting at the last non-space char,
      // so return that char plus all previous chars of the string.
  		resultStr = str.substring(0, i + 1);
    }
    
    return resultStr;  	
  }
  
  // Este script depende de otros 2: Trim derecha y Trim izquierda
  function trim( str ) {
	var resultStr = "";
	
	resultStr = TrimLeft(str);
	resultStr = TrimRight(resultStr);
	
	return resultStr;
  }