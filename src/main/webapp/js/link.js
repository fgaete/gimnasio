
function DisableEnableLinks(xHow){
  objLinks = document.links;
  for(i=0;i<objLinks.length;i++){
    objLinks[i].disabled = xHow;
    //link with onclick

    if(xHow && objLinks[i].onclick){
        objLinks[i].onclick = new Function("return false;" + objLinks[i].onclick.toString().getFuncBody());
    }
    //link without onclick
    else if(xHow){
      objLinks[i].onclick = function(){return false;}
    }
    //remove return false with link without onclick
    else
      if(!xHow && objLinks[i].onclick.toString().indexOf("function(){return false;}") != -1){
      objLinks[i].onclick = null;
    }
    //remove return false link with onclick
    else
      if(!xHow && objLinks[i].onclick.toString().indexOf("return false;") != -1){
      strClick = objLinks[i].onclick.toString().getFuncBody().replace("return false;","");
      objLinks[i].onclick = new Function(strClick);
    }
  }
}

String.prototype.getFuncBody = function(){
  var str=this.toString();
  str=str.replace(/[^{]+{/,"");
  str=str.substring(0,str.length-1);
  str = str.replace(/\n/gi,"");
  if(!str.match(/\(.*\)/gi))str += ""; // ")"
  return str;
}
