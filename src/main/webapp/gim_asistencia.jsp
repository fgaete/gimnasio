<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
  <%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>
<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 25/07/2007
   // �ltima Actualizaci�n :
   // esta es la muestra de las asistenacias que tiene el "alumno"
%>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");

  int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int cod_perfil = session.getAttribute("cod_perfil") != null?
                  Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
  Vector vec_asistaerobica = new Vector();
  Vector vec_asistmaquinas = new Vector();
  Vector vec_periodo = new Vector();
  Vector vec_periodo_sala = new Vector();
  boolean hayDatos = false;
  boolean cabecera = false;
  
  boolean esAdministrador = cod_perfil == 2;
  
  int consulta =  request.getParameter("consulta")!=null &&
  !request.getParameter("consulta").equals("null") &&
  !request.getParameter("consulta").equals("NULL") &&
  !request.getParameter("consulta").equals("")  &&
  !request.getParameter("consulta").equals(" ")?
  Integer.parseInt(request.getParameter("consulta")+""):0;
  
  String left = "0";
  String right = "0";
  String top = "0";
  
  if (consulta > 0 && esAdministrador) {
	  rut_usuario = consulta;
	  left = "20";
	  right = "20";
	  top = "20";
  }


  String f_inicio = "";
  String f_termino = "";
  String s_inicio_ant = "";
  String a�o_sem_m = "";
  String a�o_sem_a = "";
  String imagen = "";
  String texto_imagen = "";


  general.getAbrirConexion();
  general.setGenerarConexionHorario();
  general.setGenerarConexionReserva();
  general.getPeriodoSemestreActual(1,1);
  a�o_sem_m = general.getA�oActual()+"-"+general.getSemestreActual();


%>
<html>
<head>
<title>Gimnasio - Asistencia</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: <%=left%>px;
	margin-right: <%=right%>px;
	margin-top: <%=top%>px;
}
-->
</style></head>

<body>
<%int cont_asiste = 0;
  int cont_falta = 0;
  int cont_justifica = 0;
  int cont_espera = 0;

if (rut_usuario > 0) {
   vec_periodo_sala = general.getPeriodoGimnasio(general.getA�oActual(),general.getSemestreActual(),10335);
   
    if (vec_periodo_sala.size() > 0){
   for (int g=0;g<vec_periodo_sala.size();g++){
   if (vec_periodo_sala.size()>0) {
  f_inicio = (vec_periodo_sala.size()>0)?((Vector)vec_periodo_sala.get(g)).get(6)+"":"";
  f_termino = (vec_periodo_sala.size()>0)?((Vector)vec_periodo_sala.get(g)).get(7)+"":"";
 // a�o_sem_m = String.valueOf(general.getA�oActual())+"-"+String.valueOf(general.getSemestreActual());


  vec_asistmaquinas = general.getAsistenciaGimnasio2(rut_usuario, f_inicio, f_termino,
                      ((Vector)vec_periodo_sala.get(g)).get(5)+"");

if (vec_asistmaquinas.size() > 0 && !cabecera) cabecera = true;

if (vec_asistmaquinas.size() > 0){
if (cabecera){%>
<table width="100%"  border="0" cellpadding="1" cellspacing="0">
  <tr>
    <td width="100%" height="37" class="Estilo_Verde">ASISTENCIA</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">M&Aacute;QUINAS <%=a�o_sem_m%></span></td>
  </tr>
</table>

<%}
  String periodo_act = "";
  String periodo_ant = "";
  String icono = "";
  for (int i=0;i < vec_asistmaquinas.size();i++){
   periodo_act = ((Vector)vec_periodo_sala.get(g)).get(5)+"";
   imagen = "";
   icono = "&nbsp;";
   if (!periodo_ant.equals(periodo_act)) {
     cont_asiste = 0;
     cont_falta = 0;
     cont_justifica = 0;
     cont_espera = 0;
   }
   switch (Integer.parseInt(((Vector)vec_asistmaquinas.get(i)).get(2)+"")) {
     case 1: {imagen = "imagen/gim_asistio.gif";// asiste
              texto_imagen = "Asisti�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_asiste ++;
       break;
     }
     case 2: {imagen = "imagen/gim_justifico.gif";  // justific�
              texto_imagen = "Justific�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_justifica ++;
       break;
     }
     case 3: {imagen = "imagen/gim_falto.gif";   // falta
              texto_imagen = "Falt�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_falta ++;
       break;
     }
   case 4: {imagen = "imagen/gim_espera.gif";   // espera
           texto_imagen = "Espera";
           icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
           cont_espera ++;
       break;
     }
   case 0: { //pendiente
	   break;
	}
   }
   if (!periodo_ant.equals(periodo_act)) {%>
  <br>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="20" class="Estilo_Azul">Reservas del Per&iacute;odo <%=((Vector)vec_periodo_sala.get(g)).get(0)+""%> - <%=((Vector)vec_periodo_sala.get(g)).get(1)+""%></td>
  </tr>
  </table>
  <%} if (i == 0) {%>
  <table width="100%"  border="0" cellpadding="0" cellspacing="1"  bgcolor="#CCCCCC" >
  <tr >
    <td width="5%" height="15" background="imagen/boton_baseFONDO05.gif">N&deg;</td>
    <td width="35%" background="imagen/boton_baseFONDO05.gif">Semana</td>
    <td width="10%" background="imagen/boton_baseFONDO05.gif">D&iacute;a</td>
    <td width="20%" background="imagen/boton_baseFONDO05.gif">Bloque</td>
    <% if (esAdministrador) {%>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Reservado por</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Asistencia por</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Fecha</td>
    <td align="center" background="imagen/boton_baseFONDO05.gif">Estado</td>
    <% } else { %>
    <td width="20%" background="imagen/boton_baseFONDO05.gif">Fecha</td>
    <td width="10%" align="center" background="imagen/boton_baseFONDO05.gif">Asistencia</td>
    <%} %>
  </tr>
  <%}
  hayDatos = true;
  %>
  <tr>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=i+1%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistmaquinas.get(i)).get(6)+""%> al <%=((Vector)vec_asistmaquinas.get(i)).get(7)+""%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistmaquinas.get(i)).get(5)+""%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistmaquinas.get(i)).get(3)+""%> - <%=((Vector)vec_asistmaquinas.get(i)).get(4)+""%></td>
    <% if (esAdministrador) {%>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><font class="letra7"><%=((Vector)vec_asistmaquinas.get(i)).get(8)+""%></font></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><font class="letra7"><%=((Vector)vec_asistmaquinas.get(i)).get(9)+""%></font></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistmaquinas.get(i)).get(1)+""%></td>
    <td align="center" bgcolor="#FFFFFF"><%=icono%></td>
    <% } else { %>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistmaquinas.get(i)).get(1)+""%></td>
    <td align="center" bgcolor="#FFFFFF"><%=icono%></td>
    <%} %>
  </tr>
  <% if (i == vec_asistmaquinas.size()-1) {%>
  <tr>
    <td height="20" colspan="<% if (esAdministrador) {%>7<% } else { %>5<%} %>" align="center" background="imagen/boton_baseFONDO06.gif"><span class="Estilo_Azulino"><img src="imagen/gim_asistio.gif" alt="ASISTI&Oacute;" width="14" height="14" align="absmiddle"> Asiste : <strong><%=cont_asiste%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_falto.gif" alt="FALT&Oacute;" width="15" height="15" align="absmiddle"> Falta : <strong><%=cont_falta%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_justifico.gif" alt="JUSTIFIC&Oacute;" width="17" height="18" align="absmiddle"> Justifica : <strong><%=cont_justifica%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_espera.gif" alt="E" width="17" height="18" align="absmiddle"> Espera : <strong><%=cont_espera%></strong></span></td>
    <td align="center" background="imagen/boton_baseFONDO06.gif"><span class="Estilo_Azulino"><strong>&nbsp;</strong></span>Total : <strong><%=cont_asiste%></strong> </td>
  </tr>
  </table>
  <%}%>
<% periodo_ant = periodo_act; }%>
</table>
<br>
<%}}}
   
   cabecera = false;
   
    vec_periodo_sala = general.getPeriodoGimnasio(general.getA�oActual(),general.getSemestreActual(),10336);
    if (vec_periodo_sala.size() > 0){
    for (int g=0;g<vec_periodo_sala.size();g++){
    if (vec_periodo_sala.size()>0) {
   f_inicio = (vec_periodo_sala.size()>0)?((Vector)vec_periodo_sala.get(g)).get(6)+"":"";
   f_termino = (vec_periodo_sala.size()>0)?((Vector)vec_periodo_sala.get(g)).get(7)+"":"";
   a�o_sem_a = String.valueOf(general.getA�oActual())+"-"+String.valueOf(general.getSemestreActual());


  vec_asistaerobica = general.getAsistenciaGimnasio2(rut_usuario, f_inicio, f_termino,
                      ((Vector)vec_periodo_sala.get(g)).get(5)+"");
  cont_asiste = 0;
  cont_falta = 0;
  cont_justifica = 0;
  cont_espera = 0;


if (vec_asistaerobica.size() > 0 && !cabecera) cabecera = true;

if (vec_asistaerobica.size() > 0){
if (cabecera){%>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr bgcolor="#FFFFFF">
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">MULTIUSO <%=a�o_sem_a%></span></td>
  </tr>
</table>
<%}

  String periodo_act = "";
  String periodo_ant = "";
  String icono = "";
  for (int i=0;i < vec_asistaerobica.size();i++){
   periodo_act = ((Vector)vec_periodo_sala.get(g)).get(5)+"";
   imagen = "";
   icono = "&nbsp;";
   if (!periodo_ant.equals(periodo_act)) {
     cont_asiste = 0;
     cont_falta = 0;
     cont_justifica = 0;
     cont_espera = 0;
   }
   switch (Integer.parseInt(((Vector)vec_asistaerobica.get(i)).get(2)+"")) {
     case 1: {imagen = "imagen/gim_asistio.gif";// asiste
              texto_imagen = "Asisti�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_asiste ++;
       break;
     }
     case 2: {imagen = "imagen/gim_justifico.gif";  // justific�
              texto_imagen = "Justific�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_justifica ++;
       break;
     }
     case 3: {imagen = "imagen/gim_falto.gif";   // falta
              texto_imagen = "Falt�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_falta ++;
       break;
     }
     case 4: {imagen = "imagen/gim_espera.gif";   // espera
              texto_imagen = "Espera";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_espera ++;
       break;
     }
     case 0: {//pendiente
	   break;
	}
   }
   if (!periodo_ant.equals(periodo_act)) {%>
  <br>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="20" class="Estilo_Azul">Reservas del Per&iacute;odo <%=((Vector)vec_periodo_sala.get(g)).get(0)+""%> - <%=((Vector)vec_periodo_sala.get(g)).get(1)+""%></td>
  </tr>
  </table>
  <%} if (i == 0) {%>
  <table width="100%"  border="0" cellpadding="0" cellspacing="1"  bgcolor="#CCCCCC" >
  <tr >
    <td width="5%" height="15" background="imagen/boton_baseFONDO05.gif">N&deg;</td>
    <td width="35%" background="imagen/boton_baseFONDO05.gif">Semana</td>
    <td width="10%" background="imagen/boton_baseFONDO05.gif">D&iacute;a</td>
    <td width="20%" background="imagen/boton_baseFONDO05.gif">Bloque</td>
    <% if (esAdministrador) {%>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Reservado por</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Asistencia por</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Fecha</td>
    <td align="center" background="imagen/boton_baseFONDO05.gif">Estado</td>
    <% } else { %>
    <td width="20%" background="imagen/boton_baseFONDO05.gif">Fecha</td>
    <td width="10%" align="center" background="imagen/boton_baseFONDO05.gif">Asistencia</td>
    <%} %>
  </tr>
  <%}
  hayDatos = true;
  %>
  <tr>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=i+1%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistaerobica.get(i)).get(6)+""%> al <%=((Vector)vec_asistaerobica.get(i)).get(7)+""%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistaerobica.get(i)).get(5)+""%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistaerobica.get(i)).get(3)+""%> - <%=((Vector)vec_asistaerobica.get(i)).get(4)+""%></td>
    <% if (esAdministrador) {%>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><font class="letra7"><%=((Vector)vec_asistaerobica.get(i)).get(8)+""%></font></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><font class="letra7"><%=((Vector)vec_asistaerobica.get(i)).get(9)+""%></font></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistaerobica.get(i)).get(1)+""%></td>
    <td align="center" bgcolor="#FFFFFF"><%=icono%></td>
    <% } else { %>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistaerobica.get(i)).get(1)+""%></td>
    <td align="center" bgcolor="#FFFFFF"><%=icono%></td>
    <%} %>
  </tr>
  <% if (i == vec_asistaerobica.size()-1) {%>
  <tr>
    <td height="20" colspan="<% if (esAdministrador) {%>7<% } else { %>5<%} %>" align="center" background="imagen/boton_baseFONDO06.gif"><span class="Estilo_Azulino"><img src="imagen/gim_asistio.gif" alt="ASISTI&Oacute;" width="14" height="14" align="absmiddle"> Asiste : <strong><%=cont_asiste%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_falto.gif" alt="FALT&Oacute;" width="15" height="15" align="absmiddle"> Falta : <strong><%=cont_falta%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_justifico.gif" alt="JUSTIFIC&Oacute;" width="17" height="18" align="absmiddle"> Justifica : <strong><%=cont_justifica%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_espera.gif" alt="E" width="17" height="18" align="absmiddle"> Espera : <strong><%=cont_espera%></strong></span></td>
    <td align="center" background="imagen/boton_baseFONDO06.gif" nowrap><span class="Estilo_Azulino"><strong>&nbsp;</strong></span>Total : <strong><%=cont_asiste%></strong> </td>
  </tr>
  </table>
  <%}%>
<% periodo_ant = periodo_act;}}}%>

<%}}}}  

if (!hayDatos){
%>
<span class="Estilo_Verde">ASISTENCIA Y RESERVAS</span><br><br>
<span class="Estilo_Rojo">No existen reservas ni asistencias para el periodo actual.</span>
<%
}

general.getCerrarConexion();%></body>
</html>
