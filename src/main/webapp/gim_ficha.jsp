<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 27/06/2007
   // �ltima Actualizaci�n : 20/10/2008 no mostrar situaci�n ya que est� mostrado en la carrera
   // Ficha Se muestra la ficha personal del alumno o funcionario.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<jsp:useBean id="us" class="gimnasio.BeanUsuarios"></jsp:useBean>

<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int cod_perfil = session.getAttribute("cod_perfil") != null?
                   Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
  Vector vec = new Vector();
  Vector vec_rama = new Vector();
  Vector vec_reg = new Vector();
  Vector vec_tutoria = new Vector();
  
 


  String rut = "";
  String nombre = "";
  String carrera = "";
  String rol = "";
  String ingreso = "";
  String nac = "";
  String calidad = "";
  String rama = "";
  String prof_rama = "";
  String estado = "";
  String depto = "";
  String anexo = "";
  String email = "";
  String tipo_contrato = "";
  String planta = "";
  String dvrut = "";

  
  int origen = 0;
  int fono = 0;
  int codContrato = 0;
  String cargo = "";
  String jerarquia = "";
  String contrato = "";
  
  if (session.getAttribute("rut_consulta") != null)
	   session.removeAttribute("rut_consulta");

  if (cod_perfil == 8)  {
    general.getAbrirConexion();
    general.setGenerarConexionDatosAl();
    general.setGenerarConexionRegistro();
    vec = general.getAlumno(rut_usuario);
    vec_reg = general.getRegistroUsuario(rut_usuario);
    if(vec_reg.size()>0 && ((vec_reg.get(1)+"").trim().equals("3") ||
                            (vec_reg.get(1)+"").trim().equals("6") )) // es de rama
    {
      vec_rama = general.getAlumnoRama(rut_usuario);
      calidad = "Rama Deportiva";
      if (vec_rama.size()>0){
        rama = vec_rama.get(1)+"";
        prof_rama = vec_rama.get(3)+"";
      } else calidad +=" - Vi�a";

    }
    if (vec_reg.size() > 0 && (vec_reg.get(1)+"").trim().equals("2")){
      calidad = "Tutor�a";
      general.setGenerarConexionReserva();
     vec_tutoria = general.getReservaAluTutoria (vec_reg.get(4)+"");

    }
    general.getCerrarConexion();
    // buscar es de vi�a
    /*if (vec.size() == 0 || // que no exista como alumno CC
        (vec.size() > 0 && vec.get(5).equals("")) || // que exista como alumno CC pero no tenga carrera vigente
        ((vec_reg.size()>0 && // que est� registrado como alumno de vi�a
        ((vec_reg.get(1)+"").trim().equals("5") ||
        (vec_reg.get(1)+"").trim().equals("6"))))){

      general.getAbrirConexionVi�a();
      general.setGenerarConexionDatosVi�a();
      vec = general.getAlumnoVi�a(rut_usuario);
      general.getCerrarConexionVi�a();
    }*/

    if (vec.size() > 0){
      rut = vec.get(12)+"";
      nombre = vec.get(1)+"";
      carrera = vec.get(5)+"";
      rol = vec.get(9)+"";
      ingreso = vec.get(10)+"";
      nac = vec.get(11)+"";
      dvrut = vec.get(17)+"";
    }
  } else{ // es funcionario
    us.getAbrirConexion();
    us.setGenerarConexionUsuario();
    vec = us.getFuncionario(rut_usuario);
    us.getCerrarConexion();
	/*if (vec.size() == 0){
        general.getAbrirConexionVi�a();
      general.setGenerarConexionDatosVi�a();
      vec = general.getFuncionarioVi�a(rut_usuario);
      general.getCerrarConexionVi�a();
  
      if (vec.size()>0){
        dvrut = vec.get(15)+"";
        rut = vec.get(13)+"";
        nombre = vec.get(0)+" "+vec.get(1)+" "+vec.get(2)+"";
        anexo = vec.get(6)+"";
        email = vec.get(7)+"";
        if (Integer.parseInt(vec.get(8)+"") == 1)
        tipo_contrato = "Funcionario";
        else  if (Integer.parseInt(vec.get(8)+"") == 2)
        tipo_contrato = "Profesor ";

        tipo_contrato += vec.get(11).equals("sin informacion")?"":vec.get(11)+"";
        planta = vec.get(12)+"";
        if(Integer.parseInt(vec.get(16)+"") == 1 || Integer.parseInt(vec.get(16)+"") == 2)
        depto = vec.get(4) + ", "+ vec.get(5);
        else depto =  vec.get(5)+"";
    	  dvrut = vec.get(1)+"";
          rut = vec.get(0)+"";
          nombre = vec.get(2)+" "+vec.get(3)+" "+vec.get(4)+"";
          anexo = vec.get(8)+"";
          email = "";
          tipo_contrato = vec.get(13)+"";
          planta = vec.get(17)+"";
          depto = vec.get(14) + ", "+ vec.get(16);

      }
       } else { // con datos de la tabla funcionario del Descad
      */
    	   
    	   if (vec.size()>0){
		       origen = 1;
		       rut = vec.get(0)+"-"+vec.get(1)+"";
		       dvrut = vec.get(1)+"";
		       nombre = vec.get(2)+" "+vec.get(3)+" "+vec.get(4)+"";
		       depto = vec.get(14) + ", "+ vec.get(20);
		       planta = vec.get(17)+"";
		       cargo = vec.get(18)+"";
		       contrato = vec.get(13)+"";
		       jerarquia = vec.get(12)+"";
		       codContrato =(vec.get(19) != null)?Integer.parseInt(vec.get(19)+""):0;
		       fono =(vec.get(8) != null)?Integer.parseInt(vec.get(8)+""):0;
		     
    	   }
    /* }*/


  }

  
  if (rut_usuario > 0){
	  session.setAttribute("rut_consulta",vec.get(0)+"");
  }
%>
<html>
<head>
<title>Gimnasio - Ficha</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script language="javascript">
  function changeImageSrc(whichImage,newGraphic) {
     if (document.images) {
         document.images[whichImage].src = newGraphic;
     }
  }
  function SinFoto(foto) {
     changeImageSrc(foto,"imagen/sinfoto.jpg");
}
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style></head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td height="37" colspan="3" class="Estilo_Naranjo">FICHA </td>
  </tr>
  <tr>
    <td width="23%" valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">RUT</span></td>
    <td width="58%" valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=rut%></span></td>
    <td width="14%" rowspan="11" align="center"><img name="foto" src="foto" align="absmiddle" onError="SinFoto('foto');" width="110" height="115"
    border="1"></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Nombre</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=nombre%></span></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=(cod_perfil == 8)?"Carrera":"Departamento"%></span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(cod_perfil == 8)?carrera:depto%></span></td>
  </tr>
    <%if(origen == 1){ %>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Contrato</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=contrato%></span></td>
  </tr>
  <%if(codContrato != 3) { %>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Planta</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=planta%></span></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Jerarqu�a</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=jerarquia%></span></td>
  </tr>
  <%}%>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Cargo</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=cargo%></span></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Tel�fono</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=fono%></span></td>
  </tr>
<% } else { %>
<tr>
  <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=(cod_perfil == 8)?"Rol":"Anexo"%></span></td>
  <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(cod_perfil == 8)?rol:anexo%></span></td>
</tr>
<tr>
  <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=(cod_perfil == 8)?"Ingreso":"Email"%></span></td>
  <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(cod_perfil == 8)?ingreso:email%></span></td>
</tr>
<tr>
  <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=(cod_perfil == 8)?"Nacionalidad":"Tipo de Contrato"%></span></td>
  <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(cod_perfil == 8)?nac:tipo_contrato%></span></td>
</tr>
<%}%>
  <%if(cod_perfil == 8 && vec_reg.size()>0 &&
   ((vec_reg.get(1)+"").trim().equals("3") || (vec_reg.get(1)+"").trim().equals("6") ||
   (vec_reg.get(1)+"").trim().equals("2"))){%>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Calidad</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=calidad%></span></td>
  </tr>
  <%if(cod_perfil == 8 && vec_reg.size()>0 &&
   ((vec_reg.get(1)+"").trim().equals("3") || (vec_reg.get(1)+"").trim().equals("6"))) {%>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Rama</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=rama%> </span></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Profesor de rama </span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=prof_rama%> </span></td>
  </tr>
  <%}%>
  <% if ((vec_reg.get(1)+"").trim().equals("2")) {
    String sala = "";
    int indice = 0;
    for (int i=0;vec_tutoria.size()>0 && i<vec_tutoria.size();i++){
    if (!sala.trim().equals(((Vector) vec_tutoria.get(i)).get(0)+ "")){
      indice = 1;
%>
    <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Sala</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=((Vector) vec_tutoria.get(i)).get(0)+ ""%> </span></td>
  </tr>
      <%} %>
   <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Reserva <%=indice%></span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=((Vector) vec_tutoria.get(i)).get(1)%> </span></td>
  </tr>

  <%sala = ((Vector) vec_tutoria.get(i)).get(0)+ "";
   indice++;
    }}%>
<%}%>

</table>
</body>
</html>
