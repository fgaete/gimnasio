<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*,java.text.*"%>
<%@ page import="gimnasio.*"%>
<%!private static final DecimalFormat FMT_dec = new DecimalFormat("#0.00");%>

<% // Creado por           : M�nica Barrera Frez
// Fecha                : 23/10/2007
// �ltima Actualizaci�n :
// Registro listado.    Estad�stica por uso seg�n rango de fecha .
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>


<%response.setHeader("Pragma","no-cache");
response.setDateHeader ("Expires", 0);
response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
String fecha_desde    = request.getParameter("fecha_desde")!=null &&
                        !request.getParameter("fecha_desde").equals("null") &&
                        !request.getParameter("fecha_desde").equals("NULL") &&
                        !request.getParameter("fecha_desde").equals("")  &&
                        !request.getParameter("fecha_desde").equals(" ")?
                        request.getParameter("fecha_desde")+"":"";
                        String fecha_hasta  = request.getParameter("fecha_hasta")!=null &&
                        !request.getParameter("fecha_hasta").equals("null") &&
                        !request.getParameter("fecha_hasta").equals("NULL") &&
                        !request.getParameter("fecha_hasta").equals("")  &&
                        !request.getParameter("fecha_hasta").equals(" ")?
                        request.getParameter("fecha_hasta")+"":"";
int   version =  request.getParameter("version")!=null &&
                 !request.getParameter("version").equals("null") &&
                 !request.getParameter("version").equals("NULL") &&
                 !request.getParameter("version").equals("")  &&
                 !request.getParameter("version").equals(" ")
                 ?Integer.parseInt(request.getParameter("version")+""):0;
int   sala =  request.getParameter("sala")!=null &&
              !request.getParameter("sala").equals("null") &&
              !request.getParameter("sala").equals("NULL") &&
              !request.getParameter("sala").equals("")  &&
              !request.getParameter("sala").equals(" ")
              ?Integer.parseInt(request.getParameter("sala")+""):0;
int anno             = request.getParameter("anno")!=null &&
                       !request.getParameter("anno").equals("null") &&
                       !request.getParameter("anno").equals("NULL") &&
                       !request.getParameter("anno").equals("")  &&
                       !request.getParameter("anno").equals(" ")?
                       Integer.parseInt(request.getParameter("anno")+""):0;
int semestre         = request.getParameter("semestre")!=null &&
                       !request.getParameter("semestre").equals("null") &&
                       !request.getParameter("semestre").equals("NULL") &&
                       !request.getParameter("semestre").equals("")  &&
                       !request.getParameter("semestre").equals(" ")?
                       Integer.parseInt(request.getParameter("semestre")+""):0;

Vector vec_bloque = new Vector();
Vector vec_asistentes = new Vector();
Vector vec_uso = new Vector();
Vector vec_aerobica = new Vector();
Vector vec_total_masc = new Vector();
Vector vec_total_porc = new Vector();
Vector vec_total_porc_fem = new Vector();
Vector vec_colspan = new Vector();
Vector vec_total_fem = new Vector();
Vector vec_dia = new Vector();
Vector vec_num_dia = new Vector();
Vector vec_tipo_reserva = new Vector();

String asist_masc = "";
String colspan = "";
double sum_masc = 0.00;

general.getAbrirConexion();
general.setGenerarConexionHorario();
general.setGenerarConexionReserva();

String m  = (Integer.parseInt(fecha_desde.substring(3,5)) < 10)?
("0"+Integer.parseInt(fecha_desde.substring(3,5))):
(""+Integer.parseInt(fecha_desde.substring(3,5)));
String dd = (Integer.parseInt(fecha_desde.substring(0,2)) < 10)?
("0"+Integer.parseInt(fecha_desde.substring(0,2))):
(""+Integer.parseInt(fecha_desde.substring(0,2)));
String a  = (Integer.parseInt(fecha_desde.substring(6,10)) < 10)?
("0"+Integer.parseInt(fecha_desde.substring(6,10))):
(""+Integer.parseInt(fecha_desde.substring(6,10)));
String   f1 = a + m + dd;
m  = (Integer.parseInt(fecha_hasta.substring(3,5)) < 10)?
("0"+Integer.parseInt(fecha_hasta.substring(3,5))):
(""+Integer.parseInt(fecha_hasta.substring(3,5)));
dd = (Integer.parseInt(fecha_hasta.substring(0,2)) < 10)?
("0"+Integer.parseInt(fecha_hasta.substring(0,2))):
(""+Integer.parseInt(fecha_hasta.substring(0,2)));
a  = (Integer.parseInt(fecha_hasta.substring(6,10)) < 10)?
("0"+Integer.parseInt(fecha_hasta.substring(6,10))):
(""+Integer.parseInt(fecha_hasta.substring(6,10)));
String   f2 = a + m + dd;

int cupo = (sala == 10335 || sala == 3)?general.getCupoGimnasio(anno, semestre, 0, 0, 0,10335,0):0;
vec_tipo_reserva = general.getTipoReserva();

int dia = 1;
int num_dia = 0;
int cant_bloque = 0;
int total_masc = 0;
int cant_masc = 0;
int total_fem = 0;
int cant_fem = 0;
int num_fecha = 0;
int num_fecha_ant = 0;
int regicount = 0; // para que no se repita el mensaje "No existen registros asociados" 


String nom_dia = "";
String nom_dia_ant = "";
String nom_fecha = "";
String nom_fecha_ant = "";
String masc = "&nbsp;";
String fem = "&nbsp;";
String nom_actividad = "";
String nom_actividad_ant = "";
String hora_inicio = "";
String hora_fin = "";
String hora_inicio_ant = "";


double f_masc = 0.00;
double total_dia_masc = 0.00;
double f_fem = 0.00;
double total_dia_fem = 0.00;
boolean pasa = true;
boolean existe = false;

int registro_usuario = 1;
%>
<html>
<head>
<title>Gimnasio - Estad&iacute;sitica</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="css/estilo.css" rel="stylesheet" type="text/css">
  <link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
  <style type="text/css">
  <!--
  .Estilo1 {color: #D40000}
  -->
  </style>
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="20" align="center" class="letra10">PORCENTAJE DE USO </td>
  </tr>
  <tr>
    <td height="20" align="center" valign="top"><%=fecha_desde%> al <%=fecha_hasta%></td>
  </tr>
</table>
<br>
<%if (sala == 10335 || sala == 3 ) {
  vec_bloque = general.getHoraBloque(10335, anno, semestre);


  %>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="20" valign="bottom"><font  class="letra10">SALA DE MAQUINAS Y PESAS</font><br>
    </td>
  </tr>
</table>
<br>
<% for (int z=0;z< vec_tipo_reserva.size();z++) {
     vec_asistentes = new Vector();
     vec_asistentes = general.getPorcentajeUso(anno, semestre, f1, f2,10335 , version, Integer.parseInt(((Vector)vec_tipo_reserva.get(z)).get(0)+""));
     vec_uso = new Vector();
     vec_aerobica = new Vector();
     vec_total_masc = new Vector();
     vec_total_porc = new Vector();
     vec_total_porc_fem = new Vector();
     vec_colspan = new Vector();
     vec_total_fem = new Vector();
     vec_dia = new Vector();
     vec_num_dia = new Vector();
     dia = 1;
     num_dia = 0;
     cant_bloque = 0;
     total_masc = 0;
     cant_masc = 0;
     total_fem = 0;
     cant_fem = 0;
     num_fecha = 0;
     num_fecha_ant = 0;
     nom_dia = "";
     nom_dia_ant = "";
     nom_fecha = "";
     nom_fecha_ant = "";
     masc = "&nbsp;";
     fem = "&nbsp;";
     nom_actividad = "";
     nom_actividad_ant = "";
     hora_inicio = "";
     hora_fin = "";
     hora_inicio_ant = "";
     f_masc = 0.00;
     total_dia_masc = 0.00;
     f_fem = 0.00;
     total_dia_fem = 0.00;
     pasa = true;
     if (version == 0){

   if (vec_asistentes.size() > 0){%>
   <br>
  <table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="20" valign="bottom"><font  class="letra10">
      <%=((Vector)vec_tipo_reserva.get(z)).get(1)%></font><br>
      <%="Cupo " + cupo%>  </td>
  </tr>
</table>
 <br>
  <table width="100%"  border="1" cellpadding="2" cellspacing="0">
    <tr valign="bottom" bgcolor="#FFFFFF">
      <td bgcolor="#CCCCCC" ><strong>D&iacute;a</strong></td>
      <td align="right" bgcolor="#CCCCCC"  ><strong>Fecha</strong></td>
      <%for (int i=0;i<vec_bloque.size();i++){%>
        <td align="center" bgcolor="#CCCCCC" class="letra7" ><strong><%=((Vector)vec_bloque.get(i)).get(1)%><br>
          <%=((Vector)vec_bloque.get(i)).get(2)%>
</strong></td>
<% vec_total_masc.addElement("0");
vec_colspan.addElement("0");
}
vec_total_masc.addElement("0");
%>
<td align="center" bgcolor="#CCCCCC"><strong>Total</strong></td>
<td align="center" bgcolor="#CCCCCC"><strong>% Uso </strong></td>
    </tr>
    <%dia = 1;
    num_dia = 0;
    cant_bloque = 0;
    nom_dia = "";
    nom_fecha = "";
    nom_fecha_ant = "";
    masc = "&nbsp;";
    boolean escribetotal = false;
    while (dia < 7) {
      escribetotal = false;
      total_masc = 0;
      cant_masc = 0;
      f_masc = 0.00;
      total_dia_masc = 0.00;
      vec_colspan = new Vector();
      pasa = true;
      for (int j=0;j<vec_asistentes.size();j++){
        num_dia = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(2)+"");
        nom_dia = ((Vector)vec_asistentes.get(j)).get(0)+"";
        colspan = "";
        if (num_dia == dia) {
          escribetotal = true;
          nom_fecha = ((Vector)vec_asistentes.get(j)).get(1)+"";
          if (!nom_fecha.trim().equals(nom_fecha_ant) && !nom_fecha_ant.trim().equals("") && !pasa) {
            if (total_masc > 0)
            f_masc = ((double)(100*total_masc)/(cupo*cant_bloque));
            else
            f_masc = 0;
            total_dia_masc += f_masc;
            %>
            <td width="8%" align="center"><%=total_masc%></td>
            <td width="8%" align="center"><%=FMT_dec.format(f_masc)%></td>
</tr>
<%  double t = total_masc + Double.parseDouble(vec_total_masc.get(vec_total_masc.size()-1)+"");
vec_total_masc.set(vec_total_masc.size()-1,t+"");

total_masc = 0;
cant_bloque = 0;
}

if (pasa) {
  pasa = false;
  %>
  <tr bgcolor="#FFFFFF">
    <td width="11%"><strong><%=nom_dia%></strong></td>
    <td width="8%" align="right"><%=nom_fecha%></td>
    <%} else {
    if (!nom_fecha.trim().equals(nom_fecha_ant)) {
      %>
      <tr bgcolor="#FFFFFF">
        <td width="11%">&nbsp;</td>
        <td width="8%" align="right"><%=nom_fecha%></td>
        <%}}
        masc = ((Vector)vec_asistentes.get(j)).get(5)+"";
        if (!masc.trim().equals("ANUL.") && !masc.trim().equals("SUSP.") &&
        !masc.trim().equals("ASEO") && !masc.trim().equals("")){
          cant_masc = Integer.parseInt(masc);
          total_masc += cant_masc;
          cant_bloque++;

        } else cant_masc = 0;

        int bi = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(3)+"");
        int bt = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(4)+"");
        int dif = (bt - bi) + 1;
        if (dif > 1){
          colspan = "colspan=\""+dif+"\"";
          int k=bi;

          while (k <= bt){
            double t = cant_masc + Double.parseDouble(vec_total_masc.get(k-1)+"");
            vec_total_masc.set(k-1,t+"");

            k++;
          }
        } else {
          double t = cant_masc +  Double.parseDouble(vec_total_masc.get(bi-1)+"");
          vec_total_masc.set(bi-1,t+"");
        }

        %>
        <td width="7%" <%=colspan%> align="center"><%=(!masc.trim().equals(""))?masc:"&nbsp;"%></td>
        <%  nom_fecha_ant = nom_fecha;
        }}
        if (total_masc > 0)
        f_masc = ((double)(100*total_masc)/(cupo*cant_bloque));
        else
        f_masc = 0;
        double t = total_masc + Double.parseDouble(vec_total_masc.get(vec_total_masc.size()-1)+"");
        vec_total_masc.set(vec_total_masc.size()-1,t+"");

        total_dia_masc += f_masc;
        if (escribetotal){
        %>
        <td width="8%" align="center"><%=total_masc%></td>
        <td width="8%" align="center"><%=FMT_dec.format(f_masc)%></td>

      </tr>
      <%}
      dia++;
      vec_colspan.addElement(colspan);
      colspan = "";
      total_masc = 0;
      f_masc = 0;
      cant_masc = 0;
      cant_bloque = 0;
       if (escribetotal){
      %>

      <tr bgcolor="#FFFFFF">
        <td>&nbsp;</td>
        <td align="right" bgcolor="#CCCCCC" ><strong>Total D&iacute;a </strong></td>
        <%for (int x=0; x < vec_total_masc.size();x++){%>
          <td align="center" bgcolor="#CCCCCC"  ><%=vec_total_masc.get(x)%></td>
          <%}%>
          <td align="center" bgcolor="#CCCCCC"  ><%=FMT_dec.format(total_dia_masc)%></td>

      </tr>
      <% }  for (int x=0; x < vec_total_masc.size();x++){
      vec_total_masc.set(x,"0") ;
    }
    total_dia_masc = 0.00;

  }%>
  </table>
  <% existe = true;} %>
   <%}
   if (!existe && version==0 && regicount==0){%>
     <span class="Estilo1">No existen registros asociados.</span>
  	 <% regicount=1; %>
  <%}   if (version==1){

        if (vec_asistentes.size() > 0){%>
  <table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="20" valign="bottom"><font  class="letra10">
      <%=((Vector)vec_tipo_reserva.get(z)).get(1)%></font><br>
      <%="Cupo " + cupo%>  </td>
  </tr>
</table>
	<% regicount = 0; %>
 <br>
  <table width="100%"  border="1" cellpadding="2" cellspacing="0">
    <tr valign="bottom" bgcolor="#FFFFFF">
      <td rowspan="2" bgcolor="#CCCCCC"><strong>D&iacute;a</strong></td>
      <td rowspan="2" bgcolor="#CCCCCC"><strong>Fecha</strong></td>

      <%for (int i=0;i<vec_bloque.size();i++){%>
        <td colspan="2" align="center" bgcolor="#CCCCCC" class="letra7">
          <strong><%=((Vector)vec_bloque.get(i)).get(1)%><br>
            <%=((Vector)vec_bloque.get(i)).get(2)%>
          </strong></td>
          <%
          vec_total_masc.addElement("0");
          vec_total_fem.addElement("0");
          vec_colspan.addElement("0");
        }
        vec_total_masc.addElement("0");
        vec_total_fem.addElement("0");
        %>
        <td align="center" colspan="2"  bgcolor="#CCCCCC"><strong>Total</strong></td>
        <td align="center" colspan="2"  bgcolor="#CCCCCC"><strong>% Uso </strong></td>

    </tr>
    <tr bgcolor="#CCCCCC">
      <%for (int i=0;i<vec_bloque.size();i++){%>
        <td width="4%" align="center">M</td>
        <td width="4%" align="center">F</td>
        <%} %>
        <td width="4%" align="center">M</td>
        <td width="4%" align="center">F</td>
        <td width="4%" align="center">M</td>
        <td width="4%" align="center">F</td>
    </tr>
    <%dia = 1;
    num_dia = 0;
    cant_bloque = 0;
    nom_dia = "";
    nom_fecha = "";
    nom_fecha_ant = "";
    masc = "&nbsp;";
    fem = "&nbsp;";
    boolean escribetotal = false;
    while (dia < 7) {
      escribetotal = false;
      total_masc = 0;
      cant_masc = 0;
      f_masc = 0.00;
      total_dia_masc = 0.00;

      total_fem = 0;
      cant_fem = 0;
      f_fem = 0.00;
      total_dia_fem = 0.00;
      pasa = true;
      vec_colspan = new Vector();

      for (int j=0;j<vec_asistentes.size();j++){
        num_dia = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(2)+"");
        nom_dia = ((Vector)vec_asistentes.get(j)).get(0)+"";
        colspan = "";
        if (num_dia == dia) {
          escribetotal = true;
          nom_fecha = ((Vector)vec_asistentes.get(j)).get(1)+"";
          if (!nom_fecha.trim().equals(nom_fecha_ant) && !nom_fecha_ant.trim().equals("") && !pasa) {
            if (total_masc > 0)
            f_masc = ((double)(100*total_masc)/(cupo*cant_bloque));
            else
            f_masc = 0;
            total_dia_masc += f_masc;

            if (total_fem > 0)
            f_fem = ((double)(100*total_fem)/(cupo*cant_bloque));
            else
            f_fem = 0;
            total_dia_fem += f_fem;
             if (escribetotal){%>
            <td width="8%" align="center"><%=total_masc%></td>
            <td width="8%" align="center"><%=total_fem%></td>
            <td width="8%" align="center"><%=FMT_dec.format(f_masc)%></td>
            <td width="8%" align="center"><%=FMT_dec.format(f_fem)%></td><%} %>
  </tr>
  <%
  double t = total_masc + Double.parseDouble(vec_total_masc.get(vec_total_masc.size()-1)+"");
  vec_total_masc.set(vec_total_masc.size()-1,t+"");
  total_masc = 0;
  cant_bloque = 0;

  double tf = total_fem + Double.parseDouble(vec_total_fem.get(vec_total_fem.size()-1)+"");
  vec_total_fem.set(vec_total_fem.size()-1,tf+"");
  total_fem = 0;

}

if (pasa) {
  pasa = false;
  %>
  <tr bgcolor="#FFFFFF">
    <td width="11%"><strong><%=nom_dia%></strong></td>
    <td width="8%" align="right"><%=nom_fecha%></td>
    <%} else {
    if (!nom_fecha.trim().equals(nom_fecha_ant)) {
      %>
      <tr bgcolor="#FFFFFF">
        <td width="11%">&nbsp;</td>
        <td width="8%" align="right"><%=nom_fecha%></td>
        <%}}
        masc = ((Vector)vec_asistentes.get(j)).get(5)+"";
        fem = ((Vector)vec_asistentes.get(j)).get(6)+"";
        if (!masc.trim().equals("ANUL.") && !masc.trim().equals("SUSP.") &&
        !masc.trim().equals("ASEO") && !masc.trim().equals("") ){
          cant_masc = Integer.parseInt(masc);
          total_masc += cant_masc;
          cant_bloque++;

        } else cant_masc = 0;

        if (!fem.trim().equals("ANUL.") && !fem.trim().equals("SUSP.") &&
        !fem.trim().equals("ASEO") && !fem.trim().equals("") ){
          cant_fem = Integer.parseInt(fem);
          total_fem += cant_fem;
          cant_bloque++;

        } else cant_fem = 0;

        int bi = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(3)+"");
        int bt = Integer.parseInt(((Vector)vec_asistentes.get(j)).get(4)+"");
        int dif = (bt - bi) + 1;
        if (dif > 1){
          colspan = "colspan=\""+dif+"\"";
          int k=bi;
          while (k <= bt){
             double t = cant_masc + Double.parseDouble(vec_total_masc.get(k-1)+"");
            vec_total_masc.set(k-1,t+"");
            double tf = cant_fem + Double.parseDouble(vec_total_fem.get(k-1)+"");
            vec_total_fem.set(k-1,tf+"");
            k++;
          }
        } else {
           double t = cant_masc +  Double.parseDouble(vec_total_masc.get(bi-1)+"");
          vec_total_masc.set(bi-1,t+"");
          double tf = cant_fem +  Double.parseDouble(vec_total_fem.get(bi-1)+"");
          vec_total_fem.set(bi-1,tf+"");

        }

        %>
        <td width="7%" <%=colspan%> align="center"><%=(!masc.trim().equals(""))?masc:"&nbsp;"%></td>
        <td width="7%" <%=colspan%> align="center"><%=(!fem.trim().equals(""))?fem:"&nbsp;"%></td>

        <%  nom_fecha_ant = nom_fecha;
        }}
        if (total_masc > 0)
        f_masc = ((double)(100*total_masc)/(cupo*cant_bloque));
        else
        f_masc = 0;
        double t = total_masc + Double.parseDouble(vec_total_masc.get(vec_total_masc.size()-1)+"");
        vec_total_masc.set(vec_total_masc.size()-1,t+"");

        total_dia_masc += f_masc;

        if (total_fem > 0)
        f_fem = ((double)(100*total_fem)/(cupo*cant_bloque));
        else
        f_fem = 0;
        double tf = total_fem + Double.parseDouble(vec_total_fem.get(vec_total_fem.size()-1)+"");
        vec_total_fem.set(vec_total_fem.size()-1,tf+"");

        total_dia_fem += f_fem;
        if (escribetotal){ %>
        <td width="8%" align="center"><%=total_masc%></td>
        <td width="8%" align="center"><%=total_fem%></td>
        <td width="8%" align="center"><%=FMT_dec.format(f_masc)%></td>
        <td width="8%" align="center"><%=FMT_dec.format(f_fem)%></td><%} %>
      </tr>
      <%
      dia++;
      vec_colspan.addElement(colspan);
      colspan = "";
      total_masc = 0;
      f_masc = 0;
      cant_masc = 0;
      cant_bloque = 0;
      total_fem = 0;
      f_fem = 0;
      cant_fem = 0;
      if (escribetotal){ %>
      <tr bgcolor="#FFFFFF">
        <td>&nbsp;</td>
        <td align="right" bgcolor="#CCCCCC" ><strong>Total D&iacute;a </strong></td>
        <%for (int x=0; x < vec_total_masc.size();x++){%>
          <td align="center"  bgcolor="#CCCCCC" ><%=vec_total_masc.get(x)%></td>
          <td align="center" bgcolor="#CCCCCC"  ><%=vec_total_fem.get(x)%></td>

          <%}%>
          <td align="center" bgcolor="#CCCCCC"  ><%=FMT_dec.format(total_dia_masc)%></td>
          <td align="center" bgcolor="#CCCCCC"  ><%=FMT_dec.format(total_dia_fem)%></td>

      </tr>
      <% }  for (int x=0; x < vec_total_masc.size();x++){
      vec_total_masc.set(x,"0") ;
    }
    total_dia_masc = 0.00;
    for (int x=0; x < vec_total_fem.size();x++){
      vec_total_fem.set(x,"0") ;
    }
    total_dia_fem = 0.00;

  }%>

  </table>
  <% existe = true;} %>
   <%}
   if (!existe && version==0 && regicount==0){%>
     <span class="Estilo1">No existen registros asociados.</span>
   	<% regicount = 1; %>
  <%}}}%>
  <br>
  <%if (sala==10336 || sala == 3) {
    vec_bloque = general.getHoraBloque(10336, anno, semestre);
    vec_aerobica = general.getPorcentajeUsoDetalle(anno, semestre, f1, f2,10336, version );
    %>
  <table width="100%"  border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td width="100%" height="20" valign="bottom" class="letra10">SALA MULTIUSO </td>
    </tr>
  </table>
  <% regicount =0; %>
  <br>
  <%if(version==0){
  dia = 1;
  if (vec_aerobica.size() > 0){
    while (dia < 7){
      num_dia = 0;
      nom_actividad_ant = "";
      vec_dia = new Vector();
      vec_num_dia = new Vector();
      vec_total_masc = new Vector();
      vec_total_porc = new Vector();
      %>
      <table width="100%"  border="1" cellpadding="2" cellspacing="0">
        <tr valign="bottom" bgcolor="#FFFFFF">
          <td rowspan="2" bgcolor="#CCCCCC"><strong>D&iacute;a</strong></td>
          <td rowspan="2" bgcolor="#CCCCCC"><strong>Actividad</strong></td>
          <td rowspan="2" align="center" bgcolor="#CCCCCC"><strong>Cupo</strong></td>
          <td rowspan="2" align="right" bgcolor="#CCCCCC"><strong>Bloque</strong></td>
          <%for (int i=0;i<vec_aerobica.size();i++){
          num_dia = Integer.parseInt(((Vector)vec_aerobica.get(i)).get(2)+"");
          if (num_dia == dia){
            nom_fecha = ((Vector)vec_aerobica.get(i)).get(1)+"";
            if (vec_dia.size() == 0 ||   !vec_dia.contains(nom_fecha)){
              vec_dia.addElement(nom_fecha);
              vec_num_dia.addElement(((Vector)vec_aerobica.get(i)).get(7)+"");
              vec_total_masc.addElement("0");
              vec_total_porc.addElement("0");
              %>
              <td height="11%" colspan="2" align="center" bgcolor="#CCCCCC" class="letra7"><strong><%=nom_fecha%></strong></td>
              <%}}} %>
        </tr>
        <tr valign="bottom" bgcolor="#FFFFFF">
          <%for (int i=0;i<vec_dia.size();i++){%>
            <td align="center" bgcolor="#CCCCCC" class="letra7"><strong>Asistencia</strong></td>
            <td height="11%" align="center" bgcolor="#CCCCCC" class="letra7"><strong>%Uso</strong></td>
            <%}%>
        </tr>

        <%  hora_inicio_ant = "";
        int j = 0;
        for (int i=0;i<vec_aerobica.size();i++){
          num_dia = Integer.parseInt(((Vector)vec_aerobica.get(i)).get(2)+"");
          if (num_dia == dia){
            nom_fecha = ((Vector)vec_aerobica.get(i)).get(1)+"";
            num_fecha = Integer.parseInt(((Vector)vec_aerobica.get(i)).get(7)+"");
            nom_actividad =  ((Vector)vec_aerobica.get(i)).get(8)+"";
            hora_inicio = ((Vector)vec_aerobica.get(i)).get(10)+"";
            hora_fin = ((Vector)vec_aerobica.get(i)).get(11)+"";

            if (!nom_actividad.trim().equals(nom_actividad_ant.trim()) || !hora_inicio.trim().equals(hora_inicio_ant.trim())){
              nom_dia = ((Vector)vec_aerobica.get(i)).get(0)+"";
              cupo = Integer.parseInt(((Vector)vec_aerobica.get(i)).get(9)+"");
              j = 0;
              %>
              <tr bgcolor="#FFFFFF">
                <td width="5%"><strong> <%=(!nom_dia.trim().equals(nom_dia_ant.trim()))?nom_dia:"&nbsp;"%></strong></td>
                <td width="14%"><%=(!nom_actividad.trim().equals(nom_actividad_ant.trim()))?nom_actividad:"&nbsp;"%></td>
                <td width="3%" align="center"><%=(!nom_actividad.trim().equals(nom_actividad_ant.trim()))?String.valueOf(cupo):"&nbsp;"%></td>
                <%     }
                masc =  ((Vector)vec_aerobica.get(i)).get(5)+"";
                int yy = 0;
                if (!masc.trim().equals("Anul.") && !masc.trim().equals("Susp.")) {
                  f_masc = ((double)(100*Integer.parseInt(masc))/(cupo));
                  yy = Integer.parseInt(masc);
                } else {
                  f_masc = 0;
                  yy = 0;
                }
                if(!hora_inicio.trim().equals(hora_inicio_ant.trim())){
                  %>

                  <td width="4%" align="right"><%=hora_inicio%><br> <%=hora_fin%> </td>
                  <%}
                  if (j < vec_num_dia.size() && (vec_num_dia.get(j)+"").trim().equals(String.valueOf(num_fecha))){%>

                    <td width="9%" align="center"><%=(!masc.trim().equals(""))?masc:"&nbsp;"%></td>
                    <td width="10%" align="center"><strong><%=FMT_dec.format(f_masc)%></strong></td>
                    <%  int tt = yy + Integer.parseInt(vec_total_masc.get(j)+"");
                    vec_total_masc.set(j,tt+"");
                    double t = f_masc + Double.parseDouble(vec_total_porc.get(j)+"");
                    vec_total_porc.set(j,t+"");
                    j++;
                  } else {

                    while(j < vec_num_dia.size() &&   num_fecha < Integer.parseInt(vec_num_dia.get(j)+"")) {%>
                    <td width="9%" align="center">&nbsp;</td>
                    <td width="10%" align="center"><strong>&nbsp;</strong></td>
                    <%j++;
                    }}%>
                    <%
                    nom_actividad_ant = nom_actividad;
                    nom_dia_ant = nom_dia;
                    hora_inicio_ant = hora_inicio;
                    if (!nom_actividad.trim().equals(nom_actividad_ant.trim()) || !hora_inicio.trim().equals(hora_inicio_ant.trim())){
                      %></tr>
                      <%}} %> <%} %>
                      <tr bgcolor="#CCCCCC">
                        <td colspan="4" align="right"><strong>Total</strong></td>
                        <%for (int i=0;i<vec_dia.size();i++){%>
                          <td align="center"><%=vec_total_masc.get(i)%></td>
                          <td height="16%" align="center"><%=FMT_dec.format(Double.parseDouble(vec_total_porc.get(i)+"")) %></td>
                          <%} %>
                      </tr>

      </table>
      <br>
      <%dia++;
      }}}%>

      <br>
      <%if (version==1){
      dia = 1;
      if (vec_aerobica.size() > 0){
        while (dia < 7){
          num_dia = 0;
          nom_actividad_ant = "";
          vec_dia = new Vector();
          vec_num_dia = new Vector();
          vec_total_masc = new Vector();
          vec_total_fem = new Vector();
          vec_total_porc = new Vector();
          vec_total_porc_fem = new Vector();
          %>
          <br>
          <table width="100%"  border="1" cellpadding="2" cellspacing="0">
            <tr valign="bottom" bgcolor="#FFFFFF">
              <td rowspan="3" bgcolor="#CCCCCC"><strong>D&iacute;a</strong></td>
              <td rowspan="3" bgcolor="#CCCCCC"><strong>Actividad</strong></td>
              <td rowspan="3" align="center" bgcolor="#CCCCCC"><strong>Cupo</strong></td>
              <td rowspan="3" align="right" bgcolor="#CCCCCC"><strong>Bloque</strong></td>
              <%for (int i=0;i<vec_aerobica.size();i++){
              num_dia = Integer.parseInt(((Vector)vec_aerobica.get(i)).get(2)+"");
              if (num_dia == dia){
                nom_fecha = ((Vector)vec_aerobica.get(i)).get(1)+"";
                if (vec_dia.size() == 0 ||   !vec_dia.contains(nom_fecha)){
                  vec_dia.addElement(nom_fecha);
                  vec_num_dia.addElement(((Vector)vec_aerobica.get(i)).get(7)+"");
                  vec_total_masc.addElement("0");
                  vec_total_fem.addElement("0");
                  vec_total_porc.addElement("0");
                  vec_total_porc_fem.addElement("0");
                  %>
                  <td height="11%" colspan="5" align="center" bgcolor="#CCCCCC" class="letra7"><strong><%=nom_fecha%></strong></td>
                  <%}}} %>
            </tr>
            <tr valign="bottom" bgcolor="#FFFFFF">
              <%for (int i=0;i<vec_dia.size();i++){%>
                <td colspan="2" align="center" bgcolor="#CCCCCC" class="letra7"><strong>Asist.</strong></td>
                <td height="11%" colspan="3" align="center" bgcolor="#CCCCCC" class="letra7"><strong>%Uso</strong></td>
                <%}%>
            </tr>
            <tr valign="bottom" bgcolor="#FFFFFF">
              <%for (int i=0;i<vec_dia.size();i++){%>
                <td align="center" bgcolor="#CCCCCC" class="letra7">M</td>
                <td align="center" bgcolor="#CCCCCC" class="letra7">F</td>
                <td align="center" bgcolor="#CCCCCC" class="letra7">M</td>
                <td align="center" bgcolor="#CCCCCC" class="letra7">F</td>
                <td align="center" bgcolor="#CCCCCC" class="letra7">T</td>
                <%}%>
            </tr>
            <%  hora_inicio_ant = "";
            int j = 0;
            for (int i=0;i<vec_aerobica.size();i++){
              num_dia = Integer.parseInt(((Vector)vec_aerobica.get(i)).get(2)+"");
              if (num_dia == dia){
                nom_fecha = ((Vector)vec_aerobica.get(i)).get(1)+"";
                num_fecha = Integer.parseInt(((Vector)vec_aerobica.get(i)).get(7)+"");
                nom_actividad =  ((Vector)vec_aerobica.get(i)).get(8)+"";
                hora_inicio = ((Vector)vec_aerobica.get(i)).get(10)+"";
                hora_fin = ((Vector)vec_aerobica.get(i)).get(11)+"";

                if (!nom_actividad.trim().equals(nom_actividad_ant.trim()) || !hora_inicio.trim().equals(hora_inicio_ant.trim())){
                  nom_dia = ((Vector)vec_aerobica.get(i)).get(0)+"";
                  cupo = Integer.parseInt(((Vector)vec_aerobica.get(i)).get(9)+"");
                  j = 0;
                  %>
                  <tr bgcolor="#FFFFFF">
                    <td width="6%"><%=(!nom_dia.trim().equals(nom_dia_ant.trim()))?nom_dia:"&nbsp;"%></td>
                    <td width="7%"><%=(!nom_actividad.trim().equals(nom_actividad_ant.trim()))?nom_actividad:"&nbsp;"%></td>
                    <td width="3%" align="center"><%=(!nom_actividad.trim().equals(nom_actividad_ant.trim()))?String.valueOf(cupo):"&nbsp;"%></td>
                    <%     }
                    masc =  ((Vector)vec_aerobica.get(i)).get(5)+"";
                    int yy = 0;
                    if (!masc.trim().equals("Anul.") && !masc.trim().equals("Susp.")) {
                      f_masc = ((double)(100*Integer.parseInt(masc))/(cupo));
                      yy = Integer.parseInt(masc);
                    } else {
                      f_masc = 0;
                      yy = 0;
                    }
                    fem =  ((Vector)vec_aerobica.get(i)).get(6)+"";
                    int yf = 0;
                    if (!fem.trim().equals("Anul.") && !fem.trim().equals("Susp.")) {
                      f_fem = ((double)(100*Integer.parseInt(fem))/(cupo));
                      yf = Integer.parseInt(fem);
                    } else {
                      f_fem = 0;
                      yf = 0;
                    }

                    if(!hora_inicio.trim().equals(hora_inicio_ant.trim())){
                      %>

                      <td width="4%" align="right"><%=hora_inicio%><br> <%=hora_fin%> </td>
                      <%}
                      if (j < vec_num_dia.size() && (vec_num_dia.get(j)+"").trim().equals(String.valueOf(num_fecha))){%>

                        <td width="5%" align="center" class="letra7"><%=(!masc.trim().equals(""))?masc:"&nbsp;"%></td>
                        <td width="6%" align="center" class="letra7"><%=(!fem.trim().equals(""))?fem:"&nbsp;"%></td>
                        <td width="2%" align="center" class="letra7"><strong><%=FMT_dec.format(f_masc)%></strong></td>
                        <td width="3%" align="center" class="letra7"><strong><%=FMT_dec.format(f_fem)%></strong></td>
                        <td width="3%" align="center" class="letra7"><strong><%=FMT_dec.format(f_masc+f_fem)%></strong></td>
                        <%  int tt = yy + Integer.parseInt(vec_total_masc.get(j)+"");
                        vec_total_masc.set(j,tt+"");
                        double t = f_masc + Double.parseDouble(vec_total_porc.get(j)+"");
                        vec_total_porc.set(j,t+"");
                        tt = yf + Integer.parseInt(vec_total_fem.get(j)+"");
                        vec_total_fem.set(j,tt+"");
                        t = f_fem + Double.parseDouble(vec_total_porc_fem.get(j)+"");
                        vec_total_porc_fem.set(j,t+"");
                        j++;
                      } else {

                        while(j < vec_num_dia.size() &&   num_fecha < Integer.parseInt(vec_num_dia.get(j)+"")) {%>
                        <td >&nbsp;</td>
                        <td ><strong>&nbsp;</strong></td>
                        <td >&nbsp;</td>
                        <td ><strong>&nbsp;</strong></td>
                        <td ><strong>&nbsp;</strong></td>
                        <%j++;
                        }}%>
                        <%
                        nom_actividad_ant = nom_actividad;
                        nom_dia_ant = nom_dia;
                        hora_inicio_ant = hora_inicio;
                        if (!nom_actividad.trim().equals(nom_actividad_ant.trim()) || !hora_inicio.trim().equals(hora_inicio_ant.trim())){
                          %></tr>
                          <%}} %> <%} %>
                          <tr bgcolor="#CCCCCC">
                            <td colspan="4" align="right"><strong>Total</strong></td>
                            <%for (int i=0;i<vec_dia.size();i++){%>
                              <td align="center"><%=vec_total_masc.get(i)%></td>
                              <td align="center"><%=vec_total_fem.get(i)%></td>
                              <td height="16%" align="center"><%=FMT_dec.format(Double.parseDouble(vec_total_porc.get(i)+"")) %></td>
                              <td height="16%" align="center"><%=FMT_dec.format(Double.parseDouble(vec_total_porc_fem.get(i)+"")) %></td>
                              <td height="16%" align="center"><%=FMT_dec.format(Double.parseDouble(vec_total_porc.get(i)+"") + Double.parseDouble(vec_total_porc_fem.get(i)+"")) %></td>
                              <%} %>
                          </tr>
          </table>
          <br>
          <%dia++;
          }}%>
          <%}}%>
</body>
</html>
<%general.getCerrarConexion(); %>
