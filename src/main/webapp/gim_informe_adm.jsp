<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<%
// Creado por           : M�nica Barrera Frez
// Fecha                : 26/09/2007
// �ltima Actualizaci�n :
// Informe.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");


  int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int cod_perfil  = session.getAttribute("cod_perfil") != null?
                    Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
  int sala        = request.getParameter("sala")!=null &&
                    !request.getParameter("sala").equals("null") &&
                    !request.getParameter("sala").equals("NULL") &&
                    !request.getParameter("sala").equals("")  &&
                    !request.getParameter("sala").equals(" ")?
                    Integer.parseInt(request.getParameter("sala")+""):0;

  int tipo_listado = request.getParameter("tipo_listado") != null &&
                     !request.getParameter("tipo_listado").trim().equals("")?
                     Integer.parseInt(request.getParameter("tipo_listado")):0;

  int bloque      = request.getParameter("bloque")!=null &&
                     !request.getParameter("bloque").equals("null") &&
                     !request.getParameter("bloque").equals("NULL") &&
                     !request.getParameter("bloque").equals("")  &&
                     !request.getParameter("bloque").equals(" ")?
                     Integer.parseInt(request.getParameter("bloque")+""):-1;
  int bloque1     = request.getParameter("bloque1")!=null &&
                      !request.getParameter("bloque1").equals("null") &&
                      !request.getParameter("bloque1").equals("NULL") &&
                      !request.getParameter("bloque1").equals("")  &&
                      !request.getParameter("bloque1").equals(" ")?
                      Integer.parseInt(request.getParameter("bloque1")+""):0;
    int bloque2     = request.getParameter("bloque2")!=null &&
                      !request.getParameter("bloque2").equals("null") &&
                      !request.getParameter("bloque2").equals("NULL") &&
                      !request.getParameter("bloque2").equals("")  &&
                      !request.getParameter("bloque2").equals(" ")?
                      Integer.parseInt(request.getParameter("bloque2")+""):0;
//    int anno     = request.getParameter("anno")!=null &&
//                  !request.getParameter("anno").equals("null") &&
//                  !request.getParameter("anno").equals("NULL") &&
//                  !request.getParameter("anno").equals("")  &&
//                  !request.getParameter("anno").equals(" ")?
//                      Integer.parseInt(request.getParameter("anno")+""):0;
//    int   semestre =  request.getParameter("semestre")!=null &&
//                            !request.getParameter("semestre").equals("null") &&
//                            !request.getParameter("semestre").equals("NULL") &&
//                            !request.getParameter("semestre").equals("")  &&
//                            !request.getParameter("semestre").equals(" ")
//                            ?Integer.parseInt(request.getParameter("semestre")+""):0;

    String periodo    = request.getParameter("periodo")!=null &&
                        !request.getParameter("periodo").equals("null") &&
                        !request.getParameter("periodo").equals("NULL") &&
                        !request.getParameter("periodo").equals("")  &&
                        !request.getParameter("periodo").equals(" ")?
                        request.getParameter("periodo")+"":"";
    String fecha    = request.getParameter("fecha_inicio")!=null &&
                      !request.getParameter("fecha_inicio").equals("null") &&
                      !request.getParameter("fecha_inicio").equals("NULL") &&
                      !request.getParameter("fecha_inicio").equals("")  &&
                      !request.getParameter("fecha_inicio").equals(" ")?
                      request.getParameter("fecha_inicio")+"":"";

  int anno = 0;
  int semestre = 0;
   if (!periodo.trim().equals("") && !periodo.trim().equals("0")){
    anno = Integer.parseInt(periodo.substring(0,4));
    semestre = Integer.parseInt(periodo.substring(4,periodo.length()));
  }

  Vector vec_horariodia = new Vector();
  Vector vec_periodo = new Vector();
  java.util.Calendar f = new java.util.GregorianCalendar();
      int a�o = f.get(java.util.Calendar.YEAR);
      int dia_actual = f.get(java.util.Calendar.DAY_OF_MONTH);
      int mes_actual = f.get(java.util.Calendar.MONTH)+1;
      String fecha_actual = (dia_actual<10)?"0"+dia_actual:dia_actual+"";
      if (mes_actual < 10) fecha_actual += "/0"+ mes_actual+"/"+ a�o;
      else fecha_actual += "/"+ mes_actual +"/"+a�o;
    if (fecha.trim().equals("") ) fecha = fecha_actual;

  general.getAbrirConexion();
  general.setGenerarConexionHorario();
  general.setGenerarConexionReserva();
  general.setGenerarConexionRegistro();
  vec_periodo = general.getProgramacionPeriodo();


  int tipo_usuario = 0;

  if (periodo.trim().equals("") || periodo.trim().equals("0")){
    general.getPeriodoSemestreActual(1,1);
    int a�o_per = general.getA�oActual();
    int sem_per = general.getSemestreActual();
    periodo = String.valueOf(a�o_per) + String.valueOf(sem_per);
  }
    general.getCerrarConexion();
  // Alumnos de Tutor�a
  if (tipo_listado == 3) {
    tipo_usuario = 2;
  }
  // Alumnos de Rama Deportiva
  if (tipo_listado == 4) {
    tipo_usuario = 3;
  }
  // Alumnos (normal)
  if (tipo_listado == 5){
    tipo_usuario = 1;
  }
  // Funcionarios
  if (tipo_listado == 6) {
    tipo_usuario = 4;
  }


%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Gimnasio - Informe</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <!-Calendario -->
<link type="text/css" rel="stylesheet" href="js/dhtml_calendar/dhtml_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="js/dhtml_calendar/dhtml_calendar.js?random=20060118"></script>

<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script src="js/centrar.js"></script>
<script language="JavaScript" type="text/JavaScript">
  function AbrirVentana(f,a) {
    var alto  = 550;
    var ancho = 800;
    f.target = "ventana";
    f.action = a;
    var par = 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=yes';
    var w = Centrar03(f.target,alto,ancho,par);
    w.focus();
    f.submit();
  }

  function CambiaCampoEntrada(){
    document.form_listado.action="gim_informe_adm.jsp";
    document.form_listado.target="_self";
    document.form_listado.submit();
  }
  function Generar(accion){
    <%if (tipo_listado != 7){%>
      if (document.form_listado.periodo.value == "" || document.form_listado.periodo.value == "0"){
        alert ('Debe seleccionar un per�odo.');
        document.form_listado.periodo.focus();
         return false;
      }
    if (accion == 1) {
      if (document.form_listado.sala.value == "0" ){
      alert ('Debe seleccionar una sala.');
      document.form_listado.sala.focus();
       return false;
      }
      if (document.form_listado.fecha_inicio.value == "" ){
        alert ('Debe ingresar fecha.');
        document.form_listado.fecha_inicio.focus();
        return false;
      }
      if (document.form_listado.bloque.value == "" ){
        alert ('Debe seleccionar un bloque de horario.');
        document.form_listado.bloque.focus();
        return false;
      }
    }
    periodo = document.form_listado.periodo.value;
    document.form_listado.anno.value = periodo.substring(0,4);
    document.form_listado.semestre.value = periodo.substring(4);
    <%}%>
    AbrirVentana(document.form_listado,"gim_generar.jsp");
  }
  function BuscaDia(){
    if (document.form_listado.fecha_inicio != null){
  arr_dia  = new Array("LUNES","MARTES","MI�RCOLES","JUEVES","VIERNES","S�BADO","DOMINGO");

  var fecha = document.form_listado.fecha_inicio.value;
  if (fecha > "") {
    var dd = (fecha.substring(0,2)/1);
    var mm = (fecha.substring(3,5)/1) -1;
    var aa = (fecha.substring(6,10)/1);
    var fdate = new Date (aa, mm, dd);
    var dia = fdate.getDay();
    if (dia > 0) {
      document.form_listado.dia.value = dia;
      document.form_listado.nom_dia.value = arr_dia[dia-1];
      document.getElementById("texto_dia").innerHTML = arr_dia[dia-1];
    //  document.form.listado.fecha.value = fecha.substring(6,10) + fecha.substring(3,5) + fecha.substring(0,2);
      CambiaDia();
    }
    else alert("Seleccione un d�a de LUNES a S�BADO.");
  }
  else alert("Seleccione una fecha.");
    }
}


arr_bloque_Cod   = new Array();
arr_bloque_Text  = new Array();
arr_bloque_Prof  = new Array();
arr_bloque_Rese  = new Array();
arr_bloque_Blo1  = new Array();
arr_bloque_Blo2  = new Array();
arr_bloque_Asign  = new Array();
arr_bloque_Par  = new Array();

<% if (anno > 0 && semestre > 0 &&  sala > 0 && !fecha.trim().equals("")){
  StringBuffer bloque_Cod = new StringBuffer(); StringBuffer bloque_Text = new StringBuffer(); StringBuffer bloque_Prof = new StringBuffer(); StringBuffer bloque_Rese = new StringBuffer(); StringBuffer bloque_Blo1 = new StringBuffer(); StringBuffer bloque_Blo2 = new StringBuffer(); StringBuffer bloque_Asign = new StringBuffer();  StringBuffer bloque_Par = new StringBuffer();
  general.getAbrirConexion(); general.setGenerarConexionHorario();
  String ff = fecha.substring(6,10) +fecha.substring(3,5) + fecha.substring(0,2);

for (int d = 0; d < 6; d++){
  bloque_Cod = new StringBuffer();
  bloque_Text = new StringBuffer();
  bloque_Prof = new StringBuffer();
  bloque_Rese = new StringBuffer();
  bloque_Blo1 = new StringBuffer();
  bloque_Blo2 = new StringBuffer();
  bloque_Asign = new StringBuffer();
  bloque_Par = new StringBuffer();



   vec_horariodia = general.getHorarioDia (anno, semestre,  sala, d+1, ff);
   bloque_Cod.append("\"0\"");
  if (vec_horariodia.size() == 0) bloque_Text.append("\"-- Sin asignaci�n de horario --\"");
  else bloque_Text.append("\"-- Seleccione --\"");
  bloque_Prof .append("\"\"");
  bloque_Rese.append("\"\"");
  bloque_Blo1.append("\"0\"");
  bloque_Blo2.append("\"0\"");
  bloque_Asign.append("\"0\"");
  bloque_Par.append("\"0\"");

  for (int i = 0; i < vec_horariodia.size();i++){
    bloque_Cod.append(",\"");
    bloque_Cod.append(i); // indice
    bloque_Cod.append("\"");

    bloque_Text.append(",\"");
    if (sala != 10335) {
      bloque_Text.append(((Vector)vec_horariodia.get(i)).get(7));
      bloque_Text.append(" - ");
    }
    bloque_Text.append(((Vector)vec_horariodia.get(i)).get(4));
    bloque_Text.append(" a ");
    bloque_Text.append(((Vector)vec_horariodia.get(i)).get(5));
    bloque_Text.append("\"");
    // Profesor
    bloque_Prof.append(",\"");
    bloque_Prof.append(((Vector)vec_horariodia.get(i)).get(9));
    bloque_Prof.append("\"");
    // tipo reserva
    bloque_Rese.append(",\"");
    bloque_Rese.append(((Vector)vec_horariodia.get(i)).get(8));
    bloque_Rese.append("\"");
    // bloque 1
    bloque_Blo1.append(",\"");
    bloque_Blo1.append(((Vector)vec_horariodia.get(i)).get(2));
    bloque_Blo1.append("\"");
    // bloque 2
    bloque_Blo2.append(",\"");
    bloque_Blo2.append(((Vector)vec_horariodia.get(i)).get(3));
    bloque_Blo2.append("\"");
    // cod_asign
    bloque_Asign.append(",\"");
    bloque_Asign.append(((Vector)vec_horariodia.get(i)).get(0));
    bloque_Asign.append("\"");
    // paralelo
    bloque_Par.append(",\"");
    bloque_Par.append(((Vector)vec_horariodia.get(i)).get(1));
    bloque_Par.append("\"");
  }
  out.println("arr_bloque_Cod["+d+"]  = new Array("+bloque_Cod+");"); // codigo
  out.println("arr_bloque_Text["+d+"] = new Array("+bloque_Text+");"); // texto
  out.println("arr_bloque_Prof["+d+"] = new Array("+bloque_Prof+");"); // Profesor
  out.println("arr_bloque_Rese["+d+"] = new Array("+bloque_Rese+");"); // Tipo reserva
  out.println("arr_bloque_Blo1["+d+"] = new Array("+bloque_Blo1+");"); // Bloque 1
  out.println("arr_bloque_Blo2["+d+"] = new Array("+bloque_Blo2+");"); // Bloque 2
  out.println("arr_bloque_Asign["+d+"] = new Array("+bloque_Asign+");"); // cod asign
  out.println("arr_bloque_Par["+d+"] = new Array("+bloque_Par+");"); // paralelo
} general.getCerrarConexion();} %>
function CambiaDia(){

var dia = (document.form_listado.dia.value/1)-1
// Limpia el combo
document.form_listado.bloque.options.length = 0; // Ciclo que llena el combo
if (dia >= 0) {
  for(i = 0; i < arr_bloque_Cod[dia].length; i++) {
    // Crea OPTION del SELECT
    var option = new
          Option(arr_bloque_Text[dia][i],arr_bloque_Cod[dia][i]);
    document.form_listado.bloque.options[i] = option;
    if(arr_bloque_Cod[dia][i] == <%=bloque%>){
      document.form_listado.bloque.options[i].selected = true;
    }
  }
}
CambiaBloque();
}
function CambiaBloque(){
  var dia = (document.form_listado.dia.value/1)-1;
  var bloque = document.form_listado.bloque.selectedIndex;
  document.getElementById("texto_tit_prof").innerHTML = "";
//  document.getElementById("texto_tit_tipo").innerHTML = "";
  document.getElementById("texto_prof").innerHTML = "";
//  document.getElementById("texto_tipo").innerHTML = "";
 // escondeBloque('BLOQUE_BtnConsultar', 1); // escondo
  document.form_listado.bloque1.value = "0";
  document.form_listado.bloque2.value = "0";
  if (dia >= 0 && bloque > 0) {
    document.getElementById("texto_tit_prof").innerHTML = "Profesor";
   // document.getElementById("texto_tit_tipo").innerHTML = ""; // Clase
    document.getElementById("texto_prof").innerHTML = arr_bloque_Prof[dia][bloque] +
                                                      " (" + arr_bloque_Rese[dia][bloque] + ")";
  //  document.getElementById("texto_tipo").innerHTML = ""; // arr_bloque_Rese[dia][bloque]
    document.form_listado.bloque1.value = arr_bloque_Blo1[dia][bloque];
    document.form_listado.bloque2.value = arr_bloque_Blo2[dia][bloque];
    document.form_listado.texto.value = arr_bloque_Text[dia][bloque];
//    document.form_accion.cod_asign.value = arr_bloque_Asign[dia][bloque];
//    document.form_accion.paralelo.value = arr_bloque_Par[dia][bloque];
//    document.form_accion.bloque1.value = arr_bloque_Blo1[dia][bloque];
//    document.form_accion.bloque2.value = arr_bloque_Blo2[dia][bloque];
    document.form_listado.cod_asign.value = arr_bloque_Asign[dia][bloque];
    document.form_listado.paralelo.value = arr_bloque_Par[dia][bloque];
//    document.form_datos.cod_asign.value = arr_bloque_Asign[dia][bloque];
//    document.form_datos.paralelo.value = arr_bloque_Par[dia][bloque];
  //  escondeBloque('BLOQUE_BtnConsultar', 2); // muestro
  }
}
// End -->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style></head>

<body onload=" if (<%=!fecha.trim().equals("")%>) BuscaDia();">
<form name="form_listado" method="post">
  <input type="hidden" name="bloque1" value="<%=bloque1%>">
  <input type="hidden" name="bloque2" value="<%=bloque2%>">
  <input type="hidden" name="cod_asign" value="">
  <input type="hidden" name="paralelo" value="">
  <input type="hidden" name="anno" value="<%=anno%>">
  <input type="hidden" name="semestre" value="<%=semestre%>">
  <input type="hidden" name="tipo_usuario" value="<%=tipo_usuario%>">
<input type="hidden" name="texto" value="">

<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="30" class="Estilo_Verde">INFORME</td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td width="11%" class="Estilo_Gris">Listado</td>
    <td width="89%" bgcolor="#FFFFFF"><select name="tipo_listado" class="Option" onChange="CambiaCampoEntrada()">
        <option value="0" <%=tipo_listado==0?"selected":""%>>--Seleccione --</option>
        <option value="1" <%=tipo_listado==1?"selected":""%>>Inscripciones por bloque</option>
        <option value="2" <%=tipo_listado==2?"selected":""%>>Asistencia por bloque</option>
        <option value="8" <%=tipo_listado==8?"selected":""%>>Asistencia de Tutor�a</option>
        <option value="3" <%=tipo_listado==3?"selected":""%>>Alumnos de Tutor&iacute;a</option>
        <option value="4" <%=tipo_listado==4?"selected":""%>>Alumnos de Rama Deportiva</option>
        <option value="5" <%=tipo_listado==5?"selected":""%>>Alumnos (normal)</option>
        <option value="6" <%=tipo_listado==6?"selected":""%>>Funcionarios</option>
        <option value="7" <%=tipo_listado==7?"selected":""%>>Ramas Deportivas</option>
    </select></td>
  </tr>
</table>
<%if (tipo_listado > 0 ) {%>
  <table width="100%"  border="0" cellpadding="2" cellspacing="0">

  <tr>
  <%if (tipo_listado != 7){%>
    <td width="11%" class="Estilo_Gris">Per&iacute;odo</td>
    <td width="15%"><select name="periodo" class="Option" id="periodo" onChange="CambiaCampoEntrada()">
      <%
      String per_actual = "";
      for (int i=0;vec_periodo.size()>0 && i < vec_periodo.size();i++){
      per_actual = ((Vector)vec_periodo.get(i)).get(0)+"" +((Vector)vec_periodo.get(i)).get(1)+"";%>
      <option value="<%=per_actual%>"
         <%=(per_actual.trim().equals(periodo.trim()) )?"selected":""%>> <%=((Vector)vec_periodo.get(i)).get(0)%> - <%=((Vector)vec_periodo.get(i)).get(1)%></option>
      <%}%>
    </select></td>
   <%}%>
    <td width="74%"></td>
  </tr>
  </table>
  <%if (tipo_listado >= 3) {%>
  <br>
  <table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseLEER.gif">
      <tr onClick="Generar(2)" onMouseOver="BotonOver(this,'Generar')" onMouseOut="BotonOut(this)">
        <td width="27">&nbsp;</td>
        <td width="64" class="Estilo_Azulino">Generar</td>
      </tr>
  </table><%}%>
  <%}%>
<% if (tipo_listado == 1 || tipo_listado == 2) {%>
<table width="100%"  border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td width="11%"><span class="Estilo_Gris">Sala</span></td>
    <td width="68%"><select name="sala" class="Option" id="sala" onchange="document.form_listado.submit();">
        <option value="0">-- Seleccione --</option>
        <option value="10335" <%=(sala == 10335)?"selected":""%>>M&aacute;quinas</option>
        <option value="10336" <%=(sala == 10336)?"selected":""%>>Multiuso</option>
    </select></td>
    <td width="21%">&nbsp;</td>
  </tr>
  <%if (sala > 0){%>
  <tr>
    <td><span class="Estilo_Gris">Fecha</span></td>
    <td><table width="86%"  border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="22%"><input  name="fecha_inicio" type="text" class="Input01" value="<%=fecha%>" size="10" maxlength="14" onChange="BuscaDia();document.form_listado.bloque.value=0;document.form_listado.submit();" readonly></td>
          <td width="78%" valign="bottom"><a href="javascript:void(0)" onclick="displayCalendar(document.form_listado.fecha_inicio,'dd/mm/yyyy',this)">
<img src="imagen/gim_calendario_verde.gif" border="0" alt="CALENDARIO" width="16" height="16"></a>
&nbsp;<b><span class="Estilo_Azul" id ="texto_dia"></span></b>
        <input type='hidden' name='dia' value='0'>
        <input type='hidden' name='nom_dia' value=''>
         </td>
        </tr>
        <%}%>
    </table></td>
    <td>&nbsp;</td>
  </tr>
    <%if (sala > 0){%>
  <tr>
    <td><span class="Estilo_Gris">Bloque</span></td>
    <td><span class="Estilo_Gris">
      <select name="bloque" class="Option" id="bloque" onChange="CambiaBloque()">
        <option value="0">-- Sin asignaci&oacute;n de horario --</option>
      </select>
    </span></td>
    <td>
     </td>
  </tr>
  <tr>
    <td height="16"><span class="Estilo_Gris" id ="texto_tit_prof"></span>&nbsp;</td>
    <td height="16"><span class="Estilo_Azul" id ="texto_prof"></span>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <%}%>
</table>
<%if (sala > 0){%>
<br>
<table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseLEER.gif">
        <tr onClick="Generar(1)" onMouseOver="BotonOver(this,'Generar')" onMouseOut="BotonOut(this)">
          <td width="27">&nbsp;</td>
          <td width="64" class="Estilo_Azulino">Generar</td>
        </tr>
</table>
 <%}%>
<%}%>
<input type="hidden" name="tipo" value="1">
</form>
</body>
</html>
