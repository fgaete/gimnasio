<%@ page import="java.util.*,java.net.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 01/06/2007
   // �ltima Actualizaci�n :
%>
<%
response.setHeader("Pragma","no-cache");
response.setDateHeader ("Expires", 0);
response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
%>
<jsp:useBean id="mant" class="gimnasio.BeanUsuarios"></jsp:useBean>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%
session.removeAttribute("rut_usuario");
int msj  =  request.getParameter("msj")!=null &&
            !request.getParameter("msj").equals("null") &&
            !request.getParameter("msj").equals("NULL") &&
            !request.getParameter("msj").equals("")  &&
            !request.getParameter("msj").equals(" ")?
            Integer.parseInt(request.getParameter("msj")+""):0;
int usuario = /*session.getAttribute("rut_usuario_intranet") != null?
              Integer.parseInt(session.getAttribute("rut_usuario_intranet")+""):*/0;
/* prueba */
//usuario = 9683011;
//session.setAttribute("rut_usuario_intranet",usuario+"");
/* fin prueba */

    InetAddress address = InetAddress.getLocalHost(); 
    String ip =address.getHostAddress(); 
    ip=ip.replace(".",";"); 
    String[] tokens =ip.split(";");
     
    String version = "";
     if (tokens.length > 0) {
      /* version = "<font color=grey style='margin-top:5px;font-family:arial;font-size:10px'>h."+ tokens[tokens.length-1] + " v.2.0</font>"; */
      /* version = "<font color=grey style='margin-top:5px;font-family:arial;font-size:10px'>h."+ tokens[tokens.length-1]+"</font>";*/
    	 version = "h."+ tokens[tokens.length-1];
      }

String nom_usuario = "";
Vector v = new Vector();
String mensaje = "";
if (usuario>0){
  mant.getAbrirConexion();
  mant.setGenerarConexionUsuario();
  v = mant.getFuncionario(usuario);
  mant.getCerrarConexion();
  if (v.size()>0) nom_usuario = v.get(1)+" "+v.get(2)+" "+v.get(3);
  else { // puede ser alumno CC
    general.getAbrirConexion();
    general.setGenerarConexionDatosAl();
    v = general.getAlumno(usuario);
    general.getCerrarConexion();
     //if (v.size() == 0){ // puede ser alumno Vi�a
     // general.getAbrirConexionVi�a();
     // general.setGenerarConexionDatosVi�a();
     // v = general.getAlumnoVi�a(usuario);
     // if (v.size() == 0){ // puede ser funcionario Vi�a
     //   general.getFuncionarioVi�a(usuario);
     // }
     // general.getCerrarConexionVi�a();
     // }
    if (v.size() > 0) {
      nom_usuario = v.get(1)+"";
    }
  }

}  switch (msj){
    case 1: mensaje = "Usuario / Clave inv�lidos";
      break;
    case 2: case 3: mensaje = "Acceso Denegado";
      break;
    case 4: mensaje = "Sistema en Mantenci�n";
      break;
  }

  // noticias
  general.getAbrirConexion();
  general.setGenerarConexionBitacora();
  Vector v_noticias = general.getNoticias();
  general.getCerrarConexion();

%>
<html>
<head>
<title>Gimnasio III</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/fecha.js"></script>
<script src="js/valida_login.js"></script>
<script src="js/centrar.js"></script>
<script language="JavaScript">
<% if (usuario == 0) {%>
function noenter() {
    return !(window.event && window.event.keyCode == 13);
}

function enter(e,t){
    if (window.event) {
      if (window.event.keyCode == 13) {
        if (t==1) document.Formulario.passwd.focus();
        else ValidaLogin(document.Formulario);
      }
    }
    else {
      if (e.which == 13){
        if (t==1) document.Formulario.passwd.focus();
        else ValidaLogin(document.Formulario);
      }
    }
}
function ValidaLogin(f) {

  if ( f.login.value.length == 0 ){
                alert( "Ingrese su nombre de usuario.\nEjemplo: nombre.apellido");
                f.login.focus();
                return;
  }
  if ( f.passwd.value.length == 0 ) {
                alert( "Ingrese su clave de su cuenta e-mail USM.");
                f.passwd.focus();
                return;
  }
  f.submit();
  return;
}
<%}%>
  function Aviso(){
    var par = 'toolbar=no,status=no,menubar=no,scrollbars=auto,resizable=yes';
    var w = Centrar02("gim_pop_up.html","aviso",250,550,par);
    w.focus();
}
function ProblemaClave(){
  var par = 'toolbar=no,status=no,menubar=no,scrollbars=auto,resizable=yes';
  var w = Centrar02("problema_clave.html","problema",300,600,par);
  w.focus();
}
var TheBrowserName = navigator.appName;
var TheBrowserVersion = parseFloat(navigator.appVersion);

function CursorOn(t){
  if ((TheBrowserName == 'Netscape') && (TheBrowserVersion >= 5)) {
    t.style.cursor="pointer";
  } else {
    t.style.cursor="hand";
  }
}
function CursorOff(t){
  t.style.cursor="default";
}
</script>
<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
body {
	background-color: #E7E7E7;
}
.Cgris { font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;}
-->
</style>
</head>
<body onLoad="<%=(v_noticias.size() > 0)?"populate();":""%><% if(usuario == 0) {%>document.Formulario.login.focus(); <%}%>fecha();hora();">
<table width="800"  height="570" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#001930">
  <tr>
    <td valign="middle">
    <table width="800" height="145" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
      <tr>
        <td height="145" align="center" background="imagen/gim_logo_borde_i.gif">&nbsp;</td>
        <td width="788" height="145" align="center" valign="top"><img name="logo" src="imagen/gim_logo.gif" width="788" height="145" border="0" alt=""></td>
        <td height="145" align="center" background="imagen/gim_logo_borde.gif">&nbsp;</td>
      </tr>
     </table>
     <table width="800" height="21" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
      <tr>
        <td height="20">&nbsp;<a href="http://www.usm.cl" target="new_win">
        <img src="imagen/gim_link_usm.gif" alt="" name="usm" width="131" height="7" border="0"></a>
        </td>
        <td width="540" height="20" valign="bottom"><table width="100%"  border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td align="right"><%if (usuario>0){%><a href="index.jsp" target="_parent"><img src="imagen/gim_finsesion.gif" alt="SALIR DEL SISTEMA" width="10" height="9" border="0"><span class="Estilo_AzulinoClaro letra8"> Cerrar</span></a><%}%>&nbsp;</td>
  </tr>
</table></td>
       </tr>
      <tr>
        <td height="1" colspan="2" align="center"><img name="linea" src="imagen/gim_linea_azul.gif" width="100%" height="1" border="0" alt=""></td>
      </tr>
</table>
    <table width="800" height="374"  border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
      <tr valign="top">
        <td colspan="3" align="center">
           <table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
            <tr>
              <td width="47%" valign="top">
                <table width="92%" cellpadding="3" cellspacing="5" border="0">
                  <tr>
                    <td height="35" colspan="2"><span class="Estilo_NaranjoOsc">Gimnasio de M&aacute;quinas y Sala Multiuso</span></td>
                  </tr>
                  <tr>
                    <td width="21%"><img src="imagen/gim_foto01.jpg" width="131" height="95"></td>
                    <td width="79%" rowspan="3" valign="top">
                    <font class="letra8" color="#003366"><b>Descripci&oacute;n general </b></font>
                      <p align="justify"><font class="letra8">El Gimnasio III es un recinto que ofrece actividades extraprogram�ticas a la
                      comunidad, con el fin de que los usuarios puedan recrearse y aprovechar el tiempo libre durante su permanencia
                      en la Universidad.</font></p>
                      <font class="letra8" color="#003366"><b>Horario de atenci&oacute;n</b></font>
                      <p align="justify"><font class="letra8"> Lunes a Viernes de 09:00 a 21:00 hrs. <br>
                      S&aacute;bado de 09:00 a 17:25 hrs.</font>

                       </p>
                      <font class="letra8" color="#003366"><b>Ubicaci&oacute;n</b></font>
                      <p align="justify"><font class="letra8"> Edificio U, subterr&aacute;neo de Biblioteca.<br>
                      Fono 32-2654134 </font></p></td>
                    </tr>
                  <tr>
                    <td><img src="imagen/gim_foto02.jpg" width="130" height="95"></td>
                    </tr>
                  <tr>
                    <td><img src="imagen/gim_foto03.jpg" width="131" height="87"></td>
                    </tr>
              </table></td>
              <form action="gim_validar.jsp" method="POST" name="Formulario" target="_self" autocomplete="off">
                  <% if (usuario == 0) {%>
              <td width="27%" valign="top">
                  <table align="right" width="100%" cellpadding="1" cellspacing="1" border="0">
                    <tr>
                      <td colspan="2" >&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="2"><font class="letra8" color="#003366"><b>Acceso</b></font></td>
                    </tr>
                    <tr>
                      <td height="35" colspan="2" class="CGris">Ingrese su cuenta de usuario y clave de correo USM.</td>
                    </tr>
                    <tr>
                      <td><font class="CGris" color="#003366">Usuario</font></td>
                      <td><input name="login" type="text" size="15" maxlength="30" class="CGris" onKeyPress="enter(event,1)">
                        <select name="server" class="CGris">
                          <option value="alumnos.usm.cl" selected>@alumnos.usm.cl</option>
                          <option value="sansano.usm.cl">@sansano.usm.cl</option>
                          <option value="postgrado.usm.cl">@postgrado.usm.cl</option>
                          <option value="usm.cl">@usm.cl</option>
                        </select></td>
                      </tr>

                    <tr>
                      <td nowrap><font class="CGris" color="#003366">Clave</font></td>
                      <td><input type="password" class="CGris" size="15" name="passwd" onKeyPress="enter(event,2)">
                        <input type="button" name="ingresar" value="Ingresar" class="boton04" onClick="ValidaLogin(document.Formulario)"></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td><font color="#996600" onmouseover="JavaScript:CursorOn(this); window.status='�problemas con su contrase�a?'; this.style.color='#CA6B0F'; return true; " onmouseout="JavaScript:CursorOff(this); this.style.color='#996600'; window.status=''; return true; " onClick="JavaScript:ProblemaClave();" class="Cgris">
            �problemas con su clave?</font></td>
                    </tr>
                    <%if(msj > 0){
                     %>
                        <tr><td>&nbsp;</td>
                          <td><font class="CGris" color="red"><%=mensaje%></font></td>
                        </tr>
                     <% } %>
<tr>
<td height="33" colspan=2>
<% if (v_noticias.size() > 0) {%>
<font class="letra8" color="#003366"><b>Noticias</b></font>
<%}%>&nbsp;</td></tr>
<tr> <td colspan=2>
<% if (v_noticias.size() > 0) {%>
<script language="JavaScript1.2">
  var scrollerwidth="260px"
// Scrollers height here
  var scrollerheight="200px"
// Scrollers speed here (larger is faster 1-10)
  var scrollerspeed=1

  var scroll_estiloPAlta  = 'Estilo_Rojo';
  var scroll_estiloPMedia = 'Estilo_NaranjoOsc';
  var scroll_estiloPBaja  = 'Estilo_AzulinoClaro';
  var scroll_estiloFecha  = 'Estilo_Gris letra6';
  var scroll_salto        = '<br><br>';
  var scrollercontent     =
  <%
    String fin = "+ scroll_salto +";
    String estilo = "Estilo_AzulinoClaro";
    for (int i=0; i < v_noticias.size(); i++) {
      if (i == v_noticias.size()-1) fin = ";";
      if (((Vector)v_noticias.get(i)).get(0).equals("2")) estilo = "Estilo_Rojo";
      if (((Vector)v_noticias.get(i)).get(0).equals("3")) estilo = "Estilo_NaranjoOsc";
      if (((Vector)v_noticias.get(i)).get(0).equals("4")) estilo = "Estilo_AzulinoClaro";

      StringBuffer mens = new StringBuffer();
      mens.append(((Vector)v_noticias.get(i)).get(3));
      while  (mens.indexOf("\n") > -1) {
        mens.replace(mens.indexOf("\n")-1,mens.indexOf("\n")+1,"<br>");
      }
  %>
                   '<font class="<%=estilo%>" size="2" face="Verdana, Arial, Helvetica, sans-serif">'+
                   '<%=((Vector)v_noticias.get(i)).get(2)%></font><hr>' +
                   '<font class="letra8" color="#003366"><%=mens%></font>' +
                   '<br><font class="'+scroll_estiloFecha+'"><%=((Vector)v_noticias.get(i)).get(4)%> <%=((Vector)v_noticias.get(i)).get(5)%> hrs.</font>'<%=fin%>
  <%}%>


// Scrollers content goes here! Keep all of the message on the same line!
  var pauseit=1
// Change nothing below!
  scrollerspeed=(document.all)?scrollerspeed : Math.max(1, scrollerspeed-1) //slow speed down by 1 for NS
  var copyspeed=scrollerspeed
  var iedom=document.all||document.getElementById
  var actualheight=''
  var cross_scroller, ns_scroller
  var pausespeed=(pauseit==0)? copyspeed: 0

  function populate(){
    if (iedom){
      cross_scroller=document.getElementById? document.getElementById("iescroller"): document.all.iescroller
      cross_scroller.style.top=parseInt(scrollerheight)+8+"px"
      cross_scroller.innerHTML=scrollercontent
      actualheight=cross_scroller.offsetHeight
    } else
        if (document.layers){
      ns_scroller=document.ns_scroller.document.ns_scroller2
      ns_scroller.top=parseInt(scrollerheight)+8
      ns_scroller.document.write(scrollercontent)
      ns_scroller.document.close()
      actualheight=ns_scroller.document.height
    }
    lefttime=setInterval("scrollscroller()",20)
  }

  function scrollscroller(){
      if (iedom){
        if (parseInt(cross_scroller.style.top)>(actualheight*(-1)+8))
          cross_scroller.style.top=parseInt(cross_scroller.style.top)-copyspeed+"px"
          else cross_scroller.style.top=parseInt(scrollerheight)+8+"px" }
      else
      if (document.layers){
        if (ns_scroller.top>(actualheight*(-1)+8))
          ns_scroller.top-=copyspeed
        else ns_scroller.top=parseInt(scrollerheight)+8 } }
          if (iedom||document.layers){
          with (document){
            if (iedom){
              write('<div style="position:relative;width:'+
                    scrollerwidth+';height:'+scrollerheight+
                    ';overflow:hidden" onMouseover="copyspeed=pausespeed" onMouseout="copyspeed=scrollerspeed">')
              write('<div id="iescroller" style="position:absolute;left:0px;top:0px;width:100%;">')
              write('</div></div>') }
              else
                if (document.layers){
              write('<ilayer width='+scrollerwidth+
                    ' height='+scrollerheight+' name="ns_scroller">')
        write('<layer name="ns_scroller2" width='+scrollerwidth+' height='+
        scrollerheight+'left=0 top=0 onMouseover="copyspeed=pausespeed" onMouseout="copyspeed=scrollerspeed"></layer>')
        write('</ilayer>') } }
  }
</script>
<%}%>&nbsp;
</td></tr>
                </table>
                </td>
                <%} else {%>
                <td width="25%" valign="top">
                  <table align="right" width="100%" cellpadding="1" cellspacing="1" border="0">
                    <tr><td colspan="2" >&nbsp;</td></tr>
                    <tr align="center">
                      <td colspan="2"><span class="Estilo_AzulinoClaro letra8">Bienvenido(a)<br><%=nom_usuario%></span></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td align="center"><input type="submit" name="ingresar" value=">> Ingrese al Sistema" class="boton04">
                      </td>
                    </tr>
                     <%if(msj > 0){ %>
                        <tr><td colspan="2" align="center">
                        <font class="letra7" color="red">Acceso Denegado<%if(msj == 3){%>.<br>Situaci�n Acad�mica Inv�lida.<%}%></font></td></tr>
                     <% } %>
                </table>
                <input type="hidden" name="rut" value="<%=usuario%>">
                </td>
                <%}%>
                <td width="1%" valign="top"><img src="imagen/spacer.gif" width="5" height="1"></td>
              </form>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td height="1" colspan="3" align="center">&nbsp;</td>
      </tr>
    </table>
    <%@ include file="gim_pie_pagina.jsp" %></td>
  </tr>
</table>
<div style="padding:30px; text-align:center;"><font style="color:white;">
<%=version%> - Actualizado el 07-06-2019</font></div>