<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%
  response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");

  int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int cod_perfil  = session.getAttribute("cod_perfil") != null?
                    Integer.parseInt(session.getAttribute("cod_perfil")+""):0;

  int accion      = request.getParameter("accion")!=null?
                    Integer.parseInt(request.getParameter("accion")+""):0;
  int correlativo = request.getParameter("correlativo")!=null?
                    Integer.parseInt(request.getParameter("correlativo")+""):0;
  int anno        = request.getParameter("anno")!=null?
                    Integer.parseInt(request.getParameter("anno")+""):0;
  int semestre    = request.getParameter("semestre")!=null?
                    Integer.parseInt(request.getParameter("semestre")+""):0;
  int prioridad   = request.getParameter("prioridad")!=null?
                    Integer.parseInt(request.getParameter("prioridad")+""):0;
  String titulo   = request.getParameter("titulo")!=null?
                    request.getParameter("titulo"):"";
  String mensaje  = request.getParameter("mensaje")!=null?
                    request.getParameter("mensaje"):"";

  general.getAbrirConexion();
  general.setGenerarConexionBitacora();

  if (anno == 0){
    general.setGenerarConexionHorario();
    general.getPeriodoSemestreActual(1,1);
     semestre = general.getSemestreActual();
     anno = general.getA�oActual();
   }


  String alert = "";
  int error = -1;

  if (accion > 0) {
    error = general.setNoticias(accion, prioridad , correlativo, anno, semestre,
                                titulo, mensaje, rut_usuario);
  }

  // agregar noticia
  if (accion == 1) {
    if(error == 0) alert = "La noticia se registr� satisfactoriamente";
    else alert = "Problemas al registrar ";
  }
  // eliminar noticia
  if (accion == 2) {
    if(error == 0) alert = "La noticia fue eliminada";
    else alert = "Problemas al eliminar ";
  }

  Vector v_noticias = general.getNoticias();

  general.getCerrarConexion();

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Gimnasio - Mantenedor de noticias</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script language="JavaScript1.2">
<% if (alert.length() > 0) {%>
function Mensaje(){
     alert ('<%=alert%>');
}
<%}%>
function validar(form){
    if(form.titulo.value ==""){
      alert("Debe ingresar un t�tulo ");
      form.titulo.focus();
      return false;
    }
    if(form.prioridad.value < 1 || form.prioridad.value > 4){
      alert("Debe seleccionar una prioridad");
      form.prioridad.focus();
      return false;
    }
    if(form.mensaje.value ==""){
      alert("Debe ingresar un mensaje");
      form.mensaje.focus();
      return false;
    }
    return true;
}

function Grabar(f){
  if (!validar(f))
    return false;
  f.submit();
}

function Eliminar(f){
  var pregunta = "Ud. va a ELIMINAR la noticia definitivamente.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  if (respuesta == true) f.submit();
}

function mouseover(el) {
  el.className = "raised";
}

function mouseout(el) {
  el.className = "button";
}

function mousedown(el) {
  el.className = "pressed";
}

function mouseup(el) {
  el.className = "raised";
}

function format_sel(v) {
  if (document.selection) {
    var str = document.selection.createRange().text;
    document.form_noticia.mensaje.focus();
    var sel = document.selection.createRange();
    sel.text = "<" + v + ">" + str + "</" + v + ">";
  }
  else {
    textfield = document.form_noticia.mensaje;
    if (textfield.selectionStart || textfield.selectionStart == '0') {
      textfield.focus();
      var startPos = textfield.selectionStart;
      var endPos = textfield.selectionEnd;
      str = textfield.value.substring(startPos, endPos);
      textfield.value = textfield.value.substring(0, startPos) + "<" + v + ">" + str + "</" + v + ">" +  textfield.value.substring(endPos, textfield.value.length);
    }
  }
  return;
}
</script>
<style>

#toolbar {
                        margin: 0;
                        padding: 0;
                        width: 420px;
                        background: buttonface;
                        border-top: 1px solid buttonhighlight;
                        border-left: 1px solid buttonhighlight;
                        border-bottom: 1px solid buttonshadow;
                        border-right: 1px solid buttonshadow;
                        text-align:right;
                          }

.button 	{
                        background: buttonface;
                        border: 1px solid buttonface;
                        margin: 1;
                        }

.raised		{
                        border-top: 1px solid buttonhighlight;
                        border-left: 1px solid buttonhighlight;
                        border-bottom: 1px solid buttonshadow;
                        border-right: 1px solid buttonshadow;
                        background: buttonface;
                        margin: 1;
                        }

.pressed	{
                        border-top: 1px solid buttonshadow;
                        border-left: 1px solid buttonshadow;
                        border-bottom: 1px solid buttonhighlight;
                        border-right: 1px solid buttonhighlight;
                        background: buttonface;
                        margin: 1;
			}

</style>
</head>

<body onload="<%=alert.length() > 0?"Mensaje();":""%><% if (v_noticias.size() > 0) {%>populate();<%}%>">
<span class="Estilo_Verde">NOTICIAS<br>
<span class="Estilo_Gris">Agregue una nueva noticia a la portada del sistema.</span> <br>
<br>
</span>
<form name="form_noticia" method="post" action="gim_noticia_adm.jsp" target="_self">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13%" height="29" class="Estilo_Gris">T&iacute;tulo</td>
    <td><input type="text" name="titulo">
      <span class="Estilo_Gris">&nbsp;&nbsp;Prioridad&nbsp; </span>
      <select name="prioridad" class="Select" id="prioridad">
        <option value="-1">-- Seleccione --</option>
        <option class="Estilo_Rojo" value="2">Alta</option>
        <option class="Estilo_NaranjoOsc" value="3">Media</option>
        <option class="Estilo_AzulinoClaro" value="4">Baja</option>

      </select>
    </td>
  </tr>
  <tr valign="top">
    <td class="Estilo_Gris">Mensaje</td>
    <td><textarea name="mensaje" cols="50" rows="5"></textarea>
    <div id="toolbar">
    <img class="button"
         onmouseover="mouseover(this);"
         onmouseout="mouseout(this);"
         onmousedown="mousedown(this);"
         onmouseup="mouseup(this);"
         onclick="format_sel('b');"
         src="imagen/bold.gif"
         width="16" height="16"
         align="middle"
         alt="Destaque el texto seleccionado en formato BOLD">
         <img class="button"
         onmouseover="mouseover(this);"
         onmouseout="mouseout(this);"
         onmousedown="mousedown(this);"
         onmouseup="mouseup(this);"
         onclick="format_sel('i');"
         src="imagen/italic.gif"
         width="16" height="16"
         align="middle"
         alt="Destaque el texto seleccionado en formato ITALIC">
    </div>
    <div style="padding-top:5px">
    Marque el texto a destacar y pulse el icono <b>B</b> o <b><i>I</i></b>.<br>
    Debe verificar que las etiquetas insertadas para <b>BOLD</b> o <b><i>ITALIC</i></b> tengan su<br>etiqueta de t�rmino.
    </div>
  </td>
  </tr>

  <tr>
    <td height="41" class="Estilo_Gris">&nbsp;</td>
    <td width="87%"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseOK.gif">
      <tr onClick="Grabar(form_noticia);" onMouseOver="BotonOver(this,'Agregar')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino">Agregar</td>
      </tr>
    </table>
    <input type="hidden" name="accion" value="1">
    </td>
  </tr>
</table>
</form>
<% if (!v_noticias.isEmpty()) {%>
<span class="Estilo_Verde">Noticias actuales</span><br>
<span class="Estilo_Gris">Las noticias se mostrar&aacute;n ordenadas por prioridad y fecha, de forma descendente.</span> <br>
<br>
<table width="100%"  border="0" cellspacing="1" cellpadding="1">
  <tr  bgcolor="#999999">
    <td>
<table width="100%"  border="0" cellspacing="0" cellpadding="2">
  <tr bgcolor="#008040" class="Estilo_GrisOsc">
    <td height="13" align="center" background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Prioridad</span></td>
    <td background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">T&iacute;tulo</span></td>
    <td background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Mensaje</span></td>
    <td align="center" background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Fecha</span></td>
    <td align="center" background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Eliminar</span></td>
  </tr>
  <% for (int i=0; i < v_noticias.size(); i++) {

    StringBuffer mens = new StringBuffer();
    mens.append(((Vector)v_noticias.get(i)).get(3));

    while  (mens.indexOf("\n") > -1) {
      mens.replace(mens.indexOf("\n")-1,mens.indexOf("\n")+1,"<br>");
    }

    String estilo_priridad = "Estilo_Azul";
    if (((Vector)v_noticias.get(i)).get(0).equals("2"))
      estilo_priridad = "Estilo_Rojo";
    else if (((Vector)v_noticias.get(i)).get(0).equals("3"))
      estilo_priridad = "Estilo_NaranjoOsc";
    else if (((Vector)v_noticias.get(i)).get(0).equals("4"))
      estilo_priridad = "Estilo_AzulinoClaro";

  %>
  <tr valign="top" bgcolor="#E8E8E8" onmouseover='this.style.background="#F4F4F4"' onmouseout='this.style.background="#E8E8E8"'>
    <form name="form_grabar<%=i%>" method="post" action="gim_noticia_adm.jsp" target="_self">
      <td width="10%" height="13" align="center" class="Estilo_Azul"><%=((Vector)v_noticias.get(i)).get(1)%>&nbsp;</td>
      <td width="16%" class="<%=estilo_priridad%>"><%=((Vector)v_noticias.get(i)).get(2)%></td>
      <td width="48%" class="Estilo_Azul"><%=mens%></td>
      <td width="15%" class="Estilo_Azul" align="center"><%=((Vector)v_noticias.get(i)).get(4) + " " + ((Vector)v_noticias.get(i)).get(5)%></td>
      <td width="11%" align="center"><a href="#" onClick="Eliminar(document.form_grabar<%=i%>)"> <img src="imagen/gim_basurero.gif" alt="ELIMINAR" border="0" align="absmiddle"></a>
      <input type="hidden" name="anno" value="<%=((Vector)v_noticias.get(i)).get(6)%>">
      <input type="hidden" name="semestre" value="<%=((Vector)v_noticias.get(i)).get(7)%>">
      <input type="hidden" name="correlativo" value="<%=((Vector)v_noticias.get(i)).get(8)%>">
      <input type="hidden" name="accion" value="2">
      </td>
    </form>
  </tr>
  <%}%>
</table>
</td>
</tr>
</table>
<br>
<span class="Estilo_Verde">Vista real en portada</span> <br>
<br>
<table width="200" height="230" border="1" cellspacing="1" cellpadding="5">
  <tr>
    <td>
<script language="JavaScript1.2">
var scrollerwidth="260px"
// Scrollers height here
var scrollerheight="200px"
// Scrollers speed here (larger is faster 1-10)
var scrollerspeed=1

var scroll_estiloPAlta  = 'Estilo_Rojo';
var scroll_estiloPMedia = 'Estilo_NaranjoOsc';
var scroll_estiloPBaja  = 'Estilo_AzulinoClaro';
var scroll_estiloFecha  = 'Estilo_Gris letra6';
var scroll_salto        = '<br><br>';
var scrollercontent     = <%

  String fin = "+ scroll_salto +";
  String estilo = "Estilo_AzulinoClaro";
  for (int i=0; i < v_noticias.size(); i++) {
    if (i == v_noticias.size()-1) fin = ";";
    if (((Vector)v_noticias.get(i)).get(0).equals("2")) estilo = "Estilo_Rojo";
    if (((Vector)v_noticias.get(i)).get(0).equals("3")) estilo = "Estilo_NaranjoOsc";
    if (((Vector)v_noticias.get(i)).get(0).equals("4")) estilo = "Estilo_AzulinoClaro";

    StringBuffer mens = new StringBuffer();
    mens.append(((Vector)v_noticias.get(i)).get(3));
    while  (mens.indexOf("\n") > -1) {
      mens.replace(mens.indexOf("\n")-1,mens.indexOf("\n")+1,"<br>");
    }
%>
                 '<font class="<%=estilo%>" size="2" face="Verdana, Arial, Helvetica, sans-serif">'+
                 '<%=((Vector)v_noticias.get(i)).get(2)%></font><hr>' +
                 '<font class="letra8" color="#003366"><%=mens%></font>' +
                 '<br><font class="'+scroll_estiloFecha+'"><%=((Vector)v_noticias.get(i)).get(4)%> <%=((Vector)v_noticias.get(i)).get(5)%> hrs.</font>'<%=fin%>
<%}%>

// Scrollers content goes here! Keep all of the message on the same line!
var pauseit=1
// Change nothing below!
scrollerspeed=(document.all)?scrollerspeed : Math.max(1, scrollerspeed-1) //slow speed down by 1 for NS
var copyspeed=scrollerspeed
var iedom=document.all||document.getElementById
var actualheight=''
var cross_scroller, ns_scroller
var pausespeed=(pauseit==0)? copyspeed: 0

function populate(){
  if (iedom){
    cross_scroller=document.getElementById? document.getElementById("iescroller"): document.all.iescroller
    cross_scroller.style.top=parseInt(scrollerheight)+8+"px"
    cross_scroller.innerHTML=scrollercontent
    actualheight=cross_scroller.offsetHeight
  } else
      if (document.layers){
    ns_scroller=document.ns_scroller.document.ns_scroller2
    ns_scroller.top=parseInt(scrollerheight)+8
    ns_scroller.document.write(scrollercontent)
    ns_scroller.document.close()
    actualheight=ns_scroller.document.height
  }
  lefttime=setInterval("scrollscroller()",20)
}

function scrollscroller(){
    if (iedom){
      if (parseInt(cross_scroller.style.top)>(actualheight*(-1)+8))
        cross_scroller.style.top=parseInt(cross_scroller.style.top)-copyspeed+"px"
        else cross_scroller.style.top=parseInt(scrollerheight)+8+"px" }
    else
    if (document.layers){
      if (ns_scroller.top>(actualheight*(-1)+8))
        ns_scroller.top-=copyspeed
      else ns_scroller.top=parseInt(scrollerheight)+8 } }
        if (iedom||document.layers){
        with (document){
          if (iedom){
            write('<div style="position:relative;width:'+
                  scrollerwidth+';height:'+scrollerheight+
                  ';overflow:hidden" onMouseover="copyspeed=pausespeed" onMouseout="copyspeed=scrollerspeed">')
            write('<div id="iescroller" style="position:absolute;left:0px;top:0px;width:100%;">')
            write('</div></div>') }
            else
              if (document.layers){
            write('<ilayer width='+scrollerwidth+
                  ' height='+scrollerheight+' name="ns_scroller">')
      write('<layer name="ns_scroller2" width='+scrollerwidth+' height='+
      scrollerheight+'left=0 top=0 onMouseover="copyspeed=pausespeed" onMouseout="copyspeed=scrollerspeed"></layer>')
      write('</ilayer>') } }
}
</script>
</td>
</tr>
</table>
<%}%>
</body>
</html>
