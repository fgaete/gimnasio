<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 07/06/2007
   // �ltima Actualizaci�n :
   // Actividad. se registran y mantienen todas las actividades que se desarrollan
   // para la sala de aer�bica.
%>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  int cod_perfil = session.getAttribute("cod_perfil") != null?
                  Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
  String   actividad =  request.getParameter("nom_actividad")!=null &&
                        !request.getParameter("nom_actividad").equals("null") &&
                        !request.getParameter("nom_actividad").equals("NULL") &&
                        !request.getParameter("nom_actividad").equals("")  &&
                        !request.getParameter("nom_actividad").equals(" ")
                        ?request.getParameter("nom_actividad")+"":"";

  int   accion =  request.getParameter("accion")!=null &&
                      !request.getParameter("accion").equals("null") &&
                      !request.getParameter("accion").equals("NULL") &&
                      !request.getParameter("accion").equals("")  &&
                      !request.getParameter("accion").equals(" ")
                          ?Integer.parseInt(request.getParameter("accion")+""):0;
  int   cod_asign =  request.getParameter("cod_asign")!=null &&
                    !request.getParameter("cod_asign").equals("null") &&
                    !request.getParameter("cod_asign").equals("NULL") &&
                    !request.getParameter("cod_asign").equals("")  &&
                    !request.getParameter("cod_asign").equals(" ")
                          ?Integer.parseInt(request.getParameter("cod_asign")+""):0;
 if (rut_usuario == 0)  response.sendRedirect("index.jsp");
 boolean soloconsulta = false;
 if (cod_perfil==28) soloconsulta = true;
 Vector vec = new Vector();
 String mensaje = "";
 int error = 0;
 general.getAbrirConexion();
 general.setGenerarConexionHorario();
 if (accion > 0){

 switch (accion){
       case 1: { // agregar
            error = general.setInsertarActividad(actividad,rut_usuario);
            if(error == 0) mensaje = "Se registr� satisfactoriamente";
            else mensaje = "Problemas al registrar ";
            error = -1;
        break;
       }
       case 2: { // actualizar
             error = general.setActualizarActividad(actividad,rut_usuario,cod_asign);
             if(error == 0) mensaje = "Se Actualiz� satisfactoriamente";
             else mensaje = "Problemas al actualizar";
             error = -1;
             break;
       }
     case 3: { // eliminar
       //alert('eliminar');	 
       error = general.setEliminaActividad(cod_asign);
       if(error == 0) mensaje = "Se Elimin� satisfactoriamente";
       else { mensaje = "Problemas al eliminar actividad";
       if (error == -8) mensaje += ", hay rama deportiva asociada";
       else if (error == -13) mensaje += ", no se encuentra registrada";
       else if (error == -14) mensaje += ", existe en Horario dentro del mantenedor de actividad ";
       else mensaje += "("+ error + ")";
       }
       error = -1;
       break;
       }
     }


 }
 vec = general.getActividad();
 general.getCerrarConexion();
  %>
<html>
<head>
<title>Gimnasio - Actividad</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script LANGUAGE="JavaScript" TYPE="text/javascript">
function Mensaje(){
<%if(error<0){%>alert ('<%=mensaje%>');<%}%>
}
<%if(!soloconsulta){%>
function GrabarNueva(f,accion){
  if (f.nom_actividad.value == ''){
    alert ('Debe ingresar una Actividad');
    f.nom_actividad.focus();
    return false;
  }
   f.accion.value = accion;
   f.submit();
}
function Grabar(f,accion){
  f.nom_actividad.value = document.getElementById('nom_actividad_mod').value;
  f.cod_actividad.value = document.getElementById('cod_actividad_mod').value;
  if (document.getElementById('nom_actividad_mod').value == ''){
    alert ('Debe ingresar una Actividad');
    document.getElementById('nom_actividad_mod').focus();
    return false;
  }
   f.accion.value = accion;
   f.submit();
}

function Eliminar(f){
  var pregunta = "Ud. va a ELIMINAR la actividad definitivamente.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  f.accion.value = 3;
  if (respuesta == true) f.submit();
}

function Editar(i, texto, codigo, f) {
  document.getElementById('act'+i).innerHTML =
      "<input name='nom_actividad_mod' id='nom_actividad_mod' type='text' class='Input01' value='"+texto+"' size='50'>"+
      "<input name='cod_actividad_mod' id='cod_actividad_mod' type='hidden' value='"+codigo+"'>";
  document.getElementById('accion'+i).onclick= function(){Grabar(f,2);};
  document ['EDITAR'+i].src = GrabarActivo.src;

  for (var j=0; j <= (<%=vec.size()%>-1); j++) {
    document.getElementById('IMAG_ELIMINAR'+j).innerHTML = "<img src='imagen/gim_basurero_des.gif' alt='ELIMINAR' align='absmiddle'>";
    if (j != i) document.getElementById('IMAG_EDITAR'+j).innerHTML = "<img src='imagen/gim_editar_des.gif' alt='EDITAR' align='absmiddle'>";
  }
  return true;
}
<%}%>
function preCargaImg(imageURL) {
                image = new Image();
                image.src = imageURL;
                return image;
}


GrabarActivo = preCargaImg("imagen/gim_grabar.gif");
</SCRIPT>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style></head>

<body onload="Mensaje();">
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td height="45" colspan="3" class="Estilo_Verde">ACTIVIDAD<br>
      SALA MULTIUSO </td>
  </tr>
   <%if(!soloconsulta){%>
  <tr bgcolor="#FFFFFF">
   <form name="form_grabar" method="post" action="gim_actividad_adm.jsp" target="_self">
   <input type="hidden" name="accion" value="">
    <td bgcolor="#FFFFFF" class="Estilo_Gris">Actividad</td>
    <td width="32%" bgcolor="#FFFFFF"><input name="nom_actividad" type="text" class="Input01" size="30">        </td>
    <td width="57%" bgcolor="#FFFFFF"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseOK.gif">
      <tr onClick="GrabarNueva(form_grabar,1);" onMouseOver="BotonOver(this,'Agregar')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino">Agregar</td>
      </tr>

    </table></td>
	 </form>
  </tr>
   <%}%>
  <tr bgcolor="#FFFFFF">
    <td width="11%" bgcolor="#FFFFFF" class="Estilo_Gris">&nbsp;</td>
    <td colspan="2" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr bgcolor="#CCCCCC">
    <td  colspan="3" class="Estilo_Gris">
      <table width="100%"  border="0" cellspacing="0" cellpadding="1">
        <tr bgcolor="#999999">
          <td width="100%"  class="Estilo_Gris">
            <table width="100%"  border="0" cellspacing="0" cellpadding="2">
              <tr bgcolor="#008040" class="Estilo_GrisOsc">
                <td height="13" background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">N&deg;</span></td>
                <td background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Actividad</span></td>
                 <%if(!soloconsulta){%><td align="center" background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Acci&oacute;n</span></td><%}%>
              </tr>
              <%for (int i=0; i < vec.size(); i++){%>
               <tr bgcolor="#E8E8E8" onmouseover='this.style.background="#F4F4F4"' onmouseout='this.style.background="#E8E8E8"'>
                <%if(!soloconsulta){%>
                 <form name="form_grabar<%=i%>" method="post" action="gim_actividad_adm.jsp" target="_self">
                 <%}%>
                 <td width="11%" height="13" class="Estilo_Azul"><%=i+1%>
                 <%if(!soloconsulta){%>
                 <input type="hidden" name="cod_asign" value="<%=((Vector) vec.get(i)).get(0)+ ""%>">
                 <input type="hidden" name="accion" value="">
                 <input type="hidden" name="actividad" value="">
                 <input type="hidden" name="nom_actividad" value="">
                 <input type="hidden" name="cod_actividad" value="">
                 <%}%>
             </td>
                <td width="77%" class="Estilo_Azul"><span id="act<%=i%>"><%=((Vector) vec.get(i)).get(1)+ ""%></span></td>
                 <%if(!soloconsulta){%><td width="12%" align="center">
                <span id="IMAG_EDITAR<%=i%>">
                <a HREF="#" id="accion<%=i%>" onClick="Editar('<%=i%>','<%=((Vector) vec.get(i)).get(1)+ ""%>',1,document.form_grabar<%=i%>); return false">
                <img src="imagen/gim_editar.gif" id="EDITAR<%=i%>" name="EDITAR<%=i%>" alt="EDITAR" border="0" align="absmiddle"></a>
                </span>&nbsp;
                <span id="IMAG_ELIMINAR<%=i%>">
                <a href="#" onClick="Eliminar(document.form_grabar<%=i%>)">
                <img src="imagen/gim_basurero.gif" alt="ELIMINAR" border="0" align="absmiddle"></a>
                </span></td><%}%>
              </tr>
              <%if(!soloconsulta){%>
              </form><%}%>
			  <%}%>
                       </table>
          </td>
        </tr>
    </table></td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="30" valign="bottom"><%if(!soloconsulta){%><span class="Estilo_GrisOsc"><img src="imagen/gim_editar.gif" alt="EDITAR" width="18" height="16" align="absmiddle"> Editar&nbsp;&nbsp;&nbsp;<img src="imagen/gim_grabar.gif" alt="GRABAR" width="16" height="16" border="0" align="absmiddle"> Grabar&nbsp;&nbsp;&nbsp;<img src="imagen/gim_basurero.gif" alt="ELIMINAR" width="15" height="18" align="absmiddle"> Eliminar</span><%}%></td>
  </tr>
</table>

</body>
</html>