<%@ page import="java.util.*"%>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");

  int rut_usuario = session.getAttribute("rut_usuario") != null?
                     Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int proceso           = request.getParameter("proceso") != null &&
                          !request.getParameter("proceso").trim().equals("") ?
                          Integer.parseInt(request.getParameter("proceso")+""):0;
  int rut_consulta      = request.getParameter("rut_consulta") != null &&
                          !request.getParameter("rut_consulta").trim().equals("") ?
                          Integer.parseInt(request.getParameter("rut_consulta")+""):0;
  int   sala =  request.getParameter("sala")!=null &&
                 !request.getParameter("sala").equals("null") &&
                 !request.getParameter("sala").equals("NULL") &&
                 !request.getParameter("sala").equals("")  &&
                 !request.getParameter("sala").equals(" ")
              ?Integer.parseInt(request.getParameter("sala")+""):0;
 int bloque1              = request.getParameter("bloque1")!=null &&
                           !request.getParameter("bloque1").equals("null") &&
                           !request.getParameter("bloque1").equals("NULL") &&
                           !request.getParameter("bloque1").equals("")  &&
                           !request.getParameter("bloque1").equals(" ")?
                           Integer.parseInt(request.getParameter("bloque1")+""):0;
 int bloque2              = request.getParameter("bloque2")!=null &&
                         !request.getParameter("bloque2").equals("null") &&
                         !request.getParameter("bloque2").equals("NULL") &&
                         !request.getParameter("bloque2").equals("")  &&
                         !request.getParameter("bloque2").equals(" ")?
                           Integer.parseInt(request.getParameter("bloque2")+""):0;
  String fecha_inicio     = request.getParameter("fecha_inicio")!=null &&
                           !request.getParameter("fecha_inicio").equals("null") &&
                           !request.getParameter("fecha_inicio").equals("NULL") &&
                           !request.getParameter("fecha_inicio").equals("")  &&
                           !request.getParameter("fecha_inicio").equals(" ")?
                           request.getParameter("fecha_inicio")+"":"";
  int dia              = request.getParameter("dia")!=null &&
                             !request.getParameter("dia").equals("null") &&
                             !request.getParameter("dia").equals("NULL") &&
                             !request.getParameter("dia").equals("")  &&
                             !request.getParameter("dia").equals(" ")?
                             Integer.parseInt(request.getParameter("dia")+""):0;
  int operacion   = request.getParameter("operacion") != null &&
                  !request.getParameter("operacion").trim().equals("") ?
                      Integer.parseInt(request.getParameter("operacion")+""):0;


  String dvrut_consulta = request.getParameter("dvrut_consulta") != null &&
                          !request.getParameter("dvrut_consulta").trim().equals("") ?
                          request.getParameter("dvrut_consulta"):"";
  String resgim_id = request.getParameter("resgim_id") != null &&
                           !request.getParameter("resgim_id").trim().equals("") ?
                           request.getParameter("resgim_id"):"";
  String fecha = request.getParameter("fecha") != null &&
                         !request.getParameter("fecha").trim().equals("") ?
                           request.getParameter("fecha"):"";
  int asistencia   = request.getParameter("asistencia") != null &&
                   !request.getParameter("asistencia").trim().equals("") ?
                   Integer.parseInt(request.getParameter("asistencia")+""):0;
  int orden   = request.getParameter("orden") != null &&
                 !request.getParameter("orden").trim().equals("") ?
                   Integer.parseInt(request.getParameter("orden")+""):0;


  int par_i         = request.getParameter("par_i") != null &&
                      !request.getParameter("par_i").trim().equals("") ?
                      Integer.parseInt(request.getParameter("par_i").trim()+""):0;

  String par_accion = request.getParameter("par_accion") != null &&
                      !request.getParameter("par_accion").trim().equals("") ?
                      request.getParameter("par_accion").trim():"";

  int par_origen    = request.getParameter("par_origen") != null &&
                      !request.getParameter("par_origen").trim().equals("") ?
                      Integer.parseInt(request.getParameter("par_origen").trim()+""):0;


  Vector vec = new Vector();
  String mensaje_proceso = "&nbsp;";
  String onload = "";
  int error = 0;

  // Asistencia
  if (proceso == 1) {

     new Vector();
    vec.addElement(resgim_id); // resgim_id
    vec.addElement(fecha); // fecha
    vec.addElement(asistencia+"");// cod_asistencia
    vec.addElement(orden+""); // orden_llegada
    vec.addElement(rut_usuario+"");// rut_usuario


    general.getAbrirConexion();
    general.setGenerarConexionReserva();

    error = general.setRegistraAsistenciaGimnasio(vec);
    general.getCerrarConexion();

    if (asistencia == 1)
      if (error == 0 || error == -6)
      mensaje_proceso = "Asistencia registrada [" +
      rut_consulta + "-" + dvrut_consulta + "]";
    else
      mensaje_proceso = "Problemas al registrar (" + error + ") [" +
      rut_consulta + "-" + dvrut_consulta + "]";


    if (asistencia == 2)
     if (error == 0 || error == -6)
      mensaje_proceso = "Justificaci�n registrada [" +
      rut_consulta + "-" + dvrut_consulta + "]";
    else
      mensaje_proceso = "Problemas al registrar (" + error + ") [" +
      rut_consulta + "-" + dvrut_consulta + "]";


    if (asistencia == 3)
       if (error == 0 || error == -6)
      mensaje_proceso = "Falta registrada [" +
      rut_consulta + "-" + dvrut_consulta + "]";
    else
      mensaje_proceso = "Problemas al registrar (" + error + ")[" +
      rut_consulta + "-" + dvrut_consulta + "]";

  }

  // L.Espera
  if (proceso == 2) {

    int a�o   = request.getParameter("a�o") != null &&
                  !request.getParameter("a�o").trim().equals("") ?
                      Integer.parseInt(request.getParameter("a�o")+""):0;
    int semestre   = request.getParameter("semestre") != null &&
               !request.getParameter("semestre").trim().equals("") ?
                      Integer.parseInt(request.getParameter("semestre")+""):0;
    String periodo = request.getParameter("periodo") != null &&
                       !request.getParameter("periodo").trim().equals("") ?
                           request.getParameter("periodo"):"";
    vec = new Vector();
    vec.addElement(resgim_id);
    vec.addElement(fecha);
    vec.addElement(rut_usuario+"");
    vec.addElement(operacion+"");
    vec.addElement(a�o+"");
    vec.addElement(semestre+"");
    vec.addElement(bloque1+"");
    vec.addElement(bloque2+"");
    vec.addElement(periodo);
    vec.addElement(dia+"");
    vec.addElement(sala+"");
    general.getAbrirConexion();
    general.setGenerarConexionReserva();
    error = general.setListaEspera(vec);
    general.getCerrarConexion();


    if (operacion == 1)
      if(error == 0)
      mensaje_proceso = "Ascenso registrado [" +
      rut_consulta + "-" + dvrut_consulta + "]" ;
      else
        mensaje_proceso = "Problemas al registrar Ascenso [" +
         rut_consulta + "-" + dvrut_consulta + "]" ;

    if (operacion == 2)
      if (error == 0)
      mensaje_proceso = "Descenso registrado [" +
      rut_consulta + "-" + dvrut_consulta + "]" ;
    else
      mensaje_proceso = "Problemas al registrar Descenso [" +
      rut_consulta + "-" + dvrut_consulta + "]" ;

    if (operacion == 3)
      if(error == 0)
      mensaje_proceso = "Eliminaci�n registrada [" +
      rut_consulta + "-" + dvrut_consulta + "]"  ;
    else
      mensaje_proceso = "Problemas en Eliminaci�n [" +
      rut_consulta + "-" + dvrut_consulta + "]"  ;



  }
  if (!mensaje_proceso.trim().equals(""))
  onload = "CargarIframe(document.form_cuadro);";
  if (proceso == 1){
    if (par_origen == 0) onload += "CambiarImagenAusencia();";
        else onload += "CargarImagen();";
  } else  if (proceso == 2 ) onload += "CambiarImagenEspera();";
%>
<html>
<head><title>Proceso</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<script LANGUAGE="JavaScript" TYPE="text/javascript">
function CargarIframe(f){
  f.submit();
}
function CargarImagen(){
  window.parent.CambiarImagen(<%=par_i%>,'<%=par_accion%>',<%=par_origen%>);
}
function CambiarImagenAusencia(){
  window.parent.CambiarImagenAusencia(<%=par_i%>,'<%=par_accion%>');
}
function CambiarImagenEspera(){
  window.parent.CambiaImagenFila(<%=operacion%>,<%=par_i%>);
}
</script>
<style type="text/css">
<!--
body {
        margin-left: 0px;
        margin-top: 6px;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body onload="<%=onload%>">
<div align="right" class="Estilo_Naranjo letra8"><%=mensaje_proceso%></div>
<form name="form_cuadro" method="post" action="gim_asistencia_cuadro_adm.jsp" target="iframe_cuadro">
  <input type="hidden" name="sala" value="<%=sala%>">
  <input type="hidden" name="fecha_inicio" value="<%=fecha_inicio%>">
  <input type="hidden" name="dia" value="<%=dia%>">
  <input type="hidden" name="bloque1" value="<%=bloque1%>">
  <input type="hidden" name="bloque2" value="<%=bloque2%>">
</form>
</body>
