<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>
<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 05/06/2007
   // �ltima Actualizaci�n :
   // el recepcionista es un alumno, se solicita el rut y muestran sus datos, si es nuevo se soliciya perfil si es antiguo se puede modificar.
%>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<jsp:useBean id="mant" class="gimnasio.BeanUsuarios"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
   int   rutnum =  request.getParameter("rutnum")!=null &&
                          !request.getParameter("rutnum").equals("null") &&
                          !request.getParameter("rutnum").equals("NULL") &&
                          !request.getParameter("rutnum").equals("")  &&
                          !request.getParameter("rutnum").equals(" ")
                          ?Integer.parseInt(request.getParameter("rutnum")+""):0;
   String   dvrut =  request.getParameter("dvrut")!=null &&
                          !request.getParameter("dvrut").equals("null") &&
                          !request.getParameter("dvrut").equals("NULL") &&
                          !request.getParameter("dvrut").equals("")  &&
                          !request.getParameter("dvrut").equals(" ")
                          ?request.getParameter("dvrut")+"":"";
   String   rut_aux =  request.getParameter("rut_aux")!=null &&
                           !request.getParameter("rut_aux").equals("null") &&
                           !request.getParameter("rut_aux").equals("NULL") &&
                           !request.getParameter("rut_aux").equals("")  &&
                           !request.getParameter("rut_aux").equals(" ")
                          ?request.getParameter("rut_aux")+"":"";
   int   accion =  request.getParameter("accion")!=null &&
                         !request.getParameter("accion").equals("null") &&
                         !request.getParameter("accion").equals("NULL") &&
                         !request.getParameter("accion").equals("")  &&
                         !request.getParameter("accion").equals(" ")
                          ?Integer.parseInt(request.getParameter("accion")+""):0;
   int   perfil =  request.getParameter("perfil")!=null &&
                        !request.getParameter("perfil").equals("null") &&
                        !request.getParameter("perfil").equals("NULL") &&
                        !request.getParameter("perfil").equals("")  &&
                        !request.getParameter("perfil").equals(" ")
                          ?Integer.parseInt(request.getParameter("perfil")+""):0;

   String nom_alumno = "";
   String carr_alumno = "";
   String sit_alumno = "";
   String tel_alumno = "";
   String dir_alumno = "";
   String email_alumno = "";
   String ultmat = "";
   String mensaje = "";
   
   if (session.getAttribute("rut_consulta") != null)
	   session.removeAttribute("rut_consulta");

   int cod_perfil_recep = 0;
   int error = 0;
   if(rutnum>0){

     general.getAbrirConexion();
     general.setGenerarConexionDatosAl();
     Vector v = new Vector();
     v = general.getAlumno(rutnum);
     general.getCerrarConexion();
     if(v.size()>0){
       nom_alumno = v.get(1)+"";
       carr_alumno = v.get(5)+"";
       sit_alumno = v.get(6)+"";
       tel_alumno = v.get(2)+"";
       dir_alumno = v.get(3)+"";
       email_alumno = v.get(4)+"";
       ultmat = v.get(7)+"";
       if (rutnum > 0) {
    	 session.setAttribute("rut_consulta",rutnum+"");
       }
     }
     mant.getAbrirConexion();
     mant.setGenerarConexionUsuario();

     switch (accion){
       case 1: { // agregar
            error = mant.setInsertarPerfilUsuario(rutnum,rut_usuario,perfil);
            if(error == 0) mensaje = "Se registr� satisfactoriamente";
            else mensaje = "Problemas al registrar ";
            error = -1;
        break;
       }
       case 2: { // eliminar
             error = mant.setBorrarPerfilUsuario(rutnum);
             if(error == 0) mensaje = "Se elimin� el perfil";
             else mensaje = "Problemas al eliminar";
             error = -1;
       break;
       }
     }
     cod_perfil_recep = mant.getPerfilUsuario(rutnum);
     mant.getCerrarConexion();
   }

%>
<html>
<head>
<title>Gimnasio - Recepcionista</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script src="js/centrar.js"></script>
<script src="js/valida_login.js"></script>
<script language="JavaScript" src="js/trim.js"></script>
<script language="JavaScript">
function Mensaje(){
<%if(error<0){%>alert ('<%=mensaje%>');<%}%>
}
function noenter() {
  return !(window.event && window.event.keyCode == 13);
}
function enter(e){
  if (window.event) {
    if (window.event.keyCode == 13)
	   validar(document.fingresar);

  }
  else {
    if (e.which == 13)
	 validar(document.fingresar);
  }
}
function AbrirVentana(f,a) {
  var alto  = 600;
  var ancho = 700;
  f.target = "ventana";
  f.action = a;
  var par = 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=yes';
  var w = Centrar03(f.target,alto,ancho,par);
  w.focus();
  f.submit();
}
function validar(form){

	if(trim(form.rut_aux.value)==""){
			alert("Debe ingresar el rut del recepcionista");
			form.rut_aux.focus();
			return false;
		}
		 if (!valida_rut2(form.rut_aux, form.rutnum, form.dvrut, 'RUT')) {
			form.rut_aux.focus();
			return false;
		}
               // return true;
	form.submit();
	}
 function registrar (accion){
   if(accion == 2) {
           if (0 == <%=cod_perfil_recep%>){
             alert ('No se puede eliminar ya que no est� registrado');
             document.fingresar.perfil.focus();
             return false;
           }
           if (<%=cod_perfil_recep%> == 28) texto = "Recepcionista";
           else texto = "Administrador";
           var agree = confirm("�Est� seguro que desea borrar el perfil " + texto +"?");
           if(!agree) return false;

   } else { if (document.fingresar.perfil.value == 0){
     alert ('Debe seleccionar perfil');
     document.fingresar.perfil.focus();
     return false;
   }
    if (document.fingresar.perfil.value == <%=cod_perfil_recep%>){
      alert ('No se han Registrado Cambios');
      document.fingresar.perfil.focus();
      return false;
    }
   }
   document.fingresar.accion.value = accion;
   document.fingresar.submit();
 }
 function changeImageSrc(whichImage,newGraphic) {
   if (document.images) {
       document.images[whichImage].src = newGraphic;
   }
}
 function SinFoto(foto) {
   changeImageSrc(foto,"imagen/sinfoto.jpg");
}
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style></head>

<body onload="<%=(rutnum==0)?"document.fingresar.rut_aux.focus()":"Mensaje();"%>">
<form name="fingresar" method="post" action ="gim_recepcionista_adm.jsp" target="_self">
<input type="hidden" name="listado" value="3">
<input type="hidden" name="accion" value="">

<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="37" class="Estilo_Verde">RECEPCIONISTA</td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="995" valign="top" bgcolor="#FFFFFF"><table width="100%" height="0%"  border="0" cellpadding="1" cellspacing="1">
        <tr>
          <td width="13%" height="24%" bgcolor="#FFFFFF"><span class="Estilo_Gris">RUT</span></td>
          <td width="87%" bgcolor="#FFFFFF">
                <input type="text" name="rut_aux" size="12" maxlength="12" value ="<%=rut_aux%>" onKeyPress="enter(event,1);return noenter()" onChange="checkRutField(document.fingresar.rut_aux.value, document.fingresar);" class="Input01">
                <input type="hidden" name="rutnum" value="<%=rutnum%>">
                <input type="hidden" name="dvrut" value="<%=dvrut%>">
            <a href="#"  onClick="validar(document.fingresar);" class="AIngr"><img src="imagen/gim_lupa.gif" width="16" height="18" border="0" align="absmiddle"> Buscar</a> </td>
        </tr>
		<%if(rutnum>0){
                   if(!nom_alumno.trim().equals("")){%>
        <tr bgcolor="#FFFFFF">
          <td height="17"><span class="Estilo_Gris">Nombre</span></td>
          <td height="17"><span class="Estilo_Azul"><%=nom_alumno%></span></td>
        </tr>
        <tr>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Gris">Carrera</span></td>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(!carr_alumno.trim().equals("null"))?carr_alumno:""%> </span></td>
        </tr>
        <tr>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Gris">Situaci&oacute;n</span></td>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=sit_alumno%><%=(!ultmat.trim().equals("0"))?", �ltima Matr�cula "+ultmat:""%> </span></td>
        </tr>
        <tr>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Gris">Tel&eacute;fono</span></td>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(!tel_alumno.trim().equals("null") && !tel_alumno.trim().equals("0"))?tel_alumno:""%></span></td>
        </tr>
        <tr>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Gris">Direcci&oacute;n</span></td>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(!dir_alumno.trim().equals("null"))?dir_alumno:""%></span></td>
        </tr>
        <tr>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Gris">E-Mail</span></td>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(!email_alumno.trim().equals("null"))?email_alumno:""%></span></td>
        </tr>
        <tr>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Gris">Perfil</span></td>
          <td height="17" bgcolor="#FFFFFF"><select name="perfil" class="Option" id="perfil">
              <option value="0" selected>--Seleccione --</option>
              <option value="2" <%=(cod_perfil_recep==2)?"selected":""%>>Administrador</option>
              <option value="28" <%=(cod_perfil_recep==28)?"selected":""%>>Recepcionista</option>
          </select></td>
        </tr><%} else {%>  <tr>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Gris">&nbsp;</span></td>
          <td height="17" bgcolor="#FFFFFF"><span class="Estilo_Azul">Este Alumno no est� registrado</span></td>
        </tr>
		<%}}%>
    </table></td>
	    <td width="122" align="center" valign="top" bgcolor="#FFFFFF">
	 <% if (rutnum == 0){%>&nbsp;
    <%} else { %>
    <img name="foto" src="/foto" align="absmiddle" onError="SinFoto('foto');" width="120" height="135"
border="1">
    <%}%></td>
  </tr>
</table>
<%if(rutnum>0 && !nom_alumno.trim().equals("")){%>
<table width="100%"  border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td width="9%" height="35"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseOK.gif">
      <tr onClick="registrar(1);" onMouseOver="BotonOver(this,'Agregar')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino"><%=(cod_perfil_recep==0)?"Agregar":"Modificar"%></td>
      </tr>
    </table></td>
    <td width="91%"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseELIMINAR.gif">
      <tr onClick="registrar(2);" onMouseOver="BotonOver(this,'Eliminar')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino">Eliminar</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</form>
</body>
</html>