<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>
<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 18/07/2007
   // �ltima Actualizaci�n :
%>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<jsp:useBean id="us" class="gimnasio.BeanUsuarios"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");

  int rut_usuario      = session.getAttribute("rut_usuario") != null?
                         Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int cod_perfil       = session.getAttribute("cod_perfil") != null?
                         Integer.parseInt(session.getAttribute("cod_perfil")+""):0;

  int sala             = request.getParameter("sala")!=null &&
                         !request.getParameter("sala").equals("null") &&
                         !request.getParameter("sala").equals("NULL") &&
                         !request.getParameter("sala").equals("")  &&
                         !request.getParameter("sala").equals(" ")?
                         Integer.parseInt(request.getParameter("sala")+""):0;
  String fecha_inicio  = request.getParameter("fecha_inicio")!=null &&
                         !request.getParameter("fecha_inicio").equals("null") &&
                         !request.getParameter("fecha_inicio").equals("NULL") &&
                         !request.getParameter("fecha_inicio").equals("")  &&
                         !request.getParameter("fecha_inicio").equals(" ")?
                         request.getParameter("fecha_inicio")+"":"";
  String fecha_termino = request.getParameter("fecha_termino")!=null &&
                         !request.getParameter("fecha_termino").equals("null") &&
                         !request.getParameter("fecha_termino").equals("NULL") &&
                         !request.getParameter("fecha_termino").equals("")  &&
                         !request.getParameter("fecha_termino").equals(" ")?
                         request.getParameter("fecha_termino")+"":"";
  int accion           = request.getParameter("accion")!=null &&
                         !request.getParameter("accion").equals("null") &&
                         !request.getParameter("accion").equals("NULL") &&
                         !request.getParameter("accion").equals("")  &&
                         !request.getParameter("accion").equals(" ")?
                         Integer.parseInt(request.getParameter("accion")+""):0;
  String id_reg        = request.getParameter("id_reg")!=null &&
                         !request.getParameter("id_reg").equals("null") &&
                         !request.getParameter("id_reg").equals("NULL") &&
                         !request.getParameter("id_reg").equals("")  &&
                         !request.getParameter("id_reg").equals(" ")?
                         request.getParameter("id_reg")+"":"";

  int rut              = request.getParameter("rut")!=null &&
                         !request.getParameter("rut").equals("null") &&
                         !request.getParameter("rut").equals("NULL") &&
                         !request.getParameter("rut").equals("")  &&
                         !request.getParameter("rut").equals(" ")?
                         Integer.parseInt(request.getParameter("rut")+""):0;
  String rut_aux       = request.getParameter("rut_aux")!=null &&
                         !request.getParameter("rut_aux").equals("null") &&
                         !request.getParameter("rut_aux").equals("NULL") &&
                         !request.getParameter("rut_aux").equals("")  &&
                         !request.getParameter("rut_aux").equals(" ")?
                         request.getParameter("rut_aux")+"":"";
  int rutnum           = request.getParameter("rutnum")!=null &&
                         !request.getParameter("rutnum").equals("null") &&
                         !request.getParameter("rutnum").equals("NULL") &&
                         !request.getParameter("rutnum").equals("")  &&
                         !request.getParameter("rutnum").equals(" ")?
                         Integer.parseInt(request.getParameter("rutnum")+""):0;
  String dvrut         = request.getParameter("dvrut")!=null &&
                         !request.getParameter("dvrut").equals("null") &&
                         !request.getParameter("dvrut").equals("NULL") &&
                         !request.getParameter("dvrut").equals("")  &&
                         !request.getParameter("dvrut").equals(" ")?
                         request.getParameter("dvrut")+"":"";

 // variables
  Vector vec_periodo = new Vector();
  Vector vec_horabloque1 = new Vector();
  Vector vec_horario_s =  new Vector();
  Vector vec_aseo =  new Vector();
  Vector vec_paralelo =  new Vector();
  Vector vec_horario =  new Vector();
  Vector vec_horario1 = new Vector();
  Vector vec_bloque = new Vector();
  Vector vec_reserva = new Vector();
  Vector vec_fecha_inicio = new Vector();
  Vector vec_fecha_termino = new Vector();
  Vector v = new Vector();
  Vector vec_fecha_reserva = new Vector();
  Vector vec_periodo_final = new Vector();
  Vector vec_periodo_ant = new Vector();


  String fec_ini = "";
  String fec_ter = "";
  String f = "";
  String dd = "";
  String m = "";
  String a = "";
  String estilo = "";
  String nombre = "";
  String selec_inicio = "";
  String selec_termino = "";
  String mensaje = "";
  String id_per = "";
  String id_per_ant = "";
  String semana_act1 = "";
  String semana_act2 = "";
//  String ini_s2  = "";
//  String ter_s2 = "";
//  String id_per_s2 = "";

  int anno = 0;
  int semestre = 0;
  int cupo = 0;
  int vacante = 0;
  int f1 = 0;
  int f2 = 0;
  int f3 = 0;
  int cod_reserva = 0;
  int error = 0;
  int fact = 0;
  int tipo_reserva = 0;

  boolean soloconsulta = false;
  if (cod_perfil==28 || cod_perfil==2) soloconsulta = true;

  general.getAbrirConexion();

  general.setGenerarConexionReserva();
  general.setGenerarConexionHorario();
  vec_periodo = general.getPeriodoGimnasioVigente(sala);
  Calendar Cal = Calendar.getInstance();
//System.out.println("2 vecPeriodo: "+vec_periodo);
  if (vec_periodo.size()>0){
    anno     = (vec_periodo.size()>0)?
               Integer.parseInt(vec_periodo.get(3)+""):0;
    semestre = (vec_periodo.size()>0)?
               Integer.parseInt(vec_periodo.get(4)+""):0;
    id_per   = (vec_periodo.size()>0)?vec_periodo.get(2)+"":"";
    vec_periodo_ant = general.getPeriodoGimnasio(anno, semestre, sala);
    if (vec_periodo_ant.size() > 1){
      id_per_ant = ((Vector)vec_periodo_ant.get(1)).get(5)+"";
    }

    // Fecha actual
    m  = ((Cal.get(Calendar.MONTH )+1) < 10)?
         ("0"+(Cal.get(Calendar.MONTH )+1)):
         (""+(Cal.get(Calendar.MONTH )+1));
    dd = (Cal.get(Calendar.DATE) < 10)?
         ("0"+Cal.get(Calendar.DATE)):
         (""+Cal.get(Calendar.DATE));
    fact = Integer.parseInt(Cal.get(Calendar.YEAR) + m + dd);
//System.out.println("FACT: "+fact);
    /*****/
    fec_ini = (vec_periodo.size()>0)?vec_periodo.get(0)+"":"";
//    m = (Integer.parseInt(fec_ini.substring(3,5)) < 10)?
//        ("0"+Integer.parseInt(fec_ini.substring(3,5))):
//        (""+Integer.parseInt(fec_ini.substring(3,5)));
//    dd = (Integer.parseInt(fec_ini.substring(0,2)) < 10)?
//         ("0"+Integer.parseInt(fec_ini.substring(0,2))):
//         (""+Integer.parseInt(fec_ini.substring(0,2)));
//    a  = (Integer.parseInt(fec_ini.substring(6,10)) < 10)?
//         ("0"+Integer.parseInt(fec_ini.substring(6,10))):
//         (""+Integer.parseInt(fec_ini.substring(6,10)));;
//    f1 = Integer.parseInt(a + m + dd);
//    f  = dd + "/" + m + "/" +Cal.get(Calendar.YEAR);
     f1 = (vec_periodo.size()>0)?Integer.parseInt(vec_periodo.get(5)+""):0;
     f = fec_ini;
    fec_ter = (vec_periodo.size()>0)?vec_periodo.get(1)+"":"";
//    m  = (Integer.parseInt(fec_ter.substring(3,5)) < 10)?
//         ("0"+Integer.parseInt(fec_ter.substring(3,5))):
//         (""+Integer.parseInt(fec_ter.substring(3,5)));
//    dd = (Integer.parseInt(fec_ter.substring(0,2)) < 10)?
//         ("0"+Integer.parseInt(fec_ter.substring(0,2))):
//         (""+Integer.parseInt(fec_ter.substring(0,2)));
//    a  = (Integer.parseInt(fec_ter.substring(6,10)) < 10)?
//         ("0"+Integer.parseInt(fec_ter.substring(6,10))):
//         (""+Integer.parseInt(fec_ter.substring(6,10)));;
//    f2 = Integer.parseInt(a + m + dd);
     f2 = (vec_periodo.size()>0)?Integer.parseInt(vec_periodo.get(6)+""):0;




    v = new Vector();
    v.addElement(f1+"");

    String s1 = "";
    String s2 = "";
    while (f1 < f2){
      // para llenar periodo con aammdd
      vec_fecha_inicio.addElement(f);
      s1 = f;

      Cal.set(Integer.parseInt(fec_ini.substring(6,10)),
              (Integer.parseInt(fec_ini.substring(3,5))-1),
              Integer.parseInt(fec_ini.substring(0,2)));
      Cal.add(Calendar.DATE, +5);
      m  = ((Cal.get(Calendar.MONTH )+1) < 10)?
           ("0"+(Cal.get(Calendar.MONTH )+1)):
           (""+(Cal.get(Calendar.MONTH )+1));
      dd = (Cal.get(Calendar.DATE) < 10)?
           ("0"+Cal.get(Calendar.DATE)):
           (""+Cal.get(Calendar.DATE));
      f1 = Integer.parseInt(Cal.get(Calendar.YEAR) + m + dd);

      f = dd + "/" + m + "/" +Cal.get(Calendar.YEAR);
      v.addElement(f1+"");
      vec_periodo_final.addElement(v);
      vec_fecha_termino.addElement(f);
      s2 = f;

      Cal.add(Calendar.DATE, +2);
      m  = ((Cal.get(Calendar.MONTH )+1) < 10)?
           ("0"+(Cal.get(Calendar.MONTH )+1)):
           (""+(Cal.get(Calendar.MONTH )+1));
      dd = (Cal.get(Calendar.DATE) < 10)?
           ("0"+Cal.get(Calendar.DATE)):
           (""+Cal.get(Calendar.DATE));
      f = dd + "/" + m + "/" +Cal.get(Calendar.YEAR);

      fec_ini = f;
      f3 =  Integer.parseInt(Cal.get(Calendar.YEAR) + m + dd);
      v = new Vector();
      v.addElement(f3+"");
      //System.out.println("F1: "+f1+" FACT: "+fact+" F3: "+f3);
      if (semana_act1.length() == 0 &&
          f1 >= fact && fact <= f3) {
        semana_act1 = s1;
        semana_act2 = s2;
      }

    }

  }


   // Busca datos de alumno
  Vector alu_vec = new Vector();
  Vector alu_vec_reg = new Vector();
  Vector alu_vec_rama = new Vector();
  String alu_calidad = "";
  String alu_rama = "";
  String alu_rut = "";
  String alu_nombre = "";
  String alu_situacion = "";
  String alu_nom_estado_asist = "";
  String texto_inhabilitado = "";
  String texto_inhabilitado2 = "";
  boolean reservaInhabilitada = false;
  boolean buscaVi�a = false;
  int alu_cod_estado_asist = 0;
  int rut_reserva = 0;
  String tipo_alumno = "0";
  String procedencia = "CC";
  int rut_graba = (rut_usuario != rut)?rut_usuario:603;
//System.out.println("vecPeriodo: "+vec_periodo);
  int f1_insc = (vec_periodo.size()>0)?Integer.parseInt(vec_periodo.get(7)+""):0;
  int f2_insc = (vec_periodo.size()>0)?Integer.parseInt(vec_periodo.get(8)+""):0;
//System.out.println("f1_insc: "+f1_insc+" f2_insc: "+f2_insc);
  if((fact >= f1_insc) && (fact <= f2_insc) && (f1_insc > 0) && (f2_insc > 0) )
        texto_inhabilitado2 = "";
      else {
        reservaInhabilitada = true;
        texto_inhabilitado2 = "Se encuentra fuera del per�odo ";
        if (sala == 10335)  texto_inhabilitado2 += "de inscripci�n";
      }
  /*
  No RAMA
  No Tutoria
  Matricula vigente
  No sancionados
  */
   if (!reservaInhabilitada){
  if (soloconsulta) rut_reserva = rutnum;
  else rut_reserva = rut_usuario;
  if (rut_reserva > 0) {
    general.setGenerarConexionDatosAl();
    general.setGenerarConexionRegistro();
    alu_vec = general.getAlumno(rut_reserva);
    alu_vec_reg = general.getRegistroUsuario(rut_reserva);
    if (id_reg.trim().equals(""))
      id_reg = (alu_vec_reg.size()>0)?alu_vec_reg.get(4)+"":"";
    if (id_reg.length() > 0 && id_per.length() > 0 &&
        id_per_ant.length() > 0) {
      general.getEstadoAsistencia(id_reg, id_per_ant);
      alu_cod_estado_asist = general.getEstadoAsistenciaCod();
      alu_nom_estado_asist = general.getEstadoAsistenciaNom();
      if (alu_cod_estado_asist == 5) {
        reservaInhabilitada = true;
        texto_inhabilitado = "SANCIONADO.<BR>Espere el pr�ximo per�odo de reservas.";
      }
    }
        if (!reservaInhabilitada){
        if (/*anno > 0 &&
            semestre > 0 && */procedencia.equals("CC") &&
            !general.getHabilitadoReserva (rut_reserva, anno, semestre) ) {
          reservaInhabilitada = true;
          texto_inhabilitado = "No est� habilitado para reservar.";
        }
         
        boolean habVi�a = false;
        Vector alu_vec_vi�a = new Vector();
        /*if ((alu_vec.size()>0 && reservaInhabilitada) ||
            (alu_vec.size()==0)){ // buscar si es de vi�a
          general.getAbrirConexionVi�a();
          general.setGenerarConexionDatosVi�a();
          alu_vec_vi�a = general.getAlumnoVi�a(rut_reserva);
          if (alu_vec_vi�a.size() > 0) {
            habVi�a = general.getHabilitadoReservaVi�a (rut_reserva, anno, semestre);
            procedencia = "V"; // alumno vi�a
            alu_vec = alu_vec_vi�a;
          }
          general.getCerrarConexionVi�a();
        }*/
        if (habVi�a) {
          texto_inhabilitado = "";
          reservaInhabilitada = false;
        }
        if (/*anno > 0 &&
            semestre > 0 &&*/ procedencia.equals("V") &&
            !habVi�a ) {
          reservaInhabilitada = true;
          texto_inhabilitado = "No posee matr�cula para el per�odo actual.";
        }
    if (alu_vec.size() > 0){
      alu_rut = alu_vec.get(12)+"";
      alu_nombre = alu_vec.get(1)+"";
      alu_situacion = alu_vec.get(5)+"";
    /*  if (!(alu_vec.get(7)+"").trim().equals(""))
        alu_situacion += (alu_vec.get(7).equals("0")?"":", �ltima matr�cula " + alu_vec.get(7)+"");*/
    } else {
      // buscar funcionario pero queda inhabilitado
      us.getAbrirConexion();us.setGenerarConexionUsuario();
      alu_vec = us.getFuncionario(rut_reserva);
      us.getCerrarConexion();
      /*if(alu_vec.size() == 0){
        general.getAbrirConexionVi�a();
        general.setGenerarConexionDatosVi�a();
        alu_vec = general.getFuncionarioVi�a(rut_reserva);
        general.getCerrarConexionVi�a();
        if (alu_vec.size() > 0)
            texto_inhabilitado = "El Funcionario no puede hacer reserva";
        else   texto_inhabilitado = "RUT inv�lido";
        if (alu_vec.size()>0){
        alu_rut = alu_vec.get(13)+"";
        alu_nombre = alu_vec.get(0)+" "+alu_vec.get(1)+" "+alu_vec.get(2);
        if(Integer.parseInt(alu_vec.get(16)+"") == 3)
        alu_situacion = "Funcionario Vi�a";
        else
          alu_situacion = "Funcionario";

      }
        
      } else  {*/
        	if (alu_vec.size() > 0){
  	        texto_inhabilitado = "El Funcionario no puede hacer reserva";
  	        if (alu_vec.size()>0){
  		        alu_rut = alu_vec.get(13)+"";
  		        alu_nombre = alu_vec.get(2)+" "+alu_vec.get(3)+" "+alu_vec.get(4);
  		        if(Integer.parseInt(alu_vec.get(16)+"") == 3)
  		        alu_situacion = "Funcionario Vi�a";
  		        else
  		          alu_situacion = "Funcionario";
  	
  	        }
        	}
       //}
      reservaInhabilitada = true;

    }
      }
   if(alu_vec_reg.size()>0 &&
      ((alu_vec_reg.get(1)+"").trim().equals("3") ||
      (alu_vec_reg.get(1)+"").trim().equals("6") )) {// es de rama
     reservaInhabilitada = true;
     texto_inhabilitado = "El alumno es de Rama Deportiva.<br>No puede reservar por esta v�a.";
     alu_vec_rama = general.getAlumnoRama(rut_reserva);
     alu_calidad = "Rama ";
     if (alu_vec_rama.size()>0)
       alu_rama = alu_vec_rama.get(1)+"";
     else alu_calidad +=" - Vi�a";
   }
   if (alu_vec_reg.size() > 0 &&
       (alu_vec_reg.get(1)+"").trim().equals("2")) {
     alu_calidad = "Tutor�a";
     reservaInhabilitada = true;
     texto_inhabilitado = "El alumno es de Tutor�a.<br>Su reserva ya fue realizada para todo el semestre.";
    // vec_periodo = general.getPeriodoGimnasioVigente(10336);

//      if (vec_periodo.size() > 0 ){
//        ini_s2      = vec_periodo.get(0)+"";
//        ter_s2     = vec_periodo.get(1)+"";
//        id_per_s2   = (vec_periodo.size()>0)?vec_periodo.get(2)+"":"";
//      }
    }
 if (!reservaInhabilitada && alu_vec_reg.size() == 0) {
   if (procedencia.equals("CC"))
     tipo_alumno = "1";
   else
     tipo_alumno = "5";
 }
 if (alu_vec_reg.size() == 0 && alu_vec.size() > 0 &&
     (cod_perfil == 8)){
   reservaInhabilitada = true;
   texto_inhabilitado = "El alumno no se encuentra registrado.<br>Debe dirigirse al Gimnasio III a registrarse.";

 }
  }
  }
  // fin verificaci�n alumno

 if (accion > 0){
   switch (accion){
     case 1:{

       String linea1  =  request.getParameter("linea1")!=null &&
                         !request.getParameter("linea1").equals("null") &&
                         !request.getParameter("linea1").equals("NULL") &&
                         !request.getParameter("linea1").equals("")  &&
                         !request.getParameter("linea1").equals(" ")?
                         request.getParameter("linea1")+"":"";
       String linea2  =  request.getParameter("linea2")!=null &&
                         !request.getParameter("linea2").equals("null") &&
                         !request.getParameter("linea2").equals("NULL") &&
                         !request.getParameter("linea2").equals("")  &&
                         !request.getParameter("linea2").equals(" ")?
                         request.getParameter("linea2")+"":"";
       int a�o        =  request.getParameter("a�o")!=null &&
                         !request.getParameter("a�o").equals("null") &&
                         !request.getParameter("a�o").equals("NULL") &&
                         !request.getParameter("a�o").equals("")  &&
                         !request.getParameter("a�o").equals(" ")?
                         Integer.parseInt(request.getParameter("a�o")+""):0;
       int sem        =  request.getParameter("semestre")!=null &&
                         !request.getParameter("semestre").equals("null") &&
                         !request.getParameter("semestre").equals("NULL") &&
                         !request.getParameter("semestre").equals("")  &&
                         !request.getParameter("semestre").equals(" ")?
                         Integer.parseInt(request.getParameter("semestre")+""):0;
       String periodo =  request.getParameter("periodo")!=null &&
                         !request.getParameter("periodo").equals("null") &&
                         !request.getParameter("periodo").equals("NULL") &&
                         !request.getParameter("periodo").equals("")  &&
                         !request.getParameter("periodo").equals(" ")?
                         request.getParameter("periodo")+"":"";

       String texto_fec_ini = "";
//       Cal.set(Integer.parseInt(fecha_inicio.substring(6,10)),
//               (Integer.parseInt(fecha_inicio.substring(3,5))-1),
//               Integer.parseInt(fecha_inicio.substring(0,2)));
//       m  = ((Cal.get(Calendar.MONTH )+1) < 10)?
//            ("0"+(Cal.get(Calendar.MONTH )+1)):
//            (""+Cal.get(Calendar.MONTH )+1);
//       dd = (Cal.get(Calendar.DATE) < 10)?
//            ("0"+Cal.get(Calendar.DATE)):
//            (""+Cal.get(Calendar.DATE));
//       f1 = Integer.parseInt(Cal.get(Calendar.YEAR) + m + dd);
       m  = (Integer.parseInt(fecha_inicio.substring(3,5)) < 10)?
            ("0"+Integer.parseInt(fecha_inicio.substring(3,5))):
            (""+Integer.parseInt(fecha_inicio.substring(3,5)));
       dd = (Integer.parseInt(fecha_inicio.substring(0,2)) < 10)?
            ("0"+Integer.parseInt(fecha_inicio.substring(0,2))):
            (""+Integer.parseInt(fecha_inicio.substring(0,2)));
       a  = (Integer.parseInt(fecha_inicio.substring(6,10)) < 10)?
            ("0"+Integer.parseInt(fecha_inicio.substring(6,10))):
            (""+Integer.parseInt(fecha_inicio.substring(6,10)));
       f1 = Integer.parseInt(a + m + dd);



//       Cal.set(Integer.parseInt(fecha_termino.substring(6,10)),
//               (Integer.parseInt(fecha_termino.substring(3,5))-1),
//               Integer.parseInt(fecha_termino.substring(0,2)));
//       m  = ((Cal.get(Calendar.MONTH )+1) < 10)?
//            ("0"+(Cal.get(Calendar.MONTH )+1)):
//            (""+Cal.get(Calendar.MONTH )+1);
//       dd = (Cal.get(Calendar.DATE) < 10)?
//            ("0"+Cal.get(Calendar.DATE)):
//            (""+Cal.get(Calendar.DATE));
//
//       f2 = Integer.parseInt(Cal.get(Calendar.YEAR) + m + dd);

       m  = (Integer.parseInt(fecha_termino.substring(3,5)) < 10)?
            ("0"+Integer.parseInt(fecha_termino.substring(3,5))):
            (""+Integer.parseInt(fecha_termino.substring(3,5)));
       dd = (Integer.parseInt(fecha_termino.substring(0,2)) < 10)?
            ("0"+Integer.parseInt(fecha_termino.substring(0,2))):
            (""+Integer.parseInt(fecha_termino.substring(0,2)));
       a  = (Integer.parseInt(fecha_termino.substring(6,10)) < 10)?
            ("0"+Integer.parseInt(fecha_termino.substring(6,10))):
            (""+Integer.parseInt(fecha_termino.substring(6,10)));
       f2 = Integer.parseInt(a + m + dd);
       int sw = 0;
       int ind2 = 0;

       for (int i=0;i<vec_periodo_final.size() && sw==0;i++){
         if ( Integer.parseInt(((Vector)vec_periodo_final.get(i)).get(0)+"") >= f1 &&
              Integer.parseInt(((Vector)vec_periodo_final.get(i)).get(1)+"") <= f2) {
         //cupo = general.getCupoGimnasio(a�o, sem, dia, bloque1, bloque2, sala);
         Vector vec = new Vector();
         vec.addElement(a�o+"");// a�o
         vec.addElement(sem+"");// semestre
         vec.addElement(vec_fecha_inicio.get(i)); // fecha_inicio
         vec.addElement(vec_fecha_termino.get(i));  // fecha_termino
         vec.addElement(periodo); // pergim_id
         vec.addElement(linea1); // linea1
         vec.addElement(linea2); // linea2
         vec.addElement(rut+""); // rut
         vec.addElement(rut_graba+"");  //rut_usuario
         vec.addElement("0");
         vec.addElement(tipo_alumno); // tipo_alumno
         vec.addElement("1"); // cod_cupo
         vec.addElement(sala+""); // Sala (M�quinas o Aerobica)
         
         error = general.setReservaGimnasio(vec);
         
         //System.out.println("GimReserva Error: "+error);
       }
       if (error != 0) sw = 1;
       }
       if (error == 0){ 
    	   mensaje= "Reserva ingresada exitosamente.";
       }else if (error == -4){
    	   mensaje = "Error al obtner cupo de asignatura";
       }else if (error == -5){
    	   mensaje = "Error al obtner cantidad de inscritos";
       }else if (error == -6){
    	   mensaje = "No se puede registrar reserva, asignatura sin cupo";
       }else{	   
    	   mensaje = "Problemas al registrar";
       }
       break;
   }
   case 2: {
     error = general.setEliminaReserva(id_reg);
     if (error == 0) mensaje = "Se elimin� satisfactoriamente.";
     if (error == -1) mensaje = "No se pudo eliminar, pues hay asistencia registrada.";
     if (error == -2) mensaje = "Problemas al eliminar";
     break;
   }
   }
   error = -1;
 }

if (vec_periodo.size()>0){
  if (fecha_inicio.trim().equals("null") ||
      fecha_inicio.trim().equals("") ||
      fecha_termino.trim().equals("")) {
    //fecha_inicio = vec_fecha_inicio.get(0)+"";
    //fecha_termino = vec_fecha_termino.get(0)+"";
    fecha_inicio = semana_act1;
    fecha_termino = semana_act2;
  }
  if(!fecha_inicio.trim().equals("") &&
     !fecha_termino.trim().equals(""))
  /*  if(     alu_calidad.trim().equals("Tutor�a") &&
            reservaInhabilitada)
      vec_reserva = general.getBloqueReserva(rut,
      id_per_s2,
      Integer.parseInt(ini_s2.substring(6,10)+
      ini_s2.substring(3,5)+
      ini_s2.substring(0,2)),
      Integer.parseInt(ter_s2.substring(6,10)+
      ter_s2.substring(3,5)+
      ter_s2.substring(0,2)));

    else*/
     vec_reserva = general.getBloqueReserva(rut,
          id_per,
          Integer.parseInt(fecha_inicio.substring(6,10)+
    fecha_inicio.substring(3,5)+
    fecha_inicio.substring(0,2)),
    Integer.parseInt(fecha_termino.substring(6,10)+
    fecha_termino.substring(3,5)+
    fecha_termino.substring(0,2)));
}

    vec_horabloque1 = general.getHoraBloque(sala, anno, semestre);
    vec_horario_s = general.getHorarioSala(anno,semestre,sala);
    vec_aseo = general.getHorarioAseo(sala,anno,semestre);
    vec_paralelo = general.getParalelo(anno,semestre);
    vec_horario = general.getHorario(anno,semestre,sala);

    /****/
    // se llena con los cod_bloques
 for(int i=0;i<vec_horabloque1.size();i++){
   vec_bloque.addElement(((Vector)vec_horabloque1.get(i)).get(0)+"");
 }

 // se llena con estilos
 Vector vec_color = new Vector();
 vec_color.addElement("Estilo_Azulino");
 vec_color.addElement("Estilo_Cafe");
 vec_color.addElement("Estilo_Morado");
 vec_color.addElement("Estilo_Naranjo");
 vec_color.addElement("Estilo_AzulinoClaro");
 vec_color.addElement("Estilo_Verde");
 vec_color.addElement("Estilo_MoradoClaro");
 vec_color.addElement("Estilo_Azul");

 // se llena con ebgcolor
Vector vec_bgcolor = new Vector();
 vec_bgcolor.addElement("#B5D9EE"); // azulino
 vec_bgcolor.addElement("#663300"); // caf�
 vec_bgcolor.addElement("#6600CC"); // morado
 vec_bgcolor.addElement("#CC6600"); // naranjo
 vec_bgcolor.addElement("#006699"); // azulino claro
 vec_bgcolor.addElement("#FFE6FF"); // verde
 vec_bgcolor.addElement("#663366"); // morado claro
 vec_bgcolor.addElement("#003366"); // azul


   // agrega a plantilla el horario de sala
    int ind_color = 0;
    Vector vec_nom_asign = new Vector();
    Vector vn = new Vector();
    Vector vg = new Vector();

    for (int y = 0; y < vec_horario_s.size(); y++){
      nombre = ((Vector)vec_horario_s.get(y)).get(3)+"";
      v = new Vector();
      v.addElement(((Vector)vec_horario_s.get(y)).get(0)+""); // 0 dia
      v.addElement(((Vector)vec_horario_s.get(y)).get(1)+""); // 1 bl inicio
      v.addElement(((Vector)vec_horario_s.get(y)).get(2)+""); // 2 bl termino
      v.addElement(nombre); // 3 nombre
      v.addElement(((Vector)vec_horario_s.get(y)).get(4)+""); // 4 prof



     int tipo_ocup = 0;
     String bg = "";
     tipo_ocup   = Integer.parseInt(((Vector)vec_horario_s.get(y)).get(5)+"");

     v.addElement(tipo_ocup+""); // 5 tipo reserva

     if (sala==10335){
       // tipo reserva
      switch (tipo_ocup){
        case 1 :{estilo = "Estilo_Azulino"; // alumnos
                 bg = "#B5D9EE";
          break;
        }
        case 2 : {estilo = "Estilo_Naranjo"; // funcionarios
                 bg = "#FDEACE";
          break;
        }
        case 3 : { estilo = "Estilo_Morado";   // rector�a
                 bg = "#E2D5F0";
          break;
        }

        case 4 : { estilo = "Estilo_MoradoClaro"; // directores
                 bg = "#FFE6FF";
          break;
        }
      }
      } else {
           if(vec_nom_asign.indexOf(nombre)<0) {
             vn.addElement(vec_color.get(ind_color));
             vg.addElement(vec_bgcolor.get(ind_color));
             vec_nom_asign.addElement(nombre);
             estilo = vec_color.get(ind_color)+"";
             bg = vec_bgcolor.get(ind_color)+"";
           } else {
                  estilo = vn.get(vec_nom_asign.indexOf(nombre))+"";
                  bg =  vg.get(vec_nom_asign.indexOf(nombre))+"";
           }
           if (ind_color < vec_color.size()-1) ind_color++;
           else ind_color = 0;
      }
       v.addElement(estilo); // 6 estilo
       v.addElement(bg); // 7 bg color
       v.addElement(((Vector)vec_horario_s.get(y)).get(6)+""); // 8 tipo reserva
       v.addElement(((Vector)vec_horario_s.get(y)).get(7)+""); // 9 cod_asign
       v.addElement(((Vector)vec_horario_s.get(y)).get(9)+""); // 10 paralelo
       v.addElement(String.valueOf(Integer.parseInt(((Vector)vec_horario_s.get(y)).get(8)+"") -
                    Integer.parseInt(((Vector)vec_horario_s.get(y)).get(10)+""))); // 11 cupo
       vacante = general.getCantidadReservados(anno, semestre,
                 Integer.parseInt(((Vector)vec_horario_s.get(y)).get(7)+"") ,
                 Integer.parseInt(((Vector)vec_horario_s.get(y)).get(9)+""),
                 Integer.parseInt(((Vector)vec_horario_s.get(y)).get(0)+""),
                 sala, Integer.parseInt(((Vector)vec_horario_s.get(y)).get(1)+""),
                 Integer.parseInt(((Vector)vec_horario_s.get(y)).get(2)+""),
                 Integer.parseInt(fecha_inicio.substring(6,10)+
                 fecha_inicio.substring(3,5)+fecha_inicio.substring(0,2)),
                 Integer.parseInt(fecha_termino.substring(6,10)+
                 fecha_termino.substring(3,5)+fecha_termino.substring(0,2)));
       vacante = Integer.parseInt(((Vector)vec_horario_s.get(y)).get(8)+"") -
                 Integer.parseInt(((Vector)vec_horario_s.get(y)).get(10)+"") -
                 vacante;
       if (vacante < 0) vacante = 0;
       v.addElement(vacante+""); // 12 - vacante
       vec_fecha_reserva = general.getFechasReservados(anno, semestre,
                 Integer.parseInt(((Vector)vec_horario_s.get(y)).get(7)+"") ,
                 Integer.parseInt(((Vector)vec_horario_s.get(y)).get(9)+""),
                 Integer.parseInt(((Vector)vec_horario_s.get(y)).get(0)+""),
                 sala, Integer.parseInt(((Vector)vec_horario_s.get(y)).get(1)+""),
                 Integer.parseInt(((Vector)vec_horario_s.get(y)).get(2)+""),
                 Integer.parseInt(fecha_inicio.substring(6,10)+
                 fecha_inicio.substring(3,5)+fecha_inicio.substring(0,2)),
                 Integer.parseInt(fecha_termino.substring(6,10)+
                 fecha_termino.substring(3,5)+fecha_termino.substring(0,2)));
       v.addElement(vec_fecha_reserva); // 13 fechas de reserva

       // Estado de la clase
       int estado_clase = general.getEstadoClaseGimnasioSemana
       (fecha_inicio.substring(6,10) + // fecha inicio
       fecha_inicio.substring(3,5) +
       fecha_inicio.substring(0,2),
       fecha_termino.substring(6,10) + // fecha t�rmino
       fecha_termino.substring(3,5) +
       fecha_termino.substring(0,2),
       Integer.parseInt(((Vector)vec_horario_s.get(y)).get(7)+""), // cod_asign
       anno, semestre,
       Integer.parseInt(((Vector)vec_horario_s.get(y)).get(9)+""), // paralelo
       1, 1, // jornada, sede
       Integer.parseInt(((Vector)vec_horario_s.get(y)).get(0)+""), // d�a
       Integer.parseInt(((Vector)vec_horario_s.get(y)).get(1)+""), // bloque 1
       Integer.parseInt(((Vector)vec_horario_s.get(y)).get(2)+"")); // bloque 2

       v.addElement(estado_clase+""); // 14 - (1 - Cerrada, 2 - Anulada, 3 - Suspendida)

       vec_horario1.addElement(v);

     }


  // agrega a vec horario el aseo
     Vector v_hor = new Vector();
     Vector blo_inicio = new Vector();
     Vector blo_fin = new Vector();
     String b1 = "";
     String b2 = "";
     String ndia = "";
     int bloini = 0;
     int blofin = 0;
  for (int y = 0; y < vec_aseo.size(); y++){
   int dia  = Integer.parseInt(((Vector)vec_aseo.get(y)).get(0)+"");
   int diafin  = Integer.parseInt(((Vector)vec_aseo.get(y)).get(1)+"");
   int blo1 = vec_bloque.indexOf(((Vector)vec_aseo.get(y)).get(2)+"");
   int blo2 = vec_bloque.indexOf(((Vector)vec_aseo.get(y)).get(3)+"");
   bloini = Integer.parseInt(vec_bloque.get(blo1)+"");
   blofin = Integer.parseInt(vec_bloque.get(blo2)+"");
   String texto_td    = "";
   int tipo_ocup = 0;
   nombre = "-- Aseo --";   // 1 - nombre
   estilo = "Estilo_Gris"; // alumnos
   for (int j=dia;j <= diafin;j++){
       v = new Vector();
       v.addElement(j+""); // 0 dia
       v.addElement(bloini+""); // 1 bl inicio
       v.addElement(blofin+""); // 2 bl fin
       v.addElement(nombre); // 3 reserva
       v.addElement(""); // 4 profesor
       v.addElement("0"); // 5 tipo reserva
       v.addElement(estilo); // 6 estilo
       v.addElement("#999999"); // 7 bg color
       v.addElement(""); // 8 tipo reserva
       v.addElement(((Vector)vec_aseo.get(y)).get(4)+""); // 9 id
       v.addElement("0"); // 10 paralelo
       v.addElement("0"); // 11 cupo
       v.addElement("0"); // 12 vacante
       v.addElement(""); // 13 vec fechas reservas
       v.addElement("0"); // 14 estado
       vec_horario1.addElement(v);
   }
    vec_horario.addElement(v_hor);
  }
    general.getCerrarConexion();
%>
<html>
<head>
<title>Gimnasio - Reserva</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/extendida.js"></script>
<script src="js/layer.js"></script>
<script src="js/boton.js"></script>
<script src="js/horario.js"></script>
<script src="js/valida_login.js"></script>
<script src="js/centrar.js"></script>
<script language="JavaScript" src="js/trim.js"></script>
<script LANGUAGE="JavaScript" TYPE="text/javascript">
 <%if(soloconsulta){%>
  function noenter() {
    return !(window.event && window.event.keyCode == 13);
  }
  function enter(e){
    if (window.event) {
      if (window.event.keyCode == 13)
             validar(document.fdatos);

    }
    else {
      if (e.which == 13)
           validar(document.fdatos);
    }
  }
  function validar(form){

          if(trim(form.rut_aux.value)==""){
                          alert("Debe ingresar el RUT a registrar");
                          form.rut_aux.focus();
                          return false;
                  }
                   if (!valida_rut2(form.rut_aux, form.rutnum, form.dvrut, 'RUT')) {
                          form.rut_aux.focus();
                          return false;
                  }
                 // return true;
           form.action = "gim_reserva.jsp";
           form.target = "_self";
          form.submit();
	}
        <%}%>
function Mensaje(){
  <%if(error<0){%>alert ('<%=mensaje%>');<%}%>
}
function Info(t,puntero,opcion){
  var texto = "";

  if (opcion ==  1) texto = "<strong>BLOQUE RESERVADO</strong>";
  if (opcion ==  2) texto = "<strong>RESERVA COMPLETA</strong><br>"+
    "Este bloque se ecuentra <strong>sin vacantes</strong>."+
    "<br>Inscr&iacute;base en la lista de espera directamente<br>con el coordinador de sala.";
  CambiaPuntero(t,puntero);
  Tip(texto,'#fefdd6');
}
function InfoAct(t,act,prof,cupo,vac){
  var texto = "";
  if (act != '') texto = act + "<br>";
  if (prof != '') texto += "Prof. <b>"+ prof + "</b><br>";
  texto += "Cupo <b>"+ cupo + "</b> Vacantes <b>"+ vac +"</b>";
  Tip(texto,'#fefdd6');
}

function CargarPag(){

  if (<%=sala%> > 0 && document.fingresar.semana != null) {
    var sem = document.fingresar.semana.value;
    document.fingresar.fecha_inicio.value = sem.substring(0,10);
    document.fingresar.fecha_termino.value = sem.substring(11);
  }


  if (<%=sala%> > 0 && document.fingresar.fecha_inicio != null &&
    document.fingresar.fecha_inicio.value != ''){
  fec_ini = document.fingresar.fecha_inicio.value;
  m = fec_ini.substring(3,5);
  dd = fec_ini.substring(0,2);
  a = fec_ini.substring(6,10);
  f1 = (a + m + dd)/1;

  fec_ter = document.fingresar.fecha_termino.value;
  m = fec_ter.substring(3,5);
  dd = fec_ter.substring(0,2);
  a = fec_ter.substring(6,10);
  f2 = (a + m + dd)/1;
  if (f2 < f1) {
    alert ('La fecha de inicio debe ser menor a la de t�rmino.');
    return false;
  }
  }
   <%if(soloconsulta){%>
  document.fingresar.rut.value = <%=rutnum%>;
   <%} else {%>
      document.fingresar.rut.value = <%=rut_usuario%> ;
     <%}%>
  document.fingresar.action = "gim_reserva.jsp";
  document.fingresar.target = "_self";
  document.fingresar.submit();
}
<% if (!reservaInhabilitada) {%>
function EnviaDatos(){

  if (<%=sala%> > 0 && document.fingresar.semana != null) {
    var sem = document.fingresar.semana.value;
    document.fingresar.fecha_inicio.value = sem.substring(0,10);
    document.fingresar.fecha_termino.value = sem.substring(11);
  }

  cont = <%=vec_reserva.size()%> + reserva.length;

  if (<%=vec_reserva.size()%> == 2 || cont > 2){
    alert ('No puede reservar m�s de 2 bloques a la semana.');
    return false;
    }
  linea1 = '';
  linea2 = '';

  if (reserva.length == 0) {
    alert ('Seleccione un bloque para confirmar la reserva.');
    return false;
  }

  for(i=0;i<reserva.length;i++){
    if (i==0)
    linea1 = reserva[i];
    if (i==1)
    linea2 = reserva[i];
  }

  document.fgrabar.fecha_inicio.value = document.fingresar.fecha_inicio.value;
  document.fgrabar.fecha_termino.value = document.fingresar.fecha_termino.value;
  document.fgrabar.linea1.value = linea1;
  document.fgrabar.linea2.value = linea2;
  document.fgrabar.a�o.value = <%=anno%>;
  document.fgrabar.semestre.value = <%=semestre%>;
  document.fgrabar.periodo.value = '<%=id_per%>';
  document.fgrabar.submit();

}
function Elimina(f){
  var pregunta = "Ud. va a ELIMINAR el registro definitivamente.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  if (respuesta == true)
     f.submit();
  else return false;
}
function EliminaReserva(id){
  var pregunta = "Ud. va a ELIMINAR la reserva definitivamente.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  if (respuesta == true) {
    document.freserva_borrar.id_reg.value = id;
    document.freserva_borrar.submit();
  }
  else return false;
}
<%}%>
function AbrirVentana(f,a, listado) {
  var alto  = 550;
  var ancho = 700;
  f.target = "ventana";
  f.action = a;
  f.listado.value = listado;
  var par = 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=yes';
  var w = Centrar03(f.target,alto,ancho,par);
  w.focus();
  f.submit();
}
</script>
<style type="text/css">
<!--
body {
        margin-left: 0px;
        margin-top: 0px;
}
-->
</style></head>
<body onload="Mensaje();">
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr valign="bottom"><td height="30" colspan="3">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="51%"><font <%=(cod_perfil==28 || cod_perfil==2)?"class=\"Estilo_Verde\"":"class=\"Estilo_Naranjo\""%>>RESERVA</font>
  <% if (vec_periodo.size() > 0) {%>&nbsp;&nbsp;<font  class="Estilo_Gris">per&iacute;odo <%=vec_periodo.get(0)+" al "+vec_periodo.get(1)%></font><%}%></td>
    <td width="49%" align="right"><a HREF="#" onClick="AbrirVentana(document.flistado,'gim_genera_list.jsp',6)" class="AElim"><img src="imagen/mas_info.gif" width="10" height="10" border="0" align="absmiddle"> Condiciones de Uso</a>&nbsp;&nbsp;
    <%if (sala==10335){%>
    <a HREF="#" onClick="AbrirVentana(document.flistado,'gim_genera_list.jsp',8)" class="AElim"> <img src="imagen/mas_info.gif" width="10" height="10" border="0" align="absmiddle"> Reglamento</a>
    <%} if (sala==10336) {%>
    <a HREF="#" onClick="AbrirVentana(document.flistado,'gim_genera_list.jsp',9)" class="AElim"> <img src="imagen/mas_info.gif" width="10" height="10" border="0" align="absmiddle"> Reglamento</a>
  <%}%></td>
  </tr>
</table>

  </td>
 </tr>
  <tr>
    <td width="60%" valign="top">
    <table width="96%"  border="0" cellspacing="0" cellpadding="3">
    <%if(soloconsulta ){%>
  <form name="fdatos" method="post" action="" target="">
      <tr>
        <td><span class="Estilo_Gris">RUT</span></td>
        <td><input type="text" name="rut_aux" size="12" maxlength="12" value ="<%=rut_aux%>" onKeyPress="enter(event,1);return noenter()" onChange="checkRutField(document.fingresar.rut_aux.value, document.fingresar);" class="Input01">
            <input type="hidden" name="rutnum" value="<%=rutnum%>">
            <input type="hidden" name="dvrut" value="<%=dvrut%>">
            <a href="#"  onClick="validar(document.fdatos);" class="AIngr"><img src="imagen/gim_lupa.gif" width="16" height="18" border="0" align="absmiddle"> Buscar</a> </td>
      </tr>
      </form>
      <%}%>
      <%if (!soloconsulta ||
            (soloconsulta && rutnum > 0)){%>
    <form name="fingresar" method="post" action="">
       <tr bgcolor="#FFFFFF">
       <input type="hidden" name="rut" value="">
        <input type="hidden" name="rutnum" value="<%=rutnum%>">
         <input type="hidden" name="rut_aux" value="<%=rut_aux%>">

          <td width="15%"><span class="Estilo_Gris">Sala</span>&nbsp;</td>
          <td width="85%"><select name="sala" class="Option" onchange = "CargarPag();">
            <option value="0">-- Seleccione --</option>
            <option value="10335" <%=(sala == 10335)?"selected":""%>>M&aacute;quinas</option>
            <option value="10336" <%=(sala == 10336)?"selected":""%>>Multiuso</option>
            </select></td>

        <%
        if (sala > 0){%>
        <tr bgcolor="#FFFFFF">
          <td><span class="Estilo_Gris">Semana</span>&nbsp; </td>
          <td>
          <%if (vec_fecha_inicio.size() > 0){%>
          <select name="semana" class="Option" onchange="CargarPag();">
           <%
           boolean pasa = false;
           for (int i=0; i < vec_fecha_inicio.size();i++){
             if (semana_act1.equals(vec_fecha_inicio.get(i)) &&
                 semana_act2.equals(vec_fecha_termino.get(i))) pasa = true;
             if (pasa) {
            %>
            <option value="<%=vec_fecha_inicio.get(i)+"-"+vec_fecha_termino.get(i)%>"
            <%=(vec_fecha_inicio.get(i)+"").trim().equals(fecha_inicio.trim()) &&
               (vec_fecha_termino.get(i)+"").trim().equals(fecha_termino.trim())?"selected":""%>>
            <%=(i+1) + ".- " + vec_fecha_inicio.get(i)+" al " +vec_fecha_termino.get(i)%></option>
           <%}}%>
          </select>
          <%} else {%><span class="Estilo_Rojo">No hay per�odo vigente</span><%}%>
          <input type="hidden" name="fecha_inicio" value="">
          <input type="hidden" name="fecha_termino" value="">
          </td>
        </tr>
        <%}%>
    </form>
    <%} %>
    <%if (reservaInhabilitada) {
        if (!texto_inhabilitado.trim().equals("") && sala > 0){%>
      <tr>
        <td valign="top"><span class="Estilo_Gris">Estado</span></td>
        <td valign="top"><span class="Estilo_Rojo"><%=texto_inhabilitado%></span></td>
      </tr>
      <%}
        if(!texto_inhabilitado2.trim().equals("") && sala > 0){%>
      <tr>
        <td valign="top"><span class="Estilo_Gris">Inscripci�n</span></td>
        <td valign="top"><span class="Estilo_Rojo"><%=texto_inhabilitado2%></span></td>
      </tr>
    <%}}%>
    </table>
 </td>
 <td width="3%" rowspan="2" valign="top" nowrap>&nbsp;</td>
 <td width="37%" rowspan="2" valign="top">
   <% if (soloconsulta || vec_reserva.size() > 0) {%>
   <table width="100%"  border="0" cellpadding="1" cellspacing="0">
     <tr>
       <td valign="top" >
         <table width="100%"  border="0" cellspacing="1" cellpadding="1">
         <%if(soloconsulta && alu_nombre.length() > 0){%>
         <tr><td>
         <table width="100%"  border="0" cellspacing="0" cellpadding="1">
         <tr>
         <td valign="top" bgcolor="#FFFFFF" >         <span class="Estilo_Azul"><%=rut_aux%>
         <a href="javascript:void(0)" onClick="AbrirVentana(document.form_masdatos,'gim_genera_list.jsp')">         </a></span></td>
         </tr>
         <tr><td valign="top" bgcolor="#FFFFFF" ><span class="Estilo_Azul"><%=alu_nombre%></span></td></tr>
         <tr><td valign="top" bgcolor="#FFFFFF" ><%=alu_situacion%></td></tr>
         <tr><td valign="top" bgcolor="#FFFFFF" ><%=alu_calidad%> <%=alu_rama%></td></tr>
         <form name="form_masdatos" method="post">
         <input type="hidden" name="listado" value="1">
         <input type="hidden" name="rut" value="<%=rutnum%>">
         </form>
         </table>
         </td></tr>
         <%}%>
         <% if (vec_reserva.size() > 0) {%>
         <tr><td bgcolor="#CCCCCC">
         <table width="100%"  border="0" cellspacing="0" cellpadding="2">
           <tr align="center">
             <td colspan="5" class="Estilo_Azul">
             <span class="Estilo_Naranjo">Bloques reservados</span><br>
                 <%=fecha_inicio%> al <%=fecha_termino%></td>
           </tr>
           <%
                for(int i=0; i < vec_reserva.size();i++){
                  int ind_ini = vec_bloque.indexOf(((Vector)vec_reserva.get(i)).get(1)+"") + 1;
                  int ind_fin = vec_bloque.indexOf(((Vector)vec_reserva.get(i)).get(2)+"") + 1;
                %>
           <tr>
             <td bgcolor="#FFFFFF">&nbsp;</td>
             <td bgcolor="#FFFFFF"><%=((Vector)vec_reserva.get(i)).get(4)+""%></td>
             <td bgcolor="#FFFFFF"><%=ind_ini%> - <%=ind_fin%></td>
             <% if (!reservaInhabilitada) {%>
             <td align="center" bgcolor="#FFFFFF">
             <a href="#" onClick="EliminaReserva(<%=((Vector)vec_reserva.get(i)).get(0)+""%>);">
             <img src="imagen/gim_eliminar2.gif" alt="ELIMINAR RESERVA" width="12" height="13" border="0" align="absmiddle"></a>
             </td><%}%>
             <td bgcolor="#FFFFFF">&nbsp;</td>
             <%}%>
           </tr>
         </table>
         </td></tr>
         <form name="freserva_borrar" method="post" action="gim_reserva.jsp" target="iframe">
           <input type="hidden" name="rut" value="<%=rut%>">
           <input type="hidden" name="rutnum" value="<%=rutnum%>">
           <input type="hidden" name="rut_aux" value="<%=rut_aux%>">
           <input type="hidden" name="sala" value="<%=sala%>">
           <input type="hidden" name="fecha_inicio" value="<%=fecha_inicio%>">
           <input type="hidden" name="fecha_termino" value="<%=fecha_termino%>">
           <input type="hidden" name="id_reg" value="">
           <input type="hidden" name="accion" value="2">
         </form>
         <%}%>
         </table>
       </td>
     </tr>
   </table>
   <%} else {%>
   &nbsp;
   <%}%></td>
 <%if (sala > 0){%>
  </tr>
  <% if (!reservaInhabilitada ) {
       if ( vec_fecha_inicio.size() > 0) {%>
  <tr>
    <td valign="top" height="35" class="Estilo_Gris"><div align="justify">
    Seleccione un m&aacute;ximo de <strong>dos bloques</strong> a la semana.
    Para seleccionar un bloque, pulse la celda y confirme la reserva. </div></td>
  </tr>
<%}} if ( vec_fecha_inicio.size() > 0) { %>
  <tr>
    <td colspan="3" valign="top"><img src="imagen/spacer.gif" width="1" height="3"></td>
  </tr>
  <%}%>
  <tr>
    <td colspan="3" valign="top" bgcolor="#CCCCCC">
  <%if (vec_fecha_inicio.size() > 0){%>
  <table width="100%"  border="0" cellspacing="1" cellpadding="0">
    <tr align="center" bgcolor="#CCCCCC">
      <td width="16%" colspan="2" height="16" background="imagen/boton_baseFONDO05.gif"><span class="Estilo_Azul">Bloques</span></td>
      <td width="14%" background="imagen/boton_baseFONDO05.gif"><span class="Estilo_Azul">Lunes</span></td>
      <td width="14%" background="imagen/boton_baseFONDO05.gif"><span class="Estilo_Azul">Martes</span></td>
      <td width="14%" background="imagen/boton_baseFONDO05.gif"><span class="Estilo_Azul">Mi&eacute;rcoles</span></td>
      <td width="14%" background="imagen/boton_baseFONDO05.gif"><span class="Estilo_Azul">Jueves</span></td>
      <td width="14%" background="imagen/boton_baseFONDO05.gif"><span class="Estilo_Azul">Viernes</span></td>
      <td width="14%" background="imagen/boton_baseFONDO05.gif"><span class="Estilo_Azul">S&aacute;bado</span></td>
    </tr>
    <% Vector vec_hordia  = new Vector();
       Vector vec_rowspan = new Vector();
         for (int i = 0; i < vec_horabloque1.size(); i++) {
             v = new Vector();
             v = (Vector) vec_horabloque1.get(i);
            int cod_bloque = Integer.parseInt(v.get(0).toString());
            int ind_cod_bloque = vec_bloque.indexOf(v.get(0).toString()) + 1;

       %>
        <tr align="center" bgcolor="#FFFFFF">
          <td bgcolor="#E9E9E9" class="letra7 Estilo_Azulino"><%=i+1%></td>
          <td bgcolor="#E9E9E9" class="letra7 Estilo_Azul"><%=v.get(1)%> - <%=v.get(2)%></td>
          <%

          /*
          0 d�a
          1 bloque inicio
          2 bloque fin
          3 nom.tipo reserva
          4 profesor
          5 tipo reserva
          6 color texto
          7 bg color
          8 tipo reserva
          9 cod_asign / id aseo
          10 paralelo
          11 cupo
          12 vacante
          13 fechas reservas

          */

          for (int d = 1; d < 7; d++) {
            boolean pasa = false;
            String td   = "";
            cod_reserva = 0;
            String clic = "";

            for (int j = 0; j < vec_reserva.size(); j++) { // reserva_personal
              Vector r = (Vector) vec_reserva.get(j);
              String tipo = r.get(8).toString();
              estilo = "Estilo_Blanco";
              clic = "";
              if (sala == 10336) tipo = r.get(7) + "<BR>" + tipo;
              int dia = Integer.parseInt(r.get(3).toString());
              int bi  = Integer.parseInt(r.get(1).toString());
              int bt  = Integer.parseInt(r.get(2).toString());
              int ind_bi = vec_bloque.indexOf(r.get(1).toString())+1;
              int ind_bt = vec_bloque.indexOf(r.get(2).toString())+1;

                if (d == dia &&
                 ind_cod_bloque >= ind_bi  &&
                 ind_cod_bloque <= ind_bt  &&
                  !vec_hordia.contains(dia+"-"+bi+"-"+bt)) {
                td   = "";
                pasa = true;
                vec_hordia.addElement(dia+"-"+bi+"-"+bt);
                // guarda los bloques reales con rowspan
                for (int b = ind_bi; b <= ind_bt; b++) {
                  if (!vec_rowspan.contains(d+"-"+b))
                    vec_rowspan.addElement(d+"-"+b);
                }
                String rowspan = "";
                clic = "bgcolor=\"#FF9900\" onmouseover=\"Info(this,'help',1)\""+
                       " onMouseout=\"EscondeTip()\"";


                int dif = (ind_bt - ind_bi) + 1;
                if (dif > 1) rowspan = "rowspan=\""+dif+"\"";


                td = "<td "+rowspan+" id=\"dia_"+dia+"_"+i+"\""+
                     clic +
                     " class=\""+estilo+"\">"+tipo+"</td>";

              }


            }

            int estado_clase = 0;
            for (int j = 0; j < vec_horario1.size(); j++) { // reserva horario
              clic = "";
              cupo = 0;
              vacante = 0;
              Vector h = (Vector) vec_horario1.get(j);
              int dia = Integer.parseInt(h.get(0).toString());
              int bi  = Integer.parseInt(h.get(1).toString());
              int bt  = Integer.parseInt(h.get(2).toString());
              int ind_bi = vec_bloque.indexOf(h.get(1).toString())+1;
              int ind_bt = vec_bloque.indexOf(h.get(2).toString())+1;
              cod_reserva = Integer.parseInt(h.get(5).toString());
              if (d == dia &&
                  ind_cod_bloque >= ind_bi  &&
                  ind_cod_bloque <= ind_bt  &&
                  !vec_hordia.contains(dia+"-"+bi+"-"+bt)) {
                td   = "";
                pasa = true;
                vec_hordia.addElement(dia+"-"+bi+"-"+bt);
                // guarda los bloques reales con rowspan
                for (int b = ind_bi; b <= ind_bt; b++) {
                  if (!vec_rowspan.contains(d+"-"+b))
                    vec_rowspan.addElement(d+"-"+b);
                }

                String prof   = h.get(4).toString();
                estilo        = h.get(6).toString();
                String tipo   = h.get(3).toString();
                vacante       = Integer.parseInt(h.get(12).toString());
                estado_clase  = Integer.parseInt(h.get(14).toString());
                cupo          = Integer.parseInt(h.get(11).toString());
                String asign  = h.get(9).toString();
                String par    = h.get(10).toString();
                String text   = asign+"@"+par+"@"+dia+"@"+h.get(1).toString()+"@"+h.get(2).toString();
               clic   = "";

                if (sala == 10336)
                     tipo += "<BR>"+ h.get(8).toString();

                if (estado_clase > 0) {
                  String nom_estado_clase = "";
                  if (estado_clase == 1) nom_estado_clase = "CERRADA";
                  else if (estado_clase == 2 || estado_clase == 4) nom_estado_clase = "ANULADA";
                  else if (estado_clase == 3 || estado_clase == 5) nom_estado_clase = "SUSPENDIDA";
                  tipo += "<br><font class='letra7 Estilo_Rojo'><b>" + nom_estado_clase + "</b></font>";
                }
                String rowspan = "";
                if (cod_reserva == 1 ){
                  if (vacante== 0) {
                    clic = " bgcolor=\"#AEC9E3\" onmouseover=\"Info(this,'help',2)\""+
                           " onMouseout=\"EscondeTip();\"";
                  }
                  else {
                    if (estado_clase > 0) {
                      clic = "";
                    }
                    else {
                      clic = "onClick=\"MarcaCelda(this,'"+text+"')\" " +
                             " onMouseOver=\"CursorOn(this,'Reserve','"+text+"');" +
                             " InfoAct(this,'','"+prof+"',"+cupo+","+vacante+");\" onMouseOut=\"CursorOff(this,'"+text+"');"+
                             " EscondeTip();\"" ;
                    }
                  }
                }
                int dif = (ind_bt - ind_bi) + 1;
                if (dif > 1) rowspan = "rowspan=\""+dif+"\"";


                    td = "<td "+rowspan+" id=\"dia_"+dia+"_"+i+"\""+
                         clic +
                         " class=\""+estilo+"\">"+tipo+"</td>";

              }
            }


            if (!pasa && !vec_rowspan.contains(d+"-"+ind_cod_bloque)) {
              td = "<td bgcolor=\"#E9E9E9\" >&nbsp;</td>";
            }
        %>
          <%=td%>
        <%}%>
        </tr>
        <%}%>
  </table>
  <%}%>
    </td>
  </tr>
  <tr valign="bottom" bgcolor="#FFFFFF">
    <td height="25">
    <% if (!reservaInhabilitada && vec_fecha_inicio.size() > 0) {%>
     <table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseOK.gif">
      <tr onClick="EnviaDatos();" onMouseOver="BotonOver(this,'Confirmar')" onMouseOut="BotonOut(this)">
        <td width="24">&nbsp;</td>
        <td width="67" class="Estilo_Azulino">Confirmar</td>
      </tr>
    </table><%}%>
    </td>
    <%if (soloconsulta){%>
    <td height="25">&nbsp;</td>
    <td height="25" align="right"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseUNDO.gif">
      <tr onMouseOver="BotonOver(this,'Volver')" onMouseOut="BotonOut(this)">
        <td width="24">&nbsp;</td>
        <td width="30"><a href="gim_reserva.jsp" target="_self"><font  class="Estilo_Azulino">Volver</font></a></td>
      </tr>
    </table></td>
    <%}}%>
  </tr>
</table>

<form name="flistado" method="post" action="" target="">
<input type="hidden" name="listado" value="">
</form>

<form name="fgrabar" method="post" action="gim_reserva.jsp" target="iframe">
 <input type="hidden" name="rut" value="<%=rut%>">
 <input type="hidden" name="rutnum" value="<%=rutnum%>">
 <input type="hidden" name="rut_aux" value="<%=rut_aux%>">
 <input type="hidden" name="sala" value="<%=sala%>">
 <input type="hidden" name="fecha_inicio" value="">
 <input type="hidden" name="fecha_termino" value="">
 <input type="hidden" name="linea1" value="">
 <input type="hidden" name="linea2" value="">
 <input type="hidden" name="a�o" value="">
 <input type="hidden" name="semestre" value="">
 <input type="hidden" name="periodo" value="">
 <input type="hidden" name="accion" value="1">
</form>
<div id="dhtmltooltip"></div>
<script language="JavaScript" type="text/javascript" src="js/box.js"></script>

</body>
</html>
