<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 16/04/2008
   // �ltima Actualizaci�n :
   // Listado.               Se listan los alumnos premiados que no se pudieron realizar sus reservas para el siguiente per�odo,
   //                        por ya tener reservas para el per�odo.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>


<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
   int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  java.util.Vector vec = (session.getAttribute("vec_listado") != null)?
                         (Vector) session.getAttribute("vec_listado"): new java.util.Vector();
   String titulo = "Listado de alumnos que no fueron realizadas sus reservas en forma autom�tica.";
session.removeAttribute("vec_listado");
java.util.Calendar f = new java.util.GregorianCalendar();
int dia = f.get(java.util.Calendar.DAY_OF_MONTH);
int mes = f.get(java.util.Calendar.MONTH) + 1;
int anno = f.get(java.util.Calendar.YEAR);
int hora = f.get(java.util.Calendar.HOUR_OF_DAY);
int minuto = f.get(java.util.Calendar.MINUTE);


String  dn       = "AM";
if (hora>12){
  dn    = "PM";
  hora = hora - 12;
}
  if (hora == 0) hora = 12;
%>
<html>
<head>
<title>Gimnasio - Lista de Premiados sin reserva</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #000000}
-->
</style>

</head>
<body >

<table width="100%"  border="0" cellspacing="0" cellpadding="1">
<%if ((vec != null && vec.size()>0) /*||
      (vec_vi�a != null && vec_vi�a.size()>0)*/){%>
  <tr>
    <td height="20" ><table width="100%"  border="0">
      <tr>
        <td width="20%" height="50" class="letra8" >Gimnasio III<br>
          USM </td>
        <td width="65%" class="letra10" align="center"><%=titulo%><span class="letra8"><br>
          </span></td>
        <td width="15%" >&nbsp;
          <div align="right"><span class="letra8"><%=dia%>/<%=mes%>/<%=anno%><br>
            <%=hora%>:<%=minuto%>  <%=dn%></span></div></td>
      </tr>
    </table>      </td>
  </tr>
  <tr>
    <td width="100%" height="20" align="center" valign="top" class="letra8">&nbsp;</td>
  </tr>
  <tr>
    <td height="20" align="center" valign="top" class="letra10">&nbsp;</td>
  </tr>
</table>
<table width="100%" height="49%"  border="1" cellpadding="2" cellspacing="1">
  <tr bgcolor="#FFFFFF">
    <td width="7%" class="Estilo_GrisBold">N&deg;</td>
    <td width="16%" class="Estilo_GrisBold">RUT</td>
    <td width="17%" class="Estilo_GrisBold">Paterno</td>
    <td width="18%" class="Estilo_GrisBold">Materno</td>
    <td  class="Estilo_GrisBold">Nombres</td>

  </tr>
  <%for (int i=0;vec != null && i < vec.size(); i++){%>
   <tr bgcolor="#FFFFFF">
    <td class="Estilo_Azul"><%=i+1%></td>
    <td class="Estilo_Azul"><%=((Vector) vec.get(i)).get(0)+ ""%></td>
    <td class="Estilo_Azul"><%=((Vector) vec.get(i)).get(2)+ ""%></td>
    <td class="Estilo_Azul"><%=((Vector) vec.get(i)).get(3)+ ""%></td>
    <td width="14%" class="Estilo_Azul"><%=((Vector) vec.get(i)).get(1)+ ""%></td>
     </tr>
   <%}}%>
 </table>
<div align="center"><font class="barra" color="#000000"><br>
  <br>
  &copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a</font>
</div>
</body>
</html>
