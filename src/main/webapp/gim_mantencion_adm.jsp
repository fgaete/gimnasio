<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

  <%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>
<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 18/07/2007
   // �ltima Actualizaci�n :
%>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>

<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
 int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");

  int   activacion =  request.getParameter("activacion")!=null &&
                    !request.getParameter("activacion").equals("null") &&
                    !request.getParameter("activacion").equals("NULL") &&
                    !request.getParameter("activacion").equals("")  &&
                    !request.getParameter("activacion").equals(" ")
                    ?Integer.parseInt(request.getParameter("activacion")+""):-1;
  int error = 0;
  String mensaje = "";
  general.getAbrirConexion();
  general.setGenerarConexionRegistro();
  if (activacion > -1){
     error = general.setActivacion("G",activacion);
     if (error == 0 ) mensaje = (activacion == 0)?"Inhabilitaci�n registrada satisfactoriamente":"Habilitaci�n registrada satisfactoriamente";
     else mensaje = "Problemas al registrar";
     error = -1;
  }

  activacion = general.getActivacion();
  general.getCerrarConexion();


%>
<html>
<head>
<title>Gimnasio - Mantenci&oacute;n</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script src="js/centrar.js"></script>
<script language = "javascript">
function Mensaje(){
 <%if(error<0){%>alert ('<%=mensaje%>');<%}%>
}
function Grabar(a) {
  if (a==1){
    var agrega = confirm("�Est� seguro que desea Habilitar?");
    if(!agrega) return false;
  } else {
    var agrega = confirm("�Est� seguro que desea DesHabilitar?");
    if(!agrega) return false;
  }

  document.fingresar.activacion.value = a;
  document.fingresar.submit();
}
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style></head>

<body onload="Mensaje();">
<form name="form" method="post">
<input type="hidden" name="listado" value="3">
</form>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%" height="37" class="Estilo_Verde">MANTENCI&Oacute;N</td>
  </tr>
</table>
<table width="100%" height="0%"  border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td width="22%" height="20" bgcolor="#FFFFFF">Esta opci&oacute;n le permite dejar temporalmente inhabilitado a los alumnos el acceso al Sistema  Gimnasio III.</td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">Estado actual del sistema: <%if (activacion == 1) {%><strong class="Estilo_Azulino">NORMAL</strong><%} else {%> <strong class="Estilo_Rojo">DESHABILITADO</strong><%}%>.</td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <%if (activacion == 1) {%>
  <tr>
    <td height="20" bgcolor="#FFFFFF">Presione el bot&oacute;n <em>&quot;Deshabilitar&quot;</em> para dejar el sistema en mantenci&oacute;n. </td>
  </tr>
  <%} else {%>
  <tr>
    <td height="20" bgcolor="#FFFFFF">Presione el bot&oacute;n <em>&quot;</em><em>Habilitar&quot;</em> para dejar el sistema en estado normal. </td>
  </tr>
  <%}%>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="0" cellpadding="3">
<%if (activacion == 0) {%>
  <tr>
    <td width="9%" height="35">
   <table height="21"  border="0" cellpadding="0" cellspacing="0">
        <tr onClick="Grabar(1);" onMouseOver="BotonOver(this,'Mantenci�n del sistema')" onMouseOut="BotonOut(this)">
          <td width="22"><img src="imagen/boton_baseMANT01.gif" width="22" height="21"></td>
          <td width="65" align="center" valign="middle" nowrap background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">Habilitar</td>
          <td width="3"><img src="imagen/boton_baseBORDEDER.gif"></td>
        </tr>
    </table>
  </td>
  </tr>
  <%} else {%>
  <tr>
    <td width="9%" height="35">
   <table height="21"  border="0" cellpadding="0" cellspacing="0">
        <tr onClick="Grabar(0);" onMouseOver="BotonOver(this,'Mantenci�n del sistema')" onMouseOut="BotonOut(this)">
          <td width="22"><img src="imagen/boton_baseCANCEL01.gif" width="22" height="21"></td>
          <td width="80" align="center" valign="middle" nowrap background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">Deshabilitar</td>
          <td width="3"><img src="imagen/boton_baseBORDEDER.gif"></td>
        </tr>
    </table>
  </td>
  </tr>
  <%}%>
</table>
   <form name="fingresar" method="post" action="gim_mantencion_adm.jsp">
   <input type="hidden" name="activacion" value="">
  </form>
</body>
</html>
