<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 08/06/2007
   // �ltima Actualizaci�n :
   // Bloque. se registran y mantienen todos los bloques de la sala de m�quinas
   // y la sala de aer�bica.
%>
<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");

  int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  int cod_perfil = session.getAttribute("cod_perfil") != null?
                  Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
  String   hora_i =  request.getParameter("hora_i")!=null &&
                        !request.getParameter("hora_i").equals("null") &&
                        !request.getParameter("hora_i").equals("NULL") &&
                        !request.getParameter("hora_i").equals("")  &&
                        !request.getParameter("hora_i").equals(" ")
                        ?request.getParameter("hora_i")+"":"";
  String   hora_t =  request.getParameter("hora_t")!=null &&
                        !request.getParameter("hora_t").equals("null") &&
                        !request.getParameter("hora_t").equals("NULL") &&
                        !request.getParameter("hora_t").equals("")  &&
                        !request.getParameter("hora_t").equals(" ")
                        ?request.getParameter("hora_t")+"":"";
  String   min_i =  request.getParameter("min_i")!=null &&
                      !request.getParameter("min_i").equals("null") &&
                      !request.getParameter("min_i").equals("NULL") &&
                      !request.getParameter("min_i").equals("")  &&
                      !request.getParameter("min_i").equals(" ")
                        ?request.getParameter("min_i")+"":"";
  String   min_t =  request.getParameter("min_t")!=null &&
                         !request.getParameter("min_t").equals("null") &&
                         !request.getParameter("min_t").equals("NULL") &&
                         !request.getParameter("min_t").equals("")  &&
                         !request.getParameter("min_t").equals(" ")
                         ?request.getParameter("min_t")+"":"";
  int   accion =  request.getParameter("accion")!=null &&
                      !request.getParameter("accion").equals("null") &&
                      !request.getParameter("accion").equals("NULL") &&
                      !request.getParameter("accion").equals("")  &&
                      !request.getParameter("accion").equals(" ")
                          ?Integer.parseInt(request.getParameter("accion")+""):0;
  int   cod_bloque =  request.getParameter("cod_bloque")!=null &&
                    !request.getParameter("cod_bloque").equals("null") &&
                    !request.getParameter("cod_bloque").equals("NULL") &&
                    !request.getParameter("cod_bloque").equals("")  &&
                    !request.getParameter("cod_bloque").equals(" ")
                    ?Integer.parseInt(request.getParameter("cod_bloque")+""):0;
  int   sala =  request.getParameter("sala")!=null &&
                    !request.getParameter("sala").equals("null") &&
                    !request.getParameter("sala").equals("NULL") &&
                    !request.getParameter("sala").equals("")  &&
                    !request.getParameter("sala").equals(" ")
                    ?Integer.parseInt(request.getParameter("sala")+""):0;
  int   secuencia =  request.getParameter("secuencia")!=null &&
                  !request.getParameter("secuencia").equals("null") &&
                  !request.getParameter("secuencia").equals("NULL") &&
                  !request.getParameter("secuencia").equals("")  &&
                  !request.getParameter("secuencia").equals(" ")
                    ?Integer.parseInt(request.getParameter("secuencia")+""):0;
  String   id =  request.getParameter("id")!=null &&
                         !request.getParameter("id").equals("null") &&
                         !request.getParameter("id").equals("NULL") &&
                         !request.getParameter("id").equals("")  &&
                         !request.getParameter("id").equals(" ")
                         ?request.getParameter("id")+"":"";
  int   anno =  request.getParameter("anno")!=null &&
                   !request.getParameter("anno").equals("null") &&
                   !request.getParameter("anno").equals("NULL") &&
                   !request.getParameter("anno").equals("")  &&
                   !request.getParameter("anno").equals(" ")
                   ?Integer.parseInt(request.getParameter("anno")+""):0;
  int   semestre =  request.getParameter("semestre")!=null &&
                   !request.getParameter("semestre").equals("null") &&
                   !request.getParameter("semestre").equals("NULL") &&
                   !request.getParameter("semestre").equals("")  &&
                   !request.getParameter("semestre").equals(" ")
                   ?Integer.parseInt(request.getParameter("semestre")+""):0;
   java.util.Calendar f = new java.util.GregorianCalendar();
  int a�o = f.get(java.util.Calendar.YEAR);
  if (anno == 0) anno = a�o;
if (rut_usuario == 0)  response.sendRedirect("index.jsp");
 boolean soloconsulta = false;
 if (cod_perfil==28) soloconsulta = true;
 Vector vec = new Vector();
 String mensaje = "";
 int error = 0;
 general.getAbrirConexion();
 general.setGenerarConexionHorario();
 if (accion > 0){
   String hora_inicio = hora_i + ":" + min_i;
   String hora_termino = hora_t + ":" + min_t;

 switch (accion){
       case 1: { // agregar
            error = general.setInsertarHoraBloque(1,1, hora_inicio, hora_termino, rut_usuario, sala, secuencia, anno, semestre);
            if(error == 0) mensaje = "Se registr� satisfactoriamente.";
            else if (error == -5) mensaje = "Este n�mero de bloque ya se encuentra registrado. Debe utilizar uno distinto.";
              else mensaje = "Problemas al registrar. ";
            error = -1;
        break;
       }
     case 2: { //  copiar semestre anterior
        error = general.setCopiarHoraBloque(rut_usuario, sala, anno, semestre);
        if(error == 0) mensaje = "Se registr� satisfactoriamente.";
        else  if (error == -2) mensaje = "No se registra bloque de semestre anterior.";
              else mensaje = "Problemas al registrar. ";
        error = -1;
    break;
       }
       case 3: { // eliminar
       error = general.setEliminarHoraBloque(rut_usuario, id);
       mensaje = "Problemas al eliminar el bloque ";
       switch (error) {
         case 0 : mensaje = "Se Elimin� satisfactoriamente.";
           break;
         case -1 : mensaje += ", existe Aseo asociado.";
           break;
         case -2 : mensaje += ", existe Bloque asociado a una sala.";
           break;
         case -3 : mensaje = "Error al eliminar bloque.";
           break;
         case -4 : mensaje += ", existe Horario asociado.";
           break;
         case -5 : mensaje = "Error al eliminar bloque.";
           break;
         case -6 :  mensaje += "porque tiene una Actividad programada.";
           break;
          }
            error = -1;
         break;
       }
     case 4: { // eliminar el semestre
          error = general.setEliminarHoraBloqueSemestral(anno, semestre, sala);
          mensaje = "Problemas al eliminar el bloque ";
          switch (error) {
            case 0 : mensaje = "Se Elimin� satisfactoriamente.";
              break;
            case -1 : mensaje += ", existe Aseo asociado.";
              break;
            case -2 : mensaje += ", existe Horario asociado a la sala.";
              break;
            case -3 : mensaje = "Error al eliminar bloque.";
              break;
         }
       error = -1;
       break;
       }
       case 5: { // modificar num bloque
          error = general.setModificarHoraBloque(rut_usuario, id, secuencia);
          switch (error){
            case 0: mensaje = "Se modific� satisfactoriamente el bloque.";
              break;
            case -1:mensaje = "No se modific� el bloque, pues ya existe este bloque.";
              break;
            case -2:mensaje = "No se modific� el bloque, error al actualizar.";
              break;
            case -3:mensaje = "No se modific� el bloque, pues no se encuentra registrado.";
              break;
          }
       }
     }
 }
 int max_secuencia = general.getMaxSecuencia(sala, anno, semestre);
 vec = general.getHoraBloque(sala, anno, semestre);
 general.getCerrarConexion();
  %>
<html>
<head>
<title>Gimnasio - Bloque</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-Calendario -->
<LINK title=win2k-cold-1 media=all href="js/calendario/calendar-verde.css" type=text/css rel=stylesheet>
<SCRIPT src="js/calendario/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="js/calendario/calendar-es.js" type=text/javascript></SCRIPT>
<SCRIPT src="js/calendario/calendar-setup.js" type=text/javascript></SCRIPT>
<!-Estilos -->
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
      function Mensaje(){
<%if(error<0){%>alert ('<%=mensaje%>');<%}%>
}
  <%if(!soloconsulta){%>
    function IsNumeric(valor)
    {
    var log=valor.length; var sw="S";
    for (x=0; x<log; x++)
    { v1=valor.substr(x,1);
    v2 = parseInt(v1);
//Compruebo si es un valor num�rico
    if (isNaN(v2)) { sw= "N";}
    }
    if (sw=="S") {return true;} else {return false; }
    }

function Grabar(f, accion){
  if (f.sala.value == 0)
{
  alert ('Debe seleccionar la sala');
  f.sala.focus();
  return false;
  } 
/*YVM se agrego validacion con los minutos, ya que deberia en la misma hora pero con los minutos validados*/
  if(accion !== 2) {
  if ((f.hora_i.value > f.hora_t.value)||(f.hora_i.value == f.hora_t.value && f.min_i.value >= f.min_t.value))
  {
  	if (f.hora_i.value == f.hora_t.value){
  		alert ('La hora de t�rmino debe ser mayor que la de inicio');
  	}else {
    	alert ('La hora de inicio debe ser menor a la de t�rmino');
    }
    f.hora_i.focus();
    return false;
  }
  }
  /* se elimin� esta validaci�n pues agregando la secuencia en hora_bloque_sala
     no importa el orden de ingreso

    hora_termino = '<%=(vec.size()>0)?(((Vector) vec.get(vec.size()-1)).get(2)+ ""):"00:00"%>';
    hora = hora_termino.substr(0,2);
    min =  hora_termino.substr(3,2);
    if ((hora/1) == f.hora_i.value){
      if (f.min_i.value < (min/1))
      {
        alert ('La hora de inicio debe ser mayor a la de t�rmino del �ltimo bloque registrado ');
        f.hora_i.focus();
        return false;
      }

    } else {
      if ((hora/1) > f.hora_i.value){
        alert ('La hora de inicio debe ser mayor a la de t�rmino del �ltimo bloque registrado ');
        f.hora_i.focus();
        return false;
      }
    }
*/
  f.accion.value = accion;
  f.submit();
}

function Eliminar(f){
  var pregunta = "Ud. va a ELIMINAR el bloque definitivamente.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  f.accion.value = "3";
  if (respuesta == true) f.submit();
}
function Modificar(f){
  var pregunta = "Ud. va a Modificar el bloque definitivamente.\n� Desea continuar ?";
  var respuesta = confirm(pregunta);
  f.accion.value = "5";

  if (respuesta == true) f.submit();
}
<%}%>
function preCargaImg(imageURL) {
                image = new Image();
                image.src = imageURL;
                return image;
}


GrabarActivo = preCargaImg("imagen/gim_grabar.gif");

function BuscarHoraFin(t){
  var valor = t.value/1;
  for (var i=0; i < 23 && valor < 23; i++) {
    var opcion = (document.form_grabar.hora_t.options[i].value)/1;
    if (opcion == valor+1) {
      document.form_grabar.hora_t.options[i].selected = true;
      break;
    }
  }
  if (valor == 23) document.form_grabar.hora_t.options[23].selected = true; }

function BuscarMinutoFin(t){
  var valor = t.value/1;
  for (var i=0; i < 56 && valor < 56; i++) {
    var opcion = (document.form_grabar.min_t.options[i].value)/1;
    if (opcion == valor) {
      document.form_grabar.min_t.options[i].selected = true;
      break;
    }
  }
}
// -->
</SCRIPT>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style></head>

<body  onload="Mensaje();">
 <table width="100%"  border="0" cellspacing="0" cellpadding="1">
 <form name="form_grabar" method="post" action="gim_bloque_adm.jsp" target="_self">
  <tr>
    <td height="37" colspan="5" class="Estilo_Verde">BLOQUE DE HORA</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td bgcolor="#FFFFFF" class="Estilo_Gris">Semestre</td>
    <td colspan="4" bgcolor="#FFFFFF"> <span class="Estilo_Azul">
      <select name="anno" class="Option" id="anno" onchange="form_grabar.submit();">
        <%for(int i = 2006;i<=a�o;i++){%>
        <option value="<%=i%>" <%=(i==anno)?"selected":""%>><%=i%></option>
        <%}%>
      </select>
-
<select name="semestre" class="Option" id="semestre" onchange="form_grabar.submit();">
  <option value="1" <%=(semestre==1)?"selected":""%>>1</option>
  <option value="2" <%=(semestre==2)?"selected":""%>>2</option>
  <option value="3" <%=(semestre==3)?"selected":""%>>3</option>
</select>
</span></td>
  </tr>

  <tr bgcolor="#FFFFFF">
    <td width="11%" bgcolor="#FFFFFF" class="Estilo_Gris">Sala</td>
    <td colspan="4" bgcolor="#FFFFFF">    <span class="Estilo_Azul">
      <select name="sala" class="Option" id="sala" onchange="form_grabar.submit();">
        <option value="0" <%=(sala==0)?"selected":""%>>-- Seleccione --</option>
        <option value="10335" <%=(sala==10335)?"selected":""%>>M&aacute;quinas</option>
        <option value="10336" <%=(sala==10336)?"selected":""%>>Multiuso</option>
      </select>
    </span></td>

  </tr>
  <%if (sala > 0){%>
  <tr bgcolor="#FFFFFF">
    <td bgcolor="#FFFFFF" class="Estilo_Gris">Bloque</td>
    <td bgcolor="#FFFFFF"><select name="secuencia" id="secuencia" class="Option">
        <%for (int sec = 1;sec<=max_secuencia+1;sec++){ %>
        <option value="<%=sec%>" <%=(sec == max_secuencia+1)?"selected":""%>><%=sec%></option>
        <%}%>
      </select>
    </td>
    <td colspan="3" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <%if(!soloconsulta){%>
  <tr bgcolor="#FFFFFF">
    <td bgcolor="#FFFFFF" class="Estilo_Gris">Hora</td>
    <td width="18%" bgcolor="#FFFFFF">
	<%String hora = "";%>
	<select name="hora_i" id="hora_i" class="Option" onChange="BuscarHoraFin(this)">
      <% for (int hor = 0;hor<24;hor++){
	     if (hor<10) hora = "0" + hor;
		 else hora = hor + "";%>
	  <option value="<%=hora%>"><%=hora%></option>
	  <%}%>
    </select>
     : <select name="min_i" id="min_i" class="Option" onChange="BuscarMinutoFin(this)">
           <% int hor = 0;
		   while (hor < 60){
	        if (hor<10) hora = "0" + hor;
		    else hora = hor + "";%>
	  <option value="<%=hora%>"><%=hora%></option>
	  <% hor = hor + 5;}%></select>
      </td>
    <td colspan="3" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td bgcolor="#FFFFFF" class="Estilo_Gris">&nbsp;</td>
    <td bgcolor="#FFFFFF"><select name="hora_t" id="hora_t" class="Option">
      <% for ( hor = 0;hor<24;hor++){
	     if (hor<10) hora = "0" + hor;
		 else hora = hor + "";%>
      <option value="<%=hora%>"><%=hora%></option>
      <%}%>
    </select>
:
<select name="min_t" id="min_t" class="Option">
  <%hor = 0;
    while (hor < 60){
      if (hor<10) hora = "0" + hor;
      else hora = hor + "";%>
  <option value="<%=hora%>"><%=hora%></option>
  <% hor = hor + 5;}%>
</select></td>
    <td width="12%" bgcolor="#FFFFFF"><table width="91" height="21"  border="0" cellpadding="0" cellspacing="0" background="imagen/boton_baseOK.gif">
      <tr onClick="Grabar(form_grabar,1);" onMouseOver="BotonOver(this,'Agregar')" onMouseOut="BotonOut(this)">
        <td width="29">&nbsp;</td>
        <td width="62" class="Estilo_Azulino">Agregar</td>
      </tr>
    </table></td>
    <td width="33%" bgcolor="#FFFFFF">
    <%if (vec.size()==0){%>
    <table height="21"  border="0" cellpadding="0" cellspacing="0">
       <tr id="BTN_copiar" onClick="Grabar(form_grabar,2);" onMouseOver="BotonOver(this,'Imprimir')" onMouseOut="BotonOut(this)">
        <td width="21"><img src="imagen/boton_baseREG01.gif" width="21" height="21"></td>
        <td width="155" align="center" background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">Copiar semestre anterior </td>
        <td width="3" class="Estilo_Azulino"><img src="imagen/boton_baseBORDEDER.gif" width="3" height="21"></td>
      </tr>
    </table>
    <%}%>
</td>
    <td width="26%" align="right" bgcolor="#FFFFFF"><%if (vec.size()>0){%>
      <table height="21"  border="0" cellpadding="0" cellspacing="0">
        <tr id="BTN_copiar" onClick="Grabar(form_grabar,4);" onMouseOver="BotonOver(this,'Imprimir')" onMouseOut="BotonOut(this)">
          <td width="21"><img src="imagen/boton_baseUNDO01.gif" width="21" height="21"></td>
          <td width="120" align="center" background="imagen/boton_baseFONDO.gif" class="Estilo_Azulino">Eliminar semestre </td>
          <td width="3" class="Estilo_Azulino"><img src="imagen/boton_baseBORDEDER.gif" width="3" height="21"></td>
        </tr>
      </table>
	<%}%></td>
  </tr>
    <%}%>
  <%if(!soloconsulta){%>
  <input type="hidden" name="accion" value="">
  <%}%>
  </form>
<%}%>
  <tr bgcolor="#FFFFFF">
    <td bgcolor="#FFFFFF" class="Estilo_Gris">&nbsp;</td>
    <td colspan="4" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <%if (sala>0 && vec.size()> 0 ){%>
  <tr bgcolor="#CCCCCC">
    <td  colspan="5" class="Estilo_Gris">
     <table width="100%"  border="0" cellspacing="0" cellpadding="1">
        <tr bgcolor="#999999">
          <td width="100%"   class="Estilo_Gris">
            <table width="100%"  border="0" cellspacing="0" cellpadding="2">
              <tr bgcolor="#008040" class="Estilo_GrisOsc">
                <td height="13" background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Bloque</span></td>
                <td background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Hora</span></td>
                    <%if(!soloconsulta){%>
                <td align="center" background="imagen/boton_baseFONDO04.gif"><span class="Estilo_Blanco">Acci&oacute;n</span></td>
                    <%}%>
              </tr>
              <%for (int i=0;i < vec.size();i++){
                int blo_secuencia = 0;
                blo_secuencia = Integer.parseInt(((Vector) vec.get(i)).get(4)+ "");
               %>
               <tr bgcolor="#E8E8E8" onmouseover='this.style.background="#F4F4F4"' onmouseout='this.style.background="#E8E8E8"'>
                 <%if(!soloconsulta){%>
                <form name="form_grabar<%=i%>" method="post" action="gim_bloque_adm.jsp" target="_self">
                <%}%>
                 <td width="11%" height="13" class="Estilo_Azul">
               <%if(!soloconsulta){%>
                   <select name="secuencia" id="secuencia" class="Option" onChange="Modificar(document.form_grabar<%=i%>);">
                     <%for (int sec = 1;sec<=max_secuencia+1;sec++){ %>
                     <option value="<%=sec%>" <%=(sec == blo_secuencia)?"selected":""%>><%=sec%></option>
                     <%}%>
                   </select>

                 <input type="hidden" name="accion" value="">
                 <input type="hidden" name="sala" value="<%=sala%>">
                 <input type="hidden" name="cod_bloque" value="<%=((Vector) vec.get(i)).get(0)+ ""%>">
                 <input type="hidden" name="id" value="<%=((Vector) vec.get(i)).get(3)+ ""%>">
                 <input type="hidden" name="semestre" value="<%=semestre%>">
                <input type="hidden" name="anno" value="<%=anno%>">
               <%} else {%><%=blo_secuencia%><%}%>
               </td>
                <td width="68%" class="Estilo_Azul"><span id="act<%=i%>"><%=((Vector) vec.get(i)).get(1)+ ""%> - <%=((Vector) vec.get(i)).get(2)+ ""%></span></td>
                <%if(!soloconsulta){%><td width="12%" align="center">
                <span id="IMAG_ELIMINAR<%=i%>">
                <a href="javascript:void(0)" onClick="Eliminar(document.form_grabar<%=i%>)">
                <img src="imagen/gim_basurero.gif" alt="ELIMINAR" border="0" align="absmiddle"></a>
                </span></td><%}%>
                <%if(!soloconsulta){%>
                 </form>
                 <%}%>
              </tr>
              <%}%>
          </table></td>
        </tr>
      </table></td>
  </tr>
  <%} %>
</table>
 <%if (sala>0 && vec.size()> 0 ){%>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="30" valign="bottom"><%if(!soloconsulta){%><span class="Estilo_GrisOsc"><img src="imagen/gim_basurero.gif" alt="ELIMINAR" width="15" height="18" align="absmiddle"> Eliminar</span><%}%></td>
  </tr>
</table>
<%}%>
<br>
</body>
</html>