<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
// Fecha                : 25/07/2007
// �ltima Actualizaci�n :
// Datos Se muestra los datos del alumno .
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<jsp:useBean id="us" class="gimnasio.BeanUsuarios"></jsp:useBean>

<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;                 
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");
  int rut_alumno     = request.getParameter("rutnum")!=null &&
                       !request.getParameter("rutnum").equals("null") &&
                       !request.getParameter("rutnum").equals("NULL") &&
                       !request.getParameter("rutnum").equals("")  &&
                       !request.getParameter("rutnum").equals(" ")?
                       Integer.parseInt(request.getParameter("rutnum")+""):0;                   
  int cod_perfil = session.getAttribute("cod_perfil") != null?
                   Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
  String rut_f     = request.getParameter("rut_aux")!=null &&
                     !request.getParameter("rut_aux").equals("null") &&
                     !request.getParameter("rut_aux").equals("NULL") &&
                     !request.getParameter("rut_aux").equals("")  &&
                     !request.getParameter("rut_aux").equals(" ")?
                     request.getParameter("rut_aux")+"":"";

  String periodo     = request.getParameter("periodo")!=null &&
                     !request.getParameter("periodo").equals("null") &&
                     !request.getParameter("periodo").equals("NULL") &&
                     !request.getParameter("periodo").equals("")  &&
                     !request.getParameter("periodo").equals(" ")?
                     request.getParameter("periodo")+"":"";

  int cod_asign  = request.getParameter("cod_asign")!=null &&
                !request.getParameter("cod_asign").equals("null") &&
                !request.getParameter("cod_asign").equals("NULL") &&
                !request.getParameter("cod_asign").equals("")  &&
                !request.getParameter("cod_asign").equals(" ")?
                Integer.parseInt(request.getParameter("cod_asign")+""):0;
int paralelo  = request.getParameter("paralelo")!=null &&
               !request.getParameter("paralelo").equals("null") &&
               !request.getParameter("paralelo").equals("NULL") &&
               !request.getParameter("paralelo").equals("")  &&
               !request.getParameter("paralelo").equals(" ")?
                 Integer.parseInt(request.getParameter("paralelo")+""):0;
int a�o  = request.getParameter("a�o")!=null &&
                !request.getParameter("a�o").equals("null") &&
                !request.getParameter("a�o").equals("NULL") &&
                !request.getParameter("a�o").equals("")  &&
                !request.getParameter("a�o").equals(" ")?
                Integer.parseInt(request.getParameter("a�o")+""):0;
int semestre  = request.getParameter("semestre")!=null &&
               !request.getParameter("semestre").equals("null") &&
               !request.getParameter("semestre").equals("NULL") &&
               !request.getParameter("semestre").equals("")  &&
               !request.getParameter("semestre").equals(" ")?
                 Integer.parseInt(request.getParameter("semestre")+""):0;
  String texto="";
  String calidad = "";
  String rama = "";
  String rut = "";
  String nombre = "";
  String situacion = "";
  String texto_curso = "";
  Vector vec_curso = new Vector();
   int perfil = 0;

  if(rut_alumno > 0) {
  general.getAbrirConexion();
  general.setGenerarConexionDatosAl();
  general.setGenerarConexionRegistro();
  general.setGenerarConexionReserva();
  general.setGenerarConexionHorario();
  Vector vec =  new Vector();

  Vector vec_reg = new Vector();
  if (a�o > 0 && semestre > 0 && cod_asign > 0)
    vec_curso = general.getInscritoDocencia(a�o, semestre, cod_asign, rut_alumno);
  texto_curso = (vec_curso.size()>0)?(vec_curso.get(0)+", paralelo "+vec_curso.get(1)):"" ;
  if (!texto_curso.trim().equals(""))
    texto_curso = "<span class='Estilo_Azulino'>Cr�dito: " + texto_curso + "</span>";
  vec = general.getAlumno(rut_alumno);
  vec_reg = general.getRegistroUsuario(rut_alumno);
  if(vec.size() > 0 && vec_reg.size()>0 && ((vec_reg.get(1)+"").trim().equals("3") ||
                           (vec_reg.get(1)+"").trim().equals("6") )) // es de rama
  {

    Vector vec_rama = general.getAlumnoRama(rut_alumno);
    calidad = "Rama ";
    if (vec_rama.size()>0){
      rama = vec_rama.get(1)+"";
    } else calidad +=" - Vi�a";
    boolean premiado = general.getRegistroPremiadoSancionado(  periodo, vec_reg.get(4)+"", 5);
    if (premiado) texto = "<img src=\"imagen/gim_estrella.gif\" width=\"33\" height=\"10\" border=\"0\" align=\"absmiddle\">";
     premiado = general.getRegistroPremiadoSancionado(  periodo, vec_reg.get(4)+"", 6);
  if (premiado) texto = "<img src=\"imagen/gim_sancion.gif\" width=\"17\" height=\"18\" border=\"0\" align=\"absmiddle\">";
  }
  if (vec_reg.size() > 0 && (vec_reg.get(1)+"").trim().equals("2")) calidad = "Tutor�a";

  // buscar es de vi�a
  /*
  if ((vec_reg.size()== 0 && vec.size()==0) || // que no exista como alumno CC
      (vec.size() > 0 && vec.get(5).equals("")) || // que exista como alumno CC pero no tenga carrera vigente
      ((vec_reg.size()>0 && // que est� registrado como alumno de vi�a
      ((vec_reg.get(1)+"").trim().equals("5") ||
        (vec_reg.get(1)+"").trim().equals("6"))))){

    general.getAbrirConexionVi�a();
    general.setGenerarConexionDatosVi�a();
    vec = general.getAlumnoVi�a(rut_alumno);
    general.getCerrarConexionVi�a();
  }*/
  if (vec.size() > 0 &&
      ((vec_reg.size()>0 && ((vec_reg.get(1)+"").trim().equals("1") ||
                           (vec_reg.get(1)+"").trim().equals("2") ||
                           (vec_reg.get(1)+"").trim().equals("3") ||
                           (vec_reg.get(1)+"").trim().equals("5") ||
                           (vec_reg.get(1)+"").trim().equals("6")) )
         || (vec_reg.size()== 0 ))){
    rut = vec.get(12)+"";
    nombre = vec.get(1)+"";
    situacion = vec.get(5)+"";
    if (!(vec.get(7)+"").trim().equals("") && !(vec.get(7)+"").trim().equals("0")) situacion += " , �ltima matr�cula " + vec.get(7)+"";
    perfil = 8;
  }
  if (((vec_reg.size()>0 && ((vec_reg.get(1)+"").trim().equals("4") ||
                          (vec_reg.get(1)+"").trim().equals("7") ) )
         || (vec_reg.size()== 0 && vec.size()==0))){ // ver si es funcionario
    if ((vec_reg.size()>0 && (vec_reg.get(1)+"").trim().equals("4") ) ||
        (vec_reg.size()== 0 && vec.size()==0)){
      us.getAbrirConexion();
      us.setGenerarConexionUsuario();
      vec = us.getFuncionario(rut_alumno);
      us.getCerrarConexion();
     }
     /*if ((vec_reg.size()>0 && (vec_reg.get(1)+"").trim().equals("7") ) ||
        (vec_reg.size()== 0 && vec.size()==0)){  // es de vi�a
      general.getAbrirConexionVi�a();
      general.setGenerarConexionDatosVi�a();
      vec = general.getFuncionarioVi�a(rut_alumno);
      general.getCerrarConexionVi�a();

    }*/
    if(vec.size() > 0){
      nombre = vec.get(2)+" "+ vec.get(3)+" "+ vec.get(4);
      situacion = vec.get(18)+"";
      if(!(vec.get(12)+"").trim().equals(""))
          situacion += ", " +vec.get(12)+"";
      calidad = vec.get(14) + ", "+ vec.get(20);
    }
    perfil = 19;
  }
    general.getCerrarConexion();
  }

%>
<html>
<head>
<title>Gimnasio - Datos de Asistente</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <!-Estilos -->
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/centrar.js"></script>
<script LANGUAGE="JavaScript" TYPE="text/javascript">
  function AbrirVentana(f,a) {
  var alto  = 550;
  var ancho = 700;
  f.target = "ventana";
  f.action = a;
  var par = 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=yes';
  var w = Centrar03(f.target,alto,ancho,par);
  w.focus();
  f.submit();
}
</script>
<style type="text/css">
<!--
body {
        margin-left: 0px;
        margin-top: 0px;
}
-->
</style></head>

<body>
<% if (rut_alumno > 0) {
  if (nombre.trim().equals("")) nombre = "<span class=\"Estilo_Rojo\">Este RUT no se encuentra registrado.</span>";
%>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr>
 <td valign="top" bgcolor="#FFFFFF" >
</tr>
<tr>
  <td valign="top" bgcolor="#FFFFFF" ><span class="Estilo_Azul"><%=nombre%>  <%=texto%></span></td>
</tr>
<tr>
  <td valign="top" bgcolor="#FFFFFF" ><%=situacion%></td>
</tr>
<tr>
  <td valign="top" bgcolor="#FFFFFF" ><%=calidad%> <%=rama%></td>
</tr>
<tr>
  <td valign="top" bgcolor="#FFFFFF" ><%=texto_curso%></td>
</tr>
<form name="form" method="post">
<input type="hidden" name="listado" value="1">
<input type="hidden" name="rut" value="<%=rut_alumno%>">
<input type="hidden" name="perfil" value="<%=perfil%>">
</form>
</table>
<%} %>
</body>
</html>
