<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 11/06/2007
   // �ltima Actualizaci�n :
   // Rama Deportiva. se registran y mantienen todas las ramas deportivas y sus profesores.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");

  int rut_usuario = session.getAttribute("rut_usuario") != null?
                  Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  int cod_perfil = session.getAttribute("cod_perfil") != null?
                  Integer.parseInt(session.getAttribute("cod_perfil")+""):0;

if (rut_usuario == 0)  response.sendRedirect("index.jsp");

 Vector vec = new Vector();
 general.getAbrirConexion();
 general.setGenerarConexionRegistro();
 vec = general.getRamaDeportiva();
 general.getCerrarConexion();

 java.util.Calendar f = new java.util.GregorianCalendar();
 int dia = f.get(java.util.Calendar.DAY_OF_MONTH);
 int mes = f.get(java.util.Calendar.MONTH) + 1;
 int a�o = f.get(java.util.Calendar.YEAR);
 int hora = f.get(java.util.Calendar.HOUR);
 int minuto = f.get(java.util.Calendar.MINUTE);

 %>
<html>
<head>
<title>Gimnasio - Rama Deportiva</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/boton.js"></script>
<script language="JavaScript" src="js/trim.js"></script>
</head>

<body >
<table width="100%"  border="0">
  <tr>
    <td width="20%" height="50" class="letra8" >Gimnasio III<br>
      USM </td>
    <td width="65%" class="letra10" align="center"><span class="letra8">Ramas Deportivas<br>
        <br>
    </span></td>
    <td width="15%" >&nbsp;
        <div align="right"><span class="letra8"><%=dia%>/<%=mes%>/<%=a�o%><br>
        <%=hora%>:<%=minuto%></span></div></td>
  </tr>
</table>
<%if (vec.size() > 0){%>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
 <tr >
    <td height="35" >
      <table width="100%"  border="0" cellspacing="0" cellpadding="1">
        <tr>
          <td width="100%" >
            <table width="100%"  border="1" cellspacing="0" cellpadding="2">
              <tr >
                <td height="13" >N&deg;</td>
                <td >Rama</td>
                <td >Profesor</td>
              </tr>
              <%for(int i=0;i<vec.size();i++){%>
                <tr>
                <td width="5%" height="13" ><%=i+1%></td>
                <td width="39%" ><span id="act<%=i%>-1"><%=((Vector) vec.get(i)).get(1)+ ""%></span></td>
                <td width="39%" ><span id="act<%=i%>-2"><%=((Vector) vec.get(i)).get(5)+ ""%></span></td>
                </tr>
               <%}%>
            </table>
          </td>
        </tr>
    </table></td>
  </tr>
</table>
<div align="center">
  <%} else {%>
  <br>
  No se encontraron registros.
  <br>
  <%}%>
</div>
<div align="center"><font class="barra" color="#000000"><br>
&copy; Universidad T&eacute;cnica Federico Santa Mar&iacute;a</font>
</div>
</body>
</html>