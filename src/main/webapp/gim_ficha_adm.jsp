<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="gimnasio.*"%>

<% // Creado por           : M�nica Barrera Frez
   // Fecha                : 27/06/2007
   // �ltima Actualizaci�n : 31/08/2007
   // Ficha Se muestra la ficha personal del alumno o funcionario.
%>

<jsp:useBean id="general" class="gimnasio.BeanGeneral"></jsp:useBean>
<jsp:useBean id="us" class="gimnasio.BeanUsuarios"></jsp:useBean>

<%response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);
  response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  int rut_usuario = session.getAttribute("rut_usuario") != null?
                    Integer.parseInt(session.getAttribute("rut_usuario")+""):0;
  if (rut_usuario == 0)  response.sendRedirect("index.jsp");

  int perfil      = session.getAttribute("cod_perfil") != null?
                    Integer.parseInt(session.getAttribute("cod_perfil")+""):0;
  int rutnum      =  request.getParameter("rutnum")!=null &&
                     !request.getParameter("rutnum").equals("null") &&
                     !request.getParameter("rutnum").equals("NULL") &&
                     !request.getParameter("rutnum").equals("")  &&
                     !request.getParameter("rutnum").equals(" ")?
                     Integer.parseInt(request.getParameter("rutnum")+""):0;
  String dvrut    =  request.getParameter("dvrut")!=null &&
                     !request.getParameter("dvrut").equals("null") &&
                     !request.getParameter("dvrut").equals("NULL") &&
                     !request.getParameter("dvrut").equals("")  &&
                     !request.getParameter("dvrut").equals(" ")?
                     request.getParameter("dvrut")+"":"";
  String rut_aux  =  request.getParameter("rut_aux")!=null &&
                     !request.getParameter("rut_aux").equals("null") &&
                     !request.getParameter("rut_aux").equals("NULL") &&
                     !request.getParameter("rut_aux").equals("")  &&
                     !request.getParameter("rut_aux").equals(" ")?
                     request.getParameter("rut_aux")+"":"";
  boolean esAdministrador = perfil == 2;
//System.out.println("rut_usuario: "+ rut_usuario);
  Vector vec = new Vector();
  Vector vec_rama = new Vector();
  Vector vec_reg = new Vector();
  String rut = "";
  String nombre = "";
  String carrera = "";
  String rol = "";
  String ingreso = "";
  String nac = "";
  String calidad = "";
  String rama = "";
  String prof_rama = "";
  String estado = "";
  String depto = "";
  String anexo = "";
  String email = "";
  String tipo_contrato = "";
  String planta = "";
  int cod_perfil = 0;
  int origen = 0;
  boolean muestra = false;
  int codContrato = 0;
  int fono = 0;
  boolean cabecera = false;
  
  if (session.getAttribute("rut_consulta") != null)
	   session.removeAttribute("rut_consulta");
  
  Vector vec_asistaerobica = new Vector();
  Vector vec_asistmaquinas = new Vector();
  Vector vec_periodo = new Vector();
  Vector vec_tutoria = new Vector();
  Vector vec_periodo_sala = new Vector();

  String f_inicio = "";
  String f_termino = "";
  String s_inicio_ant = "";
  String a�o_sem_m = "";
  String a�o_sem_a = "";
  String imagen = "";
  String texto_imagen = "";
  String cargo = "";
  String jerarquia = "";
  String contrato = "";

  general.getAbrirConexion();
  general.setGenerarConexionDatosAl();
  general.setGenerarConexionRegistro();
  general.setGenerarConexionReserva();


  if (rutnum > 0){

  session.setAttribute("rut_consulta",rutnum+"");

  vec_reg = general.getRegistroUsuario(rutnum);

  if (vec_reg.size()>0) {
    muestra = true;
    if ((vec_reg.get(1)+"").trim().equals("1") ||
         (vec_reg.get(1)+"").trim().equals("2") ||
         (vec_reg.get(1)+"").trim().equals("3") )
    {
      vec = general.getAlumno(rutnum);
     
      
      if ((vec_reg.get(1)+"").trim().equals("3") ) // es de rama
      {
        vec_rama = general.getAlumnoRama(rutnum);
        calidad = "Rama Deportiva";
        if (vec_rama.size()>0){
          rama = vec_rama.get(1)+"";
          prof_rama = vec_rama.get(3)+"";
        }

      }
      if ((vec_reg.get(1)+"").trim().equals("2")) {
        calidad = "Tutor�a";
        general.setGenerarConexionReserva();
        vec_tutoria = general.getReservaAluTutoria (vec_reg.get(4)+"");
          }
      }
      if (((vec_reg.get(1)+"").trim().equals("5") ||
           (vec_reg.get(1)+"").trim().equals("6")  )){ // buscar es de vi�a
      /*general.getAbrirConexionVi�a();
      general.setGenerarConexionDatosVi�a();
      vec = general.getAlumnoVi�a(rutnum);
      general.getCerrarConexionVi�a();
      vec = general.getAlumno(rutnum);*/

      if ((vec_reg.get(1)+"").trim().equals("6"))
         calidad ="Rama Deportiva - Vi�a";
    }

    if (vec.size() > 0){
    	
      cod_perfil = 8;
      rut = vec.get(12)+"";
      nombre = vec.get(1)+"";
      carrera = vec.get(5)+"";
      rol = vec.get(9)+"";
      ingreso = vec.get(10)+"";
      nac = vec.get(11)+"";

    } else { // es funcionario
      cod_perfil = 19;
      //if ((vec_reg.get(1)+"").trim().equals("4") ){ // es funcionario CC
        us.getAbrirConexion();
        us.setGenerarConexionUsuario();
        vec = us.getFuncionario(rutnum);
        us.getCerrarConexion();
        origen = 1;
       if (vec.size()>0){
        rut = vec.get(0)+"-"+vec.get(1)+"";
        nombre = vec.get(2)+" "+vec.get(3)+" "+vec.get(4)+"";
        /*depto = vec.get(15) + ", "+ vec.get(17);
        planta = vec.get(18)+"";
        cargo = vec.get(19)+"";
        contrato = vec.get(14)+"";
        jerarquia = vec.get(13)+"";
        codContrato =(vec.get(20) != null)?Integer.parseInt(vec.get(20)+""):0;
        fono =(vec.get(8) != null)?Integer.parseInt(vec.get(8)+""):0;*/
        depto = vec.get(14) + ", "+ vec.get(20);
        planta = vec.get(17)+"";
        cargo = vec.get(18)+"";
        contrato = vec.get(13)+"";
        jerarquia = vec.get(12)+"";
        codContrato =(vec.get(19) != null)?Integer.parseInt(vec.get(19)+""):0;
        fono =(vec.get(8) != null)?Integer.parseInt(vec.get(8)+""):0;
      }
       /*} else {// es funcionario de vi�a
        general.getAbrirConexionVi�a();
        general.setGenerarConexionDatosVi�a();
        vec = general.getFuncionarioVi�a(rutnum);
        general.getCerrarConexionVi�a();
      if (vec.size()>0){
        rut = vec.get(13)+"";
        nombre = vec.get(0)+" "+vec.get(1)+" "+vec.get(2)+"";
        //depto = vec.get(4) + ", "+ vec.get(5);
        //anexo = vec.get(6)+"";
        //email = vec.get(7)+"";
        //tipo_contrato = vec.get(11)+"";
        //planta = vec.get(12)+"";
        depto = vec.get(14) + ", "+ vec.get(20);
        anexo = vec.get(8)+"";
        email = "";
        tipo_contrato = vec.get(13)+"";
        planta = vec.get(17)+"";
      }
      }*/


    }

    

 } else muestra = false;

  }
  general.setGenerarConexionHorario();
  general.getPeriodoSemestreActual(1,1);

  a�o_sem_m = general.getA�oActual()+"-"+general.getSemestreActual();

  if (rut_usuario > 0){
	  session.setAttribute("rut_consulta","77777777");
	  //System.out.println("rut_usuario  ficha.adm linea 212: "+rutnum);
  }
%>
<html>
<head>
<title>Gimnasio - Ficha</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/estilo_boton.css" rel="stylesheet" type="text/css">
<script src="js/valida_login.js"></script>
<script language="JavaScript" src="js/trim.js"></script>
<script language="javascript">
  function noenter() {
  return !(window.event && window.event.keyCode == 13);
}
function enter(e){
  if (window.event) {
    if (window.event.keyCode == 13)
      validar(document.fdatos);

  }
  else {
    if (e.which == 13)
      validar(document.fdatos);
  }
}
function validar(form){

  if(trim(form.rut_aux.value)==""){
    alert("Debe ingresar el RUT a registrar");
    form.rut_aux.focus();
    return false;
  }
  if (!valida_rut2(form.rut_aux, form.rutnum, form.dvrut, 'RUT')) {
    form.rut_aux.focus();
    return false;
  }
  // return true;
  form.action = "gim_ficha_adm.jsp";
  form.target = "_self";
  form.submit();
}
function changeImageSrc(whichImage,newGraphic) {
  if (document.images) {
    document.images[whichImage].src = newGraphic;
  }
}
function SinFoto(foto){
  changeImageSrc(foto,"imagen/sinfoto.jpg");
}
</script>
<style type="text/css">
<!--
body {
        margin-left: 0px;
        margin-top: 0px;
}
-->
</style></head>

<body onload="document.fdatos.rut_aux.focus();">
<table width="100%"  border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td height="37" colspan="3" class="Estilo_Verde">FICHA </td>
  </tr>

  <form name="fdatos" method="post" action="" target="">
      <tr>
        <td><span class="Estilo_Gris">RUT</span></td>
        <td><input type="text" name="rut_aux" size="12" maxlength="12" value ="<%=rut_aux%>" onKeyPress="enter(event,1);return noenter()" onChange="checkRutField(document.fingresar.rut_aux.value, document.fingresar);" class="Input01">
            <input type="hidden" name="rutnum" value="<%=rutnum%>">
            <input type="hidden" name="dvrut" value="<%=dvrut%>">
            <a href="#"  onClick="validar(document.fdatos);" class="AIngr"><img src="imagen/gim_lupa.gif" width="16" height="18" border="0" align="absmiddle"> Buscar</a> </td>
            <%if (rutnum > 0 && vec_reg.size() > 0) {%>
            <td width="14%" rowspan="11" align="center" valign="top"><img src="foto?f=<%=rutnum%>" name="foto" width="120" height="135" border="1" align="absmiddle" onError="SinFoto('foto');"></td>
            <%}%>
</tr>
  </form>
  <% 
   if (vec_reg.size()>0) {
     %>
<tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Nombre</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=nombre%></span></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=(cod_perfil == 8)?"Carrera":"Departamento"%></span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(cod_perfil == 8)?carrera:depto%></span></td>
  </tr>
  <%if(origen == 1){ %>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Contrato</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=contrato%></span></td>
  </tr>
  <%if(codContrato != 3) { %>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Planta</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=planta%></span></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Jerarqu�a</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=jerarquia%></span></td>
  </tr>
  <%}%>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Cargo</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=cargo%></span></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Tel�fono</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=fono%></span></td>
  </tr>
<% } else { %>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=(cod_perfil == 8)?"Rol":"Anexo"%></span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(cod_perfil == 8)?rol:anexo%></span></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=(cod_perfil == 8)?"Ingreso":"Email"%></span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(cod_perfil == 8)?ingreso:email%></span></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris"><%=(cod_perfil == 8)?"Nacionalidad":"Tipo de Contrato"%></span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=(cod_perfil == 8)?nac:tipo_contrato%></span></td>
  </tr>
  <%}%>
  <%if(cod_perfil == 8 && vec_reg.size()>0 &&
   ((vec_reg.get(1)+"").trim().equals("3") || (vec_reg.get(1)+"").trim().equals("6") ||
   (vec_reg.get(1)+"").trim().equals("2"))){%>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Calidad</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=calidad%></span></td>
  </tr>
  <%if(cod_perfil == 8 && vec_reg.size()>0 &&
   ((vec_reg.get(1)+"").trim().equals("3") )) {%>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Rama</span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=rama%> </span></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Profesor de rama </span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=prof_rama%> </span></td>
  </tr>
  <%}%>
<% if ((vec_reg.get(1)+"").trim().equals("2")) {
    String sala = "";
    int indice = 0;
    for (int i=0;vec_tutoria.size()>0 && i<vec_tutoria.size();i++){
    if (!sala.trim().equals(((Vector) vec_tutoria.get(i)).get(0)+ "")){
      indice = 1;
%>
    <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Sala </span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=((Vector) vec_tutoria.get(i)).get(0)+ ""%> </span></td>
  </tr>
      <%} %>
   <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">Reserva <%=indice%></span></td>
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Azul"><%=((Vector) vec_tutoria.get(i)).get(1)%> </span></td>
  </tr>

  <%sala = ((Vector) vec_tutoria.get(i)).get(0)+ "";
   indice++; }}%>
 <%}%>
  <%
  
 } else { if (rutnum > 0){%>
 <tr>
    <td>&nbsp; </td>
   <td valign="top" bgcolor="#FFFFFF">  
   <table width="100%"  border="0" cellspacing="0" cellpadding="0">
   <tr><td><font class="letra7" color="red"> No se encuentra Registrado en el Gimnasio</font></td></tr>
   </table>
   </td>

  </tr>
    <%}}%>
</table>
<%int cont_asiste = 0;
  int cont_falta = 0;
  int cont_justifica = 0;
  int cont_espera = 0;
  String icono = "";

if (vec_reg.size() > 0) {
   vec_periodo_sala = general.getPeriodoGimnasio(general.getA�oActual(),general.getSemestreActual(),10335);
    if (vec_periodo_sala.size() > 0){
   for (int g=0;g<vec_periodo_sala.size();g++){
//   vec_periodo = general.getPeriodoGimnasioSala(10335,((Vector)vec_periodo_sala.get(g)).get(0)+"");
   if (vec_periodo_sala.size()>0) {
  f_inicio = (vec_periodo_sala.size()>0)?((Vector)vec_periodo_sala.get(g)).get(6)+"":"";
  f_termino = (vec_periodo_sala.size()>0)?((Vector)vec_periodo_sala.get(g)).get(7)+"":"";
  a�o_sem_m = String.valueOf(general.getA�oActual())+"-"+String.valueOf(general.getSemestreActual());

  vec_asistmaquinas = general.getAsistenciaGimnasio2(rutnum, f_inicio, f_termino,
                      ((Vector)vec_periodo_sala.get(g)).get(5)+"");


  if (vec_asistmaquinas.size() > 0 && !cabecera) cabecera = true;

  if (vec_asistmaquinas.size() > 0){
  if (cabecera){%>
<table width="100%"  border="0" cellpadding="1" cellspacing="0">
  <tr>
    <td width="100%" height="37" class="Estilo_Verde">ASISTENCIA</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">M&Aacute;QUINAS <%=a�o_sem_m%></span></td>
  </tr>
</table>

<%}
  String periodo_act = "";
  String periodo_ant = "";

  for (int i=0;i < vec_asistmaquinas.size();i++){
   periodo_act = ((Vector)vec_periodo_sala.get(g)).get(5)+"";
   imagen = "";
   icono = "&nbsp;";
   if (!periodo_ant.equals(periodo_act)) {
     cont_asiste = 0;
     cont_falta = 0;
     cont_justifica = 0;
     cont_espera = 0;
   }
   switch (Integer.parseInt(((Vector)vec_asistmaquinas.get(i)).get(2)+"")) {
     case 1: {imagen = "imagen/gim_asistio.gif";// asiste
              texto_imagen = "Asisti�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_asiste ++;
       break;
     }
     case 2: {imagen = "imagen/gim_justifico.gif";  // justific�
              texto_imagen = "Justific�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_justifica ++;
       break;
     }
     case 3: {imagen = "imagen/gim_falto.gif";   // falta
              texto_imagen = "Falt�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_falta ++;
       break;
     }
   case 4: {imagen = "imagen/gim_espera.gif";   // espera
           texto_imagen = "Espera";
           icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
           cont_espera ++;
    break;
     }

   }
   if (!periodo_ant.equals(periodo_act)) {
    %>
  <br>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="20" class="Estilo_Azul">Reservas del Per&iacute;odo <%=((Vector)vec_periodo_sala.get(g)).get(0)+""%> - <%=((Vector)vec_periodo_sala.get(g)).get(1)+""%></td>
  </tr>
  </table>
  <%} if (i == 0) {%>
  <table width="100%"  border="0" cellpadding="0" cellspacing="1"  bgcolor="#CCCCCC" >
  <tr >
    <td width="5%" height="15" background="imagen/boton_baseFONDO05.gif">N&deg;</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Semana</td>
    <td width="10%" background="imagen/boton_baseFONDO05.gif">D&iacute;a</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Bloque</td>
   <% if (esAdministrador) {%>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Reservado por</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Asistencia por</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Fecha</td>
    <td align="center" background="imagen/boton_baseFONDO05.gif">Estado</td>
    <% } else { %>
    <td width="20%" background="imagen/boton_baseFONDO05.gif">Fecha</td>
    <td width="10%" align="center" background="imagen/boton_baseFONDO05.gif">Asistencia</td>
    <%} %>
  </tr>
  <%}%>
  <tr>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=i+1%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistmaquinas.get(i)).get(6)+""%> al <%=((Vector)vec_asistmaquinas.get(i)).get(7)+""%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistmaquinas.get(i)).get(5)+""%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistmaquinas.get(i)).get(3)+""%> - <%=((Vector)vec_asistmaquinas.get(i)).get(4)+""%></td>
    <% if (esAdministrador) {%>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><font class="letra7"><%=((Vector)vec_asistmaquinas.get(i)).get(8)+""%></font></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><font class="letra7"><%=((Vector)vec_asistmaquinas.get(i)).get(9)+""%></font></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistmaquinas.get(i)).get(1)+""%></td>
    <td align="center" bgcolor="#FFFFFF"><%=icono%></td>
    <% } else { %>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistmaquinas.get(i)).get(1)+""%></td>
    <td align="center" bgcolor="#FFFFFF"><%=icono%></td>
    <%} %>
  </tr>
  <% if (i == vec_asistmaquinas.size()-1) {%>
  <tr>
    <td height="20" colspan="7" align="center" background="imagen/boton_baseFONDO06.gif"><span class="Estilo_Azulino"><img src="imagen/gim_asistio.gif" alt="ASISTI&Oacute;" width="14" height="14" align="absmiddle"> Asiste : <strong><%=cont_asiste%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_falto.gif" alt="FALT&Oacute;" width="15" height="15" align="absmiddle"> Falta : <strong><%=cont_falta%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_justifico.gif" alt="JUSTIFIC&Oacute;" width="17" height="18" align="absmiddle"> Justifica : <strong><%=cont_justifica%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_espera.gif" alt="E" width="17" height="18" align="absmiddle"> Espera : <strong><%=cont_espera%></strong></span></td>
    <td align="center" background="imagen/boton_baseFONDO06.gif"><span class="Estilo_Azulino"><strong>&nbsp;</strong></span>Total : <strong><%=cont_asiste%></strong> </td>
  </tr>
  </table>
  <%}%>
<% periodo_ant = periodo_act; }%>

<br>
<%}}}
 // vec_periodo = general.getPeriodoGimnasioVigente(10336);
//  if (vec_periodo.size()>0) {
//  f_inicio = (vec_periodo.size()>0)?vec_periodo.get(5)+"":"";
//  f_termino = (vec_periodo.size()>0)?vec_periodo.get(6)+"":"";
//  a�o_sem_a = (vec_periodo.size()>0)?(vec_periodo.get(3)+"-"+vec_periodo.get(4)+""):"";

    cabecera = false;
    vec_periodo_sala = general.getPeriodoGimnasio(general.getA�oActual(),general.getSemestreActual(),10336);
    if (vec_periodo_sala.size() > 0){
    for (int g=0;g<vec_periodo_sala.size();g++){
//   vec_periodo = general.getPeriodoGimnasioSala(10335,((Vector)vec_periodo_sala.get(g)).get(0)+"");
    if (vec_periodo_sala.size()>0) {
   f_inicio = (vec_periodo_sala.size()>0)?((Vector)vec_periodo_sala.get(g)).get(6)+"":"";
   f_termino = (vec_periodo_sala.size()>0)?((Vector)vec_periodo_sala.get(g)).get(7)+"":"";
   a�o_sem_a = String.valueOf(general.getA�oActual())+"-"+String.valueOf(general.getSemestreActual());


  vec_asistaerobica = general.getAsistenciaGimnasio2(rutnum, f_inicio, f_termino,
                      ((Vector)vec_periodo_sala.get(g)).get(5)+"");
  cont_asiste = 0;
  cont_falta = 0;
  cont_justifica = 0;
  cont_espera = 0;

  if (vec_asistaerobica.size() > 0 && !cabecera) cabecera = true;

  if (vec_asistaerobica.size() > 0){
  if (cabecera){%>
<table width="100%"  border="0" cellspacing="0" cellpadding="1">
  <tr bgcolor="#FFFFFF">
    <td valign="top" bgcolor="#FFFFFF"><span class="Estilo_Gris">MULTIUSO <%=a�o_sem_a%></span></td>
  </tr>
</table>
<%}
  String periodo_act = "";
  String periodo_ant = "";
  for (int i=0;i < vec_asistaerobica.size();i++){
   periodo_act = ((Vector)vec_periodo_sala.get(g)).get(5)+"";
   imagen = "";
   icono = "&nbsp;";
   if (!periodo_ant.equals(periodo_act)) {
     cont_asiste = 0;
     cont_falta = 0;
     cont_justifica = 0;
     cont_espera = 0;
   }
   switch (Integer.parseInt(((Vector)vec_asistaerobica.get(i)).get(2)+"")) {
     case 1: {imagen = "imagen/gim_asistio.gif";// asiste
              texto_imagen = "Asisti�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_asiste ++;
       break;
     }
     case 2: {imagen = "imagen/gim_justifico.gif";  // justific�
              texto_imagen = "Justific�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_justifica ++;
       break;
     }
     case 3: {imagen = "imagen/gim_falto.gif";   // falta
              texto_imagen = "Falt�";
              icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
              cont_falta ++;
       break;
     }
   case 4: {imagen = "imagen/gim_espera.gif";   // espera
      texto_imagen = "Espera";
      icono = "<img src=\""+imagen+"\" alt=\""+texto_imagen+"\" align=\"absmiddle\">";
      cont_espera ++;
break;
     }
   }
   if (!periodo_ant.equals(periodo_act)) {
    %>
  <br>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="20" class="Estilo_Azul">Reservas del Per&iacute;odo <%=((Vector)vec_periodo_sala.get(g)).get(0)+""%> - <%=((Vector)vec_periodo_sala.get(g)).get(1)+""%></td>
  </tr>
  </table>
  <%} if (i == 0) {%>
  <table width="100%"  border="0" cellpadding="0" cellspacing="1"  bgcolor="#CCCCCC" >
  <tr >
    <td width="5%" height="15" background="imagen/boton_baseFONDO05.gif">N&deg;</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Semana</td>
    <td width="10%" background="imagen/boton_baseFONDO05.gif">D&iacute;a</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Bloque</td>
    <% if (esAdministrador) {%>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Reservado por</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Asistencia por</td>
    <td width="15%" background="imagen/boton_baseFONDO05.gif">Fecha</td>
    <td align="center" background="imagen/boton_baseFONDO05.gif">Estado</td>
    <% } else { %>
    <td width="20%" background="imagen/boton_baseFONDO05.gif">Fecha</td>
    <td width="10%" align="center" background="imagen/boton_baseFONDO05.gif">Asistencia</td>
    <%} %>
  </tr>
  <%}%>
  <tr>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=i+1%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistaerobica.get(i)).get(6)+""%> al <%=((Vector)vec_asistaerobica.get(i)).get(7)+""%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistaerobica.get(i)).get(5)+""%></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistaerobica.get(i)).get(3)+""%> - <%=((Vector)vec_asistaerobica.get(i)).get(4)+""%></td>
    <% if (esAdministrador) {%>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><font class="letra7"><%=((Vector)vec_asistaerobica.get(i)).get(8)+""%></font></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><font class="letra7"><%=((Vector)vec_asistaerobica.get(i)).get(9)+""%></font></td>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistaerobica.get(i)).get(1)+""%></td>
    <td align="center" bgcolor="#FFFFFF"><%=icono%></td>
    <% } else { %>
    <td bgcolor="#FFFFFF" class="Estilo_Azul"><%=((Vector)vec_asistaerobica.get(i)).get(1)+""%></td>
    <td align="center" bgcolor="#FFFFFF"><%=icono%></td>
    <%} %>

  </tr>
  <% if (i == vec_asistaerobica.size()-1) {%>
  <tr>
    <td height="20" colspan="7" align="center" background="imagen/boton_baseFONDO06.gif"><span class="Estilo_Azulino"><img src="imagen/gim_asistio.gif" alt="ASISTI&Oacute;" width="14" height="14" align="absmiddle"> Asiste : <strong><%=cont_asiste%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_falto.gif" alt="FALT&Oacute;" width="15" height="15" align="absmiddle"> Falta : <strong><%=cont_falta%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_justifico.gif" alt="JUSTIFIC&Oacute;" width="17" height="18" align="absmiddle"> Justifica : <strong><%=cont_justifica%></strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="Estilo_Azulino"><img src="imagen/gim_espera.gif" alt="E" width="17" height="18" align="absmiddle"> Espera : <strong><%=cont_espera%></strong></span></td>
    <td align="center" background="imagen/boton_baseFONDO06.gif"><span class="Estilo_Azulino"><strong>&nbsp;</strong></span>Total : <strong><%=cont_asiste%></strong> </td>
  </tr>
  </table>
  <%}%>
<% periodo_ant = periodo_act;}}}%>

<%}}}%>
<%if (vec_asistmaquinas.size() > 0 || vec_asistaerobica.size() > 0 ){%>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="40" valign="bottom"><span class="Estilo_GrisOsc"><img src="imagen/gim_asistio.gif" alt="ASISTI&Oacute;" width="14" height="14" align="absmiddle">Asiste &nbsp;&nbsp;<img src="imagen/gim_falto.gif" alt="FALT&Oacute;" width="15" height="15" align="absmiddle">Falta&nbsp;&nbsp; <img src="imagen/gim_justifico.gif" alt="JUSTIFIC&Oacute;" width="17" height="18" align="absmiddle"> Justifica&nbsp;&nbsp; <img src="imagen/gim_espera.gif" alt="ESPERA" width="17" height="18" align="absmiddle"> En Espera</span></td>
  </tr>
</table>
<%}} general.getCerrarConexion();%>
</body>
</html>
